---
image: registry.gitlab.com/acklio/dev-sdk/devsdk-registry/gcc-arm:11.2
variables:
  GIT_SUBMODULE_STRATEGY: recursive
  GIT_STRATEGY: clone
  PACKAGE_PROJECT_ID: 24101800
  DUMMY_VERSION: 1.0.0
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${PACKAGE_PROJECT_ID}/packages/generic/acklio-sdk-apps-${CI_COMMIT_TAG}/${DUMMY_VERSION}"
  BASE_RELEASE_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/${CI_COMMIT_TAG}"
  app_name:
    value: "SDKTunApp"
    description: "Select the application to build"
  l2_stack:
    value: "udp"
    description: "Select the layer 2 implementation"
  platform:
    value: "linux"
    description: "Select the target platform"
  toolchain:
    value: "gcc-native"
    description: "Toolchain to be used to build the application"
  target:
    value: "default"
    description: "Target for the toolchain"
  optional_env_variables:
    value: ""
    description: "Additionnal VAR=VALUE statements to be passed to the build command"

stages:
  - prepare
  - build
  - build_obfuscation
  - upload
  - release
  - release_additions

# Avoid double pipelines when using rules
# See https://gitlab.com/gitlab-org/gitlab/-/issues/34756
workflow:
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "web"

# prepare
build_version_string:
  image: alpine:3.17
  stage: prepare
  variables:
    GIT_SUBMODULE_STRATEGY: none
  script:
    - if [ -n "${CI_COMMIT_TAG}" ]; then echo -n "${CI_COMMIT_TAG}-" >> sdk_version_string.txt; fi
    - echo -n "${CI_COMMIT_SHORT_SHA}" >> sdk_version_string.txt
    - if [ -n "${EXTENSION_API}" ]; then echo -n "-api_${EXTENSION_API}" >> sdk_version_string.txt; fi
    - if [ -n "${TRACE_ENABLED}" ]; then echo -n "-trace" >> sdk_version_string.txt; fi
  artifacts:
    paths:
      - sdk_version_string.txt

# Scheduled applications build stages.
.build_scheduled_template: &build_scheduled_definition
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_COMMIT_TAG && $CI_PIPELINE_SOURCE != "web"'
      when: always
    - when: never
  before_script:
    - apt-get update && apt-get install -y libudev-dev
  script:
    - env
    - >
      cmake -S . -B ./build/
      -DFULLSDK_VERSION=$(cat sdk_version_string.txt)
      -DBUILD_INFO=$(cat sdk_version_string.txt)-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_JOB_ID}
      -DAPP_NAME=${app_name}
      -DPLATFORM=${platform}
      -DTOOLCHAIN=${toolchain}
      -DTARGET=${target}
      -DL2_STACK=${l2_stack}
      ${optional_env_variables} && make -C ./build/
    - >
      cp build/${toolchain}/${target}/${app_name}.bin
      fullsdk-apps-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_COMMIT_TAG}-${CI_JOB_ID}.bin
      || cp build/${toolchain}/${target}/${app_name}
      fullsdk-apps-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_COMMIT_TAG}-${CI_JOB_ID}
  artifacts:
    paths:
      - fullsdk-apps-*

build_ATModem_Multi_Region:
  <<: *build_scheduled_definition
  before_script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar
  variables:
    app_name: ATModem
    platform: B-L072Z-LRWAN1
    toolchain: gcc-arm-none
    target: m0plus
    l2_stack: semtech_4.4.7
    optional_env_variables: >
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DAPP_VERSION=CI
      -DFULLSDK_LORA_REGIONS=REGION_AS923_2,REGION_EU868
      -DFULLSDK_LORA_AS923_SUBREGION=CHANNEL_PLAN_GROUP_AS923_2
      -DL2_REGION_PATCH=everynet
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON
      -DMAX_MTU_SIZE=256
      -DFULLSDK_MAX_PAYLOAD_SIZE=768
      -DFULLSDK_DTG_MAX_SOCKET=1

build_DownlinkFrag:
  <<: *build_scheduled_definition
  variables:
    app_name: DownlinkFrag
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111

build_DLMSMulticast_st:
  <<: *build_scheduled_definition
  image: registry.gitlab.com/acklio/dev-sdk/devsdk-registry/st1.3.1:11.2
  before_script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar
  variables:
    app_name: DLMSMulticast
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DLORAWAN_GENAPPKEY=11111111111111111111111111111111

build_x86_FragAoEExample:
  <<: *build_scheduled_definition
  variables:
    app_name: FragAoEExample
    platform: linux
    toolchain: gcc-native
    target: default
    l2_stack: udp
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON

build_uoscore_tun_proxy:
  <<: *build_scheduled_definition
  before_script:
    - apt-get update && apt-get install -y libudev-dev pkg-config autoconf automake libtool libsodium-dev
    - ./examples/OscoreTunProxy/build_libs.sh
  after_script:
    - mkdir deps
    - cp libs/nanocbor/bin/nanocbor.so libs/libcose/bin/libcose.so libs/libcoap/.libs/libcoap-2.so.2.0.1 libs/libsodium/src/libsodium/.libs/libsodium.so.23.3.0 deps
    - for f in deps/; do cp -- "$f" "${app_name}-$f"; done
  variables:
    app_name: OscoreTunProxy
    platform: linux
    toolchain: gcc-native
    target: default
    l2_stack: udp
    optional_env_variables: >
      -DEXTENSION_API=orangelabs
      -DFRAGMENTATION_API=p2p
      -DOSCORE_LIB=UOSCORE
      -DEDHOC_TEST=1
  artifacts:
    paths:
      - fullsdk-apps-*
      - deps/*

build_oscore_tun_proxy:
  <<: *build_scheduled_definition
  before_script:
    - apt-get update && apt-get install -y libudev-dev pkg-config autoconf automake libtool libsodium-dev
    - ./examples/OscoreTunProxy/build_libs.sh
  after_script:
    - mkdir deps
    - cp libs/nanocbor/bin/nanocbor.so libs/libcose/bin/libcose.so libs/libcoap/.libs/libcoap-2.so.2.0.1 libs/libsodium/src/libsodium/.libs/libsodium.so.23.3.0 deps
    - for f in deps/; do cp -- "$f" "${app_name}-$f"; done
  variables:
    app_name: OscoreTunProxy
    platform: linux
    toolchain: gcc-native
    target: default
    l2_stack: udp
    optional_env_variables: >
      -DEXTENSION_API=orangelabs
      -DFRAGMENTATION_API=p2p
      -DOSCORE_LIB=LIBOSCORE
      -DEDHOC_TEST=1
  artifacts:
    paths:
      - fullsdk-apps-*
      - deps/*

build_x86_PingPongEdhoc:
  <<: *build_scheduled_definition
  before_script:
    - apt-get update && apt-get install -y libudev-dev pkg-config autoconf automake libtool libsodium-dev
    - ./examples/PingPongEdhoc/build_libs.sh
  variables:
    app_name: PingPongEdhoc
    platform: linux
    l2_stack: udp
    toolchain: gcc-native
    target: default
    optional_env_variables: >
      -DEXTENSION_API=fraunhofer
      -DFRAGMENTATION_API=p2p
      -DEDHOC_TEST=1

build_x86_PingPong:
  <<: *build_scheduled_definition
  variables:
    app_name: PingPong
    platform: linux
    toolchain: gcc-native
    target: default
    l2_stack: udp
    optional_env_variables: >
      -DEXTENSION_API=nocomp
      -DFRAGMENTATION_API=nocomp
      -DTRACE_ENABLED=1
      -DTRACE_LEVEL=0

build_SDKTunApp_risingHF_p2p:
  <<: *build_scheduled_definition
  variables:
    app_name: SDKTunApp
    platform: linux
    l2_stack: at_risinghf_p2p
    toolchain: gcc-native
    target: default
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udpdynsuffixport
      -DFRAGMENTATION_API=p2p
      -DSCHC_CORE_MODE=1
      -DMBEDTLS_IO_BUFF_SIZE=2000

build_semtech_UpDownlinkFrag_external_timeserver:
  <<: *build_scheduled_definition
  variables:
    app_name: UpDownFrag
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DUSE_EXTERNAL_TIMESERVER=1

build_x86_UpDownFrag_RisingHF:
  <<: *build_scheduled_definition
  variables:
    app_name: UpDownFrag
    platform: linux
    toolchain: gcc-native
    target: default
    l2_stack: at_risinghf
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111

build_AT_Sigfox_modem:
  <<: *build_scheduled_definition
  image: registry.gitlab.com/acklio/dev-sdk/devsdk-registry/st1.3.1:11.2
  variables:
    app_name: AT_Sigfox_modem
    platform: B-L072Z-LRWAN1
    toolchain: gcc-arm-none
    target: m0plus
    l2_stack: sigfox
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=dynamic
      -DPLATFORM_TRACE_ENABLED=ON
      -DMGT_HEADER_COMP_RULES_SIZE=1000
      -DPAYLOAD_MEMORY_SIZE=0
      -DFULLSDK_MAX_PAYLOAD_SIZE=500

build_arm_template: &build_linux_arm_definition
  stage: build
  rules:
    - if: '$CI_COMMIT_TAG && $CI_PIPELINE_SOURCE != "web"'
  script:
    - apt-get update && apt-get install gcc-arm-linux-gnueabihf -y
    - >
      cmake -S . -B ./build/
      -DFULLSDK_VERSION=$(cat sdk_version_string.txt)
      -DBUILD_INFO=$(cat sdk_version_string.txt)-${CI_JOB_ID}
      -DAPP_NAME=${app_name}
      -DPLATFORM=${platform}
      -DTOOLCHAIN=${toolchain}
      -DTARGET=${target}
      -DL2_STACK=${l2_stack}
      ${optional_env_variables} && make -C ./build/
    - >
      cp build/${toolchain}/${target}/${app_name}.bin
      fullsdk-apps-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_COMMIT_TAG}-${CI_JOB_ID}.bin
      || cp build/${toolchain}/${target}/${app_name}
      fullsdk-apps-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_COMMIT_TAG}-${CI_JOB_ID}
  artifacts:
    paths:
      - fullsdk-apps-*

# Example application build stages.
.build_template: &build_definition
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE != "web"'
  before_script:
    - apt-get update && apt-get install -y libudev-dev
  script:
    - env
    - >
      cmake -S . -B ./build/
      -DFULLSDK_VERSION=$(cat sdk_version_string.txt)
      -DBUILD_INFO=$(cat sdk_version_string.txt)-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_JOB_ID}
      -DAPP_NAME=${app_name}
      -DPLATFORM=${platform}
      -DTOOLCHAIN=${toolchain}
      -DTARGET=${target}
      -DL2_STACK=${l2_stack}
      ${optional_env_variables} && make -C ./build/
    - >
      cp build/${toolchain}/${target}/${app_name}.bin
      fullsdk-apps-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_COMMIT_TAG}-${CI_JOB_ID}.bin
      || cp build/${toolchain}/${target}/${app_name}
      fullsdk-apps-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_COMMIT_TAG}-${CI_JOB_ID}

# Every commit build stages.
build_SDKTunApp_risingHF:
  <<: *build_definition
  variables:
    app_name: SDKTunApp
    platform: linux
    l2_stack: at_risinghf
    toolchain: gcc-native
    target: default
    optional_env_variables: >
      -DLORAWAN_APPKEY=42424242424242424242424242424242
      -DDTLS_ENABLED=ON
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DMBEDTLS_IO_BUFF_SIZE=2000

build_SDKTunApp_nb_iot_slm:
  <<: *build_definition
  variables:
    app_name: SDKTunApp
    platform: linux
    l2_stack: nb_iot_slm
    toolchain: gcc-native
    target: default
    optional_env_variables: >
      -DDTLS_ENABLED=ON
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DFULLSDK_DTG_MAX_SOCKET=1
      -DMBEDTLS_IO_BUFF_SIZE=2000

build_SDKTunApp_nb_iot_exs82:
  <<: *build_definition
  variables:
    app_name: SDKTunApp
    platform: linux
    l2_stack: nb_iot_exs82
    toolchain: gcc-native
    target: default
    optional_env_variables: >
      -DDTLS_ENABLED=ON
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DFULLSDK_DTG_MAX_SOCKET=1
      -DMBEDTLS_IO_BUFF_SIZE=2000

build_ATModem_DTG_template_dynamic:
  <<: *build_definition
  before_script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --lar libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sobj libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --ssym libfullsdk.a
  variables:
    app_name: ATModem
    platform: B-L072Z-LRWAN1
    toolchain: gcc-arm-none
    target: m0plus
    l2_stack: semtech_4.4.7
    optional_env_variables: >
      -DAPP_VERSION=CI
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DEXTENSION_API=template
      -DTEMPLATE_ID=dynamic
      -DFULLSDK_DTG_MAX_SOCKET=1
      -DMGT_HEADER_COMP_RULES_SIZE=1200
      -DMGT_PAYLOAD_COMP_RULES_SIZE=0
      -DFULLSDK_MAX_PAYLOAD_SIZE=512
      -DSCHC_CERTIFICATION_ENABLED=OFF

build_ATModem_DTG_LBM_template_ipv6udp:
  <<: *build_definition
  before_script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --lar libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sobj libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --ssym libfullsdk.a
  variables:
    app_name: ATModem
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: lbm
    optional_env_variables: >
      -DAPP_VERSION=CI
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DFULLSDK_DTG_MAX_SOCKET=1

build_ATModem_NET_LBM_template_ipv6udpdlms:
  <<: *build_definition
  before_script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --lar libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sobj libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --ssym libfullsdk.a
  variables:
    app_name: ATModem
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: lbm
    optional_env_variables: >
      -DAPP_VERSION=CI
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udpdlms

build_ATModem_NET_Semtech_template_ipv6udpdlms:
  <<: *build_definition
  before_script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --lar libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sobj libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --ssym libfullsdk.a
  variables:
    app_name: ATModem
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DAPP_VERSION=CI
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udpdlms

build_ATModem_NET_template_ipv6udpdlms:
  <<: *build_definition
  before_script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --lar libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sobj libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --ssym libfullsdk.a
  variables:
    app_name: ATModem
    platform: B-L072Z-LRWAN1
    toolchain: gcc-arm-none
    target: m0plus
    l2_stack: semtech_4.4.7
    optional_env_variables: >
      -DAPP_VERSION=CI
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udpdlms
      -DFULLSDK_MAX_PAYLOAD_SIZE=768
      -DFULLSDK_DTG_MAX_SOCKET=1

build_ATModem_DTG_semtech_template_dynamic:
  <<: *build_definition
  before_script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --lar libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sobj libfullsdk.a
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --ssym libfullsdk.a
  variables:
    app_name: ATModem
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DAPP_VERSION=CI
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DEXTENSION_API=template
      -DTEMPLATE_ID=dynamic
      -DFULLSDK_DTG_MAX_SOCKET=1
      -DSYNC_ENABLED=ON
      -DDTLS_ENABLED=ON
      -DMBEDTLS_IO_BUFF_SIZE=1152

build_semtech_UpDownlinkFrag:
  <<: *build_definition
  variables:
    app_name: UpDownFrag
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111

build_LBM_UpDownFrag_LBM:
  <<: *build_definition
  variables:
    app_name: UpDownFrag_LBM
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: lbm
    optional_env_variables: >
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON
      -DFULLSDK_DTG_MAX_SOCKET=1

build_semtech_FragAoEExample:
  <<: *build_definition
  variables:
    app_name: FragAoEExample
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111

build_DLMSMulticast_semtech:
  <<: *build_definition
  before_script:
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar
  variables:
    app_name: DLMSMulticast
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DLORAWAN_GENAPPKEY=11111111111111111111111111111111

build_uIP_UDPClient:
  <<: *build_definition
  variables:
    app_name: uIP_udp_client_server_example
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DFABI=hard
      -DUSE_FREERTOS=ON
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111

build_x86_SDKTunApp:
  <<: *build_definition
  variables:
    app_name: SDKTunApp
    platform: linux
    toolchain: gcc-native
    target: default
    l2_stack: udp
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=dynamic
      -DMBEDTLS_IO_BUFF_SIZE=2000

build_x86_SDKTunApp_DTLS:
  <<: *build_definition
  variables:
    app_name: SDKTunApp
    platform: linux
    toolchain: gcc-native
    target: default
    l2_stack: udp
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=dynamic
      -DDTLS_ENABLED=ON
      -DMBEDTLS_IO_BUFF_SIZE=2000

build_SCHC_Certification_app:
  <<: *build_definition
  image: registry.gitlab.com/acklio/dev-sdk/devsdk-registry/st1.3.1:11.2
  variables:
    app_name: SCHCCertificationApp
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
    optional_env_variables: >
      -DEXTENSION_API=template
      -DTEMPLATE_ID=dynamic
      -DSCHC_CERTIFICATION_ENABLED=ON
      -DAPP_VERSION=CI
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111

# build with obfuscation
build_obfus_FragAoEExample:
  stage: build_obfuscation
  rules:
    - if: '$CI_PIPELINE_SOURCE != "web"'
  variables:
    app_name: FragAoEExample
    platform: STM32L476RG-Nucleo
    toolchain: gcc-arm-none
    target: m4
    l2_stack: semtech
  before_script:
    - cpan -i File::Find::Rule
    - apt-get update && apt-get install -y python3-pip
    - pip3 install fpvgcc
  script:
    - env
    - >
      cmake -S . -B ./build/
      -DFULLSDK_VERSION=$(cat sdk_version_string.txt)
      -DBUILD_INFO=$(cat sdk_version_string.txt)-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_JOB_ID}
      -DAPP_NAME=${app_name}
      -DPLATFORM=${platform}
      -DTOOLCHAIN=${toolchain}
      -DTARGET=${target}
      -DL2_STACK=${l2_stack}
      -DEXTENSION_API=template
      -DTEMPLATE_ID=ipv6udp
      -DSYNC_ENABLED=ON
      -DLORAWAN_DEVEUI=1111111111111111
      -DLORAWAN_APPEUI=1111111111111111
      -DLORAWAN_APPKEY=11111111111111111111111111111111
      -DLORAWAN_APPKEY=2B7E151628AED2A6ABF7158809CF4F3C
      -DOBFUSCATION_ENABLED=ON
      -DOBFUS_PREFIX=cUsToMeRnAmE
      -DOBFUS_SEED=seedstring
      -DCRYPTO_KEY=0123456789012345 && make -C ./build/
    - cp build/${toolchain}/${target}/${app_name}.bin fullsdk-apps-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_COMMIT_TAG}.bin
  after_script:
    - fpvgcc build/${toolchain}/${target}/${app_name}.map --sar

# Example application manual build
build_manual_target:
  stage: build
  rules:
    - if: '$CI_COMMIT_TAG && $CI_PIPELINE_SOURCE == "web"'
  before_script:
    - set -x
    - apt-get update && apt-get install -y libudev-dev
    - if [ -z ${app_name+x} ]
      || [ -z ${platform+x} ]
      || [ -z ${l2_stack+x} ]
      || [ -z ${toolchain+x} ]
      || [ -z ${target+x} ];
      then
      echo "the 'app_name', 'platform', 'l2_stack', 'toolchain' and 'target' env vars are required for manual build ";
      exit 1;
      fi
    - if [[ ${toolchain} == "gcc-arm-linux-hf" ]];
      then
      apt-get install -y gcc-arm-linux-gnueabihf patchelf;
      fi
    - if [[ ${toolchain} == "gcc-arm-none" ]];
      then
      apt-get install -y gcc-arm-none-eabi;
      fi

  script:
    - env
    - >
      cmake -S . -B ./build/
      -DFULLSDK_VERSION=$(cat sdk_version_string.txt)
      -DBUILD_INFO=$(cat sdk_version_string.txt)-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_JOB_ID}
      -DAPP_NAME=${app_name}
      -DPLATFORM=${platform}
      -DL2_STACK=${l2_stack}
      -DTOOLCHAIN=${toolchain}
      -DTARGET=${target}
      ${optional_env_variables}
    - >
      cp build/${toolchain}/${target}/${app_name}.bin
      fullsdk-apps-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_COMMIT_TAG}-${CI_JOB_ID}.bin
      || cp build/${toolchain}/${target}/${app_name}
      fullsdk-apps-${app_name}-${toolchain}-${target}-${platform}-${l2_stack}-${CI_COMMIT_TAG}-${CI_JOB_ID}
  artifacts:
    paths:
      - fullsdk-apps-*

# upload
.upload_binary_template: &upload_binary_definition
  stage: upload
  image: curlimages/curl:latest
  only:
    - tags
  script:
    - find
    - cd build/app/
    - find
    - "curl --header \"JOB-TOKEN: ${CI_JOB_TOKEN}\" \
      --upload-file \"../../${PACKAGE_NAME}\" \
      \"${PACKAGE_REGISTRY_URL}/${PACKAGE_NAME}\""

upload_binaries:
  stage: upload
  image: curlimages/curl:latest
  only:
    - tags
  script:
    - ls
    - >
      for f in $(ls fullsdk-apps-*);
      do
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file "${f}"
      "${PACKAGE_REGISTRY_URL}/${f}";
      done

upload_dependencies:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: '$CI_COMMIT_TAG && $CI_PIPELINE_SOURCE == "web"'
      allow_failure: true
    - if: "$CI_COMMIT_TAG"
  script:
    - ls deps/
    - >
      for f in $(ls deps/);
      do curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file "deps/${f}"
      "${PACKAGE_REGISTRY_URL}/dependency-${f}";
      done

# release
release:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  rules:
    - if: '$CI_COMMIT_TAG && $CI_PIPELINE_SOURCE != "web"'
  script:
    - export SAMPLE_PACKAGE_NAME=fullsdk
    - export SAMPLE_APP_NAME=fullsdk-apps
    - >
      release-cli create
      --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG

# release additions
add_binaries_to_release:
  image: curlimages/curl:latest
  stage: release_additions
  rules:
    - if: "$CI_COMMIT_TAG"
  script:
    - set -x
    - >
      for f in $(ls fullsdk-apps-*);
      do
      curl
      --header "PRIVATE-TOKEN: ${RELEASE_EDITOR_TOKEN}"
      --header "Content-Type: application/json"
      --request POST
      --data "{
      \"name\": \"${f}\",
      \"url\":\"${PACKAGE_REGISTRY_URL}/${f}\"
      }"
      "${BASE_RELEASE_URL}/assets/links";
      done

add_dependencies_to_release:
  image: curlimages/curl:latest
  stage: release_additions
  rules:
    - if: "$CI_COMMIT_TAG"
  script:
    - set -x
    - >
      for f in $(ls deps/);
      do
      curl
      --header "PRIVATE-TOKEN: ${RELEASE_EDITOR_TOKEN}"
      --header "Content-Type: application/json"
      --request POST
      --data "{
      \"name\": \"dependency-${f}\",
      \"url\":\"${PACKAGE_REGISTRY_URL}/dependency-${f}\"
      }"
      "${BASE_RELEASE_URL}/assets/links";
      done

# We want to keep build logs in a release
# However such artifacts wille eventually expire, so we query the list
# of logs for the build stage, and post it to the package registry,
# then reference them in the release
.add_logs_to_release_template: &add_log_to_release_definition
  image: alpine:3.13.1
  script:
    - set -x
    - apk add curl jq
    - export PACKAGE_NAME=acklio-sdk-apps-$(cat sdk_version_string.txt)
    - >
      export JOB_IDS=$(curl
      --header "PRIVATE-TOKEN: ${RELEASE_EDITOR_TOKEN}"
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs?per_page=100" |
      jq '(.[] | select((.stage == "build") or (.stage == "delivery"))).id' )
    - >
      for ID in ${JOB_IDS};
      do
      curl
      --header "PRIVATE-TOKEN: ${RELEASE_EDITOR_TOKEN}"
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${ID}/trace"
      > ${ID}.log;
      curl
      --header "JOB-TOKEN: ${CI_JOB_TOKEN}"
      --upload-file "${ID}.log"
      "${PACKAGE_REGISTRY_URL}/build_log_${PACKAGE_NAME}_${ID}.log";
      curl
      --header "PRIVATE-TOKEN: ${RELEASE_EDITOR_TOKEN}"
      --header "Content-Type: application/json"
      --request POST
      --data "{
      \"name\": \"build_log_${PACKAGE_NAME}_${ID}.log\",
      \"url\":\"${PACKAGE_REGISTRY_URL}/build_log_${PACKAGE_NAME}_${ID}.log\"
      }"
      "${BASE_RELEASE_URL}/assets/links"
      ; done

add_logs_to_release_on_tag:
  <<: *add_log_to_release_definition
  stage: release_additions
  rules:
    - if: '$CI_COMMIT_TAG && $CI_PIPELINE_SOURCE != "web"'

add_logs_to_release_on_web:
  <<: *add_log_to_release_definition
  stage: release_additions
  rules:
    - if: '$CI_PIPELINE_SOURCE == "web" && $CI_COMMIT_TAG'
