# Open-source Release July 2024 - Notes and Contributors

Code released in open-source under the MIT licence by Actility in 2024 under the joint lab with IMT Atlantique - **lab.SCHC**. 

## Lab.SCHC Members and Contributors

- Javier Alejandro Fernandez 
- Quy Nguyen Hoang
- Olivier Hersent
- Laurent Toutain
- Alexander Pelov

## Embedded Software Developers

### Lead developers

- Flavien Moullec
- Thibaut Artis
- Aydogan Ersoz
- Pascal Bodin
- Jérôme Elias

### Additional developers (alphabetical order)

- Arthur Josso
- Arthur Jourdan
- Borislav Simov
- Dancho Iliev
- François Lelay
- Hadi Bereksi
- Kylian Balan
- Mateo Agudelo Jaramillo
- Milen Stoychev
- Nathan Lecorchet
- Simon Racaud
- Tanguy Kerdoncuff

## Global Solution Contributors - embedded+core+supporting teams (alphabetical order)

        Alexander Pelov           https://www.linkedin.com/in/pelov/
        Ana Minaburo              https://www.linkedin.com/in/ana-minaburo-377121213/
        Andrew Hackett            https://www.linkedin.com/in/andrew-hackett-a64202/
        Arnaud Alies              https://www.linkedin.com/in/arnaud-ali%C3%A8s-2b6640110/
        Arthur Josso
        Arthur Jourdan            https://www.linkedin.com/in/arthurjourdan/
        Arunprabhu Kandasamy      https://www.linkedin.com/in/arunprabhu-k-b2b1b420/
        Aydogan Ersoz             https://www.linkedin.com/in/aydoganersoz/
        Bich-Thuy Gueno           https://www.linkedin.com/in/bich-thuy-gueno-49878b38/
        Boris Simov               https://www.linkedin.com/in/boris-simov-7850ba2/
        Dancho Iliev              https://www.linkedin.com/in/dancho-iliev/
        Elisabeth Lopez           https://www.linkedin.com/in/elisabeth-lopez-rh/
        Estefania Huryta          https://www.linkedin.com/in/est%C3%A9fania-huryta/
        Flavien Auffret           https://www.linkedin.com/in/flavien-auffret/
        Flavien Moullec           https://www.linkedin.com/in/flavien-moullec/
        Florence Venisse          https://www.linkedin.com/in/florencevenisse/
        François Lelay            https://www.linkedin.com/in/francois-lelay/
        Gabriela G.               https://www.linkedin.com/in/gabriela-g-604a4a9a/
        Gwen Maudet               https://www.linkedin.com/in/gwen-maudet-1a1490171/
        Hadi Bereksi              https://www.linkedin.com/in/hadibereksi/
        Hussein Al Haj Hassan     https://www.linkedin.com/in/hussein-al-haj-hassan/
        Ivaylo Petrov             https://www.linkedin.com/in/ivaylo-petrov-a0b17241/
        Javier Fernandez          https://www.linkedin.com/in/javier-fddz/
        Jérôme Elias              https://www.linkedin.com/in/j%C3%A9r%C3%B4me-e-03740189/
        Julien Le Sech            https://www.linkedin.com/in/julien-le-sech-1666ab86/
        Julien Lecoeuvre          https://www.linkedin.com/in/lecoeuvre-julien-5786351/
        Kiko Dagnogo              https://www.linkedin.com/in/kiko-dagnogo/
        Kylian Balan              https://www.linkedin.com/in/kylian-balan/
        Laurent Toutain           https://www.linkedin.com/in/laurent-toutain-56986212/
        Lore-Maëlle Bourdais      https://www.linkedin.com/in/lore-ma%C3%ABlle-bourdais/
        Marianne Laurent          https://www.linkedin.com/in/mlaurent/
        Mateo Agudelo Jaramillo   https://www.linkedin.com/in/mateoagudelojaramillo/
        Matthieu Brient           https://www.linkedin.com/in/matthieubrient/
        Maxence Moutoussamy       https://www.linkedin.com/in/maxence-moutoussamy-266444119/
        Mihail Uzunov             https://www.linkedin.com/in/mihail-uzunov/
        Mikael Morvan             https://www.linkedin.com/in/mikaelmorvan/
        Milen Stoychev            https://www.linkedin.com/in/milenstoychev/
        Nathan Lecorchet          https://www.linkedin.com/in/nathan-lecorchet/
        Owen Morvan               https://www.linkedin.com/in/owen-morvan/
        Pascal Bodin              https://www.linkedin.com/in/pascalbodin/
        Philippe Loctaux          https://www.linkedin.com/in/philippeloctaux/
        Quy Nguyen Hoang          https://www.linkedin.com/in/quy-nguyen-hoang/
        Simon Racaud              https://www.linkedin.com/in/simon-racaud/
        Sony Nolais               https://www.linkedin.com/in/snolais/
        Svilen Spasov             https://www.linkedin.com/in/svilenspasov/
        Tanguy Kerdoncuff         https://www.linkedin.com/in/tanguy-kerdoncuff-3a175463/
        Tanguy Ropitault          https://www.linkedin.com/in/tanguy-ropitault-7553265a/
        Thibaut Artis             https://www.linkedin.com/in/tibautartis/

