#!/bin/bash

#This script is used to modify se-identity.h
BUILD_DIR=$1
DEVEUI=$2
APPEUI=$3
APPKEY=$4
GENAPPKEY=$5

group_two_digit_DEVEUI="${DEVEUI:0:2}"
group_two_digit_APPEUI="${APPEUI:0:2}"
group_two_digit_APPKEY="${APPKEY:0:2}"
group_two_digit_GENAPPKEY="${GENAPPKEY:0:2}"
declare -a DEV_EUI
declare -a APP_EUI
declare -a APP_KEY
declare -a GEN_APP_KEY

#Fill DEV_EUI
for i in {0..18..2}
do
 group_two_digits_DEVEUI="${DEVEUI:i:2}"
 DEV_EUI[i]=$group_two_digits_DEVEUI
done

#Fill APP_EUI
for i in {0..18..2}
do
 group_two_digits_APPEUI="${APPEUI:i:2}"
 APP_EUI[i]=$group_two_digits_APPEUI
done

#Fill APP_KEY
for i in {0..36..2}
do
 group_two_digits_APPKEY="${APPKEY:i:2}"
 APP_KEY[i]=$group_two_digits_APPKEY
done

#Fill GEN_APP_KEY
for i in {0..36..2}
do
 group_two_digits_GENAPPKEY="${GENAPPKEY:i:2}"
 GEN_APP_KEY[i]=$group_two_digits_GENAPPKEY
done

Search_for_DEVEUI="#define LORAWAN_DEVICE_EUI"
Replace_DEVEUI="{ 0x${DEV_EUI[0]}, 0x${DEV_EUI[2]}, 0x${DEV_EUI[4]}, 0x${DEV_EUI[6]}, 0x${DEV_EUI[8]}, 0x${DEV_EUI[10]}, 0x${DEV_EUI[12]}, 0x${DEV_EUI[14]} }"
sed -i 's:^'"$Search_for_DEVEUI"'.*$:'"$Search_for_DEVEUI"'\t\t\t\t\t'"$Replace_DEVEUI"':g' $BUILD_DIR/se-identity.h

Search_for_APPEUI="#define LORAWAN_JOIN_EUI"
Replace_APPEUI="{ 0x${APP_EUI[0]}, 0x${APP_EUI[2]}, 0x${APP_EUI[4]}, 0x${APP_EUI[6]}, 0x${APP_EUI[8]}, 0x${APP_EUI[10]}, 0x${APP_EUI[12]}, 0x${APP_EUI[14]} }"
sed -i 's:^'"$Search_for_APPEUI"'.*$:'"$Search_for_APPEUI"'\t\t\t\t\t'"$Replace_APPEUI"':g' $BUILD_DIR/se-identity.h

Search_for_APPKEY="#define LORAWAN_APP_KEY"
Replace_APPKEY1="{ 0x${APP_KEY[0]}, 0x${APP_KEY[2]}, 0x${APP_KEY[4]}, 0x${APP_KEY[6]}, 0x${APP_KEY[8]}, 0x${APP_KEY[10]}, 0x${APP_KEY[12]}, 0x${APP_KEY[14]}"
Replace_APPKEY2="0x${APP_KEY[16]}, 0x${APP_KEY[18]}, 0x${APP_KEY[20]}, 0x${APP_KEY[22]}, 0x${APP_KEY[24]}, 0x${APP_KEY[26]}, 0x${APP_KEY[28]}, 0x${APP_KEY[30]} }"
sed -i 's:^'"$Search_for_APPKEY"'.*$:'"$Search_for_APPKEY"'\t\t\t\t\t\t'"$Replace_APPKEY1, $Replace_APPKEY2"':g' $BUILD_DIR/se-identity.h

Search_for_GENAPPKEY="#define LORAWAN_GEN_APP_KEY"
Replace_GENAPPKEY1="{ 0x${GEN_APP_KEY[0]}, 0x${GEN_APP_KEY[2]}, 0x${GEN_APP_KEY[4]}, 0x${GEN_APP_KEY[6]}, 0x${GEN_APP_KEY[8]}, 0x${GEN_APP_KEY[10]}, 0x${GEN_APP_KEY[12]}, 0x${GEN_APP_KEY[14]}"
Replace_GENAPPKEY2="0x${GEN_APP_KEY[16]}, 0x${GEN_APP_KEY[18]}, 0x${GEN_APP_KEY[20]}, 0x${GEN_APP_KEY[22]}, 0x${GEN_APP_KEY[24]}, 0x${GEN_APP_KEY[26]}, 0x${GEN_APP_KEY[28]}, 0x${GEN_APP_KEY[30]} }"
sed -i 's:^'"$Search_for_GENAPPKEY"'.*$:'"$Search_for_GENAPPKEY"'\t\t\t\t\t'"$Replace_GENAPPKEY1, $Replace_GENAPPKEY2"':g' $BUILD_DIR/se-identity.h

