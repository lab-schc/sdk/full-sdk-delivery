/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Jerome Elias - jerome.elias@ackl.io
 *
 * Semtech L2A layer interface.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "Commissioning.h"
#include "LmHandler.h"
#include "LmhpCompliance.h"
#include "RegionCommon.h"
#include "fullsdkl2.h"
#include "fullsdkl2a.h"
#ifdef SCHC_CERTIFICATION_ENABLED
#include "fullsdkl2acert.h"
#endif
#include "mac_strings.h"
#include "platform.h"
#include "timer.h"

/* Private definitions --------------------------------------------------------*/

#define ACTIVE_REGION LORAMAC_REGION_EU868
#define LORAWAN_DEFAULT_CLASS CLASS_C
#define LORAWAN_ADR_STATE LORAMAC_HANDLER_ADR_OFF
#define LORAWAN_DEFAULT_DATA_RATE DR_3
#define LORAWAN_DEFAULT_ACTIVATION_TYPE ACTIVATION_TYPE_OTAA
#define LORAWAN_APP_DATA_BUFFER_MAX_SIZE 242
#define LORAWAN_DUTYCYCLE_ON true

// LoRaWAN ruleID byte size.
#define LORAWAN_RULEID_BYTE_SIZE 1

#ifndef COMPLIANCE_PORT
#define COMPLIANCE_PORT 224
#endif

/* Private variables ---------------------------------------------------------*/

#ifndef TRANSMISSION_RESULT_TIMER_VALUE
#define TRANSMISSION_RESULT_TIMER_VALUE 10000 // 10s
#endif

uint8_t dev_eui[] = LORAWAN_DEVICE_EUI;
uint8_t join_eui[] = LORAWAN_JOIN_EUI;
uint8_t app_key[] = LORAWAN_APP_KEY;
bool redirect_next_transmission_result = false;
void (*transmission_result_redirection)(bool has_error);

#ifdef SCHC_CERTIFICATION_ENABLED
// SCHC Certification payload RX data
static uint8_t schc_cert_rx_data[SCHC_CERT_RX_MAX_LENGTH] = {0};
// Last Compliance data transmitted
static bool schc_cert_tx_result = false;
#endif

/*!
 * User application data for compliance data
 */
static uint8_t compliance_data_buffer[LORAWAN_APP_DATA_BUFFER_MAX_SIZE];

typedef struct
{
  uint8_t joined : 1;
  uint8_t mac_process_required : 1;
  uint8_t tx_result : 1;
  uint8_t retransmission : 1;
  uint8_t transmission_result_timeout : 1;
  uint8_t tx_periodicity_changed : 1;
} lora_events_t;

typedef struct
{
  bool joined;
  bool busy;
  bool is_cert_fport_on;
  l2a_callbacks_t callbacks;
#ifdef SCHC_CERTIFICATION_ENABLED
  l2a_cert_callbacks_t cert_callbacks;
#endif
  uint8_t *receive_buffer;
  uint16_t receive_buffer_size;
  uint32_t next_tx_delay;
  uint32_t polling_periodicity;
  lora_events_t lora_events;
  l2_app_data_t tx_data;
  l2a_status_t last_tx_status;
  TimerEvent_t retransmission_timer;
  TimerEvent_t transmission_result_timer;
  uint8_t device_iid[8];
  int16_t snr;
  int16_t rssi;
} l2a_ctx_t;

#ifdef SCHC_CERTIFICATION_ENABLED
l2a_technology_t l2a_technology = L2A_LORA;
#else
l2a_technology_t l2a_technology = L2A_DEFAULT;
#endif

/* Private functions  ---------------------------------------------------------*/
static void onMacProcessNotify(void);
static void onNvmDataChange(LmHandlerNvmContextStates_t state, uint16_t size);
static void onNetworkParametersChange(CommissioningParams_t *params);
static void onMacMcpsRequest(LoRaMacStatus_t status, McpsReq_t *mcpsReq,
                             TimerTime_t nextTxIn);
static void onMacMlmeRequest(LoRaMacStatus_t status, MlmeReq_t *mlmeReq,
                             TimerTime_t nextTxIn);
static void onJoinRequest(LmHandlerJoinParams_t *params);
static void onTxData(LmHandlerTxParams_t *params);
static void onRxData(LmHandlerAppData_t *appData,
                     LmHandlerRxParams_t *params);
static void onClassChange(DeviceClass_t deviceClass);
static void onBeaconStatusChange(LoRaMacHandlerBeaconParams_t *params);
#ifdef SCHC_CERTIFICATION_ENABLED
static void onSCHCCertificationTxData(LmHandlerTxParams_t *params);
static void onSCHCCertificationRxData(LmHandlerAppData_t *appData,
                                      LmHandlerRxParams_t *params);
#endif
static void onTxPeriodicityChanged(uint32_t periodicity);
static void onTxFrameCtrlChanged(LmHandlerMsgTypes_t isTxConfirmed);
#if (LMH_SYS_TIME_UPDATE_NEW_API == 1)
static void onSysTimeUpdate(bool isSynchronized, int32_t timeCorrection);
#else
static void onSysTimeUpdate(void);
#endif
static void l2a_action_tx_status(l2a_status_t status);
static void retransmission_timer_event(void *context);
static void transmission_result_event(void *context);

// LoRaWAN callbacks.
static LmHandlerCallbacks_t LmHandlerCallbacks =
    {
        .GetBatteryLevel = NULL,
        .GetTemperature = NULL,
        .GetRandomSeed = NULL,
        .OnMacProcess = onMacProcessNotify,
        .OnJoinRequest = onJoinRequest,
        .OnTxData = onTxData,
        .OnRxData = onRxData,
        .OnClassChange = onClassChange,
        .OnNetworkParametersChange = onNetworkParametersChange,
        .OnNvmDataChange = onNvmDataChange,
        .OnMacMcpsRequest = onMacMcpsRequest,
        .OnMacMlmeRequest = onMacMlmeRequest,
        .OnBeaconStatusChange = onBeaconStatusChange,
        .OnSysTimeUpdate = onSysTimeUpdate,
};

// LoRaWAN parameters.
static LmHandlerParams_t LmHandlerParams =
    {
        .Region = ACTIVE_REGION,
        .AdrEnable = LORAWAN_ADR_STATE,
        .TxDatarate = LORAWAN_DEFAULT_DATA_RATE,
        .IsTxConfirmed = LORAWAN_DEFAULT_CONFIRMED_MSG_STATE,
        .DutyCycleEnabled = LORAWAN_DUTYCYCLE_ON,
        .DataBuffer = compliance_data_buffer,
        .DataBufferMaxSize = LORAWAN_APP_DATA_BUFFER_MAX_SIZE,
        .PingSlotPeriodicity = REGION_COMMON_DEFAULT_PING_SLOT_PERIODICITY,
        .PublicNetworkEnable = LORAWAN_PUBLIC_NETWORK};

// LoRaWAN compliance parameters.
static LmhpComplianceParams_t LmhpComplianceParams =
    {
        .FwVersion.Value = 0x01000000,
        .OnTxPeriodicityChanged = onTxPeriodicityChanged,
        .OnTxFrameCtrlChanged = onTxFrameCtrlChanged,
#ifdef SCHC_CERTIFICATION_ENABLED
        .OnSCHCCertificationTxData = onSCHCCertificationTxData,
        .OnSCHCCertificationRxData = onSCHCCertificationRxData
#endif
};

// L2A context structure.
static l2a_ctx_t l2a_ctx;

l2a_status_t l2a_initialize(const l2a_callbacks_t *callbacks,
                            uint8_t *receive_buffer,
                            uint16_t receive_buffer_size)
{
  memset(&l2a_ctx, 0, sizeof(l2a_ctx));

  if (callbacks == NULL || callbacks->processing_required == NULL ||
      callbacks->transmission_result == NULL ||
      callbacks->data_received == NULL ||
      callbacks->connectivity_available == NULL)
  {
    return L2A_REQ_CALLBACK_ERR;
  }

  TimerInit(&l2a_ctx.retransmission_timer, retransmission_timer_event);
  TimerInit(&l2a_ctx.transmission_result_timer, transmission_result_event);

  l2a_ctx.callbacks = *callbacks;
  l2a_ctx.receive_buffer = receive_buffer;
  l2a_ctx.receive_buffer_size = receive_buffer_size;
  l2a_ctx.last_tx_status = L2A_SUCCESS;

  l2_region_id_t id;
  if (l2_get_active_region(&id) != L2_SUCCESS)
  {
    return L2A_L2_ERROR;
  }

  if (LmHandlerInit(&LmHandlerCallbacks, &LmHandlerParams) !=
      LORAMAC_HANDLER_SUCCESS)
  {
    return L2A_L2_ERROR;
  }

  // The LoRa-Alliance Compliance protocol package should always be
  // initialized and activated.
  LmHandlerPackageRegister(PACKAGE_ID_COMPLIANCE, &LmhpComplianceParams);

  // OTAA-Activation
  l2_set_network_activation((l2_network_activation_type_t)LORAWAN_DEFAULT_ACTIVATION_TYPE);

  // Overwrite infos
  l2_set_dev_eui(dev_eui);
  l2_set_join_eui(join_eui);
  l2_set_app_key(app_key);
  l2_set_class('A');
  l2_set_adr(false);

  // Force join request
  LmHandlerJoin();

  // Get duty cycle
  l2a_ctx.next_tx_delay = LmHandlerGetDutyCycleWaitTime();

  PRINT_DBG("l2a> join request\n");
  PRINT_DBG("l2a> duty cycle wait time: %lu ms\n", l2a_ctx.next_tx_delay);

  return L2A_SUCCESS;
}

static l2a_status_t l2_try_send(void)
{
  l2a_ctx.busy = true;

  if (l2_send(l2a_ctx.tx_data, l2_get_default_datarate(),
              &l2a_ctx.next_tx_delay) != L2_SUCCESS)
  {
    // The packet cannot be transmitted immediately. Wait next Tx Delay slot for
    // packet retransmission.
    TimerSetValue(&l2a_ctx.retransmission_timer, l2a_ctx.next_tx_delay + 1);
    TimerStart(&l2a_ctx.retransmission_timer);

    PRINT_DBG("l2a> uplink packet retransmission will be in %lu ms\n",
              l2a_ctx.next_tx_delay);
    return L2A_SUCCESS;
  }

  // This timer is used as a workaround to a case where the semtech stack does
  // not transmit the transmission result event
  TimerSetValue(&l2a_ctx.transmission_result_timer,
                TRANSMISSION_RESULT_TIMER_VALUE);
  TimerStart(&l2a_ctx.transmission_result_timer);

  PRINT_DBG("l2a> uplink transmission request\n");
  PRINT_DBG("l2a> duty cycle wait time: %lu ms\n", l2a_ctx.next_tx_delay);

  return L2A_SUCCESS;
}
l2a_status_t l2a_send_data(const uint8_t *buffer, uint16_t size)
{
  if (size < LORAWAN_RULEID_BYTE_SIZE)
  {
    return L2A_INTERNAL_ERR;
  }

  if (!l2a_ctx.joined)
  {
    l2a_action_tx_status(L2A_CONNECT_ERR);
    return L2A_SUCCESS;
  }

  if (size > LORAWAN_APP_DATA_BUFFER_MAX_SIZE)
  {
    l2a_action_tx_status(L2A_L2_ERROR);
    return L2A_SUCCESS;
  }

#ifdef SCHC_CERTIFICATION_ENABLED
  if (l2a_ctx.busy || LmHandlerIsBusy())
  {
    l2a_action_tx_status(L2A_BUSY_ERR);
    return L2A_SUCCESS;
  }
#else
  if (l2a_ctx.busy)
  {
    l2a_action_tx_status(L2A_BUSY_ERR);
    return L2A_SUCCESS;
  }
#endif

  // Update tx_data struct to save tx context
  l2a_ctx.tx_data.port = buffer[0];
  l2a_ctx.tx_data.size = size - LORAWAN_RULEID_BYTE_SIZE;
  l2a_ctx.tx_data.buffer = (uint8_t *)(&buffer[LORAWAN_RULEID_BYTE_SIZE]);
  l2a_ctx.tx_data.type = l2_get_message_type();

  return l2_try_send();
}

l2a_technology_t l2a_get_technology(void)
{
  return l2a_technology;
}

uint16_t l2a_get_mtu(void)
{
  uint16_t mtu = l2_get_mtu();
  PRINT_DBG("l2a> mtu: %d\n", mtu);
  return mtu;
}

uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  (void)data_size;
  PRINT_DBG("l2a> next tx delay: %lu ms\n", l2a_ctx.next_tx_delay);
  return l2a_ctx.next_tx_delay;
}

l2a_status_t l2a_process(void)
{
  if (l2a_ctx.lora_events.mac_process_required)
  {
    l2a_ctx.lora_events.mac_process_required = false;
    LoRaMacProcess();
  }

  if (l2a_ctx.lora_events.joined)
  {
    l2a_ctx.lora_events.joined = false;
    l2a_ctx.joined = true;
    l2a_ctx.callbacks.connectivity_available();
  }

  if (l2a_ctx.lora_events.tx_result)
  {
    TimerStop(&l2a_ctx.transmission_result_timer);
    l2a_ctx.lora_events.tx_result = false;
#ifdef SCHC_CERTIFICATION_ENABLED
    if (schc_cert_tx_result)
    {
      schc_cert_tx_result = false;
      l2a_ctx.cert_callbacks.cert_transmission_result(l2a_ctx.last_tx_status, 0);
    }
    else
    {
#endif
      l2a_ctx.callbacks.transmission_result(l2a_ctx.last_tx_status, 0);
#ifdef SCHC_CERTIFICATION_ENABLED
    }
#endif
    l2a_ctx.last_tx_status = L2A_SUCCESS;
  }

  if (l2a_ctx.lora_events.retransmission)
  {
    l2a_ctx.lora_events.retransmission = false;
    l2_try_send();
  }

  if (l2a_ctx.lora_events.transmission_result_timeout)
  {
    l2a_ctx.lora_events.transmission_result_timeout = false;
    PRINT_DBG("l2a> transmission result event not received from the stack\n");
    l2a_action_tx_status(L2A_L2_ERROR);
    l2a_ctx.busy = false;
  }

  if (l2a_ctx.lora_events.tx_periodicity_changed)
  {
    l2a_ctx.lora_events.tx_periodicity_changed = false;
#ifdef SCHC_CERTIFICATION_ENABLED
    l2a_ctx.cert_callbacks.cert_polling_msg_periodicity_changed(l2a_ctx.polling_periodicity);
#endif
  }

  return L2A_SUCCESS;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  *dev_iid = l2a_ctx.device_iid;

  if (l2_compute_iid(l2a_ctx.device_iid) != L2_SUCCESS)
  {
    return false;
  }
  return true;
}

void onJoinRequest(LmHandlerJoinParams_t *params)
{
  if (params->Status == LORAMAC_HANDLER_ERROR)
  {
    LmHandlerJoin();
  }
  else
  {
    // Set device class
    LmHandlerRequestClass(LORAWAN_DEFAULT_CLASS);
    l2a_ctx.lora_events.joined = true;
    l2a_ctx.callbacks.processing_required();
  }
}

void onTxData(LmHandlerTxParams_t *params)
{
  uint16_t channels_mask[5]; // either 1 or 5 bytes depending on the region
  uint8_t channels_mask_size;
  char class;

  if (!l2a_ctx.busy)
  {
    PRINT_DBG(
        "l2a> tx result happened after transmission result timer timeout\n");
    return;
  }

  if (!params->IsMcpsConfirm)
  {
    PRINT_DBG(
        "l2a> not a transmission result confirm\n");
    return;
  }

  if (l2a_ctx.tx_data.port == COMPLIANCE_PORT)
  {
    PRINT_DBG("l2a> compliance request event, ignore tx event\n");
    return;
  }

  l2a_ctx.busy = false;

  if (redirect_next_transmission_result &&
      transmission_result_redirection != NULL)
  {
    redirect_next_transmission_result = false;
    transmission_result_redirection((bool)params->Status);
    return;
  }

  if (params->Status != LORAMAC_EVENT_INFO_STATUS_OK)
  {
    PRINT_DBG("mac> mcps confirm status not ok: %s\n",
              lora_get_event_info_status_string(params->Status));
    l2a_action_tx_status(L2A_L2_ERROR);
    return;
  }

  PRINT_DBG("l2a> uplink packet %lu transmitted\n", params->UplinkCounter);
  PRINT_DBG("l2a> port: %d, ", l2a_ctx.tx_data.port);
  PRINT_DBG("dr: %d, ", params->Datarate);
  l2_get_class(&class);
  PRINT_DBG("class: %c, ", class);
  PRINT_DBG("freq: %lu, ", l2_get_uplink_frequency(params->Channel));
  PRINT_DBG("tx pwr: %d, ", params->TxPower);
  if (l2a_ctx.tx_data.type == L2_MSG_TYPE_CONFIRMED)
  {
    PRINT_DBG("confirmed, ");
    PRINT_DBG("ack: %d, ", params->AckReceived);
  }
  else
  {
    PRINT_DBG("unconfirmed, ");
  }
  l2_get_channels_mask(channels_mask, &channels_mask_size);
  PRINT_DBG("ch mask: ");
  for (uint8_t i = 0; i < channels_mask_size; i++)
  {
    PRINT_DBG("%04X", channels_mask[i]);
  }
  PRINT_DBG(", data len: %d, ", l2a_ctx.tx_data.size);
  PRINT_DBG("data:\n");
  PRINT_HEX_BUF_DBG(l2a_ctx.tx_data.buffer, l2a_ctx.tx_data.size);

  if (l2a_ctx.joined)
  {
    l2a_action_tx_status(L2A_SUCCESS);
  }
}

void onRxData(LmHandlerAppData_t *appData, LmHandlerRxParams_t *params)
{
  if (params->Status != LORAMAC_EVENT_INFO_STATUS_OK)
  {
    PRINT_DBG("mac> mcps indication status not ok: %s\n",
              lora_get_event_info_status_string(params->Status));
    return;
  }

  if (!params->IsMcpsIndication)
  {
    PRINT_DBG(
        "l2a> not an MCPS indication\n");
    return;
  }

  if (appData->Port == COMPLIANCE_PORT)
  {
    PRINT_DBG("l2a> compliance request event, ignore rx event\n");
    return;
  }

  if ((appData->BufferSize + LORAWAN_RULEID_BYTE_SIZE) >
      l2a_ctx.receive_buffer_size)
  {
    PRINT_DBG("l2a> downlink receive error\n");
    l2a_ctx.callbacks.data_received(appData->BufferSize + LORAWAN_RULEID_BYTE_SIZE, L2A_BUFFER_ERR);
    return;
  }

  PRINT_DBG("l2a> downlink packet %lu received\n", params->DownlinkCounter);
  PRINT_DBG("l2a> port: %d, ", appData->Port);
  PRINT_DBG("dr: %d, ", params->Datarate);
  PRINT_DBG("win: %s, ", lora_get_slot_string(params->RxSlot));
  PRINT_DBG("rssi: %d, ", params->Rssi);
  PRINT_DBG("snr: %d, ", params->Snr);
  PRINT_DBG("data len: %d, ", appData->BufferSize);
  PRINT_DBG("data:\n");
  PRINT_HEX_BUF_DBG(appData->Buffer, appData->BufferSize);

  l2a_ctx.snr = params->Snr;
  l2a_ctx.rssi = params->Rssi;
  // Concatenate port with the buffer
  l2a_ctx.receive_buffer[0] = appData->Port;
  memcpy(&l2a_ctx.receive_buffer[1], appData->Buffer, appData->BufferSize);
  l2a_ctx.callbacks.data_received(appData->BufferSize + LORAWAN_RULEID_BYTE_SIZE, L2A_SUCCESS);
}

static void onClassChange(DeviceClass_t class)
{
  char class_c;
  switch (class)
  {
  case CLASS_A:
    class_c = 'A';
    break;
  case CLASS_B:
    class_c = 'B';
    break;
  case CLASS_C:
    class_c = 'C';
    break;
  default:
    class_c = 'B';
  }
  PRINT_DBG("l2a> LoRa class switched to %c\n\r", class_c);
  (void)class_c;
}

#ifdef SCHC_CERTIFICATION_ENABLED
l2a_status_t l2a_cert_initialize(const l2a_cert_callbacks_t *cert_callbacks)
{
  if (cert_callbacks == NULL)
  {
    return L2A_REQ_CALLBACK_ERR;
  }

  if (cert_callbacks->cert_transmission_result == NULL ||
      cert_callbacks->cert_data_received == NULL ||
      cert_callbacks->cert_polling_msg_periodicity_changed == NULL)
  {
    return L2A_REQ_CALLBACK_ERR;
  }

  // Set SCHC Certification callbacks
  l2a_ctx.cert_callbacks = *cert_callbacks;

  return L2A_SUCCESS;
}

static void onSCHCCertificationTxData(LmHandlerTxParams_t *params)
{
  uint16_t channels_mask[5]; // either 1 or 5 bytes depending on the region
  uint8_t channels_mask_size;
  char class;

  if (!l2a_ctx.busy)
  {
    PRINT_DBG(
        "l2a> tx result happened after transmission result timer timeout\n");
    return;
  }

  if (l2a_ctx.tx_data.port != COMPLIANCE_PORT)
  {
    PRINT_DBG("l2a> not a compliance port, ignore tx event\n");
    return;
  }

  if (!params->IsMcpsConfirm)
  {
    PRINT_DBG(
        "l2a> not a transmission result confirm\n");
    return;
  }

  l2a_ctx.busy = false;

  if (redirect_next_transmission_result &&
      transmission_result_redirection != NULL)
  {
    redirect_next_transmission_result = false;
    transmission_result_redirection((bool)params->Status);
    return;
  }

  // At this stage, it is SCHC Certification transmission result
  schc_cert_tx_result = true;

  if (params->Status != LORAMAC_EVENT_INFO_STATUS_OK)
  {
    PRINT_DBG("mac> mcps confirm status not ok: %s\n",
              lora_get_event_info_status_string(params->Status));
    l2a_action_tx_status(L2A_L2_ERROR);
    return;
  }

  PRINT_DBG("l2a> SCHC certification uplink packet %d transmitted\n",
            params->UplinkCounter);
  PRINT_DBG("dr: %d, ", params->Datarate);
  l2_get_class(&class);
  PRINT_DBG("class: %c, ", class);
  PRINT_DBG("freq: %lu, ", l2_get_uplink_frequency(params->Channel));
  PRINT_DBG("tx pwr: %d, ", params->TxPower);
  l2_get_channels_mask(channels_mask, &channels_mask_size);
  PRINT_DBG("ch mask: ");
  for (uint8_t i = 0; i < channels_mask_size; i++)
  {
    PRINT_DBG("%04X", channels_mask[i]);
  }

  if (l2a_ctx.joined)
  {
    l2a_action_tx_status(L2A_SUCCESS);
  }
}

static void onSCHCCertificationRxData(LmHandlerAppData_t *appData,
                                      LmHandlerRxParams_t *params)
{
  if (params->Status != LORAMAC_EVENT_INFO_STATUS_OK)
  {
    PRINT_DBG("mac> mcps indication status not ok: %s\n",
              lora_get_event_info_status_string(params->Status));
    return;
  }

  if (!params->IsMcpsIndication)
  {
    PRINT_DBG(
        "l2a> not an MCPS indication\n");
    return;
  }

  if (appData->BufferSize == 1)
  {
    PRINT_DBG(
        "l2a> SCHC certification request is too short\n");
    return;
  }

  // Ignore first byte of command Id
  if (((appData->BufferSize) - 1) > (uint8_t)sizeof(schc_cert_rx_data))
  {
    PRINT_DBG("l2a> downlink receive error\n");
    l2a_ctx.cert_callbacks.cert_data_received(schc_cert_rx_data, appData->BufferSize, L2A_BUFFER_ERR);
    return;
  }

  PRINT_DBG("l2a> SCHC certification downlink packet %d received\n",
            params->DownlinkCounter);
  PRINT_DBG("l2a> port: %d, ", appData->Port);
  PRINT_DBG("dr: %d, ", params->Datarate);
  PRINT_DBG("win: %s, ", lora_get_slot_string(params->RxSlot));
  PRINT_DBG("rssi: %d, ", params->Rssi);
  PRINT_DBG("snr: %d, ", params->Snr);
  PRINT_DBG("data len: %d, ", appData->BufferSize);
  PRINT_DBG("data:\n");
  PRINT_HEX_BUF_DBG(appData->Buffer, appData->BufferSize);

  // Update SNR and RSSI values anyway
  l2a_ctx.snr = params->Snr;
  l2a_ctx.rssi = params->Rssi;

  // Do not concatenate port with the buffer as usually
  memset(schc_cert_rx_data, 0, sizeof(schc_cert_rx_data));
  // Ignore first byte of command Id
  memcpy(schc_cert_rx_data, &appData->Buffer[1], appData->BufferSize);
  l2a_ctx.cert_callbacks.cert_data_received(schc_cert_rx_data, (appData->BufferSize - 1), L2A_SUCCESS);
}

uint8_t l2a_cert_get_compliance_rule_id(void)
{
  return COMPLIANCE_PORT;
}

bool l2a_cert_get_joined_status(void)
{
  return l2a_ctx.joined;
}

bool l2a_cert_get_is_cert_fport_on(void)
{
  l2_get_is_cert_fport_on(&l2a_ctx.is_cert_fport_on);
  return l2a_ctx.is_cert_fport_on;
}

void l2a_cert_compliance_process(void)
{
  if (!l2a_ctx.busy)
  {
    // Process the LoRaMac events and send compliance acknoledgements
    LmHandlerProcess();
  }
  return;
}

uint8_t l2a_cert_get_schc_compliance_ind_cid(void)
{
  return (uint8_t)COMPLIANCE_SCHC_IND;
}
#endif

static void onTxPeriodicityChanged(uint32_t periodicity)
{
  l2a_ctx.lora_events.tx_periodicity_changed = true;
  l2a_ctx.polling_periodicity = periodicity;
  l2a_ctx.callbacks.processing_required();
}

static void onTxFrameCtrlChanged(LmHandlerMsgTypes_t isTxConfirmed)
{
  l2_set_message_type((l2_msg_type_t)isTxConfirmed);
}

static void l2a_action_tx_status(l2a_status_t status)
{
  l2a_ctx.lora_events.tx_result = true;
  l2a_ctx.last_tx_status = status;
  l2a_ctx.callbacks.processing_required();
}

void onMacProcessNotify(void)
{
  l2a_ctx.lora_events.mac_process_required = true;
  l2a_ctx.callbacks.processing_required();
}

static void retransmission_timer_event(void *context)
{
  (void)context;
  l2a_ctx.lora_events.retransmission = true;
  l2a_ctx.callbacks.processing_required();
}

static void transmission_result_event(void *context)
{
  (void)context;
  l2a_ctx.lora_events.transmission_result_timeout = true;
  l2a_ctx.callbacks.processing_required();
}

static void onNvmDataChange(LmHandlerNvmContextStates_t state, uint16_t size)
{
  (void)state;
  (void)size;
}

static void onNetworkParametersChange(CommissioningParams_t *params)
{
  (void)*params;
}

static void onMacMcpsRequest(LoRaMacStatus_t status, McpsReq_t *mcpsReq,
                             TimerTime_t nextTxIn)
{
  (void)status;
  (void)*mcpsReq;
  (void)nextTxIn;
}

static void onMacMlmeRequest(LoRaMacStatus_t status, MlmeReq_t *mlmeReq,
                             TimerTime_t nextTxIn)
{
  (void)status;
  (void)*mlmeReq;
  (void)nextTxIn;
}

static void onBeaconStatusChange(LoRaMacHandlerBeaconParams_t *params)
{
  (void)*params;
}

#if (LMH_SYS_TIME_UPDATE_NEW_API == 1)
static void onSysTimeUpdate(bool isSynchronized, int32_t timeCorrection)
{
  (void)isSynchronized;
  (void)timeCorrection;
}
#else
static void onSysTimeUpdate(void)
{
}
#endif

int16_t lora_get_snr(void)
{
  return l2a_ctx.snr;
}

int16_t lora_get_rssi(void)
{
  return l2a_ctx.rssi;
}