# Semtech LoRaWAN stack

- Upstream repository: https://github.com/Lora-net/LoRaMac-node

- Current version: 4.6.0

# DevNonce computation for Join Request to LNS

Be careful, in LoRaMac-noce 4.6.0, DevNonce starts from 0 and is incremented by one each join request.
To use random DevNonce, you need to enable `USE_RANDOM_DEV_NONCE` in `LoRaMacCrypto.h` and to declare `SecureElementRandomNumber` in 
`secure-element.h` and to define it in `soft-se.c`. `SoftSeHalGetRandomNumber` function has to be defined as well in `soft-se-hal.c` too. You can rely on LoRaMac-node 4.4.7 library.

## Duty cycle management

As described [here](https://github.com/Lora-net/LoRaMac-node/issues/520), Semtech implements a new way of managing duty cycle while respecting ETSI regulations.

By default, the stack defines 30 minutes of time credit where the device can send burst uplinks that corresponds to 18 seconds of time on air (36 seconds for 1 hour time credit). Once the device consumes this credit, the stack rejects any uplink requests and provides a wait time for the next transmission.

In order to converge to the traditional duty cycle management (x milliseconds time on air then 99x milliseconds wait), an environment variable called `FULLSDK_LORA_DC_TIME_CREDIT` can be set with the time in milliseconds in the compilation command otherwise it's set to 1800000 by default which corresponds to 30 minutes of time credit as described above.

`FULLSDK_LORA_DC_TIME_CREDIT` can't be set to 0 and must be equal to or bigger than 60000 which corresponds to 1 minute of time credit by experience.

Below is an example to set the time credit to 2 minutes:

```sh
FULLSDK_LORA_DC_TIME_CREDIT=120000
```

## Regions

It is possible to change the enabled region(s) (which is REGION_EU868 by default) using the following environment variable: `FULLSDK_LORA_REGIONS`.

### AS923 region and its subregions

In order to set subregions of `REGION_AS923` region, `FULLSDK_LORA_AS923_SUBREGION` environment variable must be set along with `FULLSDK_LORA_REGIONS=REGION_AS923` respecting the following information:

* `FULLSDK_LORA_AS923_SUBREGION=CHANNEL_PLAN_GROUP_AS923_1` for the subregion 1

* `FULLSDK_LORA_AS923_SUBREGION=CHANNEL_PLAN_GROUP_AS923_2` for the subregion 2

* `FULLSDK_LORA_AS923_SUBREGION=CHANNEL_PLAN_GROUP_AS923_3` for the subregion 3

### Enabling multiple regions

In order to set multiple regions, values provided to `FULLSDK_LORA_REGIONS` must be separated by commas (`,`) as below:

```sh
FULLSDK_LORA_REGIONS=REGION_AS923,REGION_EU868,REGION_AU915
```

### Enabling SCHC Certification submodule

```sh
SCHC_CERTIFICATION_ENABLED=1
```
SCHC Certification application is described in FullSDK ReadMe.md. This env variable is used for the L2 layer and works only with `semtech` L2 layer, which used LoRaWAN v1.0.4 implemented in v4.6.0 LoRaMac-node library.

To be able to run SCHC Certification application, LoRaMac-node v4.6.0 must be modified to integrate SCHC commands. Two patches can be found in `/l2/semtech/patch/LmhpCompliance.*` for .h and .c and are automatically launched if `SCHC_CERTIFICATION_ENABLED` env variable is enabled.

SCHC Certification submodule uses two additionnal timers, one for sending polling messages regularly, and a second one to retry sending messages. Make sure to initialize enough timers to not to have an issue during runtime.

### Patching a region

In order to apply a patch to a region, `L2_REGION_PATCH` environment variable must be set respecting the following information:

* `L2_REGION_PATCH=everynet` for Everynet only available for REGION_AU915 region

### Example

Following example enables three different regions: REGION_CN470, subregion 3 of REGION_AS923 and everynet patched REGION_AU915

```sh
FULLSDK_LORA_REGIONS=REGION_AU915,REGION_CN470,REGION_AS923
FULLSDK_LORA_AS923_SUBREGION=CHANNEL_PLAN_GROUP_AS923_3
L2_REGION_PATCH=everynet
```

## Secure element

As the boards we support in `full-sdk-delivery` don't have secure elements, the stack is configured to use the software emulation of it by using `SOFT_SE` definition in the CMake configuration.
