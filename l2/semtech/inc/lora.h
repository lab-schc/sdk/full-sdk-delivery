/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * Semtech L2 Lora binding layer.
 */

#ifndef LORA_H_
#define LORA_H_

#include <stdbool.h>
#include <stdint.h>

#include "LoRaMac.h"
#include "fullsdkl2.h"

int16_t lora_get_snr(void);
int16_t lora_get_rssi(void);

extern uint8_t dev_eui[];
extern uint8_t join_eui[];
extern uint8_t app_key[];
extern bool redirect_next_transmission_result;
extern void (*transmission_result_redirection)(bool has_error);

#endif // LORA_H_