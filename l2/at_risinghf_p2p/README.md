# Layer 2 - AT RisingHF - Point to point

## Overview

This l2 implementation is based on the section ***3.6 Point to Point communication with LoRa***,
page 7 of the document ***'[RHF-AN01571] Ho To Use RisingHF's AT Modem For LoRaWAN Communication - v0.5.pdf'***,
downloadable at the bottom of this page : https://www.risinghf.com/product/detail/6

### Compression

The template used is ipv6udpdynipsuffixudpport, which compress the whole ip packet
except last byte of each ip address and udp port

One device must compress as core (by compiling with SCHC_CORE_COMPRESS=1)
which means that it **reverses** device and application fields in IPv6 addresses and UDP port during **compression**

The other device(s) must compress as device
which means that it **reverses** device and application fields in IPv6 addresses and UDP port
during **decompression**

This allows point to point communication with device identifying (last byte of IP),
while communicating point to point in a star shaped network (one gateway and several devices)

### Fragmentation

The fragmentation API is p2p, which is symmetrical between device and app (the gateway)

The sdk mode (device or app) is defined **automatically** according to the SCHC_CORE_MODE macro \
The sdk mode defines up / downlink ruleId \
With the point to point fragmentation API, one device must be app, the other device \
Doing so, their fragmentation rules will be mirrored

## Build

### Device 1 aka gateway

```sh
cd full-sdk-delivery
make FULLSDK_VERSION=dev APP_NAME=SDKTunApp TOOLCHAIN=gcc-native TARGET=default PLATFORM=linux L2_STACK=at_risinghf_p2p FRAGMENTATION_API=p2p EXTENSION_API=template TEMPLATE_ID=ipv6udpdynsuffixport SCHC_CORE_MODE=1 app
```

### Device 2 aka end device

```sh
cd full-sdk-delivery
make FULLSDK_VERSION=dev APP_NAME=SDKTunApp TOOLCHAIN=gcc-native TARGET=default PLATFORM=linux L2_STACK=at_risinghf_p2p FRAGMENTATION_API=p2p EXTENSION_API=template TEMPLATE_ID=ipv6udpdynsuffixport app
```

## Run

### Device 1 aka gateway

```sh
## create interface and ip address 5454::9 on my device, will communicate with 1212::8
sudo ./examples/SDKTunApp/common/scripts/ipv6_tun_client.sh 5454::9 1212::8
./build/linux/SDKTunApp examples/SDKTunApp/config/my_static_template.json
```

```sh
## In an other terminal
# netcat listens on port 5539, ip 5454::9 and writes on port 5540, ip 1212::8, for udp communication
nc -u -6 -s 5454::9 -p 5539 1212::8 5540
```

```sh
## config:
cat examples/SDKTunApp/config/my_static_config.json
{
  "tun_name": "sdk_tun_client",
  "l2": {
    "vidpid": "0483:5740"
  },
  "template": {
    "params": [
      {
        "index": 0,
        "value": "0x1212000000000000" ## My ip address prefix (on the TUN interface)
      },
      {
        "index": 1,
        "value": "0x00000000000000" ## First 7 bytes of my ip address suffix
      },
      {
        "index": 2,
        "value": "0x5454000000000000" ## The remote ip address prefix
      },
      {
        "index": 3,
        "value": "0x00000000000000" ## First 7 bytes of the remote ip address suffix
      }
    ]
  }
}
## note : comments are not allowed in JSON, remove them in real .json file
## in Core de/compression mode, the order of app and device fields is reversed at compression
```

### Device 2 aka end device

```sh
## create interface and ip address 1212::8 on my device, will communicate with 5454::9
sudo ./examples/SDKTunApp/common/scripts/ipv6_tun_client.sh 1212::8 5454::9
./build/linux/SDKTunApp examples/SDKTunApp/config/my_static_template.json
```

```sh
## In an other terminal
# netcat listens on port 5540, ip 1212::8 and writes on port 5539, ip 5454::8, for udp communication
nc -u -6 -s 1212::8 -p 5540 5454::9 5539 
```

```sh
## config:
cat examples/SDKTunApp/config/my_static_config.json
{
  "tun_name": "sdk_tun_client",
  "l2": {
    "vidpid": "0483:5740"
  },
  "template": {
    "params": [
      {
        "index": 0,
        "value": "0x1212000000000000"
      },
      {
        "index": 1,
        "value": "0x00000000000000"
      },
      {
        "index": 2,
        "value": "0x5454000000000000"
      },
      {
        "index": 3,
        "value": "0x00000000000000"
      }
    ]
  }
}
## note : comments are not allowed in JSON, remove them in real .json file
## in device de/compression mode, the order of app and device fields is reversed at decompression
```