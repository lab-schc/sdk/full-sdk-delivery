#  @copyright 2022-2022 ACKLIO SAS - All Rights Reserved
#  Unauthorized copying of this file, via any medium is strictly prohibited
#  Proprietary and confidential
#  @author Arthur Jourdan - arthur.jourdan@ackl.io

import socket
import time
from multiprocessing import Queue, Process

from test_utils import *


class communication:
  my_remote_address: (str, int)
  my_sock = None
  default_recv_size = 1024
  recv_timeout = 3  # in seconds

  recv_async_queue = Queue()
  recv_unexpected: str = ""
  recv_missed_packets = []

  def __new__(cls, *args, **kwargs):
    return super().__new__(cls)

  def __init__(self, local_address: (str, int), remote_address: (str, int)):
    self.my_remote_address = remote_address

    self.my_sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
    self.my_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    self.my_sock.bind(local_address)
    self.my_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BINDTODEVICE, str("sdk_tun_client").encode('utf-8'))

  def send(self, payload: str) -> int:
    return self.my_sock.sendto(payload.encode('utf-8'), self.my_remote_address)

  def send_packet(self, packet: str):
    print_color.Info("Sending \'" + packet + "\'")
    sent_bytes = self.send(packet)
    if sent_bytes != len(packet):
      print_color.Failure("Unable to send entire packet, " + sent_bytes + " bytes sent")
      return retvals.SEND_FAILURE
    print_color.Success("Packet sent")
    return retvals.TEST_SUCCESS

  def async_recv(self, size: int = default_recv_size):
    payload, addr = self.my_sock.recvfrom(size + 1)
    if addr == (self.my_remote_address[0], self.my_remote_address[1], 0, 0):
      recv_buff = payload.decode("utf-8")
      recv_buff = recv_buff.strip("\n")
      self.recv_async_queue.put(recv_buff)

  def recv(self, size: int = default_recv_size, timeout: float = recv_timeout) -> str:
    blocking_recv_process = Process(target=self.async_recv, args=(size,), name='Wait to receive')
    blocking_recv_process.start()
    blocking_recv_process.join(timeout=timeout)
    blocking_recv_process.terminate()
    if self.recv_async_queue.empty():
      return ""
    recv_buff = self.recv_async_queue.get()
    if recv_buff == "":
      return ""
    return recv_buff

  def clean_unexpected_buffs(self, packet):
    if packet in self.recv_missed_packets:
      self.recv_missed_packets.remove(packet)
    self.recv_unexpected.replace(packet, "")

  def add_unexpected_buffs(self, recv_buff: str, expected_packet: str, register_missed_expected):
    if recv_buff != "":
      self.recv_unexpected += recv_buff
    if register_missed_expected and expected_packet != "":
      self.recv_missed_packets.append(expected_packet)

  def recv_too_early(self, expected_packet):
    if expected_packet in self.recv_unexpected:
      print_color.Success("Expected packet was received earlier")
      self.clean_unexpected_buffs(expected_packet)
      return retvals.TEST_SUCCESS
    return retvals.TEST_FAILURE

  def recv_too_late(self, recv_buff: str):
    if recv_buff == "":
      return retvals.TEST_FAILURE
    for packet in self.recv_missed_packets:
      if packet in recv_buff:
        print_color.Success("Received packet corresponds to earlier expected : \'" + recv_buff + "\'")
        self.clean_unexpected_buffs(packet)
        return retvals.RECEIVE_LATE
    return retvals.TEST_FAILURE

  def recv_unexpected_packet(self, recv_buff: str, expected_packet: str, register_missed_expected: bool):
    if recv_buff != "":
      print_color.Failure("Received packet differs from expected")
    if self.recv_too_early(expected_packet) == retvals.TEST_SUCCESS:
      return retvals.TEST_SUCCESS
    if self.recv_too_late(recv_buff) == retvals.RECEIVE_LATE:
      return retvals.RECEIVE_LATE
    self.add_unexpected_buffs(recv_buff, expected_packet, register_missed_expected)
    return retvals.TEST_FAILURE

  def try_recv_packet(self, expected_packet: str, spent_time: float, register_missed_expected: bool) -> int:
    recv_buff = self.recv(len(expected_packet), timeout=(self.recv_timeout - spent_time))

    if recv_buff != expected_packet:
      return self.recv_unexpected_packet(recv_buff, expected_packet, register_missed_expected)
    print_color.Success("Received expected packet")
    return retvals.TEST_SUCCESS

  def recv_packet(self, expected_packet) -> int:
    register_missed_expected = True
    retval = retvals.TEST_FAILURE

    print_color.Info("Waiting for \'" + expected_packet + "\'")

    start_time = time.time()
    spent_time = time.time() - start_time
    while spent_time < self.recv_timeout:
      spent_time = time.time() - start_time
      tmp_retval = self.try_recv_packet(expected_packet, spent_time, register_missed_expected)
      if tmp_retval == retvals.TEST_SUCCESS:
        return retvals.TEST_SUCCESS
      elif tmp_retval == retvals.RECEIVE_LATE:
        retval = retvals.RECEIVE_LATE
      else:
        register_missed_expected = False

    print_color.Failure("Did not receive expected packet")
    return retval

  wait_before_sync = 5  # in seconds
  sync_timeout = 10  # in seconds
  sync_wait_before_response = 2  # in seconds

  def sync_session(self, session_name: str, master: bool = False):
    print_color.Info("Waiting for session start : \'" + session_name + "\'")

    if master:
      time.sleep(self.wait_before_sync)
      self.send(session_name)
      recv_buff = self.recv(timeout=self.sync_timeout, size=len(session_name))
      if recv_buff != session_name:
        print_color.Error("Could not initialize session \'" + session_name + "\'")
        return retvals.INVALID_SESSION
    else:
      recv_buff = self.recv(timeout=self.sync_timeout, size=len(session_name))
      if recv_buff != session_name:
        print_color.Error("Could not initialize session \'" + session_name + "\'")
        return retvals.INVALID_SESSION
      time.sleep(self.sync_wait_before_response)
      self.send(session_name)
    print_color.Info("Session initialized")
    return retvals.EXIT_SUCCESS