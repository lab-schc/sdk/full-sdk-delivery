#  @copyright 2022-2022 ACKLIO SAS - All Rights Reserved
#  Unauthorized copying of this file, via any medium is strictly prohibited
#  Proprietary and confidential
#  @author Arthur Jourdan - arthur.jourdan@ackl.io

import os

from utils import *


def parse_data_files_through_args(argv: [str]) -> []:
  data_files = []

  for index in range(7, len(argv)):
    if os.path.isdir(argv[index]):
      for file in os.listdir(argv[index]):
        if file.endswith(".csv"):
          data_files.append(os.path.join(argv[index], file))
    if os.path.isfile(argv[index]):
      if argv[index].endswith(".csv"):
        data_files.append(os.path.join(argv[index]))
  if len(data_files) == 0:
    print("No valid folder nor data file provided as parameter")
    exit(retvals.BAD_PARAMS)
  data_files.sort()
  return data_files


def get_args(argv: [str]) -> (str, (str, int), (str, int), str, [str]):
  if len(argv) < 7:
    print("Error, argc should be 7")
    exit(retvals.BAD_PARAMS)

  Compression_mode: str = (argv[1])

  if Compression_mode != "Device" and Compression_mode != "Core":
    print("First param must either be 'Device' or 'Core'")
    exit(retvals.BAD_PARAMS)

  local_address = (str(argv[2]), int(argv[3]))
  remote_address = (str(argv[4]), int(argv[5]))
  stats_file = str(argv[6])
  data_files = parse_data_files_through_args(argv)

  return (Compression_mode, local_address, remote_address, stats_file, data_files)