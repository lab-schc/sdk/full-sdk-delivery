#!/bin/python3

#  @copyright 2022-2022 ACKLIO SAS - All Rights Reserved
#  Unauthorized copying of this file, via any medium is strictly prohibited
#  Proprietary and confidential
#  @author Arthur Jourdan - arthur.jourdan@ackl.io

import csv
import os.path
import sys

from arguments import get_args
from communicate import communication
from stat_utils import *
from test_utils import *

Compression_mode: str
my_stats: stat
my_socket: communication

def get_packet_from_csv_row(csv_row: []) -> (str, str):
    my_row = []

    for elem in csv_row:
        my_row.append(elem)
    if len(my_row) < 3:
        return retvals.BAD_CSV
    mode = str(my_row[0])
    nb_reproduction = int(my_row[1])
    content = str(my_row[2])
    packet = nb_reproduction * content
    return (mode, packet)


def process_packet(csv_row: []):
    global my_stats

    (mode, packet) = get_packet_from_csv_row(csv_row)
    if mode == Compression_mode:
        retval = my_socket.send_packet(packet)
        if retval == retvals.TEST_SUCCESS:
            my_stats.add_sent_bytes(len(packet))
        else:
            my_stats.add_missed_send_bytes(len(packet))
    else:
        retval = my_socket.recv_packet(packet)
        if retval == retvals.TEST_SUCCESS:
            my_stats.add_received_bytes(len(packet))
        elif retval == retvals.RECEIVE_LATE:
            my_stats.remove_missed_receive_bytes(len(packet))
            my_stats.add_received_bytes(len(packet))
        else:
            my_stats.add_missed_receive_bytes(len(packet))
    return retval

def test_session(csv_rows: [], file: str):
    global my_stats
    retval: int = retvals.TEST_SUCCESS
    tmp_retval: int = retvals.TEST_SUCCESS

    my_stats.start_session("Session start", file)
    for csv_row in csv_rows:
        tmp_retval = process_packet(csv_row)
        if tmp_retval != retvals.TEST_SUCCESS:
            retval = retvals.TEST_FAILURE
    my_stats.end_session(retval, "Session end, stats :")
    return retval


def parse_data_file(file: str):
    global my_socket

    with open(file, newline='') as csvfile:
        print("---")
        if my_socket.sync_session(os.path.basename(file),
                                  master=True if Compression_mode == "Core" else False) != retvals.EXIT_SUCCESS:
            return retvals.INVALID_SESSION
        csv_rows = csv.reader(csvfile, delimiter=',')
        return test_session(csv_rows, file=file)

def parse_data_files(files: [str]):
    for file in files:
        test_success = parse_data_file(file)
        print_color.check_test_success(test_success, "Data test file : \'" + file + "\'")

def exec_test_suite(argv: [str]):
    global Compression_mode
    (Compression_mode, local_address, remote_address, stats_file, data_files) = get_args(argv)

    global my_stats
    my_stats = stat(stats_file)

    global my_socket
    my_socket = communication(local_address, remote_address)

    parse_data_files(data_files)

exec_test_suite(sys.argv)