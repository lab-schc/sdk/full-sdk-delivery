#  @copyright 2022-2022 ACKLIO SAS - All Rights Reserved
#  Unauthorized copying of this file, via any medium is strictly prohibited
#  Proprietary and confidential
#  @author Arthur Jourdan - arthur.jourdan@ackl.io

import json
import time
from io import StringIO
from os.path import basename

from test_utils import print_color


class stat:
  sent_bytes: int = 0
  missed_send_bytes: int = 0
  send_nb: int = 0
  missed_send_nb: int = 0
  received_bytes: int = 0
  missed_receive_bytes: int = 0
  receive_nb: int = 0
  missed_receive_nb: int = 0

  save_filepath_json: str = "./stats.json"
  test_filename: str = ""

  def __init__(self, stats_file: str):
    self.save_filepath_json = stats_file
    if not self.save_filepath_json.endswith(".json"):
      self.save_filepath_json += ".json"

  def start_timer(self):
    self.start_time = time.time()

  def end_timer(self):
    self.end_time = time.time()

  def print_timer(self):
    print_color.Stat("Lasted " + str(self.end_time - self.start_time) + " seconds")

  def reset_timer(self):
    self.start_time = 0
    self.end_time = 0

  start_time: float
  end_time: float

  def add_sent_bytes(self, nb_bytes: int):
    self.send_nb += 1
    self.sent_bytes += nb_bytes

  def add_missed_send_bytes(self, nb_bytes: int):
    self.missed_send_nb += 1
    self.missed_send_bytes += nb_bytes

  def reset_sent_bytes(self):
    self.sent_bytes = 0
    self.send_nb = 0
    self.missed_send_bytes = 0
    self.missed_send_nb = 0

  def print_sent_bytes(self):
    print_color.Stat("Sent " + str(self.sent_bytes) + " byte(s) in " + str(self.send_nb) + " packet(s)  ")
    if self.missed_send_bytes > 0:
      print_color.Stat(
        "Should have sent " + str(self.missed_send_bytes) + " byte(s) in " +
        str(self.missed_send_nb) + " packet(s) more")

  def add_received_bytes(self, nb_bytes: int):
    self.receive_nb += 1
    self.received_bytes += nb_bytes

  def add_missed_receive_bytes(self, nb_bytes: int):
    self.missed_receive_nb += 1
    self.missed_receive_bytes += nb_bytes

  def remove_missed_receive_bytes(self, nb_bytes: int):
    self.missed_receive_nb -= 1
    self.missed_receive_bytes -= nb_bytes

  def reset_received_bytes(self):
    self.received_bytes = 0
    self.receive_nb = 0
    self.missed_receive_bytes = 0
    self.missed_receive_nb = 0

  def print_received_bytes(self):
    print_color.Stat(
      "Received " + str(self.received_bytes) + " byte(s) in " + str(self.receive_nb) + " packet(s)")
    if self.missed_receive_bytes > 0:
      print_color.Stat("Should have received " + str(self.missed_receive_bytes) + " byte(s) in " + str(
        self.missed_receive_nb) + " packet(s) more")

  def save_file(self, retval):
    with open(self.save_filepath_json, mode='a') as save:
      io = StringIO()
      json.dump({"Data_test_file": self.test_filename,
                 "Exit_code": retval,
                 "Time_seconds": self.end_time - self.start_time,
                 "Sent": {"Packets": self.send_nb,
                          "Bytes_total": self.sent_bytes,
                          "Missed": {
                            "Packets": self.missed_send_nb,
                            "Bytes_total": self.missed_send_bytes
                          }},
                 "Received": {"Packets": self.receive_nb,
                              "Bytes_total": self.received_bytes,
                              "Missed": {
                                "Packets": self.missed_receive_nb,
                                "Bytes_total": self.missed_receive_bytes
                              }}}, fp=io, indent=2)
      save.write(io.getvalue())
      save.write(",\n")

  def reset(self):
    self.reset_timer()
    self.test_filename = ""
    self.reset_sent_bytes()
    self.reset_received_bytes()

  def start_session(self, msg: str, test_filename: str):
    self.test_filename = basename(test_filename)
    self.start_timer()
    print_color.Stat(msg)

  def end_session(self, retval: int, msg: str):
    self.end_timer()
    print_color.Stat(msg)
    print_color.Stat("Exit " + str(retval))
    self.print_timer()
    self.print_sent_bytes()
    self.print_received_bytes()
    self.save_file(retval)
    self.reset()