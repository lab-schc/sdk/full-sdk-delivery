#  @copyright 2022-2022 ACKLIO SAS - All Rights Reserved
#  Unauthorized copying of this file, via any medium is strictly prohibited
#  Proprietary and confidential
#  @author Arthur Jourdan - arthur.jourdan@ackl.io

class retvals:
  EXIT_SUCCESS = 0
  EXIT_FAILURE = 1
  TEST_SUCCESS = 0
  TEST_FAILURE = 2
  BAD_PARAMS = 4
  BAD_CSV = 4
  INVALID_SESSION = 5
  SEND_FAILURE = 6
  RECEIVE_LATE = 7

  # def int(self):
  #   return self.value
  # def __init__(self):
  # return self.value


class colors:
  enabled: bool = True

  if enabled:
    Off = '\033[0m'  # Text Reset
  else:
    Off = ""

  # Regular Colors
  if enabled:
    Black='\033[0;30m'        # Black
  else:
    Black = ""
  if enabled:
    Red='\033[0;31m'          # Red
  else:
    Red = ""
  if enabled:
    Green='\033[0;32m'        # Green
  else:
    Green = ""
  if enabled:
    Yellow='\033[0;33m'       # Yellow
  else:
    Yellow = ""
  if enabled:
    Blue='\033[0;34m'         # Blue
  else:
    Blue = ""
  if enabled:
    Purple='\033[0;35m'       # Purple
  else:
    Purple = ""
  if enabled:
    Cyan='\033[0;36m'         # Cyan
  else:
    Cyan = ""
  if enabled:
    White='\033[0;37m'        # White
  else:
    White = ""

  # Bold
  if enabled:
    BBlack='\033[1;30m'       # Black
  else:
    BBlack = ""
  if enabled:
    BRed='\033[1;31m'         # Red
  else:
    BRed = ""
  if enabled:
    BGreen='\033[1;32m'       # Green
  else:
    BGreen = ""
  if enabled:
    BYellow='\033[1;33m'      # Yellow
  else:
    BYellow = ""
  if enabled:
    BBlue='\033[1;34m'        # Blue
  else:
    BBlue = ""
  if enabled:
    BPurple='\033[1;35m'      # Purple
  else:
    BPurple = ""
  if enabled:
    BCyan='\033[1;36m'        # Cyan
  else:
    BCyan = ""
  if enabled:
    BWhite='\033[1;37m'       # White
  else:
    BWhite = ""

  # Underline
  if enabled:
    UBlack='\033[4;30m'       # Black
  else:
    UBlack = ""
  if enabled:
    URed='\033[4;31m'         # Red
  else:
    URed = ""
  if enabled:
    UGreen='\033[4;32m'       # Green
  else:
    UGreen = ""
  if enabled:
    UYellow='\033[4;33m'      # Yellow
  else:
    UYellow = ""
  if enabled:
    UBlue='\033[4;34m'        # Blue
  else:
    UBlue = ""
  if enabled:
    UPurple='\033[4;35m'      # Purple
  else:
    UPurple = ""
  if enabled:
    UCyan='\033[4;36m'        # Cyan
  else:
    UCyan = ""
  if enabled:
    UWhite='\033[4;37m'       # White
  else:
    UWhite = ""

  # Background
  if enabled:
    On_Black='\033[40m'       # Black
  else:
    On_Black = ""
  if enabled:
    On_Red='\033[41m'         # Red
  else:
    On_Red = ""
  if enabled:
    On_Green='\033[42m'       # Green
  else:
    On_Green = ""
  if enabled:
    On_Yellow='\033[43m'      # Yellow
  else:
    On_Yellow = ""
  if enabled:
    On_Blue='\033[44m'        # Blue
  else:
    On_Blue = ""
  if enabled:
    On_Purple='\033[45m'      # Purple
  else:
    On_Purple = ""
  if enabled:
    On_Cyan='\033[46m'        # Cyan
  else:
    On_Cyan = ""
  if enabled:
    On_White='\033[47m'       # White
  else:
    On_White = ""

  # High Intensity
  if enabled:
    IBlack='\033[0;90m'       # Black
  else:
    IBlack = ""
  if enabled:
    IRed='\033[0;91m'         # Red
  else:
    IRed = ""
  if enabled:
    IGreen='\033[0;92m'       # Green
  else:
    IGreen = ""
  if enabled:
    IYellow='\033[0;93m'      # Yellow
  else:
    IYellow = ""
  if enabled:
    IBlue='\033[0;94m'        # Blue
  else:
    IBlue = ""
  if enabled:
    IPurple='\033[0;95m'      # Purple
  else:
    IPurple = ""
  if enabled:
    ICyan='\033[0;96m'        # Cyan
  else:
    ICyan = ""
  if enabled:
    IWhite='\033[0;97m'       # White
  else:
    IWhite = ""

  # Bold High Intensity
  if enabled:
    BIBlack='\033[1;90m'      # Black
  else:
    BIBlack = ""
  if enabled:
    BIRed='\033[1;91m'        # Red
  else:
    BIRed = ""
  if enabled:
    BIGreen='\033[1;92m'      # Green
  else:
    BIGreen = ""
  if enabled:
    BIYellow='\033[1;93m'     # Yellow
  else:
    BIYellow = ""
  if enabled:
    BIBlue='\033[1;94m'       # Blue
  else:
    BIBlue = ""
  if enabled:
    BIPurple='\033[1;95m'     # Purple
  else:
    BIPurple = ""
  if enabled:
    BICyan='\033[1;96m'       # Cyan
  else:
    BICyan = ""
  if enabled:
    BIWhite='\033[1;97m'      # White
  else:
    BIWhite = ""

  # High Intensity backgrounds
  if enabled:
    On_IBlack='\033[0;100m'   # Black
  else:
    On_IBlack = ""
  if enabled:
    On_IRed='\033[0;101m'     # Red
  else:
    On_IRed = ""
  if enabled:
    On_IGreen='\033[0;102m'   # Green
  else:
    On_IGreen = ""
  if enabled:
    On_IYellow='\033[0;103m'  # Yellow
  else:
    On_IYellow = ""
  if enabled:
    On_IBlue='\033[0;104m'    # Blue
  else:
    On_IBlue = ""
  if enabled:
    On_IPurple='\033[0;105m'  # Purple
  else:
    On_IPurple = ""
  if enabled:
    On_ICyan='\033[0;106m'    # Cyan
  else:
    On_ICyan = ""
  if enabled:
    On_IWhite='\033[0;107m'   # White
  else:
    On_IWhite = ""