#  @copyright 2022-2022 ACKLIO SAS - All Rights Reserved
#  Unauthorized copying of this file, via any medium is strictly prohibited
#  Proprietary and confidential
#  @author Arthur Jourdan - arthur.jourdan@ackl.io

from utils import *


class print_color:

  def Any(color: str, msg: str):
    print(colors.Off + color + msg + colors.Off)

  def Success(msg: str):
    print(colors.Off + colors.On_Green + " Success " + colors.Off + " " + colors.Green + msg + colors.Off + "\n")

  def Failure(msg: str):
    print(colors.Off + colors.On_Purple + " Failure " + colors.Off + " " + colors.Purple + msg + colors.Off + "\n")

  def Error(msg: str):
    print(colors.Off + colors.On_Red + " Error " + colors.Off + " " + colors.Red + msg + colors.Off)

  def Info(msg: str):
    print(colors.Off + colors.On_Yellow + " Info " + colors.Off + " " + colors.Yellow + msg + colors.Off)

  def Stat(msg: str):
    print(colors.Off + colors.On_Cyan + " Stat " + colors.Off + " " + colors.Cyan + msg + colors.Off)


  def check_test_success(retval: int, msg: str):
    if retval == retvals.TEST_SUCCESS:
      print_color.Success(msg)
    else:
      print_color.Error(msg)