#!/bin/bash

# dependency and dependant of benchmark.sh

PAYLOAD=""

function construct_payload
{
  reproduction_nb=$1
  sequence=$2

  if (( $# != 2 )) || (( $reproduction_nb <= 0 )) || [ "$sequence" = "" ] ; then
      return $BAD_PARAMS
  fi
  PAYLOAD=""
  for (( i = 0; i < $reproduction_nb; i++ )); do
    PAYLOAD="$PAYLOAD$sequence"
  done
  return $EXIT_SUCCESS
}

function send_payload
{
  echo "Sending\t\t'$PAYLOAD'"
  echo $PAYLOAD > $NC_PIPE_IN
}

function receive_payload
# TODO keep old lines ? (if receive one packet before another)
#   clearly should remove old lines
{
  echo "Waiting for\t'$PAYLOAD'"
  end=$((SECONDS+3)) # SECONDS represents the count of the time that the script has been running for
  ## todo adjust waiting time

  while [ $SECONDS -lt $end ]; do
    while read -r line
      do
#        echo -e "$NC_OUT has :\t" "\"$line\""
#        echo -e "Payload has :\t\t" "\"$PAYLOAD\""

        if [ "$line" = "$PAYLOAD" ]; then
          echo "Received expected packet"
          echo "" > $NC_OUT ## reset content
          return $EXIT_SUCCESS
        fi
    done < $NC_OUT
  done
  return $EXIT_FAILURE
}

function communicate_data_file
{
  ## todo before starting session, send magic payload in loop, when receiver sends it back, then start
  Communication_data_file=$1

  if [ "$Communication_data_file" = "" ] || ! [ -r $Communication_data_file ] ; then
      err_msg "Communication data file '$Communication_data_file' cannot be read"
      return $BAD_PARAMS
  fi

  while IFS="," read -r mode reproduction_nb sequence
  do
    echo "---"

    PAYLOAD=""

#    echo "Mode : $mode"
#    echo "reproduction: $reproduction_nb"
#    echo "sequence: $sequence"

    construct_payload $reproduction_nb $sequence
    if (( $? != $EXIT_SUCCESS )); then
      continue
    fi
    if [ "$mode" = "$COMPRESSION_MODE" ]; then
      send_payload
    else
      receive_payload
      local TMP_RETVAL=$?
      if (( $TMP_RETVAL != $EXIT_SUCCESS )); then
        return $TMP_RETVAL
      fi
    fi
    echo ""

  done < $Communication_data_file
  return $EXIT_SUCCESS
}

function communicate_data_files
{
  local files=$@
  local RETVAL=$EXIT_SUCCESS

  for file in ${files[@]} ; do
    echo "\nCommunicate data file :" $file
    communicate_data_file $file
    local TMP_RETVAL=$?
    check_test_success $TMP_RETVAL "Test data file : $file"
    if (( $TMP_RETVAL != $EXIT_SUCCESS )); then
      RETVAL=$TEST_FAILED
    fi
  done
  return $RETVAL
}