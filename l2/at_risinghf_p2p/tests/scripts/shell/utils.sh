#!/bin/bash

EXIT_SUCCESS=0
EXIT_FAILURE=1
TEST_FAILED=2
SIGNAL_CAUGHT=3
BAD_PARAMS=4
ERR_TUN_INTERFACE=5
ERR_SDKTUNAPP=6
ERR_NETCAT=7

COLOR_ENABLED=1
# Color use :
## Yellow : Info
## Purple : Non fatal error
## Red : Fatal error

# Reset
Color_Off='\033[0m'       # Text Reset

# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Bold
BBlack='\033[1;30m'       # Black
BRed='\033[1;31m'         # Red
BGreen='\033[1;32m'       # Green
BYellow='\033[1;33m'      # Yellow
BBlue='\033[1;34m'        # Blue
BPurple='\033[1;35m'      # Purple
BCyan='\033[1;36m'        # Cyan
BWhite='\033[1;37m'       # White

# Underline
UBlack='\033[4;30m'       # Black
URed='\033[4;31m'         # Red
UGreen='\033[4;32m'       # Green
UYellow='\033[4;33m'      # Yellow
UBlue='\033[4;34m'        # Blue
UPurple='\033[4;35m'      # Purple
UCyan='\033[4;36m'        # Cyan
UWhite='\033[4;37m'       # White

# Background
On_Black='\033[40m'       # Black
On_Red='\033[41m'         # Red
On_Green='\033[42m'       # Green
On_Yellow='\033[43m'      # Yellow
On_Blue='\033[44m'        # Blue
On_Purple='\033[45m'      # Purple
On_Cyan='\033[46m'        # Cyan
On_White='\033[47m'       # White

# High Intensity
IBlack='\033[0;90m'       # Black
IRed='\033[0;91m'         # Red
IGreen='\033[0;92m'       # Green
IYellow='\033[0;93m'      # Yellow
IBlue='\033[0;94m'        # Blue
IPurple='\033[0;95m'      # Purple
ICyan='\033[0;96m'        # Cyan
IWhite='\033[0;97m'       # White

# Bold High Intensity
BIBlack='\033[1;90m'      # Black
BIRed='\033[1;91m'        # Red
BIGreen='\033[1;92m'      # Green
BIYellow='\033[1;93m'     # Yellow
BIBlue='\033[1;94m'       # Blue
BIPurple='\033[1;95m'     # Purple
BICyan='\033[1;96m'       # Cyan
BIWhite='\033[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\033[0;100m'   # Black
On_IRed='\033[0;101m'     # Red
On_IGreen='\033[0;102m'   # Green
On_IYellow='\033[0;103m'  # Yellow
On_IBlue='\033[0;104m'    # Blue
On_IPurple='\033[0;105m'  # Purple
On_ICyan='\033[0;106m'    # Cyan
On_IWhite='\033[0;107m'   # White

regexp_is_number='^[0-9]+$'
regexp_is_ip='(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))'


function echo
{
  command echo -e "$@"
}

declare -a CHILD_PIDS
# add element after forking (with '&' after a command) :
#   CHILD_PIDS+=($!)

function kill_children
{
  if [ $COLOR_ENABLED ]; then
    echo -n "$Color_Off$Yellow"
  fi
  for CHILD in "${CHILD_PIDS[@]}"
  do
    echo "killing child : $CHILD"
    sudo kill $CHILD
  done
  if [ $COLOR_ENABLED ]; then
    echo -n $Color_Off
  fi
  sudo pkill SDK # TODO Search why pending process
}

function exit
{
  local TMP_RETVAL=$1
  MSG="$2"

  kill_children

  if [ "$MSG" != "" ]; then
    if [ $COLOR_ENABLED ]; then
      echo -n "$Color_Off$On_Yellow"
    fi
    echo -n " Exit "
    if [ $COLOR_ENABLED ]; then
      echo -n "$Color_Off$Yellow"
    fi
    echo " $MSG"
    if [ $COLOR_ENABLED ]; then
      echo -n $Color_Off
    fi
  fi
  command exit $TMP_RETVAL
}

function fatal_err
{
  local TMP_RETVAL=$1

  if [ $COLOR_ENABLED ]; then
    command echo -ne "$Color_Off$On_Red" >&2
  fi
  command echo -ne " Error " >&2
  if [ $COLOR_ENABLED ]; then
    command echo -ne "$Color_Off$Red" >&2
  fi
  args=("$@")
  for (( i = 1; i < $#; i++ )); do
    command echo -e ${args[${i}]} >&2
  done
  if [ $COLOR_ENABLED ]; then
    command echo -ne $Color_Off >&2
  fi
  exit $TMP_RETVAL
}

function err_msg
{
  if [ $COLOR_ENABLED ]; then
    command echo -ne "$Color_Off$On_Purple" >&2
  fi
  command echo -ne " Error " >&2
  if [ $COLOR_ENABLED ]; then
    command echo -ne "$Color_Off$Purple" >&2
  fi
  command echo -e $@ >&2
  if [ $COLOR_ENABLED ]; then
    command echo -ne $Color_Off >&2
  fi
}

function info_msg
{
  if [ $COLOR_ENABLED ]; then
    command echo -ne "$Color_Off$On_Yellow" >&2
  fi
  command echo -ne " Info " >&2
  if [ $COLOR_ENABLED ]; then
    command echo -ne "$Color_Off$Yellow" >&2
  fi
  command echo -e $@ >&2
  if [ $COLOR_ENABLED ]; then
    command echo -ne $Color_Off >&2
  fi
}

function check_test_success
{
  local TMP_RETVAL=$1

  if (( $TMP_RETVAL == $EXIT_SUCCESS )) ; then
    display=" Success "
    color_display=$On_Green
    color_msg=$Green
  else
    display=" Failure "
    color_display=$On_Red
    color_msg=$Red
  fi

  if [ $COLOR_ENABLED ]; then
    command echo -ne "$Color_Off$color_display"
  fi
  command echo -ne "$display"
  if [ $COLOR_ENABLED ]; then
    command echo -ne "$Color_Off$color_msg"
  fi
  args=("$@")
  for (( i = 1; i < $#; i++ )); do
    command echo -e "" ${args[${i}]}
  done
  if [ $COLOR_ENABLED ]; then
    command echo -ne $Color_Off
  fi
  return $TMP_RETVAL
}

function reset_color
{
  if [ $COLOR_ENABLED ]; then
    command echo -ne "$Color_Off"
  fi
}