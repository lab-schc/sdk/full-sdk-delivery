#!/bin/bash

#PARAMS=$*
for param in $@; do
    PARAMS+=("$param")
done



NB_PARAMS=$#
SCRIPT_NAME="$0"
COMPRESSION_MODE="$1"
# Either Core or Device,
# Used during
#   compilation
#   test management

NC_PIPE_IN=/tmp/netcat_pipe_in
NC_OUT=/tmp/netcat_out

# TODO add option to launch certain data files, or all

SCRIPT_DIR=$( dirname $SCRIPT_NAME )

source "$SCRIPT_DIR/utils.sh"
source "$SCRIPT_DIR/parameters.sh"
source "$SCRIPT_DIR/communicate_data_file.sh"

trap "exit $SIGNAL_CAUGHT 'Caught SIGINT'" SIGINT

check_params

communicate_data_files ${DATA_TEST_FILES[@]}
RETVAL=$?
sleep 2

if (( $RETVAL == $EXIT_SUCCESS  )) ; then
  exit $EXIT_SUCCESS "Tests executed successfully"
else
  exit $RETVAL "One or more test failed"
fi