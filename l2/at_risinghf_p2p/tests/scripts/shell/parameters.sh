#!/bin/bash

# benchmark.sh parameter handling
function help_msg
{
  if [ $COLOR_ENABLED ]; then
      echo -n $Color_Off $On_Yellow >&2
  fi
  echo -n " USAGE " >&2
  if [ $COLOR_ENABLED ]; then
      echo  $Color_Off >&2
  fi
  command echo -e "\t" $SCRIPT_NAME "<Compression mode: Device|Core>" "<Directory|Files to test>" >&2
  if [ $COLOR_ENABLED ]; then
        echo -n $On_Yellow >&2
    fi
    echo -n " EXAMPLES " >&2
    if [ $COLOR_ENABLED ]; then
        echo  $Color_Off >&2
    fi
  command echo -e "\t" $SCRIPT_NAME "Device /path/to/Directory_containing_data_test_files" >&2
  command echo -e "\t" $SCRIPT_NAME "Device /path/to/data_test_file_1 /path/to/data_test_file_2" >&2
}


function check_params
{
  if (( $NB_PARAMS < 2 )) ; then
    help_msg
    fatal_err $BAD_PARAMS "Incorrect number of parameters"
  fi
  if [ "$COMPRESSION_MODE" != "Core" ] && [ "$COMPRESSION_MODE" != "Device"  ]; then
    help_msg
    fatal_err $BAD_PARAMS "Compression mode does not exist (should be either \"Core\" or \"Device\")"
  fi

  # if 2nd param is folder
  #  get test files from it
  # else
  #  check for next params, exclude from test
  for (( i = 1; i < $NB_PARAMS; i++ )); do
  if [ -d ${PARAMS[i]} ]; then
      TMP_DATA_TEST_FILES=`ls "${PARAMS[1]}"/*.csv`
      for file in ${TMP_DATA_TEST_FILES[@]}; do
        DATA_TEST_FILES+=("$file")
      done
  elif [ -r "${PARAMS[i]}" ]; then
    DATA_TEST_FILES+=("${PARAMS[i]}")
  fi
  done

  if (( ${#DATA_TEST_FILES[@]} == 0 )) ; then
    help_msg
    fatal_err $BAD_PARAMS "No data file correctly provided as parameter"
  fi
}