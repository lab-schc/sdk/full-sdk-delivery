# The way one test data file should look like
## CSV Format
1. Each row represents one packet
2. Each packet contains several elements
3. Each elements are separated commas
   1. The first element of the line must be either "Device" or "Core", this designates the sender
   2. The second is the number of times the sequence (third element) must be reproduced
      - It should be an integer equal or bigger than 1
      - (this facilitates sending big packet without having to write nor count)
   3. The last element is a string, corresponding to the content that is to be sent 
      - surrounded by double quotes ("), 
      - any newline character (\n) would be misunderstood and should therefore be avoided
   4. Each more element is ignored
      - used as comment in the bellow example
   
packet template:
   <sender : Device|Core>,<number of reproductions>,<sequence to send>

```csv
Device,3,abcd, good example
Core,100,a, good example
Device,a,abcd, bad example this won't work
Core,-123,abcd, bad example this won't work
```