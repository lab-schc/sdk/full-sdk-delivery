# Ansible benchmark point to point

This file explains the stakes of **Ansible** / **shell scripts**
intended for packet sending / receiving automation, especially in point to point (with no IP Core) setup

## Configuration

The setup is intended to be used as follows :

1. One main computer, connected through usb to a RisingHF module (RHF3M076B)
    - Having the full-sdk-delivery
2. One other computer (**accessible through ssh**), connected through usb to another RisingHF
    - Having the full-sdk-delivery (on the same state as computer 1)

\
The RisingHF could be replaced by any **l2**

## Configuration

### Ansible hosts

In file [ansible_hosts](ansible/ansible_hosts), under label 'at_risinghf_p2p_test_servers' must be the ip of computer 2,
ssh user's username and ssh private key location.
(It is actually filled as an example)

### Ansible variables

[test_configuration.json](ansible/test_configuration.json)
contains every variable the Ansible script needs to perform its tasks

Do not remove any of the variables from the JSON, it would cause the script to fail !

Each step of the test can be enabled / disabled : TUN interface script, SDKTunApp compilation, SDKTunApp launch
to be ran locally, for easier error check for example.

#### Compilation

Concerning compilation / environment variables,
value must be "VARIABLE=VALUE",
instead of removing the variable set the value as an empty string ("")

Alternatively it can be disabled with boolean 'enabled' (you must the compile manually)

#### Environment

'FullSDKDelivery_absolute_path' must be an absolute path as the other paths will be concatenated to it

## Start benchmark

By launching the Ansible playbook, on both computers, it will :

1. **Compile SDKTunApp**
    - With the environment variables provided in configuration
2. **Set up the TUN interface**
    - With the IP addresses specified in configuration
3. **Launch the SDKTunApp** (asynchronously)
    - With the configuration file specified in configuration
4. **Launch a netcat connection** between the two computers (asynchronously)
    - With the IP addresses and UDP ports specified in configuration
5. **Launch [benchmark.sh](scripts/benchmark.sh)**, thus starting the test suite
    - With the data files specified in configuration

To do so, use the following command :

```shell
ANSIBLE_CONFIG=path/to/ansible.cfg ansible-playbook path/to/launch_tests_playbook.yml -i path/to/ansible_hosts --ask-become-pass -v
```

## How it works

### Python
One instance of [benchmark.py](scripts/python/benchmark.py) will write onto a socket,
which will send a UDP packet,
the SDKTunApp will catch, compress and fragment it then send it through the TUN interface.

On the other computer, the contrary :
TUN -> SDKTunApp -> socket
-> the script reads the data and checks whether it's correct or not


### Shell (deprecated)
One instance of [benchmark.sh](scripts/shell/benchmark.sh) will write onto the netcat,
which will send a UDP packet,
the SDKTunApp will catch, compress and fragment it then send it through the TUN interface.

On the other computer, the contrary :
TUN -> SDKTunApp -> netcat
-> the script reads the data and checks whether it's correct or not

---

The scripts must therefore be run in parallel. (Thus the use of Ansible)

[//]: # (todo update about folder copy)

---

If the one test file fails, it might fail only on one side,
and as it is UDP, the other computer would not be aware of the failure

### Data files

The data is taken from **CSV files**, they must be similar on both computers
in order for one script to know what to send and the other, what to expect.

Examples and [tutorial](test_data/data_to_send.md) are stored in [test_data](test_data/)

## Results

```shell
cat `find . -type f -name "stats.json"` ## benchmark.py writes on file, it is located where ansible was launched
```
[//]: # (todo : publish benchmark results)