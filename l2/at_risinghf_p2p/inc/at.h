/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Jourdan arthur.jourdan@ackl.io
 *
 *
 * This header is an interface that should be used by LoRa shield
 * implementations. The implementation should provide a filled `at_callbacks_t`
 * structure to the user.
 */

#ifndef AT_H_
#define AT_H_

#include "platform.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <termios.h>

// Comments show which callback will return which values.
typedef enum
{
  // All
  AT_SUCCESS,
  AT_INTERNAL_ERR,
  AT_UNKNOWN_ERR,

  // command execution
  AT_CMD_ERROR,

  // init
  AT_NO_MODE,
  AT_NO_LOG,
  AT_NO_CONF,
  AT_NO_ACTIVATE_RECV,

  // send
  AT_SEND_TOO_BIG,

  // receive
  AT_MALFORMED,
  AT_BUFF_TOO_SMALL,

} at_error_t;

const char *at_strerror(at_error_t e);
at_error_t at_error_raw_to_enum(const char *raw);
const char *at_error_raw_to_str(const char *raw);

#define print_at_error(msg)                                                    \
  {                                                                            \
    fprintf(stderr, "AT error in %s():", __FUNCTION__);                        \
    fprintf(stderr, "%s\n", msg);                                              \
  }

typedef struct
{
  // Serial device configuration :
  const uint8_t *vid_pid;
  speed_t baudrate;
  const char *app_key;
  // AT configuration :
  //  Most are used in AT config command :
  //  AT+TEST=RFCFG,[Freq],[SF],[BW],[TXPR],[RXPR],[POW]
  /**
   * @var frequency
   *  in MHz
   */
  const float frequency;
  /**
   * @var spreadfactor
   *  (7, 8, 9, 10, 11 or 12)
   */
  uint8_t spreadfactor;
  /**
   * @var bandwidth
   *  (125, 250...)
   * in kHz
   */
  const uint16_t bandwidth;
  /**
   * @var tx_preamble_len
   *  in symbols
   */
  const uint8_t tx_preamble_len;
  /**
   * @var rx_preamble_len
   * Preamble length of RX should be equal to or bigger than TX
   *  in symbols
   */
  const uint8_t rx_preamble_len;
  /**
   * @var tx_power
   * Transmission power
   *  in dbm
   */
  const uint8_t tx_power;
  /**
   * @var cyclic_redundancy_check
   * activate or deactivate LoRa data check
   *  ON or OFF
   */
  const bool cyclic_redundancy_check;
  /**
   * @var IQ
   * activate or deactivate LoRa inversion IQ mode
   *  ON or OFF
   */
  const bool IQ;
  /**
   * @var cyclic_redundancy_check
   * activate or deactivate LoRa NET
   *  ON or OFF
   */
  const bool NET;
  /**
   * @var log_mode
   *  log_mode can be DEBUG, INFO, WARN, ERROR, FATAL, PANIC or QUIET
   *  if not quiet, there is a risk of getting blocking read, due to the number
   *  of serial_readline not being equal to the number of lines written by rhf
   */
  const char *log_mode;
  /**
   * @var mtu
   * MTU will be capped if too big : see _set_mtu() in at_risinghf.c
   * Set this variable to 0 for maximum
   */
  const uint16_t mtu;
} at_shield_params_t;

typedef at_error_t (*at_init_t)(at_shield_params_t*);
typedef void (*at_fini_t)(void);
typedef at_error_t (*at_join_t)(void);
typedef at_error_t (*at_send_t)(const uint8_t *data, size_t size);
/*
 * at_recv_t takes as parameters:
 * A buffer `data` of size `*size`
 *
 * at_recv_t stores:
 * The received data in `data`
 * The number of bytes received in `*size`
 */
typedef at_error_t (*at_recv_t)(uint8_t *data, size_t *size);
/**
 * at_poll_t has to be called once a real call to poll() on the serial fd
 * indicates that data is available. It allows to encapsulate some unwanted
 * behavior from a raw call to poll(), such as:
 * - avoid reading empty lines
 * - cache some lines that have been read too early
 *
 * Parameter:
 * - block: - true to check for buffered line AND read from serial if nothing
 *          - false to only check for buffered line
 *
 * Returns:
 * - 1 if at_send_t will not block (pending data)
 * - 0 if at_send_t will block
 * - -1 on error
 */
typedef int (*at_poll_t)(bool block);
typedef uint16_t (*at_mtu_t)(void);

typedef struct
{
  at_init_t init; // Initialize shield with provided parameters
  at_fini_t fini; // Free the resources used by the shield
  at_send_t send; // Send uplink frame
  at_recv_t recv; // Receive downlink frame (blocking call)
  at_poll_t poll; // Poll the serial port to check if there is any incoming data
  at_mtu_t mtu;   // Retrieve the Maximum Transmission Unit for a given data rate
} at_callbacks_t;

#endif