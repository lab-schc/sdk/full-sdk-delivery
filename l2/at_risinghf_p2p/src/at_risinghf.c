/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Jourdan arthur.jourdan@ackl.io
 */

#define _GNU_SOURCE // needed to use asprintf()
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "vpid_to_tty.h"
#include "at_risinghf.h"
#include "serial.h"
#include "tools.h"

typedef struct
{
  const char *fmt;
  char *cmd;
  at_error_t (*run)(const char*);
} command_t;

typedef struct
{
  command_t mode;
  command_t log;
  command_t conf;
  command_t send;
  command_t activate_recv;
} commands_t;

static at_error_t _cmd_send(const char*);

static at_error_t _cmd_exec_check(const char *cmd);

static void _set_mtu(const at_shield_params_t *p);

static commands_t _cmd = {
    {NULL, "AT+MODE=TEST\n", _cmd_exec_check},
    // Log type can be : DEBUG/INFO/WARN/ERROR/FATAL/PANIC/QUIET
    // Warning : log type changes the writes made by rhf and possibly cause
    //  infinite blocking reads
    // todo make sure it does not cause infinite blocking reads
    {"AT+LOG=%s\n", NULL, _cmd_exec_check},
    // config such as : frequency, spreadfactor, bandwidth, tx_preamble_len,
    // rx_preamble_len, tx_power, cyclic redundancy check, IQ, NET
    {"AT+TEST=RFCFG,%.3f,%i,%i,%i,%i,%i,%s,%s,%s\n", NULL, _cmd_exec_check},
    {"AT+TEST=TXLRPKT,\"%s\"\n", NULL, _cmd_send},
    {NULL, "AT+TEST=RXLRPKT\n", _cmd_exec_check}};

static char *_response_buff = NULL; // serial_readline() buff
static char *_rx_buff = NULL; // buff to store immediate RX during TX
static const char _data_needle[] = "+TEST: RX \"";
static const char _send_done_needle[] = "+TEST: TX DONE";
static uint16_t _mtu_size = 252; // bytes, will be changed at initialisation
// serial limitation : max 254 bytes, 508 characters in hex
//    Trying to send a payload above this size will result in "+AT: ERROR(-24)"
// full-sdk limitation : max 252 bytes as the memory needs to be aligned on 4

static inline at_error_t _exec_cmd(command_t *cmd)
{
  return cmd->run(cmd->cmd);
}

static at_error_t _init_get_serial_port(char path[PATH_MAX],
                                        const at_shield_params_t *p)
{
  // Find serial port TTY path from VID and PID
  char split_vpid[strlen((const char *)p->vid_pid) + 1];
  strcpy(split_vpid, (const char *)p->vid_pid);
  char *vid = split_vpid;
  char *pid = strchr(split_vpid, ':');

  if (!pid)
  {
    fprintf(stderr, "Error: RisingHF init: VID:PID parsing failed\n");
    return AT_INTERNAL_ERR;
  }
  *(pid++) = '\0'; // replace ':' by '\0' to split the strings
  if (!find_tty_path_from_vpid(path, vid, pid)) {
    fprintf(stderr, "Error: RisingHF init: find_tty_path_from_vpid() failed\n");
    return AT_INTERNAL_ERR;
  }
  else if (path[0] == '\0') {
#ifdef USE_LPWAN_SIMULATOR
    strcpy(path, LPWAN_SIMU_TTY_PORT);
#else
    fprintf(stderr, "Error: RisingHF init: no TTY associated with such VID:PID\n");
    return AT_INTERNAL_ERR;
#endif
  }
  return AT_SUCCESS;
}

static at_error_t _init_config(const at_shield_params_t *p)
{
  at_error_t err;
  at_error_t ret_val;

  if ((err = _exec_cmd(&_cmd.mode)) != AT_SUCCESS)
  {
    ret_val = AT_NO_MODE;
    goto check_err_return;
  }
  if ((err = _exec_cmd(&_cmd.log)) != AT_SUCCESS)
  {
    ret_val = AT_NO_LOG;
    goto check_err_return;
  }
  if ((err = _exec_cmd(&_cmd.conf)) != AT_SUCCESS)
  {
    ret_val = AT_NO_CONF;
    goto check_err_return;
  }
  _set_mtu(p);
  if ((err = _exec_cmd(&_cmd.activate_recv)) != AT_SUCCESS) {
    ret_val = AT_NO_ACTIVATE_RECV;
    goto check_err_return;
  }
check_err_return:
  if (err == AT_CMD_ERROR)
    return ret_val;
  return err;
}

static at_error_t _init(at_shield_params_t *p)
{
  at_error_t err;
  char path[PATH_MAX]; // Filepath to the serial port

  // Init command strings
  if (asprintf(&_cmd.log.cmd, _cmd.log.fmt, p->log_mode) == -1 ||
      asprintf(&_cmd.conf.cmd, _cmd.conf.fmt, p->frequency, p->spreadfactor,
               p->bandwidth, p->tx_preamble_len, p->rx_preamble_len,
               p->tx_power, (p->cyclic_redundancy_check ? "ON" : "OFF"),
               (p->IQ ? "ON" : "OFF"), (p->NET ? "ON" : "OFF")) == -1)
    return AT_INTERNAL_ERR;

  if ((err = _init_get_serial_port(path, p)))
    return err;

  // Init serial com
  if (serial_init(path, p->baudrate) != 0)
    return AT_INTERNAL_ERR;

  if ((err = _init_config(p)) != AT_SUCCESS)
    return err;
  return AT_SUCCESS;
}

static void _fini(void)
{
  free(_response_buff);
  free(_rx_buff);
  free(_cmd.log.cmd);
  free(_cmd.conf.cmd);
  free(_cmd.send.cmd);
  serial_fini();
}

static at_error_t _send(const uint8_t *data, size_t size)
{
  char hexstr[2 * size - 1];

  if (size < 2) // check for rule ID + payload
    return AT_INTERNAL_ERR;
  memset(hexstr, 0, sizeof(hexstr));
  for (size_t i = 0; i < size; ++i)
    sprintf(hexstr + (i * 2), "%02X", data[i]);
  free(_cmd.send.cmd);
  if (asprintf(&_cmd.send.cmd, _cmd.send.fmt, hexstr) == -1)
    return AT_INTERNAL_ERR;
  return _exec_cmd(&_cmd.send);
}

static at_error_t _parse_downlink(char *start, uint8_t *data, size_t *size)
{
  char *end;
  size_t nbr_bytes;

  // Find data starting byte
  if (!(start = strstr(start, _data_needle)))
    return AT_MALFORMED;
  start += sizeof(_data_needle) - 1;
  data[0] = atoi(start); // check Rule id
  if (data[0] == 0)
    return AT_MALFORMED;
  end = strchr(start, '"');
  if (!end) // incomplete string
    return AT_MALFORMED;
  nbr_bytes = (end - start);
  if (nbr_bytes & 1) // check parity
    return AT_MALFORMED;
  nbr_bytes /= 2; // hex str to bin
  if (nbr_bytes > *size)
    return AT_BUFF_TOO_SMALL;
  // Parse data
  for (uint16_t i = 0; i < nbr_bytes; ++i)
  {
    int8_t hb = hexchar_to_bin(start[i * 2]);
    int8_t lb = hexchar_to_bin(start[i * 2 + 1]);
    if (hb == -1 || lb == -1) // non-hex char
      return AT_MALFORMED;
    data[i] = (hb << 4) | lb;
  }
  *size = nbr_bytes;
  return AT_SUCCESS;
}

static at_error_t _recv(uint8_t *data, size_t *size)
{
  char *start = NULL;
  at_error_t err = AT_SUCCESS;

  if (_rx_buff)
    {
      err = _parse_downlink(strstr(_rx_buff, _data_needle), data, size);
      free(_rx_buff);
      _rx_buff = NULL;
      return err;
    }
    do
    {
      if (serial_readline(&_response_buff) == -1)
      {
        return AT_INTERNAL_ERR;
      }
      if (!start && strstr(_response_buff, _data_needle) &&
          (start = strstr(_response_buff, _data_needle)))
      {
        err = _parse_downlink(start, data, size);
      }
      if (strstr(_response_buff, "ERROR"))
      {
        print_at_error(at_error_raw_to_str(_response_buff));
        err = at_error_raw_to_enum(_response_buff);
        if (err == AT_UNKNOWN_ERR)
          break;
      }
    } while (!strstr(_response_buff, _data_needle));
    if (start == NULL)
      *size = 0;
    return err;
}

static int _poll(bool block)
{
  if (_rx_buff)
    return 1;
  return serial_poll(block);
}

static inline void _set_mtu(const at_shield_params_t *p)
{
  uint16_t max_mtu_size;
#ifdef MAX_MTU_SIZE
  max_mtu_size = MAX_MTU_SIZE;
#else
  max_mtu_size = 252;
// limitation of full-sdk, as the memory needs to be aligned on 4
#endif

  if (p->mtu && p->mtu < max_mtu_size)
    _mtu_size = p->mtu;
  else if (!_mtu_size)
    _mtu_size = max_mtu_size;

  // actual limitation of RisingHF :
  // ((526 - strlen(_cmd.send.fmt)) + 2) / 2;
  // 526 = Max number of characters in AT command
  // - strlen(_cmd.send.fmt) = the space that send command takes
  // + 2 = %s will not be part of the command
  // / 2 = from characters to bytes
  //  AT+TEST=TXLRPKT,"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
  //  == 254 bytes
}

static inline uint16_t _get_mtu(void)
{
  return _mtu_size;
}

static at_callbacks_t _callbacks =
  {
    .init = &_init,
    .fini = &_fini,
    .send = &_send,
    .recv = &_recv,
    .poll = &_poll,
    .mtu = &_get_mtu
  };

const at_callbacks_t *at_get_risinghf()
{
  return &_callbacks;
}

static at_error_t _cmd_exec_check(const char *cmd)
{
  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  if (serial_readline(&_response_buff) == -1)
    return AT_INTERNAL_ERR;
  if (strstr(_response_buff, "ERROR"))
  {
    print_at_error(at_error_raw_to_str(_response_buff));
    return AT_CMD_ERROR;
  }
  return AT_SUCCESS;
}

static at_error_t _cmd_send(const char *cmd)
{
  at_error_t ret_val = AT_SUCCESS;

  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
  {
    ret_val = AT_INTERNAL_ERR;
    goto activate_recv_return;
  }
  do
  {
    if (serial_readline(&_response_buff) == -1)
    {
      ret_val = AT_INTERNAL_ERR;
      goto activate_recv_return;
    }
    if (strstr(_response_buff, "ERROR"))
    {
      print_at_error(at_error_raw_to_str(_response_buff));
      ret_val = at_error_raw_to_enum(_response_buff);
      goto activate_recv_return;
    }
  } while (!strstr(_response_buff, _send_done_needle));
  // Read until rhf explicitly says send is done (thus omitting debug log if activated)
  // The number of read must be equal to the number of written lines by rhf
  //  otherwise read will block infinitely
activate_recv_return:
  // Ensure that device is listening again
  //  as it should always listen, except when sending
  _exec_cmd(&_cmd.activate_recv);
  return ret_val;
}