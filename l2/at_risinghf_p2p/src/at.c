/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Jourdan arthur.jourdan@ackl.io
 */

#include "at.h"
#include <errno.h>
#include <stdlib.h>
#include <string.h>

static const char *_errors[] = {
    "Success",
    "Internal error",
    "Unknown error",
    "AT command logged an error",
    "init(): Cannot init mode",
    "init(): Cannot init log",
    "init(): Cannot init configuration",
    "init(): Cannot activate receive",
    "send(): Sending buffer is too big",
    "recv(): Received message is malformed",
    "recv(): Receiving buffer is too small",
};

const char *at_strerror(at_error_t e)
{
  if (e < AT_SUCCESS || e > AT_BUFF_TOO_SMALL)
    return NULL;
  return _errors[e];
}

static const struct
{
  uint8_t code;
  const char *msg;
} at_error_raw_to_message[] = {
    {-1, "The number of parameters is invalid"},
    {-2, "The content of the parameter is invalid"},
    {-3, "API function returns error when user parameter is passed to it"},
    {-4, "LoRaWAN modem can't save parameter to EEPROM"},
    {-5, "The command is disabled currently"},
    {-6, "Unknown error occurs"},
    {-7, "There is not enough HEAP to execute user operation"},
    {-10, "Command unknown"},
    {-11, "Command is in wrong format"},
    {-12, "Command is unavailable in current mode(Check with \"AT+MODE\")"},
    {-20, "Too many parameters.LoRaWAN modem support max 15 parameters"},
    {-21, "Length of command is too long(exceed 528 bytes)"},
    {-22, "Receive end symbol timeout, command must end with<LF>"},
    {-23, "Invalid character received"},
    {-24, "Either \"Length of command is too long(exceed 528 bytes)\", "
          "\"Receive end symbol timeout\""
          " or "
          "\"Invalid character received\""},
    // taken from
    //  https://www.thethingsnetwork.org/forum/uploads/default/original/2X/9/9b9067781ac614cff3d2e25b669c084e09e47751.pdf
};

static const char *at_error_raw_code_to_str(uint8_t code)
{
  for (ushort i = 0;
       i < sizeof(at_error_raw_to_message) / sizeof(at_error_raw_to_message[0]);
       ++i)
  {
    if (code == at_error_raw_to_message[i].code)
    {
      return at_error_raw_to_message[i].msg;
    }
  }
  return NULL;
}

/**
 * @return 0 if not found
 */
static u_int8_t at_error_find_raw_code(const char *raw)
{
  const char error_needle[] = "ERROR(";
  const char error_closing_needle[] = ")";
  char *err_code_str;
  uint8_t err_code;
  char *tmp;

  if (!(tmp = strstr(raw, error_needle)))
    return 0;
  tmp += strlen(error_needle);
  err_code_str = tmp;
  if (!(tmp = strstr(tmp, error_closing_needle)))
    return 0;
  tmp[0] = '\0';
  errno = 0;
  err_code = strtol(err_code_str, NULL, 10);
  if (errno)
    return 0;
  return err_code;
}

static const struct
{
  uint8_t at_code;
  at_error_t enum_code;
} at_error_raw_code_to_enum[] = {
    {-6, AT_UNKNOWN_ERR},
    {-21, AT_SEND_TOO_BIG},
    {-24, AT_UNKNOWN_ERR},
};

at_error_t at_error_raw_to_enum(const char *raw)
{
  u_int8_t err_code = at_error_find_raw_code(raw);

  for (uint i = 0; i < sizeof(at_error_raw_code_to_enum) /
                           sizeof(at_error_raw_code_to_enum[0]);
       ++i)
  {
    if (err_code == at_error_raw_code_to_enum[i].at_code)
      return at_error_raw_code_to_enum[i].enum_code;
  }
  return AT_SUCCESS;
}

const char *at_error_raw_to_str(const char *raw)
{
  uint8_t err_code = at_error_find_raw_code(raw);

  if (!err_code)
    return NULL;
  return at_error_raw_code_to_str(err_code);
}