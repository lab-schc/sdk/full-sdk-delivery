/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Jourdan arthur.jourdan@ackl.io
 */

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <poll.h>
#include <errno.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include "serial.h"

#define MAX_LINE_SIZE 4096 // max line size from `man termios`

static int _fd = -1;
static char _line[MAX_LINE_SIZE];

int serial_get_fd(void)
{
  return _fd;
}

int serial_init(const char *path, speed_t baudrate)
{
  struct termios options;

  if (!path || !baudrate)
    return -1;
  if (_fd != -1)
    return -1;
  _line[0] = '\0';
  _fd = open(path, O_RDWR | O_NOCTTY | O_SYNC | O_DSYNC);
  if (_fd == -1) {
    if (errno == EACCES)
      fprintf(stderr, "Error: cannot open serial port due to bad permission."
	      "\nYou can try running it as root, or add yourself to the "
	      "'dialout' Linux group.\n");
    return -1;
  }
  if (!isatty(_fd)) {
    fprintf(stderr, "Error: \"%s\" is not a tty file\n", path);
    return -1;
  }
  if (ioctl(_fd, TIOCEXCL, NULL) < 0) {
    fprintf(stderr, "Error: Cannot get exclusivity on serial port\n");
    return -1;
  }
  cfsetispeed(&options, baudrate);
  cfsetospeed(&options, baudrate);
  if (tcgetattr(_fd, &options) == -1)
    return -1;
  options.c_cflag |= (CLOCAL | CREAD);
  options.c_lflag |= ICANON;
  if (tcsetattr(_fd, TCSANOW, &options) == -1)
    return -1;
  if (tcflush(_fd, TCIFLUSH) == -1)
    return -1;
  return 0;
}

void serial_fini(void)
{
  if (_fd == -1)
    return;
  close(_fd);
  _fd = -1;
}

static ssize_t _read_line(void)
{
  ssize_t ret = 0;

  if (_fd == -1)
  {
    return -1;
  }
  do
  {
    ret = read(_fd, _line, sizeof(_line));
    // debug
    fprintf(stderr, "\n%s\n", "Just read : \'");
    (void)write(2, _line, strlen(_line));
    fprintf(stderr, "\'\n");
    //

  } while (ret == -1 && errno == EINTR);
  if (ret <= 0 || (_line[ret - 1] != '\n' && _line[ret - 1] != '\r'))
  {
    fprintf(stderr, "Error: serial error or EOF received.\n");
    return -1;
  }
  if (ret == sizeof(_line))
  {
    fprintf(stderr, "Warning: Serial input line truncated.\n");
  }
  _line[ret - 1] = '\0'; // replace '\n' by '\0'
  return ret - 1;
}

// the number of serial_readline
// must be equal to the number of written lines by rhf
//  (as long as the line is smaller than the read buffer)
//  otherwise it blocks
ssize_t serial_readline(char **l) {
  ssize_t ret;

  if (!l)
    return -1;
  ret = strlen(_line);
  while (ret == 0) { // while we read empty line, retry
    if ((ret = _read_line()) == -1)
      return -1;
  }
  *l = realloc(*l, ret + 1);
  memcpy(*l, _line, ret + 1);
  _line[0] = '\0';
  return ret;
}

ssize_t serial_writeline(const char *l)
{
  ssize_t ret;

  if (_fd == -1 || !l)
    return -1;
  do
  {
    // debug
    fprintf(stderr, "\n%s\n", "About to write : \'");
    (void)write(2, l, strlen(l));
    fprintf(stderr, "\'\n");
    //
    ret = write(_fd, l, strlen(l));
  } while (ret == -1 && errno == EINTR);
  if (tcdrain(_fd) == -1)
    fprintf(stderr, "Warning: tcdrain() on serial failed\n");
  return ret;
}

int serial_poll(bool block)
{
  if (block && !_line[0] && _read_line() == -1)
    return -1; // _read_line() failed
  if (_line[0])
    return 1; // line is available
  return 0; // nothing to read
}