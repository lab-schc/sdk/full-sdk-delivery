/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Jourdan arthur.jourdan@ackl.io
 *
 * Rising HF module L2 layer implementation.
 */

#include <string.h>

#include "at_risinghf.h"
#include "fullsdkl2.h"

// Set vid pid before compilation when testing
//  or if using SDKTunApp, set in examples/SDKTunApp/config/<config_file>.json
// obtained through lsusb format: "ffff:ffff"
uint8_t vid_pid[VIDPID_LEN] = "0483:5740";

/**
 * @var _shield_params
 * @brief parameters used during l2 processing
 *  this is used in fullsdkl2.c to change the data rate aka spread factor
 */
extern at_shield_params_t _shield_params;

static const struct
{
  const uint8_t dr;
  const uint8_t sf;
} conversion_dr_sf[] = {
    {0, 12}, {1, 11}, {2, 10}, {3, 9}, {4, 8}, {5, 7}, {6, 7},
};

l2_status_t l2_get_class(char *class)
{
  // Useless as it is not LoRaWAN but only LoRa point to point
  *class = 'A';
  return L2_SUCCESS;
}

l2_status_t l2_set_class(char class)
{
  // Useless as it is not LoRaWAN but only LoRa point to point

  // NOP
  (void)class;
  return L2_SUCCESS;
}

l2_status_t l2_get_dutycycle(bool *value)
{
  // Useless as it is not LoRaWAN but only LoRa point to point

  // NOP
  (void)value;
  return L2_SUCCESS;
}

l2_status_t l2_set_dutycycle(bool dutycycle_on)
{
  // Useless as it is not LoRaWAN but only LoRa point to point

  // NOP
  (void)dutycycle_on;

  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_dr(uint8_t dr)
{
  for (uint8_t i = 0;
       i < sizeof(conversion_dr_sf) / sizeof(conversion_dr_sf[0]); ++i)
  {
    if (conversion_dr_sf[i].dr == dr)
    {
      _shield_params.spreadfactor = conversion_dr_sf[i].sf;
      at_get_risinghf()->init(&_shield_params);
      return L2_SUCCESS;
    }
  }
  return L2_ERROR;
}

l2_status_t l2_get_dr(uint8_t *dr)
{
  for (uint8_t i = 0;
       i < sizeof(conversion_dr_sf) / sizeof(conversion_dr_sf[0]); ++i)
  {
    if (conversion_dr_sf[i].sf == _shield_params.spreadfactor)
    {
      *dr = conversion_dr_sf[i].dr;
      return L2_SUCCESS;
    }
  }
  return L2_ERROR;
}

l2_status_t l2_set_adr(bool adr)
{
  // Useless as it is not LoRaWAN but only LoRa point to point

  // NOP
  (void)adr;

  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_cert_mode(void)
{
  // NOP
  return L2_NOT_SUPPORTED_ERR;
}

void l2_get_region(int8_t *region)
{
  // NOP
  (void)region;
}

bool l2_set_otaa(bool otaa_on)
{
  // Useless as it is not LoRaWAN but only LoRa point to point

  (void)otaa_on;
  return false;
}

void l2_get_otaa(bool *otaa_on)
{
  // Useless as it is not LoRaWAN but only LoRa point to point

  // NOP
  (void)otaa_on;
}

bool l2_set_conf_mode(uint8_t conf_mode_on)
{
  // Useless as it is not LoRaWAN but only LoRa point to point

  (void)conf_mode_on;
  return false;
}

void l2_set_technology(l2a_technology_t technology)
{
  l2a_technology = technology;
}

void l2_set_vidpid(const uint8_t *vidpid)
{
  memcpy(vid_pid, vidpid, VIDPID_LEN);
}