/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Jourdan arthur.jourdan@ackl.io
 */

#include "fullsdkl2a.h"
#include "at_risinghf.h"
#include "fullsdkl2.h"
#include "lora_duty_cycle.h"
#include "platform.h"
#include "serial.h"
#include "tools.h"
#include <stdbool.h>
#include <stdlib.h>

// see at.h for information about these variables
at_shield_params_t _shield_params = {
    .vid_pid = vid_pid,
    .baudrate = B9600,
    .frequency = 433.0f,
    .spreadfactor = 7,
    .bandwidth = 250,
    .tx_preamble_len = 8,
    .rx_preamble_len = 8,
    .tx_power = 1,
    .cyclic_redundancy_check = true,
    .IQ = false,
    .NET = false,
    .log_mode = "QUIET",
    .mtu = 252,
}; // AT+TEST=RFCFG,433,7,250,8,8,1,ON,OFF,OFF

static l2a_callbacks_t l2a_cb;
static uint8_t *l2a_rx_buffer;
static uint16_t l2a_rx_buffer_size;
static uint16_t l2a_mtu;
static l2a_status_t l2a_result_code;

// This global variable must be defined in L2A layer and initialized
// to L2A_DEFAULT. Its use is reserved.
l2a_technology_t l2a_technology = L2A_DEFAULT;

// Data size of previous transmission request, in bytes.
static uint16_t cached_data_size = 0;

static enum {
  NO_EVENT = 0,
  CONNECTIVITY_AVAILABLE_EVENT = 1 << 0,
  TRANSMISSION_RESULT_EVENT = 1 << 1,
  DOWNLINK_AVAILABLE_EVENT = 1 << 2,
} event;

static void _poll_downlink(bool block)
{
  int ret = at_get_risinghf()->poll(block);
  if (ret == -1)
    printf("AT Error: poll() failed");
  else if (ret == 1)
  {
    event |= DOWNLINK_AVAILABLE_EVENT;
    l2a_cb.processing_required();
  }
}

static void _downlink_available_callback(void)
{
  _poll_downlink(true);
}

static inline l2a_status_t check_callbacks()
{
  if (!l2a_cb.processing_required || !l2a_cb.transmission_result ||
      !l2a_cb.data_received || !l2a_cb.connectivity_available)
    return L2A_REQ_CALLBACK_ERR;
  return L2A_SUCCESS;
}

l2a_status_t l2a_initialize(const l2a_callbacks_t *pp_callbacks,
                            uint8_t *p_receive_buffer,
                            uint16_t receive_buffer_size)
{
  at_error_t err;

  l2a_cb = *pp_callbacks;
  l2a_rx_buffer = p_receive_buffer;
  l2a_rx_buffer_size = receive_buffer_size;

  if (check_callbacks() != L2A_SUCCESS)
    return L2A_REQ_CALLBACK_ERR;
  if (!l2a_rx_buffer)
    return L2A_BUFFER_ERR;

  event = NO_EVENT;

  if ((err = at_get_risinghf()->init(&_shield_params)) != AT_SUCCESS)
  {
    fprintf(stderr, "AT Error in init(): %s\n", at_strerror(err));
    return L2A_CONNECT_ERR;
  }
  if ((l2a_mtu = at_get_risinghf()->mtu()) == 0)
  {
    fprintf(stderr, "AT Error: Cannot retrieve MTU\n");
    return L2A_CONNECT_ERR;
  }

  event |= CONNECTIVITY_AVAILABLE_EVENT;
  l2a_cb.processing_required();

  if (!watch_fd_for_input(serial_get_fd(), &_downlink_available_callback))
  {
    fprintf(stderr, "Error: watch_fd_for_input() failed\n");
    return L2A_CONNECT_ERR;
  }

  // as we dont have a proper way to close connection
  atexit(at_get_risinghf()->fini);

  return L2A_SUCCESS;
}

l2a_status_t l2a_send_data(const uint8_t *p_data, uint16_t data_size)
{
  at_error_t err;

  printf("\nl2a_send_data() called");
  PRINT_HEX_BUF((uint8_t *)p_data, data_size);

  cached_data_size = data_size;

  if (check_callbacks() != L2A_SUCCESS)
    return L2A_REQ_CALLBACK_ERR;

  if ((err = at_get_risinghf()->send(p_data, data_size)) != AT_SUCCESS)
  {
    fprintf(stderr, "AT Error in send(): %s\n", at_strerror(err));
    switch (err)
    {
      case AT_INTERNAL_ERR:
        return L2A_INTERNAL_ERR;
      case AT_SEND_TOO_BIG:
        l2a_result_code = L2A_BUFFER_ERR;
        return L2A_SUCCESS;
      default:
        return L2A_SUCCESS;
    }
  }
  event |= TRANSMISSION_RESULT_EVENT;
  l2a_cb.processing_required();
  return L2A_SUCCESS;
}

l2a_technology_t l2a_get_technology(void)
{
  // TODO(flavien): we have to return L2A_LORA when the profile
  //  will be defined and tested in fullSDK.
  return L2A_DEFAULT;
}

uint16_t l2a_get_mtu(void)
{
  return l2a_mtu;
}

/**
 * @return value in milliseconds
 */
uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  if (!data_size)
    data_size = cached_data_size;
  if (_shield_params.tx_power <= 1)
    // todo(arthur) check power limits for dutycycle
    return 1; // 1ms equals instantaneous
  struct lora_time_on_air toa = {
      .payload_size = data_size,
      .sf = _shield_params.spreadfactor,
      .preamble_len = _shield_params.tx_preamble_len,
      .bw = _shield_params.bandwidth,
      .header = false,
      .low_DR_optimize = false,
      .cr = 1,
  };
  uint32_t time_on_air = get_lora_time_on_air(&toa);
  struct lora_duty_cycle l_dc = {.frequency = _shield_params.frequency,
                                 .bw = _shield_params.bandwidth,
                                 .tx_power = _shield_params.tx_power};
  const float duty_cycle_multiplier = get_lora_duty_cycle_multiplier(&l_dc);
  uint32_t time_to_wait = time_on_air * duty_cycle_multiplier;

  fprintf(stderr, "%i milliseconds before sending to respect dutycycle\n",
          time_to_wait);
  return time_to_wait;
}

static void _read_downlink(void)
{
  size_t size = l2a_rx_buffer_size;
  at_error_t err;

  if ((err = at_get_risinghf()->recv(l2a_rx_buffer, &size)) != AT_SUCCESS)
  {
    fprintf(stderr, "AT Error in recv(): %s", at_strerror(err));
    switch (err)
    {
      case AT_BUFF_TOO_SMALL:
        l2a_cb.data_received(0, L2A_BUFFER_ERR);
        break;
      case AT_UNKNOWN_ERR: // Ignore these errors as they are not fatal
        l2a_cb.data_received(0, L2A_SUCCESS);
        break;
      default:
        l2a_cb.data_received(0, L2A_L2_ERROR);
    }
  }
  else if (size != 0)
  {
    printf("\nRX packet received");
    PRINT_HEX_BUF(l2a_rx_buffer, size);
    l2a_cb.data_received(size, L2A_SUCCESS);
  }
}

l2a_status_t l2a_process(void)
{
  if (event & CONNECTIVITY_AVAILABLE_EVENT)
    l2a_cb.connectivity_available();
  if (event & TRANSMISSION_RESULT_EVENT)
    l2a_cb.transmission_result(l2a_result_code, 0);
  if (event & DOWNLINK_AVAILABLE_EVENT) {
    _read_downlink();
  }
  event = NO_EVENT;
  _poll_downlink(false); // poll for any remaining buffered line
  return L2A_SUCCESS;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  (void)dev_iid;

  return false;
}