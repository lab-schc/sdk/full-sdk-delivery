/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Jourdan arthur.jourdan@ackl.io
 */

#ifndef FULL_SDK_DELIVERY_LORA_DUTY_CYCLE_H
#define FULL_SDK_DELIVERY_LORA_DUTY_CYCLE_H

#include <stdbool.h>
#include <stdint.h>

/**
 * LoRa duty cycle imperatives depending on frequency, bandwidth and tx power
 * from http://iot-strasbourg.strataggem.com/ref/duty-cycle.html
 */
struct lora_duty_cycle
{
  /**
   * @var frequency
   * in MHz
   * The frequency over which operates the LoRa communication
   * It is const as it is only used as an indicator for
   *  get_lora_duty_cycle_percent()
   */
  const float frequency;
  /**
   * @var bw
   * in kHz
   * The bandwidth over which operates the LoRa communication
   * It not const as it is only used as an indicator for
   *  get_lora_duty_cycle_percent()
   */
  const uint16_t bw;
  /**
   * @var tx_power
   * in dBm
   * The power at which LoRa communication is sent
   * It is const as it is only used as an indicator for
   *  get_lora_duty_cycle_percent()
   */
  const uint8_t tx_power;
  /**
   * @var dc
   *  Duty cycle
   *  in percent such as : dc = 5, dc is 5%
   * It is not const as it will be changed by get_lora_duty_cycle_percent()
   */
  float dc;
};

/**
 * @brief Get a duty cycle, that corresponds to a bandwidth, frequency and tx
 *  power according to LoRa regulations
 *
 * @param input frequency, bandwidth and tx_power
 *  to which could correspond a duty cycle
 *
 * @return true if duty cycle corresponds to input,
 *  the duty cycle is set in input->dc.
 *  false otherwise, default value of 1% is set in input->dc
 */
bool get_lora_duty_cycle_percent(struct lora_duty_cycle *input);

/**
 * @brief Get a duty cycle multiplier, that corresponds to a bandwidth,
 *  frequency and tx power according to LoRa regulations The multiplier is to be
 *  used such as return_value * time_on_air = wait_time_until_next_transmission
 *  in order get the time to wait for next tx (transmission)
 *
 * @param input frequency, bandwidth and tx_power
 *  to which could correspond a duty cycle
 *
 * @return The multiplier with which to compute Time on Air with
 */
float get_lora_duty_cycle_multiplier(struct lora_duty_cycle *input);

struct lora_time_on_air
{
  /**
   * @var payload_size
   * The number of bytes sent in last transmission
   */
  const uint16_t payload_size;
  /**
   * @var sf
   *  spreadfactor (7, 8, 9, 10, 11 or 12)
   */
  const uint8_t sf;
  /**
   * @var preamble_len
   *
   */
  const uint8_t preamble_len;
  /**
   * @var bw
   *  bandwidth, usually 125 or 250
   */
  const uint16_t bw;
  /**
   * @var header
   *  false implicit, true explicit
   */
  const bool header;
  /**
   * @var low_DR_optimize
   * false disabled, true enabled
   */
  const bool low_DR_optimize;
  /**
   * @var cr
   *  Code rate or encoding rate (1, 2, 3, 4),
   *   default 1 (will produce 4/5 encoding rate,
   *   because will be computed as (4/(4+cr)))
   */
  const uint8_t cr;
};

/**
 * @brief Get the LoRaWAN time on air
 *  according to payload size and transmission configuration
 *
 * Links
 *  https://www.rfwireless-world.com/calculators/LoRaWAN-Airtime-calculator.html
 *  https://loratools.nl/#/airtime
 *  https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjYxf6C2uv3AhUExoUKHSA3A1EQFnoECC8QAQ&url=https%3A%2F%2Fzenodo.org%2Frecord%2F1698621%2Ffiles%2FLoRa%2528WAN%2529%2520airtime%2520calculator.xlsx%3Fdownload%3D1&usg=AOvVaw0uyYXWlf8Pfvt2ZsIhbILs
 *  https://www.thethingsnetwork.org/airtime-calculator
 *
 * @param input setup with which the data has been / will be sent
 *
 * @return value in milliseconds
 *  negative value if error
 */
float get_lora_time_on_air(struct lora_time_on_air *input);

#endif // FULL_SDK_DELIVERY_LORA_DUTY_CYCLE_H