/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Jourdan arthur.jourdan@ackl.io
 */

#ifndef FULL_SDK_DELIVERY_L2_TOOLS_H
#define FULL_SDK_DELIVERY_L2_TOOLS_H

#include <stdlib.h>

int8_t hexchar_to_bin(char c);

#endif // FULL_SDK_DELIVERY_L2_TOOLS_H