/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Jourdan arthur.jourdan@ackl.io
 */

#include "../inc/lora_duty_cycle.h"
#include <math.h>

static const struct lora_duty_cycle dc_rules[] = {
    {868.1f, 125, 14, 1.f},    {868.3f, 125, 14, 1.f},
    {868.5f, 125, 14, 1.f},    {868.8f, 125, 14, 0.1f},
    {869.525f, 125, 27, 10.f}, {869.525f, 250, 27, 10.f},
    {869.775f, 125, 14, 1.f},  {869.925f, 125, 14, 1.f},
    {869.775f, 125, 7, 100.f}, {869.925f, 125, 7, 100.f},
};

bool get_lora_duty_cycle_percent(struct lora_duty_cycle *input)
{
  for (uint8_t i = 0; i < sizeof(dc_rules) / sizeof(dc_rules[0]); ++i)
  {
    if (input->frequency == dc_rules[i].frequency &&
        input->bw == dc_rules[i].bw && input->tx_power == dc_rules[i].tx_power)
    {
      input->dc = dc_rules[i].dc;
      return true; // notice that correspondence has been found
    }
  }
  input->dc = 1.f; // 1% by default
  return false; // notice that no correspondence has been found, so give default
}

float get_lora_duty_cycle_multiplier(struct lora_duty_cycle *input)
{
  get_lora_duty_cycle_percent(input);

  return ((100 - input->dc) / input->dc);
}

float get_lora_time_on_air(struct lora_time_on_air *input)
{
  if (!input->cr || !input->sf || !input->preamble_len || !input->bw)
    return -1;

  const float symbol_time = pow(2, input->sf) / input->bw;
  const float preamble_airtime = (input->preamble_len + 4.25f) * symbol_time;
  const float tmp_pl_symb_nb =
      ceil(((8 * input->payload_size) - (4 * input->sf) + 28 + 16 -
            (20 * input->header)) /
           (4 * (input->sf - (2 * input->low_DR_optimize)))) *
      (input->cr + 4);
  const float payload_symb_nb = 8 + (tmp_pl_symb_nb > 0 ? tmp_pl_symb_nb : 0);
  const float payload_airtime = payload_symb_nb * symbol_time;
  const float packet_airtime = preamble_airtime + payload_airtime;

  return packet_airtime;
}