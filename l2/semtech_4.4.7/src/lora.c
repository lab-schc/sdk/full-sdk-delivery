/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * Semtech L2 Lora binding layer.
 */

#include "lora.h"
#include "Commissioning.h"
#include "mac_strings.h"
#include "platform.h"

uint8_t dev_eui[] = LORAWAN_DEVICE_EUI;
uint8_t join_eui[] = LORAWAN_JOIN_EUI;
uint8_t app_key[] = LORAWAN_APP_KEY;
bool redirect_next_transmission_result = false;
void (*transmission_result_redirection)(bool has_error);

#if (OVER_THE_AIR_ACTIVATION == 0)
static uint32_t dev_addr = LORAWAN_DEVICE_ADDRESS;
#endif

static lora_callbacks_t callbacks;

bool lora_init(lora_callbacks_t *lora_cb)
{
  bool error = false;

  if (lora_cb == NULL || lora_cb->join == NULL || lora_cb->tx_needed == NULL ||
      lora_cb->link_check == NULL || lora_cb->frame_pending == NULL ||
      lora_cb->data_received == NULL || lora_cb->transmission_result == NULL)
  {
    error = true;
  }

  callbacks = *lora_cb;

  lora_set_dev_eui(dev_eui);
  lora_set_join_eui(join_eui);
  lora_set_app_key(app_key);
  l2_set_class('A');
  l2_set_adr(false);

  return error;
}

l2_status_t l2_join(uint8_t join_dr, uint32_t *duty_cycle_wait_time)
{
  l2_op_status_t status = LORAMAC_STATUS_ERROR;
  l2_network_activation_type_t activation_type;
  l2_status_t st = L2_ERROR;

#if (OVER_THE_AIR_ACTIVATION == 0)
  l2_set_lorawan_version();
  l2_set_network_id(LORAWAN_NETWORK_ID);
#if (STATIC_DEVICE_ADDRESS != 1)
  srand1(BoardGetRandomSeed());
  dev_addr = randr(0, 0x01FFFFFF);
#endif
  l2_set_dev_addr(dev_addr);
#endif

  LoRaMacStart();

  if (l2_get_network_activation(&activation_type) == L2_SUCCESS)
  {
    if (activation_type == L2_NETWORK_ACTIVATION_TYPE_NONE ||
        activation_type == L2_NETWORK_ACTIVATION_TYPE_OTAA)
    {
#if (OVER_THE_AIR_ACTIVATION == 0)
      st = l2_set_network_activation(L2_NETWORK_ACTIVATION_TYPE_ABP);
#else
      status = lora_join(join_dr, duty_cycle_wait_time);
      if (status != LORAMAC_STATUS_OK)
      {
        if (status == LORAMAC_STATUS_DUTYCYCLE_RESTRICTED)
        {
          PRINT_DBG("mac> duty cycle restriction ends in %lu ms\n",
                    *duty_cycle_wait_time);
        }
        else
        {
          PRINT_DBG("mac> unexpected join request status: %s\n",
                    lora_get_mac_status_string(status));
        }
      }
      else
      {
        st = L2_SUCCESS;
      }
#endif
    }
    else
    {
      PRINT_DBG("mac> unexpected activation type: %d\n", activation_type);
    }
  }
  else
  {
    PRINT_DBG("mac> network activation failed: %s\n",
              lora_get_mac_status_string(status));
  }

  return st;
}

l2_status_t l2_send(l2_app_data_t app_data, uint8_t datarate,
                    uint32_t *duty_cycle_wait_time)
{
  l2_op_status_t status;

  status = lora_send(app_data, datarate, duty_cycle_wait_time);

  if (status != LORAMAC_STATUS_OK)
  {
    if (status == LORAMAC_STATUS_DUTYCYCLE_RESTRICTED)
    {
      PRINT_DBG("mac> duty cycle restriction ends in %lu ms\n",
                *duty_cycle_wait_time);
    }
    else
    {
      PRINT_DBG("mac> unexpected send request status: %s\n",
                lora_get_mac_status_string(status));
    }

    return L2_ERROR;
  }

  return L2_SUCCESS;
}

void lora_mlme_indication(MlmeIndication_t *mlme_indication)
{
  if (mlme_indication->Status != LORAMAC_EVENT_INFO_STATUS_BEACON_LOCKED)
  {
    PRINT_DBG("mac> beacon not locked: %s\n",
              lora_get_event_info_status_string(mlme_indication->Status));
  }

  if (mlme_indication->Status != LORAMAC_EVENT_INFO_STATUS_OK)
  {
    PRINT_DBG("mac> mlme indication status not ok: %s\n",
              lora_get_event_info_status_string(mlme_indication->Status));
  }

  switch (mlme_indication->MlmeIndication)
  {
    case MLME_SCHEDULE_UPLINK:
    {
      callbacks.tx_needed();
      break;
    }
    default:
      break;
  }
}

void lora_mlme_confirm(MlmeConfirm_t *mlme_confirm)
{
  if (mlme_confirm->Status != LORAMAC_EVENT_INFO_STATUS_OK)
  {
    PRINT_DBG("mac> mlme confirm status not ok: %s\n",
              lora_get_event_info_status_string(mlme_confirm->Status));
  }

  switch (mlme_confirm->MlmeRequest)
  {
    case MLME_JOIN:
    {
      if (mlme_confirm->Status == LORAMAC_EVENT_INFO_STATUS_OK)
      {
        callbacks.join(true);
      }
      else
      {
        callbacks.join(false);
      }
      break;
    }
    case MLME_LINK_CHECK:
    {
      if (mlme_confirm->Status == LORAMAC_EVENT_INFO_STATUS_OK)
      {
        callbacks.link_check();
      }
      break;
    }
    default:
      break;
  }
}

void lora_mcps_indication(McpsIndication_t *mcps_indication)
{
  if (mcps_indication->Status != LORAMAC_EVENT_INFO_STATUS_OK)
  {
    PRINT_DBG("mac> mcps indication status not ok: %s\n",
              lora_get_event_info_status_string(mcps_indication->Status));
    return;
  }

  switch (mcps_indication->McpsIndication)
  {
    case MCPS_UNCONFIRMED:
    {
      break;
    }
    case MCPS_CONFIRMED:
    {
      break;
    }
    case MCPS_PROPRIETARY:
    {
      break;
    }
    case MCPS_MULTICAST:
    {
      break;
    }
    default:
      break;
  }

  // Check Multicast
  // Check FramePending
  if (mcps_indication->FramePending == true)
  {
    callbacks.frame_pending();
  }

  if (mcps_indication->RxData == true)
  {
    callbacks.data_received(mcps_indication->Port, mcps_indication->Buffer,
                            mcps_indication->BufferSize,
                            mcps_indication->DownLinkCounter,
                            lora_get_slot_string(mcps_indication->RxSlot),
                            mcps_indication->RxDatarate, mcps_indication->Rssi,
                            mcps_indication->Snr);
  }
}

void lora_mcps_confirm(McpsConfirm_t *mcps_confirm)
{
  bool has_error;

  if (mcps_confirm->Status != LORAMAC_EVENT_INFO_STATUS_OK)
  {
    PRINT_DBG("mac> mcps confirm status not ok: %s\n",
              lora_get_event_info_status_string(mcps_confirm->Status));
    has_error = true;
  }
  else
  {
    switch (mcps_confirm->McpsRequest)
    {
      case MCPS_UNCONFIRMED:
        has_error = false;
        break;
      case MCPS_CONFIRMED:
        has_error = false;
        break;
      default:
        has_error = true;
        break;
    }
  }

  if (redirect_next_transmission_result &&
      transmission_result_redirection != NULL)
  {
    redirect_next_transmission_result = false;
    transmission_result_redirection(has_error);
    return;
  }

  callbacks.transmission_result(
      has_error, mcps_confirm->UpLinkCounter, mcps_confirm->Channel,
      mcps_confirm->Datarate, mcps_confirm->TxPower, mcps_confirm->AckReceived,
      mcps_confirm->NbRetries);
}

uint8_t *lora_get_dev_eui(void)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_DEV_EUI;
  LoRaMacMibGetRequestConfirm(&mib_req);

  return mib_req.Param.DevEui;
}

void lora_set_dev_eui(uint8_t *value)
{
  // To prevent Unused Parametre warning.
  (void)(value);
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_DEV_EUI;
  mib_req.Param.DevEui = dev_eui;
  LoRaMacMibSetRequestConfirm(&mib_req);
}

uint8_t *lora_get_join_eui(void)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_JOIN_EUI;
  LoRaMacMibGetRequestConfirm(&mib_req);

  return mib_req.Param.JoinEui;
}

void lora_set_join_eui(uint8_t *value)
{
  // To prevent Unused Parametre warning.
  (void)(value);
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_JOIN_EUI;
  mib_req.Param.JoinEui = join_eui;
  LoRaMacMibSetRequestConfirm(&mib_req);
}

uint8_t *lora_get_app_key(void)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_APP_KEY;
  LoRaMacMibGetRequestConfirm(&mib_req);

  return mib_req.Param.AppKey;
}

void lora_set_app_key(uint8_t *value)
{
  // To prevent Unused Parametre warning.
  (void)(value);
  MibRequestConfirm_t mib_req;

  // See
  // full-sdk-delivery/libs/loramac-node/src/peripherals/soft-se/se-identity.h
  mib_req.Type = MIB_NWK_KEY;
  mib_req.Param.AppKey = app_key;
  LoRaMacMibSetRequestConfirm(&mib_req);
}
