/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * Semtech L2 configuration layer.
 */

#include "fullsdkl2.h"
#include "Commissioning.h"
#include "LoRaMac.h"
#include "LoRaMacTest.h"
#include "LoRaMacTypes.h"
#include "cmac.h"
#include "lora.h"
#include "mac_strings.h"
#include "secure-element.h"

SecureElementStatus_t GetKeyByID(KeyIdentifier_t key_id, Key_t **key);

static l2_region_id_t active_region = LORAMAC_REGION_EU868;
static uint8_t default_datarate = DR_3;
static uint8_t l2_default_message_type = L2_MSG_TYPE_UNCONFIRMED;
static bool dutycycle_state = true;

/*!
 * Available regions
 */
l2_region_t regions[L2_MAX_SUPPORTED_REGION] = {
    {.str = "AS923", .id = LORAMAC_REGION_AS923},
    {.str = "AU915", .id = LORAMAC_REGION_AU915},
    {.str = "CN470", .id = LORAMAC_REGION_CN470},
    {.str = "CN779", .id = LORAMAC_REGION_CN779},
    {.str = "EU433", .id = LORAMAC_REGION_EU433},
    {.str = "EU868", .id = LORAMAC_REGION_EU868},
    {.str = "KR920", .id = LORAMAC_REGION_KR920},
    {.str = "IN865", .id = LORAMAC_REGION_IN865},
    {.str = "US915", .id = LORAMAC_REGION_US915},
    {.str = "RU864", .id = LORAMAC_REGION_RU864}};

const char *l2_get_version(void)
{
  // TODO return semtech version;
  return "Semtech 4.4.7";
}

l2_status_t l2_init(void)
{
  // nothing to do for Semtech stack
  return L2_SUCCESS;
}

l2_msg_type_t l2_get_message_type(void)
{
  return l2_default_message_type;
}

void l2_set_message_type(l2_msg_type_t message_type)
{
  l2_default_message_type = message_type;
}

uint8_t l2_get_default_datarate(void)
{
  return default_datarate;
}

void l2_set_default_datarate(uint8_t datarate)
{
  default_datarate = datarate;
}

l2_status_t l2_get_active_region(l2_region_id_t *region)
{
  *region = active_region;

  return L2_SUCCESS;
}

l2_status_t l2_set_active_region(l2_region_id_t region)
{
  active_region = region;

  return L2_SUCCESS;
}

l2_status_t l2_get_class(char *class)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_DEVICE_CLASS;
  LoRaMacMibGetRequestConfirm(&mib_req);

  switch (mib_req.Param.Class)
  {
    case CLASS_A:
      *class = 'A';
      break;
    case CLASS_C:
      *class = 'C';
      break;
    default:
      *class = 'B';
      break;
  }

  return L2_SUCCESS;
}

l2_status_t l2_set_class(char class)
{
  MibRequestConfirm_t mib_req;

  switch (class)
  {
    case 'A':
      mib_req.Param.Class = CLASS_A;
      break;
    case 'C':
      mib_req.Param.Class = CLASS_C;
      break;
    case 'B':
      mib_req.Param.Class = CLASS_B;
      break;
    default:
      return L2_ERROR;
  }

  mib_req.Type = MIB_DEVICE_CLASS;
  if (LoRaMacMibSetRequestConfirm(&mib_req) != LORAMAC_STATUS_OK)
  {
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_system_max_rx_error(uint32_t *value)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_SYSTEM_MAX_RX_ERROR;
  if (LoRaMacMibGetRequestConfirm(&mib_req) != LORAMAC_STATUS_OK)
  {
    return L2_ERROR;
  }

  *value = mib_req.Param.SystemMaxRxError;

  return L2_SUCCESS;
}

l2_status_t l2_set_system_max_rx_error(uint32_t sys_max_rx_error)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_SYSTEM_MAX_RX_ERROR;
  mib_req.Param.SystemMaxRxError = sys_max_rx_error;
  if (LoRaMacMibSetRequestConfirm(&mib_req) != LORAMAC_STATUS_OK)
  {
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

uint32_t l2_get_uplink_frequency(uint32_t channel)
{
  MibRequestConfirm_t mib_req;
  uint32_t frequency = 0;

  mib_req.Type = MIB_CHANNELS;
  if (LoRaMacMibGetRequestConfirm(&mib_req) == LORAMAC_STATUS_OK)
  {
    frequency = mib_req.Param.ChannelList[channel].Frequency;
  }

  return frequency;
}

void l2_get_channels_mask(uint16_t *channels_mask, uint8_t *channels_mask_size)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_CHANNELS_MASK;
  if (LoRaMacMibGetRequestConfirm(&mib_req) == LORAMAC_STATUS_OK)
  {
#if defined(REGION_AS923) || defined(REGION_CN779) || defined(REGION_EU868) || \
    defined(REGION_IN865) || defined(REGION_KR920) || defined(REGION_EU433) || \
    defined(REGION_RU864)
    *channels_mask_size = 1;
#elif defined(REGION_AU915) || defined(REGION_US915) || defined(REGION_CN470)
    *channels_mask_size = 5;
#else
#error "Please define a region in the compiler options."
#endif
    for (uint8_t i = 0; i < *channels_mask_size; i++)
    {
      channels_mask[i] = mib_req.Param.ChannelsMask[i];
    }
  }
}

uint8_t *l2_get_dev_eui(void)
{
  return dev_eui;
}

l2_status_t l2_set_dev_eui(uint8_t *value)
{
  memcpy1(dev_eui, value, 8);
}

uint8_t *l2_get_join_eui(void)
{
  return join_eui;
}

l2_status_t l2_set_join_eui(uint8_t *value)
{
  memcpy1(join_eui, value, 8);

  return L2_SUCCESS;
}

l2_status_t l2_get_nwk_s_key(uint8_t *nwk_s_key)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_NWK_S_ENC_KEY;
  LoRaMacMibGetRequestConfirm(&mib_req);

  memcpy1(nwk_s_key, mib_req.Param.NwkSEncKey, 16);

  return L2_SUCCESS;
}

l2_status_t l2_set_nwk_s_key(uint8_t *nwk_s_key)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_NWK_S_ENC_KEY;
  mib_req.Param.NwkSEncKey = nwk_s_key;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

uint8_t *l2_get_app_key(void)
{
  return app_key;
}

l2_status_t l2_set_app_key(uint8_t *value)
{
  memcpy1(app_key, value, 16);

  return L2_SUCCESS;
}

l2_status_t l2_get_app_s_key(uint8_t *value)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_APP_S_KEY;
  LoRaMacMibGetRequestConfirm(&mib_req);

  memcpy1(value, mib_req.Param.AppSKey, 16);

  return L2_SUCCESS;
}

l2_status_t l2_set_app_s_key(uint8_t *app_s_key)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_APP_S_KEY;
  mib_req.Param.AppSKey = app_s_key;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

uint8_t *l2_get_se_pin(void)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_SE_PIN;
  LoRaMacMibGetRequestConfirm(&mib_req);

  return mib_req.Param.SePin;
}

l2_status_t l2_get_dr(uint8_t *dr)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_CHANNELS_DATARATE;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *dr = (uint8_t)mib_req.Param.ChannelsDatarate;

  return L2_SUCCESS;
}

l2_status_t l2_set_dr(uint8_t dr)
{
  MibRequestConfirm_t mib_req;

  l2_set_default_datarate(dr);

  mib_req.Type = MIB_CHANNELS_DATARATE;
  mib_req.Param.ChannelsDatarate = dr;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

void l2_set_lorawan_version(void)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_ABP_LORAWAN_VERSION;
  mib_req.Param.AbpLrWanVersion.Value = ABP_ACTIVATION_LRWAN_VERSION;
  LoRaMacMibSetRequestConfirm(&mib_req);
}

l2_status_t l2_get_dev_addr(uint32_t *value)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_DEV_ADDR;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *value = mib_req.Param.DevAddr;

  return L2_SUCCESS;
}

l2_status_t l2_set_dev_addr(uint32_t dev_addr)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_DEV_ADDR;
  mib_req.Param.DevAddr = dev_addr;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

void l2_enable_public_network(void)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_PUBLIC_NETWORK;
  mib_req.Param.EnablePublicNetwork = LORAWAN_PUBLIC_NETWORK;
  LoRaMacMibSetRequestConfirm(&mib_req);
}

l2_status_t l2_get_adr(bool *adr)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_ADR;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *adr = mib_req.Param.AdrEnable;

  return L2_SUCCESS;
}

l2_status_t l2_set_adr(bool adr)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_ADR;
  mib_req.Param.AdrEnable = adr;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

l2_status_t l2_get_dutycycle(bool *value)
{
  *value = dutycycle_state;

  return L2_SUCCESS;
}

l2_status_t l2_set_dutycycle(bool dutycycle_on)
{
  dutycycle_state = dutycycle_on;
  LoRaMacTestSetDutyCycleOn(dutycycle_on);

  return L2_SUCCESS;
}

uint16_t l2_get_mtu(void)
{
  LoRaMacTxInfo_t tx_info;
  uint16_t mtu;

  if (LoRaMacQueryTxPossible(0, &tx_info) != LORAMAC_STATUS_OK)
  {
    mtu = 0;
  }
  else
  {
    mtu = (uint16_t)tx_info.MaxPossibleApplicationDataSize;
  }

  return mtu;
}

l2_status_t
l2_get_network_activation(l2_network_activation_type_t *activation_type)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_NETWORK_ACTIVATION;
  if (LoRaMacMibGetRequestConfirm(&mib_req) != LORAMAC_STATUS_OK)
  {
    return L2_ERROR;
  }

  *activation_type = mib_req.Param.NetworkActivation;

  return L2_SUCCESS;
}

l2_status_t
l2_set_network_activation(l2_network_activation_type_t activation_type)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_NETWORK_ACTIVATION;
  mib_req.Param.NetworkActivation = activation_type;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

l2_op_status_t lora_join(uint8_t join_datarate, uint32_t *duty_cycle_wait_time)
{
  MlmeReq_t mlme_req;
  l2_op_status_t status;

  mlme_req.Type = MLME_JOIN;
  mlme_req.Req.Join.Datarate = join_datarate;
  status = LoRaMacMlmeRequest(&mlme_req);

  *duty_cycle_wait_time = mlme_req.ReqReturn.DutyCycleWaitTime;

  return status;
}

l2_op_status_t lora_send(l2_app_data_t app_data, uint8_t datarate,
                         uint32_t *duty_cycle_wait_time)
{
  McpsReq_t mcps_req;
  LoRaMacTxInfo_t tx_info;

  if (LoRaMacQueryTxPossible(app_data.size, &tx_info) != LORAMAC_STATUS_OK)
  {
    // Send empty frame in order to flush MAC commands
    mcps_req.Type = MCPS_UNCONFIRMED;
    mcps_req.Req.Unconfirmed.fBuffer = NULL;
    mcps_req.Req.Unconfirmed.fBufferSize = 0;
    mcps_req.Req.Unconfirmed.Datarate = datarate;
  }
  else
  {
    if (app_data.type == L2_MSG_TYPE_UNCONFIRMED)
    {
      mcps_req.Type = MCPS_UNCONFIRMED;
      mcps_req.Req.Unconfirmed.fPort = app_data.port;
      mcps_req.Req.Unconfirmed.fBuffer = (void *)app_data.buffer;
      mcps_req.Req.Unconfirmed.fBufferSize = app_data.size;
      mcps_req.Req.Unconfirmed.Datarate = datarate;
    }
    else
    {
      mcps_req.Type = MCPS_CONFIRMED;
      mcps_req.Req.Confirmed.fPort = app_data.port;
      mcps_req.Req.Confirmed.fBuffer = (void *)app_data.buffer;
      mcps_req.Req.Confirmed.fBufferSize = app_data.size;
      mcps_req.Req.Confirmed.NbTrials = 8;
      mcps_req.Req.Confirmed.Datarate = datarate;
    }
  }

  l2_op_status_t status = LoRaMacMcpsRequest(&mcps_req);

  *duty_cycle_wait_time = mcps_req.ReqReturn.DutyCycleWaitTime;

  return status;
}

l2_status_t l2_get_join_status(uint8_t *status)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_NETWORK_ACTIVATION;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *status = (uint8_t)mib_req.Param.NetworkActivation;
  return L2_SUCCESS;
}

l2_status_t l2_get_network_id(uint32_t *value)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_NET_ID;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *value = mib_req.Param.NetID;

  return L2_SUCCESS;
}

l2_status_t l2_set_network_id(uint32_t value)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_NET_ID;
  mib_req.Param.NetID = value;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

l2_status_t l2_get_public_network(bool *value)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_PUBLIC_NETWORK;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *value = mib_req.Param.EnablePublicNetwork;

  return L2_SUCCESS;
}

l2_status_t l2_set_public_network(bool status)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_PUBLIC_NETWORK;
  mib_req.Param.EnablePublicNetwork = status;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

l2_status_t l2_get_rx2_dr(uint8_t *dr)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_RX2_CHANNEL;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *dr = mib_req.Param.Rx2Channel.Datarate;

  return L2_SUCCESS;
}

l2_status_t l2_set_rx2_dr(uint8_t dr)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_RX2_CHANNEL;
  LoRaMacMibGetRequestConfirm(&mib_req);

  mib_req.Param.Rx2Channel.Datarate = dr;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

l2_status_t l2_get_rx2_frequency(uint32_t *freq)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_RX2_CHANNEL;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *freq = (uint32_t)mib_req.Param.Rx2Channel.Datarate;
  return L2_SUCCESS;
}

l2_status_t l2_set_rx2_frequency(uint32_t freq)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_RX2_CHANNEL;
  LoRaMacMibGetRequestConfirm(&mib_req);

  mib_req.Param.Rx2Channel.Frequency = freq;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

l2_status_t l2_get_join_accept_delay1(uint32_t *delay)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_JOIN_ACCEPT_DELAY_1;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *delay = mib_req.Param.JoinAcceptDelay1;

  return L2_SUCCESS;
}

l2_status_t l2_get_join_accept_delay2(uint32_t *delay)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_JOIN_ACCEPT_DELAY_2;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *delay = mib_req.Param.JoinAcceptDelay2;

  return L2_SUCCESS;
}

l2_status_t l2_set_join_accept_delay1(uint32_t delay)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_JOIN_ACCEPT_DELAY_1;
  mib_req.Param.JoinAcceptDelay1 = delay;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

l2_status_t l2_set_join_accept_delay2(uint32_t delay)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_JOIN_ACCEPT_DELAY_2;
  mib_req.Param.JoinAcceptDelay2 = delay;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

l2_status_t l2_get_rx1_delay(uint32_t *delay)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_RECEIVE_DELAY_1;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *delay = mib_req.Param.ReceiveDelay1;

  return L2_SUCCESS;
}

l2_status_t l2_get_rx2_delay(uint32_t *delay)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_RECEIVE_DELAY_2;
  LoRaMacMibGetRequestConfirm(&mib_req);

  *delay = mib_req.Param.ReceiveDelay2;

  return L2_SUCCESS;
}

l2_status_t l2_set_rx1_delay(uint32_t delay)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_RECEIVE_DELAY_1;
  mib_req.Param.ReceiveDelay1 = delay;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

l2_status_t l2_set_rx2_delay(uint32_t delay)
{
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_RECEIVE_DELAY_2;
  mib_req.Param.ReceiveDelay2 = delay;
  LoRaMacMibSetRequestConfirm(&mib_req);

  return L2_SUCCESS;
}

int16_t l2_get_last_rssi(void)
{
  return lora_get_rssi();
}

int16_t l2_get_last_snr(void)
{
  return lora_get_snr();
}

l2_status_t l2_set_cert_mode(void)
{
  return L2_NOT_SUPPORTED_ERR;
}

void l2_set_technology(l2a_technology_t technology)
{
  l2a_technology = technology;
}

l2_status_t l2_compute_iid(uint8_t *iid)
{
  Key_t *app_s_key;
  uint8_t cmac[16];
  AES_CMAC_CTX aes_cmac_ctx;
  uint8_t dev_eui[] = LORAWAN_DEVICE_EUI;

  SecureElementStatus_t retval = GetKeyByID(APP_S_KEY, &app_s_key);
  if (retval != SECURE_ELEMENT_SUCCESS)
  {
    return L2_ERROR;
  }

  AES_CMAC_Init(&aes_cmac_ctx);
  AES_CMAC_SetKey(&aes_cmac_ctx, app_s_key->KeyValue);
  AES_CMAC_Update(&aes_cmac_ctx, dev_eui, 8);
  AES_CMAC_Final(cmac, &aes_cmac_ctx);

  memcpy(iid, cmac, 8);

  return L2_SUCCESS;
}

l2_status_t l2_enable_multicast(uint32_t mc_addr, uint8_t **mc_keys)
{
  // Check we're in class C and switch to it if its not the case
  MibRequestConfirm_t mib_req;

  mib_req.Type = MIB_DEVICE_CLASS;
  LoRaMacMibGetRequestConfirm(&mib_req);

  if (mib_req.Param.Class != CLASS_C)
  {
    mib_req.Param.Class = CLASS_C;
    if (!LoRaMacMibSetRequestConfirm(&mib_req))
    {
      return L2_ERROR;
    }
  }

  McChannelParams_t channel = {
      .IsRemotelySetup = false,
      .Class = CLASS_C,
      .IsEnabled = true,
      .GroupID = MULTICAST_0_ADDR,
      .Address = mc_addr,
      .McKeys.Session =
          {
              .McAppSKey = mc_keys[0],
              .McNwkSKey = mc_keys[1],
          },
      .FCountMin = 0,
      .FCountMax = UINT32_MAX,
      .RxParams.ClassC = {.Frequency = 0, .Datarate = 0}};

  l2_op_status_t lora_status = LoRaMacMcChannelSetup(&channel);
  switch (lora_status)
  {
    case LORAMAC_STATUS_OK:
      return L2_SUCCESS;
    case LORAMAC_STATUS_BUSY:
    case LORAMAC_STATUS_CRYPTO_ERROR:
    default:
      return L2_ERROR;
  }
}

l2_status_t l2_redirect_next_mcps_confirm_event(void (*cb)(bool has_error))
{
  redirect_next_transmission_result = true;
  transmission_result_redirection = cb;

  return L2_SUCCESS;
}

uint32_t l2_periodic_scheduler(void)
{
  // in theory, the value should be provided by the scheduler itself (see
  // l2/lbm implementation). if not, it's better to provide a big value to
  // force system to sleep as much as it can.
  return UINT32_MAX;
}