/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * Semtech L2A layer interface.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "Commissioning.h"
#include "LoRaMac.h"
#include "fullsdkl2.h"
#include "fullsdkl2a.h"
#include "lora.h"
#include "mac_strings.h"
#include "platform.h"
#include "timer.h"

#define LORAWAN_RULEID_BYTE_SIZE 1

#ifndef TRANSMISSION_RESULT_TIMER_VALUE
#define TRANSMISSION_RESULT_TIMER_VALUE 10000 // 10s
#endif

typedef struct
{
  uint8_t joined : 1;
  uint8_t mac_process_required : 1;
  uint8_t tx_result : 1;
  uint8_t retransmission : 1;
  uint8_t transmission_result_timeout : 1;
  uint8_t rfu : 3;
} lora_events_t;

typedef struct
{
  bool joined;
  bool busy;
  l2a_callbacks_t callbacks;
  uint8_t *receive_buffer;
  uint16_t receive_buffer_size;
  uint32_t next_tx_delay;
  LoRaMacPrimitives_t mac_primitives;
  LoRaMacCallback_t mac_callbacks;
  lora_events_t lora_events;
  l2_app_data_t tx_data;
  l2a_status_t last_tx_status;
  TimerEvent_t retransmission_timer;
  TimerEvent_t transmission_result_timer;
  uint8_t device_iid[8];
  int16_t snr;
  int16_t rssi;
} l2a_ctx_t;

l2a_technology_t l2a_technology = L2A_DEFAULT;

static void lora_on_mac_process_notify(void);
void lora_transmission_result(bool tx_result, uint32_t uplink_counter,
                              uint32_t channel, uint8_t datarate,
                              int8_t tx_power, bool ack_received,
                              uint8_t nb_retries);
static void lora_data_received(uint8_t port, uint8_t *buffer, uint16_t size,
                               uint32_t downlink_counter,
                               const char *receive_window, uint8_t datarate,
                               int16_t rssi, int8_t snr);
static void lora_frame_pending(void);
static void lora_link_check(void);
static void lora_tx_needed(void);
static void lora_joined(bool state);
static void l2a_action_tx_status(l2a_status_t status);
static void retransmission_timer_event(void *context);
static void transmission_result_event(void *context);

static l2a_ctx_t l2a_ctx;

l2a_status_t l2a_initialize(const l2a_callbacks_t *callbacks,
                            uint8_t *receive_buffer,
                            uint16_t receive_buffer_size)
{
  memset(&l2a_ctx, 0, sizeof(l2a_ctx));

  if (callbacks == NULL || callbacks->processing_required == NULL ||
      callbacks->transmission_result == NULL ||
      callbacks->data_received == NULL ||
      callbacks->connectivity_available == NULL)
  {
    return L2A_REQ_CALLBACK_ERR;
  }

  TimerInit(&l2a_ctx.retransmission_timer, retransmission_timer_event);
  TimerInit(&l2a_ctx.transmission_result_timer, transmission_result_event);

  l2a_ctx.callbacks = *callbacks;
  l2a_ctx.receive_buffer = receive_buffer;
  l2a_ctx.receive_buffer_size = receive_buffer_size;
  l2a_ctx.last_tx_status = L2A_SUCCESS;

  l2a_ctx.mac_primitives.MacMcpsConfirm = lora_mcps_confirm;
  l2a_ctx.mac_primitives.MacMcpsIndication = lora_mcps_indication;
  l2a_ctx.mac_primitives.MacMlmeConfirm = lora_mlme_confirm;
  l2a_ctx.mac_primitives.MacMlmeIndication = lora_mlme_indication;
  l2a_ctx.mac_callbacks.GetBatteryLevel = NULL;
  l2a_ctx.mac_callbacks.GetTemperatureLevel = NULL;
  l2a_ctx.mac_callbacks.NvmDataChange = NULL;
  l2a_ctx.mac_callbacks.MacProcessNotify = lora_on_mac_process_notify;

  l2_region_id_t id;
  if (l2_get_active_region(&id) != L2_SUCCESS)
  {
    return L2A_L2_ERROR;
  }

  l2_op_status_t status = LoRaMacInitialization(&l2a_ctx.mac_primitives,
                                                &l2a_ctx.mac_callbacks, id);
  if (status != LORAMAC_STATUS_OK)
  {
    PRINT_DBG("l2a> loramac initialization error: %d\n",
              lora_get_mac_status_string(status));
    return L2A_L2_ERROR;
  }

  lora_callbacks_t lora_callbacks;
  lora_callbacks.join = lora_joined;
  lora_callbacks.tx_needed = lora_tx_needed;
  lora_callbacks.link_check = lora_link_check;
  lora_callbacks.frame_pending = lora_frame_pending;
  lora_callbacks.data_received = lora_data_received;
  lora_callbacks.transmission_result = lora_transmission_result;

  bool error = lora_init(&lora_callbacks);
  if (error)
  {
    PRINT_DBG("l2a> lora initialization error\n");
    return L2A_L2_ERROR;
  }

  if (l2_join(l2_get_default_datarate(), &l2a_ctx.next_tx_delay) != L2_SUCCESS)
  {
    PRINT_DBG("l2a> join error\n");
    return L2A_L2_ERROR;
  }

  PRINT_DBG("l2a> join request\n");
  PRINT_DBG("l2a> duty cycle wait time: %lu ms\n", l2a_ctx.next_tx_delay);

  return L2A_SUCCESS;
}

static l2a_status_t l2_try_send(void)
{
  l2a_ctx.busy = true;

  if (l2_send(l2a_ctx.tx_data, l2_get_default_datarate(),
              &l2a_ctx.next_tx_delay) != L2_SUCCESS)
  {
    // The packet cannot be transmitted immediately. Wait next Tx Delay slot for
    // packet retransmission.
    TimerSetValue(&l2a_ctx.retransmission_timer, l2a_ctx.next_tx_delay + 1);
    TimerStart(&l2a_ctx.retransmission_timer);

    PRINT_DBG("l2a> uplink packet retransmission will be in %lu ms\n",
              l2a_ctx.next_tx_delay);
    return L2A_SUCCESS;
  }

  // This timer is used as a workaround to a case where the semtech stack does
  // not transmit the transmission result event
  TimerSetValue(&l2a_ctx.transmission_result_timer,
                TRANSMISSION_RESULT_TIMER_VALUE);
  TimerStart(&l2a_ctx.transmission_result_timer);

  PRINT_DBG("l2a> uplink transmission request\n");
  PRINT_DBG("l2a> duty cycle wait time: %lu ms\n", l2a_ctx.next_tx_delay);

  return L2A_SUCCESS;
}

l2a_status_t l2a_send_data(const uint8_t *buffer, uint16_t size)
{
  if (size < LORAWAN_RULEID_BYTE_SIZE)
  {
    return L2A_INTERNAL_ERR;
  }

  if (!l2a_ctx.joined)
  {
    l2a_action_tx_status(L2A_CONNECT_ERR);
    return L2A_SUCCESS;
  }

  if (l2a_ctx.busy)
  {
    l2a_action_tx_status(L2A_BUSY_ERR);
    return L2A_SUCCESS;
  }

  memset(&l2a_ctx.tx_data, 0, sizeof(l2a_ctx.tx_data));

  l2a_ctx.tx_data.port = buffer[0];
  l2a_ctx.tx_data.size = size - LORAWAN_RULEID_BYTE_SIZE;
  l2a_ctx.tx_data.buffer = (uint8_t *)(&buffer[LORAWAN_RULEID_BYTE_SIZE]);
  l2a_ctx.tx_data.type = l2_get_message_type();

  return l2_try_send();
}

l2a_technology_t l2a_get_technology(void)
{
  return l2a_technology;
}

uint16_t l2a_get_mtu(void)
{
  uint16_t mtu = l2_get_mtu();
  PRINT_DBG("l2a> mtu: %d\n", mtu);
  return mtu;
}

uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  (void)data_size;
  PRINT_DBG("l2a> next tx delay: %lu ms\n", l2a_ctx.next_tx_delay);
  return l2a_ctx.next_tx_delay;
}

l2a_status_t l2a_process(void)
{
  if (l2a_ctx.lora_events.mac_process_required)
  {
    l2a_ctx.lora_events.mac_process_required = false;
    LoRaMacProcess();
  }

  if (l2a_ctx.lora_events.joined)
  {
    l2a_ctx.lora_events.joined = false;
    l2a_ctx.joined = true;
    l2a_ctx.callbacks.connectivity_available();
  }

  if (l2a_ctx.lora_events.tx_result)
  {
    TimerStop(&l2a_ctx.transmission_result_timer);
    l2a_ctx.lora_events.tx_result = false;
    l2a_ctx.callbacks.transmission_result(l2a_ctx.last_tx_status, 0);
    l2a_ctx.last_tx_status = L2A_SUCCESS;
  }

  if (l2a_ctx.lora_events.retransmission)
  {
    l2a_ctx.lora_events.retransmission = false;
    l2_try_send();
  }

  if (l2a_ctx.lora_events.transmission_result_timeout)
  {
    l2a_ctx.lora_events.transmission_result_timeout = false;
    PRINT_DBG("l2a> transmission result event not received from the stack\n");
    l2a_action_tx_status(L2A_L2_ERROR);
    l2a_ctx.busy = false;
  }

  return L2A_SUCCESS;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  *dev_iid = l2a_ctx.device_iid;

  if (l2_compute_iid(l2a_ctx.device_iid) != L2_SUCCESS)
  {
    return false;
  }
  return true;
}

void lora_joined(bool state)
{
  if (state)
  {
    l2a_ctx.lora_events.joined = true;
    l2a_ctx.callbacks.processing_required();
  }
}

void lora_transmission_result(bool has_error, uint32_t uplink_counter,
                              uint32_t channel, uint8_t datarate,
                              int8_t tx_power, bool ack_received,
                              uint8_t nb_retries)
{
  uint16_t channels_mask[5]; // either 1 or 5 bytes depending on the region
  uint8_t channels_mask_size;
  char class;

  if (!l2a_ctx.busy)
  {
    PRINT_DBG(
        "l2a> tx result happened after transmission result timer timeout\n");
    return;
  }

  l2a_ctx.busy = false;
  if (has_error)
  {
    PRINT_DBG("l2a> error in tx result\n");
    l2a_action_tx_status(L2A_L2_ERROR);
    return;
  }

  PRINT_DBG("l2a> uplink packet %d transmitted\n", uplink_counter);
  PRINT_DBG("l2a> port: %d, ", l2a_ctx.tx_data.port);
  PRINT_DBG("dr: %d, ", datarate);
  l2_get_class(&class);
  PRINT_DBG("class: %c, ", class);
  PRINT_DBG("freq: %lu, ", l2_get_uplink_frequency(channel));
  PRINT_DBG("tx pwr: %d, ", tx_power);
  if (l2a_ctx.tx_data.type == L2_MSG_TYPE_CONFIRMED)
  {
    PRINT_DBG("confirmed, ");
    PRINT_DBG("ack: %d, ", ack_received);
  }
  else
  {
    PRINT_DBG("unconfirmed, ");
  }
  l2_get_channels_mask(channels_mask, &channels_mask_size);
  PRINT_DBG("ch mask: ");
  for (uint8_t i = 0; i < channels_mask_size; i++)
  {
    PRINT_DBG("%04X", channels_mask[i]);
  }
  PRINT_DBG(", data len: %d, ", l2a_ctx.tx_data.size);
  PRINT_DBG("data:\n");
  PRINT_HEX_BUF_DBG(l2a_ctx.tx_data.buffer, l2a_ctx.tx_data.size);

  if (l2a_ctx.joined)
  {
    l2a_action_tx_status(L2A_SUCCESS);
  }
}

void lora_data_received(uint8_t port, uint8_t *buffer, uint16_t size,
                        uint32_t downlink_counter, const char *receive_window,
                        uint8_t datarate, int16_t rssi, int8_t snr)
{
  if (size + LORAWAN_RULEID_BYTE_SIZE > l2a_ctx.receive_buffer_size)
  {
    PRINT_DBG("l2a> downlink receive error\n");
    l2a_ctx.callbacks.data_received(size + LORAWAN_RULEID_BYTE_SIZE,
                                    L2A_BUFFER_ERR);
    return;
  }

  PRINT_DBG("l2a> downlink packet %d received\n", downlink_counter);
  PRINT_DBG("l2a> port: %d, ", port);
  PRINT_DBG("dr: %d, ", datarate);
  PRINT_DBG("win: %s, ", receive_window);
  PRINT_DBG("rssi: %d, ", rssi);
  PRINT_DBG("snr: %d, ", snr);
  PRINT_DBG("data len: %d, ", size);
  PRINT_DBG("data:\n");
  PRINT_HEX_BUF_DBG(buffer, size);

  l2a_ctx.snr = snr;
  l2a_ctx.rssi = rssi;
  // Concatenate port with the buffer
  l2a_ctx.receive_buffer[0] = port;
  memcpy(&l2a_ctx.receive_buffer[1], buffer, size);
  l2a_ctx.callbacks.data_received(size + LORAWAN_RULEID_BYTE_SIZE, L2A_SUCCESS);
}

static void l2a_action_tx_status(l2a_status_t status)
{
  l2a_ctx.lora_events.tx_result = true;
  l2a_ctx.last_tx_status = status;
  l2a_ctx.callbacks.processing_required();
}

void lora_on_mac_process_notify(void)
{
  l2a_ctx.lora_events.mac_process_required = true;
  l2a_ctx.callbacks.processing_required();
}

static void lora_tx_needed(void)
{
  // TODO(aydu): Not necessary for now
}

static void lora_link_check(void)
{
  // TODO(aydu): Not necessary for now
}

static void lora_frame_pending(void)
{
  // TODO(aydu): Not necessary for now
}

int16_t lora_get_snr(void)
{
  return l2a_ctx.snr;
}

int16_t lora_get_rssi(void)
{
  return l2a_ctx.rssi;
}

static void retransmission_timer_event(void *context)
{
  l2a_ctx.lora_events.retransmission = true;
  l2a_ctx.callbacks.processing_required();
}

static void transmission_result_event(void *context)
{
  l2a_ctx.lora_events.transmission_result_timeout = true;
  l2a_ctx.callbacks.processing_required();
}