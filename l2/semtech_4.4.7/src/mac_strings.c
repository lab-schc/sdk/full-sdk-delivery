/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * Semtech Mac layer event and error strings.
 */

#include "mac_strings.h"

static const char *mac_status_strings[] = {
    "OK",                            // LORAMAC_STATUS_OK
    "Busy",                          // LORAMAC_STATUS_BUSY
    "Service unknown",               // LORAMAC_STATUS_SERVICE_UNKNOWN
    "Parameter invalid",             // LORAMAC_STATUS_PARAMETER_INVALID
    "Frequency invalid",             // LORAMAC_STATUS_FREQUENCY_INVALID
    "Datarate invalid",              // LORAMAC_STATUS_DATARATE_INVALID
    "Frequency or datarate invalid", // LORAMAC_STATUS_FREQ_AND_DR_INVALID
    "No network joined",             // LORAMAC_STATUS_NO_NETWORK_JOINED
    "Length error",                  // LORAMAC_STATUS_LENGTH_ERROR
    "Region not supported",          // LORAMAC_STATUS_REGION_NOT_SUPPORTED
    "Skipped APP data",              // LORAMAC_STATUS_SKIPPED_APP_DATA
    "Duty-cycle restricted",         // LORAMAC_STATUS_DUTYCYCLE_RESTRICTED
    "No channel found",              // LORAMAC_STATUS_NO_CHANNEL_FOUND
    "No free channel found",         // LORAMAC_STATUS_NO_FREE_CHANNEL_FOUND
    "Busy beacon reserved time",     // LORAMAC_STATUS_BUSY_BEACON_RESERVED_TIME
    "Busy ping-slot window time", // LORAMAC_STATUS_BUSY_PING_SLOT_WINDOW_TIME
    "Busy uplink collision",      // LORAMAC_STATUS_BUSY_UPLINK_COLLISION
    "Crypto error",               // LORAMAC_STATUS_CRYPTO_ERROR
    "FCnt handler error",         // LORAMAC_STATUS_FCNT_HANDLER_ERROR
    "MAC command error",          // LORAMAC_STATUS_MAC_COMMAD_ERROR
    "ClassB error",               // LORAMAC_STATUS_CLASS_B_ERROR
    "Confirm queue error",        // LORAMAC_STATUS_CONFIRM_QUEUE_ERROR
    "Multicast group undefined",  // LORAMAC_STATUS_MC_GROUP_UNDEFINED
    "Unknown error",              // LORAMAC_STATUS_ERROR
};

static const char *event_info_status_strings[] = {
    "OK",                       // LORAMAC_EVENT_INFO_STATUS_OK
    "Error",                    // LORAMAC_EVENT_INFO_STATUS_ERROR
    "Tx timeout",               // LORAMAC_EVENT_INFO_STATUS_TX_TIMEOUT
    "Rx 1 timeout",             // LORAMAC_EVENT_INFO_STATUS_RX1_TIMEOUT
    "Rx 2 timeout",             // LORAMAC_EVENT_INFO_STATUS_RX2_TIMEOUT
    "Rx1 error",                // LORAMAC_EVENT_INFO_STATUS_RX1_ERROR
    "Rx2 error",                // LORAMAC_EVENT_INFO_STATUS_RX2_ERROR
    "Join failed",              // LORAMAC_EVENT_INFO_STATUS_JOIN_FAIL
    "Downlink repeated",        // LORAMAC_EVENT_INFO_STATUS_DOWNLINK_REPEATED
    "Tx DR payload size error", // LORAMAC_EVENT_INFO_STATUS_TX_DR_PAYLOAD_SIZE_ERROR
    "Downlink too many frames loss", // LORAMAC_EVENT_INFO_STATUS_DOWNLINK_TOO_MANY_FRAMES_LOSS
    "Address fail",                  // LORAMAC_EVENT_INFO_STATUS_ADDRESS_FAIL
    "MIC fail",                      // LORAMAC_EVENT_INFO_STATUS_MIC_FAIL
    "Multicast fail",                // LORAMAC_EVENT_INFO_STATUS_MULTICAST_FAIL
    "Beacon locked",                 // LORAMAC_EVENT_INFO_STATUS_BEACON_LOCKED
    "Beacon lost",                   // LORAMAC_EVENT_INFO_STATUS_BEACON_LOST
    "Beacon not found" // LORAMAC_EVENT_INFO_STATUS_BEACON_NOT_FOUND
};

static const char *slot_strings[] = {
    "1", "2", "C", "C Multicast", "B Ping-Slot", "B Multicast Ping-Slot"};

const char *lora_get_mac_status_string(uint8_t status)
{
  return mac_status_strings[status];
}

const char *lora_get_event_info_status_string(uint8_t status)
{
  return event_info_status_strings[status];
}

const char *lora_get_slot_string(uint8_t slot)
{
  return slot_strings[slot];
}