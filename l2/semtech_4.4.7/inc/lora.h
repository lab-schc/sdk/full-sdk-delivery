/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * Semtech L2 Lora binding layer.
 */

#ifndef LORA_H_
#define LORA_H_

#include <stdbool.h>
#include <stdint.h>

#include "LoRaMac.h"
#include "fullsdkl2.h"

typedef void (*lora_connectivity_state_t)(bool state);
typedef void (*lora_tx_needed_t)(void);
typedef void (*lora_link_check_t)(void);
typedef void (*lora_frame_pending_t)(void);
typedef void (*lora_data_received_t)(uint8_t port, uint8_t *buffer,
                                     uint16_t size, uint32_t downlink_counter,
                                     const char *receive_window,
                                     uint8_t datarate, int16_t rssi,
                                     int8_t snr);
typedef void (*lora_transmission_result_t)(bool has_error,
                                           uint32_t uplink_counter,
                                           uint32_t channel, uint8_t datarate,
                                           int8_t tx_power, bool ack_received,
                                           uint8_t nb_retries);

typedef struct
{
  lora_connectivity_state_t join;
  lora_tx_needed_t tx_needed;
  lora_link_check_t link_check;
  lora_frame_pending_t frame_pending;
  lora_data_received_t data_received;
  lora_transmission_result_t transmission_result;
} lora_callbacks_t;

bool lora_init(lora_callbacks_t *lora_cb);
l2_op_status_t lora_join(uint8_t join_datarate, uint32_t *duty_cycle_wait_time);
l2_op_status_t lora_send(l2_app_data_t app_data, uint8_t datarate,
                         uint32_t *duty_cycle_wait_time);
void lora_mlme_indication(MlmeIndication_t *mlme_indication);
void lora_mlme_confirm(MlmeConfirm_t *mlme_confirm);
void lora_mcps_indication(McpsIndication_t *mcps_indication);
void lora_mcps_confirm(McpsConfirm_t *mcps_confirm);
uint8_t *lora_get_dev_eui(void);
void lora_set_dev_eui(uint8_t *value);
uint8_t *lora_get_join_eui(void);
void lora_set_join_eui(uint8_t *value);
uint8_t *lora_get_app_key(void);
void lora_set_app_key(uint8_t *value);
int16_t lora_get_snr(void);
int16_t lora_get_rssi(void);

extern uint8_t dev_eui[];
extern uint8_t join_eui[];
extern uint8_t app_key[];
extern bool redirect_next_transmission_result;
extern void (*transmission_result_redirection)(bool has_error);

#endif // LORA_H_