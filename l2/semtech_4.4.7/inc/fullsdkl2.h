/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * Semtech L2 configuration layer.
 */

#ifndef FULLSDK_L2_H_
#define FULLSDK_L2_H_

#include <stdint.h>
#include <string.h>

#include "LoRaMac.h"
#include "fullsdkl2a.h"
#include "fullsdkl2common.h"

#define SEMTECH_L2

#define L2_MAX_SUPPORTED_REGION (10)

typedef LoRaMacStatus_t l2_op_status_t;

l2_status_t l2_init(void);
const char *l2_get_version(void);
l2_msg_type_t l2_get_message_type(void);
void l2_set_message_type(l2_msg_type_t message_type);
uint8_t l2_get_default_datarate(void);
void l2_set_default_datarate(uint8_t datarate);
l2_status_t l2_get_active_region(l2_region_id_t *region);
l2_status_t l2_set_active_region(l2_region_id_t region);
l2_status_t l2_get_class(char *class);
l2_status_t l2_set_class(char class);
uint32_t l2_get_uplink_frequency(uint32_t channel);
void l2_get_channels_mask(uint16_t *channels_mask, uint8_t *channels_mask_size);
uint8_t *l2_get_dev_eui(void);
l2_status_t l2_set_dev_eui(uint8_t *value);
uint8_t *l2_get_join_eui(void);
l2_status_t l2_set_join_eui(uint8_t *value);
l2_status_t l2_get_nwk_s_key(uint8_t *nwk_s_key);
l2_status_t l2_set_nwk_s_key(uint8_t *nwk_s_key);
uint8_t *l2_get_app_key(void);
l2_status_t l2_set_app_key(uint8_t *value);
l2_status_t l2_get_app_s_key(uint8_t *value);
l2_status_t l2_set_app_s_key(uint8_t *app_s_key);
uint8_t *l2_get_se_pin(void);
l2_status_t l2_get_dr(uint8_t *dr);
l2_status_t l2_set_dr(uint8_t dr);
void l2_set_lorawan_version(void);
l2_status_t l2_get_dev_addr(uint32_t *value);
l2_status_t l2_set_dev_addr(uint32_t dev_addr);
void l2_enable_public_network(void);
l2_status_t
l2_get_network_activation(l2_network_activation_type_t *activation_type);
l2_status_t
l2_set_network_activation(l2_network_activation_type_t activation_type);
l2_status_t l2_set_adr(bool adr);
l2_status_t l2_get_adr(bool *adr);
l2_status_t l2_get_dutycycle(bool *value);
l2_status_t l2_set_dutycycle(bool dutycycle_on);
uint16_t l2_get_mtu(void);
l2_status_t l2_set_system_max_rx_error(uint32_t sys_max_rx_error);
l2_status_t l2_get_system_max_rx_error(uint32_t *value);
l2_status_t l2_get_join_status(uint8_t *status);
l2_status_t l2_get_network_id(uint32_t *value);
l2_status_t l2_get_public_network(bool *value);
l2_status_t l2_set_public_network(bool status);
l2_status_t l2_set_network_id(uint32_t value);
l2_status_t l2_get_rx2_dr(uint8_t *dr);
l2_status_t l2_set_rx2_dr(uint8_t dr);
l2_status_t l2_get_rx2_frequency(uint32_t *freq);
l2_status_t l2_set_rx2_frequency(uint32_t freq);
l2_status_t l2_get_join_accept_delay1(uint32_t *delay);
l2_status_t l2_set_join_accept_delay1(uint32_t delay);
l2_status_t l2_get_join_accept_delay2(uint32_t *delay);
l2_status_t l2_set_join_accept_delay2(uint32_t delay);
l2_status_t l2_get_rx1_delay(uint32_t *delay);
l2_status_t l2_get_rx2_delay(uint32_t *delay);
l2_status_t l2_set_rx1_delay(uint32_t delay);
l2_status_t l2_set_rx2_delay(uint32_t delay);
l2_status_t l2_set_cert_mode(void);
void l2_set_technology(l2a_technology_t technology);
l2_status_t l2_join(uint8_t join_dr, uint32_t *duty_cycle_wait_time);
l2_status_t l2_send(l2_app_data_t app_data, uint8_t datarate,
                    uint32_t *duty_cycle_wait_time);
l2_status_t l2_compute_iid(uint8_t *iid);
l2_status_t l2_enable_multicast(uint32_t mc_addr, uint8_t **mc_keys);
l2_status_t l2_redirect_next_mcps_confirm_event(void (*cb)(bool has_error));
uint32_t l2_periodic_scheduler(void);
int16_t l2_get_last_snr(void);
int16_t l2_get_last_rssi(void);

// This global variable must be defined in L2A layer and initialized
// to L2A_DEFAULT. Its use is reserved.
extern l2a_technology_t l2a_technology;
// This global variable must be defined in fullsdkl2.c
extern l2_region_t regions[L2_MAX_SUPPORTED_REGION];

#endif // FULLSDK_L2_H_