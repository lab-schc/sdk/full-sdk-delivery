/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * Semtech Mac layer event and error strings.
 */

#ifndef MAC_STRINGS_H_
#define MAC_STRINGS_H_

#include <stdint.h>

const char *lora_get_mac_status_string(uint8_t status);
const char *lora_get_event_info_status_string(uint8_t status);
const char *lora_get_slot_string(uint8_t slot);

#endif // MAC_STRINGS_H_