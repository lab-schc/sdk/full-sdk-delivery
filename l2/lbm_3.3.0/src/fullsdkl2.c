/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Authors: Jerome Elias  jerome.elias@ackl.io
 *
 * LBM L2 configuration layer.
 */

#include "fullsdkl2.h"
#include "cmac.h"
#include "platform.h"
#include "lorawan_api.h"
#include "smtc_secure_element.h"
#include "smtc_modem_api.h"

#define STACK_ID (0)
#define MIN_NEXT_TX_DELAY 1000 // wait at least 1 second when we have tx credit

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE VARIABLES -------------------------------------------------------
 */
static uint8_t rx_payload[SMTC_MODEM_MAX_LORAWAN_PAYLOAD_LENGTH] = {0}; // Buffer for rx payload
static uint8_t rx_payload_size = 0;                                     // Size of the payload in the rx_payload buffer
static smtc_modem_dl_metadata_t rx_metadata = {0};                      // Metadata of downlink

static l2_msg_type_t l2_default_message_type = L2_MSG_TYPE_UNCONFIRMED;
static uint8_t user_dev_eui[8] = {0};
static uint8_t user_join_eui[8] = {0};
static uint8_t user_app_key[16] = {0};

static uint8_t current_datarate = 3;
static l2_region_id_t active_region = SMTC_MODEM_REGION_EU_868;
static smtc_modem_class_t device_class = SMTC_MODEM_CLASS_C;
static smtc_modem_adr_profile_t device_adr = SMTC_MODEM_ADR_PROFILE_NETWORK_CONTROLLED;

static bool l2_init_done = false;

typedef struct soft_se_key_s
{
  smtc_se_key_identifier_t key_id;     //!< Key identifier
  uint8_t key_value[SMTC_SE_KEY_SIZE]; //!< Key value
} soft_se_key_t;

// Defined in soft-se.c
// A patch must be applied to LBM L2 stack to make get_key_by_id() public so callable.
smtc_se_return_code_t get_key_by_id(smtc_se_key_identifier_t key_id, soft_se_key_t **key_item, uint8_t stack_id);

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE FUNCTIONS DECLARATION -------------------------------------------
 */

static void modem_event_callback(void);
static void on_modem_network_joined(void);
static void on_modem_down_data(int8_t rssi, int8_t snr,
                               smtc_modem_dl_window_t rx_window, uint8_t port,
                               const uint8_t *payload, uint8_t size);
static void on_modem_tx_done(smtc_modem_event_txdone_status_t status);

l2_region_t regions[L2_MAX_SUPPORTED_REGION] = {
    {.str = "AS923_1", .id = SMTC_MODEM_REGION_AS_923_GRP1},
    {.str = "AS923_2", .id = SMTC_MODEM_REGION_AS_923_GRP2},
    {.str = "AS923_3", .id = SMTC_MODEM_REGION_AS_923_GRP3},
    {.str = "AU915", .id = SMTC_MODEM_REGION_AU_915},
    {.str = "CN470", .id = SMTC_MODEM_REGION_CN_470},
    {.str = "CN470_RP10", .id = SMTC_MODEM_REGION_CN_470_RP_1_0},
    {.str = "WW2G4", .id = SMTC_MODEM_REGION_WW2G4},
    {.str = "EU868", .id = SMTC_MODEM_REGION_EU_868},
    {.str = "KR920", .id = SMTC_MODEM_REGION_KR_920},
    {.str = "IN865", .id = SMTC_MODEM_REGION_IN_865},
    {.str = "US915", .id = SMTC_MODEM_REGION_US_915},
    {.str = "RU864", .id = SMTC_MODEM_REGION_RU_864}};

const char *l2_get_version(void)
{
  return "LoRa Basics Modem - v3.3.0";
}

l2_status_t l2_init(void)
{
  // Disable IRQ to avoid unwanted behaviour during init
  hal_mcu_disable_irq();

  // Configure all the µC periph (clock, gpio, timer, ...)
  hal_mcu_init();

  // Init the modem and use modem_event_callback as event callback, please note that the callback will be
  // called immediatly after the first call to smtc_modem_run_engine because of the reset detection
  smtc_modem_init(&modem_event_callback);

  // Re-enable IRQ
  hal_mcu_enable_irq();

  l2_init_done = true;

  return L2_SUCCESS;
}

l2_status_t l2_get_class(char *class)
{
  // Default class A
  if (device_class == SMTC_MODEM_CLASS_A)
  {
    *class = 'A';
  }
  // Class C
  else if (device_class == SMTC_MODEM_CLASS_C)
  {
    *class = 'C';
  }
  else
  {
    // Class B
    *class = 'B';
  }

  return L2_SUCCESS;
}

l2_status_t l2_set_class(char class)
{
  if (class == 'A')
  {
    smtc_modem_set_class(STACK_ID, SMTC_MODEM_CLASS_A);
    device_class = SMTC_MODEM_CLASS_A;
  }
  else if (class == 'C')
  {
    smtc_modem_set_class(STACK_ID, SMTC_MODEM_CLASS_C);
    device_class = SMTC_MODEM_CLASS_C;
  }
  else
  {
    PRINT_DBG("l2> unsupported class\n");
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

uint16_t l2_get_mtu(void)
{
  uint8_t mtu = 0;

  smtc_modem_return_code_t status =
      smtc_modem_get_next_tx_max_payload(STACK_ID, &mtu);
  if (status != SMTC_MODEM_RC_OK)
  {
    mtu = 0;
    PRINT_DBG("l2> mtu get error, returning: %d\n", mtu);
  }

  return (uint16_t)mtu;
}

uint32_t l2_get_next_tx_delay(void)
{
  int32_t next_tx_delay;

  smtc_modem_return_code_t status =
      smtc_modem_get_duty_cycle_status(STACK_ID, &next_tx_delay);
  if (status != SMTC_MODEM_RC_OK)
  {
    next_tx_delay = UINT32_MAX;
    PRINT_DBG("l2> next tx delay get error, returning: %d ms\n",
              next_tx_delay);
  }

  if (next_tx_delay < 0)
  {
    next_tx_delay = -next_tx_delay;

    PRINT_DBG("l2> rnext_tx_delay: %d ms\n", next_tx_delay);
    return (uint32_t)next_tx_delay;
  }

  PRINT_DBG("l2a> remaining time credit: %d ms\n", next_tx_delay);
  return MIN_NEXT_TX_DELAY;
}

l2_status_t l2_get_network_id(uint32_t *value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_network_id(uint32_t value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_public_network(bool *value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_public_network(bool status)
{
  return L2_NOT_SUPPORTED_ERR;
}

uint8_t l2_get_default_datarate(void)
{
  return current_datarate;
}

l2_status_t l2_join(uint8_t datarate, uint32_t *duty_cycle_wait_time)
{
  (void)*duty_cycle_wait_time;

  // L2 layer must be initialized
  if (!l2_init_done)
  {
    return L2_ERROR;
  }

  // Save datarate
  current_datarate = datarate;

  // Reset devNonce
  lorawan_api_factory_reset(STACK_ID);
  // Set user credentials
  smtc_modem_set_deveui(STACK_ID, user_dev_eui);
  smtc_modem_set_joineui(STACK_ID, user_join_eui);
  smtc_modem_set_nwkkey(STACK_ID, user_app_key);
  // Set user region
  smtc_modem_set_region(STACK_ID, SMTC_MODEM_REGION_EU_868);
  // Set class C by default
  smtc_modem_set_class(STACK_ID, SMTC_MODEM_CLASS_C);
  // OTAA configuration
  lorawan_api_set_activation_mode(ACTIVATION_MODE_OTAA, STACK_ID);
  // Set custom datarate
  l2_set_dr(current_datarate);
  // Schedule a Join LoRaWAN network
  smtc_modem_join_network(STACK_ID);

  return L2_SUCCESS;
}

l2_status_t l2_set_active_region(l2_region_id_t region)
{
  smtc_modem_return_code_t status;

  active_region = region;

  status = smtc_modem_set_region(STACK_ID, region);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2> region set failed: %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_active_region(l2_region_id_t *region)
{
  smtc_modem_return_code_t status;

  status = smtc_modem_get_region(STACK_ID, region);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2> region get failed: %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

void l2_set_technology(l2a_technology_t technology)
{
  l2a_technology = technology;
}

void l2_set_message_type(l2_msg_type_t message_type)
{
  l2_default_message_type = message_type;
}

l2_msg_type_t l2_get_message_type(void)
{
  return l2_default_message_type;
}

l2_status_t l2_send(l2_app_data_t app_data, uint8_t datarate,
                    uint32_t *duty_cycle_wait_time)
{
  (void)*duty_cycle_wait_time;

  smtc_modem_return_code_t status;

  // Set datarate
  l2_set_dr(datarate);

  // Handle empty payload.
  if (app_data.size == 0)
  {
    status = smtc_modem_request_empty_uplink(STACK_ID, false, 0,
                                             l2_default_message_type);
    if (status != SMTC_MODEM_RC_OK)
    {
      PRINT_DBG("l2> send empty uplink request failed %d\n", status);
      return L2_ERROR;
    }

    return L2_SUCCESS;
  }

  status = smtc_modem_request_uplink(STACK_ID, app_data.port,
                                     l2_default_message_type, app_data.buffer,
                                     app_data.size);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2> send data request failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_adr(bool *adr)
{
  if (device_adr == SMTC_MODEM_ADR_PROFILE_NETWORK_CONTROLLED)
  {
    *adr = true;
  }
  else
  {
    *adr = false;
  }

  return L2_SUCCESS;
}

l2_status_t l2_set_adr(bool adr)
{
  if (adr)
  {
    smtc_modem_return_code_t status;

    status = smtc_modem_adr_set_profile(
        STACK_ID, SMTC_MODEM_ADR_PROFILE_NETWORK_CONTROLLED, NULL);
    if (status != SMTC_MODEM_RC_OK)
    {
      PRINT_DBG("l2> set adr failed %d\n", status);
      return L2_ERROR;
    }
    // Save ADR
    device_adr = SMTC_MODEM_ADR_PROFILE_NETWORK_CONTROLLED;
  }
  else
  {
    l2_set_dr(current_datarate);
    // Save ADR
    device_adr = SMTC_MODEM_ADR_PROFILE_CUSTOM;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_dr(uint8_t *dr)
{
  *dr = current_datarate;
  return L2_SUCCESS;
}

l2_status_t l2_set_dr(uint8_t dr)
{
  smtc_modem_return_code_t status;
  uint8_t custom_datarate[SMTC_MODEM_CUSTOM_ADR_DATA_LENGTH];

  for (uint8_t i = 0; i < sizeof(custom_datarate); i++)
  {
    custom_datarate[i] = dr;
  }

  current_datarate = dr;
  status = smtc_modem_adr_set_profile(STACK_ID, SMTC_MODEM_ADR_PROFILE_CUSTOM,
                                      custom_datarate);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2> adr set profile failed failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_dutycycle(bool *value)
{
  (void)*value;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_dutycycle(bool dutycycle_on)
{
  (void)dutycycle_on;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_nwk_s_key(uint8_t *nwk_s_key)
{
  (void)*nwk_s_key;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_nwk_s_key(uint8_t *nwk_s_key)
{
  (void)*nwk_s_key;
  return L2_NOT_SUPPORTED_ERR;
}

uint8_t *l2_get_app_key(void)
{
  // app key is nwk key in lorawan v1.0.x
  return user_app_key;
}

l2_status_t l2_set_app_key(uint8_t *value)
{
  smtc_modem_return_code_t status;

  // app key is nwk key in lorawan v1.0.x
  memcpy(user_app_key, value, SMTC_MODEM_KEY_LENGTH);

  status = smtc_modem_set_nwkkey(STACK_ID, value);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2> set app key failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_app_s_key(uint8_t *value)
{
  (void)*value;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_app_s_key(uint8_t *app_s_key)
{
  (void)*app_s_key;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_dev_addr(uint32_t *value)
{
  (void)*value;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_dev_addr(uint32_t dev_addr)
{
  (void)dev_addr;
  return L2_NOT_SUPPORTED_ERR;
}

uint8_t *l2_get_join_eui(void)
{
  return user_join_eui;
}

l2_status_t l2_set_join_eui(uint8_t *value)
{
  smtc_modem_return_code_t status;

  memcpy(user_join_eui, value, SMTC_MODEM_EUI_LENGTH);

  status = smtc_modem_set_joineui(STACK_ID, value);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2> set join eui failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

uint8_t *l2_get_dev_eui(void)
{
  return user_dev_eui;
}

l2_status_t l2_set_dev_eui(uint8_t *value)
{
  smtc_modem_return_code_t status;

  memcpy(user_dev_eui, value, SMTC_MODEM_EUI_LENGTH);

  status = smtc_modem_set_deveui(STACK_ID, value);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2> set dev eui failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_redirect_next_mcps_confirm_event(void (*cb)(bool has_error))
{
  (void)(*cb);
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_join_status(uint8_t *status)
{
  (void)*status;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t
l2_get_network_activation(l2_network_activation_type_t *activation_type)
{
  (void)*activation_type;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t
l2_set_network_activation(l2_network_activation_type_t activation_type)
{
  (void)activation_type;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_system_max_rx_error(uint32_t *value)
{
  (void)*value;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_system_max_rx_error(uint32_t sys_max_rx_error)
{
  (void)sys_max_rx_error;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_join_accept_delay1(uint32_t *delay)
{
  (void)*delay;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_join_accept_delay1(uint32_t delay)
{
  (void)delay;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_join_accept_delay2(uint32_t *delay)
{
  (void)*delay;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_join_accept_delay2(uint32_t delay)
{
  (void)delay;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_rx1_delay(uint32_t *delay)
{
  (void)*delay;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_rx1_delay(uint32_t delay)
{
  (void)delay;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_rx2_delay(uint32_t *delay)
{
  (void)*delay;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_rx2_dr(uint8_t *dr)
{
  (void)*dr;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_rx2_dr(uint8_t dr)
{
  (void)dr;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_rx2_delay(uint32_t delay)
{
  (void)delay;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_rx2_frequency(uint32_t *freq)
{
  (void)freq;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_rx2_frequency(uint32_t freq)
{
  (void)freq;
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_cert_mode(void)
{
  return L2_NOT_SUPPORTED_ERR;
}

int16_t l2_get_last_snr(void)
{
  return rx_metadata.snr;
}

int16_t l2_get_last_rssi(void)
{
  return rx_metadata.rssi;
}

l2_status_t l2_compute_iid(uint8_t *iid)
{
  soft_se_key_t *app_s_key;
  uint8_t cmac[16];
  AES_CMAC_CTX aes_cmac_ctx;

  smtc_se_return_code_t status = get_key_by_id(SMTC_SE_APP_S_KEY, &app_s_key, STACK_ID);
  if (status != SMTC_SE_RC_SUCCESS)
  {
    return L2_ERROR;
  }

  AES_CMAC_Init(&aes_cmac_ctx);
  AES_CMAC_SetKey(&aes_cmac_ctx, app_s_key->key_value);
  AES_CMAC_Update(&aes_cmac_ctx, user_dev_eui, 8);
  AES_CMAC_Final(cmac, &aes_cmac_ctx);

  memcpy(iid, cmac, 8);

  return L2_SUCCESS;
}

uint32_t l2_periodic_scheduler(void)
{
  // Do not process LBM stack until L2 and L2A layers are initialized.
  if (!l2_init_done)
  {
    return UINT32_MAX;
  }

  return smtc_modem_run_engine();
}

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE FUNCTIONS DEFINITION --------------------------------------------
 */

static void on_modem_network_joined(void)
{
  PRINT_DBG("l2> on_modem_network_joined\n");
  // To avoid extra uplinks for ADR (ADR is default in LBM).
  l2_set_adr(false);
  lorawan_process_joined_event();
}

static void on_modem_down_data(int8_t rssi, int8_t snr,
                               smtc_modem_dl_window_t rx_window, uint8_t port,
                               const uint8_t *payload, uint8_t size)
{
  (void)rssi;
  (void)snr;
  PRINT_DBG("l2> on_modem_down_data\n");

  if (rx_window == SMTC_MODEM_DL_WINDOW_RX1)
  {
    PRINT_DBG("window: %s, ", "rx1");
  }
  else if (rx_window == SMTC_MODEM_DL_WINDOW_RX2)
  {
    PRINT_DBG("window: %s, ", "rx2");
  }
  else if (rx_window == SMTC_MODEM_DL_WINDOW_RXC)
  {
    PRINT_DBG("window: %s, ", "rxc");
  }
  else // multicast
  {
    PRINT_DBG("window: %s, ", "mc");
  }

  lorawan_process_downlink_rcvd_event(port,
                                      payload,
                                      size);
}

static void on_modem_tx_done(smtc_modem_event_txdone_status_t status)
{
  PRINT_DBG("l2> on_modem_tx_done\n");
  lorawan_process_tx_result_event(status != SMTC_MODEM_EVENT_TXDONE_SENT ? true : false);
}

static void modem_event_callback(void)
{
  smtc_modem_event_t current_event;
  uint8_t event_pending_count;
  uint8_t rx_remaining = 0;

  // Do not process LBM stack until L2 and L2A layers are initialized.
  if (!l2_init_done)
  {
    return;
  }

  // Continue to read modem event until all event has been processed
  do
  {
    // Read modem event
    smtc_modem_get_event(&current_event, &event_pending_count);

    switch (current_event.event_type)
    {
    case SMTC_MODEM_EVENT_RESET:
    {
      PRINT_DBG("Event received: RESET\n");
      break;
    }
    case SMTC_MODEM_EVENT_ALARM:
    {
      PRINT_DBG("Event received: ALARM\n");
      break;
    }
    case SMTC_MODEM_EVENT_JOINED:
    {
      PRINT_DBG("Event received: JOINED\n");
      // Inform upper layer
      on_modem_network_joined();
      break;
    }
    case SMTC_MODEM_EVENT_TXDONE:
    {
      PRINT_DBG("Event received: TXDONE\n");
      // Inform upper layer
      on_modem_tx_done(current_event.event_data.txdone.status);
      break;
    }
    case SMTC_MODEM_EVENT_DOWNDATA:
    {
      PRINT_DBG("Event received: DOWNDATA\n");
      // Get downlink data
      smtc_modem_get_downlink_data(rx_payload, &rx_payload_size, &rx_metadata, &rx_remaining);
      if (rx_metadata.window >= SMTC_MODEM_DL_WINDOW_RXC)
      {
        PRINT_DBG("Receive a class C downlink \n");
        if (rx_metadata.window >= SMTC_MODEM_DL_WINDOW_RXC_MC_GRP0)
        {
          PRINT_DBG("Receive on multicast session group %d downlink \n", rx_metadata.window - 4);
        }
      }
      else
      {
        PRINT_DBG("Receive a class A downlink \n");
      }
      PRINT_DBG("Data received on port %u\n", rx_metadata.fport);
      PRINT_DBG("Received payload", rx_payload, rx_payload_size);
      // Inform upper layer
      on_modem_down_data(rx_metadata.rssi, rx_metadata.snr, rx_metadata.window, rx_metadata.fport, rx_payload, rx_payload_size);
      break;
    }
    case SMTC_MODEM_EVENT_JOINFAIL:
      PRINT_DBG("Event received: JOINFAIL\n");
      PRINT_DBG("Join request failed \n");
      break;

    case SMTC_MODEM_EVENT_ALCSYNC_TIME:
      PRINT_DBG("Event received: ALCSync service TIME\n");
      break;

    case SMTC_MODEM_EVENT_LINK_CHECK:
      PRINT_DBG("Event received: LINK_CHECK\n");
      break;

    case SMTC_MODEM_EVENT_CLASS_B_PING_SLOT_INFO:
      PRINT_DBG("Event received: PING SLOT INFO REQ\n");
      break;

    case SMTC_MODEM_EVENT_CLASS_B_STATUS:
      PRINT_DBG("Event received: CLASS_B_STATUS\n");
      break;

    case SMTC_MODEM_EVENT_LORAWAN_MAC_TIME:
      PRINT_DBG("Event received: LORAWAN MAC TIME\n");
      break;

    default:
      PRINT_DBG("Unknown event %u\n", current_event.event_type);
      break;
    }
  } while (event_pending_count > 0);

  return;
}