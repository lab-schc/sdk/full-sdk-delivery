/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Jerome Elias - jerome.elias@ackl.io
 *
 * LBM L2A layer interface.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "fullsdkl2.h"
#include "fullsdkl2a.h"
#include "platform.h"

#define LORAWAN_RULEID_BYTE_SIZE 1
#define LORAWAN_DATARATE_DEFAULT 3

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE FUNCTIONS DECLARATION -------------------------------------------
 */
static void l2a_action_tx_status(l2a_status_t status);
static void l2a_action_data_rcvd(l2a_status_t status, uint16_t data_len);

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE VARIABLES -------------------------------------------------------
 */
typedef struct
{
  uint8_t joined : 1;
  uint8_t tx_result : 1;
  uint8_t data_rcvd : 1;
  uint8_t rfu : 5;
} lorawan_events_t;

typedef struct
{
  bool joined;
  bool busy;
  l2a_callbacks_t callbacks;
  uint8_t *receive_buffer;
  uint16_t receive_buffer_size;
  uint32_t next_tx_delay;
  lorawan_events_t lora_events;
  l2a_status_t last_tx_status;
  l2a_status_t last_data_rcvd_status;
  uint16_t last_rcvd_data_len;
  uint8_t device_iid[8];
} l2a_ctx_t;

static l2a_ctx_t l2a_ctx;
l2a_technology_t l2a_technology = L2A_DEFAULT;

/*
 * -----------------------------------------------------------------------------
 * --- PUBLIC FUNCTIONS DEFINITION ---------------------------------------------
 */
l2a_status_t l2a_initialize(const l2a_callbacks_t *callbacks,
                            uint8_t *receive_buffer,
                            uint16_t receive_buffer_size)
{
  memset(&l2a_ctx, 0, sizeof(l2a_ctx));

  if (callbacks == NULL || callbacks->processing_required == NULL ||
      callbacks->transmission_result == NULL ||
      callbacks->data_received == NULL ||
      callbacks->connectivity_available == NULL)
  {
    return L2A_REQ_CALLBACK_ERR;
  }

  l2a_ctx.callbacks = *callbacks;
  l2a_ctx.receive_buffer = receive_buffer;
  l2a_ctx.receive_buffer_size = receive_buffer_size;
  l2a_ctx.last_tx_status = L2A_SUCCESS;

  // Perform a join request (OTAA mode)
  if (l2_join(LORAWAN_DATARATE_DEFAULT, NULL) != L2_SUCCESS)
  {
    PRINT_DBG("l2a> init error\n");
    return L2A_L2_ERROR;
  }

  PRINT_DBG("l2a> network join request sent\n");

  return L2A_SUCCESS;
}

l2a_status_t l2a_send_data(const uint8_t *buffer, uint16_t data_len)
{
  if (data_len < LORAWAN_RULEID_BYTE_SIZE)
  {
    return L2A_INTERNAL_ERR;
  }

  if (!l2a_ctx.joined)
  {
    l2a_action_tx_status(L2A_CONNECT_ERR);
    return L2A_SUCCESS;
  }

  if (l2a_ctx.busy)
  {
    l2a_action_tx_status(L2A_BUSY_ERR);
    return L2A_SUCCESS;
  }

  // Save context
  l2_app_data_t app_data;
  app_data.type = L2_MSG_TYPE_CONFIRMED;
  app_data.port = buffer[0];
  app_data.size = data_len - LORAWAN_RULEID_BYTE_SIZE;
  app_data.buffer = (uint8_t *)(&buffer[LORAWAN_RULEID_BYTE_SIZE]);

  // Get datarate
  uint8_t dr = 0;
  l2_get_dr(&dr);

  // Send data through L2 layer
  if (l2_send(app_data, dr, NULL) != L2_SUCCESS)
  {
    l2a_action_tx_status(L2A_L2_ERROR);
    return L2A_SUCCESS;
  }

  PRINT_DBG("l2a> send data request done\n");
  PRINT_DBG("l2a> port: %d, ", app_data.port);
  PRINT_DBG("unconfirmed, ");
  PRINT_DBG("data len: %d, ", app_data.size);
  PRINT_DBG("data:\n");
  PRINT_HEX_BUF_DBG(app_data.buffer, app_data.size);

  l2a_ctx.busy = true;

  return L2A_SUCCESS;
}

l2a_technology_t l2a_get_technology(void)
{
  return l2a_technology;
}

uint16_t l2a_get_mtu(void)
{
  return l2_get_mtu();
}

uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  (void)data_size;
  return l2_get_next_tx_delay();
}

l2a_status_t l2a_process(void)
{
  if (l2a_ctx.lora_events.joined)
  {
    l2a_ctx.lora_events.joined = false;

    l2a_ctx.joined = true;
    l2a_ctx.callbacks.connectivity_available();
  }

  if (l2a_ctx.lora_events.tx_result)
  {
    l2a_ctx.lora_events.tx_result = false;

    l2a_ctx.callbacks.transmission_result(l2a_ctx.last_tx_status, 0);

    PRINT_DBG("l2a> tx done\n");

    l2a_ctx.last_tx_status = L2A_SUCCESS;
  }

  if (l2a_ctx.lora_events.data_rcvd)
  {
    l2a_ctx.lora_events.data_rcvd = false;

    PRINT_DBG("l2a> downlink packet received\n");
    PRINT_DBG("l2a> port: %d, ", l2a_ctx.receive_buffer[0]);
    PRINT_DBG("data len: %d, ",
              l2a_ctx.last_rcvd_data_len - LORAWAN_RULEID_BYTE_SIZE);
    PRINT_DBG("data:\n");
    PRINT_HEX_BUF_DBG(
        (uint8_t *)(&l2a_ctx.receive_buffer[LORAWAN_RULEID_BYTE_SIZE]),
        l2a_ctx.last_rcvd_data_len - LORAWAN_RULEID_BYTE_SIZE);

    l2a_ctx.callbacks.data_received(l2a_ctx.last_rcvd_data_len,
                                    l2a_ctx.last_data_rcvd_status);

    l2a_ctx.last_data_rcvd_status = L2A_SUCCESS;
    l2a_ctx.last_rcvd_data_len = 0;
  }

  return L2A_SUCCESS;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  *dev_iid = l2a_ctx.device_iid;

  if (l2_compute_iid(l2a_ctx.device_iid) != L2_SUCCESS)
  {
    return false;
  }
  return true;
}

/*
 * -----------------------------------------------------------------------------
 * --- L2 functions adaptation -------------------------------------------------
 */

void lorawan_process_joined_event(void)
{
  l2a_ctx.lora_events.joined = true;
  l2a_ctx.callbacks.processing_required();
}

void lorawan_process_tx_result_event(bool sending_error)
{
  if (!l2a_ctx.busy)
  {
    // drop the event as tx result happened when the stack is not busy
    return;
  }

  l2a_ctx.busy = false;

  if (sending_error)
  {
    l2a_action_tx_status(L2A_L2_ERROR);
    return;
  }

  if (!l2a_ctx.joined)
  {
    l2a_action_tx_status(L2A_CONNECT_ERR);
    return;
  }

  l2a_action_tx_status(L2A_SUCCESS);
}

void lorawan_process_downlink_rcvd_event(
    uint8_t port, const uint8_t *buffer, uint16_t data_len)
{
  uint16_t total_rcvd_data_len = data_len + LORAWAN_RULEID_BYTE_SIZE;

  if (!l2a_ctx.joined)
  {
    l2a_action_data_rcvd(L2A_CONNECT_ERR, total_rcvd_data_len);
    return;
  }

  if (total_rcvd_data_len > l2a_ctx.receive_buffer_size)
  {
    l2a_action_data_rcvd(L2A_BUFFER_ERR, total_rcvd_data_len);
    return;
  }

  // Concatenate port with the buffer
  l2a_ctx.receive_buffer[0] = port;
  memcpy(&l2a_ctx.receive_buffer[1], buffer, data_len);
  l2a_action_data_rcvd(L2A_SUCCESS, total_rcvd_data_len);
}

/*
 * -----------------------------------------------------------------------------
 * --- PRIVATE FUNCTIONS DEFINITION --------------------------------------------
 */

static void l2a_action_tx_status(l2a_status_t status)
{
  l2a_ctx.lora_events.tx_result = true;
  l2a_ctx.last_tx_status = status;
  l2a_ctx.callbacks.processing_required();
}

static void l2a_action_data_rcvd(l2a_status_t status, uint16_t data_len)
{
  l2a_ctx.lora_events.data_rcvd = true;
  l2a_ctx.last_data_rcvd_status = status;
  l2a_ctx.last_rcvd_data_len = data_len;
  l2a_ctx.callbacks.processing_required();
}
