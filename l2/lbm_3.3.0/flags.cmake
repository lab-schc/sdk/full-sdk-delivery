set(LBM_MAIN_SRC_DIR ${PROJECT_SOURCE_DIR}/libs/lbm_3.3.0)

set(L2_INC_DIR
  ${CMAKE_CURRENT_LIST_DIR}/inc
  ${PROJECT_SOURCE_DIR}/l2/common/inc
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_api
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lorawan_api
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lorawan_manager
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lorawan_packages
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lorawan_packages/fuota_packages
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/lr1mac_class_b
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/lr1mac_class_c
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/relay/common
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/relay/relay_ed
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/relay/relay_master
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/services
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/services/smtc_multicast
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/smtc_real/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/modem_supervisor
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/modem_utilities
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_planner/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_modem_crypto/
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_modem_crypto/smtc_secure_element
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_modem_crypto/soft_secure_element
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_ral/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_ralf/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/mcu_drivers/cmsis/Core/Include
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/mcu_drivers/cmsis/Device/ST/STM32L4xx/Include
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/mcu_drivers/core
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/radio_hal
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/smtc_hal_l4
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/smtc_modem_hal
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_hal
  ${VTAL_SRC_DIR}
  ${VTAL_SRC_DIR}/vTAL-src
)

if(${PLATFORM_RADIO_SHIELD} STREQUAL "LR1110" OR ${PLATFORM_RADIO_SHIELD} STREQUAL "LR1120")
  add_compile_definitions(LR11XX LR11XX_TRANSCEIVER)

  set(L2_INC_DIR
    ${L2_INC_DIR}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src)

  set(L2_STACK_SRC
    ${L2_STACK_SRC}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_ral/src/ral_lr11xx.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_ralf/src/ralf_lr11xx.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/radio_hal/lr11xx_hal.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/radio_hal/radio_utilities.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/radio_hal/ral_lr11xx_bsp.c)
elseif(${PLATFORM_RADIO_SHIELD} STREQUAL "SX1261" OR ${PLATFORM_RADIO_SHIELD} STREQUAL "SX1262" OR ${PLATFORM_RADIO_SHIELD} STREQUAL "SX1268")
  add_compile_definitions(SX126X)

  set(L2_INC_DIR
    ${L2_INC_DIR}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/sx126x_driver/src)

  set(L2_STACK_SRC
    ${L2_STACK_SRC}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_ral/src/ral_sx126x.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_ralf/src/ralf_sx126x.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/radio_hal/ral_sx126x_bsp.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/radio_hal/radio_utilities.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/radio_hal/sx126x_hal.c)
else()
  message(FATAL_ERROR "SHIELD ${PLATFORM_RADIO_SHIELD} is not supported")
endif()
