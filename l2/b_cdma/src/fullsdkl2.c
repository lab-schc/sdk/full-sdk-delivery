/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut@ackl.io
 *
 * B_CDMA specific L2 helper functions.
 */

#include <string.h>

#include "fullsdkl2.h"

static uint8_t dest_addr[BCDMA_MAC_ADDRESS_SIZE] = {};
static uint8_t src_addr[BCDMA_MAC_ADDRESS_SIZE] = {};

void bcdma_set_dest_addr(const uint8_t *addr)
{
  memcpy(dest_addr, addr, BCDMA_MAC_ADDRESS_SIZE);
}

void bcdma_set_src_addr(const uint8_t *addr)
{
  memcpy(src_addr, addr, BCDMA_MAC_ADDRESS_SIZE);
}

const uint8_t *bcdma_get_dest_addr(void)
{
  return dest_addr;
}

const uint8_t *bcdma_get_src_addr(void)
{
  return src_addr;
}