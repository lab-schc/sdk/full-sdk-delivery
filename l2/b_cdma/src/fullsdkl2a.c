/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut@ackl.io
 *
 * Mock BCDMA (UART) L2 layer interface.
 */

#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>

#include "fullsdkl2.h"
#include "fullsdkl2a.h"
#include "platform.h"

#define CRC16_POLY 0x8408

static l2a_callbacks_t l2a_cb;
static uint8_t *l2a_rx_buff;
static uint16_t l2a_rx_buff_size;
static int uart_fd;

static uint8_t bcdma_data[BCDMA_MAX_FRAME_SIZE] = {0};

typedef enum
{
  NO_EVENT = 0,
  CONNECTIVITY_AVAILABLE_EVENT = 1 << 0,
  TRANSMISSION_RESULT_EVENT = 1 << 1,
  DOWNLINK_AVAILABLE_EVENT = 1 << 2,
} l2_event_t;

static l2_event_t event = 0;

static void downlink_available(void)
{
  event |= DOWNLINK_AVAILABLE_EVENT;
  l2a_cb.processing_required();
}

l2a_status_t l2a_initialize(const l2a_callbacks_t *callbacks,
                            uint8_t *receive_buff, uint16_t receive_buff_size)
{
  struct termios serial;
  memset(&serial, 0, sizeof(struct termios));

  if (callbacks == NULL)
    return L2A_REQ_CALLBACK_ERR;

  if (((uart_fd = open(BCDMA_UART_FILE, O_RDWR | O_NOCTTY)) == -1) ||
      tcgetattr(uart_fd, &serial) != 0)
  {
    close(uart_fd);
    return L2A_CONNECT_ERR;
  }

  serial.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
  serial.c_iflag = IGNPAR;
  serial.c_iflag &= ~(IXON | IXOFF | IXANY);
  serial.c_oflag &= ~OPOST;
  serial.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
  serial.c_cc[VTIME] = 0;
  serial.c_cc[VMIN] = 1;

  if (tcflush(uart_fd, TCIFLUSH) == -1 ||
      tcsetattr(uart_fd, TCSANOW, &serial) == -1 ||
      !watch_fd_for_input(uart_fd, &downlink_available))
  {
    close(uart_fd);
    return L2A_L2_ERROR;
  }

  l2a_cb = *callbacks;
  l2a_rx_buff = receive_buff;
  l2a_rx_buff_size = receive_buff_size;

  event |= CONNECTIVITY_AVAILABLE_EVENT;
  l2a_cb.processing_required();

  return L2A_SUCCESS;
}

static uint16_t crc16(const uint8_t *data_p, uint16_t length)
{
  uint16_t i;
  unsigned int data;
  unsigned int crc = 0xffff;

  if (length == 0)
    return (uint16_t)(~crc);

  do
  {
    for (i = 0, data = (unsigned int)0xff & *data_p++; i < 8; i++, data >>= 1)
    {
      if ((crc & 0x0001) ^ (data & 0x0001))
        crc = (crc >> 1) ^ CRC16_POLY;
      else
        crc >>= 1;
    }
  } while (--length);

  crc = ~crc;
  data = crc;
  crc = (crc << 8) | (data >> 8 & 0xff);

  return (uint16_t)(crc);
}

// Wraps a SCHC fragment in a B-CDMA packet
static void wrap_in_bcdma(uint8_t *buff, const uint8_t *data,
                          uint16_t data_size, uint16_t *bcdma_size)
{
  uint16_t idx = 0;

  buff[idx] = BCDMA_START_OF_PACKET;
  idx++;
  buff[idx] = BCDMA_VERSION;
  idx++;

  memcpy(&buff[idx], bcdma_get_dest_addr(), BCDMA_MAC_ADDRESS_SIZE);
  idx += BCDMA_MAC_ADDRESS_SIZE;
  memcpy(&buff[idx], bcdma_get_src_addr(), BCDMA_MAC_ADDRESS_SIZE);
  idx += BCDMA_MAC_ADDRESS_SIZE;

  uint8_t *packet_len = (uint8_t *)&buff[idx];
  uint16_t packet_len_value = data_size + BCDMA_OVERHEAD;
  packet_len[0] = ((uint8_t *)&packet_len_value)[1];
  packet_len[1] = ((uint8_t *)&packet_len_value)[0];
  idx += BCDMA_PACKET_LEN_SIZE;
  // command (0x70 to 0x7F for SCHC use)
  buff[idx] = BCDMA_SCHC_CMD_START;
  idx++;
  // Status
  buff[idx] = BCDMA_STATUS_SUCCESS;
  idx++;
  // Link Quality Indicator
  buff[idx] = 0xFF;
  idx++;
  // RSSI
  buff[idx] = 0xFF;
  idx++;
  // data
  memcpy(&buff[idx], data, data_size);
  idx += data_size;
  // CRC
  uint8_t *crc = (uint8_t *)&buff[idx];
  uint16_t crc_value = crc16(buff, BCDMA_HEADER + data_size);
  crc[0] = ((uint8_t *)&crc_value)[1];
  crc[1] = ((uint8_t *)&crc_value)[0];
  idx += BCDMA_CRC_SIZE;
  buff[idx] = BCDMA_END_OF_PACKET;
  idx++;

  *bcdma_size = idx;
}

l2a_status_t l2a_send_data(const uint8_t *data, uint16_t data_size)
{
  uint16_t bcdma_size = 0;

  if (data_size + BCDMA_OVERHEAD > BCDMA_MAX_FRAME_SIZE)
    return L2A_BUFFER_ERR;

  wrap_in_bcdma(bcdma_data, data, data_size, &bcdma_size);

  if (write(uart_fd, bcdma_data, bcdma_size) == -1)
    return L2A_L2_ERROR;
  event |= TRANSMISSION_RESULT_EVENT;
  l2a_cb.processing_required();

  return L2A_SUCCESS;
}

l2a_technology_t l2a_get_technology(void)
{
  return L2A_DEFAULT;
}

uint16_t l2a_get_mtu(void)
{
  return BCDMA_MAX_PAYLOAD;
}

uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  (void)data_size;
  return BCDMA_NEXT_TX_DELAY;
}

static l2a_status_t parse_bcdma(const uint8_t *bcdma, uint16_t bcdma_size,
                                const uint8_t **data, uint16_t *data_size)
{
  uint16_t idx = 0;

  if (bcdma[idx] != BCDMA_START_OF_PACKET)
    return L2A_INTERNAL_ERR;
  idx++;
  if (bcdma[idx] != BCDMA_VERSION)
    return L2A_INTERNAL_ERR;
  idx++;
  // Dest MAC address
  idx += BCDMA_MAC_ADDRESS_SIZE;
  // Src MAC address
  idx += BCDMA_MAC_ADDRESS_SIZE;
  // the packet size has already been parsed during the reading phase
  if (bcdma_size > BCDMA_MAX_FRAME_SIZE)
    return L2A_INTERNAL_ERR;
  idx += BCDMA_PACKET_LEN_SIZE;
  // command (0x70 to 0x7F for SCHC use)
  if (bcdma[idx] < BCDMA_SCHC_CMD_START || bcdma[idx] > BCDMA_SCHC_CMD_END)
    return L2A_INTERNAL_ERR;
  idx++;
  // status 0x30 : Success
  if (bcdma[idx] != BCDMA_STATUS_SUCCESS)
    return L2A_INTERNAL_ERR;
  idx++;
  // Link Quality Indicator
  idx++;
  // RSSI
  idx++;
  // data
  *data = &bcdma[idx];
  *data_size = bcdma_size - BCDMA_OVERHEAD;
  idx += *data_size;
  // CRC
  uint8_t *crc = (uint8_t *)&bcdma[idx];
  uint16_t crc_value = 0;
  ((uint8_t *)&crc_value)[1] = crc[0];
  ((uint8_t *)&crc_value)[0] = crc[1];
  if (crc16(bcdma, BCDMA_HEADER + (*data_size)) != crc_value)
    return L2A_INTERNAL_ERR;
  idx += BCDMA_CRC_SIZE;
  if (bcdma[idx] != BCDMA_END_OF_PACKET)
    return L2A_INTERNAL_ERR;
  return L2A_SUCCESS;
}

static l2a_status_t read_downlink(void)
{
  l2a_status_t rx_status = L2A_SUCCESS;
  uint16_t rx_size = 0;
  uint8_t *data = NULL;
  uint16_t data_size = 0;

  while (rx_size < BCDMA_PACKET_LEN_IDX + BCDMA_PACKET_LEN_SIZE)
    rx_size += read(uart_fd, &l2a_rx_buff[rx_size], l2a_rx_buff_size - rx_size);

  uint8_t *packet_len = (uint8_t *)&l2a_rx_buff[BCDMA_PACKET_LEN_IDX];
  uint16_t packet_len_value = 0;
  ((uint8_t *)&packet_len_value)[1] = packet_len[0];
  ((uint8_t *)&packet_len_value)[0] = packet_len[1];

  while (rx_size < packet_len_value)
    rx_size += read(uart_fd, &l2a_rx_buff[rx_size], l2a_rx_buff_size - rx_size);

  if (rx_size <= 0)
    rx_status = L2A_L2_ERROR;
  if (parse_bcdma(l2a_rx_buff, packet_len_value, (const uint8_t **)&data,
                  &data_size) != L2A_SUCCESS)
    return L2A_INTERNAL_ERR;

  // Shift the SCHC fragment to the beginning of the buffer
  memcpy(l2a_rx_buff, data, data_size);
  l2a_cb.data_received(data_size, rx_status);
  return L2A_SUCCESS;
}

l2a_status_t l2a_process(void)
{
  if (event & CONNECTIVITY_AVAILABLE_EVENT)
    l2a_cb.connectivity_available();
  if (event & TRANSMISSION_RESULT_EVENT)
    l2a_cb.transmission_result(L2A_SUCCESS, 0);
  if (event & DOWNLINK_AVAILABLE_EVENT)
    read_downlink();
  event = NO_EVENT;
  return L2A_SUCCESS;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  (void)dev_iid;
  return false;
}
