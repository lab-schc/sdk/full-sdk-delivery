/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut@ackl.io
 *
 * B_CDMA specific L2 helper functions.
 */

#ifndef FULLSDK_L2_H_
#define FULLSDK_L2_H_

#include <stdint.h>

#define BCDMA_HEADER 20
// Part of the header located at the end of the packet
#define BCDMA_TAIL 3
#define BCDMA_OVERHEAD (BCDMA_HEADER + BCDMA_TAIL)
#define BCDMA_MAX_PAYLOAD 105
#define BCDMA_MAX_FRAME_SIZE (BCDMA_OVERHEAD + BCDMA_MAX_PAYLOAD)

#define BCDMA_START_OF_PACKET 0x02
#define BCDMA_VERSION 0x33
#define BCDMA_STATUS_SUCCESS 0x30
#define BCDMA_END_OF_PACKET 0x03

// Index in a BCDMA packet where the packet length value can be retrieved
#define BCDMA_PACKET_LEN_IDX 14

#define BCDMA_MAC_ADDRESS_SIZE 6
#define BCDMA_CRC_SIZE 2
#define BCDMA_PACKET_LEN_SIZE 2

#define BCDMA_SCHC_CMD_START 0x70
#define BCDMA_SCHC_CMD_END 0x7F

#define BCDMA_NEXT_TX_DELAY 500 // in ms

#ifndef BCDMA_UART_FILE
#define BCDMA_UART_FILE "/dev/ttyO4"
#endif // BCDMA_UART_FILE

/**
 * Copies the content of addr into the internal buffer
 * holding the BCDMA destination MAC address (6 bytes)
 */
void bcdma_set_dest_addr(const uint8_t *addr);

/**
 * Copies the content of addr into the internal buffer
 * holding the BCDMA source MAC address (6 bytes)
 */
void bcdma_set_src_addr(const uint8_t *addr);

/**
 * Returns a pointer to the internal buffer
 * holding the BCDMA destination MAC address
 */
const uint8_t *bcdma_get_dest_addr(void);

/**
 * Returns a pointer to the internal buffer
 * holding the BCDMA source MAC address
 */
const uint8_t *bcdma_get_src_addr(void);

#endif // FULLSDK_L2_H_