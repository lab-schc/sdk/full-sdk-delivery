/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#ifndef FULLSDKL2_H_
#define FULLSDKL2_H_

#include <stdbool.h>
#include <stdint.h>

#include "fullsdkl2common.h"

#define NEXT_TX_DELAY 2000 // 2s arbitrary value
#define MTU 1500           // To match recommended ipv6 MTU

void l2_set_ipv4_host_addr(const char *addr);
void l2_set_ipv4_remote_addr(const char *addr);
void l2_set_udp_dest_port(const char *port);
void l2_set_udp_src_port(const char *port);
bool l2_set_serial_port(const char *port_path);

l2_status_t sock_init(void);
void sock_uninit(void);
l2_status_t sock_connect(void);
l2_status_t sock_receive(uint8_t *data, uint16_t *size);
l2_status_t sock_poll_downlink(bool block);
l2_status_t sock_send_data(const uint8_t *p_data, uint16_t data_size);

#endif
