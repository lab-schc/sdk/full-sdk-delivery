/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fullsdkl2.h"
#include "serial.h"

#define MAX_CMD_SIZE 64
#define IO_BUFF_SIZE (MTU * 2 + MAX_CMD_SIZE)

static char serial_port[100] = SERIAL_PATH;
static const uint8_t *l2_remote_addr = NULL;
static const uint8_t *l2_remote_port = NULL;
static const uint8_t *l2_local_port = NULL;
static uint8_t socket_id = -1;

static char io_buff[IO_BUFF_SIZE] = {0};
static uint16_t rx_size = 0;

static const char sock_needle[] = "Socket created socket_id=";
static const char data_needle[] = "Received data for socket";
static const char rx_size_needle[] = "buffer_size=";
static const char conn_lost_needle[] = "closing";

void l2_set_ipv4_host_addr(const char *addr)
{
  (void)addr;
}

void l2_set_ipv4_remote_addr(const char *addr)
{
  l2_remote_addr = addr;
}

void l2_set_udp_dest_port(const char *port)
{
  l2_remote_port = port;
}

void l2_set_udp_src_port(const char *port)
{
  l2_local_port = port;
}

bool l2_set_serial_port(const char *port_path)
{
  if (port_path == NULL)
  {
    return false;
  }
  size_t port_path_size = strlen(port_path);
  if (port_path_size > sizeof(serial_port))
  {
    return false;
  }
  memcpy(serial_port, port_path, port_path_size);
  return true;
}

l2_status_t sock_init(void)
{
  if (serial_init(serial_port, NRF9160_BAUDRATE) != SERIAL_SUCCESS)
  {
    return L2_INTERN_ERR;
  }
  return L2_SUCCESS;
}

void sock_uninit(void)
{
  sprintf(io_buff, "sock close -i %d\n", socket_id);
  serial_writeline(io_buff);
  serial_fini();
}

l2_status_t sock_connect(void)
{
  char *substr = NULL;

  sprintf(io_buff, "sock connect -b %s -a %s -p %s -t dgram\n", l2_local_port,
          l2_remote_addr, l2_remote_port);
  if (serial_writeline(io_buff) != (ssize_t)strlen(io_buff))
  {
    return L2_INTERN_ERR;
  }
  // Read the echo of what we wrote, then info about socket and
  // finally there is the line with the socket ID
  // Since all these prints are done from inside the function creating the
  // socket there is no need to do an asynchronous polling
  // We loop a bit more than necessary in order to ensure parasite logs dont make the function fail
  for (uint8_t i = 0; i < 5; i++)
  {
    if (serial_readline(io_buff) == -1)
    {
      return L2_INTERN_ERR;
    }
    if ((substr = strstr(io_buff, sock_needle)) != NULL)
    {
      break;
    }
  }
  if (substr == NULL)
  {
    return L2_INTERN_ERR;
  }
  sscanf(substr + sizeof(sock_needle) - 1, "%d", &socket_id);
  // Make mosh print received data as ascii hex
  sprintf(io_buff, "sock recv -i %d -P hex\n", socket_id);
  if (serial_writeline(io_buff) != (ssize_t)strlen(io_buff))
  {
    sock_uninit();
    return L2_INTERN_ERR;
  }
  // Disable edrx and psm features to ensure better stability
  sprintf(io_buff, "link edrx -d\n");
  if (serial_writeline(io_buff) != (ssize_t)strlen(io_buff))
  {
    sock_uninit();
    return L2_INTERN_ERR;
  }
  sprintf(io_buff, "link psm -d\n");
  if (serial_writeline(io_buff) != (ssize_t)strlen(io_buff))
  {
    sock_uninit();
    return L2_INTERN_ERR;
  }
  return L2_SUCCESS;
}

static uint8_t str_hex_to_hex(const uint8_t *str)
{
  uint8_t base[] = "0123456789ABCDEF";
  uint8_t ret = 0;

  for (uint8_t i = 0; i < 2; i++)
  {
    for (uint8_t j = 0; j < sizeof(base) - 1; j++)
    {
      if (base[j] == str[i])
      {
        ret += j;
        if (i == 0)
        {
          ret = ret << 4;
        }
        break;
      }
    }
  }
  return ret;
}

l2_status_t sock_receive(uint8_t *data, uint16_t *size)
{
  // Need to parse multiple lines in order to get the whole payload
  uint16_t bytes_read = 0;
  char *substr = NULL;
  do
  {
    if (serial_readline(io_buff) == -1)
    {
      return L2_INTERN_ERR;
    }
    if ((substr = strstr(io_buff, "\t")) == NULL)
    {
      return L2_INTERN_ERR;
    }
    // Skip the tab
    substr++;
    // Modem shell writes at most 8 bytes/line in hex
    uint16_t nb_loops = rx_size - bytes_read >= 8 ? 8 : (rx_size % 8);
    for (uint16_t j = 0; j < nb_loops; j++)
    {
      // For some reason sscanf didnt work here
      data[bytes_read] = str_hex_to_hex(substr + 2);
      bytes_read++;
      // Skip to the next hex number
      substr += sizeof("0xXX ") - 1; // Trailing null byte is counted in literals size
    }
  } while (bytes_read < rx_size);

  *size = rx_size;
  rx_size = 0;
  return L2_SUCCESS;
}

l2_status_t sock_poll_downlink(bool block)
{
  char *substr = NULL;
  l2_status_t status = L2_SUCCESS;

  if (serial_poll(block) == SERIAL_INTERN_ERR)
  {
    return L2_INTERN_ERR;
  }
  serial_readline(io_buff);
  if (strstr(io_buff, data_needle) != NULL)
  {
    // Look for the size of the data received
    if ((substr = strstr(io_buff, rx_size_needle)) == NULL)
    {
      return L2_INTERN_ERR;
    }
    sscanf(substr + sizeof(rx_size_needle) - 1, "%d", &rx_size);
    status = L2_DOWN_AVAIL;
  }
  else if (strstr(io_buff, conn_lost_needle) != NULL)
  {
    status = L2_CONN_LOST;
  }
  return status;
}

l2_status_t sock_send_data(const uint8_t *p_data, uint16_t data_size)
{
  int written = 0;

  if ((written = sprintf(io_buff, "sock send -i %d -xd ", socket_id)) < 0)
  {
    return L2_INTERN_ERR;
  }
  for (uint16_t i = 0; i < data_size; i++)
  {
    if (sprintf(&io_buff[i * 2 + written], "%.02x", p_data[i]) < 0)
    {
      return L2_INTERN_ERR;
    }
  }
  io_buff[data_size * 2 + written] = '\n';
  io_buff[data_size * 2 + written + 1] = '\0';

  if (serial_writeline(io_buff) == -1)
  {
    return L2_INTERN_ERR;
  }
  return L2_SUCCESS;
}