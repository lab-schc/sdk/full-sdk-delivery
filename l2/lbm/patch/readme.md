# Readme

* The `smtc_hal_uart.c` patch sets the UART speed to 9600 b/s and adds reception handling.
* The `soft_se.c` patch lets use `get_key_by_id` function in Dev IID computation.
* The `stm32l4xx_hal_uart.c` patch prevents an ST HAL problem related to the UART RX interruption from occuring. The problem is similar to the one presented [here](https://github.com/STMicroelectronics/STM32CubeWL/issues/34)
