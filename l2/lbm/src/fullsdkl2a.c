/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * LBM L2A layer interface.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "fullsdkl2.h"
#include "fullsdkl2a.h"
#include "platform.h"
#include "smtc_modem_api.h"

#define LORAWAN_RULEID_BYTE_SIZE 1
#define STACK_ID 0             // we support single lorawan stack for now
#define MIN_NEXT_TX_DELAY 1000 // wait at least 1 second when we have tx credit

l2a_technology_t l2a_technology = L2A_DEFAULT;

static void l2a_action_tx_status(l2a_status_t status);
static void l2a_action_data_rcvd(l2a_status_t status, uint16_t data_len,
                                 smtc_modem_event_downdata_window_t window);

typedef struct
{
  uint8_t joined : 1;
  uint8_t tx_result : 1;
  uint8_t data_rcvd : 1;
  uint8_t rfu : 5;
} lorawan_events_t;

typedef struct
{
  bool joined;
  bool busy;
  l2a_callbacks_t callbacks;
  uint8_t *receive_buffer;
  uint16_t receive_buffer_size;
  uint32_t next_tx_delay;
  lorawan_events_t lora_events;
  l2a_status_t last_tx_status;
  l2a_status_t last_data_rcvd_status;
  uint16_t last_rcvd_data_len;
  smtc_modem_event_downdata_window_t last_rcvd_window;
  uint8_t device_iid[8];
} l2a_ctx_t;

static l2a_ctx_t l2a_ctx;

l2a_status_t l2a_initialize(const l2a_callbacks_t *callbacks,
                            uint8_t *receive_buffer,
                            uint16_t receive_buffer_size)
{
  memset(&l2a_ctx, 0, sizeof(l2a_ctx));

  if (callbacks == NULL || callbacks->processing_required == NULL ||
      callbacks->transmission_result == NULL ||
      callbacks->data_received == NULL ||
      callbacks->connectivity_available == NULL)
  {
    return L2A_REQ_CALLBACK_ERR;
  }

  l2a_ctx.callbacks = *callbacks;
  l2a_ctx.receive_buffer = receive_buffer;
  l2a_ctx.receive_buffer_size = receive_buffer_size;
  l2a_ctx.last_tx_status = L2A_SUCCESS;

  uint32_t dc;

  if (l2_join(l2_get_default_datarate(), &dc) != L2_SUCCESS)
  {
    PRINT_DBG("l2a> init error\n");
    return L2A_L2_ERROR;
  }

  PRINT_DBG("l2a> network join request sent\n");

  return L2A_SUCCESS;
}

l2a_status_t l2a_send_data(const uint8_t *buffer, uint16_t data_len)
{
  if (data_len < LORAWAN_RULEID_BYTE_SIZE)
  {
    return L2A_INTERNAL_ERR;
  }

  if (!l2a_ctx.joined)
  {
    l2a_action_tx_status(L2A_CONNECT_ERR);
    return L2A_SUCCESS;
  }

  if (l2a_ctx.busy)
  {
    l2a_action_tx_status(L2A_BUSY_ERR);
    return L2A_SUCCESS;
  }

  l2_app_data_t app_data;
  uint32_t dc;

  app_data.type = L2_MSG_TYPE_CONFIRMED;
  app_data.port = buffer[0];
  app_data.size = data_len - LORAWAN_RULEID_BYTE_SIZE;
  app_data.buffer = (uint8_t *)(&buffer[LORAWAN_RULEID_BYTE_SIZE]);

  if (l2_send(app_data, l2_get_default_datarate(), &dc) != L2_SUCCESS)
  {
    l2a_action_tx_status(L2A_L2_ERROR);
    return L2A_SUCCESS;
  }

  PRINT_DBG("l2a> send data request done\n");
  PRINT_DBG("l2a> port: %d, ", app_data.port);
  PRINT_DBG("unconfirmed, ");
  PRINT_DBG("data len: %d, ", app_data.size);
  PRINT_DBG("data:\n");
  PRINT_HEX_BUF_DBG(app_data.buffer, app_data.size);

  l2a_ctx.busy = true;

  return L2A_SUCCESS;
}

l2a_technology_t l2a_get_technology(void)
{
  return l2a_technology;
}

uint16_t l2a_get_mtu(void)
{
  uint8_t mtu = 0;

  smtc_modem_return_code_t status =
      smtc_modem_get_next_tx_max_payload(STACK_ID, &mtu);
  PRINT_DBG("l2a> mtu: %d\n", mtu);
  if (status != SMTC_MODEM_RC_OK)
  {
    mtu = 0;
    PRINT_DBG("l2a> mtu get error, returning: %d\n", mtu);
  }

  return mtu;
}

uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  int32_t next_tx_delay;

  smtc_modem_return_code_t status =
      smtc_modem_get_duty_cycle_status(&next_tx_delay);
  if (status != SMTC_MODEM_RC_OK)
  {
    next_tx_delay = UINT32_MAX;
    PRINT_DBG("l2a> next tx delay get error, returning: %d ms\n",
              next_tx_delay);
  }

  if (next_tx_delay < 0)
  {
    next_tx_delay = -next_tx_delay;

    PRINT_DBG("l2a> next tx delay: %d ms\n", next_tx_delay);

    return (uint32_t)next_tx_delay;
  }

  PRINT_DBG("l2a> remaining time credit: %d ms\n", next_tx_delay);

  return MIN_NEXT_TX_DELAY;
}

l2a_status_t l2a_process(void)
{
  if (l2a_ctx.lora_events.joined)
  {
    l2a_ctx.lora_events.joined = false;

    l2a_ctx.joined = true;
    l2a_ctx.callbacks.connectivity_available();
  }

  if (l2a_ctx.lora_events.tx_result)
  {
    l2a_ctx.lora_events.tx_result = false;

    l2a_ctx.callbacks.transmission_result(l2a_ctx.last_tx_status, 0);

    PRINT_DBG("l2a> tx done\n");

    l2a_ctx.last_tx_status = L2A_SUCCESS;
  }

  if (l2a_ctx.lora_events.data_rcvd)
  {
    l2a_ctx.lora_events.data_rcvd = false;

    PRINT_DBG("l2a> downlink packet received\n");
    PRINT_DBG("l2a> port: %d, ", l2a_ctx.receive_buffer[0]);
    PRINT_DBG("data len: %d, ",
              l2a_ctx.last_rcvd_data_len - LORAWAN_RULEID_BYTE_SIZE);
    if (l2a_ctx.last_rcvd_window == SMTC_MODEM_EVENT_DOWNDATA_WINDOW_RX1)
    {
      PRINT_DBG("window: %s, ", "rx1");
    }
    else if (l2a_ctx.last_rcvd_window == SMTC_MODEM_EVENT_DOWNDATA_WINDOW_RX2)
    {
      PRINT_DBG("window: %s, ", "rx2");
    }
    else if (l2a_ctx.last_rcvd_window == SMTC_MODEM_EVENT_DOWNDATA_WINDOW_RXC)
    {
      PRINT_DBG("window: %s, ", "rxc");
    }
    else // multicast
    {
      PRINT_DBG("window: %s, ", "mc");
    }
    PRINT_DBG("data:\n");
    PRINT_HEX_BUF_DBG(
        (uint8_t *)(&l2a_ctx.receive_buffer[LORAWAN_RULEID_BYTE_SIZE]),
        l2a_ctx.last_rcvd_data_len - LORAWAN_RULEID_BYTE_SIZE);

    l2a_ctx.callbacks.data_received(l2a_ctx.last_rcvd_data_len,
                                    l2a_ctx.last_data_rcvd_status);

    l2a_ctx.last_data_rcvd_status = L2A_SUCCESS;
    l2a_ctx.last_rcvd_data_len = 0;
  }

  return L2A_SUCCESS;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  *dev_iid = l2a_ctx.device_iid;

  if (l2_compute_iid(l2a_ctx.device_iid) != L2_SUCCESS)
  {
    return false;
  }
  return true;
}

void lorawan_process_joined_event(void)
{
  l2a_ctx.lora_events.joined = true;
  l2a_ctx.callbacks.processing_required();
}

void lorawan_process_tx_result_event(uint8_t status)
{
  if (!l2a_ctx.busy)
  {
    // drop the event as tx result happened when the stack is not busy
    return;
  }

  l2a_ctx.busy = false;

  if (status != SMTC_MODEM_EVENT_TXDONE_SENT)
  {
    l2a_action_tx_status(L2A_L2_ERROR);
    return;
  }

  if (!l2a_ctx.joined)
  {
    l2a_action_tx_status(L2A_CONNECT_ERR);
    return;
  }

  l2a_action_tx_status(L2A_SUCCESS);
}

void lorawan_process_downlink_rcvd_event(
    uint8_t port, const uint8_t *buffer, uint16_t data_len,
    smtc_modem_event_downdata_window_t window)
{
  uint16_t total_rcvd_data_len = data_len + LORAWAN_RULEID_BYTE_SIZE;

  if (!l2a_ctx.joined)
  {
    l2a_action_data_rcvd(L2A_CONNECT_ERR, total_rcvd_data_len, window);
    return;
  }

  if (total_rcvd_data_len > l2a_ctx.receive_buffer_size)
  {
    l2a_action_data_rcvd(L2A_BUFFER_ERR, total_rcvd_data_len, window);
    return;
  }

  // Concatenate port with the buffer
  l2a_ctx.receive_buffer[0] = port;
  memcpy(&l2a_ctx.receive_buffer[1], buffer, data_len);
  l2a_action_data_rcvd(L2A_SUCCESS, total_rcvd_data_len, window);
}

static void l2a_action_tx_status(l2a_status_t status)
{
  l2a_ctx.lora_events.tx_result = true;
  l2a_ctx.last_tx_status = status;
  l2a_ctx.callbacks.processing_required();
}

static void l2a_action_data_rcvd(l2a_status_t status, uint16_t data_len,
                                 smtc_modem_event_downdata_window_t window)
{
  l2a_ctx.lora_events.data_rcvd = true;
  l2a_ctx.last_data_rcvd_status = status;
  l2a_ctx.last_rcvd_data_len = data_len;
  l2a_ctx.last_rcvd_window = window;
  l2a_ctx.callbacks.processing_required();
}