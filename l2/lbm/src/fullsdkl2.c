/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Authors: Aydogan Ersoz aydogan.ersoz@ackl.io
 *          Pascal Bodin  pascal@ackl.io
 *
 * LBM L2 configuration layer.
 */

#include "fullsdkl2.h"
#include "cmac.h"
#include "platform.h"

#include "apps_modem_event.h"
#include "lorawan_key_config.h"
#include "lorawan_api.h"
#include "main_dm_info.h"
#include "ralf_drv.h"
#include "smtc_board.h"
#include "smtc_board_ralf.h"
#include "smtc_hal_mcu.h"
#include "smtc_modem_utilities.h"
#include "smtc_secure_element.h"

#define STACK_ID (0)

typedef struct soft_se_key_s
{
  smtc_se_key_identifier_t key_id;     //!< Key identifier
  uint8_t key_value[SMTC_SE_KEY_SIZE]; //!< Key value
} soft_se_key_t;

smtc_se_return_code_t get_key_by_id(smtc_se_key_identifier_t key_id,
                                    soft_se_key_t **key_item);

static l2_msg_type_t l2_default_message_type = L2_MSG_TYPE_UNCONFIRMED;
static const uint8_t default_datarate = 3;
static uint8_t current_datarate = default_datarate;
static l2_region_id_t active_region = SMTC_MODEM_REGION_EU_868;
static uint8_t user_dev_eui[8] = LORAWAN_DEVICE_EUI;
static uint8_t user_join_eui[8] = LORAWAN_JOIN_EUI;
static uint8_t user_app_key[16] = LORAWAN_APP_KEY;
static bool l2_init_done = false;
static ralf_t *modem_radio;

static void on_modem_reset(uint16_t reset_count);
static void on_modem_network_joined(void);
static void on_modem_down_data(int8_t rssi, int8_t snr,
                               smtc_modem_event_downdata_window_t rx_window, uint8_t port,
                               const uint8_t *payload, uint8_t size);
static void on_modem_tx_done(smtc_modem_event_txdone_status_t status);

static apps_modem_event_callback_t smtc_event_callback = {
    .adr_mobile_to_static = NULL,
    .alarm = NULL,
    .almanac_update = NULL,
    .down_data = on_modem_down_data,
    .join_fail = NULL,
    .joined = on_modem_network_joined,
    .link_status = NULL,
    .mute = NULL,
    .new_link_adr = NULL,
    .reset = on_modem_reset,
    .set_conf = NULL,
    .stream_done = NULL,
    .time_updated_alc_sync = NULL,
    .tx_done = on_modem_tx_done,
    .upload_done = NULL,
};

l2_region_t regions[L2_MAX_SUPPORTED_REGION] = {
    {.str = "AS923_1", .id = SMTC_MODEM_REGION_AS_923_GRP1},
    {.str = "AS923_2", .id = SMTC_MODEM_REGION_AS_923_GRP2},
    {.str = "AS923_3", .id = SMTC_MODEM_REGION_AS_923_GRP3},
    {.str = "AU915", .id = SMTC_MODEM_REGION_AU_915},
    {.str = "CN470", .id = SMTC_MODEM_REGION_CN_470},
    {.str = "CN470_RP10", .id = SMTC_MODEM_REGION_CN_470_RP_1_0},
    {.str = "WW2G4", .id = SMTC_MODEM_REGION_WW2G4},
    {.str = "EU868", .id = SMTC_MODEM_REGION_EU_868},
    {.str = "KR920", .id = SMTC_MODEM_REGION_KR_920},
    {.str = "IN865", .id = SMTC_MODEM_REGION_IN_865},
    {.str = "US915", .id = SMTC_MODEM_REGION_US_915},
    {.str = "RU864", .id = SMTC_MODEM_REGION_RU_864}};

const char *l2_get_version(void)
{
  return "LoRa Basics Modem - v2.1.0";
}

l2_status_t l2_init(void)
{
  l2_init_done = true;

  // Initialize the ralf_t object corresponding to the board.
  modem_radio = smtc_board_initialise_and_get_ralf();

  // Disable IRQ to avoid unwanted behaviour during init.
  hal_mcu_disable_irq();

  // Init board and peripherals.
  smtc_board_init_periph();

  // Init the Lora Basics Modem event callbacks.
  apps_modem_event_init(&smtc_event_callback);

  // Init the modem and use apps_modem_event_process as event callback,
  // please note that the callback will be called immediately after
  // the first call to modem_run_engine because of the reset detection.
  // Reception of the reset event will trigger the join request
  // (check on_modem_reset() callback function).
  smtc_modem_init(modem_radio, &apps_modem_event_process);

  // Re-enable IRQ.
  hal_mcu_enable_irq();

  return L2_SUCCESS;
}

l2_status_t l2_get_class(char *class)
{
  smtc_modem_class_t value;

  smtc_modem_get_class(STACK_ID, &value);

  // Default class A
  if (value == SMTC_MODEM_CLASS_A)
  {
    *class = 'A';
  }
  // Class C
  else if (value == SMTC_MODEM_CLASS_C)
  {
    *class = 'C';
  }
  else
  {
    // Class B
    *class = 'B';
  }

  return L2_SUCCESS;
}

l2_status_t l2_set_class(char class)
{
  if (class == 'A')
  {
    smtc_modem_set_class(STACK_ID, SMTC_MODEM_CLASS_A);
  }
  else if (class == 'C')
  {
    smtc_modem_set_class(STACK_ID, SMTC_MODEM_CLASS_C);
  }
  else
  {
    PRINT_DBG("l2a> unsupported class\n");
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_network_id(uint32_t *value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_network_id(uint32_t value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_public_network(bool *value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_public_network(bool status)
{
  return L2_NOT_SUPPORTED_ERR;
}

uint8_t l2_get_default_datarate(void)
{
  return default_datarate;
}

l2_status_t l2_join(uint8_t join_dr, uint32_t *duty_cycle_wait_time)
{
  smtc_modem_return_code_t status;

  (void)join_dr;              // not used
  (void)duty_cycle_wait_time; // not used

  // Call this function to be able to leave the network and rejoin later.
  // It is useful especially with AT command modem in order to avoid to reset
  // the board when we need to rejoin.
  status = smtc_modem_leave_network(STACK_ID);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> network rejoin failed: %d\n", status);
    return L2_ERROR;
  }

  if (l2_set_dev_eui(user_dev_eui) != L2_SUCCESS)
  {
    return L2_ERROR;
  }

  if (l2_set_join_eui(user_join_eui) != L2_SUCCESS)
  {
    return L2_ERROR;
  }

  if (l2_set_app_key(user_app_key) != L2_SUCCESS)
  {
    return L2_ERROR;
  }

  if (l2_set_active_region(active_region) != L2_SUCCESS)
  {
    return L2_ERROR;
  }

  status = smtc_modem_join_network(STACK_ID);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> network join failed: %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_set_active_region(l2_region_id_t region)
{
  smtc_modem_return_code_t status;

  active_region = region;

  status = smtc_modem_set_region(STACK_ID, region);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> region set failed: %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_active_region(l2_region_id_t *region)
{
  smtc_modem_return_code_t status;

  status = smtc_modem_get_region(STACK_ID, region);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> region get failed: %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

void l2_set_technology(l2a_technology_t technology)
{
  l2a_technology = technology;
}

void l2_set_message_type(l2_msg_type_t message_type)
{
  l2_default_message_type = message_type;
}

l2_msg_type_t l2_get_message_type(void)
{
  return l2_default_message_type;
}

l2_status_t l2_send(l2_app_data_t app_data, uint8_t datarate,
                    uint32_t *duty_cycle_wait_time)
{
  l2_op_status_t status;

  // Handle empty payload.
  if (app_data.size == 0)
  {
    status = smtc_modem_request_empty_uplink(STACK_ID, false, 0,
                                             l2_default_message_type);
    if (status != SMTC_MODEM_RC_OK)
    {
      PRINT_DBG("l2a> send empty uplink request failed %d\n", status);
      return L2_ERROR;
    }

    return L2_SUCCESS;
  }

  status = smtc_modem_request_uplink(STACK_ID, app_data.port,
                                     l2_default_message_type, app_data.buffer,
                                     app_data.size);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> send data request failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_adr(bool *adr)
{
  smtc_modem_adr_profile_t value;
  smtc_modem_return_code_t status;

  status = smtc_modem_adr_get_profile(STACK_ID, &value);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> get adr failed %d\n", status);
    return L2_ERROR;
  }

  if (value == SMTC_MODEM_ADR_PROFILE_NETWORK_CONTROLLED)
  {
    *adr = true;
  }
  else
  {
    *adr = false;
  }

  return L2_SUCCESS;
}

l2_status_t l2_set_adr(bool adr)
{
  if (adr)
  {
    smtc_modem_return_code_t status;

    status = smtc_modem_adr_set_profile(
        STACK_ID, SMTC_MODEM_ADR_PROFILE_NETWORK_CONTROLLED, NULL);
    if (status != SMTC_MODEM_RC_OK)
    {
      PRINT_DBG("l2a> set adr failed %d\n", status);
      return L2_ERROR;
    }
  }
  else
  {
    l2_set_dr(current_datarate);
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_dr(uint8_t *dr)
{
  *dr = current_datarate;

  return L2_SUCCESS;
}

l2_status_t l2_set_dr(uint8_t dr)
{
  smtc_modem_return_code_t status;
  uint8_t custom_datarate[SMTC_MODEM_CUSTOM_ADR_DATA_LENGTH];

  for (uint8_t i = 0; i < sizeof(custom_datarate); i++)
  {
    custom_datarate[i] = dr;
  }

  current_datarate = dr;
  status = smtc_modem_adr_set_profile(STACK_ID, SMTC_MODEM_ADR_PROFILE_CUSTOM,
                                      custom_datarate);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> adr set profile failed failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_dutycycle(bool *value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_dutycycle(bool dutycycle_on)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_nwk_s_key(uint8_t *nwk_s_key)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_nwk_s_key(uint8_t *nwk_s_key)
{
  return L2_NOT_SUPPORTED_ERR;
}

uint8_t *l2_get_app_key(void)
{
  // app key is nwk key in lorawan v1.0.x
  return user_app_key;
}

l2_status_t l2_set_app_key(uint8_t *value)
{
  smtc_modem_return_code_t status;

  // app key is nwk key in lorawan v1.0.x
  memcpy(user_app_key, value, SMTC_MODEM_KEY_LENGTH);

  status = smtc_modem_set_nwkkey(STACK_ID, value);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> set app key failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_get_app_s_key(uint8_t *value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_app_s_key(uint8_t *app_s_key)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_dev_addr(uint32_t *value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_dev_addr(uint32_t dev_addr)
{
  return L2_NOT_SUPPORTED_ERR;
}

uint8_t *l2_get_join_eui(void)
{
  return user_join_eui;
}

l2_status_t l2_set_join_eui(uint8_t *value)
{
  smtc_modem_return_code_t status;

  memcpy(user_join_eui, value, SMTC_MODEM_EUI_LENGTH);

  status = smtc_modem_set_joineui(STACK_ID, value);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> set join eui failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

uint8_t *l2_get_dev_eui(void)
{
  return user_dev_eui;
}

l2_status_t l2_set_dev_eui(uint8_t *value)
{
  smtc_modem_return_code_t status;

  memcpy(user_dev_eui, value, SMTC_MODEM_EUI_LENGTH);

  status = smtc_modem_set_deveui(STACK_ID, value);
  if (status != SMTC_MODEM_RC_OK)
  {
    PRINT_DBG("l2a> set dev eui failed %d\n", status);
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t l2_redirect_next_mcps_confirm_event(void (*cb)(bool has_error))
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_join_status(uint8_t *status)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t
l2_get_network_activation(l2_network_activation_type_t *activation_type)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t
l2_set_network_activation(l2_network_activation_type_t activation_type)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_system_max_rx_error(uint32_t *value)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_system_max_rx_error(uint32_t sys_max_rx_error)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_join_accept_delay1(uint32_t *delay)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_join_accept_delay1(uint32_t delay)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_join_accept_delay2(uint32_t *delay)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_join_accept_delay2(uint32_t delay)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_rx1_delay(uint32_t *delay)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_rx1_delay(uint32_t delay)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_rx2_delay(uint32_t *delay)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_rx2_dr(uint8_t *dr)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_rx2_dr(uint8_t dr)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_rx2_delay(uint32_t delay)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_rx2_frequency(uint32_t *freq)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_rx2_frequency(uint32_t freq)
{
  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_cert_mode(void)
{
  return L2_NOT_SUPPORTED_ERR;
}

int16_t l2_get_last_snr(void)
{
  return lorawan_api_last_snr_get();
}

int16_t l2_get_last_rssi(void)
{
  return lorawan_api_last_rssi_get();
}

// Defined in soft-se.c
smtc_se_return_code_t get_key_by_id(smtc_se_key_identifier_t key_id, soft_se_key_t **key_item);

l2_status_t l2_compute_iid(uint8_t *iid)
{
  soft_se_key_t *app_s_key;
  uint8_t cmac[16];
  AES_CMAC_CTX aes_cmac_ctx;

  smtc_se_return_code_t status = get_key_by_id(SMTC_SE_APP_S_KEY, &app_s_key);
  if (status != SMTC_SE_RC_SUCCESS)
  {
    return L2_ERROR;
  }

  AES_CMAC_Init(&aes_cmac_ctx);
  AES_CMAC_SetKey(&aes_cmac_ctx, app_s_key->key_value);
  AES_CMAC_Update(&aes_cmac_ctx, user_dev_eui, 8);
  AES_CMAC_Final(cmac, &aes_cmac_ctx);

  memcpy(iid, cmac, 8);

  return L2A_SUCCESS;
}

uint32_t l2_periodic_scheduler(void)
{
  // we should not poll if the modem is not initialized
  if (!l2_init_done)
  {
    return UINT32_MAX;
  }

  return smtc_modem_run_engine();
}

static void on_modem_reset(uint16_t reset_count)
{
  PRINT_DBG("l2a> on_modem_reset\n");
  // Inhibit device management service.
  smtc_modem_dm_set_info_interval(APP_MODEM_DM_FORMAT, 0);
}

static void on_modem_network_joined(void)
{
  PRINT_DBG("l2a> on_modem_network_joined\n");
  // To avoid extra uplinks for ADR (ADR is default in LBM).
  l2_set_adr(false);
  lorawan_process_joined_event();
}

static void on_modem_down_data(int8_t rssi, int8_t snr,
                               smtc_modem_event_downdata_window_t rx_window, uint8_t port,
                               const uint8_t *payload, uint8_t size)
{
  PRINT_DBG("l2a> on_modem_down_data\n");
  lorawan_process_downlink_rcvd_event(port,
                                      payload,
                                      size,
                                      rx_window);
}

static void on_modem_tx_done(smtc_modem_event_txdone_status_t status)
{
  PRINT_DBG("l2a> on_modem_tx_done\n");
  lorawan_process_tx_result_event(status);
}
