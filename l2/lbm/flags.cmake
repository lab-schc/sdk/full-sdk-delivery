set(LBM_MAIN_SRC_DIR ${PROJECT_SOURCE_DIR}/libs/lbm)
set(VTAL_SRC_DIR ${PROJECT_SOURCE_DIR}/libs/vTAL/vTAL)

set(L2_INC_DIR ${L2_INC_DIR}
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/lora_basics_modem/smtc_modem_core/lr1mac/src/lr1mac_class_b/
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/lora_basics_modem/smtc_modem_core/modem_config/
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/lora_basics_modem/smtc_modem_api/
  ${LBM_MAIN_SRC_DIR}/host_driver/STMicroelectronics/STM32L4xx/Drivers/CMSIS/Include
  ${PROJECT_SOURCE_DIR}/l2/${L2_STACK}/inc/
  ${PROJECT_SOURCE_DIR}/l2/common/inc/)

string(SUBSTRING ${PLATFORM_RADIO_SHIELD} 0 6 PRS_RADIO_IC)
string(SUBSTRING ${PLATFORM_RADIO_SHIELD} 6 6 PRS_SUFFIX)

if(${PRS_RADIO_IC} STREQUAL "LR1110" OR ${PRS_RADIO_IC} STREQUAL "LR1120")
  set(L2_INC_DIR
    ${L2_INC_DIR}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src
    ${LBM_MAIN_SRC_DIR}/shields/LR11XX/common/inc
    ${LBM_MAIN_SRC_DIR}/shields/LR11XX/radio_drivers_hal
    ${LBM_MAIN_SRC_DIR}/shields/LR11XX/smtc_shield_lr11xx/common/inc
  )
elseif(${PRS_RADIO_IC} STREQUAL "SX1261")
  set(L2_INC_DIR
    ${L2_INC_DIR}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/lora_basics_modem/smtc_modem_core/radio_drivers/sx126x_driver/src
    ${LBM_MAIN_SRC_DIR}/shields/SX126X/common/inc
  )
endif()

set(MAX_MTU_SIZE 256)