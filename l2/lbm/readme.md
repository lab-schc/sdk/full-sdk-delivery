# LBM LoRaWAN stack

It is possible to change the enabled region(s) (which is REGION_EU868 by default) using the following environment variable: `FULLSDK_LORA_REGIONS`.

## Supported regions

- REGION_EU868

- REGION_AS923

- REGION_US915

- REGION_AU915

- REGION_CN470

- REGION_AS923

- REGION_AS923

- REGION_IN865

- REGION_KR920

- REGION_RU864

- REGION_CN470RP10

## Enabling multiple regions

In order to set multiple regions, values provided to `FULLSDK_LORA_REGIONS` must be separated by commas (`,`) as below:

```sh
FULLSDK_LORA_REGIONS=REGION_AS923,REGION_EU868,REGION_AU915
```

## Regional parameters

`LORAWAN_REG_PARAM` environment variable allows to specify LoRaWAN regional parameters. Its value can be set to `RP2_101` or to `RP2_103`. If not set, default value is `RP2_101`.

## Radio shield

Following radio shields are supported:
* LR1110MB1DxS
* LR1110MB1GxS
* LR1120MB1DxS
* LR1120MB1Gxs
* SX1261MB2BAS
* SX1261MB1BAS
* SX1261MB1CAS

The `PLATFORM_RADIO_SHIELD` environment variable allows to specify the radio shield. If it is not defined, the default value is `LR1110MB1DxS`.

# Trace Disclaimer

Third-party user must use `FULLSDK_MAX_PAYLOAD_SIZE=x` environment variable where `x` stands for the maximal size in bytes of received/sent payload by the application.
If this environment variable is not set, maximal payload will be set to 1024 bytes. By default, `hal_trace_print` function use a 255 bytes buffer to print logs.