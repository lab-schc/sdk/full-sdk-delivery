/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Flavien Moullec flavien@ackl.io
 *
 * x86 L2 layer implementation.
 */

#include <stdlib.h>

#include "fullsdkl2.h"

l2_status_t l2_get_class(char *class)
{
  // NOP
  *class = 'A';

  return L2_SUCCESS;
}

l2_status_t l2_set_class(char class)
{
  (void)class;
  return L2_SUCCESS;
}

l2_status_t l2_get_dutycycle(bool *value)
{
  // NOP
  (void)value;
  return L2_SUCCESS;
}

l2_status_t l2_set_dutycycle(bool dutycycle_on)
{
  // NOP
  (void)dutycycle_on;

  return L2_SUCCESS;
}

l2_status_t l2_set_dr(uint8_t dr)
{
  // NOP
  (void)dr;

  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_get_dr(uint8_t *dr)
{
  // NOP
  (void)dr;

  return L2_SUCCESS;
}

l2_status_t l2_set_adr(bool adr)
{
  // NOP
  (void)adr;

  return L2_NOT_SUPPORTED_ERR;
}

l2_status_t l2_set_cert_mode(void)
{
  // NOP
  return L2_NOT_SUPPORTED_ERR;
}

void l2_get_region(int8_t *region)
{
  // NOP
  (void)region;
}

bool l2_set_otaa(bool otaa_on)
{
  (void)otaa_on;
  return false;
}

void l2_get_otaa(bool *otaa_on)
{
  // NOP
  (void)otaa_on;
}

bool l2_set_conf_mode(uint8_t conf_mode_on)
{
  (void)conf_mode_on;
  return false;
}

void l2_get_conf_mode(uint8_t *conf_mode_on)
{
  // NOP
  (void)conf_mode_on;
}

void l2_set_technology(l2a_technology_t technology)
{
  l2a_technology = technology;
}

void bcdma_set_dest_addr(const uint8_t *addr)
{
  (void)addr;
}

void bcdma_set_src_addr(const uint8_t *addr)
{
  (void)addr;
}

const uint8_t *bcdma_get_dest_addr(void)
{
  return NULL;
}

const uint8_t *bcdma_get_src_addr(void)
{
  return NULL;
}