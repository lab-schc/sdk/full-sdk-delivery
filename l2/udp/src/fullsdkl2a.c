/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Flavien Moullec flavien@ackl.io
 */

#include "base64.h"
#include "fullsdkl2a.h"
#include "parson.h"
#include "platform.h"

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

// L2 technology. It can be overriden by the application.
l2a_technology_t l2a_technology = L2A_DEFAULT;

// L2 MTU. It can be overriden by the application.
static uint16_t l2_mtu = 1500;

// Default socket parameters. It can be overriden by the application.
const char *SRC_PORT = "5684";
const char *DST_PORT = "5683";
const char *SRC_IP = "127.0.0.1";
const char *DST_IP = "127.0.0.1";

static l2a_callbacks_t l2a_cb;
static uint8_t *l2a_rx_buffer;
static uint16_t l2a_rx_buffer_size;
static int sockfd;

#ifdef ALLOW_MICRO_IPCORE_FORMAT_OPTION
static bool micro_ipcore_format = false;
static char *dev_id = NULL;
static char *organization = NULL;
#endif

static enum {
  NO_EVENT = 0,
  CONNECTIVITY_AVAILABLE_EVENT = 1 << 0,
  TRANSMISSION_RESULT_EVENT = 1 << 1,
  DOWNLINK_AVAILABLE_EVENT = 1 << 2,
} event;

void l2_set_mtu(uint16_t mtu)
{
  l2_mtu = mtu;
}

void l2_set_ipv4_host_addr(const char *addr)
{
  SRC_IP = addr;
}

void l2_set_ipv4_remote_addr(const char *addr)
{
  DST_IP = addr;
}

void l2_set_udp_src_port(const char *port)
{
  SRC_PORT = port;
}

void l2_set_udp_dest_port(const char *port)
{
  DST_PORT = port;
}

void l2_set_micro_ipcore_format(bool enable)
{
#ifndef ALLOW_MICRO_IPCORE_FORMAT_OPTION
  (void)enable;
  fprintf(stderr, "Function \"l2_set_micro_ipcore_format\" has no effect as ALLOW_MICRO_IPCORE_FORMAT_OPTION was not defined during compilation\n");
#else
  micro_ipcore_format = enable;
#endif
}

void l2_set_organization(const char *new_organization)
{
#ifndef ALLOW_MICRO_IPCORE_FORMAT_OPTION
  (void)new_organization;
  fprintf(stderr, "Function \"l2_set_organization\" has no effect as ALLOW_MICRO_IPCORE_FORMAT_OPTION was not defined during compilation\n");
#else
  if (organization)
  {
    free(organization);
  }
  organization = strdup(new_organization);
#endif
}

void l2_set_device_id(const char *new_device_id)
{
#ifndef ALLOW_MICRO_IPCORE_FORMAT_OPTION
  (void)new_device_id;
  fprintf(stderr, "Function \"l2_set_device_id\" has no effect as ALLOW_MICRO_IPCORE_FORMAT_OPTION was not defined during compilation\n");
#else
  if (dev_id)
  {
    free(dev_id);
  }
  dev_id = strdup(new_device_id);
#endif
}

static void _downlink_available_callback(void)
{
  event |= DOWNLINK_AVAILABLE_EVENT;
  l2a_cb.processing_required();
}

l2a_status_t l2a_initialize(const l2a_callbacks_t *pp_callbacks,
                            uint8_t *p_receive_buffer,
                            uint16_t receive_buffer_size)
{
  struct sockaddr_in servaddr = {};
  struct sockaddr_in srcaddr = {};

  printf("l2a>l2a_initialized() called\n");
  l2a_cb = *pp_callbacks;
  l2a_rx_buffer = p_receive_buffer;
  l2a_rx_buffer_size = receive_buffer_size;

  event = NO_EVENT;

  printf("l2a>UDP socket configuration");
  printf("l2a>host: %s:%s", SRC_IP, SRC_PORT);
  printf("l2a>remote: %s:%s", DST_IP, DST_PORT);

  // Creating socket file descriptor
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
  {
    return L2A_CONNECT_ERR;
  }
  // Indicate that the connectivity is available
  event |= CONNECTIVITY_AVAILABLE_EVENT;
  l2a_cb.processing_required();

  srcaddr.sin_family = AF_INET;
  srcaddr.sin_addr.s_addr = inet_addr(SRC_IP);
  srcaddr.sin_port = htons(atoi(SRC_PORT));
  // Bind the socket
  if (bind(sockfd, (struct sockaddr *)&srcaddr, sizeof(srcaddr)) < 0)
  {
    printf("l2a>bind failed\n");
    return L2A_CONNECT_ERR;
  }

  // Filling server information
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(atoi(DST_PORT));
  servaddr.sin_addr.s_addr = inet_addr(DST_IP);
  // connect to server
  if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
  {
    printf("l2a>connect failed\n");
    return L2A_CONNECT_ERR;
  }
  if (!watch_fd_for_input(sockfd, &_downlink_available_callback))
  {
    printf("l2a>watch_fd_for_input() failed\n");
    return L2A_CONNECT_ERR;
  }
  return L2A_SUCCESS;
}

#ifdef ALLOW_MICRO_IPCORE_FORMAT_OPTION
bool format_send_for_micro_ipcore(const uint8_t **p_data, uint16_t *data_size)
{
  JSON_Value *root_value = NULL;
  uint8_t *b64_data = NULL;
  uint8_t *json_payload = NULL;

  if ((root_value = json_value_init_object()) == NULL)
  {
    return false;
  }
  JSON_Object *root_object = json_value_get_object(root_value);
  if (root_object == NULL)
  {
    return false;
  }
  if ((b64_data = (uint8_t *)b64_encode(*p_data, *data_size)) == NULL)
  {
    return false;
  }
  if (json_object_set_string(root_object, "device_id", dev_id) == JSONFailure ||
      json_object_set_string(root_object, "organization", organization ? organization : "") == JSONFailure || // organization is not mandatory (yet)
      json_object_set_string(root_object, "data", (const char *)b64_data) == JSONFailure)
  {
    return false;
  }
  if ((json_payload = (uint8_t *)json_serialize_to_string(root_value)) == NULL)
  {
    return false;
  }
  *p_data = json_payload;
  *data_size = strlen((const char *)json_payload);
  printf("JSON : %s\n", json_payload);

  free(b64_data);
  json_value_free(root_value);
  return true;
}
#endif

l2a_status_t l2a_send_data(const uint8_t *p_data, uint16_t data_size)
{
  ssize_t ret;

  printf("l2a>l2a_send_data() called\n");
  PRINT_HEX_BUF(p_data, data_size);

#ifdef ALLOW_MICRO_IPCORE_FORMAT_OPTION
  if (micro_ipcore_format && !format_send_for_micro_ipcore(&p_data, &data_size))
  {
    return L2A_L2_ERROR;
  }
#endif

  do
  {
    ret = send(sockfd, p_data, data_size, 0);
  } while (ret == -1 && errno == EINTR);

#ifdef ALLOW_MICRO_IPCORE_FORMAT_OPTION
  if (micro_ipcore_format)
  {
    json_free_serialized_string((char *)p_data);
  }
#endif

  if (ret == -1)
    return L2A_L2_ERROR;

  event |= TRANSMISSION_RESULT_EVENT;
  l2a_cb.processing_required();

  return L2A_SUCCESS;
}

l2a_technology_t l2a_get_technology(void)
{
  return l2a_technology;
}

uint16_t l2a_get_mtu(void)
{
  printf("l2a>l2a_get_mtu() called\n");

  return l2_mtu;
}

uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  (void)data_size;
  printf("l2a>l2a_get_next_tx_delay() called\n");

  // Return 2000ms delay.
  return 2000;
}

#ifdef ALLOW_MICRO_IPCORE_FORMAT_OPTION
int parse_micro_ipcore_format(void)
{
  JSON_Value *root_value = json_parse_string((const char *)l2a_rx_buffer);
  if (root_value == NULL)
  {
    l2a_cb.data_received(0, L2A_L2_ERROR);
    return -1;
  }
  printf("l2a>JSON data received: %s\n", l2a_rx_buffer);
  JSON_Object *root_object = json_value_get_object(root_value);
  if (root_object == NULL)
  {
    l2a_cb.data_received(0, L2A_L2_ERROR);
    return -1;
  }
  const char *b64_data = json_object_get_string(root_object, "data");
  size_t decoded_data_size = 0;
  const uint8_t *decoded_data = b64_decode(b64_data, &decoded_data_size);
  if (decoded_data_size >= l2a_rx_buffer_size)
  {
    l2a_cb.data_received(0, L2A_BUFFER_ERR);
    return -1;
  }
  memcpy(l2a_rx_buffer, decoded_data, decoded_data_size);
  return decoded_data_size;
}
#endif

static void _read_downlink(void)
{
  ssize_t ret;

  do
  {
    ret = recv(sockfd, l2a_rx_buffer, l2a_rx_buffer_size, 0);
  } while (ret == -1 && errno == EINTR);
  if (ret == -1)
  {
    if (errno == EMSGSIZE) // UDP packet too long for the provided buffer
      l2a_cb.data_received(0, L2A_BUFFER_ERR);
    else
      l2a_cb.data_received(0, L2A_L2_ERROR);
  }
  else
  {
#ifdef ALLOW_MICRO_IPCORE_FORMAT_OPTION
    if (micro_ipcore_format && (ret = parse_micro_ipcore_format()) == -1)
    {
      return;
    }
#endif
    printf("l2a>rx packet received\n");
    PRINT_HEX_BUF(l2a_rx_buffer, ret);
    l2a_cb.data_received(ret, L2A_SUCCESS);
  }
}

l2a_status_t l2a_process(void)
{
  printf("l2a>l2a_process() called\n");
  if (event & CONNECTIVITY_AVAILABLE_EVENT)
    l2a_cb.connectivity_available();
  if (event & TRANSMISSION_RESULT_EVENT)
    l2a_cb.transmission_result(L2A_SUCCESS, 0);
  if (event & DOWNLINK_AVAILABLE_EVENT)
    _read_downlink();
  event = NO_EVENT;
  return L2A_SUCCESS;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  (void)dev_iid;
  return false;
}
