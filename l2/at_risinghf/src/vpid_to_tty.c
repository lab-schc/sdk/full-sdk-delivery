/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Josso arthur.josso@ackl.io
 */

#include <libudev.h>
#include <stdint.h>
#include <string.h>
#include "vpid_to_tty.h"

static inline bool _free_on_error(struct udev *u,
				  struct udev_enumerate *e,
				  struct udev_device *d) {
  udev_device_unref(d);
  udev_enumerate_unref(e);
  udev_unref(u);
  return false;
}

static bool _find_dev_from_parent(char tty_path[PATH_MAX],
				  struct udev *udev,
				  struct udev_device *parent) {
  struct udev_enumerate *e;
  struct udev_list_entry *devs;
  struct udev_list_entry *entry;

  if (!(e = udev_enumerate_new(udev)))
    return false;

  if (udev_enumerate_add_match_parent(e, parent) != 0 ||
      udev_enumerate_add_match_subsystem(e, "tty") != 0 ||
      udev_enumerate_scan_devices(e) != 0)
    return _free_on_error(NULL, e, NULL);

  devs = udev_enumerate_get_list_entry(e);
  udev_list_entry_foreach(entry, devs) {
    const char *devnode;
    const char *path = udev_list_entry_get_name(entry);
    struct udev_device *dev = udev_device_new_from_syspath(udev, path);
    if (!dev)
      return _free_on_error(NULL, e, NULL);
    if (!(devnode = udev_device_get_devnode(dev)))
      return _free_on_error(NULL, e, dev);
    strncpy(tty_path, udev_device_get_devnode(dev), PATH_MAX - 1);
    udev_device_unref(dev);
    break;
  }

  udev_enumerate_unref(e);
  return true;
}

bool find_tty_path_from_vpid(char tty_path[PATH_MAX],
			     const char *vid,
			     const char *pid) {
  struct udev *udev;
  struct udev_enumerate *e;
  struct udev_list_entry *devs;
  struct udev_list_entry *entry;

  memset(tty_path, 0, PATH_MAX);
  if (!(udev = udev_new()))
    return false;
  if (!(e = udev_enumerate_new(udev)))
    return _free_on_error(udev, NULL, NULL);

  if (udev_enumerate_add_match_sysattr(e, "idVendor", vid) != 0 ||
      udev_enumerate_add_match_sysattr(e, "idProduct", pid) != 0 ||
      udev_enumerate_add_match_subsystem(e, "usb") != 0 ||
      udev_enumerate_scan_devices(e) != 0)
    return _free_on_error(udev, e, NULL);

  devs = udev_enumerate_get_list_entry(e);
  udev_list_entry_foreach(entry, devs) {
    const char *path = udev_list_entry_get_name(entry);
    struct udev_device *dev = udev_device_new_from_syspath(udev, path);
    if (!dev)
      return _free_on_error(udev, e, NULL);
    if (!_find_dev_from_parent(tty_path, udev, dev))
      return _free_on_error(udev, e, dev);
    udev_device_unref(dev);
    break;
  }
  
  udev_enumerate_unref(e);
  udev_unref(udev);
  return true;
}
