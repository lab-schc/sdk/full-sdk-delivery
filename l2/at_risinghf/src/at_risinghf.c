/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Josso arthur.josso@ackl.io
 */

#define _GNU_SOURCE // needed to use asprintf()
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "vpid_to_tty.h"
#include "at_risinghf.h"
#include "serial.h"
#include "tools.h"
#include "fullsdkl2.h"

typedef struct
{
  const char *fmt;
  char *cmd;
  at_error_t (*run)(const char*);
} command_t;

typedef struct
{
  command_t dr;
  command_t mode;
  command_t key;
  command_t class;
  command_t adr;
  command_t dc;
  command_t dceu;
  command_t join;
  command_t msg;
  command_t port;
} commands_t;

static at_error_t _cmd_dr(const char*);
static at_error_t _cmd_mode(const char*);
static at_error_t _cmd_key(const char*);
static at_error_t _cmd_class(const char*);
static at_error_t _cmd_adr(const char*);
static at_error_t _cmd_dc(const char*);
static at_error_t _cmd_dceu(const char*);
static at_error_t _cmd_join(const char*);
static at_error_t _cmd_msg(const char*);
static at_error_t _cmd_port(const char*);

static commands_t _cmd =
{
  {"AT+DR=%d\n", NULL, _cmd_dr},
  {NULL, "AT+MODE=LWOTAA\n", _cmd_mode},
  {"AT+KEY=APPKEY,\"%s\"\n", NULL, _cmd_key},
  {"AT+CLASS=%c\n", NULL, _cmd_class},
  {"AT+ADR=%s\n", NULL, _cmd_adr},
  {NULL, "AT+LW=DC,0\n", _cmd_dc},
  {"AT+LW=DC,%s\n", NULL, _cmd_dceu},
  {NULL, "AT+JOIN\n", _cmd_join},
  {"AT+MSGHEX=\"%s\"\n", NULL, _cmd_msg},
  {"AT+PORT=%d\n", NULL, _cmd_port}
};

static char *_response_buff = NULL; // serial_readline() buff
static char *_rx_buff = NULL; // buff to store immediate RX during TX
static const char *_net_id;
static uint8_t _max_join_iter;
static uint8_t _join_intervals;
static const char _port_needle[] = "PORT: ";
static const char _data_needle[] = "RX: \"";

char dev_class = 'C';

l2_status_t l2_set_class(char lora_dev_class)
{
  if (lora_dev_class != 'C' && lora_dev_class != 'A')
  {
    return L2_ERROR;
  }
  dev_class = lora_dev_class;
  return L2_SUCCESS;
}

char l2_get_class(void)
{
  return dev_class;
}

static inline at_error_t _exec_cmd(command_t *cmd)
{
  return cmd->run(cmd->cmd);
}

static at_error_t _init(at_shield_params_t *p)
{
  // Init command strings
  if (asprintf(&_cmd.key.cmd, _cmd.key.fmt, p->app_key) == -1 ||
      asprintf(&_cmd.dr.cmd, _cmd.dr.fmt, p->data_rate) == -1 ||
      asprintf(&_cmd.adr.cmd, _cmd.adr.fmt, p->enable_adr ? "ON" : "OFF") == -1 ||
      asprintf(&_cmd.dceu.cmd, _cmd.dceu.fmt, p->enable_eu_dc ? "ON" : "OFF") == -1 ||
      asprintf(&_cmd.class.cmd, _cmd.class.fmt, dev_class) == -1)
    return AT_INTERNAL_ERR;

  // Save AT+JOIN params
  _net_id = p->net_id;
  _max_join_iter = p->max_join_iter;
  _join_intervals = p->join_intervals;

  // Find serial port TTY path from VID and PID
  char path[PATH_MAX];
  char split_vpid[strlen(p->vid_pid) + 1];
  strcpy(split_vpid, p->vid_pid);
  char *vid = split_vpid;
  char *pid = strchr(split_vpid, ':');
  if (!pid) {
    fprintf(stderr, "Error: RisingHF init: VID:PID parsing failed\n");
    return AT_INTERNAL_ERR;
  }
  *(pid++) = '\0'; // replace ':' by '\0' to split the strings
  if (!find_tty_path_from_vpid(path, vid, pid)) {
    fprintf(stderr, "Error: RisingHF init: find_tty_path_from_vpid() failed\n");
    return AT_INTERNAL_ERR;
  }
  else if (path[0] == '\0') {
      #ifdef USE_LPWAN_SIMULATOR
          strcpy(path, LPWAN_SIMU_TTY_PORT);
      #else
          fprintf(stderr, "Error: RisingHF init: no TTY associated with such VID:PID\n");
          return AT_INTERNAL_ERR;
      #endif
  }

  // Init serial com
  if (serial_init(path, p->baudrate) != 0)
    return AT_INTERNAL_ERR;

  // Init LoRa params
  at_error_t err;
  if ((err = _exec_cmd(&_cmd.dr)) != AT_SUCCESS)
    return err;
  if ((err = _exec_cmd(&_cmd.mode)) != AT_SUCCESS)
    return err;
  if ((err = _exec_cmd(&_cmd.key)) != AT_SUCCESS)
    return err;
  if ((err = _exec_cmd(&_cmd.class)) != AT_SUCCESS)
    return err;
  if ((err = _exec_cmd(&_cmd.adr)) != AT_SUCCESS)
    return err;
  if ((err = _exec_cmd(&_cmd.dc)) != AT_SUCCESS)
    return err;
  if ((err = _exec_cmd(&_cmd.dceu)) != AT_SUCCESS)
    return err;

  return AT_SUCCESS;
}

static void _fini(void)
{
  free(_response_buff);
  free(_rx_buff);
  free(_cmd.key.cmd);
  free(_cmd.dr.cmd);
  free(_cmd.adr.cmd);
  free(_cmd.dceu.cmd);
  free(_cmd.msg.cmd);
  free(_cmd.port.cmd);
  serial_fini();
}

static at_error_t _join(void)
{
  return _exec_cmd(&_cmd.join);
}

static at_error_t _send(const uint8_t *data, size_t size)
{
  char hexstr[2 * size - 1];
  at_error_t setport_ret;

  free(_cmd.port.cmd);
  if (asprintf(&_cmd.port.cmd, _cmd.port.fmt, data[0]) == -1)
    return AT_INTERNAL_ERR;
  setport_ret = _exec_cmd(&_cmd.port);
  if (setport_ret != AT_SUCCESS)
    return setport_ret;
  if (size > 1)
  {
    data++;
    size--;
  }

  memset(hexstr, 0, sizeof(hexstr));
  for (size_t i = 0; i < size; ++i)
    sprintf(hexstr + (i * 2), "%02x", data[i]);
  free(_cmd.msg.cmd);
  if (asprintf(&_cmd.msg.cmd, _cmd.msg.fmt, hexstr) == -1)
    return AT_INTERNAL_ERR;
  return _exec_cmd(&_cmd.msg);
}

static at_error_t _parse_downlink(char *start, uint8_t *data, size_t *size)
{
  char *end;
  size_t nbr_bytes;

  // Parse rule ID
  start += sizeof(_port_needle) - 1; // -1 to exclude '\0'
  data[0] = atoi(start);
  if (data[0] == 0)
    return AT_MALFORMED;

  // Find data starting byte
  if (!(start = strstr(start, _data_needle)))
    return AT_MALFORMED;
  start += sizeof(_data_needle) - 1;
  end = strchr(start, '"');
  if (!end) // incomplete string
    return AT_MALFORMED;
  nbr_bytes = (end - start);
  if (nbr_bytes & 1) // check parity
    return AT_MALFORMED;
  nbr_bytes /= 2; // hex str to bin

  // Adjustment from previous rule ID parsing
  data++;
  nbr_bytes++;
  if (nbr_bytes > *size)
    return AT_BUFF_TOO_SMALL;

  // Parse data
  for (size_t i = 0; i < nbr_bytes - 1; ++i) {
    int8_t hb = hexchar_to_bin(start[i * 2]);
    int8_t lb = hexchar_to_bin(start[i * 2 + 1]);
    if (hb == -1 || lb == -1) // non-hex char
      return AT_MALFORMED;
    data[i] = (hb << 4) | lb;
  }
  *size = nbr_bytes;
  return AT_SUCCESS;
}

static at_error_t _recv(uint8_t *data, size_t *size)
{
  char *start = NULL;
  at_error_t err = AT_SUCCESS;

  if (_rx_buff)
    {
      err = _parse_downlink(strstr(_rx_buff, _port_needle), data, size);
      free(_rx_buff);
      _rx_buff = NULL;
      return err;
    }
    do
    {
      if (serial_readline(&_response_buff) == -1)
	      return AT_INTERNAL_ERR;
      if (!start && strstr(_response_buff, _data_needle) &&
	        (start = strstr(_response_buff, _port_needle)))
	      err = _parse_downlink(start, data, size);
    } while (!strstr(_response_buff, "Done"));
  /*
   * Weird case where we were notified for downlink but received no data:
   * +MSG: RXWIN0, RSSI -41, SNR 6.5
   * +MSG: Done
   * Hypothesis: Empty LoRa frame ?
   * Palliation: recv() can return empty buffer (of size 0)
   */
  if (start == NULL)
    *size = 0;
  return err;
}

static int _poll(bool block)
{
  if (_rx_buff)
    return 1;
  return serial_poll(block);
}

static uint16_t _mtu(void)
{
  uint16_t mtu = 0;
  const char cmd[] = "AT+LW=LEN\n";

  if (serial_writeline(cmd) != sizeof(cmd) - 1)
    return 0;
  if (serial_readline(&_response_buff) == -1)
    return 0;
  sscanf(_response_buff, "+LW: LEN, %hu", &mtu);
  return mtu;
}

static at_callbacks_t _callbacks =
  {
    &_init, &_fini, &_join, &_send, &_recv, &_poll, &_mtu
  };

const at_callbacks_t *at_get_risinghf()
{
  return &_callbacks;
}

static at_error_t _cmd_dr(const char *cmd)
{
  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  if (serial_readline(&_response_buff) == -1)
    return AT_INTERNAL_ERR;
  if (strstr(_response_buff, "ERROR"))
    return AT_NO_DR;
  else if (serial_readline(&_response_buff) == -1) // drop next line
    return AT_INTERNAL_ERR;
  return AT_SUCCESS;
}

static at_error_t _cmd_mode(const char *cmd)
{
  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  if (serial_readline(&_response_buff) == -1)
    return AT_INTERNAL_ERR;
  if (strstr(_response_buff, "ERROR"))
    return AT_NO_MODE;
  return AT_SUCCESS;
}

static at_error_t _cmd_key(const char *cmd)
{
  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  if (serial_readline(&_response_buff) == -1) // drop next line
    return AT_INTERNAL_ERR;
  return AT_SUCCESS;
}

static at_error_t _cmd_class(const char *cmd)
{
  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  if (serial_readline(&_response_buff) == -1)
    return AT_INTERNAL_ERR;
  if (strstr(_response_buff, "ERROR"))
    return AT_NO_CLASS;
  return AT_SUCCESS;
}

static at_error_t _cmd_adr(const char *cmd)
{
  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  if (serial_readline(&_response_buff) == -1)
    return AT_INTERNAL_ERR;
  if (strstr(_response_buff, "ERROR"))
    return AT_NO_ADR;
  return AT_SUCCESS;
}

static at_error_t _cmd_dc(const char *cmd)
{
  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  if (serial_readline(&_response_buff) == -1)
    return AT_INTERNAL_ERR;
  if (strstr(_response_buff, "ERROR"))
    return AT_NO_EU_DC;
  return AT_SUCCESS;
}

static at_error_t _cmd_dceu(const char *cmd)
{
  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  if (serial_readline(&_response_buff) == -1)
    return AT_INTERNAL_ERR;
  if (strstr(_response_buff, "ERROR"))
    return AT_NO_EU_DC;
  return AT_SUCCESS;
}

static bool _network_match(const char *start) {
  const char needle[] = "NetID ";
  const size_t id_len = strlen(_net_id);

  start += sizeof(needle) - 1; // -1 to exclude '\0'
  if (strncmp(_net_id, start, id_len) != 0)
    return false;
  if (start[id_len] != ' ')
    return false;
  return true;
}

static at_error_t _cmd_join(const char *cmd)
{
  at_error_t err = AT_INTERNAL_ERR;

  for (uint8_t try = 0; try < _max_join_iter && err != AT_SUCCESS; ++try)
  {
    if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
      return AT_INTERNAL_ERR;
    while (1)
    {
      char *start;
      if (serial_readline(&_response_buff) == -1)
        return AT_INTERNAL_ERR;
      if (strstr(_response_buff, "failed"))
        err = AT_JOIN_FAILED;
      else if ((start = strstr(_response_buff, "NetID ")))
        err = _network_match(start) ? AT_SUCCESS : AT_NO_NETWORK;
      else if (strstr(_response_buff, "Done"))
        break; // last message
      else if (strstr(_response_buff, "Joined already"))
              return AT_SUCCESS; // last message but no retry
    }
    if (err != AT_SUCCESS)
      sleep(_join_intervals);
  }
  return err;
}

static at_error_t _cmd_msg(const char *cmd)
{
  const struct {at_error_t code; const char *str;} errors[] =
  {
    {AT_SUCCESS, "Done"},
    {AT_NOT_JOINED, "Please join network first"},
    {AT_NO_FREE_CHAN, "No free channel"},
    {AT_NO_BAND, "No band"},
    {AT_DR_NOT_SUPPORTED, "DR error"},
    {AT_TOO_LONG, "Length error"},
    {AT_BUSY, "LoRaWAN modem is busy"},
    {AT_UNKNOWN_ERR, "ERROR"}
  };

  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  while (1)
  {
    if (serial_readline(&_response_buff) == -1)
      return AT_INTERNAL_ERR;
    if (strstr(_response_buff, _data_needle)) // Check if RX within TX
    {
      if (_rx_buff)
      {
        fprintf(stderr, "WARNING: RisingHF: consecutive RX not read," \
          "dropping first one\n");
        free(_rx_buff);
      }
      if ((_rx_buff = strdup(_response_buff)) == NULL)
        return AT_INTERNAL_ERR;
      continue;
    }
    for (uint8_t i = 0; i < sizeof(errors) / sizeof(*errors); ++i)
    {
	    if (strstr(_response_buff, errors[i].str))
      {
	      return errors[i].code;
      }
    }
  }
}

static at_error_t _cmd_port(const char *cmd)
{
  if (serial_writeline(cmd) != (ssize_t)strlen(cmd))
    return AT_INTERNAL_ERR;
  if (serial_readline(&_response_buff) == -1)
    return AT_INTERNAL_ERR;
  if (strstr(_response_buff, "ERROR"))
    return AT_NO_PORT;
  return AT_SUCCESS;
}
