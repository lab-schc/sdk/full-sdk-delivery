/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Josso arthur.josso@ackl.io
 */

#include "at.h"

static const char *_errors[] =
{
  "Success",
  "Internal error",
  "init(): Cannot init Data Rate",
  "init(): Cannot init Mode",
  "init(): Cannot init Class",
  "init(): Cannot init Adaptive Data Rate",
  "init(): Cannot init European Duty Cycle",
  "join(): Cannot join (unknown error)",
  "join(): Wrong network joined",
  "send(): Need to join() before send()",
  "send(): No free channel available",
  "send(): Cannot emit on this band yet",
  "send(): Data Rate not supported",
  "send(): Message is too long",
  "send(): AT shield is busy",
  "send(): Invalid fport",
  "send(): Cannot send (unknown error)",
  "recv(): Receiving buffer is too small",
  "recv(): Received message is malformed",
};

const char *at_strerror(at_error_t e)
{
  if (e < AT_SUCCESS || e > AT_MALFORMED)
    return NULL;
  return _errors[e];
}
