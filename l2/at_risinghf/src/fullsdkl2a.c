/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Josso arthur.josso@ackl.io
 */

#include "fullsdkl2a.h"
#include "fullsdkl2.h"
#include "at_risinghf.h"
#include "platform.h"
#include "serial.h"
#include <stdlib.h>

static at_shield_params_t _shield_params = {.vid_pid = vid_pid,
                                            .baudrate = B9600,
                                            .net_id = "010203",
                                            .app_key = APPKEY,
                                            .max_join_iter = 3,
                                            .join_intervals = 10,
                                            .data_rate = 5,
                                            .enable_adr = false,
                                            .enable_eu_dc = false};

static l2a_callbacks_t l2a_cb;
static uint8_t *l2a_rx_buffer;
static uint16_t l2a_rx_buffer_size;
static uint16_t l2a_mtu;

l2a_technology_t l2a_technology = L2A_LORA;

// Data size of previous transmission request, in bytes.
static uint16_t cached_data_size = 0;

static enum {
  NO_EVENT = 0,
  CONNECTIVITY_AVAILABLE_EVENT = 1 << 0,
  TRANSMISSION_RESULT_EVENT = 1 << 1,
  DOWNLINK_AVAILABLE_EVENT = 1 << 2,
} event;

static void _poll_downlink(bool block)
{
  int ret = at_get_risinghf()->poll(block);
  if (ret == -1)
    printf("AT Error: poll() failed");
  else if (ret == 1)
  {
    event |= DOWNLINK_AVAILABLE_EVENT;
    l2a_cb.processing_required();
  }
}

static void _downlink_available_callback(void)
{
  _poll_downlink(true);
}

static inline l2a_status_t check_callbacks()
{
  if (!l2a_cb.processing_required || !l2a_cb.transmission_result ||
      !l2a_cb.data_received || !l2a_cb.connectivity_available)
    return L2A_REQ_CALLBACK_ERR;
  return L2A_SUCCESS;
}

l2a_status_t l2a_initialize(const l2a_callbacks_t *pp_callbacks,
                            uint8_t *p_receive_buffer,
                            uint16_t receive_buffer_size)
{
  at_error_t err;

  printf("l2a_initialize() called");
  l2a_cb = *pp_callbacks;
  l2a_rx_buffer = p_receive_buffer;
  l2a_rx_buffer_size = receive_buffer_size;

  if (check_callbacks() != L2A_SUCCESS)
    return L2A_REQ_CALLBACK_ERR;
  if (!l2a_rx_buffer)
    return L2A_BUFFER_ERR;

  event = NO_EVENT;

  if ((err = at_get_risinghf()->init(&_shield_params)) != AT_SUCCESS)
  {
    printf("AT Error in init(): %s", at_strerror(err));
    return L2A_CONNECT_ERR;
  }
  if ((err = at_get_risinghf()->join()) != AT_SUCCESS)
  {
    printf("AT Error in join(): %s", at_strerror(err));
    return L2A_CONNECT_ERR;
  }
  if ((l2a_mtu = at_get_risinghf()->mtu()) == 0)
  {
    printf("AT Error: Cannot retrieve MTU");
    return L2A_CONNECT_ERR;
  }

  event |= CONNECTIVITY_AVAILABLE_EVENT;
  l2a_cb.processing_required();

  if (!watch_fd_for_input(serial_get_fd(), &_downlink_available_callback))
  {
    printf("Error: watch_fd_for_input() failed");
    return L2A_CONNECT_ERR;
  }

  // as we dont have a proper way to close connection
  atexit(at_get_risinghf()->fini);

  return L2A_SUCCESS;
}

l2a_status_t l2a_send_data(const uint8_t *p_data, uint16_t data_size)
{
  at_error_t err;

  printf("\nl2a_send_data() called");
  PRINT_HEX_BUF((uint8_t *)p_data, data_size);

  cached_data_size = data_size;

  if (check_callbacks() != L2A_SUCCESS)
    return L2A_REQ_CALLBACK_ERR;

  if ((err = at_get_risinghf()->send(p_data, data_size)) != AT_SUCCESS)
  {
    printf("AT Error in send(): %s", at_strerror(err));
    switch (err)
    {
      case AT_NOT_JOINED:
        return L2A_CONNECT_ERR;
      case AT_BUSY:
        return L2A_BUSY_ERR;
      case AT_INTERNAL_ERR:
        return L2A_INTERNAL_ERR;
      default:
        return L2A_L2_ERROR;
    }
  }
  event |= TRANSMISSION_RESULT_EVENT;
  l2a_cb.processing_required();
  return L2A_SUCCESS;
}

void l2_set_technology(l2a_technology_t technology)
{
  l2a_technology = technology;
}

l2a_technology_t l2a_get_technology(void)
{
  return l2a_technology;
}

uint16_t l2a_get_mtu(void)
{
  return l2a_mtu;
}

uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  if (data_size == 0)
    data_size = cached_data_size;
  switch (_shield_params.data_rate)
  {
    case 0:
      return 125000 + data_size * 3200;
    case 1:
      return 61000 + data_size * 1818;
    case 2:
      return 29000 + data_size * 816;
    case 3:
      return 22212 + data_size * 455;
    case 4:
      return 9000 + data_size * 256;
    case 5:
      return 4000 + data_size * 147;
    case 6:
      return data_size * 73;
    default:
      return UINT32_MAX;
  }
}

static void _read_downlink(void)
{
  size_t size = l2a_rx_buffer_size;
  at_error_t err;

  if ((err = at_get_risinghf()->recv(l2a_rx_buffer, &size)) != AT_SUCCESS)
  {
    printf("AT Error in recv(): %s", at_strerror(err));
    if (err == AT_BUFF_TOO_SMALL)
      l2a_cb.data_received(0, L2A_BUFFER_ERR);
    else
      l2a_cb.data_received(0, L2A_L2_ERROR);
  }
  else if (size != 0)
  {
    printf("\nRX packet received");
    PRINT_HEX_BUF(l2a_rx_buffer, size);
    l2a_cb.data_received(size, L2A_SUCCESS);
  }
}

l2a_status_t l2a_process(void)
{
  if (event & CONNECTIVITY_AVAILABLE_EVENT)
    l2a_cb.connectivity_available();
  if (event & TRANSMISSION_RESULT_EVENT)
    l2a_cb.transmission_result(L2A_SUCCESS, 0);
  if (event & DOWNLINK_AVAILABLE_EVENT)
    _read_downlink();
  event = NO_EVENT;
  _poll_downlink(false); // poll for any remaining buffered line
  return L2A_SUCCESS;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  (void)dev_iid;
  return false;
}
