/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Josso arthur.josso@ackl.io
 *
 *
 * This header is an interface that should be used by LoRa shield implementations.
 * The implementation should provide a filled `at_callbacks_t` structure to the user.
 */

#ifndef AT_H_
#define AT_H_

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <termios.h>

// Comments show which callback will return which values.
typedef enum
{
  // All
  AT_SUCCESS,
  AT_INTERNAL_ERR,

  // init
  AT_NO_DR,
  AT_NO_MODE,
  AT_NO_CLASS,
  AT_NO_ADR,
  AT_NO_EU_DC,

  // join
  AT_JOIN_FAILED,
  AT_NO_NETWORK,

  // send
  AT_NOT_JOINED,
  AT_NO_FREE_CHAN,
  AT_NO_BAND,
  AT_DR_NOT_SUPPORTED,
  AT_TOO_LONG,
  AT_BUSY,
  AT_NO_PORT,
  AT_UNKNOWN_ERR,

  // recv
  AT_BUFF_TOO_SMALL,
  AT_MALFORMED
} at_error_t;

const char *at_strerror(at_error_t e);

typedef struct
{
  const char *vid_pid;
  speed_t baudrate;
  const char *net_id;
  const char *app_key;
  uint8_t max_join_iter;
  uint8_t join_intervals;
  uint8_t data_rate;
  bool enable_adr;
  bool enable_eu_dc;
} at_shield_params_t;

typedef at_error_t (*at_init_t)(at_shield_params_t*);
typedef void (*at_fini_t)(void);
typedef at_error_t (*at_join_t)(void);
typedef at_error_t (*at_send_t)(const uint8_t *data, size_t size);
/*
 * at_recv_t takes as parameters:
 * A buffer `data` of size `*size`
 *
 * at_recv_t stores:
 * The received data in `data`
 * The number of bytes received in `*size`
 */
typedef at_error_t (*at_recv_t)(uint8_t *data, size_t *size);
/*
 * at_poll_t has to be called once a real call to poll() on the serial fd
 * indicates that data is available. It allows to encapsulate some unwanted
 * behavior from a raw call to poll(), such as:
 * - avoid reading empty lines
 * - cache some lines that have been read too early
 *
 * Parameter:
 * - block: - true to check for buffered line AND read from serial if nothing
 *          - false to only check for buffered line
 *
 * Returns:
 * - 1 if at_send_t will not block (pending data)
 * - 0 if at_send_t will block
 * - -1 on error
*/
typedef int (*at_poll_t)(bool block);
typedef uint16_t (*at_mtu_t)(void);

typedef struct
{
  at_init_t init; // Initialize shield with provided parameters
  at_fini_t fini; // Free the resources used by the shield
  at_join_t join; // Join the LoRaWAN server
  at_send_t send; // Send uplink frame
  at_recv_t recv; // Receive downlink frame (blocking call)
  at_poll_t poll; // Poll the serial port to check if there is any incoming data
  at_mtu_t mtu;   // Retrieve the Maximum Transmission Unit for a given data rate
} at_callbacks_t;

#endif