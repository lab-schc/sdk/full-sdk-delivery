/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Josso arthur.josso@ackl.io
 */

#ifndef SERIAL_H_
#define SERIAL_H_

#include <termios.h>
#include <stdbool.h>

int serial_init(const char *path, speed_t baudrate);
void serial_fini(void);
ssize_t serial_readline(char **l);
ssize_t serial_writeline(const char *l);
int serial_poll(bool block);
int serial_get_fd(void);

#endif
