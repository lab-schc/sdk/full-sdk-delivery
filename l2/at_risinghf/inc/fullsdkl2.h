/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 *
 * Rising HF module L2 layer interface.
 */

#ifndef FULLSDK_L2_H_
#define FULLSDK_L2_H_

#include <stdbool.h>
#include <stdint.h>

#include "fullsdkl2a.h"
#include "fullsdkl2common.h"

#define RISING_HF_L2

#define VIDPID_LEN 10
// This global variable holds the value use to connect to the serial port
// of the risingHF
extern char vid_pid[VIDPID_LEN];

// This global variable must be defined in L2A layer and initialized
// to L2A_DEFAULT. Its use is reserved.
extern l2a_technology_t l2a_technology;

/**
 * This function sets l2a_technology global variable defined in L2A layer.
 * When used, it must be called before mgt_initialize().
 */
void l2_set_technology(l2a_technology_t technology);

char l2_get_class(void);

/**
 * This function sets the class of the device.
 *
 * Parameters:
 * - class: device class ('A' or 'C')
 *
 * Returned value: true if the operation is successful, false if not
 */
l2_status_t l2_set_class(char lora_dev_class);

l2_status_t l2_set_dr(uint8_t dr);

l2_status_t l2_get_dr(uint8_t *dr);

l2_status_t l2_set_adr(bool adr);

/**
 * This function gets the region defined during the compilation time.
 *
 * Parameters:
 * - region: the region (LORAMAC_REGION_AS923, ...)
 *
 * Returned value: none
 */
void l2_get_region(int8_t *region);

/**
 * This function sets join activation process to OTAA.
 *
 * Parameters:
 * - otaa_on: true to set OTAA on (ABP off), false to set it off (ABP on)
 *
 * Returned value: true if the operation is successful, false if not
 */
bool l2_set_otaa(bool otaa_on);

/**
 * This function gets join activation process to OTAA.
 *
 * Parameters:
 * - otaa_on: true if OTAA is on (ABP off), false if it is off (ABP on)
 *
 * Returned value: none
 */
void l2_get_otaa(bool *otaa_on);

/**
 * This function sets whether or not confirmation is required.
 *
 * Parameters:
 * - conf_mode_on: true if confirmation is required, false if it is not
 *
 * Returned value: true if the operation is successful, false if not
 */
bool l2_set_conf_mode(uint8_t conf_mode_on);

/**
 * This function gets whether or not confirmation mode is on.
 *
 * Parameters:
 * - conf_mode_on: true if confirmation is on, false if it is not
 *
 * Returned value: none
 */
void l2_get_conf_mode(uint8_t *conf_mode_on);

/**
 * This function gets whether or not duty cycle restriction is enabled.
 *
 * Parameters:
 * - value: true if duty cycle restriction is enabled, false if it is not
 *
 * Returned value: l2_status_t
 */
l2_status_t l2_get_dutycycle(bool *value);

/**
 * This function sets whether or not duty cycle restriction is enabled.
 *
 * Parameters:
 * - dutycycle_on: true if duty cycle restriction is enabled, false if it is not
 *
 * Returned value: l2_status_t
 */
l2_status_t l2_set_dutycycle(bool dutycycle_on);

/**
 * @brief This function sets the vid:pid couple used to connect to the serial
 * port of the risingHF
 *
 * @param vidpid buffer holding the value in the format "vid:pid"
 */
void l2_set_vidpid(const uint8_t *vidpid);

#endif /* FULLSDK_L2_H_ */
