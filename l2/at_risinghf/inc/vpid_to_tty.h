/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Arthur Josso arthur.josso@ackl.io
 */

#ifndef VPID_TO_TTY_H_
#define VPID_TO_TTY_H_

#include <limits.h>
#include <stdbool.h>

bool find_tty_path_from_vpid(char tty_path[PATH_MAX],
                             const char *vid,
                             const char *pid);

#endif
