/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis - thibaut.artis@ackl.io
 *
 * This file provides the implementation of L2A layer the SigFox protocol V1
 * library
 *
 * The time to next possible TX is 0 because in practice messages can be sent
 * immediately after a transmission, the only limitation is the duty cycle
 * limitation of 1%
 *
 * Reception is done in the same function as transmission
 *
 * Transmission result callback is called after the RX windows.
 *
 * If l2a_send_data() is called before transmission result callback has been
 * called, it returns a busy status code. If it is called after the callback
 * call but before time to next possible TX, the LoRaWAN stack returns an error,
 * that will be returned by l2a_send_data().
 *
 * Connectivity lost callback is never called.
 */

#include <stdbool.h>

#include "fullsdkl2a.h"
#include "hw_eeprom.h"
#include "platform.h"
#include "sigfoxconfiguration.h"
#include "st_sigfox_api.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

#define SIGFOX_MTU_UPLINK 12
#define SIGFOX_MTU_DOWNLINK 8

#define NEXT_TX_DELAY 15000;
#ifndef NEXT_TX_DELAY
#define NEXT_TX_DELAY 600 * 1000;
#endif

static uint8_t tx_repeat = 0;

typedef struct
{
  uint8_t connected : 1;
  uint8_t tx_result : 1;
  uint8_t data_rcvd : 1;
  uint8_t rfu : 5;
} l2a_events_t;
typedef struct
{
  l2a_callbacks_t callbacks;
  uint8_t *receive_buffer;
  uint16_t receive_buffer_size;
  l2a_status_t last_tx_status;
  l2a_events_t events;
  bool downlink_enabled;
  bool connected;
  bool busy;
} l2a_ctx_t;

static sfx_error_t st_sigfox_open(st_sfx_rc_t sgfx_rc);
static bool check_downlink_reception(uint8_t *p_data, uint8_t data_size);
static void l2a_action_tx_status(l2a_status_t status);

static l2a_ctx_t l2a_ctx;

l2a_status_t l2a_initialize(const l2a_callbacks_t *callbacks,
                            uint8_t *receive_buffer,
                            uint16_t receive_buffer_size)
{
  memset(&l2a_ctx, 0, sizeof(l2a_ctx));

  if (callbacks == NULL || callbacks->processing_required == NULL ||
      callbacks->transmission_result == NULL ||
      callbacks->data_received == NULL ||
      callbacks->connectivity_available == NULL)
  {
    return L2A_REQ_CALLBACK_ERR;
  }

  l2a_ctx.callbacks = *callbacks;
  l2a_ctx.receive_buffer = receive_buffer;
  l2a_ctx.receive_buffer_size = receive_buffer_size;
  l2a_ctx.last_tx_status = L2A_SUCCESS;
  // Enable downlink reception.
  l2a_ctx.downlink_enabled = true;

  st_sfx_rc_t sgfx_rc = APPLI_RC;
  sfx_error_t error = st_sigfox_open(sgfx_rc);
  if (error)
  {
    PRINT_DBG("l2a> init error (status: %s)\n", error);

    return L2A_CONNECT_ERR;
  }

  l2a_ctx.connected = true;
  l2a_ctx.callbacks.connectivity_available();

  PRINT_DBG("l2a> l2a initialized\n");

  return L2A_SUCCESS;
}

l2a_status_t l2a_send_data(const uint8_t *p_data, uint16_t data_size)
{
  PRINT_DBG("l2a> send data request\n");
  if (!l2a_ctx.connected)
  {
    l2a_action_tx_status(L2A_CONNECT_ERR);
    return L2A_SUCCESS;
  }

  if (l2a_ctx.busy)
  {
    l2a_action_tx_status(L2A_BUSY_ERR);
    return L2A_SUCCESS;
  }
  l2a_ctx.busy = true;

  // Reset receptin buffer.
  memset(l2a_ctx.receive_buffer, 0, SIGFOX_MTU_DOWNLINK);

  uint8_t transmission_error =
      SIGFOX_API_send_frame((sfx_u8 *)p_data, data_size, l2a_ctx.receive_buffer,
                            tx_repeat, l2a_ctx.downlink_enabled);
  l2a_ctx.busy = false;
  /*
   * We ignore SFX_ERR_INT_GET_RECEIVED_FRAMES_TIMEOUT
   * since its only a timeout error there's no need to reset the session
   */
  if (transmission_error &&
      transmission_error != SFX_ERR_INT_GET_RECEIVED_FRAMES_TIMEOUT)
  {
    l2a_action_tx_status(L2A_L2_ERROR);
    return L2A_SUCCESS;
  }

  /*
   * We check that the data we received is not only 0s, because this means
   * that SIGFOX_API_send_fram failed without yielding an error
   */
  if (l2a_ctx.downlink_enabled &&
      check_downlink_reception(l2a_ctx.receive_buffer, SIGFOX_MTU_DOWNLINK))
  {
    l2a_ctx.events.data_rcvd = true;
  }

  l2a_action_tx_status(L2A_SUCCESS);

  PRINT_DBG("l2a> send data done (size %d)\n", data_size);
  PRINT_HEX_BUF_DBG(p_data, data_size);

  return L2A_SUCCESS;
}

l2a_technology_t l2a_get_technology(void)
{
  return L2A_SIGFOX;
}

uint16_t l2a_get_mtu(void)
{
  return SIGFOX_MTU_UPLINK;
}

/*
 *   A Sigfox message takes 6 seconds to send and the duty cycle limitation is
 * 1% So the time until the next possible transmission is always the same :
 * 600 seconds Source :
 * https://build.sigfox.com/sigfox-radio-configurations-rc#rc-technical-details
 *            https://www.disk91.com/2017/technology/internet-of-things-technology/all-what-you-need-to-know-about-regulation-on-rf-868mhz-for-lpwan/
 */
uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  return NEXT_TX_DELAY;
}

l2a_status_t l2a_process(void)
{
  if (l2a_ctx.events.tx_result)
  {
    l2a_ctx.events.tx_result = false;
    l2a_ctx.callbacks.transmission_result(l2a_ctx.last_tx_status, 0);
    PRINT_DBG("l2a> tx done\n");
    l2a_ctx.last_tx_status = L2A_SUCCESS;
  }

  if (l2a_ctx.events.data_rcvd)
  {
    l2a_ctx.events.data_rcvd = false;
    PRINT_DBG("l2a> downlink packet received\n");
    PRINT_HEX_BUF_DBG(l2a_ctx.receive_buffer, SIGFOX_MTU_DOWNLINK);
    l2a_ctx.callbacks.data_received(SIGFOX_MTU_DOWNLINK,
                                    l2a_ctx.last_tx_status);
  }

  return L2A_SUCCESS;
}

/********************************
 *  Functions used for SigFox.  *
 ********************************/

static sfx_error_t st_sigfox_open(st_sfx_rc_t sgfx_rc)
{
  sfx_error_t error = SFX_ERR_NONE;

  /*record RCZ*/
  switch (sgfx_rc.id)
  {
  case RC1_ID:
  {
    error = SIGFOX_API_open(&sgfx_rc.param);
    break;
  }
  case RC2_ID:
  {
    sfx_u32 config_words[3];

    config_words[0] = E2pData.macroch_config_words_rc2[0];
    config_words[1] = E2pData.macroch_config_words_rc2[1];
    config_words[2] = E2pData.macroch_config_words_rc2[2];

    error = SIGFOX_API_open(&sgfx_rc.param);

    if (error == SFX_ERR_NONE)
    {
      error =
          SIGFOX_API_set_std_config(config_words, RC2_SET_STD_TIMER_ENABLE);
    }

    break;
  }
  case RC3C_ID:
  {
    sfx_u32 config_words[3];

    config_words[0] = E2pData.macroch_config_words_rc3[0];
    config_words[1] = E2pData.macroch_config_words_rc3[1];
    config_words[2] = E2pData.macroch_config_words_rc3[2];

    error = SIGFOX_API_open(&sgfx_rc.param);

    if (error == SFX_ERR_NONE)
    {
      error = SIGFOX_API_set_std_config(config_words, NA);
    }

    break;
  }
  case RC4_ID:
  {
    sfx_u32 config_words[3];

    config_words[0] = E2pData.macroch_config_words_rc4[0];
    config_words[1] = E2pData.macroch_config_words_rc4[1];
    config_words[2] = E2pData.macroch_config_words_rc4[2];

    error = SIGFOX_API_open(&sgfx_rc.param);

    if (error == SFX_ERR_NONE)
    {
      error =
          SIGFOX_API_set_std_config(config_words, RC4_SET_STD_TIMER_ENABLE);
    }

    break;
  }
  default:
  {
    error = SFX_ERR_API_OPEN;
    break;
  }
  }

  return error;
}

static bool check_downlink_reception(uint8_t *p_data, uint8_t data_size)
{
  // A downlink is considered as received when one of the bits is different than 0.
  for (uint8_t i = 0; i < data_size; i++)
  {
    if (p_data[i])
      return true;
  }
  return false;
}

void enable_downlink(const bool enable)
{
  l2a_ctx.downlink_enabled = enable;
}

bool set_tx_repeat_mode(const uint8_t mode)
{
  if (mode < 1 || mode > 3)
    return true;
  tx_repeat = mode;
  return false;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  return false;
}

static void l2a_action_tx_status(l2a_status_t status)
{
  l2a_ctx.events.tx_result = true;
  l2a_ctx.last_tx_status = status;
  l2a_ctx.callbacks.processing_required();
}