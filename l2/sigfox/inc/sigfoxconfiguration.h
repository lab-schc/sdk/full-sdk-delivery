/*
 * configuration_zone.h
 *
 *  Created on: 17 sept. 2019
 *      Author: thibaut
 */

#ifndef FULL_SDK_SIGFOXCONFIGURATION_H_
#define FULL_SDK_SIGFOXCONFIGURATION_H_

#include <stdint.h>
#include <stdbool.h>
#include "st_sigfox_api.h"

/*
 * The other values possible can be found in st_sigfox_api.h
 */
#define APPLI_RC ST_RC1

/*
 * Number of bytes sent for one fragment
*/
#define SGFX_PAYLOAD_PER_MSG 8

/*
 * Sets the initiate_downlink_flag to the value given as parameter
 * If true a downlink will be asked to the SIGFOX network after reception of the uplink
 * If false the uplink will be transmitted without asking for a downlink
 */
void enable_downlink(const bool param);

/*
 * See SIGFOX API for more information about the different repeat modes
 */
bool set_tx_repeat_mode(const uint8_t mode);

typedef enum
{
  NO_EVENT = 0,
  TRANSMISSION_RESULT_EVENT,
  DATA_RECEIVED,
} l2a_event_t;

#endif /* FULL_SDK_SIGFOXCONFIGURATION_H_ */
