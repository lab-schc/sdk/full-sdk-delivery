set(STM_SIGFOX_TOP_DIR /opt/STM32CubeExpansion_SFOX_V1.1.0/Middlewares/Third_Party/Sgfx)
set(L2_INC_DIR
  ${STM_SIGFOX_TOP_DIR}/SigfoxLib
  ${STM_SIGFOX_TOP_DIR}/SigfoxLibTest
  ${STM_SIGFOX_TOP_DIR}/Crypto
  ${STM_SIGFOX_TOP_DIR}/utils
  ${CMAKE_CURRENT_LIST_DIR}/inc
)

set(MAX_MTU_SIZE 12)

find_library(SIGFOX_CORE_LIBRARY SgfxCoreV231_CM0_GCC.a PATHS "${STM_SIGFOX_TOP_DIR}/SigfoxLib")
find_library(SIGFOX_CRYPTO_LIBRARY SgfxCmacV100_CM0_GCC.a PATHS "${STM_SIGFOX_TOP_DIR}/Crypto")
find_library(SIGFOX_ADDON_LIBRARY SgfxAddonV020_CM0_GCC.a PATHS "${STM_SIGFOX_TOP_DIR}/SigfoxLibTest")

# Had to declare a new variable to not use EXTRA_LIBS
set(
  EXTRA_L2_LIBS 
  ${SIGFOX_CORE_LIBRARY} ${SIGFOX_ADDON_LIBRARY} ${SIGFOX_CRYPTO_LIBRARY})