/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "fullsdkl2.h"
#include "platform.h"
#include "serial.h"

#define IPV4_HEADER_SIZE 20
#define UDP_HEADER_SIZE 8

#define MAX_IP_STR_LEN 46

#define AT_COMMAND_TIMEOUT 10
#define AT_JOIN_TIMEOUT 60

#define MAX_CMD_SIZE (64 + MAX_IP_STR_LEN)
#define IO_BUFF_SIZE (MTU * 2 + MAX_CMD_SIZE)

#define IOT_CREATOR_NIDD_APN "nonip.iot.t-mobile.nl"
#define IOT_CREATOR_IP_APN "cdp.iot.t-mobile.nl"
#define IOT_CREATOR_IP "172.27.131.100"
#define IOT_CREATOR_PORT 15683

#define APN_IP "IPV4V6"
#define APN_NIDD "Non-IP"

#define AT_PING_MODEM "AT"                          // Pings the modem, also used to clear output buffer
#define AT_DISABLE_ECHO "ATE0"                      // Disables command echo from the modem
#define AT_ENABLE_URC "AT^SCFG=\"Tcp/WithURCs\",on" // Enable Unsolicited Result Codes
#define AT_REG_NWK_EVENTS "AT+CEREG=1"              // Register to be notified of network related events
#define AT_UNREG_NWK_EVENTS "AT+CEREG=0"
#define AT_LIST_NETWORKS "AT+COPS?"                                  // List the different networks detected
#define AT_ONLY_NBIOT "AT^SXRAT=8"                                   // Only allow NB-IoT connectivity
#define AT_SET_EDRX_PARAM "AT+CEDRXS=1,5,\"%s\""                     // Command to set the eDRX parameter
#define AT_DISABLE_PSM "AT+CPSMS=0"                                  // Disable PSM
#define AT_SET_APN_NAME "AT+CGDCONT=1,\"%s\",\"%s\""                 // Command to set the APN that will be used by the board
#define AT_GET_CONNECTION_STATUS "AT+CEREG?"                         // Get network connection status
#define AT_RESTART_MODULE "AT+CFUN=1,1"                              // Reboots the EXS82 module
#define AT_SLEEP_CONF "AT^SCFG=\"MEopMode/PwrSave\",\"enabled\",0,1" // Configure device's sleep mode
#define AT_DISABLE_TZ_UPDATE "AT+CTZU=0"

// UDP commands
#define AT_ENABLE_NET_SERVICE "AT^SICA=1,1"                    // Activate connection for connection profile 1
#define AT_CREATE_SOCKET "AT^SISS=0,srvType,Socket"            // Create a service profile of type Socket and assign it to index 0
#define AT_BIND_SOCKET "AT^SISS=0,conid,1"                     // Link service profile 0 with connection profile 1
#define AT_CONF_SOCKET "AT^SISS=0,address,\"sockudp://%s:%d\"" // Configure remote address and port
#define AT_OPEN_SOCKET "AT^SISO=0"                             // Activate service profile 0
#define AT_GET_IP "AT^SISO=0,1"                                // Show parameters for service profile 0
#define AT_SEND_DATA "AT^SISW=0,%d"                            // Request to send %d number of bytes
#define AT_RECV_DATA "AT^SISR=0,%d"                            // Receive up to %d number of bytes
#define AT_CLOSE_SOCKET "AT^SISC=0"
#define AT_DISABLE_MODEM "AT^SICA=0,1"

// UDP needles
#define AT_NEEDLE_GET_IP "^SISO: 0,"                // Service profile information
#define AT_NEEDLE_WANT_WRITE "^SISW: 0,"            // Modem wants read
#define AT_NEEDLE_TX_RESULT "^SISW: 0,1"            // Transmission result
#define AT_NEEDLE_RX "^SISR: 0,"                    // Data received
#define AT_NEEDLE_INTERNET_SERVICE_URC "^SIS: 0,0," // Internet service URC

// NIDD commands
#define AT_ENABLE_URC_NIDD "AT+CRTDCP=1"
#define AT_SEND_DATA_NIDD "AT+CSODCP=1,%d,"

// NIDD needles
#define AT_NEEDLE_RX_NIDD "+CRTDCP: 1,"

// Common stuff
#define AT_RESPONSE_OK "OK"                     // No error
#define AT_RESPONSE_ERR "ERROR"                 // An error happened
#define AT_NEEDLE_NET_LOST_URC "+CEREG: 4"      // Network lost
#define AT_NEEDLE_NET_JOIN_URC "+CEREG: 1"      // Network registration done
#define AT_NEEDLE_NET_JOIN_URC_ROAM "+CEREG: 5" // Roaming network registration done
#define AT_NEEDLE_NET_JOIN "+CEREG: 0,1"
#define AT_NEEDLE_NET_JOIN_ROAM "+CEREG: 0,5"
#define AT_NEEDLE_NET_JOINING_URC "+CEREG: 2"
#define AT_NEEDLE_BOOT "+CIEV: prov,0,\"fallb3gpp\""

// For more information about the AT commands see here: https://www.gs-m2m.de/fileadmin/Bilder/GSM_Module/Module/EXS62_82/exs82-w_atc_v01100a.pdf

#define TIME_STR_FORMAT "%T" // Gives the following format: HH:mm:ss (no subsecond precision)
#define TIME_STR_MAX_SIZE sizeof("HH:mm:ss")
char time_str[TIME_STR_MAX_SIZE] = {0};
#define GET_TIME_STR() time_as_formatted_string(time_str, TIME_STR_MAX_SIZE, TIME_STR_FORMAT)

static uint8_t io_buff[IO_BUFF_SIZE] = {0};
static uint8_t dev_l2_ip[MAX_IP_STR_LEN] = {0};
static uint16_t dev_l2_port = 0;

static char serial_port[100] = SERIAL_PATH;
static const char *edrx_param = "0010";      // More info here: https://infocenter.nordicsemi.com/index.jsp?topic=%2Fref_at_commands%2FREF%2Fat_commands%2Fnw_service%2Fcedrxs.html
static const char *apn = IOT_CREATOR_IP_APN; // IoTCreators APN https://docs.iotcreators.com/docs/2-attach-to-nb-iot-network
static const char *apn_type = APN_IP;
static const char *remote_ip = NULL;
static int remote_port = -1;
static bool nidd = false;
static bool traces = false;

static bool initial_setup_done = false;
static bool connection_done = false;

typedef struct
{
  char *cmd;
  const char **expected;
  int nb_try;
  int timeout;
} expect_resp_params_t;

// Function used to signal events to L2A
void l2_event_happened(l2_status_t status);

static int exs_expect_response(const char **responses, int nb_responses, int nb_try, int timeout_value, char **response);

void l2_set_edrx_param(const char *edrx)
{
  edrx_param = edrx;
}

void l2_set_apn(const char *new_apn)
{
  apn = new_apn;
}

void l2_set_remote_ip(const char *new_remote_ip)
{
  remote_ip = new_remote_ip;
}

void l2_set_remote_port(int new_remote_port)
{
  remote_port = new_remote_port;
}

void l2_set_nidd(bool enable)
{
  nidd = enable;
  apn_type = nidd ? APN_NIDD : APN_IP;
}

void l2_set_traces(bool enable)
{
  traces = enable;
}

bool l2_set_serial_port(const char *port_path)
{
  if (port_path == NULL)
  {
    return false;
  }
  size_t port_path_size = strlen(port_path);
  if (port_path_size > sizeof(serial_port))
  {
    return false;
  }
  memcpy(serial_port, port_path, port_path_size);
  return true;
}

void exs_print_packet(const uint8_t *data, uint16_t data_size)
{
  printf("size: %d\n", data_size);
  for (uint16_t i = 0; i < data_size; i++)
  {
    printf("%.2X ", data[i]);
  }
  printf("\n");
}

static l2_status_t exs_write(const char *str)
{
  sprintf((char *)io_buff, "%s\r\n", str);
  if (traces)
  {
    GET_TIME_STR();
    printf("%s COMMAND: %s\n", time_str, str);
  }
  set_rts(0);
  set_rts(1);
  if (serial_writeline(io_buff) != (ssize_t)strlen((const char *)io_buff))
  {
    return L2_ERROR;
  }
  return L2_SUCCESS;
}

static bool parse_ip(const char *response)
{
  uint8_t commas = 6;
  uint8_t start_index = 0;
  uint8_t ip_len = 0;
  bool ipv6 = false;
  for (size_t i = 0; i < strlen(response); i++)
  {
    if (response[i] == ',')
    {
      commas--;
      if (commas == 0 && response[i + 1] != '"')
      {
        return false;
      }
      else if (commas == 0)
      {
        start_index = i + 2;
        // IPv6 are surrounded by brackets
        if (response[i + 2] == '[')
        {
          start_index++;
          i++;
          ipv6 = true;
        }
      }
    }
    else if (commas == 0 && ((!ipv6 && response[i] == ':') || (ipv6 && response[i] == ']')))
    {
      ip_len = i - start_index;
      memset(dev_l2_ip, 0, MAX_IP_STR_LEN);
      memcpy(dev_l2_ip, &response[start_index], ip_len);
      dev_l2_port = atoi(&response[start_index + ip_len + 1 + ipv6]);
      return true;
    }
  }
  return false;
}

void exs_connect(void)
{
  char *response = NULL;
  const char *ok_response = AT_RESPONSE_OK;

  if (!nidd)
  {
    const char *want_write_response = AT_NEEDLE_WANT_WRITE;
    const char *get_ip_response = AT_NEEDLE_GET_IP;

    char conf_socket_cmd[MAX_CMD_SIZE];
    sprintf(conf_socket_cmd, AT_CONF_SOCKET,
            remote_ip != NULL ? remote_ip : IOT_CREATOR_IP,
            remote_port != -1 ? remote_port : IOT_CREATOR_PORT);

    expect_resp_params_t params[] = {
        {AT_ENABLE_URC, &ok_response, 4, AT_COMMAND_TIMEOUT},
        {AT_ENABLE_NET_SERVICE, &ok_response, 6, AT_COMMAND_TIMEOUT},
        {AT_PING_MODEM, &ok_response, 4, AT_COMMAND_TIMEOUT},
        {AT_CREATE_SOCKET, &ok_response, 2, AT_COMMAND_TIMEOUT},
        {AT_BIND_SOCKET, &ok_response, 2, AT_COMMAND_TIMEOUT},
        {conf_socket_cmd, &ok_response, 2, AT_COMMAND_TIMEOUT},
        {AT_OPEN_SOCKET, &want_write_response, 4, AT_COMMAND_TIMEOUT},
        {AT_GET_IP, &get_ip_response, 4, AT_COMMAND_TIMEOUT},
    };

    for (size_t i = 0; i < sizeof(params) / sizeof(expect_resp_params_t); i++)
    {
      exs_write(params[i].cmd);
      if (exs_expect_response(params[i].expected, 1, params[i].nb_try, params[i].timeout, &response) != 0)
      {
        l2_event_happened(L2_ERROR);
        return;
      }
    }

    if (!parse_ip(response))
    {
      fprintf(stderr, "Error: Could not parse device's IP\n");
      l2_event_happened(L2_ERROR);
      return;
    }
    if (exs_expect_response(&ok_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
    {
      l2_event_happened(L2_ERROR);
      return;
    }
    printf("Info: Device IP: %s:%d\n", dev_l2_ip, dev_l2_port);
  }
  else
  {
    exs_write(AT_ENABLE_URC_NIDD);
    if (exs_expect_response(&ok_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
    {
      l2_event_happened(L2_ERROR);
      return;
    }
  }
  l2_event_happened(L2_CONN_AVAIL);
}

void exs_connection_lost(void)
{
  const char *ok_response = AT_RESPONSE_OK;

  l2_event_happened(L2_CONN_LOST);

  if (!nidd)
  {
    expect_resp_params_t params[] = {
        {AT_CLOSE_SOCKET, &ok_response, 4, AT_COMMAND_TIMEOUT},
        {AT_DISABLE_MODEM, &ok_response, 4, AT_COMMAND_TIMEOUT},
    };

    char *response = NULL;
    for (size_t i = 0; i < sizeof(params) / sizeof(expect_resp_params_t); i++)
    {
      exs_write(params[i].cmd);
      if (exs_expect_response(params[i].expected, 1, params[i].nb_try, params[i].timeout, &response) != 0)
      {
        return l2_event_happened(L2_ERROR);
      }
    }
  }
}

// Check for Unsolicited Result Codes that inform us of something happening in the EXS-82
static void check_urc(const char *buff)
{
  if ((!nidd && strstr(buff, AT_NEEDLE_RX)) || (nidd && strstr(buff, AT_NEEDLE_RX_NIDD)))
  {
    l2_event_happened(L2_DOWN_AVAIL);
  }
  else if (!connection_done && (strstr(buff, AT_NEEDLE_NET_JOIN_URC) || strstr(buff, AT_NEEDLE_NET_JOIN_URC_ROAM)))
  {
    connection_done = true;
    if (initial_setup_done)
    {
      exs_connect();
    }
  }
  else if ((initial_setup_done && connection_done) && (strstr(buff, AT_NEEDLE_NET_JOINING_URC) || strstr(buff, AT_NEEDLE_NET_LOST_URC)))
  {
    connection_done = false;
    exs_connection_lost();
  }
  else if (strstr(buff, AT_NEEDLE_INTERNET_SERVICE_URC))
  {
    int urc_id = atoi(buff + sizeof(AT_NEEDLE_INTERNET_SERVICE_URC) - 1);
    if (urc_id != 0)
    {
      exs_connection_lost();
    }
  }
}

/**
 * @brief reads a line from the serial nb_try times and compares it to the strings in responses, if a response is found *response will be set to the result of strstr
 *
 * @param responses responses expected
 * @param nb_responses number of possible responses to be expected
 * @param nb_try maximum number of lines to be read before quitting
 * @param response pointer that will be set to the beginning of the found response if there is one
 *
 * @return int index of the response found, -1 if there has been an error with the serial and nb_responses if none has been found without other error
 */
static int exs_expect_response(const char **responses, int nb_responses, int nb_try, int timeout_value, char **response)
{
  struct pollfd fds;
  int ret;
  char *substr = NULL;

  fds.fd = serial_get_fd();
  fds.events = POLLIN;

  // Try nb_try times to read a response and wait timeout_value seconds each time
  for (int j = 0; j < nb_try; j++)
  {
    ret = poll(&fds, 1, timeout_value * 1000);
    if (ret == 0 || (ret == -1 && errno == EINTR))
    {
      continue;
    }
    if (ret == -1 || fds.revents & POLLHUP)
    {
      // Broken FD
      fprintf(stderr, "ERROR: in poll: %s\n", strerror(errno));
      return -1;
    }

    int red = serial_readline(io_buff);
    if (red == -1)
    {
      return -1;
    }
    if (traces && isprint(io_buff[0]))
    {
      GET_TIME_STR();
      printf("%s RESPONSE: %s\n", time_str, io_buff);
    }
    for (int i = 0; i < nb_responses; i++)
    {
      if ((substr = strstr((const char *)io_buff, responses[i])) != NULL)
      {
        *response = substr;
        return i;
      }
    }
    check_urc((const char *)io_buff);
  }
  return nb_responses;
}

void exs_fini(void)
{
  serial_fini();
}

l2_status_t exs_init(void)
{
  char edrx_cmd[MAX_CMD_SIZE];
  char apn_cmd[MAX_CMD_SIZE];
  const char *ok_response = AT_RESPONSE_OK;
  const char *boot_ok_response = AT_NEEDLE_BOOT;
  char *response = NULL;

  if (serial_init(serial_port, EXS82_BAUDRATE) != SERIAL_SUCCESS)
  {
    return L2_ERROR;
  }

  sprintf(edrx_cmd, AT_SET_EDRX_PARAM, edrx_param);
  sprintf(apn_cmd, AT_SET_APN_NAME, apn_type, apn);

  exs_write(AT_RESTART_MODULE);
  exs_expect_response(&boot_ok_response, 1, 10, AT_COMMAND_TIMEOUT, &response);

  {
    expect_resp_params_t params[] = {
        {AT_DISABLE_ECHO, &ok_response, 4, AT_COMMAND_TIMEOUT},
        {AT_REG_NWK_EVENTS, &ok_response, 4, AT_COMMAND_TIMEOUT},
        {AT_SLEEP_CONF, &ok_response, 4, AT_COMMAND_TIMEOUT},
        {AT_DISABLE_TZ_UPDATE, &ok_response, 2, AT_COMMAND_TIMEOUT},
        {edrx_cmd, &ok_response, 2, AT_COMMAND_TIMEOUT},
        {AT_DISABLE_PSM, &ok_response, 2, AT_COMMAND_TIMEOUT},
        {apn_cmd, &ok_response, 4, AT_COMMAND_TIMEOUT},
    };

    for (size_t i = 0; i < sizeof(params) / sizeof(expect_resp_params_t); i++)
    {
      exs_write((params[i].cmd));
      if (exs_expect_response(params[i].expected, 1, params[i].nb_try, params[i].timeout, &response) != 0)
      {
        return L2_ERROR;
      }
    }
  }
  initial_setup_done = true;

  if (connection_done)
  {
    exs_connect();
  }

  return L2_SUCCESS;
}

l2_status_t exs_receive_ip(uint8_t *data, uint16_t *size)
{
  ssize_t ret = 0;
  char command[MAX_CMD_SIZE] = {0};

  serial_set_canon(true);
  sprintf(command, AT_RECV_DATA, MTU);
  exs_write(command);

  if ((ret = serial_readdata(io_buff, 0)) == -1)
  {
    return L2_ERROR;
  }
  serial_set_canon(false);

  // Here we just read the AT string indicating the number of bytes and received,
  // the following newline and the data payload at once so we have to parse it manually
  uint16_t offset = 0;
  if (io_buff[0] == '\r' || io_buff[0] == '\n')
  {
    offset++;
  }
  if (io_buff[1] == '\r' || io_buff[1] == '\n')
  {
    offset++;
  }
  offset += sizeof(AT_NEEDLE_RX) - 1;
  uint16_t rx_size = (uint16_t)atoi((char *)(io_buff + offset));
  // Count the number of characters used to write the number of bytes received
  offset++;
  int magnitude[] = {9, 99, 999};
  for (size_t i = 0; i < 3; i++)
  {
    if (rx_size > magnitude[i])
    {
      offset++;
    }
  }
  // Go over new line characters
  offset += 2;

  if (traces)
  {
    printf("L2 DOWNLINK:\t");
    exs_print_packet(io_buff + offset, rx_size);
  }

  memcpy(data, io_buff + offset, ret);
  *size = ret;

  return L2_SUCCESS;
}

static bool hex_to_bin(uint8_t *bin_buff, const char *hex, uint16_t size)
{
  for (uint16_t i = 0; i < size; ++i)
  {
    char str_byte[3] = {0};
    char *err = NULL;
    memcpy(str_byte, hex + 2 * i, 2);
    if (!isxdigit(str_byte[0]) || !isxdigit(str_byte[1]))
    {
      fprintf(stderr, "Error: string not HEX %c%c\n", str_byte[0], str_byte[1]);
      return false;
    }
    bin_buff[i] = (uint8_t)strtol(str_byte, &err, 16);
    if (*err)
    {
      fprintf(stderr, "Error: strtol() failed: %s\n", strerror(errno));
      return false;
    }
  }
  return true;
}

l2_status_t exs_receive_nidd(uint8_t *data, uint16_t *size)
{
  int offset = sizeof(AT_NEEDLE_RX_NIDD) - 1;

  *size = atoi((char *)(io_buff + offset));
  // Go to beginning of hex string
  for (; io_buff[offset] != '\"'; offset++)
    ;
  offset++;
  return hex_to_bin(data, (char *)(io_buff + offset), (uint16_t)(*size)) ? L2_SUCCESS : L2_ERROR;
}

l2_status_t exs_receive(uint8_t *data, uint16_t *size)
{
  return nidd ? exs_receive_nidd(data, size) : exs_receive_ip(data, size);
}

l2_status_t exs_poll_downlink(bool block)
{
  serial_status_t ser_status = SERIAL_SUCCESS;

  if ((ser_status = serial_poll(block)) == SERIAL_INTERN_ERR)
  {
    return L2_ERROR;
  }
  if (ser_status != SERIAL_LINE_AVAIL)
  {
    return ser_status == SERIAL_SUCCESS ? L2_SUCCESS : L2_INTERN_ERR;
  }
  serial_readline(io_buff);
  if (strcmp((char *)io_buff, "\r\n") != 0)
  {
    GET_TIME_STR();
    printf("%s RESPONSE: %s\n", time_str, io_buff);
  }
  check_urc((const char *)io_buff);
  return L2_SUCCESS;
}

l2_status_t exs_send_data_ip(const uint8_t *p_data, uint16_t data_size)
{
  const char *ok_response = AT_RESPONSE_OK;
  const char *want_write_response = AT_NEEDLE_WANT_WRITE;
  const char *tx_result_response = AT_NEEDLE_TX_RESULT;
  char *response = NULL;
  char command[MAX_CMD_SIZE] = {0};

  sprintf(command, AT_SEND_DATA, data_size);
  exs_write(command);
  if (exs_expect_response(&want_write_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  int can_send = atoi(response + sizeof(AT_NEEDLE_WANT_WRITE) - 1);
  if (can_send < data_size)
  {
    fprintf(stderr, "Error: modem reported it can send %d bytes but %d were requested\n", can_send, data_size);
    return L2_ERROR;
  }

  if (traces)
  {
    printf("L2 UPLINK:\t");
    exs_print_packet(p_data, data_size);
  }

  if (serial_set_canon(true) != SERIAL_SUCCESS)
  {
    return L2_ERROR;
  }

  if (serial_writedata(p_data, data_size) == -1)
  {
    return L2_ERROR;
  }

  if (serial_set_canon(false) != SERIAL_SUCCESS)
  {
    return L2_ERROR;
  }

  if (exs_expect_response(&ok_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }

  if (exs_expect_response(&tx_result_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }

  return L2_SUCCESS;
}

l2_status_t exs_send_data_nidd(const uint8_t *p_data, uint16_t data_size)
{
  const char *ok_response = AT_RESPONSE_OK;
  char *response = NULL;
  char *command = malloc(sizeof(uint8_t) * (sizeof(AT_SEND_DATA_NIDD) + 4 + data_size * 2));

  int idx = sprintf(command, AT_SEND_DATA_NIDD, data_size);
  for (size_t i = 0; i < data_size; i++)
  {
    sprintf(command + idx + (i * 2), "%.2X", p_data[i]);
  }
  exs_write(command);
  free(command);
  return exs_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) == 0 ? L2_SUCCESS : L2_ERROR;
}

l2_status_t exs_send_data(const uint8_t *p_data, uint16_t data_size)
{
  set_rts(0);
  set_rts(1);
  return nidd ? exs_send_data_nidd(p_data, data_size) : exs_send_data_ip(p_data, data_size);
}
