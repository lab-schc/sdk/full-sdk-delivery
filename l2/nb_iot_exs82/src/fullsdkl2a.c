/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <stdio.h>
#include <stdlib.h>

#include "fullsdkl2.h"
#include "fullsdkl2a.h"
#include "platform.h"
#include "serial.h"

static l2a_callbacks_t l2a_cb;
static uint8_t *l2a_rx_buffer;
static uint16_t l2a_rx_buffer_size;

static enum {
  NO_EVENT = 0,
  CONNECTIVITY_AVAILABLE_EVENT = 1 << 0,
  TRANSMISSION_RESULT_EVENT = 1 << 1,
  DOWNLINK_AVAILABLE_EVENT = 1 << 2,
  CONNECTIVITY_LOST_EVENT = 1 << 3,
  CRITICAL_ERROR = 1 << 4,
} event;

bool connected = false;

void l2_event_happened(l2_status_t status)
{
  switch (status)
  {
  case L2_DOWN_AVAIL:
    event |= DOWNLINK_AVAILABLE_EVENT;
    l2a_cb.processing_required();
    break;
  case L2_CONN_LOST:
    if (connected)
    {
      connected = false;
      event |= CONNECTIVITY_LOST_EVENT;
      l2a_cb.processing_required();
    }
    break;
  case L2_CONN_AVAIL:
    if (!connected)
    {
      connected = true;
      event |= CONNECTIVITY_AVAILABLE_EVENT;
      l2a_cb.processing_required();
    }
    break;
  case L2_SUCCESS:
    break;
  default:
    event |= CRITICAL_ERROR;
    break;
  }
}

static void downlink_available_callback(void)
{
  if (exs_poll_downlink(true) != L2_SUCCESS)
  {
    event |= CRITICAL_ERROR;
  }
}

l2a_status_t l2a_initialize(const l2a_callbacks_t *pp_callbacks,
                            uint8_t *p_receive_buffer,
                            uint16_t receive_buffer_size)
{
  printf("l2a_initialize() called\n");

  if (!pp_callbacks || !pp_callbacks->connectivity_available ||
      !pp_callbacks->connectivity_lost || !pp_callbacks->data_received ||
      !pp_callbacks->processing_required || !pp_callbacks->transmission_result)
  {
    return L2A_REQ_CALLBACK_ERR;
  }
  if (!p_receive_buffer)
  {
    return L2A_BUFFER_ERR;
  }

  l2a_cb = *pp_callbacks;
  l2a_rx_buffer = p_receive_buffer;
  l2a_rx_buffer_size = receive_buffer_size;

  event = NO_EVENT;

  if (exs_init() != L2_SUCCESS)
  {
    return L2A_L2_ERROR;
  }

  if (!watch_fd_for_input(serial_get_fd(), &downlink_available_callback))
  {
    fprintf(stderr, "Error: watch_fd_for_input() failed\n");
    return L2A_CONNECT_ERR;
  }

  return L2A_SUCCESS;
}

l2a_status_t l2a_send_data(const uint8_t *p_data, uint16_t data_size)
{
  l2_status_t err = exs_send_data(p_data, data_size);
  if (err)
  {
    fprintf(stderr, "Error in exs_send_data()\n");
    event |= CRITICAL_ERROR;
    l2a_cb.processing_required();
    return L2A_L2_ERROR;
  }
  event |= TRANSMISSION_RESULT_EVENT;
  l2a_cb.processing_required();
  return L2A_SUCCESS;
}

l2a_technology_t l2a_get_technology(void)
{
  return L2A_DEFAULT;
}

uint16_t l2a_get_mtu(void)
{
  return MTU;
}

uint32_t l2a_get_next_tx_delay(uint16_t data_size)
{
  (void)data_size;
  return NEXT_TX_DELAY;
}

static void read_downlink(void)
{
  uint16_t size = 0;

  if (exs_receive(l2a_rx_buffer, &size) != L2_SUCCESS)
  {
    // this case means that the board lost connection to the network
    event |= CRITICAL_ERROR;
  }
  else
  {
    l2a_cb.data_received(size, L2A_SUCCESS);
  }
}

l2a_status_t l2a_process(void)
{
  l2a_status_t status = L2A_SUCCESS;

  if (event & CRITICAL_ERROR)
  {
    exs_fini();
    status = L2A_L2_ERROR;
  }
  if (event & CONNECTIVITY_AVAILABLE_EVENT)
  {
    l2a_cb.connectivity_available();
  }
  else if (event & CONNECTIVITY_LOST_EVENT)
  {
    l2a_cb.connectivity_lost();
  }
  if (event & TRANSMISSION_RESULT_EVENT)
  {
    l2a_cb.transmission_result(L2A_SUCCESS, 0);
  }
  if (event & DOWNLINK_AVAILABLE_EVENT)
  {
    read_downlink();
  }
  event = NO_EVENT;
  return status;
}

bool l2a_get_dev_iid(uint8_t **dev_iid)
{
  (void)dev_iid;
  return false;
}