/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#ifndef SERIAL_H_
#define SERIAL_H_

#include <termios.h>
#include <stdbool.h>
#include <stdint.h>

#define EXS82_BAUDRATE B115200

#ifndef SERIAL_PATH
#define SERIAL_PATH "/dev/ttyUSB0"
#endif

typedef enum
{
  SERIAL_SUCCESS,
  SERIAL_LINE_AVAIL,
  SERIAL_PARAM_ERR,
  SERIAL_NOT_INIT_ERR,
  SERIAL_ALRDY_INIT_ERR,
  SERIAL_INTERN_ERR
} serial_status_t;

serial_status_t serial_init(const char *path, speed_t baudrate);
void serial_fini(void);
int32_t serial_readline(uint8_t *l);
int32_t serial_readdata(uint8_t *l, uint16_t read_size);
ssize_t serial_writeline(const uint8_t *l);
serial_status_t serial_poll(bool block);
int32_t serial_get_fd(void);
ssize_t serial_writedata(const uint8_t *data, uint16_t data_size);
serial_status_t serial_set_canon(bool enable);
bool set_rts(unsigned short level);
bool set_dtr(unsigned short level);

#endif
