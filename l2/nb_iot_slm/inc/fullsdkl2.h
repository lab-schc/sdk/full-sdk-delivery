/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#ifndef FULLSDKL2_H_
#define FULLSDKL2_H_

#include <stdbool.h>
#include <stdint.h>

#include "fullsdkl2a.h"
#include "fullsdkl2common.h"

#define NEXT_TX_DELAY 2000 // 2s arbitrary value
#define MTU 1500           // To match recommended ipv6 MTU

void l2_set_apn(const char *new_apn);
void l2_set_nidd(bool enable);
void l2_set_traces(bool enable);
void l2_set_remote_ip(const char *new_remote_ip);
void l2_set_remote_port(int new_remote_port);
bool l2_set_serial_port(const char *port_path);

l2_status_t slm_init(void);
void slm_reset(void);
void slm_fini(void);
l2_status_t slm_connect(void);
l2_status_t slm_receive(uint8_t *data, uint16_t *size);
l2_status_t slm_poll_downlink(bool block);
l2_status_t slm_send_data(const uint8_t *p_data, uint16_t data_size);

#endif
