/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#ifndef GEN_IPV4_H_
#define GEN_IPV4_H_

#include <stdint.h>

bool generate_ipv4_udp_packet(const char *src_ip, uint16_t src_port, const char *dst_ip, uint16_t dst_port, const uint8_t *payload, uint16_t payload_size, uint8_t *packet_out, uint16_t *packet_out_size);

#endif