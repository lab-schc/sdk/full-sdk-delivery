/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <errno.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fullsdkl2.h"
#include "serial.h"
#include "utils.h"

#define MAX_CMD_SIZE 64
#define IO_BUFF_SIZE (MTU * 2 + MAX_CMD_SIZE)

#define EXIT_DATA_MODE_DELAY 2

#define IOT_CREATOR_NIDD_APN "nonip.iot.t-mobile.nl"
#define IOT_CREATOR_IP_APN "cdp.iot.t-mobile.nl"
#define IOT_CREATOR_IP "172.27.131.100"
#define IOT_CREATOR_PORT 15683
#define L2_SRC_PORT 4444

#define AT_PING_MODEM "AT"                           // Pings the modem, also used to clear output buffer
#define AT_FACTORY_RESET "AT%XFACTORYRESET=0"        // Resets the board's network parameters
#define AT_ONLY_NBIOT "AT%XSYSTEMMODE=0,1,0,0"       // Only activates NB IoT network on the board
#define AT_REG_NWK_EVENTS "AT+CEREG=5"               // Register to be notified of network related events
#define AT_DISABLE_PSM "AT+CPSMS=0"                  // Disable PSM
#define AT_SET_EDRX_PARAM "AT+CEDRXS=1,5,\"%s\""     // Command to set the eDRX parameter
#define AT_SET_APN_NAME "AT+CGDCONT=0,\"%s\",\"%s\"" // Command to set the APN that will be used by the board
#define AT_ENABLE_MODEM "AT+CFUN=1"                  // Join the network
#define AT_DISABLE_MODEM "AT+CFUN=0"                 // Leave the network
#define AT_DATACTRL "AT#XDATACTRL=60"                // Delay (in ms) after which SLM considers a packet has finished being written on the serial (Empirically chosen value)
#define AT_OPEN_SOCKET "AT#XRAWCLI=1"                // Create a RAW socket client (see doc for XUDPCLI)
#define AT_CLOSE_SOCKET "AT#XRAWCLI=0"               // Closes the RAW socket client
#define AT_ENTER_DATA_MODE "AT#XRAWSEND"             // Send a packet through the RAW socket client
#define AT_GET_IP "AT+CGDCONT?"                      // Get network infos, among which the board's IP
#define AT_REG_PKT_EVENTS "AT+CGEREP=1"              // Subscribes packet domain event notifications
#define AT_CONF_UNSOLICITED_RES_CODE "AT+CSCON=3"    // Subscribes and configures unsolicited result code notifications

// For more information about the AT commands see here: https://infocenter.nordicsemi.com/index.jsp?topic=%2Fref_at_commands%2FREF%2Fat_commands%2Fintro.html

#define APN_NON_IP "Non-ip"
#define APN_IP "IPV4V6"

#define DATA_MODE_TERMINATOR "+++" // Character sequence to exit data mode

#define AT_RESPONSE_OK "OK"
#define AT_RESPONSE_ERR "ERROR"

#define AT_NEEDLE_CONNECTED "+CEREG: 1,"
#define AT_NEEDLE_CONNECTED_ROAMING "+CEREG: 5,"
#define AT_NEEDLE_NWK_EVENT "+CEREG"
#define AT_NEEDLE_GET_IP "+CGDCONT:"

#define IPV4_HEADER_SIZE 20
#define UDP_HEADER_SIZE 8

#define MAX_IP_STR_LEN 46

#define AT_COMMAND_TIMEOUT 10

static uint8_t io_buff[IO_BUFF_SIZE] = {0};
static uint16_t rx_size = 0;

static const char *edrx_param = "0010"; // More info here: https://infocenter.nordicsemi.com/index.jsp?topic=%2Fref_at_commands%2FREF%2Fat_commands%2Fnw_service%2Fcedrxs.html

static char serial_port[100] = SERIAL_PATH;
static const char *apn = IOT_CREATOR_IP_APN; // IoTCreators APN https://docs.iotcreators.com/docs/2-attach-to-nb-iot-network
static const char *apn_type = APN_IP;
static const char *remote_ip = NULL;
static int remote_port = -1;
static bool nidd = false;

static bool data_mode = false;

static bool traces = false;

static uint8_t dev_l2_ip[MAX_IP_STR_LEN] = {0};

void l2_set_edrx_param(const char *edrx)
{
  edrx_param = edrx;
}

void l2_set_apn(const char *new_apn)
{
  apn = new_apn;
}

void l2_set_remote_ip(const char *new_remote_ip)
{
  remote_ip = new_remote_ip;
}

void l2_set_remote_port(int new_remote_port)
{
  remote_port = new_remote_port;
}

void l2_set_nidd(bool enable)
{
  nidd = enable;
  apn_type = nidd ? APN_NON_IP : APN_IP;
}

void l2_set_traces(bool enable)
{
  traces = enable;
}

bool l2_set_serial_port(const char *port_path)
{
  if (port_path == NULL)
  {
    return false;
  }
  size_t port_path_size = strlen(port_path);
  if (port_path_size > sizeof(serial_port))
  {
    return false;
  }
  memcpy(serial_port, port_path, port_path_size);
  return true;
}

void slm_print_packet(const uint8_t *data, uint16_t data_size)
{
  printf("size: %d\n", data_size);
  for (uint16_t i = 0; i < data_size; i++)
  {
    printf("%.2X ", data[i]);
  }
  printf("\n");
}

l2_status_t slm_init(void)
{
  if (serial_init(serial_port, NRF9160_BAUDRATE) != SERIAL_SUCCESS)
  {
    return L2_ERROR;
  }
  return L2_SUCCESS;
}

static l2_status_t slm_write(const char *str)
{
  sprintf(io_buff, "%s\r\n", str);
  if (traces)
  {
    printf("COMMAND:\t%s\n", str);
  }
  if (serial_writeline(io_buff) != (ssize_t)strlen(io_buff))
  {
    return L2_ERROR;
  }
  return L2_SUCCESS;
}

l2_status_t slm_enter_data_mode()
{
  if (slm_write(AT_ENTER_DATA_MODE) != SERIAL_SUCCESS)
  {
    return L2_ERROR;
  }
  data_mode = true;
  if (serial_set_canon(data_mode) != SERIAL_SUCCESS)
  {
    return L2_ERROR;
  }
}

/**
 * @brief reads a line from the serial nb_try times and compares it to the strings in responses, if a response is found *response will be set to the result of strstr
 *
 * @param responses responses expected
 * @param nb_responses number of possible responses to be expected
 * @param nb_try maximum number of lines to be read before quitting
 * @param response pointer that will be set to the beginning of the found response if there is one
 * @return int index of the response found, -1 if there has been an error with the serial and nb_responses if none has been found without other error
 */
static int slm_expect_response(const char **responses, int nb_responses, int nb_try, int timeout_value, char **response)
{
  fd_set set;
  int ret;
  struct timeval timeout;
  char *substr = NULL;

  FD_ZERO(&set);
  FD_SET(serial_get_fd(), &set);
  timeout.tv_sec = 0;
  timeout.tv_usec = timeout_value * 1000;

  for (int j = 0; j < nb_try; j++)
  {
    ret = select(1, &set, NULL, NULL, &timeout);
    if (ret == -1)
    {
      fprintf(stderr, "ERROR: in select: %s\n", strerror(errno));
      return -1;
    }

    int red = serial_readline(io_buff);
    if (red == -1)
    {
      return -1;
    }
    if (traces && isprint(io_buff[0]))
    {
      printf("RESPONSE: %s\n", io_buff);
    }
    for (int i = 0; i < nb_responses; i++)
    {
      if ((substr = strstr(io_buff, responses[i])) != NULL)
      {
        *response = substr;
        return i;
      }
    }
  }
  return nb_responses;
}

l2_status_t slm_exit_data_mode()
{
  sleep(EXIT_DATA_MODE_DELAY);
  serial_writedata(DATA_MODE_TERMINATOR, sizeof(DATA_MODE_TERMINATOR) - 1);
  data_mode = false;
  if (serial_set_canon(data_mode) != SERIAL_SUCCESS)
  {
    return L2_ERROR;
  }
  sleep(EXIT_DATA_MODE_DELAY);
  return L2_SUCCESS;
}

void slm_reset(void)
{
  const char *ok_response = AT_RESPONSE_OK;
  char *response = NULL;

  if (data_mode)
  {
    slm_exit_data_mode();
  }
  serial_set_canon(false);
  slm_write(AT_CLOSE_SOCKET);
  slm_expect_response(&ok_response, 1, 3, AT_COMMAND_TIMEOUT, &response);
  slm_write(AT_DISABLE_MODEM);
  slm_expect_response(&ok_response, 1, 3, AT_COMMAND_TIMEOUT, &response);
}

void slm_fini(void)
{
  slm_reset();
  serial_fini();
}

static bool parse_ip(const uint8_t *response)
{
  uint8_t commas = 3;
  uint8_t start_index = 0;
  uint8_t ip_len = 0;
  for (size_t i = 0; i < strlen(response); i++)
  {
    if (response[i] == ',')
    {
      commas--;
      if (commas == 0 && response[i + 1] != '"')
      {
        return false;
      }
      else if (commas == 0)
      {
        start_index = i + 2;
      }
    }
    else if (commas == 0 && response[i] == '"' && response[i + 1] == ',')
    {
      ip_len = i - start_index;
      memset(dev_l2_ip, 0, MAX_IP_STR_LEN);
      memcpy(dev_l2_ip, &response[start_index], ip_len);
      return true;
    }
  }
  return false;
}

l2_status_t slm_connect(void)
{
  const char *ok_response = AT_RESPONSE_OK;
  char *response = NULL;
  char command[50] = {0};

  slm_write(AT_PING_MODEM);
  if (slm_expect_response(&ok_response, 1, 5, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  slm_write(AT_DISABLE_MODEM);
  if (slm_expect_response(&ok_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  slm_write(AT_CLOSE_SOCKET);
  if (slm_expect_response(&ok_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  slm_write(AT_FACTORY_RESET);
  if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  slm_write(AT_ONLY_NBIOT);
  if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  slm_write(AT_REG_NWK_EVENTS);
  if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  slm_write(AT_DISABLE_PSM);
  if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  sprintf(command, AT_SET_EDRX_PARAM, edrx_param);
  slm_write(command);
  if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  sprintf(command, AT_SET_APN_NAME, apn_type, apn);
  slm_write(command);
  if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }

  if (nidd)
  {
    slm_write(AT_REG_PKT_EVENTS);
    if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
    {
      return L2_ERROR;
    }
    slm_write(AT_CONF_UNSOLICITED_RES_CODE);
    if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
    {
      L2_ERROR;
    }
  }

  slm_write(AT_ENABLE_MODEM);
  if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  const char *connect_response[] = {AT_NEEDLE_CONNECTED, AT_NEEDLE_CONNECTED_ROAMING};
  int resp = slm_expect_response(connect_response, 2, 10, AT_COMMAND_TIMEOUT, &response);
  if (resp != 0 && resp != 1)
  {
    return L2_ERROR;
  }

  if (!nidd)
  {
    slm_write(AT_GET_IP);
    const char *get_ip_response = AT_NEEDLE_GET_IP;
    if (slm_expect_response(&get_ip_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
    {
      return L2_ERROR;
    }
    if (!parse_ip(response))
    {
      return L2_ERROR;
    }
    printf("Info: Device IPv4: %s\n", dev_l2_ip);
    if (slm_expect_response(&ok_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
    {
      return L2_ERROR;
    }
  }

  slm_write(AT_OPEN_SOCKET);
  if (slm_expect_response(&ok_response, 1, 4, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  slm_write(AT_DATACTRL);
  if (slm_expect_response(&ok_response, 1, 2, AT_COMMAND_TIMEOUT, &response) != 0)
  {
    return L2_ERROR;
  }
  return slm_enter_data_mode();
}

static uint8_t str_hex_to_hex(const uint8_t *str)
{
  uint8_t base[] = "0123456789ABCDEF";
  uint8_t ret = 0;

  for (uint8_t i = 0; i < 2; i++)
  {
    for (uint8_t j = 0; j < sizeof(base) - 1; j++)
    {
      if (base[j] == str[i])
      {
        ret += j;
        if (i == 0)
        {
          ret = ret << 4;
        }
        break;
      }
    }
  }
  return ret;
}

l2_status_t slm_receive(uint8_t *data, uint16_t *size)
{
  uint16_t ret = 0;

  if ((ret = serial_readline(io_buff)) == -1)
  {
    return L2_ERROR;
  }
  uint8_t *rcv;
  uint16_t rcv_size;

  if (traces)
  {
    printf("L2 DOWNLINK:\t");
    slm_print_packet(io_buff, ret);
  }

  if (nidd)
  {
    rcv = io_buff;
    rcv_size = ret;
  }
  else
  {
    if (io_buff[0] != 0x45 || ret < IPV4_HEADER_SIZE + UDP_HEADER_SIZE)
    {
      data_mode = false;
      return L2_ERROR;
    }
    rcv = io_buff + (IPV4_HEADER_SIZE + UDP_HEADER_SIZE);
    rcv_size = ret - (IPV4_HEADER_SIZE + UDP_HEADER_SIZE);
  }

  memcpy(data, rcv, rcv_size);
  *size = rcv_size;
  return L2_SUCCESS;
}

l2_status_t slm_poll_downlink(bool block)
{
  char *substr = NULL;
  l2_status_t status = L2_SUCCESS;

  if (serial_poll(block) == SERIAL_INTERN_ERR)
  {
    return L2_ERROR;
  }
  if (data_mode)
  {
    return L2_DOWN_AVAIL;
  }
  serial_readline(io_buff);
  if (strstr(io_buff, AT_NEEDLE_NWK_EVENT))
  {
    int event_type = 0;
    substr = io_buff + sizeof(AT_NEEDLE_NWK_EVENT) + 1;
    sscanf(substr, "%d", &event_type);
    if (event_type != 1 && event_type != 5)
    {
      status = L2_CONN_LOST;
    }
    else
    {
      status = L2_CONN_AVAIL;
    }
  }
  return status;
}

l2_status_t slm_send_data(const uint8_t *p_data, uint16_t data_size)
{
  int written = 0;
  uint8_t *packet;
  uint16_t pkt_size = 0;

  if (!data_mode)
  {
    slm_enter_data_mode();
  }

  if (nidd)
  {
    packet = (uint8_t *)p_data; // We know p_data won't be modified here
    pkt_size = data_size;
  }
  else
  {
    if ((packet = malloc(data_size + (IPV4_HEADER_SIZE + UDP_HEADER_SIZE))) == NULL)
    {
      return L2_INTERN_ERR;
    }
    if (!generate_ipv4_udp_packet(dev_l2_ip, L2_SRC_PORT,
                                  remote_ip != NULL ? remote_ip : IOT_CREATOR_IP,
                                  remote_port != -1 ? remote_port : IOT_CREATOR_PORT,
                                  p_data, data_size, packet, &pkt_size))
    {
      return L2_INTERN_ERR;
    }
  }

  if (traces)
  {
    printf("L2 UPLINK:\t");
    slm_print_packet(packet, pkt_size);
  }

  if (serial_writedata(packet, pkt_size) == -1)
  {
    return L2_ERROR;
  }
  return L2_SUCCESS;
}