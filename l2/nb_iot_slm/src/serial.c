/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <poll.h>
#include <errno.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <stdint.h>
#include <stdbool.h>

#include "serial.h"

#define MAX_LINE_SIZE 4096 // max line size from `man termios`

static int32_t fd = -1;
static uint8_t line[MAX_LINE_SIZE];
static uint16_t line_size = 0;
static bool data_mode = false;

int32_t serial_get_fd(void)
{
  return fd;
}

serial_status_t serial_init(const uint8_t *path, speed_t baudrate)
{
  struct termios options;

  if (!path || !baudrate)
  {
    return SERIAL_PARAM_ERR;
  }
  if (fd != -1)
  {
    return SERIAL_ALRDY_INIT_ERR;
  }
  line[0] = '\0';
  fd = open(path, O_RDWR | O_NOCTTY | O_SYNC | O_DSYNC);
  if (fd == -1)
  {
    if (errno == EACCES)
      fprintf(stderr, "Error: cannot open serial port due to bad permission."
                      "\nYou can try running it as root, or add yourself to the "
                      "'dialout' Linux group.\n");
    return SERIAL_INTERN_ERR;
  }
  if (!isatty(fd))
  {
    fprintf(stderr, "Error: \"%s\" is not a tty file\n", path);
    return SERIAL_INTERN_ERR;
  }
  if (ioctl(fd, TIOCEXCL, NULL) < 0)
  {
    fprintf(stderr, "Error: Cannot get exclusivity on serial port\n");
    return SERIAL_INTERN_ERR;
  }
  cfsetispeed(&options, baudrate);
  cfsetospeed(&options, baudrate);
  if (tcgetattr(fd, &options) == -1)
  {
    return SERIAL_INTERN_ERR;
  }
  options.c_cflag |= (CLOCAL | CREAD);
  options.c_lflag |= ICANON;
  if (tcsetattr(fd, TCSANOW, &options) == -1)
  {
    return SERIAL_INTERN_ERR;
  }
  if (tcflush(fd, TCIOFLUSH) == -1)
  {
    return SERIAL_INTERN_ERR;
  }
  return SERIAL_SUCCESS;
}

void serial_fini(void)
{
  if (fd == -1)
    return;
  close(fd);
  fd = -1;
}

static ssize_t read_line(void)
{
  ssize_t ret = 0;
  size_t bread = 0;

  do
  {
    ret = read(fd, line + bread, sizeof(line) - bread);
    bread += ret;
  } while ((ret != 0 && data_mode) || (ret == -1 && errno == EINTR && !data_mode));
  if (ret < 0 || (!data_mode && (line[bread - 1] != '\r' && line[bread - 1] != '\n')))
  {
    fprintf(stderr, "Error: serial error or EOF received. %s\n", strerror(errno));
    return -1;
  }
  if (bread == sizeof(line))
  {
    fprintf(stderr, "Warning: Serial input line truncated.\n");
  }
  if (!data_mode)
  {
    line[bread] = '\0';
  }
  line_size = bread;
  return 1;
}

int32_t serial_readline(uint8_t *l)
{
  int32_t ret;

  if (fd == -1)
  {
    return -1;
  }
  ret = line_size;
  while (ret == 0) // while we read empty line, retry
  {
    if ((ret = read_line()) == -1)
    {
      return -1;
    }
  }
  memcpy(l, line, line_size + 1);
  line[0] = '\0';
  ret = line_size;
  line_size = 0;
  return ret;
}

serial_status_t serial_set_canon(bool disable)
{
  struct termios options;
  if (tcgetattr(fd, &options) == -1)
  {
    return SERIAL_INTERN_ERR;
  }
  if (disable)
  {
    // Remove canonical mode and sets a timer of 0.5 seconds before read() times out as we enter data mode
    options.c_lflag &= ~ICANON;
    options.c_cc[VMIN] = 0;
    options.c_cc[VTIME] = 5;
    data_mode = true;
  }
  else
  {
    data_mode = false;
    options.c_lflag |= ICANON;
  }
  if (tcsetattr(fd, TCSANOW, &options) == -1)
  {
    return SERIAL_INTERN_ERR;
  }
  if (tcflush(fd, TCIOFLUSH) == -1)
  {
    return SERIAL_INTERN_ERR;
  }
  return SERIAL_SUCCESS;
}

int32_t serial_writedata(const uint8_t *data, uint16_t data_size)
{
  ssize_t ret;

  if (fd == -1 || !data)
  {
    return -1;
  }
  do
  {
    ret = write(fd, data, data_size);
  } while (ret == -1 && errno == EINTR);
  if (tcsendbreak(fd, 0) == -1)
  {
    fprintf(stderr, "Warning: tcsendbreak() on serial failed\n");
  }
  if (tcdrain(fd) == -1)
  {
    fprintf(stderr, "Warning: tcdrain() on serial failed\n");
  }
  return ret;
}

int32_t serial_writeline(const uint8_t *l)
{
  return serial_writedata(l, strlen(l));
}

serial_status_t serial_poll(bool block)
{
  if (fd == -1)
  {
    return SERIAL_NOT_INIT_ERR;
  }
  if (block && !line[0] && read_line() == -1)
  {
    return SERIAL_INTERN_ERR; // read_line() failed
  }
  if (line[0])
  {
    return SERIAL_LINE_AVAIL; // line is available
  }
  return SERIAL_SUCCESS; // nothing to read
}
