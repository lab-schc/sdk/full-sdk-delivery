/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "fullsdkl2.h"
#include "platform.h"
#include "serial.h"

#define APN_IP "IP"
#define APN_NIDD "Non-IP"

#define IPV4_HEADER_SIZE 20
#define UDP_HEADER_SIZE 8

#define MAX_IP_STR_LEN 46

#define MAX_CMD_SIZE (64 + MAX_IP_STR_LEN)
#define IO_BUFF_SIZE (MTU * 2 + MAX_CMD_SIZE)

#define TIME_STR_FORMAT "%T" // Gives the following format: HH:mm:ss (no subsecond precision)
#define TIME_STR_MAX_SIZE sizeof("HH:mm:ss")
char time_str[TIME_STR_MAX_SIZE] = {0};
#define GET_TIME_STR() time_as_formatted_string(time_str, TIME_STR_MAX_SIZE, TIME_STR_FORMAT)

#define CON_STAT_URC "+KCNX_IND:"
#define UDP_STAT_URC "+KUDP_IND:"
#define NET_STAT_URC "+CEREG:"
#define UDP_DATA_AVAIL_URC "+KUDP_DATA:"
#define UDP_NOTIF_URC "+KUDP_NOTIF:"
#define NIDD_DATA_AVAIL_URC "+CRTDCP:"

#define CMD_SET_QUEUE_SIZE 10

#define DEFAULT_CMD_TIMEOUT 20

static char io_buff[IO_BUFF_SIZE] = {0};

static char serial_port[100] = SERIAL_PATH;
static char edrx_param[5] = "0010"; // More info here: https://infocenter.nordicsemi.com/index.jsp?topic=%2Fref_at_commands%2FREF%2Fat_commands%2Fnw_service%2Fcedrxs.html
static char apn[64] = "m2m.public.nl";
static char apn_type[64] = APN_IP;
static char remote_ip[64] = "";
static int remote_port = -1;
static char remote_port_str[10] = "";
static bool nidd = false;
static bool traces = false;

// Function used to signal events to L2A
void l2_event_happened(l2_status_t status);

typedef struct
{
  char *cmd;
  const char **expected;
  int nb_try;
  int timeout;
} expect_resp_params_t;
typedef enum
{
  DISCONNECTED,
  CONNECTING,
  CONNECTED,
} connection_state_t;

typedef enum
{
  IDLE,
  WAITING_FOR_RESPONSE,
  RESPONSE_ARRIVED,
} at_cmd_state_t;

typedef struct
{
  const char *cmd;
  const char **cmd_params;
  int params_nb;
  const char **expected_responses;
  int expected_responses_nb;
  bool failure_is_critical;
  int timeout;
  bool wait_for_ok_err;
  l2_status_t (*process_cmd_resp)(const char *, int resp_idx);
  void (*timeout_cmd)();
} at_cmd_t;

static at_cmd_t *cmd_set_queue[CMD_SET_QUEUE_SIZE] = {};
static int cmd_set_queue_idx = 0;
static int last_queue_insertion_idx = -1;
static int queue_used = 0;
static at_cmd_t *current_cmd_set = NULL;
static int current_cmd_idx = 0;
static at_cmd_t *current_cmd = NULL;
static at_cmd_state_t at_state = IDLE;
static connection_state_t lte_connection_state = DISCONNECTED;
static connection_state_t ip_connection_state = CONNECTED; // The URC doesnt always appear so we take the asumption it's already connected
static bool pdp_context_activated = false;
static bool ps_attached = false;

static bool wait_for_ok_err = false;
static bool found = false;
static bool failure = false;

static bool init_done = false;

static int pdp_cont_idx = 1;
static char pdp_cont_idx_str[2] = "1";
static int pdp_conf_status = 0;

static int udp_session_idx = -1;
static char udp_session_idx_str[2] = "";
static int udp_session_status = 0;

timer_obj_t l2_timer;

static const char *kcnxcfg_params[2];
static const char *kudpcfg_params[1];
static const char *kudpcfg_responses[] = {"+KUDPCFG:", "ERROR"};
static const char *cereg_responses[] = {"+CEREG:", "ERROR"};
static bool waiting_cereg_resp = false;

static const char *udp_send_params[4];
static const char *udp_send_responses[] = {"CONNECT", "ERROR"};

static const char *nidd_send_params[3];

static int tx_data_size = 0;
static char tx_data_size_str[10];
static char tx_buf[2 * MTU] = {0};

static const char *cgatt_responses[] = {"+CGATT:", "ERROR"};
static const char *cgact_responses[] = {"+CGACT:", "ERROR"};

static const char *cgdcont_params[3];

static const char *udp_rcv_params[2];
static int rcv_data_size;
static char rcv_data_size_str[10];
static const char *udp_rcv_responses[] = {"CONNECT", "ERROR"};

static const char *udp_close_params[1];

static const char *nidd_send_responses[] = {"OK", "ERROR"};

static l2_status_t process_kudpcfg(const char *response, int resp_idx);
static l2_status_t process_cereg(const char *response, int resp_idx);
static l2_status_t process_udp_send(const char *response, int resp_idx);
static l2_status_t process_nidd_send(const char *response, int resp_idx);
static l2_status_t process_cgatt_read(const char *response, int resp_idx);
static l2_status_t process_cgact(const char *response, int resp_idx);
static l2_status_t process_enable_psm(const char *response, int resp_idx);
static l2_status_t process_nidd_init_done(const char *response, int resp_idx);

static void process_send_timeout(void);

at_cmd_t board_init_cmds[] =
    {
        {
            .cmd = "AT",
            .cmd_params = NULL,
            .params_nb = 0,
            .expected_responses = NULL,
            .expected_responses_nb = 0,
            .failure_is_critical = true,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = NULL,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = "ATE0",
            .cmd_params = NULL,
            .params_nb = 0,
            .expected_responses = NULL,
            .expected_responses_nb = 0,
            .failure_is_critical = false,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = NULL,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = "AT+CGDCONT=%s,\"%s\",\"%s\"",
            .cmd_params = cgdcont_params,
            .params_nb = 3,
            .expected_responses = NULL,
            .expected_responses_nb = 0,
            .failure_is_critical = true,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = NULL,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = "AT+CGACT?",
            .cmd_params = NULL,
            .params_nb = 0,
            .expected_responses = cgact_responses,
            .expected_responses_nb = 2,
            .failure_is_critical = false,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = process_cgact,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = "AT+KUDPCLOSE=1",
            .cmd_params = NULL,
            .params_nb = 0,
            .expected_responses = NULL,
            .expected_responses_nb = 0,
            .failure_is_critical = false,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = NULL,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = "AT+CEREG=5",
            .cmd_params = NULL,
            .params_nb = 0,
            .expected_responses = NULL,
            .expected_responses_nb = 0,
            .failure_is_critical = false,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = NULL,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = "AT+CEREG?",
            .cmd_params = NULL,
            .params_nb = 0,
            .expected_responses = (const char **)cereg_responses,
            .expected_responses_nb = 2,
            .failure_is_critical = false,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = process_cereg,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = NULL,
        }};

at_cmd_t psm_init_cmds[] = {
    {
        .cmd = "AT+CPSMS=1",
        .cmd_params = cgdcont_params,
        .params_nb = 0,
        .expected_responses = NULL,
        .expected_responses_nb = 0,
        .failure_is_critical = false,
        .timeout = DEFAULT_CMD_TIMEOUT,
        .process_cmd_resp = process_enable_psm,
        .wait_for_ok_err = true,
        .timeout_cmd = NULL,
    },
    {.cmd = NULL}};

at_cmd_t nidd_init_cmds[] =
    {
        {
            .cmd = "AT+CRTDCP=1",
            .cmd_params = NULL,
            .params_nb = 0,
            .expected_responses = NULL,
            .expected_responses_nb = 0,
            .failure_is_critical = true,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = process_nidd_init_done,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = NULL,
        }};

at_cmd_t net_attach_cmds[] =
    {
        {
            .cmd = "AT+CGATT=1",
            .cmd_params = NULL,
            .params_nb = 0,
            .expected_responses = NULL,
            .expected_responses_nb = 0,
            .failure_is_critical = true,
            .timeout = DEFAULT_CMD_TIMEOUT * 2, // This command can take a while to respond
            .process_cmd_resp = NULL,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = NULL,
        }};

at_cmd_t udp_init_cmds[] =
    {
        {
            .cmd = "AT+KCNXCFG=%s,\"GPRS\",\"%s\"",
            .cmd_params = kcnxcfg_params,
            .params_nb = 2,
            .expected_responses = NULL,
            .expected_responses_nb = 0,
            .failure_is_critical = true,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = NULL,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = "AT+KUDPCFG=%s,0,,,,,,1",
            .cmd_params = (const char **)&kudpcfg_params,
            .params_nb = 1,
            .expected_responses = (const char **)kudpcfg_responses,
            .expected_responses_nb = 2,
            .failure_is_critical = true,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = process_kudpcfg,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = NULL,
        }};

at_cmd_t udp_send_cmds[] =
    {
        {
            .cmd = "AT+KUDPSND=%s,\"%s\",%s,%s",
            .cmd_params = udp_send_params,
            .params_nb = 4,
            .expected_responses = (const char **)&udp_send_responses,
            .expected_responses_nb = 2,
            .failure_is_critical = false,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = process_udp_send,
            .wait_for_ok_err = false,
            .timeout_cmd = process_send_timeout,
        },
        {.cmd = NULL}};

at_cmd_t udp_rcv_cmds[] =
    {
        {
            .cmd = "AT+KUDPRCV=%s,%s",
            .cmd_params = udp_rcv_params,
            .params_nb = 2,
            .expected_responses = (const char **)&udp_rcv_responses,
            .expected_responses_nb = 2,
            .failure_is_critical = false,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = NULL,
            .wait_for_ok_err = false,
            .timeout_cmd = NULL,
        },
        {
            .cmd = NULL,
        }};

at_cmd_t udp_close_cmds[] =
    {
        {
            .cmd = "AT+KUDPCLOSE=%s",
            .cmd_params = udp_close_params,
            .params_nb = 1,
            .expected_responses = NULL,
            .expected_responses_nb = 0,
            .failure_is_critical = false,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = NULL,
            .wait_for_ok_err = true,
            .timeout_cmd = NULL,
        },
        {
            .cmd = NULL,
        }};

at_cmd_t nidd_send_cmds[] =
    {
        {
            .cmd = "AT+CSODCP=%s,%s,\"%s\",0,0",
            .cmd_params = nidd_send_params,
            .params_nb = 3,
            .expected_responses = nidd_send_responses,
            .expected_responses_nb = 2,
            .failure_is_critical = false,
            .timeout = DEFAULT_CMD_TIMEOUT,
            .process_cmd_resp = process_nidd_send,
            .wait_for_ok_err = false,
            .timeout_cmd = process_send_timeout,
        },
        {
            .cmd = NULL,
        }};

static void hl78_print_packet(const char *data, uint16_t data_size)
{
  printf("size: %d\n", data_size);
  for (uint16_t i = 0; i < data_size; i++)
  {
    printf("%.2X ", (uint8_t)data[i]);
  }
  printf("\n");
}

static l2_status_t hl78_write(const char *str)
{
  sprintf((char *)io_buff, "%s\r\n", str);
  if (traces)
  {
    GET_TIME_STR();
    printf("%s COMMAND: %s\n", time_str, str);
  }
  if (serial_writeline(io_buff) != (ssize_t)strlen((const char *)io_buff))
  {
    return L2_ERROR;
  }
  return L2_SUCCESS;
}

static l2_status_t hl78_queue_cmd_set(at_cmd_t *set)
{
  if (queue_used == CMD_SET_QUEUE_SIZE)
  {
    // No more space in the queue
    return L2_ERROR;
  }
  last_queue_insertion_idx = (last_queue_insertion_idx + 1) % CMD_SET_QUEUE_SIZE;
  cmd_set_queue[last_queue_insertion_idx] = set;
  queue_used++;

  if (current_cmd_set == NULL || current_cmd_set[current_cmd_idx].cmd == NULL)
  {
    current_cmd_set = cmd_set_queue[cmd_set_queue_idx];
    current_cmd_idx = 0;
    cmd_set_queue_idx = (cmd_set_queue_idx + 1) % CMD_SET_QUEUE_SIZE;
    queue_used--;
    l2_event_happened((l2_status_t)L2_CMD_RDY_TO_RUN);
  }
  return L2_SUCCESS;
}

static bool is_connected()
{
  return lte_connection_state == CONNECTED && ((!nidd && udp_session_status == 1 && ip_connection_state == CONNECTED) || nidd) && /*ps_attached &&*/ pdp_context_activated && init_done;
}

l2_status_t hl78_conf_cmd(char *cmd_buff, at_cmd_t *cmd)
{
  switch (cmd->params_nb)
  {
  case 0:
    memcpy(cmd_buff, cmd->cmd, strlen(cmd->cmd) + 1);
    break;
  case 1:
    sprintf(cmd_buff, cmd->cmd, cmd->cmd_params[0]);
    break;
  case 2:
    sprintf(cmd_buff, cmd->cmd, cmd->cmd_params[0], cmd->cmd_params[1]);
    break;
  case 3:
    sprintf(cmd_buff, cmd->cmd, cmd->cmd_params[0], cmd->cmd_params[1], cmd->cmd_params[2]);
    break;
  case 4:
    sprintf(cmd_buff, cmd->cmd, cmd->cmd_params[0], cmd->cmd_params[1], cmd->cmd_params[2], cmd->cmd_params[3]);
    break;
  default:
    return L2_ERROR;
  }
  return L2_SUCCESS;
}

l2_status_t hl78_run_cmd()
{
  l2_status_t status;
  if (at_state != IDLE)
  {
    return L2_ERROR;
  }
  current_cmd = &current_cmd_set[current_cmd_idx];
  wait_for_ok_err = current_cmd->wait_for_ok_err;
  // If the command expects no specific response we can consider it as already found
  found = current_cmd->expected_responses_nb == 0 ? true : false;
  failure = false;
  if ((status = hl78_conf_cmd(io_buff, current_cmd)) != L2_SUCCESS)
  {
    return status;
  }
  if ((status = hl78_write(io_buff)) != L2_SUCCESS)
  {
    return status;
  }
  if (current_cmd_set[current_cmd_idx].expected_responses_nb == 0 && !wait_for_ok_err)
  {
    if (current_cmd_set[current_cmd_idx].process_cmd_resp != NULL)
    {
      current_cmd_set[current_cmd_idx].process_cmd_resp(NULL, -1);
    }
    at_state = IDLE;
    l2_event_happened((l2_status_t)L2_CMD_RDY_TO_RUN);
  }
  else
  {
    platform_timer_set_duration(&l2_timer, current_cmd->timeout * 1000);
    platform_timer_start(&l2_timer);
    at_state = WAITING_FOR_RESPONSE;
  }
  return L2_SUCCESS;
}

#define RCV_OVERHEAD_SIZE (sizeof("\r\n\r\nCONNECT\r\n") - 1 + sizeof("--EOF--Pattern--\r\n") - 1)
#define UDP_DATA_START_NEEDLE "CONNECT\r\n"

l2_status_t hl78_udp_receive(char *data, uint16_t *size)
{
  sprintf(io_buff, "AT+KUDPRCV=%d,%d", udp_session_idx, rcv_data_size);
  hl78_write(io_buff);

  int32_t ret;
  serial_set_canon(false);
  if ((ret = serial_readdata(io_buff, rcv_data_size + RCV_OVERHEAD_SIZE)) == -1)
  {
    return L2_ERROR;
  }
  serial_set_canon(true);
  char *udp_data = strstr(io_buff, UDP_DATA_START_NEEDLE);
  if (udp_data == NULL)
  {
    return L2_ERROR;
  }
  udp_data = udp_data + sizeof(UDP_DATA_START_NEEDLE) - 1;
  memcpy(data, udp_data, rcv_data_size);
  *size = rcv_data_size;

  return L2_SUCCESS;
}

static bool hex_to_bin(char *bin_buff, const char *hex, uint16_t size)
{
  for (uint16_t i = 0; i < size; ++i)
  {
    char str_byte[3] = {0};
    char *err = NULL;
    memcpy(str_byte, hex + 2 * i, 2);
    if (!isxdigit(str_byte[0]) || !isxdigit(str_byte[1]))
    {
      fprintf(stderr, "Error: string not HEX %c%c\n", str_byte[0], str_byte[1]);
      return false;
    }
    bin_buff[i] = (uint8_t)strtol(str_byte, &err, 16);
    if (*err)
    {
      fprintf(stderr, "Error: strtol() failed: %s\n", strerror(errno));
      return false;
    }
  }
  return true;
}

l2_status_t hl78_nidd_receive(char *data, uint16_t *size)
{
  int offset = sizeof(NIDD_DATA_AVAIL_URC " X,") - 1;
  *size = rcv_data_size;
  for (; io_buff[offset] != '\"'; offset++)
    ;
  offset++;
  return hex_to_bin(data, (char *)(io_buff + offset), (uint16_t)(rcv_data_size)) ? L2_SUCCESS : L2_ERROR;
}

l2_status_t hl78_receive(char *data, uint16_t max_size, uint16_t *size)
{
  if (rcv_data_size > max_size)
  {
    return L2_ERROR;
  }
  l2_status_t ret = nidd ? hl78_nidd_receive(data, size) : hl78_udp_receive(data, size);
  if (traces)
  {
    GET_TIME_STR();
    printf("%s DOWNLINK :", time_str);
    hl78_print_packet(data, rcv_data_size);
  }
  return ret;
}

void hl78_fini(void)
{
  serial_fini();
}

static void hl78_forward_cmd()
{
  current_cmd_idx++;
  if (current_cmd_set[current_cmd_idx].cmd == NULL && queue_used != 0)
  {
    current_cmd_set = cmd_set_queue[cmd_set_queue_idx];
    current_cmd_idx = 0;
    cmd_set_queue_idx = (cmd_set_queue_idx + 1) % CMD_SET_QUEUE_SIZE;
    queue_used--;
    l2_event_happened((l2_status_t)L2_CMD_RDY_TO_RUN);
  }
  else if (current_cmd_set[current_cmd_idx].cmd != NULL)
  {
    l2_event_happened((l2_status_t)L2_CMD_RDY_TO_RUN);
  }
}

void l2_timer_timeout_func(void *ctx)
{
  (void)ctx;
  printf("L2 timed out on command : %s after %ds\n", current_cmd->cmd, current_cmd->timeout);
  at_cmd_state_t _at_state = at_state;
  at_state = IDLE;
  if (current_cmd->timeout_cmd != NULL)
  {
    current_cmd->timeout_cmd();
  }
  if (_at_state == WAITING_FOR_RESPONSE && current_cmd->failure_is_critical)
  {
    l2_event_happened((l2_status_t)L2_TIMER_TIMEOUT);
  }
  else if (_at_state == WAITING_FOR_RESPONSE)
  {
    hl78_forward_cmd();
  }
}

l2_status_t hl78_process_at()
{
  if (at_state != RESPONSE_ARRIVED || current_cmd == NULL)
  {
    return L2_INTERN_ERR;
  }

  if (wait_for_ok_err)
  {
    if (strstr(io_buff, "OK"))
    {
      wait_for_ok_err = false;
    }
    if (strstr(io_buff, "ERROR"))
    {
      wait_for_ok_err = false;
      failure = true;
    }
  }

  if (!wait_for_ok_err && current_cmd_set[current_cmd_idx].expected_responses_nb == 0
      && current_cmd_set[current_cmd_idx].process_cmd_resp != NULL)
  {
    if (current_cmd_set[current_cmd_idx].process_cmd_resp(NULL, 0) != L2_SUCCESS)
    {
      failure = true;
    }
  }

  for (int i = 0; i < current_cmd_set[current_cmd_idx].expected_responses_nb; i++)
  {
    if (strstr(io_buff, current_cmd_set[current_cmd_idx].expected_responses[i]))
    {
      found = true;
      if (current_cmd_set[current_cmd_idx].process_cmd_resp != NULL)
      {
        if (current_cmd_set[current_cmd_idx].process_cmd_resp(io_buff, i) != L2_SUCCESS)
        {
          failure = true;
        }
      }
    }
  }
  if (found && at_state != WAITING_FOR_RESPONSE && !wait_for_ok_err)
  {
    found = false;
    platform_timer_stop(&l2_timer);
    at_state = IDLE;
    if (failure && current_cmd_set[current_cmd_idx].failure_is_critical)
    {
      failure = false;
      return L2_INTERN_ERR;
    }
    hl78_forward_cmd();
  }
  else
  {
    at_state = WAITING_FOR_RESPONSE;
  }
  return L2_SUCCESS;
}

l2_status_t hl78_init(void)
{
  if (serial_init(serial_port, HL78_BAUDRATE) != SERIAL_SUCCESS)
  {
    return L2_ERROR;
  }
  platform_timer_add(&l2_timer, 0, l2_timer_timeout_func, NULL);

  cgdcont_params[0] = pdp_cont_idx_str;
  cgdcont_params[1] = apn_type;
  cgdcont_params[2] = apn;

  kcnxcfg_params[0] = pdp_cont_idx_str;
  kcnxcfg_params[1] = apn;
  kudpcfg_params[0] = pdp_cont_idx_str;

  l2_status_t stat = hl78_queue_cmd_set(board_init_cmds);
  if (nidd && stat == L2_SUCCESS)
  {
    stat = hl78_queue_cmd_set(nidd_init_cmds);
  }
  return stat;
}

l2_status_t hl78_udp_send_data(const char *data, uint16_t data_size)
{
  udp_send_params[0] = udp_session_idx_str;
  udp_send_params[1] = remote_ip;
  udp_send_params[2] = remote_port_str;
  sprintf(tx_data_size_str, "%d", data_size);
  udp_send_params[3] = tx_data_size_str;
  tx_data_size = data_size;
  memcpy(tx_buf, data, data_size);

  return hl78_queue_cmd_set(udp_send_cmds);
}

l2_status_t hl78_nidd_send_data(const char *data, uint16_t data_size)
{
  nidd_send_params[0] = pdp_cont_idx_str;
  sprintf(tx_data_size_str, "%d", data_size);
  nidd_send_params[1] = tx_data_size_str;
  tx_data_size = data_size;
  for (size_t i = 0; i < data_size; i++)
  {
    sprintf(tx_buf + i * 2, "%.2X", (uint8_t)data[i]);
  }
  tx_buf[data_size * 2] = '\0';
  nidd_send_params[2] = tx_buf;

  return hl78_queue_cmd_set(nidd_send_cmds);
}

l2_status_t hl78_send_data(const char *data, uint16_t data_size)
{
  if (data == NULL)
  {
    return L2_ERROR;
  }
  if (traces)
  {
    GET_TIME_STR();
    printf("%s UPLINK : ", time_str);
    hl78_print_packet(data, data_size);
  }
  return nidd ? hl78_nidd_send_data(data, data_size) : hl78_udp_send_data(data, data_size);
}

// Check for Unsolicited Result Codes that inform us of something happening in the hl78
static bool check_urc(const char *buff)
{
  if (strstr(buff, NET_STAT_URC))
  {
    int status, n;
    if (sscanf(buff, NET_STAT_URC " %d,%d", &n, &status) == 2)
    {
      // This is the response to the AT+CEREG? function
      return false;
    }
    sscanf(buff, NET_STAT_URC " %d", &status);
    switch (status)
    {
    case 2:
      lte_connection_state = CONNECTING;
      break;
    case 1:
    case 5:
      pdp_context_activated = true;
      ps_attached = true;
      if (lte_connection_state != CONNECTED)
      {
        lte_connection_state = CONNECTED;
        if (is_connected())
        {
          l2_event_happened(L2_CONN_AVAIL);
        }
        else if (!nidd && udp_session_idx == -1)
        {
          if (hl78_queue_cmd_set(udp_init_cmds) != L2_SUCCESS)
          {
            l2_event_happened(L2_ERROR);
            return false;
          }
        }
      }
      break;
    case 4:
      // This case usually means that we're in sleep mode
      break;
    default:
      if (lte_connection_state == CONNECTED)
      {
        l2_event_happened(L2_CONN_LOST);
      }
      lte_connection_state = DISCONNECTED;
      break;
    }
  }
  else if (!nidd && strstr(buff, CON_STAT_URC))
  {
    int pdp_idx;
    int status;
    if (sscanf(buff, CON_STAT_URC " %d,%d", &pdp_idx, &status) != 2)
    {
      return false;
    }
    if (pdp_idx == pdp_cont_idx)
    {
      pdp_conf_status = status;
      switch (status)
      {
      case 0:
      case 2:
      case 3:
        ip_connection_state = DISCONNECTED;
        break;
      case 1:
        if (ip_connection_state != CONNECTED)
        {
          ip_connection_state = CONNECTED;
          if (is_connected())
          {
            l2_event_happened(L2_CONN_AVAIL);
          }
          else if (udp_session_idx == -1)
          {
            if (hl78_queue_cmd_set(udp_init_cmds) != L2_SUCCESS)
            {
              l2_event_happened(L2_ERROR);
              return false;
            }
          }
        }
        break;
      case 4:
        ip_connection_state = CONNECTING;
        break;
      default:
        break;
      }
    }
  }
  else if (!nidd && strstr(buff, UDP_STAT_URC))
  {
    int udp_idx;
    int status;
    if (sscanf(buff, UDP_STAT_URC " %d,%d", &udp_idx, &status) != 2)
    {
      return false;
    }
    if (udp_idx == udp_session_idx)
    {
      if (udp_session_status != status)
      {
        udp_session_status = status;
        if (is_connected())
        {
          l2_event_happened(L2_CONN_AVAIL);
        }
      }
    }
  }
  else if (!nidd && strstr(buff, UDP_DATA_AVAIL_URC))
  {
    int udp_idx;
    int ndata;
    if (sscanf(buff, UDP_DATA_AVAIL_URC " %d, %d", &udp_idx, &ndata) != 2)
    {
      return false;
    }
    if (udp_idx == udp_session_idx)
    {
      rcv_data_size = ndata;
      sprintf(rcv_data_size_str, "%d", rcv_data_size);
      l2_event_happened(L2_DOWN_AVAIL);
    }
  }
  else if (!nidd && strstr(buff, UDP_NOTIF_URC))
  {
    int session;
    int notif;

    if (sscanf(buff, UDP_NOTIF_URC " %d,%d", &session, &notif) != 2)
    {
      return false;
    }
    if (session != udp_session_idx)
    {
      return false;
    }
    switch (notif)
    {
    case 8:
      return false;
    case 6:
      if (udp_session_idx != -1 && ip_connection_state != CONNECTED)
      {
        udp_close_params[0] = udp_session_idx_str;
        udp_session_idx = -1;
        if (hl78_queue_cmd_set(udp_close_cmds) != L2_SUCCESS)
        {
          l2_event_happened(L2_ERROR);
          return false;
        }
      }
      break;
    default:
      l2_event_happened(L2_INTERN_ERR);
    }
  }
  else if (nidd && strstr(buff, NIDD_DATA_AVAIL_URC))
  {
    int pdp_idx;

    if (sscanf(buff, NIDD_DATA_AVAIL_URC " %d,%d", &pdp_idx, &rcv_data_size) != 2)
    {
      return false;
    }
    if (pdp_idx != pdp_cont_idx)
    {
      return false;
    }
    l2_event_happened(L2_DOWN_AVAIL);
  }
  else
  {
    return false;
  }
  // Return true in case a URC has been found
  return true;
}

l2_status_t hl78_poll_downlink()
{
  serial_readline(io_buff);
  if (strcmp((char *)io_buff, "\r\n") == 0 || strcmp((char *)io_buff, "\n") == 0)
  {
    return L2_SUCCESS;
  }
  if (traces)
  {
    GET_TIME_STR();
    printf("%s RESPONSE: %s\n", time_str, io_buff);
  }
  if (!check_urc(io_buff) && at_state == WAITING_FOR_RESPONSE)
  {
    at_state = RESPONSE_ARRIVED;
    l2_event_happened((l2_status_t)L2_CMD_RESP_AVAIL);
  }
  return L2_SUCCESS;
}

static l2_status_t process_cereg(const char *response, int resp_idx)
{
  if (resp_idx != 0)
  {
    return L2_ERROR;
  }
  int mode, status;
  waiting_cereg_resp = false;
  if (sscanf(response, "+CEREG: %d,%d", &mode, &status) != 2)
  {
    return L2_ERROR;
  }
  if (status == 5 || status == 1 || status == 4)
  {
    pdp_context_activated = true;
    ps_attached = true;
    if (lte_connection_state != CONNECTED)
    {
      lte_connection_state = CONNECTED;
      if (is_connected())
      {
        l2_event_happened(L2_CONN_AVAIL);
      }
      else if (!nidd && udp_session_idx == -1)
      {
        hl78_queue_cmd_set(udp_init_cmds);
      }
    }
  }
  return L2_SUCCESS;
}

static l2_status_t process_kudpcfg(const char *response, int resp_idx)
{
  if (resp_idx != 0 || sscanf(response, "+KUDPCFG: %d", &udp_session_idx) != 1)
  {
    return L2_ERROR;
  }
  sprintf(udp_session_idx_str, "%d", udp_session_idx);
  init_done = true;
  if (is_connected())
  {
    l2_event_happened(L2_CONN_AVAIL);
  }

  return L2_SUCCESS;
}

static l2_status_t process_udp_send(const char *response, int resp_idx)
{
  (void)response;
  if (resp_idx != 0)
  {
    l2_event_happened(L2_TX_FAIL);
    return L2_ERROR;
  }
  serial_writedata(tx_buf, tx_data_size);
  usleep(500000);
  serial_writedata("+++", sizeof("+++") - 1);
  usleep(500000);
  l2_event_happened(L2_TX_DONE);
  return L2_SUCCESS;
}

static l2_status_t process_nidd_send(const char *response, int resp_idx)
{
  (void)response;
  if (resp_idx != 0)
  {
    l2_event_happened(L2_TX_FAIL);
    return L2_ERROR;
  }
  l2_event_happened(L2_TX_DONE);
  return L2_SUCCESS;
}

static l2_status_t process_cgact(const char *response, int resp_idx)
{
  if (resp_idx != 0)
  {
    return L2_ERROR;
  }
  int pdp_idx;
  int pdp_status;
  sscanf(response, "+CGACT: %d,%d", &pdp_idx, &pdp_status);
  if (pdp_idx < pdp_cont_idx)
  {
    at_state = WAITING_FOR_RESPONSE;
    return L2_SUCCESS;
  }
  if (pdp_idx > pdp_cont_idx)
  {
    return L2_ERROR;
  }
  pdp_context_activated = pdp_status == 1 ? true : false;
  return L2_SUCCESS;
}

static l2_status_t process_cgatt_read(const char *response, int resp_idx)
{
  if (resp_idx != 0)
  {
    return L2_ERROR;
  }
  int cgatt_status;
  sscanf(response, "+CGATT: %d", &cgatt_status);
  ps_attached = cgatt_status == 1 ? true : false;
  if (!nidd && !ps_attached)
  {
    hl78_queue_cmd_set(net_attach_cmds);
  }
  return L2_SUCCESS;
}

static l2_status_t process_enable_psm(const char *response, int resp_idx)
{
  (void)response;
  (void)resp_idx;
  int sl = 10;
  for (size_t i = 0; sl != 0; i++)
  {
    sl = sleep(sl);
  }
  return L2_SUCCESS;
}

static l2_status_t process_nidd_init_done(const char *response, int resp_idx)
{
  (void)response;
  (void)resp_idx;
  init_done = true;
  if (is_connected())
  {
    l2_event_happened(L2_CONN_AVAIL);
  }
  return L2_SUCCESS;
}

static void process_send_timeout(void)
{
  l2_event_happened(L2_TX_FAIL);
}

void l2_set_edrx_param(const char *edrx)
{
  memcpy(edrx_param, edrx, strlen(edrx) + 1);
}

void l2_set_apn(const char *new_apn)
{
  memcpy(apn, new_apn, strlen(new_apn) + 1);
}

void l2_set_remote_ip(const char *new_remote_ip)
{
  memcpy(remote_ip, new_remote_ip, strlen(new_remote_ip) + 1);
}

void l2_set_remote_port(int new_remote_port)
{
  remote_port = new_remote_port;
  sprintf(remote_port_str, "%d", remote_port);
}

void l2_set_nidd(bool enable)
{
  nidd = enable;
  if (nidd)
  {
    memcpy(apn_type, APN_NIDD, sizeof(APN_NIDD));
  }
  else
  {
    memcpy(apn_type, APN_IP, sizeof(APN_IP));
  }
}

void l2_set_traces(bool enable)
{
  traces = enable;
}

void l2_set_pdp_context_index(int idx)
{
  if (idx < 0 || idx > 9)
  {
    return;
  }
  pdp_cont_idx = idx;
  pdp_cont_idx_str[0] = '0' + idx;
}

bool l2_set_serial_port(const char *port_path)
{
  if (port_path == NULL)
  {
    return false;
  }
  printf("setting port to %s\n\n", port_path);
  size_t port_path_size = strlen(port_path);
  if (port_path_size > sizeof(serial_port))
  {
    return false;
  }
  memcpy(serial_port, port_path, port_path_size);
  return true;
}

void l2_enable_psm(void)
{
  hl78_queue_cmd_set(psm_init_cmds);
}