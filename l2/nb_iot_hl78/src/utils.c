/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <netinet/ip.h>
#include <netinet/udp.h>
#include <stdbool.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/ioctl.h>

static uint16_t checksum(uint16_t *addr, uint32_t len)
{

  uint32_t count = len;
  register uint32_t sum = 0;
  uint16_t answer = 0;

  // Sum up 2-byte values until none or only one byte left.
  while (count > 1)
  {
    sum += *(addr++);
    count -= 2;
  }

  // Add left-over byte, if any.
  if (count > 0)
  {
    sum += *(uint8_t *)addr;
  }

  // Fold 32-bit sum into 16 bits; we lose information by doing this,
  // increasing the chances of a collision.
  // sum = (lower 16 bits) + (upper 16 bits shifted right 16 bits)
  while (sum >> 16)
  {
    sum = (sum & 0xffff) + (sum >> 16);
  }

  // Checksum is one's compliment of sum.
  answer = ~sum;

  return answer;
}

void compute_ip_checksum(struct iphdr *iphdrp)
{
  iphdrp->check = 0;
  iphdrp->check = checksum((uint16_t *)iphdrp, iphdrp->ihl << 2);
}

// Build IPv6 UDP pseudo-header and call checksum function (Section 8.1 of RFC 2460).
static uint16_t udp4_checksum(const struct iphdr *iphdr, const struct udphdr *udphdr, const uint8_t *payload, uint16_t payloadlen)
{

  uint8_t buf[IP_MAXPACKET];
  uint8_t *ptr;
  uint32_t chksumlen = 0;
  uint32_t i;

  ptr = &buf[0]; // ptr points to beginning of buffer buf

  // Copy source IP address into buf (32 bits)
  memcpy(ptr, &iphdr->saddr, sizeof(iphdr->saddr));
  ptr += sizeof(iphdr->saddr);
  chksumlen += sizeof(iphdr->saddr);

  // Copy destination IP address into buf (32 bits)
  memcpy(ptr, &iphdr->daddr, sizeof(iphdr->daddr));
  ptr += sizeof(iphdr->daddr);
  chksumlen += sizeof(iphdr->daddr);

  // Copy UDP length into buf (32 bits)
  memcpy(ptr, &udphdr->len, sizeof(udphdr->len));
  ptr += sizeof(udphdr->len);
  chksumlen += sizeof(udphdr->len);

  // Copy zero field to buf (24 bits)
  *ptr = 0;
  ptr++;
  *ptr = 0;
  ptr++;
  *ptr = 0;
  ptr++;
  chksumlen += 3;

  // Copy next header field to buf (8 bits)
  memcpy(ptr, &iphdr->protocol, sizeof(iphdr->protocol));
  ptr += sizeof(iphdr->protocol);
  chksumlen += sizeof(iphdr->protocol);

  // Copy UDP source port to buf (16 bits)
  memcpy(ptr, &udphdr->source, sizeof(udphdr->source));
  ptr += sizeof(udphdr->source);
  chksumlen += sizeof(udphdr->source);

  // Copy UDP destination port to buf (16 bits)
  memcpy(ptr, &udphdr->dest, sizeof(udphdr->dest));
  ptr += sizeof(udphdr->dest);
  chksumlen += sizeof(udphdr->dest);

  // Copy UDP length again to buf (16 bits)
  memcpy(ptr, &udphdr->len, sizeof(udphdr->len));
  ptr += sizeof(udphdr->len);
  chksumlen += sizeof(udphdr->len);

  // Copy UDP checksum to buf (16 bits)
  // Zero, since we don't know it yet
  *ptr = 0;
  ptr++;
  *ptr = 0;
  ptr++;
  chksumlen += 2;

  // Copy payload to buf
  memcpy(ptr, payload, payloadlen * sizeof(uint8_t));
  ptr += payloadlen;
  chksumlen += payloadlen;

  // Pad to the next 16-bit boundary
  for (i = 0; i < payloadlen % 2; i++, ptr++)
  {
    *ptr = 0;
    ptr++;
    chksumlen++;
  }

  return checksum((uint16_t *)buf, chksumlen);
}

bool generate_ipv4_udp_packet(const char *src_ip, uint16_t src_port, const char *dst_ip, uint16_t dst_port, const uint8_t *payload, uint16_t payload_size, uint8_t *packet_out, uint16_t *packet_out_size)
{
  struct iphdr ipv4_hdr;
  struct udphdr udp_hdr;

  ((uint8_t *)(&ipv4_hdr))[0] = 0x45;
  ipv4_hdr.tos = 0x00;
  // Payload length (16 bits): UDP header + UDP data
  ipv4_hdr.tot_len = htons(sizeof(struct iphdr) + sizeof(struct udphdr) + payload_size);
  ipv4_hdr.id = 0;
  ipv4_hdr.frag_off = 0x40;
  ipv4_hdr.ttl = 0xFF;
  // Next header (8 bits): 17 for UDP
  ipv4_hdr.protocol = IPPROTO_UDP;
  // Source IPv4 address (32 bits)
  if ((inet_pton(AF_INET, src_ip, &(ipv4_hdr.saddr))) != 1)
  {
    return false;
  }
  // Destination IPv4 address (32 bits)
  if ((inet_pton(AF_INET, dst_ip, &(ipv4_hdr.daddr))) != 1)
  {
    return false;
  }

  compute_ip_checksum(&ipv4_hdr);

  // UDP header

  // Source port number (16 bits): pick a number
  udp_hdr.source = htons(src_port);

  // Destination port number (16 bits): pick a number
  udp_hdr.dest = htons(dst_port);

  // Length of UDP datagram (16 bits): UDP header + UDP data
  udp_hdr.len = htons(sizeof(struct udphdr) + payload_size);

  //UDP checksum (16 bits)
  udp_hdr.check = udp4_checksum(&ipv4_hdr, &udp_hdr, payload, payload_size);
  if (!memcpy(packet_out, &ipv4_hdr, sizeof(struct iphdr)))
    return false;
  if (!memcpy(packet_out + sizeof(struct iphdr), &udp_hdr, sizeof(struct udphdr)))
    return false;
  if (!memcpy(packet_out + sizeof(struct iphdr) + sizeof(struct udphdr), payload, payload_size))
    return false;
  *packet_out_size = sizeof(struct iphdr) + sizeof(struct udphdr) + payload_size;
  return true;
}