/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis thibaut.artis@ackl.io
 */

#ifndef FULLSDKL2_H_
#define FULLSDKL2_H_

#include <stdbool.h>
#include <stdint.h>

#include "fullsdkl2a.h"
#include "fullsdkl2common.h"

#define NEXT_TX_DELAY 2000 // 2s arbitrary value
#define MTU 1500           // To match recommended ipv6 MTU

typedef enum
{
  L2_CMD_RESP_AVAIL = L2_NOT_SUPPORTED_ERR + 1,
  L2_CMD_RDY_TO_RUN,
  L2_TIMER_TIMEOUT,
} hl78_l2_status_t;

void l2_set_apn(const char *new_apn);
void l2_set_nidd(bool enable);
void l2_set_traces(bool enable);
void l2_set_remote_ip(const char *new_remote_ip);
void l2_set_remote_port(int new_remote_port);
bool l2_set_serial_port(const char *port_path);

l2_status_t hl78_init(void);
l2_status_t hl78_run_cmd(void);
void hl78_reset(void);
void hl78_fini(void);
l2_status_t hl78_receive(char *data, uint16_t max_size, uint16_t *size);
l2_status_t hl78_poll_downlink();
l2_status_t hl78_send_data(const char *p_data, uint16_t data_size);
l2_status_t hl78_restart(void);
l2_status_t hl78_process_at();
void l2_set_sleep_after_join(bool sleep);
void l2_enable_psm(void);

#endif
