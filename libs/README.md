# Third-party libraries

List of libraries that are currently used, in addition to the fullSDK libray in our example applications. We currently have:

* `wakaama`: LwM2M library
* `mbdetls`: DTLS library
