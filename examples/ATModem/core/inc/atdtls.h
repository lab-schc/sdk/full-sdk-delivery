/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Pascal Bodin pascal@ackl.io
 *
 * @brief Declarations for FullSDK DTLS handling layer.
 */

#include <stdbool.h>

#include "fullsdkdtls.h"

extern bool handshake_result_received;
extern dtls_status_t handshake_result;
extern bool handshake_requested;

extern bool dtls_processing_required;

bool handshake_done(void);

void reset_dtls_automaton(void);

void fullsdk_dtls_process(void);