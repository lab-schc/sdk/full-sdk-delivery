/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author  Flavien Moullec flavien@ackl.io
 *          Pascal Bodin pascal@ackl.io
 *
 * @brief AT helper functions
 */

#ifndef AT_HELPERS_H
#define AT_HELPERS_H

#include <stdbool.h>
#include <stdint.h>

#define IPV6_ADDRESS_LENGTH_BYTES 16
#define IPV6_ADDRESS_PREFIX_LENGTH_BYTES 8
#define IPV6_ADDRESS_IID_LENGTH_BYTES 8
#define IPV4_ADDRESS_LENGTH_BYTES 4
#define IP_ADDRESS_MAX_LENGTH_BYTES IPV6_ADDRESS_LENGTH_BYTES

void print_d(int value);

void print_u(unsigned int value);

void print_16_02x(uint8_t *pt);

void print_16_02x_2(uint8_t *pt);

int sscanf_uint32_as_hhx(const char *from, uint32_t *value);

void print_uint32_as_02x(uint32_t value);

void print_8_02x(uint8_t *pt);

/**
 * @brief Scans a string of format %hhx:...%hhx (16 times 1 to 2 hexa
 *        digits separated by colons) and extracts associated values.
 *
 * @param from - string
 * @param pt - array of 16 integers
 * @return int - 16 if scan succeeded, 0 otherwise
 */
int sscanf_16_hhx(const char *from, uint8_t *pt);

/**
 * @brief Scans a string of format %hhx:...%hhx (8 times 1 to 2 hexa
 *        digits separated by colons) and extracts associated values.
 *
 * @param from - string
 * @param pt - array of 8 integers
 * @return int - 8 if scan succeeded, 0 otherwise
 */
int sscanf_8_hhx(const char *from, uint8_t *pt);

/**
 * @brief Converts a string containing an hexadecimal integer
 *        to its value. Format can be x to xxxxxxxx. Returns true
 *        if conversion went OK, false otherwise.
 *
 * @param from - string
 * @param l - string length. Must be in [1, 8] interval.
 * @param to - value. Undefined value when false is returned
 * @return bool - true if conversion successful, false otherwise
 */
bool sscanf_8x(const char *from, const uint8_t l, uint32_t *to);

/**
 * @brief Converts a string containing an unsigned decimal integer to
 *        its value. Format can be i to iiiiiiiiii (10 i). Returns true
 *        if conversion went OK, false otherwise.
 *
 * @param from - string
 * @param l - string length
 * @param to - value. Undefined value when false i returned
 * @return bool - true if conversion successful, false otherwise
 */
bool sscanf_10i(const char *from, const uint8_t l, uint32_t *to);

/**
 * Scans IPv6 prefix or IID.
 */
int sscanf_4_hx(const char *from, uint16_t *pt);

/**
 * @brief Converts an hexadecimal ASCII character into
 *        its value.
 *
 * @param c '0' to '9', 'a' to 'f', 'A' to 'F'
 * @return the integer value of the hexadecimal character
 *         -1 if not an hexadecimal character
 */
int8_t ascii_to_hex(char c);

int get_int_len(int value);

/**
 * @brief Converts an hexadecimal ASCII CBOR package into
 * hexadecimal format.
 *
 * @param buf rules
 * @param buf_size size of the rules, in bytes

 * @return true if conversion succeeded, false otherwise.
 */
bool load_rules(const char *buf, uint16_t buf_size);

#endif /* AT_HELPERS_H */
