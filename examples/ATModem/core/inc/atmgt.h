/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Flavien Moullec flavien@ackl.io
 *
 * @brief AT SCHC commands for Management layer
 */

#ifndef AT_MGT_H
#define AT_MGT_H

#include <stdbool.h>

#include "at.h"
#include "fullsdkmgt.h"

bool fullsdk_is_busy(void);

mgt_status_t fullsdk_mgt_init(void);

void fullsdk_process(void);

/**
 * @brief Prints the version of the fullsdk running
 * @param Param represented in ASCII format
 * @retval AT_OK
 */
at_status_t at_fullsdk_version_get(const char *param, uint8_t *error);

/**
 * @brief This function selects the SDK API to be used (NET or DTG)
 * @param param API
 * @param error A pointer where an error code can be stored
 * @retval at_status_t
 */
at_status_t at_set_fullsdk_api(const char *param, uint8_t *error);

/**
 * @brief This function allows to add a template
 * @param param template
 * @param error A pointer where an error code can be stored
 * @retval at_status_t
 */
at_status_t at_set_tpl(const char *param, uint8_t *error);

/**
 * @brief This function allows to add payload compression rules
 *
 * @param param rules
 * @param error A pointer where an error code can be stored
 * @return at_status_t
 */
at_status_t at_set_payload_rules(const char *param, uint8_t *error);

/**
 * @brief This function allows to get the ID of the current template
 * @param param
 * @param error A pointer where an error code can be stored
 * @retval at_status_t
 */
at_status_t at_get_tpl_id(const char *param, uint8_t *error);

/**
 * @brief This function allows to get the number of parameters of the current
 * template
 * @param param
 * @param error A pointer where an error code can be stored
 * @retval at_status_t
 */
at_status_t at_get_nb_tpl_params(const char *param, uint8_t *error);

/**
 * @brief This function clear the template parameters
 *
 * @param[in] param not used
 * @param error A pointer where an error code can be stored
 * @return at_status_t
 */
at_status_t at_clear_tpl_params(const char *param, uint8_t *error);

/**
 * @brief This function allows to set a template parameter
 *
 * @param[in] param A list of template parameters (indexes,value)
 * @param error A pointer where an error code can be stored
 * @return at_status_t
 */
at_status_t at_set_template_params(const char *param, uint8_t *error);

/**
 * @brief This function allows to get a template parameter
 *
 * @param[in] param A list of comma separated template parameter indexes
 * @param error A pointer where an error code can be stored
 * @return at_status_t
 */
at_status_t at_get_template_params(const char *param, uint8_t *error);

/**
 * @brief Triggers a synchronization procedure with the IPCore
 * @param param represented in ASCII format
 * @return AT_OK or AT_SCHC_MGT_ERROR
 */
at_status_t at_sync(const char *param, uint8_t *error);

/**
 * @brief Allows to enable/disable class A polling as well as uplink suspension
 *
 * @param param Represented in ASCII format
 * @return AT_OK, AT_SCHC_MGT_ERROR or AT_PARAM_ERROR
 */
at_status_t at_schc_conf_poll(const char *param, uint8_t *error);

/**
 * @brief Triggers class A polling by sending an empty uplink frame
 *
 * @param param Represented in ASCII format
 * @return AT_OK, AT_SCHC_MGT_ERROR or AT_PARAM_ERROR
 */
at_status_t at_schc_trigger_poll(const char *param, uint8_t *error);

/**
 * @brief  Get device IP address
 * @param  String string of the AT command
 * @retval AT_OK if OK, or an appropriate AT_xxx error code
 */
at_status_t at_fullsdk_ip_get(const char *param, uint8_t *error);

/**
 * @brief Overwrite fragmentation profile rule
 *
 * @param param CBOR
 * @param error
 * @return at_status_t
 */
at_status_t at_fullsdk_frag_set(const char *param, uint8_t *error);

/**
 * @brief Overwrite QoS classes
 *
 * @param param CBOR
 * @param error
 * @return at_status_t
 */
at_status_t at_fullsdk_qos_set(const char *param, uint8_t *error);

/**
 * @brief Select QoS class
 *
 * @param param enum value (0: NoReliability - 1: NetworkReliability - 2: SystematicReport)
 * @param error
 * @return at_status_t
 */
at_status_t at_fullsdk_qos_select(const char *param, uint8_t *error);

#ifdef DTLS_ENABLED
/**
 * @brief Configure DTLS
 * 
 * @param param 
 * @param error 
 * @return at_status_t 
 */
at_status_t at_fullsdk_dtls_conf_set(const char *param, uint8_t *error);
#endif

#ifdef SCHC_TEMPLATE_SID
/**
 * @brief Initialize buffers to decode CBOR template for 
 * template using SID/YANG registry.
 * 
 * @return mgt_status_t 
 */
mgt_status_t at_sync_tpl_initialize(void);

/**
 * @brief Synchronize the SCHC template parameter with
 * the IPCore for template using SID/YANG registry.
 * 
 * @param param 
 * @param error 
 * @return at_status_t 
 */
at_status_t at_sync_tpl_params(const char *param, uint8_t *error);
#endif

#endif /* AT_MGT_H */
