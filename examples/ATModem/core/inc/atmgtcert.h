/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Jerome Elias - jerome.elias@ackl.io
 *
 * @brief AT SCHC commands for Management layer for SCHC Certification app.
 */

#ifndef AT_MGT_CERT_H
#define AT_MGT_CERT_H

#include <stdbool.h>

#include "at.h"
#include "fullsdkmgtcert.h"

extern uint8_t tpl_memory_area[];

/**
 * @brief This function initializes the SCHC certification layer.
 *
 * @return
 * - MGT_SUCCESS: success
 * - MGT_REQ_CALLBACK_ERR: error
 */
mgt_status_t fullsdk_mgt_cert_init(void);

/**
 * @brief This function performs layer processing, when some has to be done,
 * including the SCHC Certification layer. It must be called at least every
 * time mgt_processing_required_t callback has been called.
 * It must not be called from the callback.
 */
void fullsdk_cert_process(void);

/**
 * @brief This function get the initialization status of
 * SCHC Certification application.
 * 
 * @return true if SCHC Certification app has been already
 * initialized, false otherwise
 */
bool mgt_cert_is_initialized(void);

/**
 * @brief This function reset the initialization status
 * of SCHC Certification app.
 */
void mgt_cert_reset_init_status(void);

#endif /* AT_MGT_CERT_H */
