/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Jerome Elias - jerome.elias@ackl.io
 *
 * @brief Implementation of AT SCHC commands for SCHC certification app.
 */

#ifndef AT_CERT_H
#define AT_CERT_H

#include <stdint.h>

#include "at.h"

/**
 * @brief Number of NET_API AT commands
 */
#define NB_CERT_API_AT_COMMANDS 1

/**
 * @brief AT commands list
 */
extern const at_command_t at_cert_api_command[NB_CERT_API_AT_COMMANDS];

/**
 * @brief This function manage the status of SCHC Certification app.
 *
 * @param[in] status status of the SCHC Certification app.
 * @param[out] error A pointer where an error code can be stored
 * @return at_status_t
 */
at_status_t at_cert_set_app_status(const char *param, uint8_t *error);

#endif /* AT_CERT_H */
