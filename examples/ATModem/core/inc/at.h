/**
 ******************************************************************************
 * @file    at.h
 * @author  MCD Application Team
 * @brief   Header for driver at.c module
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AT_H__
#define __AT_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>

#include "platform.h"

/**
 * @brief AT command status codes
 */
typedef enum
{
  AT_OK = 0,
  AT_ERROR,
  AT_PARAM_ERROR,
  AT_TEST_PARAM_OVERFLOW,
  AT_RX_ERROR,
  AT_L2_ERROR,
  AT_NOT_IMPLEMENTED,
  AT_PLATFORM_ERROR,
  AT_OPERATION_NOT_ALLOWED,
  AT_SCHC_MGT_ERROR,
  AT_SCHC_DTG_ERROR,
  AT_SCHC_NET_ERROR,
  AT_CERT_ERROR,
  AT_SCHC_DTLS_ERROR,
  AT_MAX
} at_status_t;

typedef enum
{
  AT_NONE_API,
  AT_DTG_API,
  AT_NET_API,
} at_active_api_t;

extern uint8_t timer_id;
extern bool join_flag;

extern at_active_api_t at_active_api;
extern bool sdk_dtg_init;
#ifdef DTLS_ENABLED
extern bool sdk_dtls_configured;
#endif
extern bool sdk_net_init;

/* Exported constants --------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* AT printf */
#define AT_PRINTF PRINT_AT

  /**
   * @brief  Structure defining an AT Command
   */
  typedef struct
  {
    const char *string; /*< command string, after the "AT" */
    const int
        size_string; /*< size of the command string, not including the final \0 */
    at_status_t (*get)(
        const char *param,
        uint8_t *error);                                   /*< =? after the string to get the current value*/
    at_status_t (*set)(const char *param, uint8_t *error); /*< = (but not =?\0)
                                            after the string to set a value */
    at_status_t (*run)(
        const char *param,
        uint8_t *error); /*< \0 after the string - run the command */
#if !defined(NO_HELP)
    const char *help_string; /*< to be printed when ? after the string */
    const char *help_param;  /*< to be printed when =? after the string */
#endif
  } at_command_t;

  /* Exported functions ------------------------------------------------------- */

  /**
   * @brief  Return AT_ERROR in all cases
   * @param  Param string of the AT command - unused
   * @retval AT_ERROR
   */
  at_status_t at_return_error(const char *param, uint8_t *error);

  /**
   * @brief  Return AT_NOT_IMPLEMENTED in all cases
   * @param  Param string of the AT command - unused
   * @retval AT_NOT_IMPLEMENTED
   */
  at_status_t at_not_implemented_error(const char *param, uint8_t *error);

  /**
   * @brief  Trig a reset of the MCU
   * @param  Param string of the AT command - unused
   * @retval AT_OK
   */
  at_status_t at_reset(const char *param, uint8_t *error);

  /**
   * @brief  Print Device EUI
   * @param  Param string of the AT command - unused
   * @retval AT_OK
   */
  at_status_t at_DevEUI_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Device Eui
   * @param  Param string of the AT command
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_DevEUI_set(const char *param, uint8_t *error);

  /**
   * @brief  Print Join Eui
   * @param  Param string of the AT command
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_JoinEUI_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Join Eui
   * @param  Param string of the AT command
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_JoinEUI_set(const char *param, uint8_t *error);

  /**
   * @brief  Set DevAddr
   * @param  String pointing to provided DevAddr
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_DevAddr_set(const char *param, uint8_t *error);

  /**
   * @brief  Print the DevAddr
   * @param  String pointing to provided DevAddr
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_DevAddr_get(const char *param, uint8_t *error);

  /**
   * @brief  Print Application Key
   * @param  Param string of the AT command
   * @retval AT_OK
   */
  at_status_t at_AppKey_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Application Key
   * @param  Param string of the AT command
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_AppKey_set(const char *param, uint8_t *error);

  /**
   * @brief  Print Network Session Key
   * @param  String pointing to provided DevAddr
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_NwkSKey_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Network Session Key
   * @param  String pointing to provided DevAddr
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_NwkSKey_set(const char *param, uint8_t *error);

  /**
   * @brief  Print Application Session Key
   * @param  String pointing to provided DevAddr
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_AppSKey_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Application Session Key
   * @param  String pointing to provided DevAddr
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_AppSKey_set(const char *param, uint8_t *error);

  /**
   * @brief  Print Adaptative Data Rate setting
   * @param  String pointing to provided ADR setting
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_ADR_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Adaptative Data Rate setting
   * @param  String pointing to provided ADR setting
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_ADR_set(const char *param, uint8_t *error);

  /**
   * @brief  Print Data Rate
   * @param  String pointing to provided rate
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_DataRate_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Data Rate
   * @param  String pointing to provided rate
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_DataRate_set(const char *param, uint8_t *error);

  /**
   * @brief  Set ETSI Duty Cycle parameter
   * @param  String pointing to provided Duty Cycle value
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_DutyCycle_set(const char *param, uint8_t *error);

  /**
   * @brief  Get ETSI Duty Cycle parameter
   * @param  0 if disable, 1 if enable
   * @retval AT_OK
   */
  at_status_t at_DutyCycle_get(const char *param, uint8_t *error);

  /**
   * @brief  Print Public Network setting
   * @param  String pointing to provided Network setting
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_PublicNetwork_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Public Network setting
   * @param  String pointing to provided Network setting
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_PublicNetwork_set(const char *param, uint8_t *error);

  /**
   * @brief  Print Rx2 window frequency
   * @param  String pointing to parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Rx2Frequency_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Rx2 window frequency
   * @param  String pointing to parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Rx2Frequency_set(const char *param, uint8_t *error);

  /**
   * @brief  Print Rx2 window data rate
   * @param  String pointing to parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Rx2DataRate_get(const char *param, uint8_t *error);

  /**
   * @brief  Set Rx2 window data rate
   * @param  String pointing to parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Rx2DataRate_set(const char *param, uint8_t *error);

  /**
   * @brief  Print the delay between the end of the Tx and the Rx Window 1
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Rx1Delay_get(const char *param, uint8_t *error);

  /**
   * @brief  Set the delay between the end of the Tx and the Rx Window 1
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Rx1Delay_set(const char *param, uint8_t *error);

  /**
   * @brief  Print the delay between the end of the Tx and the Rx Window 2
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Rx2Delay_get(const char *param, uint8_t *error);

  /**
   * @brief  Set the delay between the end of the Tx and the Rx Window 2
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Rx2Delay_set(const char *param, uint8_t *error);

  /**
   * @brief  Print the delay between the end of the Tx and the Join Rx Window 1
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_JoinAcceptDelay1_get(const char *param, uint8_t *error);

  /**
   * @brief  Set the delay between the end of the Tx and the Join Rx Window 1
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_JoinAcceptDelay1_set(const char *param, uint8_t *error);

  /**
   * @brief  Print the delay between the end of the Tx and the Join Rx Window 2
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_JoinAcceptDelay2_get(const char *param, uint8_t *error);

  /**
   * @brief  Set the delay between the end of the Tx and the Join Rx Window 2
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_JoinAcceptDelay2_set(const char *param, uint8_t *error);

  /**
   * @brief  Print network join mode
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_NetworkJoinMode_get(const char *param, uint8_t *error);

  /**
   * @brief  Set network join mode
   * @param  String pointing to provided param: "1" for OTAA, "0" for ABP
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_NetworkJoinMode_set(const char *param, uint8_t *error);

  /**
   * @brief  Print the Network ID
   * @param  String pointing to provided parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_NetworkID_get(const char *param, uint8_t *error);

  /**
   * @brief  Set the Network ID
   * @param  String pointing to provided parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_NetworkID_set(const char *param, uint8_t *error);

  /**
   * @brief  Print the Device Class
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_DeviceClass_get(const char *param, uint8_t *error);

  /**
   * @brief  Set the Device Class
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_DeviceClass_set(const char *param, uint8_t *error);

  /**
   * @brief  Print active region
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_ActiveRegion_get(const char *param, uint8_t *error);

  /**
   * @brief  Set active region
   * @param  String pointing to provided param
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_ActiveRegion_set(const char *param, uint8_t *error);

#ifdef DTLS_ENABLED
  /**
   * @brief  Join a network and start DTLS
   * @param  String parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_JoinDtls(const char *param, uint8_t *error);
#else
  /**
   * @brief  Join a network
   * @param  String parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Join(const char *param, uint8_t *error);
#endif

  /**
   * @brief  Print the join status
   * @param  String parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_NetworkJoinStatus(const char *param, uint8_t *error);

  /**
   * @brief  Send a message
   * @param  String parameter of hexadecimal value
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_SendBinary(const char *param, uint8_t *error);

  /**
   * @brief  Print last received message
   * @param  String parameter
   * @retval AT_OK if OK, or an appropriate AT_xxx error code
   */
  at_status_t at_Send(const char *param, uint8_t *error);

  /**
   * @brief  Print the version of the ATModem FW
   * @param  String parameter
   * @retval AT_OK
   */
  at_status_t at_version_get(const char *param, uint8_t *error);

  /**
   * @brief  Set if message acknowledgment is required (1) or not (0)
   * @param  String parameter
   * @retval AT_OK
   */
  at_status_t at_ack_set(const char *param, uint8_t *error);

  /**
   * @brief  Get if message acknowledgment is required (1) or not (0)
   * @param  String parameter
   * @retval AT_OK
   */
  at_status_t at_ack_get(const char *param, uint8_t *error);

  /**
   * @brief  set the Modem in Certif Mode
   * @param  String parameter
   * @retval AT_OK
   */
  at_status_t at_Certif(const char *param, uint8_t *error);

  /**
   * @brief Set system rx error parameter
   * @param  String parameter
   * @retval AT_OK
   */
  at_status_t at_SystemRxError_set(const char *param, uint8_t *error);

  /**
   * @brief Get system rx error parameter
   * @param  String parameter
   * @retval AT_OK
   */
  at_status_t at_SystemRxError_get(const char *param, uint8_t *error);

#ifdef PLATFORM_TRACE_ENABLED
  /**
   * @brief Sets whether platform traces should be enabled or not
   * @param bool parameter
   * @retval AT_OK
   */
  at_status_t at_set_platform_traces(const char *param, uint8_t *error);
#endif

  /**
   * @brief Get last downlink SNR
   * @param  String parameter
   * @retval AT_OK
   */
  at_status_t at_get_snr(const char *param, uint8_t *error);

  /**
   * @brief Get last downlink RSSI
   * @param  String parameter
   * @retval AT_OK
   */
  at_status_t at_get_rssi(const char *param, uint8_t *error);

  /**
   * @brief enable or disable modem echo mode
   *
   * @param param 1 or 0
   * @param error
   * @return at_status_t
   */
  at_status_t at_modem_echo(const char *param, uint8_t *error);

  /**
   * @brief Get enable echo flag
   *
   * @return bool true or false
   */
  bool get_enable_echo_flag(void);

#ifdef __cplusplus
}
#endif

#endif /* __AT_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
