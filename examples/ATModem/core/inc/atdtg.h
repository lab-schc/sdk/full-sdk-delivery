/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Flavien Moullec flavien@ackl.io
 *
 * @brief AT SCHC commands for Datagram layer
 */

#ifndef AT_DTG_H
#define AT_DTG_H

#include <stdint.h>

#include "at.h"

/**
 * @brief Number of DTG_API AT commands
 */
#define NB_DTG_API_AT_COMMANDS 5

/**
 * @brief AT commands list
 */
extern const at_command_t at_dtg_api_command[NB_DTG_API_AT_COMMANDS];

/**
 * @brief Get RX buffer from the first socket (not a AT command)
 * @param Param size of rx buffer poiter
 * @retval RX buffer
 */
uint8_t* at_dtg_get_first_socket_rx_buf(uint16_t *size);

/**
 * @brief Get TX buffer from the first socket (not a AT command)
 * @param Param size of rx buffer poiter
 * @retval TX buffer
 */
uint8_t* at_dtg_get_first_socket_tx_buf(uint16_t *size);

/**
 * @brief Close first DTG socket (not a AT command)
 */
void at_dtg_close_first_socket(void);

/**
 * @brief Creates a datagram socket
 * @param Param represented in ASCII format
 * @retval AT_OK if OK otherwise AT_ERROR
 */
at_status_t at_socket(const char *param, uint8_t *error);

/**
 * @brief Binds host IP address and port to a datagram socket
 * @param Param represented in ASCII format
 * @retval AT_OK if OK otherwise AT_ERROR
 */
at_status_t at_bind(const char *param, uint8_t *error);

/**
 * @brief Closes a datagram socket
 * @param Param represented in ASCII format
 * @retval AT_OK if OK otherwise AT_ERROR
 */
at_status_t at_close(const char *param, uint8_t *error);

/**
 * @brief Sends data through FullSDK
 * @param Param represented in ASCII format
 * @retval AT_OK if OK otherwise AT_ERROR
 */
at_status_t at_ext_send(const char *param, uint8_t *error);

/**
 * @brief Sends data through FullSDK
 * @param Param represented in hexadecimal format
 * @retval AT_OK if OK otherwise AT_ERROR
 */
at_status_t at_ext_send_b(const char *param, uint8_t *error);

#endif /* AT_DTG_H */
