/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Pascal Bodin pascal@ackl.io
 *
 * @brief Handling of FullSDK DTLS layer.
 * 
 * @details When DTLS layer is activated, all communication between
 *          the device and IPCore is done over DTLS (synchronization
 *          excluded).
 * 
 *          Following command flow is possible:
 *          - AT+SCHC=DTLSCONF,SET,...
 *          - AT+JOINDTLS=...
 *          - AT+SCHC=API,...
 *          - synchronization
 *          - communication
 *          Or this one:
 *          - AT+SCHC=DTLSCONF,SET,...
 *          - AT+JOINDTLS=...
 *          - synchronization
 *          - AT+SCHC=API,...
 *          - communication
 * 
 *          If a new handshake phase is required, it is handled in
 *          a transparent way. Threeresult codes are sent to the 
 *          application by AT+JOINDTLS: OK, +JOINED and then +HANDSHAKEOK 
 *          or +HANDSHAKEFAIL.
 */

#include "at.h"
#include "atdtls.h"
#include "atmgt.h" // To use mgt_trigger_polling()

typedef enum
{
    HANDSHAKE_REQUIRED,
    HANDSHAKING,
    HANDSHAKE_DONE,
} handshake_state_t;

bool handshake_result_received = false;
dtls_status_t handshake_result;
bool handshake_requested = false;

bool dtls_processing_required = false;

static handshake_state_t handshake_state = HANDSHAKE_REQUIRED;

bool handshake_done(void)
{
    if (handshake_state == HANDSHAKE_DONE)
    {
        return true;
    }
    return false;
}

void reset_dtls_automaton(void)
{
    handshake_state = HANDSHAKE_REQUIRED;
}

void fullsdk_dtls_process(void)
{

    if (!dtls_processing_required) return;

    // No problem of data consistency as data is modified or read
    // in application context.
    dtls_processing_required = false;

    switch (handshake_state)
    {
    case HANDSHAKE_REQUIRED:
        if (handshake_requested)
        {
            handshake_requested = false;
            // Trigger a dummy uplink message
            // because first uplink message will be a confirmed one
            // in using stack semtech L2 layer. An empty frame with
            // RuleId 0 will be sent by LNS.
            // So, device must send an uplink first before starting
            // handshake process.
            mgt_trigger_polling();

            dtls_status_t dtls_status = dtls_handshake();
            if (dtls_status != DTLS_SUCCESS)
            {
                AT_PRINTF("+HANDSHAKEFAIL\n");
                // Stay in same state.
                break;
            }
            handshake_state = HANDSHAKING;
            break;
        }
        if (handshake_result_received)
        {
            // Ignore and silently absorb.
            handshake_result_received = false;
        }
        break;
    case HANDSHAKING:
        if (handshake_result_received)
        {
            handshake_result_received = false;
            if (handshake_result == DTLS_SUCCESS)
            {
                AT_PRINTF("+HANDSHAKEOK\n");
                handshake_state = HANDSHAKE_DONE;
                break;
            }
            AT_PRINTF("+HANDSHAKEFAIL\n");
            handshake_state = HANDSHAKE_REQUIRED;
            break;
        }
        if (handshake_requested)
        {
            // Ignore and silently absorb.
            handshake_requested = false;
        }
        break;
    case HANDSHAKE_DONE:
        if (handshake_result_received)
        {
            handshake_result_received = false;
            if (handshake_result != DTLS_SUCCESS)
            {
                AT_PRINTF("+HANDSHAKEFAIL\n");
                dtls_status_t dtls_status = dtls_handshake();
                if (dtls_status != DTLS_SUCCESS)
                {
                    handshake_state = HANDSHAKE_REQUIRED;
                    break;
                }
                handshake_state = HANDSHAKING;
                break;
            }
            AT_PRINTF("+HANDSHAKEOK\n");
            // Stay in same state.
            break;
        }
        if (handshake_requested)
        {
            // Ignore and silently absorb.
            handshake_requested = false;
        }
        break;
    default:
        ;
    }
}