/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @authors Flavien Moullec flavien@ackl.io
 *          Pascal Bodin pascal@ackl.io
 *
 * @brief Implementation of AT helper functions
 */

#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>

#include "helpers.h"
#include "at.h"
#include "platform.h"

// Forward declarations.
static int sscanf_n_mx(const char *from, uint8_t n, uint8_t m, uint32_t *pt);
static uint8_t hex_digit_to_int(char c);

void print_d(int value)
{
  AT_PRINTF("%d\n", value);
}

void print_u(unsigned int value)
{
  AT_PRINTF("%u\n", value);
}

void print_16_02x(uint8_t *pt)
{
  AT_PRINTF("%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x:%"
            "02x:%02x:%02x\n",
            pt[0], pt[1], pt[2], pt[3], pt[4], pt[5], pt[6], pt[7], pt[8],
            pt[9], pt[10], pt[11], pt[12], pt[13], pt[14], pt[15]);
}

void print_16_02x_2(uint8_t *pt)
{
  AT_PRINTF("%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:"
            "%02x%02x\n",
            pt[0], pt[1], pt[2], pt[3], pt[4], pt[5], pt[6], pt[7], pt[8],
            pt[9], pt[10], pt[11], pt[12], pt[13], pt[14], pt[15]);
}

// Input format: xx:xx:xx:xx (4 times 1 to 2 hexa digits separated by colons).
int sscanf_uint32_as_hhx(const char *from, uint32_t *value)
{
  return sscanf_n_mx(from, 4, 2, value);
}

void print_uint32_as_02x(uint32_t value)
{
  AT_PRINTF("%02x:%02x:%02x:%02x\n", (unsigned)((unsigned char *)(&value))[3],
            (unsigned)((unsigned char *)(&value))[2],
            (unsigned)((unsigned char *)(&value))[1],
            (unsigned)((unsigned char *)(&value))[0]);
}

void print_8_02x(uint8_t *pt)
{
  AT_PRINTF("%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\n", pt[0], pt[1], pt[2],
            pt[3], pt[4], pt[5], pt[6], pt[7]);
}

// Input format: xx:xx:...xx:xx (16 times 1 to 2 hexa digits separated by colons).
int sscanf_16_hhx(const char *from, uint8_t *pt)
{
  uint32_t val[16];
  int scanned_val_nb = sscanf_n_mx(from, 16, 2, val);
  // If scan failed, scanned_val_nb is 0.
  for (int i = 0; i < scanned_val_nb; i++)
  {
    pt[i] = (uint8_t)val[i];
  }
  return scanned_val_nb;
}

// Input format: xx:xx:...xx:xx (8 times 1 to 2 hexa digits separated by colons).
int sscanf_8_hhx(const char *from, uint8_t *pt)
{
  uint32_t val[8];
  int scanned_val_nb = sscanf_n_mx(from, 8, 2, val);
  // If scan failed, scanned_val_nb is 0.
  for (int i = 0; i < scanned_val_nb; i++)
  {
    pt[i] = (uint8_t)val[i];
  }
  return scanned_val_nb;
}

// Input format: xxxx:xxxx:xxxx:xxxx (4 times 1 to 4 hexa digits separated by colons).
int sscanf_4_hx(const char *from, uint16_t *pt)
{
  uint32_t val[4];
  int scanned_val_nb = sscanf_n_mx(from, 4, 4, val);
  // If scan failed, scanned_val_nb is 0.
  for (int i = 0; i < scanned_val_nb; i++)
  {
    pt[i] = (uint16_t)val[i];
  }
  return scanned_val_nb;
}

// Scans n values of up to m hexadecimal digits separated by colons.
// Returns 0 at first detected error.
// n must be > 1. m must be <= 8.
static int sscanf_n_mx(const char *from, uint8_t n, uint8_t m, uint32_t *pt)
{
  const uint8_t MAX_LENGTH = n * m + n - 1;
  enum
  {
    S4_FIRST_VALS,
    S4_LAST_VAL
  } current_state = S4_FIRST_VALS;
  uint8_t index = 1;
  uint8_t val_start_index = 0;
  uint8_t val_index = 0;
  uint32_t val = 0;
  bool sscanf_res;
  while (true)
  {
    switch (current_state)
    {
    case S4_FIRST_VALS:
      if (from[index] == ':')
      {
        // Check that we don't have more digits than authorized.
        if (index - val_start_index > m)
        {
          return 0;
        }
        sscanf_res = sscanf_8x(from + val_start_index, index - val_start_index, &val);
        if (!sscanf_res)
        {
          return 0;
        }
        // At this stage, a valid value is available.
        pt[val_index] = (uint16_t)val;
        index++;
        val_index++;
        val_start_index = index;
        if (val_index >= n - 1)
        {
          // This was the penultimate value.
          current_state = S4_LAST_VAL;
          break;
        }
        // At this stage, this wasn't the penultimate value yet.
        // Stay in same state.
        break;
      }
      // At this stage, not a colon. Go on scanning number.
      index++;
      // Prevent buffer overflow.
      if (index >= MAX_LENGTH)
      {
        return 0;
      }
      break;
    case S4_LAST_VAL:
      // For last value, we stop at first non digit or at max value length.
      // Type cast to prevent Array Subscript Has Type 'char' warning.
      if (!isxdigit((uint8_t)(from[index])) || (index - val_start_index >= m))
      {
        sscanf_res = sscanf_8x(from + val_start_index, index - val_start_index, &val);
        if (!sscanf_res)
        {
          return 0;
        }
        // At this stage, a valid value is available.
        pt[val_index] = (uint16_t)val;
        return n;
      }
      // At this stage, not a colon. Go on scanning number.
      index++;
      // Prevent buffer overflow. > instead of >= is important: it allows to go
      // through the end test above.
      if (index > MAX_LENGTH)
      {
        return 0;
      }
      break;
    default:
      // Internal error.
      return 0;
    }
  }
}

bool sscanf_8x(const char *from, const uint8_t l, uint32_t *to)
{
  uint8_t index = 0;
  uint32_t val = 0;
  if ((l == 0) || (l > 4))
  {
    return false;
  }
  while (true)
  {
    // Type cast to prevent Array Subscript Has Type 'char' warning.
    if (!isxdigit((unsigned char)(from[index])))
    {
      return false;
    }
    val = 16 * val + hex_digit_to_int(from[index]);
    index++;
    if (index >= l)
    {
      *to = val;
      return true;
    }
  }
}

bool sscanf_10i(const char *from, const uint8_t l, uint32_t *to)
{
  uint8_t index = 0;
  uint32_t val = 0;
  if ((l == 0) || (l > 10))
  {
    return false;
  }
  while (true)
  {
    // Type cast to prevent Array Subscript Has Type 'char' warning.
    if (!isdigit((unsigned char)(from[index])))
    {
      return false;
    }
    val = 10 * val + from[index] - '0';
    index++;
    if (index >= l)
    {
      *to = val;
      return true;
    }
  }
}

/**
 * @brief Converts an hexadecimal character to its value. Must
 *        be called only on an hexadecimal character.
 *
 * @param c - hexadecimal character: '0' to '9', 'A' to 'F', 'a' to 'f'
 * @return uint8_t - value of the hexadecimal character
 */
static uint8_t hex_digit_to_int(char c)
{
  c |= 0x20;
  if ((c >= '0') && (c <= '9'))
  {
    return c - '0';
  }
  return c - 'a' + 10;
}

int8_t ascii_to_hex(char c)
{
  c |= 0x20;

  if (c >= '0' && c <= '9')
  {
    return c - '0';
  }
  else if (c >= 'a' && c <= 'f')
  {
    return (c - 'a') + 10;
  }
  else
  {
    return -1;
  }
}

int get_int_len(int value)
{
  int l = 1;

  while (value > 9)
  {
    l++;
    value /= 10;
  }

  return l;
}

bool load_rules(const char *buf, uint16_t buf_size)
{
  char hex[2] = {0};
  uint16_t size = 0;
  uint32_t val;

  while (buf_size > 1)
  {
    hex[0] = buf[size * 2];
    hex[1] = buf[size * 2 + 1];

    if (!sscanf_8x(hex, 2, &val))
    {
      return false;
    }
    ((uint8_t *)buf)[size] = (uint8_t)val;

    size++;
    buf_size -= 2;
  }

  // We don't expect parameters anymore
  if (buf_size != 0)
  {
    return false;
  }
  return true;
}
