/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Authors: Aydogan Ersoz - aydogan.ersoz@ackl.io
 *          Pascal Bodin - pascal@ackl.io
 *
 * Based on ST's AT_Slave example. Check license here: www.st.com/SLA0044.
 *
 */

#include "at.h"
#ifdef DTLS_ENABLED
#include "atdtls.h"
#endif
#include "atmgt.h"
#include "fullsdkl2.h"
#include "platform.h"
#ifdef SCHC_CERTIFICATION_ENABLED
#include "atmgtcert.h"
#include "fullsdkl2acert.h"
#endif
#ifdef TRACE_ENABLED
#include "fullsdktrace.h"
#endif

void CMD_Init(void);
void CMD_Process(void);

int main(void)
{
  platform_hw_init();

  l2_init();

  CMD_Init();

  while (1)
  {
    /* Handle UART commands */
    CMD_Process();

    /* Handle FullSDK stack */
    fullsdk_process();

#ifdef DTLS_ENABLED
    /* Handle DTLS */
    fullsdk_dtls_process();
#endif

#ifdef SCHC_CERTIFICATION_ENABLED
    /* Manage SCHC Certification application process */
    fullsdk_cert_process();

    // Call LmHandler process
    l2a_cert_compliance_process();
#endif

    /* Poll L2 periodic scheduler if needed */
    l2_periodic_scheduler();

    /* Reset watchdog if needed */
    platform_reload_watchdog();
  }
}

#ifdef TRACE_ENABLED
/**
 * For traces. Called for each trace message.
 */
void tr_handle_trace(const uint8_t *trace, uint8_t length)
{
  static const char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                             '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
  // For every byte of the binary trace message, we need two bytes in the
  // output
  // string, and a final '\0' byte.
  char out_str[TRACE_MAX_LENGTH * 2 + 1];
  for (uint8_t i = 0; i < length; i++)
  {
    out_str[2 * i] = hex[trace[i] >> 4];
    out_str[2 * i + 1] = hex[trace[i] & 0x0f];
  }
  out_str[length * 2] = '\0';
  PRINT_MSG("TRA>%s\n", out_str);
}
#endif
