/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Jerome Elias - jerome.elias@ackl.io
 *
 * @brief Implementation of AT SCHC commands for SCHC certification app.
 */

/* Includes ------------------------------------------------------------------*/
#include <string.h>

#include "at.h"
#include "atcert.h"
#include "atdtg.h"
#include "atmgtcert.h"
#include "fullsdkextapi.h"
#include "fullsdkl2.h"

#define AT_CERT_APP_ENABLE "+CERTIF=SCHC"

const at_command_t at_cert_api_command[NB_CERT_API_AT_COMMANDS] = {{
    .string = AT_CERT_APP_ENABLE,
    .size_string = sizeof(AT_CERT_APP_ENABLE) - 1,
#ifndef NO_HELP
    .help_string = "AT" AT_CERT_APP_ENABLE ": Set the module in SCHC Certification over LoRaWAN Mode.\n",
    .help_param = "",
#endif
    .get = at_return_error,
    .set = at_cert_set_app_status,
    .run = at_return_error,
}};

at_status_t at_cert_set_app_status(const char *param, uint8_t *error)
{
  const char *buf = param;
  uint16_t buf_size = strlen(param);

  if (buf_size != 1)
  {
    return AT_PARAM_ERROR;
  }

  if (!join_flag)
  {
    *error = MGT_L2A_ERR;
    return AT_CERT_ERROR;
  }

  // Get app status
  switch (buf[0])
  {
  case '1':
    // Close first DTG socket if opened
    at_dtg_close_first_socket();
    // Enable SCHC Certification app.
    mgt_cert_enable_cert_app(true);
    // Reset dtg/net layer
    sdk_net_init = false;
    sdk_dtg_init = false;
    // Initialize SCHC Certification MGT layer.
    mgt_status_t status = fullsdk_mgt_cert_init();
    if (status != MGT_SUCCESS)
    {
      *error = status;
      return AT_CERT_ERROR;
    }
    break;
  case '0':
    mgt_cert_enable_cert_app(false);
    mgt_cert_reset_init_status();
    // After disabling SCHC Certification app, user must:
    // 1/ re-load his own CBOR compression rules.
    // 2/ re-initialize dtg/network layer.
    // 3/ set his favorite L2A technology for fragmentation
    // 4/ do a join request to apply new fragmentation profile if updated
    if (mgt_cert_is_initialized())
    {
      // We just reset the template parameters
      mgt_status_t status = mgt_reset_template_params();
      if (status != MGT_SUCCESS)
      {
        *error = status;
        return AT_SCHC_MGT_ERROR;
      }
      // Close first DTG socket if opened
      at_dtg_close_first_socket();
      // Reset dtg/net layer
      sdk_net_init = false;
      sdk_dtg_init = false;
    }
    break;
  default:
    return AT_PARAM_ERROR;
  }

  return AT_OK;
}
