/**
 ******************************************************************************
 * @file    at.c
 * @author  MCD Application Team
 * @brief   at command API
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <ctype.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "at.h"
#include "atdtg.h"
#include "atmgt.h"
#include "atnet.h"
#ifdef DTLS_ENABLED
#include "atdtls.h"
#include "fullsdkdtls.h"
#endif
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "helpers.h"
#include "platform.h"

static bool ctx_enable_echo_flag = true;

/**
 * @brief Max size of the data that can be received
 */
#define MAX_RECEIVED_DATA 1024
/* Private variables ---------------------------------------------------------*/
/*!
 * User application data buffer size
 */
#define LORAWAN_APP_DATA_BUFF_SIZE 64

/*!
 * User application data
 */
static uint8_t app_data_buff[LORAWAN_APP_DATA_BUFF_SIZE];

/*!
 * User application data structure
 */
static l2_app_data_t app_data = {
    .type = L2_MSG_TYPE_UNCONFIRMED,
    .port = 0,
    .size = 0,
    .buffer = app_data_buff,
};

uint8_t timer_id = 0;
bool join_flag = false;
bool sdk_dtg_init = false;
#ifdef DTLS_ENABLED
bool sdk_dtls_configured = false;
#endif
bool sdk_net_init = false;
at_active_api_t at_active_api = AT_NONE_API;

char class = 'A';

static void transmission_result_received(bool has_error)
{
  AT_PRINTF(has_error ? "+SENDFAIL\n" : "+SENDOK\n");
}

/* Exported functions ------------------------------------------------------- */

bool get_enable_echo_flag(void)
{
  return ctx_enable_echo_flag;
}

at_status_t at_return_error(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  return AT_ERROR;
}

at_status_t at_not_implemented_error(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
}

at_status_t at_reset(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  platform_reset();

  return AT_OK;
}

at_status_t at_DevEUI_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint8_t *dev_eui;

  dev_eui = l2_get_dev_eui();
  print_8_02x(dev_eui);

  return AT_OK;
}

at_status_t at_DevEUI_set(const char *param, uint8_t *error)
{
  (void)error;
  uint8_t dev_eui[8];
  if (sscanf_8_hhx(param, dev_eui) != 8)
  {
    return AT_PARAM_ERROR;
  }

  l2_set_dev_eui(dev_eui);

  return AT_OK;
}

at_status_t at_JoinEUI_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint8_t *join_eui;

  join_eui = l2_get_join_eui();
  print_8_02x(join_eui);

  return AT_OK;
}

at_status_t at_JoinEUI_set(const char *param, uint8_t *error)
{
  (void)error;
  uint8_t join_eui[8];
  if (sscanf_8_hhx(param, join_eui) != 8)
  {
    return AT_PARAM_ERROR;
  }

  l2_set_join_eui(join_eui);

  return AT_OK;
}

at_status_t at_DevAddr_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t dev_addr;
  if (sscanf_uint32_as_hhx(param, &dev_addr) != 4)
  {
    return AT_PARAM_ERROR;
  }
  l2_set_dev_addr(dev_addr);

  return AT_OK;
}

at_status_t at_DevAddr_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint32_t dev_addr;
  if (l2_get_dev_addr(&dev_addr) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_uint32_as_02x(dev_addr);

  return AT_OK;
}

at_status_t at_AppKey_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  print_16_02x(l2_get_app_key());

  return AT_OK;
}

at_status_t at_AppKey_set(const char *param, uint8_t *error)
{
  (void)error;
  uint8_t app_key[16];

  if (sscanf_16_hhx(param, app_key) != 16)
  {
    return AT_PARAM_ERROR;
  }

  l2_set_app_key(app_key);

  return AT_OK;
}

at_status_t at_NwkSKey_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint8_t nwk_s_key[16];

  if (l2_get_nwk_s_key(nwk_s_key) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_16_02x(nwk_s_key);

  return AT_OK;
}

at_status_t at_NwkSKey_set(const char *param, uint8_t *error)
{
  (void)error;
  uint8_t nwk_s_key[16];

  if (sscanf_16_hhx(param, nwk_s_key) != 16)
  {
    return AT_PARAM_ERROR;
  }

  l2_set_nwk_s_key(nwk_s_key);
  return AT_OK;
}

at_status_t at_AppSKey_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint8_t app_s_key[16];
  if (l2_get_app_s_key(app_s_key) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_16_02x(app_s_key);

  return AT_OK;
}

at_status_t at_AppSKey_set(const char *param, uint8_t *error)
{
  (void)error;
  uint8_t app_s_key[16];

  if (sscanf_16_hhx(param, app_s_key) != 16)
  {
    return AT_PARAM_ERROR;
  }
  l2_set_app_s_key(app_s_key);

  return AT_OK;
}

at_status_t at_Certif(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  l2_set_cert_mode();

  return AT_OK;
}

at_status_t at_ADR_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  bool adr;

  if (l2_get_adr(&adr) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_d(adr);

  return AT_OK;
}

at_status_t at_ADR_set(const char *param, uint8_t *error)
{
  (void)error;
  switch (param[0])
  {
  case '0':
  case '1':
    l2_set_adr(param[0] - '0');
    break;
  default:
    return AT_PARAM_ERROR;
  }

  return AT_OK;
}

at_status_t at_DataRate_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint8_t dr;

  if (l2_get_dr(&dr) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_d(dr);

  return AT_OK;
}

at_status_t at_DataRate_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t data_rate;

  if (!sscanf_10i(param, strlen(param), &data_rate))
  {
    return AT_PARAM_ERROR;
  }
  if (data_rate > 15)
  {
    return AT_PARAM_ERROR;
  }

  if (l2_set_dr((uint8_t)data_rate) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_DutyCycle_set(const char *param, uint8_t *error)
{
  (void)error;
  switch (param[0])
  {
  case '0':
    l2_set_dutycycle(false);
    break;
  case '1':
    l2_set_dutycycle(true);
    break;
  default:
    return AT_PARAM_ERROR;
  }

  return AT_OK;
}

at_status_t at_DutyCycle_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  bool dutycycle;
  if (l2_get_dutycycle(&dutycycle) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }
  if (dutycycle)
    AT_PRINTF("1\n");
  else
    AT_PRINTF("0\n");

  return AT_OK;
}

at_status_t at_PublicNetwork_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  bool public_network;
  if (l2_get_public_network(&public_network) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }
  print_d(public_network);

  return AT_OK;
}

at_status_t at_PublicNetwork_set(const char *param, uint8_t *error)
{
  (void)error;
  switch (param[0])
  {
  case '0':
  case '1':
    l2_set_public_network(param[0] - '0');
    break;
  default:
    return AT_PARAM_ERROR;
  }

  return AT_OK;
}

at_status_t at_Rx2Frequency_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint32_t freq;
  if (l2_get_rx2_frequency(&freq) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_d(freq);

  return AT_OK;
}

at_status_t at_Rx2Frequency_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t rx2_freq;

  if (!sscanf_10i(param, strlen(param), &rx2_freq))
  {
    return AT_PARAM_ERROR;
  }
  if (l2_set_rx2_frequency(rx2_freq) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_Rx2DataRate_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint8_t dr;
  if (l2_get_rx2_dr(&dr) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_d(dr);

  return AT_OK;
}

at_status_t at_Rx2DataRate_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t rx2_dr;

  if (!sscanf_10i(param, strlen(param), &rx2_dr))
  {
    return AT_PARAM_ERROR;
  }
  if (l2_set_rx2_dr((uint8_t)rx2_dr) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_Rx1Delay_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint32_t delay;

  if (l2_get_rx1_delay(&delay) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_u(delay);

  return AT_OK;
}

at_status_t at_Rx1Delay_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t rx1_delay;

  if (!sscanf_10i(param, strlen(param), &rx1_delay))
  {
    return AT_PARAM_ERROR;
  }
  if (l2_set_rx1_delay(rx1_delay) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_Rx2Delay_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint32_t delay;
  if (l2_get_rx2_delay(&delay) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_d(delay);

  return AT_OK;
}

at_status_t at_Rx2Delay_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t rx2_delay;

  if (!sscanf_10i(param, strlen(param), &rx2_delay))
  {
    return AT_PARAM_ERROR;
  }
  if (l2_set_rx2_delay(rx2_delay) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_JoinAcceptDelay1_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint32_t delay;

  if (l2_get_join_accept_delay1(&delay) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_u(delay);

  return AT_OK;
}

at_status_t at_JoinAcceptDelay1_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t ja_delay1;

  if (!sscanf_10i(param, strlen(param), &ja_delay1))
  {
    return AT_PARAM_ERROR;
  }
  if (l2_set_join_accept_delay1(ja_delay1) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_JoinAcceptDelay2_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint32_t delay;
  if (l2_get_join_accept_delay2(&delay) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }
  print_u(delay);

  return AT_OK;
}

at_status_t at_JoinAcceptDelay2_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t ja_delay2;

  if (sscanf_10i(param, strlen(param), &ja_delay2))
  {
    return AT_PARAM_ERROR;
  }
  if (l2_set_join_accept_delay2(ja_delay2) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_SystemRxError_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t sys_max_rx_err;

  if (sscanf_10i(param, strlen(param), &sys_max_rx_err))
    return AT_PARAM_ERROR;
  if (l2_set_system_max_rx_error(sys_max_rx_err) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_SystemRxError_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint32_t max_rx_error;
  if (l2_get_system_max_rx_error(&max_rx_error) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_d(max_rx_error);

  return AT_OK;
}

at_status_t at_NetworkJoinMode_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  l2_network_activation_type_t mode;

  if (l2_get_network_activation(&mode) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_d((uint8_t)mode);

  return AT_OK;
}

at_status_t at_NetworkJoinMode_set(const char *param, uint8_t *error)
{
  (void)error;
  l2_network_activation_type_t mode;
  switch (param[0])
  {
  case '0':
    mode = L2_NETWORK_ACTIVATION_TYPE_ABP;
    break;
  case '1':
    mode = L2_NETWORK_ACTIVATION_TYPE_OTAA;
    break;
  default:
    return AT_PARAM_ERROR;
  }

  l2_set_network_activation(mode);

  return AT_OK;
}

at_status_t at_NetworkID_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint32_t network_id;
  int l2_status = l2_get_network_id(&network_id);
  if (l2_status != L2_SUCCESS)
  {
    *error = l2_status;
    return AT_L2_ERROR;
  }

  print_uint32_as_02x(network_id);

  return AT_OK;
}

at_status_t at_NetworkID_set(const char *param, uint8_t *error)
{
  (void)error;
  uint32_t network_id;

  if (sscanf_uint32_as_hhx(param, &network_id) != 4)
  {
    return AT_PARAM_ERROR;
  }

  l2_set_network_id(network_id);

  return AT_OK;
}

at_status_t at_DeviceClass_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  char class;

  if (l2_get_class(&class) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  AT_PRINTF("%c\n", class);

  return AT_OK;
}

static int8_t convert_string_to_region(const char *param)
{
  (void)param;
  for (size_t i = 0; i < L2_MAX_SUPPORTED_REGION; i++)
  {
    if (strcmp(regions[i].str, param) == 0)
    {
      return regions[i].id;
    }
  }

  return -1;
}

static const char *convert_region_to_string(l2_region_id_t region)
{
  for (size_t i = 0; i < L2_MAX_SUPPORTED_REGION; i++)
  {
    if (regions[i].id == region)
    {
      return regions[i].str;
    }
  }

  return NULL;
}

at_status_t at_ActiveRegion_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  l2_region_id_t id;
  if (l2_get_active_region(&id) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  const char *region = convert_region_to_string(id);
  if (region == NULL)
  {
    return AT_L2_ERROR;
  }

  AT_PRINTF("%s\n", region);

  return AT_OK;
}

at_status_t at_ActiveRegion_set(const char *param, uint8_t *error)
{
  (void)error;
  int8_t region = convert_string_to_region(param);

  if (region != -1)
  {
    if (l2_set_active_region(region) != L2_SUCCESS)
    {
      return AT_L2_ERROR;
    }

    return AT_OK;
  }

  return AT_ERROR;
}

#ifdef DTLS_ENABLED
at_status_t at_JoinDtls(const char *param, uint8_t *error)
#else
at_status_t at_Join(const char *param, uint8_t *error)
#endif
{
  uint32_t duty_cycle_wait_time;

#ifdef DTLS_ENABLED
  if (!sdk_dtls_configured)
  {
    *error = DTLS_NOT_INITIALIZED_ERR;
    return AT_SCHC_DTLS_ERROR;
  }
  // If the application requests another JOINDTLS, perform a new
  // join and a new handshake. So, reset our DTLS automaton.
  if (handshake_done())
  {
    reset_dtls_automaton();
  }
#endif

  if (join_flag)
  {
    // This is to avoid the reexecution of the lines below but trigger `join`
    // operation at the same time
    if (l2_join(l2_get_default_datarate(), &duty_cycle_wait_time) != L2_SUCCESS)
    {
      return AT_L2_ERROR;
    }
    else
    {
      return AT_OK;
    }
  }

  class = param[0];
#ifdef SCHC_CERTIFICATION_ENABLED
  // Use LORA technology to be compliant to RFC9011
  l2_set_technology(L2A_LORA);
#else
  l2_set_technology(class == 'A' ? L2A_DEFAULT_CLASS_A : L2A_DEFAULT);
#endif
  // Initialize SDK MGT layer.
  mgt_status_t status = fullsdk_mgt_init();
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  join_flag = true;

  return AT_OK;
}

at_status_t at_NetworkJoinStatus(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  uint8_t status;
  if (l2_get_join_status(&status) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  print_d(status);

  return AT_OK;
}

at_status_t at_SendBinary(const char *param, uint8_t *error)
{
  (void)error;
  const char *buf = param;
  unsigned char buf_size = strlen(param);
  uint32_t app_port;
  unsigned size = 0;
  char hex[2];
  uint8_t dr;

  // Look for colon after application port.
  uint16_t index = 1;
  while (true)
  {
    if (buf[index] == ':')
      break;
    index++;
    if (index >= buf_size)
    {
      AT_PRINTF("AT+SEND without the application port\n");
      return AT_PARAM_ERROR;
    }
  }
  if (!sscanf_10i(buf, index, &app_port))
  {
    AT_PRINTF("AT+SEND without the application port\n");
    return AT_PARAM_ERROR;
  }

  buf += index;
  buf++;
  buf_size -= index;
  buf_size--;

  uint32_t val;
  while ((size < LORAWAN_APP_DATA_BUFF_SIZE) && (buf_size > 1))
  {
    hex[0] = buf[size * 2];
    hex[1] = buf[size * 2 + 1];
    if (!sscanf_8x(hex, 2, &val))
    {
      return AT_PARAM_ERROR;
    }
    app_data.buffer[size] = (uint8_t)val;
    size++;
    buf_size -= 2;
  }
  if (buf_size != 0)
  {
    return AT_PARAM_ERROR;
  }

  app_data.size = size;
  app_data.port = app_port;
  app_data.type = l2_get_message_type();
  l2_get_dr(&dr);

  uint32_t duty_cycle_wait_time;
  l2_redirect_next_mcps_confirm_event(transmission_result_received);
  if (l2_send(app_data, dr, &duty_cycle_wait_time))
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_Send(const char *param, uint8_t *error)
{
  (void)error;
  const char *buf = param;
  unsigned char buf_size = strlen(param);
  uint32_t app_port;
  uint8_t dr;

  // Look for colon after application port.
  uint16_t index = 1;
  while (true)
  {
    if (buf[index] == ':')
      break;
    index++;
    if (index >= buf_size)
    {
      AT_PRINTF("AT+SEND without the application port\n");
      return AT_PARAM_ERROR;
    }
  }
  if (!sscanf_10i(buf, index, &app_port))
  {
    AT_PRINTF("AT+SEND without the application port\n");
    return AT_PARAM_ERROR;
  }

  buf += index;
  buf++;
  buf_size -= index;
  buf_size--;

  if (buf_size > LORAWAN_APP_DATA_BUFF_SIZE)
  {
    buf_size = LORAWAN_APP_DATA_BUFF_SIZE;
  }
  memcpy(app_data.buffer, (uint8_t *)buf, buf_size);
  app_data.size = buf_size;
  app_data.port = app_port;
  app_data.type = l2_get_message_type();
  l2_get_dr(&dr);

  uint32_t duty_cycle_wait_time;
  l2_redirect_next_mcps_confirm_event(transmission_result_received);
  if (l2_send(app_data, dr, &duty_cycle_wait_time) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  return AT_OK;
}

at_status_t at_version_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  AT_PRINTF("ATModem " APP_VERSION " %s\n", l2_get_version());

  return AT_OK;
}

at_status_t at_ack_set(const char *param, uint8_t *error)
{
  (void)error;
  l2_set_message_type(param[0] - '0');

  return AT_OK;
}

at_status_t at_ack_get(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  print_d(l2_get_message_type());

  return AT_OK;
}

at_status_t at_set_platform_traces(const char *param, uint8_t *error)
{
  (void)error;
  platform_enable_traces(param[0] == '1');

  return AT_OK;
}

at_status_t at_get_snr(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  print_d(l2_get_last_snr());
  return AT_OK;
}

at_status_t at_get_rssi(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  print_d(l2_get_last_rssi());
  return AT_OK;
}

at_status_t at_modem_echo(const char *param, uint8_t *error)
{
  (void)error;
  if (param[0] == '1' || param[0] == '0')
  {
    ctx_enable_echo_flag = (param[0] == '1');
    return AT_OK;
  }
  return AT_PARAM_ERROR;
}