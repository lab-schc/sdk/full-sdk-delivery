/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Jerome Elias jerome.elias@ackl.io
 *
 * @brief Implementation of AT SCHC commands for template
 * using SID/YANG registry management
 */

#include "at.h"
#include "atmgt.h"

#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "fullsdkschc.h"
#include "helpers.h"

#ifndef TPL_MEMORY_SIZE
#define TPL_MEMORY_SIZE 2500
#endif

static uint8_t tpl_memory_area[TPL_MEMORY_SIZE];

at_status_t at_get_tpl_id(const char *param, uint8_t *error)
{
  uint8_t id;
  (void)param;

  mgt_status_t status = mgt_get_template_id(&id);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  AT_PRINTF("%d\n", id);
  return AT_OK;
}

at_status_t at_get_nb_tpl_params(const char *param, uint8_t *error)
{
  uint8_t nb;
  (void)param;

  mgt_status_t status = mgt_get_nb_template_params(&nb);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  AT_PRINTF("%d\n", nb);
  return AT_OK;
}

at_status_t at_set_tpl(const char *param, uint8_t *error)
{
  (void)error;
  const char *buf = param;
  uint16_t buf_size = strlen(param);

  // Initialize synchronization buffers
  mgt_status_t status = at_sync_tpl_initialize();
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  if (buf_size == 0 || !load_rules(buf, buf_size))
  {
    return AT_PARAM_ERROR;
  }

  // Provision template
  status = mgt_provision_template((const uint8_t *)buf, buf_size / 2);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  return AT_OK;
}

at_status_t at_clear_tpl_params(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  // We just reset the template parameters
  mgt_status_t status = mgt_reset_template_params();
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  return AT_OK;
}

mgt_status_t at_sync_tpl_initialize(void)
{
  return mgt_sync_bootstrap_initialize(tpl_memory_area, sizeof(tpl_memory_area));
}

at_status_t at_sync_tpl_params(const char *param, uint8_t *error)
{
  if (!join_flag)
  {
    *error = MGT_L2A_ERR;
    return AT_SCHC_MGT_ERROR;
  }

  const char *buf = param;
  uint16_t buf_size = strlen(param);
  uint32_t val;
  uint16_t comma_index;

  // Extract retransmission_delay
  // Find next comma.
  comma_index = 1;
  while (true)
  {
    if (buf[comma_index] == ',')
      break;
    comma_index++;
    if (comma_index >= buf_size)
    {
      return AT_PARAM_ERROR;
    }
  }
  if (!sscanf_10i(buf, comma_index, &val))
  {
    return AT_PARAM_ERROR;
  }

  uint32_t retransmission_delay = val;

  buf += comma_index;
  buf_size -= comma_index;
  buf++;
  buf_size--;

  // Extract max_retrans
  if (!sscanf_10i(buf, strlen(buf), &val))
  {
    return AT_PARAM_ERROR;
  }
  if (val > 255)
  {
    return AT_PARAM_ERROR;
  }

  uint8_t max_retrans = (uint8_t)val;

  // Synchronization process for template parameters
  *error = mgt_sync_params(retransmission_delay, max_retrans);
  return *error == MGT_SUCCESS ? AT_OK : AT_SCHC_MGT_ERROR;
}