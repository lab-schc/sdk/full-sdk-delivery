/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Flavien Moullec flavien@ackl.io
 *
 * @brief Implementation of AT SCHC commands without template management
 */

#include "atmgt.h"
#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "fullsdkschc.h"
#include "helpers.h"

at_status_t at_set_tpl(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
}

at_status_t at_clear_tpl_params(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
}

at_status_t at_get_tpl_id(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
}

at_status_t at_get_nb_tpl_params(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
}

at_status_t at_set_payload_rules(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
}

at_status_t at_fullsdk_frag_set(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
}

at_status_t at_fullsdk_qos_set(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
}