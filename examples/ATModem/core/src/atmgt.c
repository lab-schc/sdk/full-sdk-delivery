/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Flavien Moullec flavien@ackl.io
 *
 * @brief Implementation of AT SCHC commands for Management layer
 */

#include "atmgt.h"
#ifdef DTLS_ENABLED
#include "fullsdkdtls.h"
#include "atdtls.h"
#endif
#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "fullsdkschc.h"
#include "helpers.h"

#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned

// DTLS
#ifdef DTLS_ENABLED
#define INTERNAL_MGT_DTLS_DATA_LEN MGT_DTLS_DATA_LEN
#define PSK_ID_MAX_LEN 64
#define PSK_MAX_LEN 255
#else 
#define INTERNAL_MGT_DTLS_DATA_LEN 0
#endif

// Template SID
#ifdef SCHC_TEMPLATE_SID
#define INTERNAL_MGT_SYNC_SID_SIZE MGT_SYNC_SID_SIZE
#else 
#define INTERNAL_MGT_SYNC_SID_SIZE 0
#endif

// Memory block size provided to mgt_initialize
#define MEM_BLOCK_SIZE                                            \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u + \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u + \
   INTERNAL_MGT_DTLS_DATA_LEN + \
   INTERNAL_MGT_SYNC_SID_SIZE)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

static bool mgt_process_request = false;

#ifdef DTLS_ENABLED
static uint8_t psk_id[PSK_ID_MAX_LEN] = {0};
static uint8_t psk[PSK_MAX_LEN] = {0};
static dtls_options_t dtls_opt = {NULL, NULL, PSK_MAX_LEN, PSK_ID_MAX_LEN, 3, 30000};
#endif

// Timers
#if defined(DTLS_ENABLED)
static timer_obj_t sdk_timers[7];
#else
static timer_obj_t sdk_timers[6];
#endif

extern char class;

// Callbacks for timers
static void sdk_timer_1_event(void *context)
{
  // To prevent Unused Parameter warning.
  (void)(context);
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  // To prevent Unused Parameter warning.
  (void)(context);
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  // To prevent Unused Parameter warning.
  (void)context;
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  // To prevent Unused Parameter warning.
  (void)(context);
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  // To prevent Unused Parameter warning.
  (void)(context);
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  // To prevent Unused Parameter warning.
  (void)(context);
  mgt_timer_timeout(5);
}

#if defined(DTLS_ENABLED)
static void sdk_timer_7_event(void *context)
{
  // To prevent Unused Parameter warning.
  (void)(context);
  mgt_timer_timeout(6);
}
#endif

static void mgt_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&sdk_timers[id], duration);
  platform_timer_start(&sdk_timers[id]);
}

static void mgt_stop_timer(uint8_t id)
{
  platform_timer_stop(&sdk_timers[id]);
}

static void mgt_bootstrap_result(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    AT_PRINTF("+SYNCOK\n");
  }
  else
  {
    AT_PRINTF("+SYNCFAIL,%d\n", state);
  }
}

static void mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    AT_PRINTF("+JOINED\n");
    // Class can only be set after joining network
    l2_set_class(class);
#ifdef DTLS_ENABLED
    // Start DTLS handshake.
    handshake_requested = true;
    dtls_processing_required = true;
#endif
  }
}

static void mgt_processing_required(void)
{
  mgt_process_request = true;
}

bool fullsdk_is_busy(void)
{
  return mgt_process_request;
}

mgt_status_t fullsdk_mgt_init(void)
{
  platform_timer_add(&sdk_timers[0], timer_id++, sdk_timer_1_event, NULL);
  platform_timer_add(&sdk_timers[1], timer_id++, sdk_timer_2_event, NULL);
  platform_timer_add(&sdk_timers[2], timer_id++, sdk_timer_3_event, NULL);
  platform_timer_add(&sdk_timers[3], timer_id++, sdk_timer_4_event, NULL);
  platform_timer_add(&sdk_timers[4], timer_id++, sdk_timer_5_event, NULL);
  platform_timer_add(&sdk_timers[5], timer_id++, sdk_timer_6_event, NULL);
#if defined(DTLS_ENABLED)
  platform_timer_add(&sdk_timers[6], timer_id++, sdk_timer_7_event, NULL);
#endif

  // Initialize MGT layer.
  mgt_callbacks_t callbacks = {mgt_processing_required, mgt_connectivity_state,
                               mgt_start_timer, mgt_stop_timer,
                               mgt_bootstrap_result};
  mgt_status_t status =
      mgt_initialize(&callbacks, mgt_mem_block, MEM_BLOCK_SIZE, MAX_MTU_SIZE,
                     MAX_PAYLOAD_SIZE);
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  return MGT_SUCCESS;
}

void fullsdk_process(void)
{
  if (!join_flag)
  {
    return;
  }

  if (mgt_process_request)
  {
    mgt_process_request = false;
    mgt_status_t mgt_status = mgt_process();
    if (mgt_status != MGT_SUCCESS)
    {
      AT_PRINTF("=> fullsdk_process - error in mgt_process:%d\n\r", mgt_status);
    }
  }
}

at_status_t at_fullsdk_version_get(const char *param, uint8_t *error)
{
  // To prevent Unused Parameter warning.
  (void)(param);
  (void)(error);
  AT_PRINTF("%s\n", mgt_get_version());
  return AT_OK;
}

at_status_t at_set_fullsdk_api(const char *param, uint8_t *error)
{
  // To prevent Unused Parameter warning.
  (void)(error);
  const char *buf = param;
  uint16_t buf_size = strlen(param);

  if (buf_size != 1)
  {
    return AT_PARAM_ERROR;
  }

  // Have we to switch on DTG or NET layer ?
  switch (buf[0])
  {
  case 'N':
    if (at_active_api != AT_NONE_API)
    {
      if (join_flag && (at_active_api != AT_NET_API))
      {
        return AT_OPERATION_NOT_ALLOWED;
      }
    }
    at_active_api = AT_NET_API;
    break;
  case 'D':
    if (at_active_api != AT_NONE_API)
    {
      if (join_flag && (at_active_api != AT_DTG_API))
      {
        return AT_OPERATION_NOT_ALLOWED;
      }
    }
    at_active_api = AT_DTG_API;
    break;
  default:
    return AT_PARAM_ERROR;
  }

  return AT_OK;
}

at_status_t at_set_template_params(const char *param, uint8_t *error)
{
  const char *buf = param;
  uint16_t buf_size = strlen(param);
  uint32_t val;
  uint16_t index;
  uint16_t comma_index;
  char hex[2];
  uint32_t hex_val;

  if (!join_flag)
  {
    *error = MGT_L2A_ERR;
    return AT_SCHC_MGT_ERROR;
  }

  while (buf_size > 1)
  {
    // Extract indexes
    // Find next comma or end of command.
    comma_index = 1;
    while (true)
    {
      // || operator guarantees left-to-right evaluation. This prevents from
      // a potential buffer overflow.
      if ((comma_index >= buf_size) || (buf[comma_index] == ','))
      {
        // We have a (final or not) parameter value.
        if (!sscanf_10i(buf, comma_index, &val))
        {
          return AT_PARAM_ERROR;
        }
        break;
      }
      comma_index++;
    }
    if (val > 65535)
    {
      return AT_PARAM_ERROR;
    }

    index = (uint16_t)val;

    buf += comma_index;
    buf_size -= comma_index;
    buf++;
    buf_size--;

    // Extract parameter and set the template parameter
    uint8_t size = 0;

    while (buf_size > 1 && buf[2 * size] != ',')
    {
      hex[0] = buf[size * 2];
      hex[1] = buf[size * 2 + 1];

      if (!sscanf_8x(hex, 2, &hex_val))
      {
        return AT_PARAM_ERROR;
      }
      ((uint8_t *)buf)[size] = (uint8_t)hex_val;

      size++;
      buf_size -= 2;
    }

    mgt_status_t status = mgt_set_template_param(index, (const uint8_t *)buf, size);
    if (status != MGT_SUCCESS)
    {
      *error = status;
      return AT_SCHC_MGT_ERROR;
    }

    buf += 2 * size;

    // No more parameters
    if (buf_size == 0)
    {
      return AT_OK;
    }

    // We expect a new index, parameter tuple
    if (buf[0] != ',')
    {
      return AT_PARAM_ERROR;
    }

    buf++;
    buf_size--;
  }

  return AT_OK;
}

at_status_t at_get_template_params(const char *param, uint8_t *error)
{
  const char *buf = param;
  uint16_t buf_size = strlen(param);
  uint32_t val;
  uint8_t index;
  uint16_t comma_index;
  uint8_t *value;
  uint16_t value_length;

  while (buf_size > 0)
  {
    // Extract indexes
    // Find next comma or end of command.
    comma_index = 1;
    while (true)
    {
      // || operator guarantees left-to-right evaluation. This prevents from
      // a potential buffer overflow.
      if ((comma_index >= buf_size) || (buf[comma_index] == ','))
      {
        // We have a (final or not) parameter index.
        if (!sscanf_10i(buf, comma_index, &val))
        {
          return AT_PARAM_ERROR;
        }
        break;
      }
      comma_index++;
    }
    if (val > 255)
    {
      return AT_PARAM_ERROR;
    }

    index = (uint8_t)val;

    buf += comma_index;
    buf_size -= comma_index;

    mgt_status_t status = mgt_get_template_param(index, &value, &value_length);
    if (status != MGT_SUCCESS)
    {
      *error = status;
      return AT_SCHC_MGT_ERROR;
    }

    AT_PRINTF("%d,", index);
    for (uint16_t i = 0; i < value_length; i++)
    {
      AT_PRINTF("%02X", value[i]);
    }
    AT_PRINTF("\n");

    // No more parameters
    if (buf_size == 0)
    {
      return AT_OK;
    }

    // We expect a new index, parameter tuple
    if (buf[0] != ',')
    {
      return AT_PARAM_ERROR;
    }

    buf++;
    buf_size--;
  }

  return AT_OK;
}

at_status_t at_sync(const char *param, uint8_t *error)
{
  if (!join_flag)
  {
    *error = MGT_L2A_ERR;
    return AT_SCHC_MGT_ERROR;
  }

  const char *buf = param;
  uint16_t buf_size = strlen(param);
  uint32_t val;
  uint16_t comma_index;

  // Extract retransmission_delay
  // Find next comma.
  comma_index = 1;
  while (true)
  {
    if (buf[comma_index] == ',')
      break;
    comma_index++;
    if (comma_index >= buf_size)
    {
      return AT_PARAM_ERROR;
    }
  }
  if (!sscanf_10i(buf, comma_index, &val))
  {
    return AT_PARAM_ERROR;
  }

#ifdef SCHC_TEMPLATE_SID
  // Initialize the synchronization buffers.
  // Must be called before mgt_sync_bootstrap() function.
  mgt_status_t status = at_sync_tpl_initialize();
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }
#endif

  uint32_t retransmission_delay = val;

  buf += comma_index;
  buf_size -= comma_index;
  buf++;
  buf_size--;

  // Extract max_retrans
  if (!sscanf_10i(buf, strlen(buf), &val))
  {
    return AT_PARAM_ERROR;
  }
  if (val > 255)
  {
    return AT_PARAM_ERROR;
  }

  uint8_t max_retrans = (uint8_t)val;

  // Synchronization process
  *error = mgt_sync_bootstrap(retransmission_delay, max_retrans);
  return *error == MGT_SUCCESS ? AT_OK : AT_SCHC_MGT_ERROR;
}

at_status_t at_schc_conf_poll(const char *param, uint8_t *error)
{
  const char *buf = param;
  uint16_t buf_size = strlen(param);

  char class;
  if (l2_get_class(&class) != L2_SUCCESS)
  {
    return AT_L2_ERROR;
  }

  if (!join_flag || class != 'A')
  {
    *error = MGT_L2A_ERR;
    return AT_SCHC_MGT_ERROR;
  }

  if (buf_size < 3)
  {
    *error = AT_PARAM_ERROR;
    return AT_PARAM_ERROR;
  }

  bool enable = false;
  bool suspend_uplinks = false;

  switch (buf[0])
  {
  case '0':
    enable = false;
    break;
  case '1':
    enable = true;
    break;
  default:
    return AT_PARAM_ERROR;
  }

  buf++;
  buf_size--;
  if (buf[0] != ',')
  {
    return AT_PARAM_ERROR;
  }
  buf++;
  buf_size--;

  switch (buf[0])
  {
  case '0':
    suspend_uplinks = false;
    break;
  case '1':
    suspend_uplinks = true;
    break;
  default:
    return AT_PARAM_ERROR;
  }
  schc_set_polling_status(enable, suspend_uplinks);
  return AT_OK;
}

at_status_t at_schc_trigger_poll(const char *param, uint8_t *error)
{
  // To prevent Unused Parameter warning.
  (void)(param);
  if (!join_flag)
  {
    *error = MGT_L2A_ERR;
    return AT_SCHC_MGT_ERROR;
  }

  if ((*error = mgt_trigger_polling()) != MGT_SUCCESS)
  {
    return AT_SCHC_MGT_ERROR;
  }

  return AT_OK;
}

at_status_t at_fullsdk_ip_get(const char *param, uint8_t *error)
{
  (void)param;

  uint8_t ip_address[IP_ADDRESS_MAX_LENGTH_BYTES];

  mgt_status_t status =
      mgt_get_ipv6_address(ip_address, IP_ADDRESS_MAX_LENGTH_BYTES);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  print_16_02x_2(ip_address);

  return AT_OK;
}

at_status_t at_fullsdk_qos_select(const char *param, uint8_t *error)
{
#ifdef QOS_ENABLED
  const char *buf = param;
  uint16_t buf_size = strlen(param);

  if (buf_size != 1)
  {
    return AT_PARAM_ERROR;
  }

  quality_of_service_t qos;

  switch (buf[0])
  {
  case '1':
    qos = QOS_NO_RELIABILTY;
    break;
  case '2':
    qos = QOS_NETWORK_RELIABILITY;
    break;
  case '3':
    qos = QOS_SYSTEMATIC_REPORT;
    break;
  default:
    return AT_PARAM_ERROR;
  }

  mgt_status_t status = mgt_set_qos(qos);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  return AT_OK;
#else
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
#endif
}

#ifdef DTLS_ENABLED
void dtls_handshake_result(dtls_status_t status)
{
    handshake_result_received = true;
    handshake_result = status;
    dtls_processing_required = true;
}

// Beware: we support key data of up to 255 bytes only.
static at_status_t set_key_data(const char *param, uint16_t param_length,
                                uint16_t max_length, uint8_t *key_data, 
                                uint8_t *key_data_length)
{
  // First, check length of param.
  if (param_length > max_length * 2)
  {
    // Key data is too long.
    return AT_PARAM_ERROR;
  }
  // Then, check that we have something that can be transformed
  // into bytes.
  if (param_length % 2 != 0)
  {
    return AT_PARAM_ERROR;
  }
  if (param_length == 0)
  {
    return AT_PARAM_ERROR;
  }
  int8_t hex_value;
  uint8_t hex_byte = 0;
  uint16_t param_index = 0;
  uint8_t id_index = 0;
  while (true)
  {
    hex_value = ascii_to_hex(param[param_index]);
    if (hex_value == -1)
    {
      return AT_PARAM_ERROR;
    }
    hex_byte = hex_value << 4;
    param_index++;
    hex_value = ascii_to_hex(param[param_index]);
    if (hex_value == -1)
    {
      return AT_PARAM_ERROR;
    }
    hex_byte += hex_value;
    param_index++;
    key_data[id_index] = hex_byte;
    id_index++;
    if (param_index >= param_length)
    {
      *key_data_length = id_index;
      return AT_OK;
    }
  }
  return AT_OK;
}

at_status_t at_fullsdk_dtls_conf_set(const char *param, uint8_t *error)
{
  // To prevent the compiler from screaming for unused parameter.
  (void)error;

  uint16_t comma_index;
  const uint16_t param_size = strlen(param);
  uint16_t param_start_index;
  at_status_t at_status;

  // Get PSK identity.
  comma_index = 1;
  while (true)
  {
    if (comma_index >= param_size)
    {
      return AT_PARAM_ERROR;
    }
    if (param[comma_index] == ',')
    {
      // We have the PSK identity.
      // Remember pointer to PSK ID.
      dtls_opt.psk_id = psk_id;
      // Extract PSK identity.
      at_status = set_key_data(param, comma_index, PSK_ID_MAX_LEN,
                               (uint8_t *)dtls_opt.psk_id,
                               &dtls_opt.psk_id_size);
      if (at_status != AT_OK)
      {
        return at_status;
      }
      break;
    }
    comma_index++;
  }

  // Get PSK.
  param_start_index = comma_index + 1;
  comma_index += 2;
  while (true)
  {
    if (comma_index >= param_size)
    {
      return AT_PARAM_ERROR;
    }
    if (param[comma_index] == ',')
    {
      // We have the PSK.
      // Remember pointer to PSK.
      dtls_opt.psk = psk;
      // Extract PSK.
      at_status = set_key_data(param + param_start_index,
                               comma_index - param_start_index,
                               PSK_MAX_LEN, (uint8_t *)dtls_opt.psk, 
                               &dtls_opt.psk_size);
      if (at_status != AT_OK)
      {
        return at_status;
      }
      break;
    }
    comma_index++;
  }

  // Get handshake try number.
  param_start_index = comma_index + 1;
  comma_index += 2;
  uint32_t val;
  while (true)
  {
    if (comma_index >= param_size)
    {
      return AT_PARAM_ERROR;
    }
    if (param[comma_index] == ',')
    {
      if (!sscanf_10i(param + param_start_index, comma_index - param_start_index, 
                      &val))
      {
        return AT_PARAM_ERROR;
      }
      dtls_opt.nbr_try = val;
      break;
    }
    comma_index++;
  }
  // Get handshake timeout.
  param_start_index = comma_index + 1;
  // No more comma.
  comma_index = param_size;
  if (param_start_index >= param_size)
  {
    return AT_PARAM_ERROR;
  }
  if (!sscanf_10i(param + param_start_index, comma_index - param_start_index, 
                  &val))
  {
    return AT_PARAM_ERROR;
  }
  // Value must be translated into ms.
  dtls_opt.handshake_timeout = val * 1000;

  dtls_callbacks_t dtls_cb = {dtls_handshake_result, platform_entropy_hardware_poll};
  dtls_status_t dtls_status = dtls_set_options(&dtls_cb, &dtls_opt);
  if (dtls_status != DTLS_SUCCESS)
  {
    return AT_PARAM_ERROR;
  }
  sdk_dtls_configured = true;

  return AT_OK;
}
#endif