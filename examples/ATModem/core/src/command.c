/**
 ******************************************************************************
 * @file    command.c
 * @author  MCD Application Team
 * @brief   main command driver dedicated to command AT
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2018 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "at.h"
#ifdef SCHC_CERTIFICATION_ENABLED
#include "atcert.h"
#endif
#include "atdtg.h"
#include "atmgt.h"
#include "atnet.h"

/* Private define ------------------------------------------------------------*/
// We support a payload of 1024 bytes in binary, so 2048 bytes in ASCII.
// Moreover there is at least 62 bytes of AT-command overhead. and the rest is
// the safety margin.
#define CMD_SIZE (2 * MAX_PAYLOAD_SIZE + 256)
// CIRC_BUFF_SIZE must not be greater than 254, as pointers to the
// buffer and number of bytes are uint8_t (widx, ridx and charCount),
// and charCount is compared against CIRC_BUFF_SIZE + 1.
#define CIRC_BUFF_SIZE 24
#define HELP_DISPLAY_FLUSH_DELAY 100
#define AT_ERROR_RX_CHAR 0x01

/* AT Command strings. Commands start with AT */
#define AT_RESET "Z"
#define AT_DEUI "+DEUI"
#define AT_DADDR "+DADDR"
#define AT_APPKEY "+APPKEY"
#define AT_NWKSKEY "+NWKSKEY"
#define AT_APPSKEY "+APPSKEY"
#define AT_JOINEUI                                                                   \
        "+APPEUI" /*to match with V1.0.x specification- For V1.1.x "+APPEUI" will be \
                     replaced by "+JOINEUI"*/
#define AT_ADR "+ADR"
#define AT_TXP "+TXP"
#define AT_DR "+DR"
#define AT_DCS "+DCS"
#define AT_PNM "+PNM"
#define AT_RX2FQ "+RX2FQ"
#define AT_RX2DR "+RX2DR"
#define AT_RX1DL "+RX1DL"
#define AT_RX2DL "+RX2DL"
#define AT_SYSRXERR "+SYSRXERR"
#define AT_JN1DL "+JN1DL"
#define AT_JN2DL "+JN2DL"
#define AT_NJM "+NJM"
#define AT_NWKID "+NWKID"
#define AT_CLASS "+CLASS"
#define AT_REGION "+REGION"
#ifdef DTLS_ENABLED
#define AT_JOINDTLS "+JOINDTLS"
#else
#define AT_JOIN "+JOIN"
#endif
#define AT_NJS "+NJS"
#define AT_SENDB "+SENDB"
#define AT_SEND "+SEND"
#define AT_VER "+VER"
#define AT_CFM "+CFM"
#define AT_CFS "+CFS"
#define AT_SNR "+SNR"
#define AT_RSSI "+RSSI"
#define AT_BAT "+BAT"
#define AT_TRSSI "+TRSSI"
#define AT_TTONE "+TTONE"
#define AT_TTLRA "+TTLRA"
#define AT_TRLRA "+TRLRA"
#define AT_TCONF "+TCONF"
#define AT_TOFF "+TOFF"
#define AT_CERTIF "+CERTIF=LORAWAN"
#define AT_PGSLOT "+PGSLOT"
#define AT_BFREQ "+BFREQ"
#define AT_BTIME "+BTIME"
#define AT_BGW "+BGW"
#define AT_LTIME "+LTIME"
#define AT_TRACE "+TRACE"
#define AT_ECHO "E"

// SCHC AT commands
#define AT_FSDKVER "+SCHC=VERSION"
#define AT_SCHC_API "+SCHC=API"
#define AT_SCHC_TPL_SET "+SCHC=TPL,SET"
#define AT_SCHC_TPL_GET_ID "+SCHC=TPL,GETID"
#define AT_SCHC_TPLPARAMS_CLR "+SCHC=TPLPARAMS,CLR"
#define AT_SCHC_TPLPARAMS_SET "+SCHC=TPLPARAMS,SET"
#define AT_SCHC_TPLPARAMS_GET "+SCHC=TPLPARAMS,GET"
#define AT_SCHC_TPLPARAMS_NB "+SCHC=TPLPARAMS,NB"
#ifdef SCHC_TEMPLATE_SID
#define AT_SCHC_SYNC_TPL_PARAMS "+SCHC=TPLPARAMS,SYNC"
#else
#define AT_SCHC_TPL_PAYLOAD_SET "+SCHC=TPLPAYLOAD,SET"
#define AT_SCHC_FRAG_SET "+SCHC=FRAG,SET"     // Set fragmentation profile (CBOR)
#define AT_SCHC_QOS_SET "+SCHC=QOS,SET"       // Set QoS classes (CBOR)
#define AT_SCHC_QOS_SELECT "+SCHC=QOS,SELECT" // Select QoS class (CBOR)
#endif
#define AT_SCHC_SYNC "+SCHC=SYNC"
#define AT_SCHC_CONF_POLL "+SCHC=CONF_POLL"
#define AT_SCHC_TRIGGER_POLL "+SCHC=TRG_POLL"
#define AT_GETIP "+SCHC=IP"
#ifdef DTLS_ENABLED
#define AT_SCHC_DTLSCONF_SET "+SCHC=DTLSCONF,SET"
#endif

/**
 * @brief  Array corresponding to the description of each possible AT Error
 */
static const char *const at_status_description[] = {
    "OK\n",                     /* AT_OK */
    "AT_ERROR\n",               /* AT_ERROR */
    "AT_PARAM_ERROR\n",         /* AT_PARAM_ERROR */
    "AT_TEST_PARAM_OVERFLOW\n", /* AT_TEST_PARAM_OVERFLOW */
    "AT_RX_ERROR\n",            /* AT_RX_ERROR */
    "AT_L2_ERROR\n",            /* AT_L2_ERROR */
    "AT_NOT_IMPLEMENTED\n",     /* AT_NOT_IMPLEMENTED */
    "AT_PLATFORM_ERROR\n",      /* AT_PLATFORM_ERROR */
    "AT_OPERATION_NOT_ALLOWED", /* AT_OPERATION_NOT_ALLOWED error */
    "AT_ERROR,1",               /* AT MGT error */
    "AT_ERROR,2",               /* AT DTG error */
    "AT_ERROR,3",               /* AT NET error */
    "AT_ERROR,4",               /* AT CERT error */
    "AT_ERROR,5",               /* AT DTLS error */
    "error unknown\n",          /* AT_MAX */
};

static void com_error(at_status_t status, uint8_t error)
{
        if (status > AT_MAX)
        {
                status = AT_MAX;
        }

        if (status < AT_SCHC_MGT_ERROR || status == AT_MAX)
        {
                // No secondary error code
                AT_PRINTF("%s\n", at_status_description[status]);
                return;
        }

        AT_PRINTF("%s,%u\n", at_status_description[status], error);
}

/**
 * @brief  Array of all supported AT Commands
 */
static const at_command_t at_command[] = {
    {
        .string = AT_RESET,
        .size_string = sizeof(AT_RESET) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_RESET ": Trig a reset of the MCU\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_reset,
    },

#ifndef NO_KEY_ADDR_EUI
    {
        .string = AT_DEUI,
        .size_string = sizeof(AT_DEUI) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_DEUI ": Get or Set the Device EUI\n",
        .help_param = "",
#endif
        .get = at_DevEUI_get,
        .set = at_DevEUI_set,
        .run = at_return_error,
    },
#endif

#ifndef NO_KEY_ADDR_EUI
    {
        .string = AT_DADDR,
        .size_string = sizeof(AT_DADDR) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_DADDR ": Get or Set the Device address\n",
        .help_param = "",
#endif
        .get = at_DevAddr_get,
        .set = at_DevAddr_set,
        .run = at_return_error,
    },
#endif

#ifndef NO_KEY_ADDR_EUI
    {
        .string = AT_APPKEY,
        .size_string = sizeof(AT_APPKEY) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_APPKEY ": Get or Set the Application Key\n",
        .help_param = "",
#endif
        .get = at_AppKey_get,
        .set = at_AppKey_set,
        .run = at_return_error,
    },
#endif

#ifndef NO_KEY_ADDR_EUI
    {
        .string = AT_NWKSKEY,
        .size_string = sizeof(AT_NWKSKEY) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_NWKSKEY ": Set the Network Session Key\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_NwkSKey_set,
        .run = at_return_error,
    },
#endif

#ifndef NO_KEY_ADDR_EUI
    {
        .string = AT_APPSKEY,
        .size_string = sizeof(AT_APPSKEY) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_APPSKEY ": Set the Application Session Key\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_AppSKey_set,
        .run = at_return_error,
    },
#endif

#ifndef NO_KEY_ADDR_EUI
    {
        .string = AT_JOINEUI,
        .size_string = sizeof(AT_JOINEUI) - 1,
#ifndef NO_HELP
        /* .help_string = "AT"AT_JOINEUI ": Get or Set the Join Eui\n",*/ /* refer
                                                                               to comment in at.h file*/
        .help_string = "AT" AT_JOINEUI ": Get or Set the App Eui\n",
        .help_param = "",
#endif
        .get = at_JoinEUI_get,
        .set = at_JoinEUI_set,
        .run = at_return_error,
    },
#endif

    {
        .string = AT_ADR,
        .size_string = sizeof(AT_ADR) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_ADR ": Get or Set the Adaptive Data Rate "
                       "setting. (0: off, 1: on)\n",
        .help_param = "",
#endif
        .get = at_ADR_get,
        .set = at_ADR_set,
        .run = at_return_error,
    },

    {
        .string = AT_TXP,
        .size_string = sizeof(AT_TXP) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_TXP ": Get or Set the Transmit Power (0-5)\n",
        .help_param = "",
#endif
        .get = at_not_implemented_error,
        .set = at_not_implemented_error,
        .run = at_return_error,
    },

    {
        .string = AT_DR,
        .size_string = sizeof(AT_DR) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_DR
            ": Get or Set the Data Rate. (0-7 corresponding to DR_X)\n",
        .help_param = "",
#endif
        .get = at_DataRate_get,
        .set = at_DataRate_set,
        .run = at_return_error,
    },

    {
        .string = AT_DCS,
        .size_string = sizeof(AT_DCS) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_DCS ": Get or Set the ETSI Duty Cycle setting - "
                       "0=disable, 1=enable - Only for testing\n",
        .help_param = "",
#endif
        .get = at_DutyCycle_get,
        .set = at_DutyCycle_set,
        .run = at_return_error,
    },

    {
        .string = AT_PNM,
        .size_string = sizeof(AT_PNM) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_PNM
            ": Get or Set the public network mode. (0: off, 1: on)\n",
        .help_param = "",
#endif
        .get = at_PublicNetwork_get,
        .set = at_PublicNetwork_set,
        .run = at_return_error,
    },

    {
        .string = AT_RX2FQ,
        .size_string = sizeof(AT_RX2FQ) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_RX2FQ ": Get or Set the Rx2 window frequency\n",
        .help_param = "",
#endif
        .get = at_Rx2Frequency_get,
        .set = at_Rx2Frequency_set,
        .run = at_return_error,
    },

    {
        .string = AT_RX2DR,
        .size_string = sizeof(AT_RX2DR) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_RX2DR ": Get or Set the Rx2 window data rate "
                       "(0-7 corresponding to DR_X)\n",
        .help_param = "",
#endif
        .get = at_Rx2DataRate_get,
        .set = at_Rx2DataRate_set,
        .run = at_return_error,
    },

    {
        .string = AT_RX1DL,
        .size_string = sizeof(AT_RX1DL) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_RX1DL ": Get or Set the delay between the end "
                       "of the Tx and the Rx Window 1 in ms\n",
        .help_param = "",
#endif
        .get = at_Rx1Delay_get,
        .set = at_Rx1Delay_set,
        .run = at_return_error,
    },

    {
        .string = AT_RX2DL,
        .size_string = sizeof(AT_RX2DL) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_RX2DL ": Get or Set the delay between the end "
                       "of the Tx and the Rx Window 2 in ms\n",
        .help_param = "",
#endif
        .get = at_Rx2Delay_get,
        .set = at_Rx2Delay_set,
        .run = at_return_error,
    },

    {
        .string = AT_SYSRXERR,
        .size_string = sizeof(AT_SYSRXERR) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_SYSRXERR ": Get or Set the system RX error in ms\n",
        .help_param = "",
#endif
        .get = at_SystemRxError_get,
        .set = at_SystemRxError_set,
        .run = at_return_error,
    },

    {
        .string = AT_JN1DL,
        .size_string = sizeof(AT_JN1DL) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_JN1DL ": Get or Set the Join Accept Delay between the end "
            "of the Tx and the Join Rx Window 1 in ms\n",
        .help_param = "",
#endif
        .get = at_JoinAcceptDelay1_get,
        .set = at_JoinAcceptDelay1_set,
        .run = at_return_error,
    },

    {
        .string = AT_JN2DL,
        .size_string = sizeof(AT_JN2DL) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_JN2DL ": Get or Set the Join Accept Delay between the end "
            "of the Tx and the Join Rx Window 2 in ms\n",
        .help_param = "",
#endif
        .get = at_JoinAcceptDelay2_get,
        .set = at_JoinAcceptDelay2_set,
        .run = at_return_error,
    },

    {
        .string = AT_NJM,
        .size_string = sizeof(AT_NJM) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_NJM ": Get or Set the Network Join Mode. (0: "
                       "NONE, 1: ABP, 2: OTAA)\n",
        .help_param = "",
#endif
        .get = at_NetworkJoinMode_get,
        .set = at_NetworkJoinMode_set,
        .run = at_return_error,
    },

#ifndef NO_KEY_ADDR_EUI
    {
        .string = AT_NWKID,
        .size_string = sizeof(AT_NWKID) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_NWKID ": Get or Set the Network ID\n",
        .help_param = "",
#endif
        .get = at_NetworkID_get,
        .set = at_NetworkID_set,
        .run = at_return_error,
    },
#endif

    {
        .string = AT_CLASS,
        .size_string = sizeof(AT_CLASS) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_CLASS ": Get or Set the Device Class\n",
        .help_param = "",
#endif
        .get = at_DeviceClass_get,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_REGION,
        .size_string = sizeof(AT_REGION) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_REGION ": Get or Set the active region\n",
        .help_param = "",
#endif
        .get = at_ActiveRegion_get,
        .set = at_ActiveRegion_set,
        .run = at_return_error,
    },

#ifdef DTLS_ENABLED
    {
        .string = AT_JOINDTLS,
        .size_string = sizeof(AT_JOINDTLS) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_JOINDTLS ": Join network and start DTLS\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_JoinDtls,
        .run = at_return_error,
    },
#else
    {
        .string = AT_JOIN,
        .size_string = sizeof(AT_JOIN) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_JOIN ": Join network\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_Join,
        .run = at_return_error,
    },
#endif

    {
        .string = AT_NJS,
        .size_string = sizeof(AT_NJS) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_NJS ": Get the join status\n",
        .help_param = "",
#endif
        .get = at_NetworkJoinStatus,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_SENDB,
        .size_string = sizeof(AT_SENDB) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_SENDB
            ": Send hexadecimal data along with the application port\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_SendBinary,
        .run = at_return_error,
    },

    {
        .string = AT_SEND,
        .size_string = sizeof(AT_SEND) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_SEND ": Send text data along with the application port\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_Send,
        .run = at_return_error,
    },

    {
        .string = AT_VER,
        .size_string = sizeof(AT_VER) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_VER ": Get the version of the ATModem application\n",
        .help_param = "",
#endif
        .get = at_version_get,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_CFM,
        .size_string = sizeof(AT_CFM) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_CFM ": Get or Set the confirmation mode (0-1)\n",
        .help_param = "",
#endif
        .get = at_ack_get,
        .set = at_ack_set,
        .run = at_return_error,
    },

    {
        .string = AT_CFS,
        .size_string = sizeof(AT_CFS) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_CFS ": Get confirmation status of the last AT+SEND (0-1)\n",
        .help_param = "",
#endif
        .get = at_not_implemented_error,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_SNR,
        .size_string = sizeof(AT_SNR) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_SNR ": Get the SNR of the last received packet\n",
        .help_param = "",
#endif
        .get = at_get_snr,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_RSSI,
        .size_string = sizeof(AT_RSSI) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_RSSI ": Get the RSSI of the last received packet\n",
        .help_param = "",
#endif
        .get = at_get_rssi,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_BAT,
        .size_string = sizeof(AT_BAT) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_BAT ": Get the battery level\n",
        .help_param = "",
#endif
        .get = at_not_implemented_error,
        .set = at_return_error,
        .run = at_return_error,
    },
    {
        .string = AT_TRSSI,
        .size_string = sizeof(AT_TRSSI) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_TRSSI ": Starts RF RSSI tone test\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_not_implemented_error,
    },

    {
        .string = AT_TTONE,
        .size_string = sizeof(AT_TTONE) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_TTONE ": Starts RF Tone test\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_not_implemented_error,
    },
    {
        .string = AT_TTLRA,
        .size_string = sizeof(AT_TTLRA) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_TTLRA ": Set Nb of packets sent with RF Tx LORA test\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_not_implemented_error,
        .run = at_return_error,
    },
    {
        .string = AT_TRLRA,
        .size_string = sizeof(AT_TRLRA) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_TRLRA ": Set Nb of packets received with RF Rx LORA test\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_not_implemented_error,
        .run = at_return_error,
    },
    {
        .string = AT_TCONF,
        .size_string = sizeof(AT_TCONF) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_TCONF ": Config LORA RF test "
            "[Freq]:[Power]:[Bandwith]:[SF]:4/[CR]:[Lna]:[PA Boost]\n",
        .help_param = "",
#endif
        .get = at_not_implemented_error,
        .set = at_not_implemented_error,
        .run = at_return_error,
    },
    {
        .string = AT_TOFF,
        .size_string = sizeof(AT_TOFF) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_TOFF ": Stops on-going RF test\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_not_implemented_error,
    },

    {
        .string = AT_CERTIF,
        .size_string = sizeof(AT_CERTIF) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_CERTIF ": Set the module in LoraWan Certification Mode\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_Certif,
    },

#ifdef LORAMAC_CLASSB_ENABLED
    {
        .string = AT_PGSLOT,
        .size_string = sizeof(AT_PGSLOT) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_PGSLOT ": Set or Get the unicast ping slot periodicity\n",
        .help_param = "",
#endif
        .get = at_not_implemented_error,
        .set = at_not_implemented_error,
        .run = at_return_error,
    },

    {
        .string = AT_BFREQ,
        .size_string = sizeof(AT_BFREQ) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_BFREQ ": Get the Beacon frequency\n",
        .help_param = "",
#endif
        .get = at_not_implemented_error,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_BTIME,
        .size_string = sizeof(AT_BTIME) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_BTIME ": Get the Beacon Time (GPS Epoch time)\n",
        .help_param = "",
#endif
        .get = at_not_implemented_error,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_BGW,
        .size_string = sizeof(AT_BGW) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_BGW ": Get the Gateway GPS coordinate, NetID and GwID\n",
        .help_param = "",
#endif
        .get = at_not_implemented_error,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_LTIME,
        .size_string = sizeof(AT_LTIME) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_LTIME ": Get the local time in UTC format\n",
        .help_param = "",
#endif
        .get = at_not_implemented_error,
        .set = at_return_error,
        .run = at_return_error,
    },
#endif /* LORAMAC_CLASSB_ENABLED */

#ifdef PLATFORM_TRACE_ENABLED
    {
        .string = AT_TRACE,
        .size_string = sizeof(AT_TRACE) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_TRACE ": Set whether platform traces are enabled or not\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_set_platform_traces,
        .run = at_return_error,
    },
#endif

    {
        .string = AT_FSDKVER,
        .size_string = sizeof(AT_FSDKVER) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_FSDKVER ": Prints the fullsdk version\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_fullsdk_version_get,
    },

    {
        .string = AT_SCHC_API,
        .size_string = sizeof(AT_SCHC_API) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_API ": Selects the API (N or D)\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_set_fullsdk_api,
        .run = at_return_error,
    },
    {
        .string = AT_SCHC_TPL_SET,
        .size_string = sizeof(AT_SCHC_TPL_SET) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_SCHC_TPL_SET ": Sets a template in the volatile memory\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_set_tpl,
        .run = at_return_error,
    },

#ifndef SCHC_TEMPLATE_SID
    {
        .string = AT_SCHC_TPL_PAYLOAD_SET,
        .size_string = sizeof(AT_SCHC_TPL_PAYLOAD_SET) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_TPL_PAYLOAD_SET
                       ": Loads payload compression rules in the memory\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_set_payload_rules,
        .run = at_return_error,
    },
#endif

    {
        .string = AT_SCHC_TPL_GET_ID,
        .size_string = sizeof(AT_SCHC_TPL_GET_ID) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_TPL_GET_ID
                       ": Returns the ID of the current template\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_get_tpl_id,
    },

    {
        .string = AT_SCHC_TPLPARAMS_NB,
        .size_string = sizeof(AT_SCHC_TPLPARAMS_NB) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_SCHC_TPLPARAMS_NB
            ": Returns the number of parameters of the current template\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_get_nb_tpl_params,
    },

    {
        .string = AT_SCHC_TPLPARAMS_SET,
        .size_string = sizeof(AT_SCHC_TPLPARAMS_SET) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_TPLPARAMS_SET
                       ": Set template parameters as index,value list\n",
        .help_param = "AT" AT_SCHC_TPLPARAMS_SET
                      ",0,<param_0>,...,<index_N>,<param_N>\n",
#endif
        .get = at_return_error,
        .set = at_set_template_params,
        .run = at_return_error,
    },
    {
        .string = AT_SCHC_TPLPARAMS_GET,
        .size_string = sizeof(AT_SCHC_TPLPARAMS_GET) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_TPLPARAMS_GET
                       ": Get template parameters as index,value list\n",
        .help_param = "AT" AT_SCHC_TPLPARAMS_GET ",<index>\n",
#endif
        .get = at_return_error,
        .set = at_get_template_params,
        .run = at_return_error,
    },

    {
        .string = AT_SCHC_TPLPARAMS_CLR,
        .size_string = sizeof(AT_SCHC_TPLPARAMS_CLR) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_TPLPARAMS_CLR
                       ": Clear the template parameters memory region\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_clear_tpl_params,
    },

#ifdef SCHC_TEMPLATE_SID
    {
        .string = AT_SCHC_SYNC_TPL_PARAMS,
        .size_string = sizeof(AT_SCHC_SYNC_TPL_PARAMS) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_SYNC_TPL_PARAMS
                       ": Starts synchronization procedure for template parameters\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_sync_tpl_params,
        .run = at_return_error,
    },
#endif

    {
        .string = AT_SCHC_SYNC,
        .size_string = sizeof(AT_SCHC_SYNC) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_SYNC ": Starts synchronization procedure\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_sync,
        .run = at_return_error,
    },

    {
        .string = AT_SCHC_CONF_POLL,
        .size_string = sizeof(AT_SCHC_CONF_POLL) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_SCHC_CONF_POLL ": Configures class A polling, 1st param: "
            "enable, 2nd param: suspend uplinks\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_schc_conf_poll,
        .run = at_return_error,
    },

    {
        .string = AT_SCHC_TRIGGER_POLL,
        .size_string = sizeof(AT_SCHC_TRIGGER_POLL) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_SCHC_TRIGGER_POLL
            ": Triggers the polling mechanism, sends an empty frame\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_schc_trigger_poll,
    },

    {
        .string = AT_GETIP,
        .size_string = sizeof(AT_GETIP) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_GETIP ": Prints the IP address of the device\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_fullsdk_ip_get,
    },

    {
        .string = AT_ECHO,
        .size_string = sizeof(AT_ECHO) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_ECHO ": Tuns OFF/ON the echo mode of the modem\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_modem_echo,
        .run = at_return_error,
    },

#ifndef SCHC_TEMPLATE_SID
    {
        .string = AT_SCHC_FRAG_SET,
        .size_string = sizeof(AT_SCHC_FRAG_SET) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_FRAG_SET ": Overwrite fragmentation profile\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_fullsdk_frag_set,
        .run = at_return_error,
    },
    {
        .string = AT_SCHC_QOS_SET,
        .size_string = sizeof(AT_SCHC_QOS_SET) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_QOS_SET ": Overwrite QoS classes\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_fullsdk_qos_set,
        .run = at_return_error,
    },

    {
        .string = AT_SCHC_QOS_SELECT,
        .size_string = sizeof(AT_SCHC_QOS_SELECT) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_QOS_SELECT ": Select QoS class (1: NoReliability - 2: NetworkReliability - 3: SystematicReport)\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_fullsdk_qos_select,
        .run = at_return_error,
    },
#endif

#ifdef DTLS_ENABLED
    {
        .string = AT_SCHC_DTLSCONF_SET,
        .size_string = sizeof(AT_SCHC_DTLSCONF_SET) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SCHC_DTLSCONF_SET ": Configure DTLS\n",
        .help_param = "AT" AT_SCHC_DTLSCONF_SET ",<PSK_ID>,<PSK_value>,<retry_number>,<timeout>",
#endif
        .get = at_return_error,
        .set = at_fullsdk_dtls_conf_set,
        .run = at_return_error,
    },
#endif
};

/* Private function prototypes -----------------------------------------------*/

/**
 * @brief  Parse a command and process it
 * @param  The command
 * @retval None
 */
static void parse_cmd(const char *cmd);

/* Exported functions
 * ---------------------------------------------------------*/
static void CMD_GetChar(uint8_t *rxChar);

static char circBuffer[CIRC_BUFF_SIZE];
static char command[CMD_SIZE];
static unsigned i = 0;
static uint8_t widx = 0;
static uint8_t ridx = 0;
static uint8_t charCount = 0;
static bool circBuffOverflow = false;

void CMD_Init(void)
{
        platform_set_uart_rx_callback(CMD_GetChar);
        widx = 0;
        ridx = 0;
        charCount = 0;
        i = 0;
        circBuffOverflow = false;
}

static void CMD_GetChar(uint8_t *rxChar)
{
        charCount++;
        if (charCount == (CIRC_BUFF_SIZE + 1))
        {
                circBuffOverflow = true;
                charCount--;
        }
        else
        {
                circBuffer[widx++] = *rxChar;
                if (widx == CIRC_BUFF_SIZE)
                {
                        widx = 0;
                }
        }
}

void CMD_Process(void)
{
        /* Process all commands */
        if (circBuffOverflow)
        {
                com_error(AT_TEST_PARAM_OVERFLOW, 0);
                /*Full flush in case of overflow */
                CRITICAL_SECTION_BEGIN();
                ridx = widx;
                charCount = 0;
                circBuffOverflow = false;
                CRITICAL_SECTION_END();
                i = 0;
        }

        while (charCount != 0)
        {
#if 0 /* echo On    */
  PRINT_AT("%c", circBuffer[ridx]);
#endif

                if (circBuffer[ridx] == AT_ERROR_RX_CHAR)
                {
                        ridx++;
                        if (ridx == CIRC_BUFF_SIZE)
                        {
                                ridx = 0;
                        }
                        CRITICAL_SECTION_BEGIN();
                        charCount--;
                        CRITICAL_SECTION_END();
                        com_error(AT_RX_ERROR, 0);
                        i = 0;
                }
                else if ((circBuffer[ridx] == '\r') || (circBuffer[ridx] == '\n'))
                {
                        ridx++;
                        if (ridx == CIRC_BUFF_SIZE)
                        {
                                ridx = 0;
                        }
                        CRITICAL_SECTION_BEGIN();
                        charCount--;
                        CRITICAL_SECTION_END();

                        if (i != 0)
                        {
                                command[i] = '\0';
                                parse_cmd(command);
                                i = 0;
                        }
                }
                else if (i == (CMD_SIZE - 1))
                {
                        i = 0;
                        com_error(AT_TEST_PARAM_OVERFLOW, 0);
                }
                else
                {
                        command[i++] = circBuffer[ridx++];
                        if (ridx == CIRC_BUFF_SIZE)
                        {
                                ridx = 0;
                        }
                        CRITICAL_SECTION_BEGIN();
                        charCount--;
                        CRITICAL_SECTION_END();
                }
        }
}

static bool process_at_cmd(const char *cmd, const at_command_t *command,
                           at_status_t *status, uint8_t *error)
{
        if (strncmp(cmd, command->string, command->size_string) != 0)
        {
                // Command doesn't match.
                return false;
        }

        /* point to the string after the command to parse it */
        cmd += command->size_string;

        /* parse after the command */
        switch (cmd[0])
        {
        case '\0': /* nothing after the command */
                *status = command->run(cmd, error);
                break;
        case ',': /* SCHC extension commands */
                *status = command->set(cmd + 1, error);
                break;
        case '=':
                if ((cmd[1] == '?') && (cmd[2] == '\0'))
                {
                        *status = command->get(cmd + 1, error);
                }
                else
                {
                        *status = command->set(cmd + 1, error);
                }
                break;
        case '?':
#ifndef NO_HELP
                AT_PRINTF(command->help_string);
                AT_PRINTF(command->help_param);
#endif
                *status = AT_OK;
                break;
        default:
                if (cmd[1] == '\0' && cmd[0] >= '0' && cmd[0] <= '9')
                {
                        *status = command->set(cmd, error); // ATE command
                }
        }

        return true;
}

static void parse_cmd(const char *cmd)
{
        at_status_t status = AT_OK;
        uint8_t error = 0;
        bool found = false;
        uint16_t i;

        if (get_enable_echo_flag())
        {
                AT_PRINTF("%s\n", cmd); // send back the command to the controlling application
        }

        if ((cmd[0] != 'A') || (cmd[1] != 'T'))
        {
                status = AT_ERROR;
        }
        else if (cmd[2] == '\0')
        {
                /* status = AT_OK; */
        }
        else if (cmd[2] == '?')
        {
#ifndef NO_HELP
                AT_PRINTF("AT+<CMD>?        : Help on <CMD>\n"
                          "AT+<CMD>         : Run <CMD>\n"
                          "AT+<CMD>=<value> : Set the value\n"
                          "AT+<CMD>=?       : Get the value\n");
                // L2/MGT help.
                for (i = 0; i < (sizeof(at_command) / sizeof(at_command_t)); i++)
                {
                        AT_PRINTF(at_command[i].help_string);
                }
                if (at_active_api == AT_DTG_API)
                {
                        // DTG API help.
                        for (i = 0; i < (sizeof(at_dtg_api_command) / sizeof(at_command_t)); i++)
                        {
                                AT_PRINTF(at_dtg_api_command[i].help_string);
                        }
                }
                else
                {
                        // NET API help.
                        for (i = 0; i < (sizeof(at_net_api_command) / sizeof(at_command_t)); i++)
                        {
                                AT_PRINTF(at_net_api_command[i].help_string);
                        }
                }
#ifdef SCHC_CERTIFICATION_ENABLED
                // SCHC Certification app help.
                for (i = 0; i < (sizeof(at_cert_api_command) / sizeof(at_command_t)); i++)
                {
                        AT_PRINTF(at_cert_api_command[i].help_string);
                }
#endif

                /* Wait for the message queue to be flushed in order
                   not to disturb following com_error() display */
                platform_delay_ms(HELP_DISPLAY_FLUSH_DELAY);
#endif
        }
        else
        {
                // Check standard AT commands.
                status = AT_ERROR;
                cmd += 2;

                for (i = 0; i < (sizeof(at_command) / sizeof(at_command_t)); i++)
                {
                        found = process_at_cmd(cmd, &at_command[i], &status, &error);
                        if (found)
                        {
                                break;
                        }
                }
                if (at_active_api == AT_DTG_API)
                {
                        // Check DTG API AT commands.
                        for (i = 0; i < (sizeof(at_dtg_api_command) / sizeof(at_command_t)); i++)
                        {
                                found = process_at_cmd(cmd, &at_dtg_api_command[i], &status, &error);
                                if (found)
                                {
                                        break;
                                }
                        }
                }
                else
                {
                        // Check NET API AT commands.
                        for (i = 0; i < (sizeof(at_net_api_command) / sizeof(at_command_t)); i++)
                        {
                                found = process_at_cmd(cmd, &at_net_api_command[i], &status, &error);
                                if (found)
                                {
                                        break;
                                }
                        }
                }
#ifdef SCHC_CERTIFICATION_ENABLED
                // SCHC Certification app help.
                for (i = 0; i < (sizeof(at_cert_api_command) / sizeof(at_command_t)); i++)
                {
                        found = process_at_cmd(cmd, &at_cert_api_command[i], &status, &error);
                        if (found)
                        {
                                break;
                        }
                }
#endif
        }

        com_error(status, error);
        error = 0;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF
 * FILE****/
