/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Flavien Moullec flavien@ackl.io
 * @author Jerome Elias jerome.elias@ackl.io
 *
 * @brief Implementation of AT SCHC commands for template management
 */

#include "atmgt.h"
#ifdef SCHC_CERTIFICATION_ENABLED
#include "atmgtcert.h"
#endif

#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "fullsdkschc.h"
#include "helpers.h"

// TODO: put formula of required memory size in function of a given template
// size
#ifndef TPL_MEMORY_SIZE
#define TPL_MEMORY_SIZE 1000
#endif

#ifndef PAYLOAD_MEMORY_SIZE
#define PAYLOAD_MEMORY_SIZE 1500
#endif

#ifdef SCHC_CERTIFICATION_ENABLED
uint8_t tpl_memory_area[(TEMPLATE_BUFFER_SIZE > TPL_MEMORY_SIZE ? TEMPLATE_BUFFER_SIZE : TPL_MEMORY_SIZE)];
// If SCHC Certification is not embedded
#else
static uint8_t tpl_memory_area[TPL_MEMORY_SIZE];
#endif

static uint8_t payload_memory_area[PAYLOAD_MEMORY_SIZE];

at_status_t at_get_tpl_id(const char *param, uint8_t *error)
{
  uint8_t id;
  (void)param;

  mgt_status_t status = mgt_get_template_id(&id);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  AT_PRINTF("%d\n", id);
  return AT_OK;
}

at_status_t at_get_nb_tpl_params(const char *param, uint8_t *error)
{
  uint8_t nb;
  (void)param;

  mgt_status_t status = mgt_get_nb_template_params(&nb);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  AT_PRINTF("%d\n", nb);
  return AT_OK;
}

at_status_t at_set_tpl(const char *param, uint8_t *error)
{
  (void)error;
  const char *buf = param;
  uint16_t buf_size = strlen(param);

#ifdef SCHC_CERTIFICATION_ENABLED
  // If SCHC Certification template is in use
  if (mgt_cert_is_initialized())
  {
    return AT_CERT_ERROR;
  }
#endif

  if (buf_size == 0 || !load_rules(buf, buf_size))
  {
    return AT_PARAM_ERROR;
  }

  // Provision template
  mgt_status_t status = mgt_provision_header_rules(
      (const uint8_t *)buf, buf_size / 2, tpl_memory_area, TPL_MEMORY_SIZE);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  return AT_OK;
}

at_status_t at_clear_tpl_params(const char *param, uint8_t *error)
{
  (void)param;
  (void)error;
  // We just reset the template parameters
  mgt_status_t status = mgt_reset_template_params();
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  return AT_OK;
}

at_status_t at_set_payload_rules(const char *param, uint8_t *error)
{
  (void)error;
  const char *buf = param;
  uint16_t buf_size = strlen(param);

  if (buf_size == 0 || !load_rules(buf, buf_size))
  {
    return AT_PARAM_ERROR;
  }

  // Provision template
  mgt_status_t status = mgt_provision_payload_rules(
      (const uint8_t *)buf, buf_size / 2, payload_memory_area, PAYLOAD_MEMORY_SIZE);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }

  return AT_OK;
}

at_status_t at_fullsdk_frag_set(const char *param, uint8_t *error)
{
  const char *buf = param;
  uint16_t buf_size = strlen(param);

  if (buf_size == 0 || !load_rules(buf, buf_size))
  {
    return AT_PARAM_ERROR;
  }

  mgt_status_t status = mgt_provision_frag_profile((uint8_t *)buf, buf_size / 2);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }
  return AT_OK;
}

at_status_t at_fullsdk_qos_set(const char *param, uint8_t *error)
{
#ifdef QOS_ENABLED
  const char *buf = param;
  uint16_t buf_size = strlen(param);

  if (buf_size == 0 || !load_rules(buf, buf_size))
  {
    return AT_PARAM_ERROR;
  }
  mgt_status_t status = mgt_provision_qos((const uint8_t *)buf, buf_size / 2);
  if (status != MGT_SUCCESS)
  {
    *error = status;
    return AT_SCHC_MGT_ERROR;
  }
  return AT_OK;
#else
  (void)param;
  (void)error;
  return AT_NOT_IMPLEMENTED;
#endif
}