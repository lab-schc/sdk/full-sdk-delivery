/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Jerome Elias jerome.elias@ackl.io
 *
 * @brief Implementation of AT SCHC commands for Management layer
 * for SCHC Certification app.
 */

#include "atmgtcert.h"
#include "atdtg.h"

static bool mgt_cert_process_request = false;
static bool mgt_cert_app_is_initialized = false;

// Timers
static timer_obj_t schc_cert_timers[2];

// Callbacks for timers
static void cert_timer_1_event(void *context)
{
  (void)context;
  mgt_cert_timer_timeout(0);
}

static void cert_timer_2_event(void *context)
{
  (void)context;
  mgt_cert_timer_timeout(1);
}

static void mgt_cert_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&schc_cert_timers[id], duration);
  platform_timer_start(&schc_cert_timers[id]);
}

static void mgt_cert_stop_timer(uint8_t id)
{
  platform_timer_stop(&schc_cert_timers[id]);
}

static void mgt_cert_processing_required(void)
{
  mgt_cert_process_request = true;
}

mgt_status_t fullsdk_mgt_cert_init(void)
{
  if (!join_flag)
  {
    return MGT_CERT_ERR;
  }

  if (mgt_cert_app_is_initialized)
  {
    return MGT_CERT_ERR;
  }

  platform_timer_add(&schc_cert_timers[0], timer_id++, cert_timer_1_event, NULL);
  platform_timer_add(&schc_cert_timers[1], timer_id++, cert_timer_2_event, NULL);

  // Initialize SCHC certification application.
  mgt_cert_callbacks_t mgt_cert_callbacks = {
      mgt_cert_processing_required,
      mgt_cert_start_timer,
      mgt_cert_stop_timer,
  };

  // Get RX and TX buffers from the first DTG socket
  uint16_t rx_buf_len = 0;
  uint8_t* rx_buf = at_dtg_get_first_socket_rx_buf(&rx_buf_len);

  uint16_t tx_buf_len = 0;
  uint8_t* tx_buf = at_dtg_get_first_socket_tx_buf(&tx_buf_len);
  if (rx_buf == NULL || tx_buf == NULL)
  {
    return MGT_CERT_ERR;
  }

  // Initialize SDK MGT layer.
  mgt_status_t status =
      mgt_cert_initialize(&mgt_cert_callbacks, tpl_memory_area, TEMPLATE_BUFFER_SIZE,
                          rx_buf, rx_buf_len, tx_buf, tx_buf_len);
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  // At this stage, SCHC Certification app is ready to be tested as soon
  // as device has joined LNS
  mgt_cert_app_is_initialized = true;

  return MGT_SUCCESS;
}

void fullsdk_cert_process(void)
{
  if (!join_flag)
  {
    return;
  }

  if (!mgt_cert_app_is_initialized)
  {
    return;
  }

  if (mgt_cert_process_request)
  {
    mgt_cert_process_request = false;
    mgt_status_t mgt_status = mgt_cert_process();
    if (mgt_status != MGT_SUCCESS)
    {
      AT_PRINTF("=> fullsdk_cert_process - error in mgt_cert_process:%d\n\r", mgt_status);
    }
  }
}

bool mgt_cert_is_initialized(void)
{
  return mgt_cert_app_is_initialized;
}

void mgt_cert_reset_init_status(void)
{
  mgt_cert_app_is_initialized = false;
}