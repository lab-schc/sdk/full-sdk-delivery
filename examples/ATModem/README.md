# ATModem application

## Overview

The module consists of an AT-command stack built on the top of Acklio FullSDK in order to provide integrators the ability to interact with the Acklio FullSDK using the widely used AT-command interface.

The Network API is used to send and receive IP packets in raw mode.

The Datagram SDK API is used by this application to send and receive application data.

The API selection is done at compilation time.

## Build

It is not necessary to set `FULLSDK_DTG_MAX_SOCKET` environment variable in the make command as the default value is set to 3.

In the root folder:

```sh
cmake -S . -B ./build/ -DAPP_NAME=ATModem -DPLATFORM=<platform> -DL2_STACK=semtech -DFULLSDK_VERSION=<sdk_version> -DAPP_VERSION=<app_version> \
-DLORAWAN_DEVEUI=<dev_eui> -DLORAWAN_APPEUI=<app_eui> -DLORAWAN_APPKEY=<app_key> -DEXTENSION_API=template \
-DTEMPLATE_ID=<template_id> -DFULLSDK_DTG_MAX_SOCKET=<nb_socket> && make -C ./build
```

Example:

```sh
cmake -S . -B ./build/ -DAPP_NAME=ATModem -DPLATFORM=B-L072Z-LRWAN1 -DL2_STACK=semtech -DFULLSDK_VERSION=3.0.0 -DAPP_VERSION=4.0.0 -DTOOLCHAIN=gcc-arm-none -DTARGET=m0plus \
-DLORAWAN_DEVEUI=1111111111111111 -DLORAWAN_APPEUI=1111111111111111 -DLORAWAN_APPKEY=11111111111111111111111111111111 \
-DEXTENSION_API=template -DTEMPLATE_ID=ipv6udp && make -C ./build
```

Another example:

```sh
cmake -S . -B ./build/ -DFULLSDK_DTG_MAX_SOCKET=1 -DEXTENSION_API=template -DTEMPLATE_ID=dynamic -DPLATFORM_TRACE_ENABLED=ON \
-DSYNC_ENABLED=1 -DAPP_VERSION=4.0.0 -DFULLSDK_VERSION=4.0.0 -DAPP_NAME=ATModem -DPLATFORM=STM32L476RG-Nucleo \
-DTOOLCHAIN=gcc-arm-none -DTARGET=m4 \
-DL2_STACK=lbm -DLORAWAN_DEVEUI=ffff0000000011ee -DLORAWAN_APPEUI=ffff0000000011ee \
-DLORAWAN_APPKEY=ffff0000000011eeffff0000000011ee -DFULLSDK_TPLMGT_MEMORY_SIZE=2000 \
-DCRYPTO_KEY=xxx -DDEBUG_ENABLED=ON && make -C ./build
```

For LBM L2 stack, some trace messages can be enabled by setting the value of `HAL_DBG_TRACE` to `1` in the `platforms/STM32L476RG-Nucleo/lbm/flags.cmake` file.

## SCHC Certification app

This ATModem application can embed and run an internal application for SCHC certification test over LoRaWAN.
To be able to do that, after the join, the user can write `AT+CERT=SET,1` command to activate SCHC certification mode. As soon as the device is in SCHC Certification mode, user should not interact anymore with the NET and DTG API.

Disclaimer : For this application, L2A technology is set to `L2A_LORA` to be RFC9011 compliant for fragmentation.

After being certified, user can go back to normal mode using the unset command `AT+CERT=SET,0`. Then:
1/ Reload his own CBOR compression rules.
2/ Reinitialize DTG or Network layer

## AT commands manual

### Common AT commands

#### List of AT commands

You can access to the list of available commands using:

```sh
AT?
```

#### SDK version

```sh
AT+SCHC=VERSION
3.0.0
OK
```

#### Application and L2 stack version

```sh
AT+VER=?
```

Example:

```sh
AT+VER=?
ATModem 1.2 Semtech 4.4.7
OK
```

#### Changing active region

Available regions are: AS923, AU915, CN470, CN779, EU433, EU868, KR920, IN865, US915 and RU864

EU868 is the default region on device startup.

```sh
AT+REGION=<region>
```

Example:

```sh
AT+REGION=AS923
OK
```

#### Displaying active region

```sh
AT+REGION=?
```

Example:

```sh
EU868
OK
```

#### Joining network

```sh
AT+JOIN=<class>
```

Example:

```sh
AT+JOIN=A
OK
+JOINED
```

#### Class A polling mechanism

To configure the class A polling:

```sh
AT+SCHC=CONF_POLL,<enable>,<suspend_uplinks>
```

Example:

```sh
AT+SCHC=CONF_POLL,1,1
```

To trigger polling:

```sh
AT+SCHC=TRG_POLL
```

#### Getting and setting system maximum rx error

Sometimes, the target boards oscillator is not very precise which is causing some rx (join accept or downlink) events lost. To increase the error margin (in milliseconds), system maximum error should be tweaked. Usually values 20 to 50 would be enough (default value is 10).

To get the system maximum rx error:

```sh
AT+SYSRXERR=?
10
OK
```

To set the system maximum rx error:

```sh
AT+SYSRXERR=45
OK
```

#### Enabling platform traces

In case troubleshooting is needed some traces can be activated using the following command:

```sh
AT+TRACE=1
OK
```

They can be disabled using:

```sh
AT+TRACE=0
OK
```

### Configure maximum payload size

By default, the maximum payload size supported over the serial is set to 1024 bytes.

It is possible to select a different value at compilation time using `FULLSDK_MAX_PAYLOAD_SIZE`.

### Template management and configuration

Note: only available when `TEMPLATE_ID` is set to `dynamic` at compilation time. Also it is possible to configure the memory area set for template provisioning using `FULLSDK_TPLMGT_MEMORY_SIZE`.

#### Setting a template

```sh
AT+SCHC=TPL,SET,<template>
```

Where:

* `template`: content of the `rules_debug.txt` file downloaded from IPCore interface.

Example (IPv6/UDP template):

```sh
AT+SCHC=TPL,SET,B1008418960186010101010101818302181C8E880C040102000081410602880D080101000081410002880E140101000081440001234502880F100101000081420000028810080101000081411102881108010100008141400288121840010200008100028813184001020000810102881418400102000081020288151840010200008103028816100102000081040288171001020000810502881818100101000081420000028818191001010000814200000224B2
OK
```

#### Getting template ID

```sh
AT+SCHC=TPL,GETID
```

Example (IPv6/UDP template):

```sh
AT+SCHC=TPL,GETID
1
OK
```

#### Setting template parameters

Template parameters are documented [here](https://gitlab.com/acklio/dev-sdk/full-sdk/-/tree/master/templates/catalog).

Parameters indexes are 0-based and the number of parameters depend on the template selected.

```sh
AT+SCHC=TPLPARAMS,SET,<param_index_1>,<param_1>,<param_index_2>,<param_2>,etc
```

Example (IPv6/UDP template):

```sh
AT+SCHC=TPLPARAMS,SET,0,ABCD000000000000,1,000000000000EFEF,2,2001000000000000,3,0000000000000001,4,1234,5,5678
OK
```

#### Getting template parameters

```sh
AT+SCHC=TPLPARAMS,GET,<param_index_1>,<param_index_2>,,etc
```

Example (IPv6/UDP template):

```sh
AT+SCHC=TPLPARAMS,GET,0,1,2,3,4,5
0,2121000000000000
1,0000000000000003
2,ABCD000000000000
3,0000000000000001
4,AE65
5,AE46
OK
```

#### Clearing the template parameters memory region

Note: only available when `TEMPLATE_ID` is set to `dynamic` at compilation time.

When you set a template parameter, it is stored in a memory area and never erased even if you redefine it.  
You can choose to erase all the previously set template parameters to reuse the memory area, using the following command:

```sh
AT+SCHC=TPLPARAMS,CLR
```

Example:

```sh
AT+SCHC=TPLPARAMS,CLR
OK
```

#### Setting payload compression pattern

```sh
AT+SCHC=TPLPAYLOAD,SET,<payload pattern>
```

Where:

* `payload pattern`: content of the `payload_pattern_debug.txt` file downloaded from IPCore interface.

Example (Gurux demo pattern):

```sh
AT+SCHC=TPLPAYLOAD,SET,620284189618698086830901888818371820010200008144C001C100028818370802000002824103410F02881837100302000081420000028818370804000002824128416002881837080500000283410041064109028818371006020000814200FF02881837080703040381410002881837080802000081410002830903848818371830010200008146C401C100020202881837080201000181410002881837080302000081410002881837100400000283420A0042160042160902830904848818371830010200008146C401C10009060288183718200203100381440000000002881837080302000081410002881837080400000282410041FF02830905848818371890010200008152C401C1000207110211101202F41105110811028818370802000002824101410202881837080302000081411102881837080400000282410041010283090681881837190760010200008158ECC401C1000103020412000F110209060000280000FF0202010B02030F0116010002030F0216010002030F0316010002030F0416010002030F0516010002030F0616010002030F0716010002030F0816010002030F0916010002030F0A16010002030F0B160100010602020F01160002020F02160002020F03160002020F04160002020F05160002020F0616000204120003110009060000600900FF0202010302030F0116010002030F0216010002030F03160100010102020F0116000204120003110009060000600600FF0202010302030F0116010002030F0216010002030F03160100010102020F01160002830907828818371820010200008144C401C100028818370002010001814100020E91
OK
```

### Quality of Service (QoS) functionality

#### To enjoy QoS function, "QOS_ENABLED=1" env. variable must be set during compilation. It is mandatory to embed this ATModem application in a constraint board as B-L072Z-LRWAN1.

In order to load the QoS fragmentation profiles, run the following command:

```sh
AT+SCHC=QOS,SET,<cbor_data>
```

where `<cbor_data>` is the content of the `quality_of_service.txt` file from the zip file downloaded from the associated SCHC template in IPCore.

Then, before sending an uplink packet, select the QoS class to be used with the following command:

```sh
AT+SCHC=QOS,SELECT,<mode>
```

where `<mode>` can have the following values:

* `1`: No Reliability
* `2`: Network Reliability
* `3`: Systematic Report

### Synchronization service

Note: only available when `SYNC_ENABLED` is on at compilation time.

If the IPCore misses parameters from the device it will send a sync request upon receiving an uplink from the device.

If the device misses parameters from the IPCore the synchronisation procedure can be started using the following command:

```sh
AT+SCHC=SYNC,<retransmission_delay>,<max_retrans>
```

Where:

* `retransmission_delay` is the delay between each request retransmission (in ms)
* `max_retrans` is the maximum number of request retransmission before considering the synchronization has failed

Example:

```sh
AT+SCHC=SYNC,60000,3
```

### API selection

It is possible to select the interface to be used with the following command:

```sh
AT+SCHC=API,<API>
```

where the `API` is one of the following values:

* `N` to select the network API (default)
* `D` to select the datagram API

### Network (NET) interface AT commands

#### Data transmission

```sh
AT+SCHC=SENDB,<ip_packet>
```

Examples:

```sh
AT+SCHC=SENDB,60000000004E117FABCD000000000000000000000000EFEF20010000000000000000000000000001123456780014C0D90001000100010004C001C200
OK
+SENDOK
```

#### Data reception

```sh
+RECVOK,<payload>
```

where the payload is in hexadecimal.

```sh
+RECVOK,60000000000E11FF20010000000000000000000000000001ABCD000000000000000000000000EFEF56781234000EA52761636B6C696F
```

### Datagram (DTG) interface AT commands

#### Socket creation

```sh
AT+SCHC=SOCKET
```

This function returns a socket id

Example:

```sh
AT+SCHC=SOCKET
0
```

#### Getting device IPv6 address

```sh
AT+SCHC=IP
```

Example:

```sh
AT+SCHC=IP
0000:0000:0000:0000:9215:7e9d:96d6:3c74
OK
```

#### Socket binding

```sh
AT+SCHC=BIND,<socket_id>,<host_ip>,<host_port>
```

* socket_id: socket id
* host_ip: IP address of the device
* host port: UDP port of the device

Example:

```sh
AT+SCHC=BIND,0,0001:0002:0003:0004:5000:6000:7000:8000,12345
OK
```

#### IP packet transmission

```sh
AT+SCHC=SEND,<socket_id>,<remote_ip>,<remote_port>,<payload>
```

or

```sh
AT+SCHC=SENDB,<socket_id>,<remote_ip>,<remote_port>,<payload>
```

* socket_id: socket id
* remote_ip IP address of the remote application
* remote_port UDP port of the remote application
* payload: payload (AT+SCHC=SEND: ascii format, AT+SCHC+SENDB: hexadecimal format)

Examples:

```sh
AT+SCHC=SEND,0,2001:0db8:85a3:08d3:1319:8a2e:0370:7348,65535,benimismimartugamursilbenackliodacalisiyorumyas
OK
+SENDOK,0
```

```sh
AT+SCHC=SENDB,0,2001:0db8:85a3:08d3:1319:8a2e:0370:7348,22222,1a1b1c1d1e
OK
+SENDOK,0
```

#### IP packet reception

Downlink data are received asynchrously by the module with this format:

```sh
+RECVOK,<socket_id>:<payload>
```

where the payload is in hexadecimal.

```sh
+RECVOK,0:776F616961636B6C696F
```

#### Closing a socket

```sh
AT+SCHC=CLOSE,<socket_id>
```

Example:

```sh
AT+SCHC=CLOSE,0
OK
```

### DTLS

ATModem can be built to configure and use the DTLS layer of FullSDK, when FullSDK is provided with it.

ATModem must be built with the `DTLS_ENABLED=1` option. The `MBEDTLS_IO_BUFF_SIZE` option must be used to specify the size of each of the two in and out buffers that will be used by MbedTLS. This size must allow to transport application payloads, while being less than the size granted by the application to MbedTLS, i.e. `MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE`.

The `AT+JOIN` command is replaced by the `AT+JOINDTLS` command, and the `AT+SCHC=DTLSCONF,SET` must be used before this command, in order to configure DTLS.