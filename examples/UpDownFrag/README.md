# UpDownFrag

## Description

This sample application demonstrates how to send and receive messages longer than LoRaWAN MTU.

On cloud side, a simple application sends a message to the device, on a periodic basis. Every message contains a timestamp, a sequence number and some text. Format of this message is:

```
nnn - hh:mm:ss - This is Ground Control to Device. You've really made the grade.
And the papers want to know which stack you run. Now it's time to reply if you dare.
```

`nnn` is the sequence number, with three figures. It starts from `000` and is incremented with each new downlink message. When it reaches `999`, it wraps around to `000`.

`hh:mm:ss` is the timestamp.

The message contains 165 characters.

On device side, *UpDownFrag* application joins the Acklio LoRaWAN network. It then waits for some time (currently: 30 s) and then sends an uplink message. Then it waits for downlink messages with the above format. For each received message, it writes its sequence number and timestamp to the serial-over-USB link.

It accumulates three successive sequence numbers and then sends them in an uplink message, formatted as below:

```
Device to Ground Control. Here are the sequence numbers I received:
nnn nnn nnn - Tell IPCore I love her very much, she knows.
```

The message contains 126 characters.

The device is switched to class C, currently required for downlink fragmentation session.

A message is written to the serial-over-USB link on a periodic basis, to let the user check that the application is alive.

## Build

### Configuration

Set device EUI, application EUI and application key using environment variables:

* `LORAWAN_DEVEUI`
* `LORAWAN_APPEUI`
* `LORAWAN_APPKEY`

To set data rate to adaptive or to a fixed value, use the `FULLSDK_DR` environment variable:

* set it to `ADR` to set adaptive data rate
* set it to a value between `0` and `5` to set data rate from DR0 to DR5

By default, the data rate is set to adaptive.

The build command has to be run from top directory (`full-sdk-delivery` if default name is not modified). The sections below provide the command for different targets.

### ST NUCLEO-L476RG board

The build command is:

```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_NAME=UpDownFrag -DL2_STACK=semtech -DPLATFORM=STM32L476RG-Nucleo -DTOOLCHAIN=gcc-arm-none -DTARGET=m4 \
-DFULLSDK_DR=<dataRate> -DLORAWAN_DEVEUI=<deviceEUI> -DLORAWAN_APPEUI=<applicationEUI> -DLORAWAN_APPKEY=<applicationKey> && make -C ./build
```

### x86_64

To build the example for x86_64 using Rising HF L2 stack, following build command can be used:

```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_NAME=UpDownFrag -DPLATFORM=linux -DL2_STACK=at_risinghf \
-DLORAWAN_DEVEUI=<deviceEUI> -DLORAWAN_APPEUI=<applicationEUI> -DLORAWAN_APPKEY=<applicationKey> && make -C ./build
```

### LNS

Create a **Web Socket server** connector.

Create a device profile with one application, with **fport** set to **0** and **Type** set to **Bytes**, and activate the above connector.

Configure the device for **Class C** and set **RX Window** to **RX2**. Assign the above profile to the device.

### IPCore

A Predefined device template can be used: `DownlinkFragSampleApp`.

Create a new flow, assign it the LNS connector created above as the source. In device profile, set **Buffering** to **0** and **Device type** to **Class C**. Complement device profile information:

* **App port**: the UDP port of the cloud application. This port has to be used as the source port by the application
* **Dev port**: the port that will be used on the device
* **Ip6 app iid**: the IID of the cloud application IPv6 address. Start the IID with `::`
* **Ip6 app prefix**: the prefix of the cloud application IPv6 address. End the prefix with `::`

Add the device to the list of profile devices.

## Run

## Device application

To run the device application:

* connect the ST board to a USB port of a PC
* trace messages can be displayed with a terminal emulator. After a few seconds, one of them informs that the device has joined the LoRaWAN network

## Cloud application

Download the NBA (network binding agent) package from IPCore website, and unzip its contents. The package is available from *Pools* page.

To run the cloud application:

* a computer connected to the Internet with IPv6 must be used. *Python3* must be available
* copy the `updownfragapp.py` file to the computer
* run the network binding agent:

```sh
sudo ./ipcore-nw-binding-agent-x86-v0.0.1 --agent-server-addr=ipcore.acklio.net:9995
```

* run the application:

```sh
python3 updownfragapp.py <deviceAddress> <devicePort> <applicationPort> <period>
```

`<deviceAddress>` is the IPv6 address of the device, as displayed in the device profile on IPCore.

`<devicePort>` is the device port, as defined on IPCore.

`<applicationPort>` is the application port, as defined on IPCore.

`<period>` is the period between two downlink transmissions, in seconds.

The application waits for `<period>` before sending the first message.
