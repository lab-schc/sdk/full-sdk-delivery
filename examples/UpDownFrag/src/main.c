/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Pascal Bodin - pascal@ackl.io
 *         Jerome Elias - jerome.elias@ackl.io
 *
 * Check README.md for the description of the application.
 *
 */

/* Includes ------------------------------------------------------------------*/
#include <ctype.h>
#include <stdint.h>
#include <string.h>

// For trace functions.
#include "fullsdktrace.h"
#include "time.h"

#include "fullsdkdtg.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "fullsdkstats.h"
#include "platform.h"
#include "fullsdkextapi.h"

#define APP_VERSION "1.4.0"

/**
 * To enable low power mode, we have to undefine here for the moment
 * because we disable the low power mode for all platforms by default
 * in ./l2/<l2_name>/flags.cmake. Once we enable by default then
 * this definition can be done inside platform makefile.
 */
#undef LOW_POWER_DISABLE

// Period to wait for before sending initial uplink message, in ms.
#define APP_INIT_MSG_PERIOD 30000

// Period between two alive messages, in ms.
#define APP_ALIVE_PERIOD 30000

// Maximum size of a downlink message.
#define APP_RECEIVE_MAX_LENGTH 512

// MAXIMUM size of an uplink message.
#define APP_SEND_MAX_LENGTH 256

// Assume (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE APP_RECEIVE_MAX_LENGTH
#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned
#define APP_TRANSMISSION_MAX_LENGTH MAX_PAYLOAD_SIZE

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                            \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u + \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

// Number of successive received messages before replying.
#define APP_REC_MSG_NB 3

// Number of caracters used to display message number.
#define APP_MSG_NB_CHAR_NB 4

// The number of parameters for this template is 6.
#define TEMPLATE_PARAM_NB 6

// Initial uplink message.
static const char APP_INIT_SEND_MSG[] = "Hello app, I'm here";
// First fixed part of uplink message.
static const char APP_SEND_MSG_1[] =
    "Device to Ground Control. Here are the sequence numbers I received: ";
// Second fixed part of uplink message.
static const char APP_SEND_MSG_2[] =
    " - Tell IPCore I love her very much, she knows.";

// Data rate: -1 to indicate ADR otherwise data rate (0 to 5) read
// from environment variable. Default is 3.
static int8_t datarate;

// Flag signaling that MGT layer requires processing.
static bool mgt_process_request = false;

// Flag signaling that the main task requires processing.
static bool app_process_request = false;

// Events handled by the main task.
static bool app_connectivity_event = false;
static bool app_reception_event = false;
static bool app_alive_timer_event = false;
static bool app_init_msg_timer_event = false;
static bool app_boostrap_event = false;

// Current state of application transmission state machine.
typedef enum
{
  TX_WAIT_JOIN,
  TX_WAIT_SYNC,
  TX_WAIT_IDLE
} appTxState_t;

static appTxState_t app_tx_current_state = TX_WAIT_JOIN;

// Timer for alive message.
static timer_obj_t app_alive_timer;

// Timer for initial uplink message.
static timer_obj_t app_init_msg_timer;

// SDK timers.
static timer_obj_t sdk_timers[6];

// Timer IDs (needed for vTAL)
static uint16_t timer_id = 0;

// Buffer for data returned by DTG layer.
static uint8_t reception_buffer[APP_RECEIVE_MAX_LENGTH];
static uint8_t transmission_buffer[APP_TRANSMISSION_MAX_LENGTH];

// Application reception buffer.
static uint16_t app_received_data_l;
static uint8_t app_received_data[APP_RECEIVE_MAX_LENGTH];
static dtg_status_t app_reception_status;

// Application transmission buffer.
static uint8_t app_send_data[APP_SEND_MAX_LENGTH];

// Array of 10 successive received messages.
static uint8_t received_sequences[APP_REC_MSG_NB];
// Index in above array of next message to be received.
static uint8_t rec_seq_index = 0;

// Device configuration.
static dtg_sockaddr_t socket_addr = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // Address.
    {0, 0}                                            // Port.
};
static dtg_sockaddr_t dest_addr = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // Address.
    {0, 0}                                            // Port.
};

static dtg_socket_t *socket = NULL;

// Declaration of functions.
inline uint8_t ascii_to_int(uint8_t c);
static void mgt_processing_required(void);
static void mgt_connectivity_state(mgt_status_t state);
static void app_main_task(void);
static void enqueue_sequence(void);
static bool check_message(void);
static void app_alive_timer_event_fct(void *context);
static void app_init_msg_timer_event_fct(void *context);
static void sdk_timer_1_event(void *context);
static void sdk_timer_2_event(void *context);
static void sdk_timer_3_event(void *context);
static void sdk_timer_4_event(void *context);
static void sdk_timer_5_event(void *context);
static void sdk_timer_6_event(void *context);

static void mgt_start_timer(uint8_t id, uint32_t duration);
static void mgt_stop_timer(uint8_t id);
static void mgt_bootstrap_result(mgt_status_t state);

static void dtg_trans_result(const dtg_socket_t *socket, dtg_status_t status,
                             uint16_t error);
static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                              uint16_t data_size,
                              const dtg_sockaddr_t *src_addr,
                              dtg_status_t status);

/**
 *
 */
int main(void)
{

  // Platform initialization.
  platform_hw_init();

  // Initialize system to enter sleep mode instead of standby mode.
  platform_configure_sleep_mode();

  PRINTNOW();
  PRINT_MSG("\nUpDownFrag - " APP_VERSION "\n");
  PRINT_MSG("app> FullSDK version: %s\n", mgt_get_version());

// Get datarate from environment variable.
#if defined(USE_ADR)
  datarate = -1;
#else
#if defined(APP_DR)
#if APP_DR < 0 || APP_DR > 5
  PRINTNOW();
  PRINT_MSG("app> Error: datarate must be in [0, 5]\n");
  datarate = 3;
#else
  datarate = APP_DR;
#endif
#else
  datarate = 3;
#endif
#endif
  if (datarate == -1)
  {
    PRINTNOW();
    PRINT_MSG("app> Datarate will be set to adaptive\n");
  }
  else
  {
    PRINTNOW();
    PRINT_MSG("app> Datarate will be set to DR%d\n", datarate);
  }

  platform_timer_add(&sdk_timers[0], timer_id++, sdk_timer_1_event, NULL);
  platform_timer_add(&sdk_timers[1], timer_id++, sdk_timer_2_event, NULL);
  platform_timer_add(&sdk_timers[2], timer_id++, sdk_timer_3_event, NULL);
  platform_timer_add(&sdk_timers[3], timer_id++, sdk_timer_4_event, NULL);
  platform_timer_add(&sdk_timers[4], timer_id++, sdk_timer_5_event, NULL);
  platform_timer_add(&sdk_timers[5], timer_id++, sdk_timer_6_event, NULL);

  // Enforce downlink L2A_DEFAULT technology in order to be compliant with template ID 1
  l2_set_technology(L2A_DEFAULT);

  // Initialize MGT layer.
  mgt_callbacks_t callbacks = {mgt_processing_required, mgt_connectivity_state,
                               mgt_start_timer, mgt_stop_timer, mgt_bootstrap_result};

  mgt_status_t mgt_status =
      mgt_initialize(&callbacks, mgt_mem_block, MEM_BLOCK_SIZE, MAX_MTU_SIZE,
                     MAX_PAYLOAD_SIZE);
  if (mgt_status != MGT_SUCCESS)
  {
    PRINTNOW();
    PRINT_MSG("sdk> Error in MGT initialization: %d\n", mgt_status);
    platform_error_handler();
  }

  while (true)
  {
    // Process MGT layer events.
    if (mgt_process_request)
    {
      mgt_process_request = false;
      mgt_status = mgt_process();
      if (mgt_status != MGT_SUCCESS)
      {
        PRINTNOW();
        PRINT_MSG("sdk> Error in mgt_process: %d\n", mgt_status);
      }
    }

    // Process main task events.
    if (app_process_request)
    {
      app_process_request = false;
      app_main_task();
    }

    // Sleep, if possible.
    BEGIN_CRITICAL_SECTION();
    // If an interrupt has occurred after DISABLE_IRQ, it is kept pending
    // and Cortex will not enter low power anyway.
    if ((!mgt_process_request) && (!app_process_request))
    {
#ifndef LOW_POWER_DISABLE
      platform_enter_low_power_ll();
#endif
    }
    END_CRITICAL_SECTION();
  }
}

/******************************************************************************
 * Functions.
 *****************************************************************************/

/**
 *
 */
static void app_main_task(void)
{
  switch (app_tx_current_state)
  {
  case TX_WAIT_JOIN:
    if (app_connectivity_event)
    {
      app_connectivity_event = false;
      PRINTNOW();
      PRINT_MSG("sdk> +JOINED\n");
      // Set data rate.
      if (datarate == -1)
      {
        l2_set_adr(true);
        PRINTNOW();
        PRINT_MSG("app> Data rate set to adaptive\n");
      }
      else
      {
        l2_set_dr(datarate);
        PRINTNOW();
        PRINT_MSG("app> Data rate set to DR%d\n", datarate);
      }
      // Request switch to class C.
      if (l2_set_class('C') != L2_SUCCESS)
      {
        PRINTNOW();
        PRINT_MSG("sdk> Error in class C switch request\n");
      }
      // Execute SYNC process.
      mgt_status_t mgt_status = mgt_sync_bootstrap(15000, 3);
      if (mgt_status != MGT_SUCCESS)
      {
        PRINT_MSG("sdk> error in mgt_sync_bootstrap: %d\n", mgt_status);
        platform_error_handler();
      }
      // Wait for SYNC response.
      app_tx_current_state = TX_WAIT_SYNC;
      break;
    }
    // At this stage, not a connectivity event. Error.
    PRINT_MSG("app> error - not a connectivity event\n\r");
    break;
  case TX_WAIT_SYNC:
    if (app_boostrap_event)
    {
      PRINT_MSG("sdk> +SYNC\n");
      app_boostrap_event = false;

      // Get all parameters from template
      int16_t param_value_len = 0;
      uint8_t *param_values[TEMPLATE_PARAM_NB];
      memset(param_values, 0, sizeof(param_values));
      for (uint8_t i = 0; i < TEMPLATE_PARAM_NB; i++)
      {
        mgt_status_t mgt_status = mgt_get_template_param(i, &param_values[i], &param_value_len);
      }

      // Then, configure socket_addr and dest_addr
      // Device Ipv6
      memcpy(socket_addr.addr, param_values[0], 8);
      memcpy(&socket_addr.addr[8], param_values[1], 8);
      // App Ipv6
      memcpy(dest_addr.addr, param_values[2], 8);
      memcpy(&dest_addr.addr[8], param_values[3], 8);
      // Device port
      memcpy(socket_addr.port, param_values[4], sizeof(socket_addr.port));
      // App port
      memcpy(dest_addr.port, param_values[5], sizeof(dest_addr.port));

      // Initialize DTG interface
      dtg_callbacks_t dtg_callbacks = {dtg_trans_result, dtg_data_received};
      dtg_status_t dtg_status = dtg_initialize(&dtg_callbacks);
      if (dtg_status != DTG_SUCCESS)
      {
        PRINTNOW();
        PRINT_MSG("sdk> Error in dtg_initialize\n");
        platform_error_handler();
      }

      // Create the socket.
      socket = dtg_socket(transmission_buffer, APP_TRANSMISSION_MAX_LENGTH);
      if (socket == DTG_INVALID_SOCKET)
      {
        PRINTNOW();
        PRINT_MSG("sdk> Error in socket creation\n");
        platform_error_handler();
      }
      dtg_status = dtg_bind(socket, &socket_addr);
      if (dtg_status != DTG_SUCCESS)
      {
        PRINTNOW();
        PRINT_MSG("sdk> Error in socket bind\n");
        platform_error_handler();
      }
      //
      platform_timer_add(&app_alive_timer, timer_id++, app_alive_timer_event_fct,
                         NULL);
      platform_timer_set_duration(&app_alive_timer, APP_ALIVE_PERIOD);
      platform_timer_start(&app_alive_timer);

      platform_timer_add(&app_init_msg_timer, timer_id++,
                         app_init_msg_timer_event_fct, NULL);
      platform_timer_set_duration(&app_init_msg_timer, APP_INIT_MSG_PERIOD);
      platform_timer_start(&app_init_msg_timer);
      //
      app_tx_current_state = TX_WAIT_IDLE;
      break;
    }
    // At this stage, not a boostrap event. Error.
    PRINT_MSG("app> error - not a boostrap event\n\r");
    break;
  default:
    break;
  }

  if (app_reception_event)
  {
    app_reception_event = false;
    // The only processing we perform in this example is to display
    // start of received data.
    bool rs;
    switch (app_reception_status)
    {
    case DTG_SUCCESS:
      PRINTNOW();
      PRINT_MSG("app> Datagram received - length: %d\n", app_received_data_l);
      rs = check_message();
      if (rs)
      {
        enqueue_sequence();
      }
      break;
    case DTG_BUFFER_ERR:
      PRINTNOW();
      PRINT_MSG("app> Receive buffer is too small\n");
      break;
    case DTG_OVERRUN_ERR:
      PRINTNOW();
      PRINT_MSG("app> Receive overrun\n");
      break;
    default:
      PRINTNOW();
      PRINT_MSG("app> Unknown status: %d\n", app_reception_status);
      break;
    }
  }

  if (app_alive_timer_event)
  {
    app_alive_timer_event = false;
    PRINTNOW();
    PRINT_MSG("app> Alive\n\r");
    PRINT_MSG("app> Tx packets: %d\n", stats_schc_packets_tx_counter());
    PRINT_MSG("app> Rx packets: %d\n", stats_schc_packets_rx_counter());
    PRINT_MSG("app> Tx SCHC fragments: %d\n", stats_schc_fragments_tx_counter());
    PRINT_MSG("app> Rx SCHC fragments: %d\n", stats_schc_fragments_rx_counter());
    PRINT_MSG("app> Tx SCHC ACK: %d\n", stats_schc_ack_tx_counter());
    PRINT_MSG("app> Rx SCHC ACK: %d\n", stats_schc_ack_rx_counter());
    platform_timer_set_duration(&app_alive_timer, APP_ALIVE_PERIOD);
    platform_timer_start(&app_alive_timer);
  }

  if (app_init_msg_timer_event)
  {
    app_init_msg_timer_event = false;
    PRINT_MSG("app> Sending initial uplink message\n");
    dtg_status_t dtg_status = dtg_sendto(socket, APP_INIT_SEND_MSG,
                                         strlen(APP_INIT_SEND_MSG), &dest_addr);
    if (dtg_status != DTG_SUCCESS)
    {
      PRINTNOW();
      PRINT_MSG("sdk> Error from dtg_sendto(): %d\n", dtg_status);
    }
  }
}

/**
 *
 */
static bool check_message(void)
{
  // Message format must be: nnn - hh:mm:ss

  // Ensure that we have at least 14 characters: sequence + timestamp
  if (app_received_data_l < 14)
  {
    PRINTNOW();
    PRINT_MSG("app> Message too small, ignored\n");
    return false;
  }
  // Check that we have a sequence number.
  for (uint8_t i = 0; i < 3; i++)
  {
    if (!isdigit(app_received_data[i]))
    {
      PRINTNOW();
      PRINT_MSG("app> Incorrect sequence number\n");
      return false;
    }
  }
  // Check that we have a timestamp.
  bool timestamp_ok = true;
  if (!isdigit(app_received_data[6]))
    timestamp_ok = false;
  if (!isdigit(app_received_data[7]))
    timestamp_ok = false;
  if (!isdigit(app_received_data[9]))
    timestamp_ok = false;
  if (!isdigit(app_received_data[10]))
    timestamp_ok = false;
  if (!isdigit(app_received_data[12]))
    timestamp_ok = false;
  if (!isdigit(app_received_data[13]))
    timestamp_ok = false;
  if (!timestamp_ok)
  {
    PRINTNOW();
    PRINT_MSG("app> Incorrect timestamp\n");
    return false;
  }
  return true;
}

/**
 *
 */
uint8_t ascii_to_int(uint8_t c)
{
  return c - '0';
}

/**
 *
 */
static void enqueue_sequence(void)
{
  uint8_t seq = ascii_to_int(app_received_data[0]) * 100 +
                ascii_to_int(app_received_data[1]) * 10 +
                ascii_to_int(app_received_data[2]);
  received_sequences[rec_seq_index] = seq;
  uint8_t h = ascii_to_int(app_received_data[6]) * 10 +
              ascii_to_int(app_received_data[7]);
  uint8_t m = ascii_to_int(app_received_data[9]) * 10 +
              ascii_to_int(app_received_data[10]);
  uint8_t s = ascii_to_int(app_received_data[12]) * 10 +
              ascii_to_int(app_received_data[13]);
  PRINTNOW();
  PRINT_MSG("app> Message %03d %02d:%02d:%02d received\n", seq, h, m, s);
  rec_seq_index++;
  if (rec_seq_index >= APP_REC_MSG_NB)
  {
    // Send the list of received sequences.
    // First, create the message. 1 is removed from sizeof(string)
    // in order not to include the final null byte, for the two strings.
    uint16_t message_l = sizeof(APP_SEND_MSG_1) - 1 +
                         APP_MSG_NB_CHAR_NB * APP_REC_MSG_NB - 1 +
                         sizeof(APP_SEND_MSG_2) - 1;
    if (APP_SEND_MAX_LENGTH < message_l)
    {
      PRINTNOW();
      PRINT_MSG("app> Error, transmission buffer is too small\n");
      return;
    }
    memcpy(app_send_data, APP_SEND_MSG_1, sizeof(APP_SEND_MSG_1) - 1);
    for (uint8_t i = 0; i < APP_REC_MSG_NB; i++)
    {
      // sprintf() adds a null byte, which is overwritten by next write.
      sprintf((char *)app_send_data + sizeof(APP_SEND_MSG_1) - 1 + 4 * i,
              "%03d ", received_sequences[i]);
    }
    memcpy(app_send_data + sizeof(APP_SEND_MSG_1) - 1 +
               APP_MSG_NB_CHAR_NB * APP_REC_MSG_NB - 1,
           APP_SEND_MSG_2, sizeof(APP_SEND_MSG_2) - 1);
    // Request to send the uplink message. Sockaddr is not used, as compression
    // is not activated. But print a trace message beforehand.
    char seq[4 * APP_REC_MSG_NB];
    memcpy(seq, app_send_data + sizeof(APP_SEND_MSG_1) - 1,
           4 * APP_REC_MSG_NB - 1);
    seq[4 * APP_REC_MSG_NB - 1] = '\0';
    PRINTNOW();
    PRINT_MSG("app> Sending sequences ack: %s - %d bytes\n", seq, message_l);
    dtg_status_t dtg_status =
        dtg_sendto(socket, app_send_data, message_l, &dest_addr);
    if (dtg_status != DTG_SUCCESS)
    {
      PRINTNOW();
      PRINT_MSG("app> Error from dtg_sendto(): %d\n", dtg_status);
    }

    rec_seq_index = 0;
  }
}

/**
 *
 */
static void app_alive_timer_event_fct(void *context)
{
  app_alive_timer_event = true;
  app_process_request = true;
}

static void app_init_msg_timer_event_fct(void *context)
{
  app_init_msg_timer_event = true;
  app_process_request = true;
}

static void sdk_timer_1_event(void *context)
{
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  mgt_timer_timeout(5);
}
/******************************************************************************
 * FullSDK callbacks.
 *****************************************************************************/

/**
 *
 */
static void mgt_processing_required(void)
{
  mgt_process_request = true;
}

/**
 *
 */
static void mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    app_connectivity_event = true;
    app_process_request = true;
  }
  else
  {
    PRINTNOW();
    PRINT_MSG("sdk> MgtConnectivityState error: %d\n", state);
  }
}

static void mgt_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&sdk_timers[id], duration);
  platform_timer_start(&sdk_timers[id]);
}

static void mgt_stop_timer(uint8_t id)
{
  platform_timer_stop(&sdk_timers[id]);
}

static void mgt_bootstrap_result(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    PRINT_MSG("sdk> mgt_bootstrap_result - OK\n");
    app_boostrap_event = true;
    app_process_request = true;
  }
  else
  {
    PRINT_MSG("sdk> mgt_bootstrap_result - KO: %d\n\r", state);
  }
}

/**
 *
 */
static void dtg_trans_result(const dtg_socket_t *socket, dtg_status_t status,
                             uint16_t error)
{
  PRINTNOW();
  PRINT_MSG("sdk> Transmission status: %d\n", status);
}

/**
 *
 */
static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                              uint16_t data_size,
                              const dtg_sockaddr_t *src_addr,
                              dtg_status_t status)
{
  // Tell main loop.
  app_received_data_l = data_size;
  app_reception_status = status;
  // Ensure no buffer overflow.
  if (data_size > APP_RECEIVE_MAX_LENGTH)
  {
    PRINTNOW();
    PRINT_MSG("sdk> Too much data received: %d\n", data_size);
    data_size = APP_RECEIVE_MAX_LENGTH;
  }
  // Transfer received data to application reception buffer.
  if (status == DTG_SUCCESS)
  {
    memcpy(app_received_data, p_buffer, data_size);
  }
  app_reception_event = true;
  app_process_request = true;
}

/**
 * For traces. Called for each trace message.
 */
void tr_handle_trace(const uint8_t *trace, uint8_t length)
{
  static const char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                             '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
  // For every byte of the binary trace message, we need two bytes in the
  // output
  // string, and a final '\0' byte.
  char out_str[TRACE_MAX_LENGTH * 2 + 1];
  for (uint8_t i = 0; i < length; i++)
  {
    out_str[2 * i] = hex[trace[i] >> 4];
    out_str[2 * i + 1] = hex[trace[i] & 0x0f];
  }
  out_str[length * 2] = '\0';
  PRINT_MSG("TRA>%s\n", out_str);
}
