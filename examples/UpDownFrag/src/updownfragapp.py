# Copyright (C) 2018-2020 ACKLIO SAS - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Author: Pascal Bodin - pascal@ackl.io

import argparse
import datetime
import socket
import threading
import time

# Size of reception buffer.
REC_BUFFER_SIZE = 4096

msg_number = 0

DOWNLINK_MESSAGE = (
    "This is Ground Control to Device. You've really made the grade. "
    "And the papers want to know which stack you run. "
    "Now it's time to reply if you dare."
    )

def receive_messages(sock):
    while True:
        uplink_bytes = sock.recv(REC_BUFFER_SIZE)
        uplink_str = uplink_bytes.decode('utf-8')
        time_str = '{:%H:%M:%S}'.format(datetime.datetime.now())
        print('==> (' + time_str + ') ' + uplink_str)

parser = argparse.ArgumentParser(description='Application for UpDownFrag sample application.')
parser.add_argument('deviceAddress', help='IPv6 device address')
parser.add_argument('devicePort', type=int, help='device port')
parser.add_argument('applicationPort', type=int, help='application port for transmission and reception')
parser.add_argument('period', type=int, help='period between two downlink messages, in seconds')
args = parser.parse_args()

deviceAddress = args.deviceAddress
devicePort = args.devicePort
applicationPort = args.applicationPort
period = args.period
print('device address:   {}'.format(deviceAddress))
print('device port:      {}'.format(devicePort))
print('application port: {}'.format(applicationPort))
print('period:           {} s'.format(period))

deviceAddrPort = (deviceAddress, devicePort)
        
s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
s.bind(('::', applicationPort))

rec_thread = threading.Thread(target=receive_messages, args=(s,))
rec_thread.start()

while True:
    time.sleep(period)
    msg_number_str = '{:03d} - '.format(msg_number)
    time_str = '{:%H:%M:%S} - '.format(datetime.datetime.now())
    downlink_str = msg_number_str + time_str + DOWNLINK_MESSAGE
    downlink_bytes = str.encode(downlink_str)
    s.sendto(downlink_bytes, deviceAddrPort)
    print('<== ' + downlink_str)
    if msg_number >= 999:
        msg_number = 0
    else:
        msg_number += 1
