/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 */

#ifndef __UIP_INTEGRATION_H
#define __UIP_INTEGRATION_H

#include "uipopt.h"
#include "psock.h"

#ifndef UIP_UDP_APPCALL
#define UIP_UDP_APPCALL uip_udp_call
#endif

#ifndef UIP_APPCALL
#define UIP_APPCALL uip_tcp_call
#endif

// uIP mandatory definitions
typedef uint8_t uip_tcp_appstate_t;
typedef uint8_t uip_udp_appstate_t;

// uIP send length (exluding headers)
extern uint16_t uip_slen;

void uip_udp_call(void);
void uip_tcp_call(void);

#endif /* __UIP_INTEGRATION_H */