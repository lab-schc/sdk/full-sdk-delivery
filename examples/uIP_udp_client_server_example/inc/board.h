/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 */

#ifndef __TARGET_H
#define __TARGET_H

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "platform.h"

void init_hw(void);

#endif /* __TARGET_H */