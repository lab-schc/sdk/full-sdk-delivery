/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 */

#ifndef __NETWORK_INFO_H
#define __NETWORK_INFO_H

#include <stdint.h>

#define MAX_NETWORK_PARAM_LEN 20
#define NETWORK_PARAM_CNT 6 // IPv6/UDP template ID 1

// Network parameter structure
typedef struct
{
  uint8_t value[MAX_NETWORK_PARAM_LEN];
  uint16_t len;
} network_params_t;

// Network info structure
typedef struct
{
  network_params_t param[NETWORK_PARAM_CNT];
} network_info_t;

#endif /* __NETWORK_INFO_H */