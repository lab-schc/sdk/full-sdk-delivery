# uIP_udp_client_server_example

## Description

*uIP_udp_client_server_example* application shows how to send and receive IPv6/UDP packets using FreeRTOS, uIP IP stack and FullSDK Network API

The application uses version 4.4.7 of Semtech stack (stackforce) and support STM32L476RG-Nucleo board.

The application sends an uplink message:

* when the user presses the blue button of the STM32L476RG-Nucleo board, or
* every 90 s, or
* when it receives a downlink message requesting it to send its local time

The application dumps any received message.

## Build

```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_NAME=uIP_udp_client_server_example -DPLATFORM=STM32L476RG-Nucleo -DTOOLCHAIN=gcc-arm-none -DTARGET=m4 -DL2_STACK=semtech -DUSE_OS=FREERTOS -DEXTENSION_API=template -DTEMPLATE_ID=ipv6udp -DLORAWAN_DEVEUI=1111111111111111 -DLORAWAN_APPEUI=1111111111111111 -DLORAWAN_APPKEY=11111111111111111111111111111111 -DGCC_FABI=hard -DSYNC_ENABLED=ON && make -C ./build
```

## Configuration

Network information is automatically configured using synchronization protocol between SDK and IPCore.

On the Network Server, the device class must be set to C.

On IPCore, the device template for the device profile in the flow must be set to `[TEMPLATE 1] ipv6-udp`. The reason is that this SCHC template uses the `SCHC Template ID` number `1`, which corresponds to the template specified by the build command above.

## Cloud application

On cloud side, a simple application can send downlink messages to the device, when the user enters the associated command, by pressing a letter key and the Enter key.

When the D key and the Enter key are pressed, a long message is sent to the device. The message contains a timestamp, a sequence number and some text. Format of this message is:

```
nnn - hh:mm:ss - text
```

`nnn` is the sequence number, with three figures. It starts from `000` and is incremented with each new downlink message. When it reaches `999`, it wraps around to `000`.

`hh:mm:ss` is the timestamp.

`text` contains enough characters to make a message of 300 bytes.

When the T key and the Enter key are pressed, a short message is sent ot the device, requesting device's local time. Device's reply is displayed.

More generally, every uplink message sent by the device is displayed.

To run the cloud application:

* a computer connected to the Internet must be used. *Python3* must be available
* run the VPN agent:

```sh
sudo ./ipcore-nw-binding-agent-x86-v0.0.1 --agent-server-addr=<ipcoreAddress>:9995
```

* add to the network interface in use the address the application should bind to:

```sh
sudo ip -6 addr add <applicationAddress> dev <networkInterface>
```

* run the application:

```sh
python3 udp_client_server_example.py <deviceAddress> <devicePort> <applicationAddress> <applicationPort>
```

`<deviceAddress>` is the IPv6 address of the device, as displayed in the device profile on IPCore.

`<devicePort>` is the device port, as defined on IPCore.

`<applicationAddress>` is the application address, as defined in IPCore.

`<applicationPort>` is the application port, as defined on IPCore.

## Note

This example uses ASCII messages. In a production application, binary messages would be used, as they can be shorter, for the same amount of transferred information.