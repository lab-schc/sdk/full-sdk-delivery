/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 */

#include "board.h"
#include "network_info.h"
#include "os.h"
#include "task_sdk.h"
#include "uip.h"
#include "uip_integration.h"
#include "uiplib.h"
#include <string.h>

// Queue configuration
#define MAX_QUEUE_RX_TIMEOUT_MS 10
#define MAX_QUEUE_MSG_NUM 10

// Thread context
typedef struct
{
  QueueHandle_t queue;
  struct uip_udp_conn *conn;
  struct
  {
    bool connectivity;
    bool transmitting;
  } state;
  uint8_t *application_data_ptr;
  uint16_t application_data_len;
  bool prepare_send;
} context_t;

// Local functions
static void task(void *args);
static void convert_ipv6_addr_fmt(uint16_t *uip_fmt, uint8_t *sdk_fmt);
static void convert_udp_port_fmt(uint16_t *uip_fmt, uint8_t *sdk_fmt);

static context_t ctx;

QueueHandle_t uip_get_queue(void)
{
  return ctx.queue;
}

void init_uip(void)
{
  // Initialize thread context
  memset(&ctx, 0, sizeof(context_t));

  // Init underlying layer
  init_sdk();

  // Create message queue
  ctx.queue = xQueueCreate(MAX_QUEUE_MSG_NUM, sizeof(message_t));
  if (ctx.queue == NULL)
  {
    PRINT_MSG("uip> xQueueCreate failed\n");
  }

  // Create FreeRTOS task
  if (xTaskCreate(task, "uip", UIP_STACK_SIZE, (void *)NULL, UIP_TASK_PRIORITY,
                  NULL) != pdPASS)
  {
    PRINT_MSG("uip> xTaskCreate failed\n");
  }
}

static void convert_ipv6_addr_fmt(uint16_t *uip_fmt, uint8_t *sdk_fmt)
{
  uip_fmt[0] = (uip_fmt[0] & 0xFF00) | sdk_fmt[1] << 0;
  uip_fmt[0] = (uip_fmt[0] & 0x00FF) | sdk_fmt[0] << 8;
  uip_fmt[1] = (uip_fmt[1] & 0xFF00) | sdk_fmt[3] << 0;
  uip_fmt[1] = (uip_fmt[1] & 0x00FF) | sdk_fmt[2] << 8;
  uip_fmt[2] = (uip_fmt[2] & 0xFF00) | sdk_fmt[5] << 0;
  uip_fmt[2] = (uip_fmt[2] & 0x00FF) | sdk_fmt[4] << 8;
  uip_fmt[3] = (uip_fmt[3] & 0xFF00) | sdk_fmt[7] << 0;
  uip_fmt[3] = (uip_fmt[3] & 0x00FF) | sdk_fmt[6] << 8;
}

static void convert_udp_port_fmt(uint16_t *uip_fmt, uint8_t *sdk_fmt)
{
  uip_fmt[0] = (uip_fmt[0] & 0xFF00) | sdk_fmt[1] << 0;
  uip_fmt[0] = (uip_fmt[0] & 0x00FF) | sdk_fmt[0] << 8;
}

static void task(void *args)
{
  message_t rx_msg;

  while (true)
  {
    uip_udp_periodic_conn(ctx.conn);

    // Receive FIFO message from the queue (if any) otherwise don't block
    if (xQueueReceive(uip_get_queue(), &rx_msg, MAX_QUEUE_RX_TIMEOUT_MS) ==
        pdPASS)
    {
      switch (rx_msg.type)
      {
        case MSG_TYPE_REQ_APP_UIP_INIT:
        {
          // Initialize SDK and join to the network
          message_t msg;
          msg.type = MSG_TYPE_REQ_UIP_SDK_INIT;
          xQueueSend(sdk_get_queue(), &msg, 0);
        }
        break;

        case MSG_TYPE_INF_SDK_UIP_NETWORK_CONFIGURED:
        {
          uip_ipaddr_t ipaddr;

          // Initialize uIP
          uip_init();

          network_info_t *ni = sdk_get_network_info();

          uint16_t host_ip_prefix[4], host_ip_iid[4];
          uint16_t remote_ip_prefix[4], remote_ip_iid[4];
          uint16_t remote_port, host_port;

          // Convert host IP address format
          convert_ipv6_addr_fmt(host_ip_prefix, ni->param[0].value);
          convert_ipv6_addr_fmt(host_ip_iid, ni->param[1].value);

          // Convert remote IP address format
          convert_ipv6_addr_fmt(remote_ip_prefix, ni->param[2].value);
          convert_ipv6_addr_fmt(remote_ip_iid, ni->param[3].value);

          // Convert host UDP port format
          convert_udp_port_fmt(&host_port, ni->param[4].value);

          // Convert remote UDP port format
          convert_udp_port_fmt(&remote_port, ni->param[5].value);

          // Set host IP address of the network stack
          uip_ip6addr(ipaddr, host_ip_prefix[0], host_ip_prefix[1],
                      host_ip_prefix[2], host_ip_prefix[3], host_ip_iid[0],
                      host_ip_iid[1], host_ip_iid[2], host_ip_iid[3]);
          uip_sethostaddr(ipaddr);

          // Establish a connection with remote UDP server
          uip_ip6addr(ipaddr, remote_ip_prefix[0], remote_ip_prefix[1],
                      remote_ip_prefix[2], remote_ip_prefix[3],
                      remote_ip_iid[0], remote_ip_iid[1], remote_ip_iid[2],
                      remote_ip_iid[3]);
          ctx.conn = uip_udp_new(&ipaddr, HTONS(remote_port));
          if (ctx.conn != NULL)
          {
            // Bind the connection to a local port to receive data from server
            uip_udp_bind(ctx.conn, HTONS(host_port));
          }

          PRINT_MSG("uip> dev info: "
                    "[%.4X:%.4X:%.4X:%.4X:%.4X:%.4X:%.4X:%.4X]:%d\n",
                    host_ip_prefix[0], host_ip_prefix[1], host_ip_prefix[2],
                    host_ip_prefix[3], host_ip_iid[0], host_ip_iid[1],
                    host_ip_iid[2], host_ip_iid[3], host_port);

          PRINT_MSG("uip> app info: "
                    "[%.4X:%.4X:%.4X:%.4X:%.4X:%.4X:%.4X:%.4X]:%d\n",
                    remote_ip_prefix[0], remote_ip_prefix[1],
                    remote_ip_prefix[2], remote_ip_prefix[3], remote_ip_iid[0],
                    remote_ip_iid[1], remote_ip_iid[2], remote_ip_iid[3],
                    remote_port);

          PRINT_MSG("uip> ready to send and receive data\n");

          message_t msg;
          msg.type = MSG_TYPE_INF_UIP_APP_READY;
          xQueueSend(app_get_queue(), &msg, 0);

          ctx.state.connectivity = true;
        }
        break;

        case MSG_TYPE_REQ_APP_UIP_SEND:
        {
          message_t msg;
          msg.type = MSG_TYPE_INF_UIP_APP_CAN_NOT_SEND;

          if (!ctx.state.connectivity)
          {
            PRINT_MSG("uip> transmission request discarded due to "
                      "non-established network connectivity\n");

            xQueueSend(app_get_queue(), &msg, 0);

            break;
          }

          if (ctx.state.transmitting)
          {
            PRINT_MSG("uip> transmission request discarded due to ongoing "
                      "communication\n");

            xQueueSend(app_get_queue(), &msg, 0);

            break;
          }

          // application data will be encapsulated when uIP is polled
          ctx.application_data_ptr = (uint8_t *)rx_msg.body.data_ptr;
          ctx.application_data_len = rx_msg.len;
          ctx.prepare_send = true;
          ctx.state.transmitting = true;
        }
        break;

        case MSG_TYPE_REQ_UIP_UIP_SEND:
        {
          PRINT_MSG("uip> sending data (len: %d)\n", ctx.application_data_len);
          PRINT_HEX_BUF(ctx.application_data_ptr, ctx.application_data_len);

          message_t msg;
          msg.type = MSG_TYPE_REQ_UIP_SDK_SEND;
          msg.body.data_ptr = uip_buf;
          msg.len = UIP_IPUDPH_LEN + uip_slen;
          xQueueSend(sdk_get_queue(), &msg, 0);
        }
        break;

        case MSG_TYPE_INF_SDK_UIP_TX_RESULT:
        {
          if (rx_msg.body.error)
          {
            PRINT_MSG("uip> data send failed\n");
          }
          else
          {
            PRINT_MSG("uip> data sent\n");
          }

          ctx.state.transmitting = false;

          message_t msg;
          msg.type = MSG_TYPE_INF_UIP_APP_TX_RESULT;
          xQueueSend(app_get_queue(), &msg, 0);
        }
        break;

        case MSG_TYPE_INF_SDK_UIP_DATA_RCVD:
        {
          if (!ctx.state.connectivity)
          {
            PRINT_MSG("uip> data received before the network connectivity is "
                      "fully established\n");
          }
          else
          {
            PRINT_MSG("uip> data received\n");
          }

          uip_len = rx_msg.len;
          memcpy(uip_buf, rx_msg.body.data_ptr, rx_msg.len);
          uip_process(UIP_DATA);
        }
        break;

        default:
        {
          PRINT_MSG("uip> unknown message type (type: %d)\n", rx_msg.type);
        }
        break;
      }
    }

    vTaskDelay(UIP_TASK_DELAY);
  }
}

// uIP mandatory callback for UDP events (called by uIP core)
void uip_udp_call(void)
{
  // UDP events
  if (uip_newdata()) // data has been received
  {
    PRINT_HEX_BUF(uip_appdata, uip_len);

    message_t msg;
    msg.type = MSG_TYPE_INF_UIP_APP_DATA_RCVD;
    msg.body.data_ptr = uip_appdata;
    msg.len = uip_len;
    xQueueSend(app_get_queue(), &msg, 0);
  }
  else if (uip_poll()) // uIP stack has been polled
  {
    if (ctx.prepare_send)
    {
      ctx.prepare_send = false;
      memcpy(uip_appdata, ctx.application_data_ptr, ctx.application_data_len);
      uip_udp_send(ctx.application_data_len);

      message_t msg;
      msg.type = MSG_TYPE_REQ_UIP_UIP_SEND;
      xQueueSend(uip_get_queue(), &msg, 0);
    }
  }
}

// uIP mandatory callback for TCP events
void uip_tcp_call(void)
{
}

// uIP mandatory callback for internal logging
void uip_log(char *msg)
{
  PRINT_MSG("[uip_lib]: %s\n", msg);
}
