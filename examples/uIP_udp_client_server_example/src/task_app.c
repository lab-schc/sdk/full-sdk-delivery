/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 */

#include "board.h"
#include "os.h"
#include "platform.h"
#include "task_uip.h"

// Application configuration
#define SEND_PERIOD 90000 // send packet every 1.5 minute

// Queue configuration
#define MAX_QUEUE_RX_TIMEOUT_MS 10
#define MAX_QUEUE_MSG_NUM 10

static const char local_time_query_cmd[] = "getLocalTime";
static const uint8_t local_time_query_cmd_len = strlen(local_time_query_cmd);

static char local_time_reply[] = "local time: nnnnnnnnnn";
//                                0           12       21
static const uint8_t local_time_reply_len = 22;

// Thread context
typedef struct
{
  QueueHandle_t queue;
  TimerHandle_t timer;
} context_t;

// Local functions
static void task(void *args);
static void timer_callback(TimerHandle_t timer);

static context_t ctx;

QueueHandle_t app_get_queue(void)
{
  return ctx.queue;
}

void init_app(void)
{
  // Initialize thread context
  memset(&ctx, 0, sizeof(context_t));

  // Create message queue
  ctx.queue = xQueueCreate(MAX_QUEUE_MSG_NUM, sizeof(message_t));
  if (ctx.queue == NULL)
  {
    PRINT_MSG("app> xQueueCreate failed\n");
  }

  // Create timers
  ctx.timer = xTimerCreate("tmr", 1, pdFALSE, (void *)NULL, timer_callback);
  if (ctx.timer == NULL)
  {
    PRINT_MSG("app> xTimerCreate failed\n");
  }

  // Init underlying layer
  init_uip();

  // Create FreeRTOS task
  if (xTaskCreate(task, "app", APP_STACK_SIZE, (void *)NULL, APP_TASK_PRIORITY,
                  NULL) != pdPASS)
  {
    PRINT_MSG("app> xTaskCreate failed\n");
  }
}

static void task(void *args)
{
  message_t rx_msg;

  message_t msg;
  msg.type = MSG_TYPE_REQ_APP_UIP_INIT;
  xQueueSend(uip_get_queue(), &msg, 0);

  while (true)
  {
    // Receive FIFO message from the queue (if any) otherwise don't block
    if (xQueueReceive(app_get_queue(), &rx_msg, MAX_QUEUE_RX_TIMEOUT_MS) ==
        pdPASS)
    {
      switch (rx_msg.type)
      {
        case MSG_TYPE_INF_UIP_APP_DATA_RCVD:
        {
          /*
            Implementation note:
            Application data is in `rx_msg.body.data_ptr` with the size of
            `rx_msg.len`. Data must be copied to another buffer otherwise it is
            possible that it can be overwritten by the last received data.
          */
          PRINT_MSG("app> data received\n");
          PRINT_HEX_BUF(rx_msg.body.data_ptr, rx_msg.len);
          if (rx_msg.len == local_time_query_cmd_len)
          {
            if (memcmp(rx_msg.body.data_ptr, local_time_query_cmd,
                       local_time_query_cmd_len) == 0)
            {
              PRINT_MSG("app> request for local time, replying...\n");
              sprintf(local_time_reply + 12, "%010d", platform_get_clock_ms());
              message_t msg;
              msg.type = MSG_TYPE_REQ_APP_UIP_SEND;
              msg.body.data_ptr = local_time_reply;
              msg.len = local_time_reply_len;;
              xQueueSend(uip_get_queue(), &msg, 0);
            }
          }
        }
        break;

        case MSG_TYPE_INF_UIP_APP_TX_RESULT:
        {
          // Start timer to send data
          xTimerChangePeriod(ctx.timer, SEND_PERIOD, 0);
          xTimerStart(ctx.timer, 0);

          PRINT_MSG("app> timer started in order to send data in %d seconds\n",
                    SEND_PERIOD / 1000);
        }
        break;

        case MSG_TYPE_INF_UIP_APP_CAN_NOT_SEND:
        {
          /*
            Implementation note:
            An action can be taken like retrying when the data transmission
            request is rejected.
          */
          PRINT_MSG("app> data send failed\n");
        }
        break;

        case MSG_TYPE_INF_UIP_APP_READY:
        {
          // Start timer to send data
          xTimerChangePeriod(ctx.timer, SEND_PERIOD, 0);
          xTimerStart(ctx.timer, 0);

          PRINT_MSG("app> timer started in order to send data in %d seconds\n",
                    SEND_PERIOD / 1000);
        }
        break;

        case MSG_TYPE_REQ_APP_APP_SEND:
        {
          PRINT_MSG("app> sending data (len: %d)\n", rx_msg.len);
          PRINT_HEX_BUF(rx_msg.body.data_ptr, rx_msg.len);

          message_t msg;
          msg.type = MSG_TYPE_REQ_APP_UIP_SEND;
          msg.body.data_ptr = rx_msg.body.data_ptr;
          msg.len = rx_msg.len;
          xQueueSend(uip_get_queue(), &msg, 0);
        }
        break;

        default:
        {
          PRINT_MSG("app> unknown message type (type: %d)\n", rx_msg.type);
        }
        break;
      }
    }

    vTaskDelay(APP_TASK_DELAY);
  }
}

uint8_t data_from_interrupt[12] = "Hi, there!\n";
uint8_t data_from_timer[16] = "Hello, Acklio!\n";

void user_button_callback(void *context)
{
  PRINT_MSG("app> send packet request from user button\n");

  /*
    Implementation note:
    Data can be sent from the interrupt by sending below message.
  */
  message_t msg;
  msg.type = MSG_TYPE_REQ_APP_APP_SEND;
  msg.body.data_ptr = data_from_interrupt;
  msg.len = sizeof(data_from_interrupt);
  xQueueSendFromISR(app_get_queue(), &msg, 0);
}

static void timer_callback(TimerHandle_t timer)
{
  PRINT_MSG("app> send packet request from timer\n");

  /*
    Implementation note:
    Data can be sent by sending below message.
  */
  message_t msg;
  msg.type = MSG_TYPE_REQ_APP_APP_SEND;
  msg.body.data_ptr = data_from_timer;
  msg.len = sizeof(data_from_timer);
  xQueueSend(app_get_queue(), &msg, 0);
}