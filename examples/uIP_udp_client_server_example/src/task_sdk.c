/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz aydogan.ersoz@ackl.io
 */

#include <string.h>

#include "board.h"
#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "fullsdknet.h"
#include "network_info.h"
#include "os.h"

// SDK configuration
#define MAX_TIMER_CNT 6            // 6 timers needed by SDK
#define MAX_RECEIVED_DATA_LEN 1500 // for IPv6 maximum MTU
#define MAX_PAYLOAD_SIZE 1500      // for IPv6 maximum MTU 
                                   // (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE)
                                   // must be 4-bytes aligned
#define MAX_MTU_SIZE 244           // Assume MAX_MTU_SIZE must be 4-bytes
                                   // aligned (242 bytes for LoRaWAN)
#define MEM_BLOCK_SIZE                                                         \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u +              \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)
#define SYNC_RETRANS_TIMER_VAL 5000
#define SYNC_RETRANS_CNT_VAL 3

// Queue configuration
#define MAX_QUEUE_RX_TIMEOUT_MS 10
#define MAX_QUEUE_MSG_NUM 10

// Thread context
typedef struct
{
  uint8_t sdk_internal_memory[MEM_BLOCK_SIZE];
  uint8_t received_data[MAX_RECEIVED_DATA_LEN];
  QueueHandle_t queue;
  struct
  {
    TimerHandle_t self;
    uint8_t id;
  } timer[MAX_TIMER_CNT];
} context_t;

// Local functions
static void task(void *args);
static void connectivity_state(mgt_status_t status);
static void sync_bootstrap_result(mgt_status_t status);
static void transmission_result(net_status_t status, uint16_t error);
static void data_received(const uint8_t *buffer, uint16_t data_size,
                          net_status_t status);
static void timer_start(uint8_t id, uint32_t duration);
static void timer_stop(uint8_t id);
static void timer_callback(TimerHandle_t timer);
static void processing_required(void);

static context_t ctx;
static network_info_t network_info;

network_info_t *sdk_get_network_info(void)
{
  return &network_info;
}

QueueHandle_t sdk_get_queue(void)
{
  return ctx.queue;
}

void init_sdk(void)
{
  // Initialize thread context
  memset(&ctx, 0, sizeof(context_t));

  // Create message queue
  ctx.queue = xQueueCreate(MAX_QUEUE_MSG_NUM, sizeof(message_t));
  if (ctx.queue == NULL)
  {
    PRINT_MSG("sdk> xQueueCreate failed\n");
  }

  // Create timers
  for (uint8_t i = 0; i < MAX_TIMER_CNT; i++)
  {
    ctx.timer[i].id = i;
    ctx.timer[i].self =
        xTimerCreate("tmr",
                     1, // 0 is not a valid one
                     pdFALSE, (void *)&ctx.timer[i].id, timer_callback);
    if (ctx.timer[i].self == NULL)
    {
      PRINT_MSG("sdk> xTimerCreate failed (id: %d)\n", i);
    }
  }

  // Create FreeRTOS task
  if (xTaskCreate(task, "sdk", SDK_STACK_SIZE, (void *)NULL, SDK_TASK_PRIORITY,
                  NULL) != pdPASS)
  {
    PRINT_MSG("sdk> xTaskCreate failed\n");
  }
}

static void task(void *args)
{
  message_t rx_msg;

  while (true)
  {
    // Receive FIFO message from the queue (if any) otherwise don't block
    if (xQueueReceive(sdk_get_queue(), &rx_msg, MAX_QUEUE_RX_TIMEOUT_MS) ==
        pdPASS)
    {
      switch (rx_msg.type)
      {
        case MSG_TYPE_REQ_UIP_SDK_INIT:
        {
          // Initialize hardware after FreeRTOS scheduler runs
          init_hw();

          // Initialize SDK management layer
          PRINT_MSG("sdk> FullSDK version: %s\n", mgt_get_version());

          mgt_callbacks_t mgt_cb;
          mgt_cb.connectivity_state = connectivity_state;
          mgt_cb.processing_required = processing_required;
          mgt_cb.sync_bootstrap_result = sync_bootstrap_result;
          mgt_cb.start_timer = timer_start;
          mgt_cb.stop_timer = timer_stop;
          mgt_status_t mgt_status =
              mgt_initialize(&mgt_cb, ctx.sdk_internal_memory, MEM_BLOCK_SIZE,
                             MAX_MTU_SIZE, MAX_PAYLOAD_SIZE);
          if (mgt_status != MGT_SUCCESS)
          {
            PRINT_MSG("sdk> mgt_initialize failed (status: %d)\n", mgt_status);
            break;
          }

          PRINT_MSG("sdk> SDK initialized\n");
        }
        break;

        case MSG_TYPE_REQ_SDK_SDK_PROCESS:
        {
          mgt_status_t mgt_status = mgt_process();
          if (mgt_process() != MGT_SUCCESS)
          {
            PRINT_MSG("sdk> mgt_process failed (status: %d)\n", mgt_status);
          }
        }
        break;

        case MSG_TYPE_INF_SDK_SDK_NETWORK_JOINED:
        {
          PRINT_MSG("sdk> device joined to the network\n");

          uint8_t datarate = 3;
          l2_set_dr(datarate);
          PRINT_MSG("sdk> device datarate set to %d\n", datarate);

          char class = 'C';
          l2_set_class(class);
          PRINT_MSG("sdk> device class set to %c\n", class);

          // Retrieve missing parameter values from IPCore
          mgt_status_t status =
              mgt_sync_bootstrap(SYNC_RETRANS_TIMER_VAL, SYNC_RETRANS_CNT_VAL);
          if (status == MGT_SUCCESS)
          {
            PRINT_MSG("sdk> synchronization request sent\n");
          }
          else
          {
            PRINT_MSG("sdk> synchronization request failed (status: %d)\n",
                      status);
          }
        }
        break;

        case MSG_TYPE_INF_SDK_SDK_NETWORK_CONFIGURED:
        {
          uint8_t param_cnt;

          mgt_get_nb_template_params(&param_cnt);

          if (param_cnt != NETWORK_PARAM_CNT)
          {
            PRINT_MSG(
                "sdk> wrong network template selected (parameter count: %d)\n",
                param_cnt);
            continue;
          }

          for (uint8_t param_idx = 0; param_idx < param_cnt; param_idx++)
          {
            uint8_t *param;
            uint16_t param_size;

            mgt_status_t status =
                mgt_get_template_param(param_idx, &param, &param_size);
            if (status == MGT_SUCCESS)
            {
              memcpy(network_info.param[param_idx].value, param, param_size);

              if (param_idx == param_cnt - 1)
              {
                // Initialize SDK Network interface
                net_callbacks_t net_cb;
                net_cb.transmission_result = transmission_result;
                net_cb.data_received = data_received;
                net_status_t net_status = net_initialize(&net_cb);
                if (net_status != NET_SUCCESS)
                {
                  PRINT_MSG("sdk> net_initialize failed (status: %d)\n",
                            net_status);
                  break;
                }

                message_t msg;
                msg.type = MSG_TYPE_INF_SDK_UIP_NETWORK_CONFIGURED;
                xQueueSend(uip_get_queue(), &msg, 0);
              }
            }
            else
            {
              PRINT_MSG("sdk> parameter get failed (status: %d)\n", status);
              break;
            }
          }
        }
        break;

        case MSG_TYPE_REQ_UIP_SDK_SEND:
        {
          PRINT_MSG("sdk> sending data (len: %d)\n", rx_msg.len);
          PRINT_HEX_BUF(rx_msg.body.data_ptr, rx_msg.len);

          net_status_t net_status =
              net_sendto(rx_msg.body.data_ptr, rx_msg.len);
          if (net_status != NET_SUCCESS)
          {
            PRINT_MSG("sdk> net_sendto failed (status: %d)\n", net_status);

            message_t msg;
            msg.type = MSG_TYPE_INF_SDK_UIP_TX_RESULT;
            msg.body.error = true;
            xQueueSend(uip_get_queue(), &msg, 0);
            break;
          }
        }
        break;

        default:
        {
          PRINT_MSG("sdk> unknown message type (type: %d)\n", rx_msg.type);
        }
        break;
      }
    }

    vTaskDelay(SDK_TASK_DELAY);
  }
}

static void connectivity_state(mgt_status_t status)
{
  if (status == MGT_SUCCESS)
  {
    message_t msg;
    msg.type = MSG_TYPE_INF_SDK_SDK_NETWORK_JOINED;
    xQueueSend(sdk_get_queue(), &msg, 0);
  }
}

static void sync_bootstrap_result(mgt_status_t status)
{
  if (status == MGT_SUCCESS)
  {
    PRINT_MSG("sdk> network parameters synchronized\n");

    message_t msg;
    msg.type = MSG_TYPE_INF_SDK_SDK_NETWORK_CONFIGURED;
    xQueueSend(sdk_get_queue(), &msg, 0);
  }
  else
  {
    PRINT_MSG("sdk> synchronization failed (status: %d) \n", status);
  }
}

static void transmission_result(net_status_t status, uint16_t error)
{
  message_t msg;
  if (status != NET_SUCCESS)
  {
    PRINT_MSG("sdk> data send failed (status: %d, error: %d)\n", status, error);
    msg.body.error = true;
  }
  else
  {
    PRINT_MSG("sdk> data sent\n");
    msg.body.error = false;
  }
  msg.type = MSG_TYPE_INF_SDK_UIP_TX_RESULT;
  xQueueSend(uip_get_queue(), &msg, 0);
}

static void data_received(const uint8_t *buffer, uint16_t data_size,
                          net_status_t status)
{
  if (status != NET_SUCCESS)
  {
    PRINT_MSG("sdk> data receive failed (status: %d)\n", status);
  }
  else
  {
    PRINT_MSG("sdk> data received\n");
    PRINT_HEX_BUF(buffer, data_size);

    message_t msg;
    msg.type = MSG_TYPE_INF_SDK_UIP_DATA_RCVD;
    msg.len = data_size;
    memcpy(ctx.received_data, buffer, data_size);
    msg.body.data_ptr = ctx.received_data;
    xQueueSend(uip_get_queue(), &msg, 0);
  }
}

static void timer_start(uint8_t id, uint32_t duration)
{
  // Add 1 ms more in case duration is set to 0 which causes an assert to be
  // raised in FreeRTOS kernel
  xTimerChangePeriod(ctx.timer[id].self, duration + 1, 0);
  xTimerStart(ctx.timer[id].self, 0);
}

static void timer_stop(uint8_t id)
{
  xTimerStop(ctx.timer[id].self, 0);
}

static void timer_callback(TimerHandle_t timer)
{
  uint8_t id = *((uint8_t *)pvTimerGetTimerID(timer));

  mgt_timer_timeout(id);
}

static void processing_required(void)
{
  message_t msg;
  msg.type = MSG_TYPE_REQ_SDK_SDK_PROCESS;
  xQueueSendFromISR(sdk_get_queue(), &msg, 0);
}
