# Copyright (C) 2018-2021 ACKLIO SAS - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Author: Pascal Bodin - pascal@ackl.io

import argparse
import datetime
import socket
import threading
import time

# Size of reception buffer.
REC_BUFFER_SIZE = 4096

msg_number = 0

DOWNLINK_MESSAGE = (
    "789012345678901234567890123456789"
    "01234567890123456789012345678901234567890123456789"
    "01234567890123456789012345678901234567890123456789"
    "01234567890123456789012345678901234567890123456789"
    "01234567890123456789012345678901234567890123456789"
    "01234567890123456789012345678901234567890123456789"
    )

def receive_messages(sock,addrPort):
    while True:
        uplink_bytes = sock.recv(REC_BUFFER_SIZE)
        uplink_str = uplink_bytes.decode('utf-8')
        time_str = '{:%H:%M:%S}'.format(datetime.datetime.now())
        print('\n==> (' + time_str + ') ' + uplink_str)
        # Uncomment the lines below to send a downlink reply to every received message.
        # time_str = '{:%H:%M:%S}'.format(datetime.datetime.now())
        # downlink_bytes = str.encode(time_str)
        # time.sleep(0.2)  # Wait for some time before replying.
        # sock.sendto(downlink_bytes, addrPort)
        # print('<== ' + time_str)

parser = argparse.ArgumentParser(description='Application for UpDownFragClassA sample application.')
parser.add_argument('deviceAddress', help='IPv6 device address')
parser.add_argument('devicePort', type=int, help='device port')
parser.add_argument('appAddress', help='IPv6 application address')
parser.add_argument('applicationPort', type=int, help='application port for transmission and reception')
args = parser.parse_args()

deviceAddress = args.deviceAddress
devicePort = args.devicePort
appAddress = args.appAddress
applicationPort = args.applicationPort
print('device address:      {}'.format(deviceAddress))
print('device port:         {}'.format(devicePort))
print('application address: {}'.format(appAddress))
print('application port:    {}'.format(applicationPort))

deviceAddrPort = (deviceAddress, devicePort)

s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
s.bind((appAddress, applicationPort))

rec_thread = threading.Thread(target=receive_messages, args=(s,deviceAddrPort,))
rec_thread.start()

while True:
    user_command = 'X'
    while True:
        user_input = input("Press D + Enter to send a downlink message, T + Enter to request device local time...")
        if len(user_input) > 0:
            if user_input[0] == 'D':
                user_command = 'D'
                break
            elif user_input[0] == 'd':
                user_command = 'D'
                break
            elif user_input[0] == 'T':
                user_command = 'T'
                break
            elif user_input[0] == 't':
                user_command = 'T'
                break
            else:
                print('**** Unknown command')
    if user_command == 'D':
        msg_number_str = '{:03d} - '.format(msg_number)
        time_str = '{:%H:%M:%S} - '.format(datetime.datetime.now())
        downlink_str = msg_number_str + time_str + DOWNLINK_MESSAGE
        if msg_number >= 999:
            msg_number = 0
        else:
            msg_number += 1
    elif user_command == 'T':
        downlink_str = 'getLocalTime'
    downlink_bytes = str.encode(downlink_str)
    s.sendto(downlink_bytes, deviceAddrPort)
    print('<== ' + downlink_str)
