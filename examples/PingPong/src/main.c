/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Flavien Moullec flavien@ackl.io
 */

#include <stdio.h>
#include <stdlib.h>

// For trace functions.
#include "fullsdktrace.h"
#include "time.h"

#include "fullsdkextapi.h"
#include "fullsdkfragapi.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "fullsdknet.h"
#include "platform.h"

#define RECEIVE_BUFFER_SIZE 1500
#define TRANSMISSION_BUFFER_SIZE 1500
#define APP_PAYLOAD_SIZE 100

// Assume (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE RECEIVE_BUFFER_SIZE
#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                                         \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u +              \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static bool mgt_process_request = false;
static bool app_process_request = false;
static bool app_send_data_request = false;
static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

static uint8_t app_rx_buf[RECEIVE_BUFFER_SIZE];
static uint8_t app_tx_buf[TRANSMISSION_BUFFER_SIZE];
static uint8_t app_payload[APP_PAYLOAD_SIZE] = {0};

static char *socket_src_addr = "";
static char *socket_dest_addr = "";
static char *socket_src_port = "";
static char *socket_dest_port = "";

static sdk_mode_t sdk_mode;

static TimerEvent_t sdk_timers[3];

// MGT callbacks.
static void cb_mgt_processing_required(void)
{
  mgt_process_request = true;
}

static void cb_mgt_connectivity_state(mgt_status_t status)
{
  if (status != MGT_SUCCESS)
  {
    PRINT_MSG("connectivity KO (status %d)\n", status);
    return;
  }
  PRINT_MSG("connectivity OK\n");
}

static void cb_start_timer(uint8_t id, uint32_t duration)
{
  TimerSetValue(&sdk_timers[id], duration);
  TimerStart(&sdk_timers[id]);
}

static void cb_stop_timer(uint8_t id)
{
  TimerStop(&sdk_timers[id]);
}

// NET callbacks.
static void net_transmission_result(net_status_t status, uint16_t error)
{
  if (status != NET_SUCCESS)
  {
    PRINT_MSG("transmission result KO (status %d)\n", status);
    return;
  }
  PRINT_MSG("transmission result OK\n");
}

static void net_data_received(const uint8_t *buffer, uint16_t data_size,
                              net_status_t status)
{
  PRINT_MSG("data received %d bytes - status %d\n", data_size, status);
  uint8_t b;
  for (uint16_t i = 0; i < data_size; i++)
  {
    b = buffer[i];
    PRINT_MSG("%02X ", b);
    if ((i + 1) % 10 == 0)
      PRINT_MSG("\n");
  }
  PRINT_MSG("\n");

  app_process_request = true;
  app_send_data_request = true;
}

static mgt_callbacks_t mgt_callbacks = {
    cb_mgt_processing_required,
    cb_mgt_connectivity_state,
    cb_start_timer,
    cb_stop_timer,
    NULL,
};

static net_callbacks_t net_callbacks = {net_transmission_result,
                                        net_data_received};

static void sdk_timer_1_event(void *context)
{
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  mgt_timer_timeout(2);
}

static bool init(void)
{
  PRINT_MSG("FullSDK version: %s\n", mgt_get_version());

  // SDK timers initialization.
  TimerInit(&sdk_timers[0], sdk_timer_1_event);
  TimerInit(&sdk_timers[1], sdk_timer_2_event);
  TimerInit(&sdk_timers[2], sdk_timer_3_event);

  // Configure L2 MTU.
  l2_set_mtu(50);

  // Configure socket parameters.
  l2_set_ipv4_host_addr(socket_src_addr);
  l2_set_ipv4_remote_addr(socket_dest_addr);
  l2_set_udp_src_port(socket_src_port);
  l2_set_udp_dest_port(socket_dest_port);

  // Informs the SDK regarding the application mode in order to use the correct
  // fragmentation profile.
  mgt_set_mode(sdk_mode);

  if (mgt_initialize(&mgt_callbacks, mgt_mem_block, MEM_BLOCK_SIZE,
                     MAX_MTU_SIZE, MAX_PAYLOAD_SIZE) != MGT_SUCCESS)
  {
    PRINT_MSG("Error : mgt_initialize() failed\n");
    return false;
  }

  net_status_t status = net_initialize(&net_callbacks);
  if (status != NET_SUCCESS)
  {
    PRINT_MSG("Error : net_initialize() failed (status %d)\n", status);
    return false;
  }

  // Initialize the payload.
  for (uint16_t i = 1; i < APP_PAYLOAD_SIZE; i++)
  {
    app_payload[i] = i;
  }

  if (sdk_mode == SDK_DEVICE_MODE)
  {
    app_process_request = true;
    app_send_data_request = true;
  }

  return true;
}

static bool run(void)
{
  mgt_status_t mgt_status;
  net_status_t net_status;
  while (true)
  {
    if (mgt_process_request)
    {
      mgt_process_request = false;
      mgt_status = mgt_process();
      if (mgt_status != MGT_SUCCESS)
      {
        PRINT_MSG("mgt_process() failed (status %d)\n", mgt_status);
        return false;
      }
    }
    if (app_process_request)
    {
      app_process_request = false;
      if (app_send_data_request)
      {
        app_send_data_request = false;

        app_payload[0]++;

        net_status = net_sendto(app_payload, APP_PAYLOAD_SIZE);
        if (net_status != NET_SUCCESS)
        {
          PRINT_MSG("net_sendto() failed (status %d)\n", net_status);
          return false;
        }
      }
    }
    if ((!mgt_process_request) && (!app_process_request))
    {
      platform_enter_low_power_ll();
    }
  }

  return true;
}

static int parse_params(int ac, char **av)
{
  int port;

  if (ac != 6)
  {
    PRINT_MSG("Usage: %s [mode] [ipv4_src_addr] [ipv4_dest_addr] "
              "[udp_src_port] [udp_dest_port]\n",
              av[0]);
    return EXIT_FAILURE;
  }

  if (strcmp(av[1], "client") == 0)
  {
    sdk_mode = SDK_DEVICE_MODE;
  }
  else if (strcmp(av[1], "server") == 0)
  {
    sdk_mode = SDK_APP_MODE;
  }
  else
  {
    PRINT_MSG("invalid sdk mode: %s\n", av[1]);
    return EXIT_FAILURE;
  }

  socket_src_addr = av[2];
  socket_dest_addr = av[3];
  socket_src_port = av[4];
  socket_dest_port = av[5];

  return EXIT_SUCCESS;
}

int main(int ac, char **av)
{
  // Validate parameters.
  if (parse_params(ac, av) != EXIT_SUCCESS)
  {
    return EXIT_FAILURE;
  }

  // Start ping-pong application.
  if (!init())
  {
    return EXIT_FAILURE;
  }

  if (!run())
  {
    PRINT_MSG("run the applcation\n");

    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

/**
 * For traces. Called for each trace message.
 */
void tr_handle_trace(const uint8_t *trace, uint8_t length)
{
  static const char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                             '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
  // For every byte of the binary trace message, we need two bytes in the
  // output
  // string, and a final '\0' byte.
  char out_str[TRACE_MAX_LENGTH * 2 + 1];
  for (uint8_t i = 0; i < length; i++)
  {
    out_str[2 * i] = hex[trace[i] >> 4];
    out_str[2 * i + 1] = hex[trace[i] & 0x0f];
  }
  out_str[length * 2] = '\0';
  PRINT_MSG("TRA>%s\n", out_str);
}
