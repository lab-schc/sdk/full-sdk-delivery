# PingPong

## Description

This example is an echo-reply demo application using the SDK library with the following behaviour:

* the client application initiates the communication by sending a dummy payload to the server. Then, every time it receives a packet from the server, it answers to it.
* the server application replies to the client by transmitting the same packet.

A counter byte is added in first position in the payload. Every time a packet is received by the client or the server, this counter is incremented and sent in the next transmitted packet.

## Build

```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DEXTENSION_API=orangelabs -DFRAGMENTATION_API=orangelabs -DTOOLCHAIN=gcc-native -DTARGET=default -DAPP_NAME=PingPong -DPLATFORM=linux -DL2_STACK=udp && make -C ./build
```

Add `-DTRACE_ENABLED=ON` to build the application with trace messages for FragAoE receiver.

## Examples

On the client side:

```sh
./PingPong client 127.0.0.1 127.0.0.1 5684 5683
```

On the server side:

```sh
./PingPong server 127.0.0.1 127.0.0.1 5683 5684
```
