# UpDownFragClassA

## Description

This sample application is used by the [*Class A fragmentation* test report](https://docs.google.com/document/d/168hCL-Qc1rmuHoEtpvdCMf4ujRGhND048ivPEPf16eE/edit?usp=sharing).

On cloud side, a simple application sends a message to the device, when the user hits the Enter key. Every message contains a timestamp, a sequence number and some text. Format of this message is:

```
nnn - hh:mm:ss - text
```

`nnn` is the sequence number, with three figures. It starts from `000` and is incremented with each new downlink message. When it reaches `999`, it wraps around to `000`.

`hh:mm:ss` is the timestamp.

`text` contains enough characters to make a message of 300 bytes.

On device side, *UpDownFragClassA* application joins the Acklio LoRaWAN network. It waits for downlink messages with the above format. For each received message, it writes its sequence number and timestamp to the serial-over-USB link. 

When the user pushes the user button, the application sends an uplink message. The message is short enough (10 bytes) not to be fragmented.

A message is written to the serial-over-USB link on a periodic basis, to let the user check that the application is alive.

## Build

### NUCLEO-L476RG board

Set device EUI, application EUI and application key using environment variables:

* `LORAWAN_DEVEUI`
* `LORAWAN_APPEUI`
* `LORAWAN_APPKEY`

To set data rate to adaptive or to a fixed value, use the `FULLSDK_DR` environment variable:

* set it to `ADR` to set adaptive data rate
* set it to a value between `0` and `5` to set data rate from DR0 to DR5

By default, the data rate is set to adaptive.

The build command has to be run from top directory (`full-sdk-delivery` if default name is not modified). The target is the ST NUCLEO-L476RG board:

```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_NAME=UpDownFragClassA -DL2_STACK=semtech -DPLATFORM=STM32L476RG-Nucleo \
-DFULLSDK_DR=<dataRate> -DLORAWAN_DEVEUI=<deviceEUI> -DLORAWAN_APPEUI=<applicationEUI> -DLORAWAN_APPKEY=<applicationKey> \
-DTOOLCHAIN=gcc-arm-none -DTARGET=m4 \
-DEXTENSION_API=template -DTEMPLATE_ID=ipv6udp \
-DFULLSDK_VERSION=development \
-DDEBUG_ENABLED=ON -DTRACE_ENABLED=ON && make -C ./build
```

### LNS

Create a **Web Socket server** connector.

Create a device profile with one application, with **fport** set to **0** and **Type** set to **Bytes**, and activate the above connector.

Configure the device for **Class A** and set **RX Window** to **RX1**. Assign the above profile to the device.

### IPCore

A Predefined device template can be used: `Pascal-LoRaUpDownFrag`.

Create a new flow, assign it the LNS connector created above as the source. In device profile, set **Buffering** to **0** and **Device type** to **Class A**. Complement device profile information:

* **App port**: the UDP port of the cloud application. This port has to be used as the source port by the application
* **Dev port**: the port that will be used on the device
* **Ip6 app iid**: the IID of the cloud application IPv6 address. Start the IID with `::`
* **Ip6 app prefix**: the prefix of the cloud application IPv6 address. End the prefix with `::` 

Add the device to the list of profile devices.

## Run

## Device application

To run the device application:

* connect the ST board to a USB port of a PC
* trace messages can be displayed with a terminal emulator. After a few seconds, one of them informs that the device has joined the LoRaWAN network
* hexadecimal trace messages can be decoded with `dectermtrace` application

## Cloud application

Download the NBA (network binding agent) package from IPCore website, and unzip its contents. The package is available from *Pools* page.

To run the cloud application:

* a computer connected to the Internet must be used. *Python3* must be available
* run the network binding agent:

```sh
sudo ./ipcore-nw-binding-agent-x86-v0.0.1 --agent-server-addr=<ipcoreAddress>:9995
```

* add the address the application should bind to to your local loopback interface:

```sh
sudo ip -6 addr add <applicationAddress> dev lo
```

* run the application:

```sh
python3 updownfragapp.py <deviceAddress> <devicePort> <applicationPort>
```

`<deviceAddress>` is the IPv6 address of the device, as displayed in the device profile on IPCore.

`<devicePort>` is the device port, as defined on IPCore.

`<applicationAddress>` is the application address, as defined in IPCore.

`<applicationPort>` is the application port, as defined on IPCore.
