/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Pascal Bodin - pascal@ackl.io
 *
 * Check README.md for the description of the application.
 *
 */

/* Includes ------------------------------------------------------------------*/
#include <ctype.h>
#include <stdint.h>
#include <string.h>

// For trace functions.
#include "fullsdktrace.h"
#include "time.h"

#include "fullsdkdtg.h"
#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "fullsdkschc.h"
#include "platform.h"

#define APP_VERSION "1.4.0"

/**
 * To enable low power mode, we have to undefine here for the moment
 * because we disable the low power mode for all platforms by default
 * in ./l2/<l2_name>/flags.cmake. Once we enable by default then
 * this definition can be done inside platform makefile.
 */
#undef LOW_POWER_DISABLE

// Period between two alive messages, in ms.
#define APP_ALIVE_PERIOD 30000

#ifndef ACK_RETRANSMISSION_TIMER
#define ACK_RETRANSMISSION_TIMER (1000 * 60 * 2)
#endif

#ifndef IDLE_POLLING_TIMER
#define IDLE_POLLING_TIMER (1000 * 60 * 10)
#endif

#ifndef INACTIVITY_TIMER_EXP
#define INACTIVITY_TIMER_EXP (1000 * 60 * 48)
#endif

#ifndef L2_TECH
#define L2_TECH L2A_LORA
#endif

// Maximum size of a downlink message.
#define APP_RECEIVE_MAX_LENGTH 512

// Assume (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE APP_RECEIVE_MAX_LENGTH
#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned
#define APP_TRANSMISSION_MAX_LENGTH MAX_PAYLOAD_SIZE

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                                         \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u +              \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

// Number of successive received messages before replying.
#define APP_REC_MSG_NB 3

// Number of caracters used to display message number.
#define APP_MSG_NB_CHAR_NB 4

#define CLASS_A_MAX_ACK_REQ 3

// Uplink message.
static char APP_UPLINK_MSG[] = "0000 - azertyuiopqsdfghjklmwxcvbn0123456789-+"
                               "azertyuiopqsdfghjklmwxcvbn0123456789-+"
                               "azertyuiopqsdfghjklmwxcvbn0123456789-+\n";

// Data rate: -1 to indicate ADR otherwise data rate (0 to 5) read
// from environment variable. Default is 3.
static int8_t datarate;

// Flag signaling that MGT layer requires processing.
static bool mgt_process_request;

// Flag signaling that the main task requires processing.
static bool app_process_request;
// Events handled by the main task.
static bool app_connectivity_event;
static bool app_reception_event;
static bool app_alive_timer_event;
static bool app_user_button_event;

// Counter of sent messages.
static uint32_t sent_messages_counter;

// Timer for alive messages.
static TimerEvent_t app_alive_timer;

// SDK timers.
static TimerEvent_t sdk_timers[5];

static uint8_t transmission_buffer[APP_TRANSMISSION_MAX_LENGTH];

// Application reception buffer.
static uint16_t app_received_data_l;
static uint8_t app_received_data[APP_RECEIVE_MAX_LENGTH];
static dtg_status_t app_reception_status;

// Array of 10 successive received messages.
static uint8_t received_sequences[APP_REC_MSG_NB];
// Index in above array of next message to be received.
static uint8_t rec_seq_index;

// Device configuration.
static const dtg_sockaddr_t socket_addr = {
    {0x15, 0x15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x02}, // Address.
    {0x30, 0x39}                                               // Port.
};
static const dtg_sockaddr_t dest_addr = {
    {0x15, 0x15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01}, // Address.
    {0x30, 0x39}                                               // Port.
};

static dtg_socket_t *socket = NULL;

// Declaration of functions.
static void mgt_processing_required(void);
static void mgt_connectivity_state(mgt_status_t state);
static void app_main_task(void);
static void app_alive_timer_event_fct(void *context);
static void sdk_timer_1_event(void *context);
static void sdk_timer_2_event(void *context);
static void sdk_timer_3_event(void *context);
static void sdk_timer_4_event(void *context);
static void sdk_timer_5_event(void *context);

static void mgt_start_timer(uint8_t id, uint32_t duration);
static void mgt_stop_timer(uint8_t id);

static void dtg_trans_result(const dtg_socket_t *socket, dtg_status_t status,
                             uint16_t error);
static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                              uint16_t data_size,
                              const dtg_sockaddr_t *src_addr,
                              dtg_status_t status);

/**
 *
 */
int main(void)
{

  // Platform initialization.
  platform_hw_init();

  // Initialize system to enter sleep mode instead of standby mode.
  platform_configure_sleep_mode();

  PRINTNOW();
  PRINT_MSG("\n\nUpDownFragClassA - " APP_VERSION "\n");

// Get datarate from environment variable.
#if defined(USE_ADR)
  datarate = -1;
#else
#if defined(APP_DR)
#if APP_DR < 0 || APP_DR > 5
  PRINTNOW();
  PRINT_MSG("Error: datarate must be in [0, 5]\n");
  datarate = 3;
#else
  datarate = APP_DR;
#endif
#else
  datarate = 3;
#endif
#endif
  if (datarate == -1)
  {
    PRINTNOW();
    PRINT_MSG("Datarate will be set to adaptive\n");
  }
  else
  {
    PRINTNOW();
    PRINT_MSG("Datarate will be set to DR%d\n", datarate);
  }

  mgt_process_request = false;
  app_process_request = false;

  app_connectivity_event = false;
  app_reception_event = false;
  app_alive_timer_event = false;
  app_user_button_event = false;

  rec_seq_index = 0;
  sent_messages_counter = 0;

  TimerInit(&app_alive_timer, app_alive_timer_event_fct);
  TimerSetValue(&app_alive_timer, APP_ALIVE_PERIOD);
  TimerStart(&app_alive_timer);

  TimerInit(&sdk_timers[0], sdk_timer_1_event);
  TimerInit(&sdk_timers[1], sdk_timer_2_event);
  TimerInit(&sdk_timers[2], sdk_timer_3_event);
  TimerInit(&sdk_timers[3], sdk_timer_4_event);
  TimerInit(&sdk_timers[4], sdk_timer_5_event);

  l2_set_technology(L2_TECH);

  // Initialize MGT layer.
  mgt_callbacks_t callbacks = {mgt_processing_required, mgt_connectivity_state,
                               mgt_start_timer, mgt_stop_timer};

  mgt_status_t mgt_status =
      mgt_initialize(&callbacks, mgt_mem_block, MEM_BLOCK_SIZE, MAX_MTU_SIZE,
                     MAX_PAYLOAD_SIZE);
  if (mgt_status != MGT_SUCCESS)
  {
    PRINTNOW();
    PRINT_MSG("Error in MGT initialization: %d\n", mgt_status);
    platform_error_handler();
  }

  mgt_set_template_param(0, &socket_addr.addr[0], 8);
  mgt_set_template_param(1, &socket_addr.addr[8], 8);
  mgt_set_template_param(2, &dest_addr.addr[0], 8);
  mgt_set_template_param(3, &dest_addr.addr[8], 8);
  mgt_set_template_param(4, &socket_addr.port[0], IP_PORT_LENGTH_BYTES);
  mgt_set_template_param(5, &dest_addr.port[0], IP_PORT_LENGTH_BYTES);

  // Initialize DTG interface
  dtg_callbacks_t dtg_callbacks = {dtg_trans_result, dtg_data_received};
  dtg_status_t dtg_status = dtg_initialize(&dtg_callbacks);
  if (dtg_status != DTG_SUCCESS)
  {
    PRINTNOW();
    PRINT_MSG("Error in dtg_initialize: %d\n", dtg_status);
    platform_error_handler();
  }

  // Create the socket.
  socket = dtg_socket(transmission_buffer, APP_TRANSMISSION_MAX_LENGTH);
  if (socket == DTG_INVALID_SOCKET)
  {
    PRINTNOW();
    PRINT_MSG("Error in socket creation\n");
    platform_error_handler();
  }
  dtg_status = dtg_bind(socket, &socket_addr);
  if (dtg_status != DTG_SUCCESS)
  {
    PRINTNOW();
    PRINT_MSG("Error in socket bind: %d\n", dtg_status);
    platform_error_handler();
  }

  while (true)
  {
    // Process MGT layer events.
    if (mgt_process_request)
    {
      mgt_process_request = false;
      mgt_status = mgt_process();
      if (mgt_status != MGT_SUCCESS)
      {
        PRINTNOW();
        PRINT_MSG("Error in mgt_process: %d\n", mgt_status);
      }
    }

    // Process main task events.
    if (app_process_request)
    {
      app_process_request = false;
      app_main_task();
    }

    // Sleep, if possible.
    BEGIN_CRITICAL_SECTION();
    // If an interrupt has occurred after DISABLE_IRQ, it is kept pending
    // and Cortex will not enter low power anyway.
    if ((!mgt_process_request) && (!app_process_request))
    {
#ifndef LOW_POWER_DISABLE
      platform_enter_low_power_ll();
#endif
    }
    END_CRITICAL_SECTION();
  }
}

/******************************************************************************
 * Functions.
 *****************************************************************************/

/**
 *
 */
static void app_main_task(void)
{

  if (app_connectivity_event)
  {
    app_connectivity_event = false;
    PRINTNOW();
    PRINT_MSG("Joined\n");
    // Set data rate.
    if (datarate == -1)
    {
      l2_set_adr(true);
      PRINTNOW();
      PRINT_MSG("Data rate set to adaptive\n");
    }
    else
    {
      l2_set_dr(datarate);
      PRINTNOW();
      PRINT_MSG("Data rate set to DR%d\n", datarate);
    }
    // Request switch to class A.
    if (l2_set_class('A') != L2_SUCCESS)
    {
      PRINTNOW();
      PRINT_MSG("Error in class A switch request\n");
    }
    schc_set_max_ack_req(CLASS_A_MAX_ACK_REQ);
    schc_set_inactivity_timer(INACTIVITY_TIMER_EXP);
    schc_set_ack_retransmission_timer(ACK_RETRANSMISSION_TIMER);
    schc_set_idle_polling_timer(IDLE_POLLING_TIMER);
    schc_set_polling_status(true, true);
  }

  if (app_reception_event)
  {
    app_reception_event = false;
    // The only processing we perform in this example is to display
    // start of received data.
    bool rs;
    switch (app_reception_status)
    {
      case DTG_SUCCESS:
        PRINTNOW();
        PRINT_MSG("Datagram received - length: %d\n", app_received_data_l);
        PRINT_HEX_BUF(app_received_data, app_received_data_l);
        if (app_received_data_l < 14)
        {
          PRINT_MSG("Message too short, ignored\n");
          break;
        }
        char displayed_msg[14 + 1];
        memcpy(displayed_msg, app_received_data, 14);
        displayed_msg[14] = '\0';
        PRINT_MSG("%s\n", displayed_msg);
        break;
      case DTG_BUFFER_ERR:
        PRINTNOW();
        PRINT_MSG("Receive buffer is too small\n");
        break;
      case DTG_OVERRUN_ERR:
        PRINTNOW();
        PRINT_MSG("Receive overrun\n");
        break;
      default:
        PRINTNOW();
        PRINT_MSG("Unknown status: %d\n", app_reception_status);
        break;
    }
  }

  if (app_alive_timer_event)
  {
    app_alive_timer_event = false;
    PRINTNOW();
    PRINT_MSG("Alive\n");
    TimerSetValue(&app_alive_timer, APP_ALIVE_PERIOD);
    TimerStart(&app_alive_timer);
  }

  if (app_user_button_event)
  {
    app_user_button_event = false;
    sent_messages_counter++;
    PRINT_MSG("Sending uplink message %04d\n", sent_messages_counter);
    PRINT_MSG(APP_UPLINK_MSG, "%04d", sent_messages_counter);
    APP_UPLINK_MSG[4] = ' ';
    dtg_status_t dtg_status =
        dtg_sendto(socket, APP_UPLINK_MSG, strlen(APP_UPLINK_MSG), &dest_addr);
    if (dtg_status != DTG_SUCCESS)
    {
      PRINTNOW();
      PRINT_MSG("Error from dtg_sendto(): %d\n", dtg_status);
    }
  }
}

/**
 *
 */
void user_button_callback(void *context)
{
  app_user_button_event = true;
  app_process_request = true;
}

/**
 *
 */
static void app_alive_timer_event_fct(void *context)
{
  app_alive_timer_event = true;
  app_process_request = true;
}

static void sdk_timer_1_event(void *context)
{
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  mgt_timer_timeout(4);
}

/******************************************************************************
 * FullSDK callbacks.
 *****************************************************************************/

/**
 *
 */
static void mgt_processing_required(void)
{
  mgt_process_request = true;
}

/**
 *
 */
static void mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    app_connectivity_event = true;
    app_process_request = true;
  }
  else
  {
    PRINTNOW();
    PRINT_MSG("MgtConnectivityState error: %d\n", state);
  }
}

static void mgt_start_timer(uint8_t id, uint32_t duration)
{
  TimerSetValue(&sdk_timers[id], duration);
  TimerStart(&sdk_timers[id]);
}

static void mgt_stop_timer(uint8_t id)
{
  TimerStop(&sdk_timers[id]);
}

/**
 *
 */
static void dtg_trans_result(const dtg_socket_t *socket, dtg_status_t status,
                             uint16_t error)
{
  PRINTNOW();
  PRINT_MSG("Transmission status: %d\n", status);
}

/**
 *
 */
static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                              uint16_t data_size,
                              const dtg_sockaddr_t *src_addr,
                              dtg_status_t status)
{
  // Tell main loop.
  app_received_data_l = data_size;
  app_reception_status = status;
  // Ensure no buffer overflow.
  if (data_size > APP_RECEIVE_MAX_LENGTH)
  {
    PRINTNOW();
    PRINT_MSG("Too much data received: %d\n", data_size);
    data_size = APP_RECEIVE_MAX_LENGTH;
  }
  // Transfer received data to application reception buffer.
  if (status == DTG_SUCCESS)
  {
    memcpy(app_received_data, p_buffer, data_size);
  }
  app_reception_event = true;
  app_process_request = true;
}

/**
 * For traces. Called for each trace message.
 */
void tr_handle_trace(const uint8_t *trace, uint8_t length)
{
  static const char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                             '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
  // For every byte of the binary trace message, we need two bytes in the
  // output
  // string, and a final '\0' byte.
  char out_str[TRACE_MAX_LENGTH * 2 + 1];
  for (uint8_t i = 0; i < length; i++)
  {
    out_str[2 * i] = hex[trace[i] >> 4];
    out_str[2 * i + 1] = hex[trace[i] & 0x0f];
  }
  out_str[length * 2] = '\0';
  PRINT_MSG("TRA>%s\n", out_str);
}
