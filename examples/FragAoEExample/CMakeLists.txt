add_library(${APP_LIB}
  OBJECT
  ${CMAKE_CURRENT_LIST_DIR}/src/main.c
)

if(${FULLSDK_DR} STREQUAL "ADR")
  add_compile_definitions(USE_ADR)
else()
  add_compile_definitions(APP_DR=${FULLSDK_DR})
endif()

target_include_directories(${APP_LIB} PRIVATE
  ${L2_INC_DIR}
  ${FULLSDK_INC_DIR}
  ${PLATFORM_INC_DIR})
