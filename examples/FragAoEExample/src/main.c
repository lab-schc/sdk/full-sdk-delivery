/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Pascal Bodin - pascal@ackl.io
 *         Jerome Elias - jerome.elias@ackl.io
 *
 * Based on ST's End_Node example. Check license here: www.st.com/SLA0044.
 *
 * This sample application provides an example of how to use the FullSDK.
 *
 * It requests to send a message of 256 bytes on a periodic basis.
 *
 * Compression is disabled.
 *
 * It displays any received message.
 *
 * On IPCore, ADR must also be activated if it is activated in this example.
 *
 */

#include <stdint.h>

#include "fullsdkextapi.h"
#include "fullsdkdtg.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "platform.h"

/**
 * To enable low power mode, we have to undefine here for the moment
 * because we disable the low power mode for all platforms by default
 * in ./l2/<l2_name>/flags.cmake. Once we enable by default then
 * this definition can be done inside platform makefile.
 */
#undef LOW_POWER_DISABLE

// Size of buffer used to store application payload.
#define APP_PAYLOAD_MAX_LENGTH 256

// Period between two successive transmission requests, in ms.
#define APP_TX_PERIOD 20000
// Period between two "alive" messages, in ms.
#define APP_ALIVE_PERIOD 30000

// Assume (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE APP_PAYLOAD_MAX_LENGTH
#define APP_TRANSMISSION_MAX_LENGTH MAX_PAYLOAD_SIZE + 100

// The number of parameters for this template is 6.
#define TEMPLATE_PARAM_NB 6

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                            \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u + \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

/* tx timer callback function*/
static void on_app_timer1_event(void *context);
static void on_alive_timer_event(void *context);
static void sdk_timer_1_event(void *context);
static void sdk_timer_2_event(void *context);
static void sdk_timer_3_event(void *context);
static void sdk_timer_4_event(void *context);
static void sdk_timer_5_event(void *context);
static void sdk_timer_6_event(void *context);

// Callbacks for DTG layer.
static void dtg_transmission_result(const dtg_socket_t *socket,
                                    dtg_status_t status, uint16_t error);
static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                              uint16_t data_size, const dtg_sockaddr_t *p_remote,
                              dtg_status_t status);

// Callbacks for MGT layer.
static void mgt_processing_required(void);
static void mgt_connectivity_state(mgt_status_t state);
static void mgt_start_timer(uint8_t id, uint32_t duration);
static void mgt_stop_timer(uint8_t id);
static void mgt_bootstrap_result(mgt_status_t state);

// Application main task.
static void app_main_task(void *args);

/* Private variables ---------------------------------------------------------*/

// For FullSDK events.
static bool mgt_process_request = false;

// For application events.
static bool app_process_request = false;
static bool app_connectivity_event = false;
static bool app_boostrap_event = false;
static bool app_timer1_event = false;
static bool app_alive_timer_event = false;
static bool app_tx_done_event = false;

// Current state of application transmission state machine.
typedef enum
{
  TX_WAIT_JOIN,
  TX_WAIT_SYNC,
  TX_WAIT_SEND_DATA
} appTxState_t;

static appTxState_t app_tx_current_state = TX_WAIT_JOIN;

// Application payload. After a send request, its contents must not be
// modified until transmission result callback is called.
static uint8_t app_payload[APP_PAYLOAD_MAX_LENGTH];

// Transmission buffer.
static uint8_t tx_buffer[APP_TRANSMISSION_MAX_LENGTH];

// Counter value sent as part of the application payload.
static uint16_t counter = 0;

static TimerEvent_t app_timer1;
// Timer used to check application liveliness.
static TimerEvent_t alive_timer;
static TimerEvent_t sdk_timers[6];

// Result of the transmission.
static dtg_status_t tx_status;
static uint16_t tx_error;

// Flag informing that we transmission result has been called.
static bool transmission_result_called = false;

static dtg_sockaddr_t dest_addr = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // addr
    {0, 0}                                            // port
};
// Socket address.
static dtg_sockaddr_t socket_addr = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // addr
    {0, 0}                                            // port
};
static dtg_socket_t *socket;

static int8_t datarate = -1; // -1 to indicate ADR otherwise datarate

// Debug to know how long a call function lasts
// Set DURATION_DEBUG to 1 to print function process duration
#define DURATION_DEBUG 0

#ifdef DURATION_DEBUG
// Time debug
static uint32_t fixed_time = 0;
#endif

/* Private functions ---------------------------------------------------------*/

int main(void)
{
  // Platform initialization.
  platform_hw_init();

  /* Initialize system to enter sleep mode instead of standby mode */
  platform_configure_sleep_mode();

  PRINT_MSG("app> FragAoEExample BareMetal example\n\r");
  PRINT_MSG("app> FullSDK version: %s\n", mgt_get_version());

#if defined(APP_DR)
#if APP_DR < 0 || APP_DR > 5
  PRINT_MSG("app> error - datarate must be between 0 and 5\n\r");
#define APP_DR 3
#endif
  datarate = APP_DR;
#endif

  // The first ten bytes of the payload, this is mandatory for the
  // monitoring the device
  char device_name[] = {"SDK_sx1276"};

  // Prepare a large payload, that will require fragmentation.
  // Bytes 11 and 12 will be replaced by counter MSB and counter LSB.
  for (uint16_t i = 0; i < APP_PAYLOAD_MAX_LENGTH; i++)
  {
    if (i < 10)
    {
      app_payload[i] = device_name[i];
    }
    else
    {
      app_payload[i] = i;
    }
  }

  // Initialize SDK timers.
  platform_timer_add(&sdk_timers[0], 0, sdk_timer_1_event, NULL);
  platform_timer_add(&sdk_timers[1], 1, sdk_timer_2_event, NULL);
  platform_timer_add(&sdk_timers[2], 2, sdk_timer_3_event, NULL);
  platform_timer_add(&sdk_timers[3], 3, sdk_timer_4_event, NULL);
  platform_timer_add(&sdk_timers[4], 4, sdk_timer_5_event, NULL);
  platform_timer_add(&sdk_timers[5], 5, sdk_timer_6_event, NULL);

  // Initialize MGT layer.
  mgt_callbacks_t callbacks = {mgt_processing_required, mgt_connectivity_state,
                               mgt_start_timer, mgt_stop_timer, mgt_bootstrap_result};

#ifdef DURATION_DEBUG
  // Get current time and save value
  fixed_time = platform_get_current_time();
#endif

  mgt_status_t mgtStatus =
      mgt_initialize(&callbacks, mgt_mem_block, MEM_BLOCK_SIZE, MAX_MTU_SIZE,
                     MAX_PAYLOAD_SIZE);

#ifdef DURATION_DEBUG
  // Get elapsed time since since the mgt_initialize() function call
  PRINT_MSG("sdk> mgt_initialize() process duration: %u ms\n\r", platform_get_elpased_time(fixed_time));
#endif

  // Check mgtStatus
  if (mgtStatus != MGT_SUCCESS)
  {
    PRINT_MSG("sdk> error in MGT initialization: %d\n\r", mgtStatus);
  }

  app_main_task(NULL);
}

/**
 *
 */
static void dtg_transmission_result(const dtg_socket_t *socket,
                                    dtg_status_t status, uint16_t error)
{
  PRINT_MSG("sdk> dtg_transmission_result called\n");
  transmission_result_called = true;
  tx_status = status;
  tx_error = error;
  app_process_request = true;
}

/**
 *
 */
static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                              uint16_t data_size, const dtg_sockaddr_t *p_remote,
                              dtg_status_t status)
{
  PRINT_MSG("sdk> dtg_data_received - %d bytes - status: %d\n\r", data_size,
            status);
  uint8_t b;
  for (uint8_t i = 0; i < data_size; i++)
  {
    b = p_buffer[i];
    PRINT_MSG("%02X ", b);
    if ((i + 1) % 10 == 0)
      PRINT_MSG("\n\r");
  }
  PRINT_MSG("\n\r");
}

static void mgt_processing_required(void)
{
  mgt_process_request = true;
}

static void mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    PRINT_MSG("sdk> mgt_connectivity_state - OK\n\r");
    app_connectivity_event = true;
    app_process_request = true;
  }
}

static void mgt_bootstrap_result(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    PRINT_MSG("sdk> mgt_bootstrap_result - OK\n");
    app_boostrap_event = true;
    app_process_request = true;
  }
  else
  {
    PRINT_MSG("sdk> mgt_bootstrap_result - KO: %d\n\r", state);
  }
}

static void OnTxTimerEvent(void *context)
{
  app_timer1_event = true;
  app_process_request = true;
}

static void OnAliveTimerEvent(void *context)
{
  app_alive_timer_event = true;
}

static void mgt_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&sdk_timers[id], duration);
  platform_timer_start(&sdk_timers[id]);
}

static void mgt_stop_timer(uint8_t id)
{
  platform_timer_stop(&sdk_timers[id]);
}

static void sdk_timer_1_event(void *context)
{
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  mgt_timer_timeout(5);
}

static void app_main_task(void *args)
{
  while (true)
  {
    if (mgt_process_request)
    {
      mgt_process_request = false;
      mgt_status_t mgtStatus = mgt_process();
      if (mgtStatus != MGT_SUCCESS)
      {
        PRINT_MSG("app> error in mgt_process: %d\n\r", mgtStatus);
      }
    }
    if (app_process_request)
    {
      dtg_status_t dtgStatus;

      app_process_request = false;

      switch (app_tx_current_state)
      {
      case TX_WAIT_JOIN:
        if (app_connectivity_event)
        {
          PRINT_MSG("sdk> +JOINED\n");
          app_connectivity_event = false;

          if (datarate == -1)
          {
            l2_set_adr(true);
            PRINT_MSG("app> ADR is enabled\n\r");
          }
          else
          {
            l2_set_dr(datarate);
            PRINT_MSG("app> DR set to %d\n\r", datarate);
          }

          // Request switch to class C.
          if (l2_set_class('C') != L2_SUCCESS)
          {
            PRINT_MSG("sdk> error - class C switch request\n");
          }
          // Execute SYNC process.
          mgt_status_t mgt_status = mgt_sync_bootstrap(15000, 3);
          if (mgt_status != MGT_SUCCESS)
          {
            PRINT_MSG("sdk> error in mgt_sync_bootstrap: %d\n", mgt_status);
            platform_error_handler();
          }
          // Wait for SYNC response.
          app_tx_current_state = TX_WAIT_SYNC;
          break;
        }
        // At this stage, not a connectivity event. Error.
        PRINT_MSG("app> error - not a connectivity event\n\r");
        break;
      case TX_WAIT_SYNC:
        if (app_boostrap_event)
        {
          PRINT_MSG("sdk> +SYNC\n");
          app_boostrap_event = false;

          // Get all parameters from template
          uint16_t param_value_len = 0;
          uint8_t *param_values[TEMPLATE_PARAM_NB];
          memset(param_values, 0, sizeof(param_values));
          for (uint8_t i = 0; i < TEMPLATE_PARAM_NB; i++)
          {
            mgt_status_t mgt_status = mgt_get_template_param(i, &param_values[i], &param_value_len);
            if (mgt_status != MGT_SUCCESS)
            {
              PRINT_MSG("sdk> error in mgt_get_template_param: %d\n", mgt_status);
              platform_error_handler();
            }
          }

          // Then, configure socket_addr and dest_addr
          // Device Ipv6
          memcpy(socket_addr.addr, param_values[0], 8);
          memcpy(&socket_addr.addr[8], param_values[1], 8);
          // App Ipv6
          memcpy(dest_addr.addr, param_values[2], 8);
          memcpy(&dest_addr.addr[8], param_values[3], 8);
          // Device port
          memcpy(socket_addr.port, param_values[4], sizeof(socket_addr.port));
          // App port
          memcpy(dest_addr.port, param_values[5], sizeof(dest_addr.port));

          // Initialize DTG interface to create and initialize DTG socket
          dtg_callbacks_t dtg_callbacks = {dtg_transmission_result, dtg_data_received};
          dtg_status_t dtg_status = dtg_initialize(&dtg_callbacks);
          if (dtg_status != DTG_SUCCESS)
          {
            PRINT_MSG("sdk> error in dtg_initialize: %d\n", dtg_status);
            platform_error_handler();
          }

          // dtg_transmission_result will never be called but is required.
          socket = dtg_socket(tx_buffer, APP_TRANSMISSION_MAX_LENGTH);
          if (socket == DTG_INVALID_SOCKET)
          {
            PRINT_MSG("sdk> error in socket creation\n");
            platform_error_handler();
          }
          dtg_status = dtg_bind(socket, &socket_addr);
          if (dtg_status != DTG_SUCCESS)
          {
            PRINT_MSG("sdk> error in socket bind: %d\n", dtg_status);
            platform_error_handler();
          }

          // Wait after join before sending data.
          platform_timer_add(&app_timer1, 6, OnTxTimerEvent, NULL);
          platform_timer_set_duration(&app_timer1, APP_TX_PERIOD);
          platform_timer_start(&app_timer1);
          platform_timer_add(&alive_timer, 7, OnAliveTimerEvent, NULL);
          platform_timer_set_duration(&alive_timer, APP_ALIVE_PERIOD);
          platform_timer_start(&alive_timer);
          //
          app_tx_current_state = TX_WAIT_SEND_DATA;
          app_tx_done_event = true;
          break;
        }
        // At this stage, not a boostrap event. Error.
        PRINT_MSG("app> error - not a boostrap event\n\r");
        break;
      case TX_WAIT_SEND_DATA:
        // Send a new uplink packet to be fragmented.
        if (app_tx_done_event)
        {
          PRINT_MSG("app> sending message %u...\n\r", counter);

          // Request to send a message.
          app_payload[10] = (uint8_t)(counter >> 8);
          app_payload[11] = (uint8_t)counter;
          dtgStatus = dtg_sendto(socket, app_payload,
                                 APP_PAYLOAD_MAX_LENGTH, &dest_addr);
          if (dtgStatus != DTG_SUCCESS)
          {
            PRINT_MSG("sdk> error on send - retry later : %d\n\r",
                      dtgStatus);
            // Stay in same state and restart timer.
            platform_timer_set_duration(&app_timer1, APP_TX_PERIOD);
            platform_timer_start(&app_timer1);
            break;
          }
          // At this stage, TX request success.
          // Increment counter.
          app_tx_done_event = false;
          counter++;
          break;
        }
        // Handle end of transmission session.
        if (transmission_result_called)
        {
          // The transmission session ends. We can send a new packet.
          transmission_result_called = false;
          if (tx_status == DTG_SUCCESS)
          {
            PRINT_MSG("app> transmission result: OK\n");
          }
          else
          {
            PRINT_MSG("app> transmission result: KO (status %d)\n",
                      tx_status);
          }
          app_tx_done_event = true;
          app_process_request = true;
          break;
        }

        PRINT_MSG("app> - waiting...\n\r");
        // Wait before next TX.
        platform_timer_set_duration(&app_timer1, APP_TX_PERIOD);
        platform_timer_start(&app_timer1);
        // At this stage, not a timer event. Error.
        break;
      default:
        PRINT_MSG("app> error: unknown state: %d\n\r",
                  app_tx_current_state);
      }
    }

    if (app_alive_timer_event)
    {
      app_alive_timer_event = false;
      PRINTNOW();
      PRINT_MSG("app> alive\n\r");
      platform_timer_set_duration(&alive_timer, APP_ALIVE_PERIOD);
      platform_timer_start(&alive_timer);
    }

    /* If a flag is set at this point, mcu must not enter low power and must
     * loop */
    BEGIN_CRITICAL_SECTION();
    /* if an interrupt has occurred after DISABLE_IRQ, it is kept pending and
     * cortex will not enter low power anyway  */
    if ((!mgt_process_request) && (!app_process_request))
    {
#ifndef LOW_POWER_DISABLE
      platform_enter_low_power_ll();
#endif
    }
    END_CRITICAL_SECTION();
  }
}