# FragAoEExample

## Description

*FragAoEExample* application show how to send large uplink messages using ACKOnError fragmentation

The application uses version 4.6.0 of semtech stack and support the following boards:

* ST NUCLEO-L476RG
* Semtech SX1276MB1MAS

Template ipv6udp and synchronization env variables must be set to compile app.

Usage:

* To enable ADR

```
FULLSDK_DR=ADR
```

* To enable particular datarate (x must be between 0 and 5)
```
FULLSDK_DR=x
```

## Build

```sh
make clean && make app FULLSDK_VERSION=dev EXTENSION_API=template TEMPLATE_ID=ipv6udp SYNC_ENABLED=1 APP_NAME=FragAoEExample PLATFORM=STM32L476RG-Nucleo TOOLCHAIN=gcc-arm-none TARGET=m4 L2_STACK=semtech LORAWAN_DEVEUI=1111111111111111 LORAWAN_APPEUI=1111111111111111 LORAWAN_APPKEY=11111111111111111111111111111111
```
