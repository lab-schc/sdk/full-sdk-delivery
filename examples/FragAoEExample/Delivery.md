# FullSDK delivery target content

The fullSDK delivery package contains:

* `lib` directory containing FullSDK static libraries compiled for ARM Cortex-M0+ and ARM Cortex-M4 targets.
* `App_example` directory including an application example which demonstrates how FullSDK is used to send uplink packets using *Ack on Error* fragmentation type.
* `L2a_layer_example` directory including an example of L2 Adaptation layer which uses semtech LoRaWAN stack to interact with the L2 layer.

In case you need support, you can always contact us via Acklio Service Desk where you can also find the online documentation of FullSDK such as FullSDK Concepts and FullSDK Reference Manual.
