# SDKTunApp

## Description

This application sets up a TUN network interface on a Linux system coupled with the full-sdk.
On Linux based systems, all uplink packets going to an IPv6 address matching the prefix set on the TUN interface will be intercepted by this application.
Those packets will then be sent to the full-sdk which will transmit them immediately as SCHC packets.

This application aims to centralize, in the future, every TUN examples.

## Supported extension APIs

Currently, this example is only supporting the following extension API:

* template
  * Options:
    - `-DTEMPLATE_ID=<template_id>` (from full-sdk/templates/catalog)

### Specificity of dynamic templates

TEMPLATE_ID is set to `dynamic`

At compilation time, `MEMORY_AREA_SIZE` variable can be set and corresponds to the size of the memory area provided for the template provisioning.  
The default value is `16000` bytes.  
You might need to change this default value if you are using a large number of rules or template parameters.

`MEMORY_AREA_SIZE` is also used to define the memory array for payload provisioning.

At the root of the repository, run the following command as for a static template:

```sh
./build/linux/SDKTunApp [json_config_file]
```

## Supported L2s

* at_risinghf
* at_risinghf_p2p
* nb_iot_ex82
* nb_iot_mosh
* nb_iot_slm
* udp

## DTLS

This application can use the SDK's DTLS layer which allows to encrypt traffic on the fly using the DTLS protocol

To enable it use the following option: `-DDTLS_ENABLED=ON`
The DTLS extension Server Name Indication can be activated by adding the following option to the compile line: `-DDTLS_USE_SNI=ON`


Options:
  - `-DDTLS_SEED=<seed>` seed used for RNG Example: `UwT6RrlxOUv10eocIuOw0UjtuogOiVBk`

Set the `DTLS_SESSION_RESUMPTION` environment variable to `true` in order to use the DTLS session resumption feature

## Sychronization

Synchronization can be enabled using the following option: `-DSYNC_ENABLED=ON`

## Template using SID/YANG registry`

Template using SID/YANG registry can be enabled using the following option: `-DEXTENSION_API=template-sid`

## Building

From the top of the repository, run the follow command:

```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_NAME=SDKTunApp -DTOOLCHAIN=gcc-native -DTARGET=default -DPLATFORM=linux -DL2_STACK=<l2> -DEXTENSION_API=template -DTEMPLATE_ID=<template name> -DFRAGMENTATION_API=<fragmentation_api> <API/L2/DTLS/SYNC options> && make -C ./build
```

Replace `<template name>` by one of the available template in `full-sdk/templates/catalog` and `<l2>` by one of the available L2s.
`<fragmentation_api>` can be explicitly set to one of the extra fragmentation profiles available in the fullSDK library.

### Examples of build command
For instance, without DTLS:

```sh
cmake -S . -B ./build/ -DFULLSDK_VERSION=dev -DAPP_NAME=SDKTunApp -DPLATFORM=linux -DTOOLCHAIN=gcc-native -DTARGET=default -DL2_STACK=<udp|udp6> -DEXTENSION_API=template -DTEMPLATE_ID=<template name> -DSYNC_ENABLED=ON && make -C ./build
```
Or with DTLS:

```sh
cmake -S . -B ./build/ -DFULLSDK_VERSION=dev -DAPP_NAME=SDKTunApp -DPLATFORM=linux -DTOOLCHAIN=gcc-native -DTARGET=default -DL2_STACK=<udp|udp6> -DEXTENSION_API=template -DTEMPLATE_ID=<template name> -DDTLS_ENABLED=ON -DGENERIC_COMP=ON -DSYNC_ENABLED=ON && make -C ./build
```

### Core compression

Alternatively, you could set variable `SCHC_CORE_MODE` in order to enable SCHC Core compression / decompression.
```sh
make -DSCHC_CORE_MODE=ON -DFULLSDK_VERSION=dev -DAPP_NAME=SDKTunApp -DTOOLCHAIN=gcc-native -DTARGET=default -DPLATFORM=linux -DL2_STACK=<l2> -DEXTENSION_API=<extension_api> -DFRAGMENTATION_API=<fragmentation_api> <API/L2/DTLS/SYNC options> && make -C ./build
```
Core compression means **compressing as a SCHC gateway : revert device and application fields during compression**, the concerned fields are :
- Device IP address prefix
- Device IP address iid
- Application IP address prefix
- Application IP address iid
- Device UDP port (if UDP)
- Application UDP port (if UDP)

Core decompression does not revert any field.

By default, device compression mode is enabled, it reverts those fields only during decompression and revert nothing during compression.

#### Point to point
The core compression mode enables point to point communication in a star shaped network, 
with one SDKTunApp running in Core compression and one or more others running in Device compression.

See [l2/at_risinghf_p2p/README.md](../../l2/at_risinghf_p2p/README.md) for an example running this configuration.

## Tun interface configuration

Depending on the template you want to use, you'll need to setup an IPv4 or an IPv6 TUN client.  
You can follow one of the two sections below depending on your usecase.

### IPv6 setup

Run `sudo ./common/scripts/ipv6_tun_client.sh <dev ipv6> <app prefix>/64` to setup the client TUN interface.  
Replace `<dev ipv6>`, by the device IPv6 address configured on IPCore.  
Replace `<app prefix>`, by the IPv6 application prefix configured on IPCore.  

### IPv4 setup

Run `sudo ./common/scripts/ipv4_tun_client.sh <dev ipv4> <app ipv4>/24` to setup the client TUN interface.  
Replace `<dev ipv4>`, by the IPv4 device address configured on IPCore.  
Replace `<app ipv4>`, by the IPv4 application address configured on IPCore.

## Running the example

At the root of the repository, run the following command to run the example:

```sh
./build/linux/SDKTunApp [json_config_file]
```

where `json_config_file` is the JSON configuration file to initialize the application with a template, static or dynamic.
Some JSON examples can be found in `SDKTunApp/config` folder.

## Enabling SDK's traces

See the next section `Configuration`. Field `trace_enabled`.

## Configuration

This application gets its configuration through JSON files (examples in `config`)

Here is a short summary of the different parameters that can be used there :

- `trace_enabled` : Showing SDK's trace messages. Default: false.
- `tun_name` : The name of the TUN interface the application will attach to
- `l2` : The configuration for the L2 used by the application
  - `serial_port` : Serial port to use to connect to the L2 (nb_iot L2s)
  - `host_ipv4` : IPv4 address of the device (IP L2)
  - `host_port` : UDP port of the device (IP L2)
  - `remote_ipv4` : IPv4 address of the remote peer (IP L2)
  - `remote_port` : UDP port of the remote peer (IP L2)
  - `vidpid` : Vendor ID and product ID of the RisingHF module (RisingHF L2s)
  - `apn` : Access Point Name to connect to (NB-IoT L2s)
  - `nidd` : Whether to use NIDD mode or not (NB-IoT L2s)
  - `traces` : If enabled activates traces for sent/received L2 packets (NB-IoT L2s)
- `dtls` : DTLS layer configuration, if used
  - `key_id` : PSK identity to be used during DTLS handshake as HEX ASCII
  - `key` : PSK to be used during DTLS handshake as HEX ASCII
  - `host_ip` : IPv6 address from where the application packets will come
  - `remote_ip` : IPv6 address of the DTLS server
  - `host_port` : UDP port from where the application packets will come
  - `remote_port` : UDP port of the DTLS server
  - `handshake_retry` : Number of times the DTLS layer should retry a handshake if it hasn't received a response from the server
  - `handshake_timeout` : Amount of time (in milliseconds) the DTLS layer should wait before attempting a handshake retry
  - `sni_hostname` : Sets the value that will be written in the SNI extension record during handshake if the extension has been enabled
  - `handshake_delay` : Time to wait after the handshake before allowing the sending of application packets, this allows preventing unwanted noise when doing electrical consumption measurements, only tested with `nb_iot_exs82` L2
- `template` : SCHC template configuration
  - `header_rules` : CBOR rules for the header rules
  - `payload_rules` : CBOR rules for the payload pattern
  - `params` : Configuration of the individual parameters within the template
    - `index` : Index of the parameter in the template
    - `value` : Value to associate to the parameter
- `trace_enabled` : Showing SDK's trace messages. 1 or any non-null value to enable.
- `fragmentation_profile`: Fragmentation profile configuration
  - `uplink`: CBOR for the uplink fragmentation profile
    - can be downloaded from IPCore SCHC template
  - `downlink`: CBOR for the downlink fragmentation profile
    - can be downloaded from IPCore SCHC template
