/**
 * @file api.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Thibaut Artis thibaut.artis@ackl.io
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "parson.h"
#include "sdk.h"
#include "tun.h"
#include "utils.h"

#ifdef OTII_MEASUREMENT
#include "otii.h"
#endif

#if defined IPV6UDP
#define TEMPLATE_NAME "ipv6udp"
#elif defined IPV6UDPDYNDEVPORT
#define TEMPLATE_NAME "ipv6udpdyndevport"
#elif defined IPV6UDPDYNSUFFIXPORT
#define TEMPLATE_NAME "ipv6udpdynsuffixport"
#elif defined IPV6UDPDLMS
#define TEMPLATE_NAME "ipv6udpdlms"
#elif defined IPV4UDP
#define TEMPLATE_NAME "ipv4udp"
#elif defined IPV6UDPICMP
#define TEMPLATE_NAME "ipv6udpicmp"
#elif defined DYNAMIC
#define TEMPLATE_NAME "dynamic"
#endif

#ifndef MEMORY_AREA_SIZE
#define MEMORY_AREA_SIZE 16000
#endif

uint8_t header_memory_area[MEMORY_AREA_SIZE];
uint8_t payload_memory_area[MEMORY_AREA_SIZE];

static JSON_Value *root_value = NULL;

static bool polling_enabled = false;
static int idle_polling_timer_value = 30000;
static uint32_t sync_retrans_delay = 0;
static uint8_t sync_nb_retry = 0;

void json_deinit(void)
{
  json_value_free(root_value);
}

bool api_init()
{
  // Configure tun interface name.
  JSON_Object *obj = json_value_get_object(root_value);
  const char *tun_name = json_object_dotget_string(obj, "tun_name");
  if (!tun_name)
  {
    printf("app>invalid tun interface parameters\n");
    return false;
  }
  set_tun_name(tun_name);

  // L2 parameters
  JSON_Object *l2 = json_object_get_object(obj, "l2");

#if defined L2_UDP || defined L2_NB_IOT_MOSH
  const char *ipv4_host_addr = json_object_get_string(l2, "host_ipv4");
  const char *host_port = json_object_get_string(l2, "host_port");
  const char *ipv4_remote_addr = json_object_get_string(l2, "remote_ipv4");
  const char *remote_port = json_object_get_string(l2, "remote_port");

  const int uses_micro_ipcore = json_object_get_boolean(l2, "peer_is_micro_ipcore");
  const char *organization = json_object_get_string(l2, "organization");
  const char *dev_id = json_object_get_string(l2, "device_id");

  if (!ipv4_host_addr || !host_port || !ipv4_remote_addr || !remote_port)
  {
    printf("app>invalid L2 parameters\n");
    printf("app>host_ipv4: %s, host_port: %s, remote_ipv4: %s, remote_port: %s\n",
           ipv4_host_addr ? "OK" : "ERROR", host_port ? "OK" : "ERROR",
           ipv4_remote_addr ? "OK" : "ERROR", remote_port ? "OK" : "ERROR");
    return false;
  }

  if (uses_micro_ipcore == 1)
  {
    l2_set_micro_ipcore_format(true);
    if (!dev_id)
    {
      printf("app>invalid device ID\n");
      return false;
    }
    l2_set_organization(organization);
    l2_set_device_id(dev_id);
  }

  l2_set_ipv4_host_addr(ipv4_host_addr);
  l2_set_udp_src_port(host_port);
  l2_set_ipv4_remote_addr(ipv4_remote_addr);
  l2_set_udp_dest_port(remote_port);
#elif defined L2_UDP6
  const char *ipv6_host_addr = json_object_get_string(l2, "host_ipv6");
  const char *host_port = json_object_get_string(l2, "host_port");
  const char *ipv6_remote_addr = json_object_get_string(l2, "remote_ipv6");
  const char *remote_port = json_object_get_string(l2, "remote_port");

  if (!ipv6_host_addr || !host_port || !ipv6_remote_addr || !remote_port)
  {
    printf("app>invalid L2 parameters\n");
    printf("app>host_ipv6: %s, host_port: %s, remote_ipv6: %s, remote_port: %s\n",
           ipv6_host_addr ? "OK" : "ERROR", host_port ? "OK" : "ERROR",
           ipv6_remote_addr ? "OK" : "ERROR", remote_port ? "OK" : "ERROR");
    return false;
  }

  l2_set_ipv6_host_addr(ipv6_host_addr);
  l2_set_udp_src_port(host_port);
  l2_set_ipv6_remote_addr(ipv6_remote_addr);
  l2_set_udp_dest_port(remote_port);
#elif defined L2_RISINGHF
  const char *vidpid = json_object_get_string(l2, "vidpid");

  if (!vidpid)
  {
    printf("app>invalid L2 parameter, could not retrieve vidpid value\n");
    return false;
  }
  l2_set_vidpid((const uint8_t *)vidpid);

  const char *dev_class = json_object_get_string(l2, "dev_class");
  if (dev_class)
  {
    if (l2_set_class(dev_class[0]) != L2_SUCCESS)
    {
      printf("app>invalid L2 parameter, dev_class value is invalid\n");
      return false;
    }
    if (dev_class[0] == 'A')
    {
      const int polling = json_object_get_boolean(l2, "polling");
      if (polling != -1)
      {
        polling_enabled = (bool)polling;
      }

      const int idle_polling_timer = json_object_get_number(l2, "idle_polling_timer");
      if (idle_polling_timer)
      {
        idle_polling_timer_value = idle_polling_timer;
      }
    }
  }

#elif defined L2_NB_IOT_SLM || L2_NB_IOT_EXS82 || L2_NB_IOT_HL78
  const char *apn = json_object_get_string(l2, "apn");

  if (!apn)
  {
    printf("app>invalid L2 parameter, could not retrieve apn value\n");
    return false;
  }

  const int nidd = json_object_get_boolean(l2, "nidd");

  if (nidd == -1)
  {
    printf("app>invalid L2 parameter, could not retrieve nidd value\n");
    return false;
  }

  const int traces = json_object_get_boolean(l2, "traces");

  if (traces == -1)
  {
    printf("app>invalid L2 parameter, could not retrieve traces value\n");
    return false;
  }

  // if we're not in NIDD and not using T-mobile's private APN we have to set a destination IP
  if (!nidd && strcmp(apn, "cdp.iot.t-mobile.nl"))
  {
    const char *remote_ip = json_object_get_string(l2, "nbiot_remote_ip");
    if (!remote_ip)
    {
      printf("app>invalid L2 parameter, could not retrieve nbiot_remote_ip value\n");
      return false;
    }

    const char *remote_port = json_object_get_string(l2, "remote_port");
    if (!remote_port)
    {
      printf("app>invalid L2 parameter, could not retrieve remote_port value\n");
      return false;
    }

    l2_set_remote_ip(remote_ip);
    l2_set_remote_port(atoi(remote_port));
  }

  l2_set_apn(apn);
  l2_set_nidd((bool)nidd);
  l2_set_traces((bool)traces);
#endif

#if defined(L2_NB_IOT_SLM) || defined(L2_NB_IOT_EXS82) || defined(L2_NB_IOT_HL78) || defined(L2_NB_IOT_MOSH)
  const char *serial_port = json_object_get_string(l2, "serial_port");

  if (serial_port)
  {
    l2_set_serial_port(serial_port);
  }
#endif

#if defined(DTLS_ENABLED)
  JSON_Object *dtls = json_object_get_object(obj, "dtls");

  if (!dtls)
  {
    printf("Missing \"dtls\" parameters section\n");
    return false;
  }

  const char *dtls_key_id = json_object_get_string(dtls, "key_id");
  const char *dtls_key = json_object_get_string(dtls, "key");
  int dtls_handshake_retry = json_object_get_number(dtls, "handshake_retry");
  int dtls_handshake_timeout = json_object_get_number(dtls, "handshake_timeout");
  int dtls_handshake_delay = json_object_get_number(dtls, "handshake_delay");

  if (!dtls_key_id || !dtls_key || !dtls_handshake_retry || !dtls_handshake_timeout)
  {
    printf("app>invalid dtls parameters\n");
    printf("app>key_id: %s, key: %s,"
           " handshake_retry: %d, handshake_timeout: %d\n",
           dtls_key_id ? dtls_key_id : "ERROR", dtls_key ? dtls_key : "ERROR",
           dtls_handshake_retry, dtls_handshake_timeout);
    return false;
  }

  if (!sdk_set_dtls_params(dtls_key_id, dtls_key, dtls_handshake_retry,
                           dtls_handshake_timeout, dtls_handshake_delay))
  {
    return false;
  }
  if (sdk_get_use_sni())
  {
    const char *sni_hostname = json_object_get_string(dtls, "sni_hostname");

    if (!sni_hostname)
    {
      printf("app>could not parse sni_hostname\n");
    }
    else
    {
      sdk_set_dtls_sni_hostname(sni_hostname);
    }
  }
#endif // DTLS_ENABLED

#ifdef OTII_MEASUREMENT
  JSON_Object *otii = json_object_get_object(obj, "otii");

  if (!otii)
  {
    printf("app> Missing \"otii\" parameters section");
    return false;
  }

  const char *ip = json_object_get_string(otii, "ip");
  int port = json_object_get_number(otii, "port");
  int timer_delay = json_object_get_number(otii, "timer_delay");
  int stop_on_rx = json_object_get_boolean(otii, "stop_on_rx");

  if (!ip || !port || !timer_delay || stop_on_rx == -1)
  {
    printf("app>invalid Otii parameters\n");
    printf("app>ip: %s, port: %d, timer_delay: %dms, stop_on_rx: %d\n", ip, port, timer_delay, stop_on_rx);
    return false;
  }

  otii_set_ip(ip);
  otii_set_port(port);
  otii_set_timer_delay(timer_delay);
  set_stop_on_rx(stop_on_rx);
#endif

  return true;
}

static bool load_ascii_rules(uint8_t *buf, uint16_t buf_size)
{
  char hex[3] = {0};
  uint16_t size = 0;

  if (buf_size == 0)
  {
    return false;
  }

  while (buf_size > 1)
  {
    hex[0] = buf[size * 2];
    hex[1] = buf[size * 2 + 1];

    if (sscanf(hex, "%hhx", &buf[size]) != 1)
    {
      return false;
    }

    size++;
    buf_size -= 2;
  }

  // We don't expect parameters anymore
  if (buf_size != 0)
  {
    return false;
  }
  return true;
}

static bool load_cbor_schc_rules(const char *cbor_ascii_data)
{
  uint16_t buf_size = strlen(cbor_ascii_data);
  uint8_t *buf = (uint8_t *)strdup(cbor_ascii_data);

  if (!load_ascii_rules(buf, buf_size))
  {
    return false;
  }
  uint16_t rules_size = buf_size / 2;

#ifdef SCHC_TEMPLATE_SID
  // Provide template buffer.
  mgt_status_t status = mgt_sync_bootstrap_initialize(header_memory_area, MEMORY_AREA_SIZE);
  if (status != MGT_SUCCESS)
  {
    printf("app>error: mgt_sync_bootstrap_initialize() failed, status: %d\n",
           status);
    return false;
  }

  // SYNC SID CBOR doesn't contain CBOR size.
  // TODO: later, the CRC received from the AT Command should be valid, CRC calculation could then be removed here
  // CRC16 is computed here to ease provisioning, it will replace the fake CRC16 (0xFFFF) from the template_rules in .json.
  uint16_t crc16 = compute_crc16(buf, rules_size - 2);
  buf[rules_size - 2] = crc16 & 0x00FF;
  buf[rules_size - 1] = (crc16 & 0xFF00) >> 8;

  // Provision template
  status = mgt_provision_template(buf, rules_size);
  free(buf);
  if (status != MGT_SUCCESS)
  {
    printf("app>error: mgt_provision_template() failed, status: %d\n",
           status);
    return false;
  }
#else
  mgt_status_t status = mgt_provision_header_rules(
      buf, rules_size, header_memory_area, MEMORY_AREA_SIZE);
  free(buf);
  if (status != MGT_SUCCESS)
  {
    printf("app>error: mgt_provision_header_rules() failed, status: %d\n",
           status);
    return false;
  }
#endif

  return true;
}

#ifndef SCHC_TEMPLATE_SID
static bool load_cbor_payload_rules(const char *cbor_ascii_data)
{
  uint16_t buf_size = strlen(cbor_ascii_data);
  uint8_t *buf = (uint8_t *)strdup(cbor_ascii_data);

  if (!load_ascii_rules(buf, buf_size))
  {
    return false;
  }
  uint16_t rules_size = buf_size / 2;
  mgt_status_t status = mgt_provision_payload_rules(
      buf, rules_size, payload_memory_area, MEMORY_AREA_SIZE);
  free(buf);
  if (status != MGT_SUCCESS)
  {
    printf("app>error: mgt_provision_payload_rules() failed, status: %d\n",
           status);
    return false;
  }
  return true;
}

static bool frag_profile_init_profile(const char *profile)
{
  uint16_t buf_size = strlen(profile);
  uint8_t *buf = (uint8_t *)strdup(profile);

  if (!load_ascii_rules(buf, buf_size))
  {
    return false;
  }

  mgt_status_t status = mgt_provision_frag_profile(buf, buf_size / 2);
  free(buf);
  if (status != MGT_SUCCESS)
  {
    printf("app>error: mgt_provision_frag_profile() failed, status: %d\n",
           status);
    return false;
  }
  return true;
}
#endif

#ifdef ZSTD_ENABLED
static bool load_zstd_dictionary(const char *ascii_data)
{
  uint16_t buf_size = strlen(ascii_data);
  uint8_t *buf = (uint8_t *)strdup(ascii_data);

  if (!load_ascii_rules(buf, buf_size))
  {
    return false;
  }

  mgt_status_t status = mgt_provision_zstd_dictionary(buf, buf_size / 2);
  free(buf);
  if (status != MGT_SUCCESS)
  {
    printf("app>error: mgt_provision_zstd_dictionary() failed, status: %d\n",
           status);
    return false;
  }
  return true;
}
#endif

bool frag_profile_init()
{
// SCHC Template using SID/YANG registry doesn't allow to load
// a custom fragmentation profile.
#ifdef SCHC_TEMPLATE_SID
  printf("app>Not allowed to use custom fragmentation profile with SCHC template SID.\n");
#else
  JSON_Object *obj = json_value_get_object(root_value);
  JSON_Object *profile = json_object_get_object(obj, "fragmentation_profile");
  if (!profile)
  {
    printf("app>No fragmentation profile to load in config file\n");
    return false;
  }

  // Load CBOR for the uplink fragmentation profile.
  const char *up_profile = json_object_get_string(profile, "uplink");

  if (up_profile)
  {
    printf("app>loading uplink fragmentation profile\n");
    if (!frag_profile_init_profile(up_profile))
    {
      return false;
    }
  }

  // Load CBOR for the downlink fragmentation profile.
  const char *down_profile = json_object_get_string(profile, "downlink");

  if (down_profile)
  {
    printf("app>loading downlink fragmentation profile\n");
    if (!frag_profile_init_profile(down_profile))
    {
      return false;
    }
  }
#endif

  return true;
}

void configure_trace(void)
{
  JSON_Object *obj = json_value_get_object(root_value);

  const bool show_trace = (bool)json_object_get_boolean(obj, "trace_enabled");
  if (show_trace)
  {
    enable_traces(true);
  }
}

bool tpl_init()
{
  mgt_status_t status;

  // Check if there is a template.
  JSON_Object *obj = json_value_get_object(root_value);
  JSON_Object *template = json_object_get_object(obj, "template");
  if (!template)
  {
    printf("app>template configuration missing\n");
    return false;
  }

#ifdef SCHC_TEMPLATE_SID
  // Load CBOR SCHC template.
  const char *template_rules = json_object_get_string(template, "template_rules");

  if (template_rules)
  {
    printf("app>loading SID template\n");
    if (!load_cbor_schc_rules(template_rules))
    {
      return false;
    }
  }

#else
  // Load CBOR SCHC template.
  const char *header_rules = json_object_get_string(template, "header_rules");

  if (header_rules)
  {
    printf("app>loading dynamic template\n");
    if (!load_cbor_schc_rules(header_rules))
    {
      return false;
    }
  }

  // Load CBOR payload rule.
  const char *payload_rules = json_object_get_string(template, "payload_rules");

  if (payload_rules)
  {
    printf("app>loading payload rules\n");
    if (!load_cbor_payload_rules(payload_rules))
    {
      return false;
    }
  }
#endif

  // Configure the template parameters.
  JSON_Array *template_params = json_object_get_array(template, "params");
  JSON_Object *template_param;
  for (uint8_t i = 0; i < json_array_get_count(template_params); i++)
  {

    template_param = json_array_get_object(template_params, i);
    uint8_t index = (uint8_t)json_object_get_number(template_param, "index");
    const char *value = json_object_get_string(template_param, "value");

    uint8_t *byte_array = NULL;
    uint16_t array_len = get_byte_array_from_hex_string(value, &byte_array);

    if (!byte_array)
    {
      printf("app>invalid template parameter %s\n", value);
      return false;
    }

    printf("app>loading parameter (index: %d, value: %s)\n", index, value);
    status = mgt_set_template_param(index, byte_array, array_len);

    free(byte_array);

    if (status != MGT_SUCCESS)
    {
      printf("app>mgt_set_template_param() failed, status: %d\n", status);
      return false;
    }
  }

#ifdef ZSTD_ENABLED
  // Load Zstd dictionary to compress payload data.
  JSON_Object *zstd = json_object_get_object(obj, "zstd");
  if (zstd)
  {
    const char *zstd_dict = json_object_get_string(zstd, "dictionary");
    if (zstd_dict)
    {
      printf("app>loading Zstd dictionary to compress payload data\n");
      if (!load_zstd_dictionary(payload_rules))
      {
        return false;
      }
    }
  }
#endif

  JSON_Object *sync = json_object_get_object(obj, "synchronization");
  if (!sync)
  {
    printf("app>synchronization disabled\n");
  }
  else
  {

    // Configure and activate parameter synchronization, or template SID synchronization
    // when compiling using template-sid template.
    sync_retrans_delay =
        (uint32_t)json_object_get_number(sync, "retransmission_delay");
    sync_nb_retry = (uint8_t)json_object_get_number(sync, "nb_retry");
    printf("app>synchronization enabled (delay:%dms, retry: %d)\n",
           sync_retrans_delay, sync_nb_retry);

    status = mgt_sync_bootstrap(sync_retrans_delay, sync_nb_retry);
    if (status == MGT_OPERATION_NOT_SUPPORTED_ERR)
    {
      printf("app>synchronization is not supported\n");
      return true;
    }
    else if (status != MGT_SUCCESS)
    {
      printf("app>mgt_sync_bootstrap failed: %d\n", status);
      return false;
    }
  }

  return true;
}

#ifdef SCHC_TEMPLATE_SID
bool tpl_sid_sync_params(void)
{
  // Buffer for Ipv6 dev @ip
  uint8_t ip[16];
  // Try to get template device @ip, in order to know if template is fully configured.
  if (MGT_TPL_INVALID_PARAM == mgt_get_ipv6_address(ip, sizeof(ip)))
  {
    if (!sync_retrans_delay || !sync_nb_retry)
    {
      printf("app>Can't synchronize parameters because values are not initialized");
      return false;
    }

    // Template is not fully configured, so have to activate template
    // parameters synchronization.
    printf("app>template parameters synchronization enabled (delay:%dms, retry: %d)\n",
           sync_retrans_delay, sync_nb_retry);
    mgt_status_t status = mgt_sync_params(sync_retrans_delay, sync_nb_retry);
    switch (status)
    {
    case MGT_SUCCESS:
      return true;
    case MGT_OPERATION_NOT_SUPPORTED_ERR:
      printf("app>template parameters synchronization is not supported\n");
      return true;
    default:
      printf("app>mgt_sync_params failed: %d\n", status);
      return false;
    }
  }
  return true;
}
#endif

static void print_usage(const char *prog_name)
{
  printf("app>EXTENSION_API: template (" TEMPLATE_NAME ")\n");
  printf("app>Usage: %s [json config file]\n", prog_name);
}

bool api_check_args(int ac, const char *av[])
{
  if (ac != 2)
  {
    printf("app>invalid number of arguments for chosen extension api\n");
    print_usage(av[0]);
    return false;
  }

  printf("app>loading file: %s\n", av[1]);
  root_value = json_parse_file(av[1]);
  if (json_value_get_type(root_value) != JSONObject)
  {
    printf("app>error: wrong JSON format\n");
    return false;
  }

  return true;
}

bool is_polling_enabled(void)
{
  return polling_enabled;
}

int get_idle_polling_timer_value(void)
{
  return idle_polling_timer_value;
}