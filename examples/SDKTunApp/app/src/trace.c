/**
 * @file trace.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author simon.racaud - simon.racaud@ackl.io
 */

#include <stdint.h>
#include <stdbool.h>

#include "platform.h"
#ifdef TRACE_ENABLED
#include "fullsdktrace.h"
#endif

static bool trace_enabled_flag = false;

void enable_traces(bool enable)
{
    trace_enabled_flag = enable;
}

#define TRACE_MAX_LENGTH 64

/**
 * For traces. Called for each trace message.
 */
void tr_handle_trace(const uint8_t *trace, uint8_t length)
{
    if (!trace_enabled_flag)
    {
        return; // don't show them
    }
    static const char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                               '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    // For every byte of the binary trace message, we need two bytes in the
    // output
    // string, and a final '\0' byte.
    char out_str[TRACE_MAX_LENGTH * 2 + 1];
    for (uint8_t i = 0; i < length; i++)
    {
        out_str[2 * i] = hex[trace[i] >> 4];
        out_str[2 * i + 1] = hex[trace[i] & 0x0f];
    }
    out_str[length * 2] = '\0';
    PRINT_MSG("TRA>%s\n", out_str);
}