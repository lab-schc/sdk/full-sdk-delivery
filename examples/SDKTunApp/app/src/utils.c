/**
 * @file utils.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

bool is_valid_hex_string(const char *str, uint32_t len)
{
  if (len < 4 || strncmp(str, "0x", 2))
  {
    return false;
  }

  if (len % 2 != 0)
  {
    return false;
  }

  for (uint32_t i = 2; i < len; i++)
  {
    if (!isxdigit(str[i]))
    {
      return false;
    }
  }
  return true;
}

FILE *file_open(const char *filepath)
{
  FILE *fs = fopen(filepath, "r");

  if (!fs)
  {
    return NULL;
  }
  return fs;
}

uint16_t file_get_size(FILE *fs)
{
  uint16_t file_size;

  fseek(fs, 0L, SEEK_END);
  file_size = ftell(fs);
  fseek(fs, 0L, SEEK_SET);
  return file_size;
}

uint8_t *file_read_n_bytes(FILE *fs, uint16_t n)
{
  uint8_t *file_content = malloc(sizeof(uint8_t) * n);

  if (!file_content)
  {
    printf("Error: malloc() failed\n");
    return NULL;
  }

  size_t rs = fread(file_content, sizeof(uint8_t), n, fs);

  if (rs != n)
  {
    return NULL;
  }
  return file_content;
}

uint16_t get_byte_array_from_hex_string(const char *hex_string,
                                        uint8_t **byte_array)
{
  uint16_t len = (uint16_t)strlen(hex_string);

  if (!is_valid_hex_string(hex_string, len))
  {
    *byte_array = NULL;
    return 0;
  }

  uint16_t array_len = (len - 2) / 2;
  *byte_array = malloc(sizeof(uint8_t) * array_len);

  if (!*byte_array)
  {
    return 0;
  }

  memset(*byte_array, 0, array_len);

  // Skipping "0x"
  hex_string += 2;

  for (uint16_t idx = 0; idx < array_len; idx++)
  {
    sscanf(hex_string, "%2hhx", &((*byte_array)[idx]));
    hex_string += 2;
  }
  return array_len;
}

uint16_t compute_crc16(const uint8_t *data, uint16_t length)
{
  uint16_t crc = 0xffff;

  while (length--)
  {
    crc ^= *data++ << CHAR_BIT;

    for (uint8_t i = 0; i < CHAR_BIT; i++)
    {
      crc = crc & 0x8000 ? (crc << 1) ^ 0x1021 : crc << 1;
    }
  }
  return crc & 0xffff;
}