/**
 * @file utils.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#ifndef UTILS_H_APP
#define UTILS_H_APP

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/**
 * @brief Checks if the given string is a valid hexadecimal number.
 * It'll only be considered valid only if it starts with "0x" and if the
 * length of it is even.
 *
 * @param str The string to check
 * @param len The length of the given string
 * @return true if the string is an hexadecimal number, false otherwise
 */
bool is_valid_hex_string(const char *str, uint32_t len);

/**
 * @brief Creates a byte array from an hexadecimal string
 *
 * @param hex_string The hexadecimal string
 * @param byte_array A pointer to a `uint8_t *` variable to store the result
 * @return uint16_t The size of the `byte_array`
 */
uint16_t get_byte_array_from_hex_string(const char *hex_string,
                                        uint8_t **byte_array);

/**
 * @brief Reads `n` bytes in the given file stream `fs`
 *
 * @param fs A pointer to a file stream
 * @param n The number of bytes to read
 * @return uint8_t* The read bytes
 */
uint8_t *file_read_n_bytes(FILE *fs, uint16_t n);

/**
 * @brief Gets the size of the file where `fs` point to
 *
 * @param fs A pointer to a file stream
 * @return uint16_t The size of the file where `fs` points to
 */
uint16_t file_get_size(FILE *fs);

/**
 * @brief Opens a file stream to a file
 *
 * @param filepath The path where the file to open is located
 * @return FILE* The file stream or NULL if the openning process failed
 */
FILE *file_open(const char *filepath);

/**
 * @brief Computes a crc16 on in-data
 *
 * @param data in-data
 * @param length in-data size, in bytes
 * @return 2-bytes length crc16
 */
uint16_t compute_crc16(const uint8_t *data, uint16_t length);

#endif /* UTILS_H */
