/**
 * @file api.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Thibaut Artis thibaut.artis@ackl.io
 */

#ifndef UTILS_COMMON_H_
#define UTILS_COMMON_H_

#include <stdint.h>

bool hex_to_bin(uint8_t *bin_buff, const char *hex, uint8_t *size);

#endif