/**
 * @file sdk.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#ifndef SDK_H
#define SDK_H

#include "fullsdknet.h"
#include "tun.h"

// In this file are the functions used to make the bridge between the SDK and
// the TUN interface parts

typedef enum
{
  CONNECTING,
  CONNECTED,
  DISCONNECTED
} sdk_connection_state_t;

sdk_connection_state_t sdk_get_connection_state(void);

// Function called to finalize the synchronization initialization.
bool process_sync(void);

// Function called at every loop iteration in tun_listen
// Calls mgt_process when required, otherwise calls wait_for_event
bool process_app(void);

bool sdk_do_send(uint8_t *packet, uint16_t data_size);

// Returns true if the SDK is free to start an uplink communication, false
// otherwise
bool sdk_get_can_send(void);

bool sdk_init(int ac, const char **av);

bool sdk_init_api(void);

bool sdk_end(void);

#if defined(DTLS_ENABLED)
bool sdk_set_dtls_params(const char *key_id, const char *key, int handshake_retry, int handshake_timeout, int handshake_delay);
bool sdk_get_use_sni();
bool sdk_set_dtls_sni_hostname(const char *hostname);
#endif // DTLS_ENABLED

#ifdef OTII_MEASUREMENT
void set_stop_on_rx(bool new_stop_on_rx);
#endif

/**
 * @brief Show/Hide SDK's traces
 *
 * @param enable
 */
void enable_traces(bool enable);

#endif /* SDK_H */
