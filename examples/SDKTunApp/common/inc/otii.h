/**
 * @file otii.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Thibaut Artis thibaut.artis@ackl.io
 */

#ifndef OTII_H_
#define OTII_H_

#include <stdbool.h>

int otii_get_timer_delay(void);
void otii_set_timer_delay(int new_timer_delay);

void otii_set_ip(const char *new_ip);
void otii_set_port(int port);

bool otii_init(void);

void otii_start_measurement(void);
void otii_stop_measurement(void *ctx);
void otii_stop_measurement_in(int delay);

#endif // OTII_H_
