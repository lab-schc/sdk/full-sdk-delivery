/**
 * @file tun.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#ifndef TUN_H
#define TUN_H

#include <stdbool.h>
#include <stdint.h>

#ifndef TUN_FILE
#define TUN_FILE "/dev/net/tun"
#endif

#define MAX_PACKET_SIZE_BYTES 1500

// Hooks up to the TUN interface and start listening on it
bool tun_listen(void);

// Enters low power mode until something can be read from tun_fd
bool wait_for_event(int tun_fd);

// Sets the name of the TUN interface to connect to
void set_tun_name(const char *name);

// Closes the TUN connection
void tun_close_connection(void);

// Sends the downlink contained in buff and of data_size to the TUN interface
bool process_downlink(const uint8_t *buff, uint16_t data_size);

#endif /* TUN_H */
