/**
 * @file api.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#ifndef API_H
#define API_H

#include <stdbool.h>

/**
 * @brief Checks if program arguments are consistent
 *
 * @param ac The number of arguments
 * @param av The program arguments
 * @return True if the arguments are consistent, false otherwise
 */
bool api_check_args(int ac, const char *av[]);

/**
 * @brief Initializes the extension api
 *
 * @return True if the initialization is successful, false otherwise
 */
bool api_init();

#ifdef TEMPLATE_API
/**
 * @brief Initializes template parameters
 *
 * @return True if the parameterization is successful, false otherwise
 */
bool tpl_init();
#endif

#ifdef SCHC_TEMPLATE_SID
/**
 * @brief Process template parameters syncrhonization
 *
 * @return True if the template parameters synchronization is successful,
 * false otherwise
 */
bool tpl_sid_sync_params(void);
#endif

/**
 * @brief Initializes fragmentation profiles
 *
 * @return true if the parameterization is successful, false otherwise
 */
bool frag_profile_init();

/**
 * @brief Frees the json objects used to parse the configuration file
 */
void json_deinit(void);

/**
 * @brief Show traces depending of the json configuration
 *
 */
void configure_trace(void);

/**
 * @brief Getter to check whether polling is enabled or not (LoRaWAN class A only)
 */
bool is_polling_enabled(void);

/**
 * @brief Getter for the value in ms of the idle polling timer
 */
int get_idle_polling_timer_value(void);

#endif /* API_H */
