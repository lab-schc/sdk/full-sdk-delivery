#!/bin/sh

TUN_NAME=sdk_tun_client

if [ "$#" -ne 2 ]; then
    echo "Bad number of argument"
    echo "Usage: $0 <dev IPv6> <app prefix>/64"
    exit 1
fi

ip tuntap add mode tun name $TUN_NAME
ip link set $TUN_NAME up
ip -6 addr add $1 dev $TUN_NAME
ip -6 route add $2 dev $TUN_NAME