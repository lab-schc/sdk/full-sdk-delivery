#!/bin/sh

TUN_NAME=sdk_tun_client

if [ "$#" -ne 2 ]; then
    echo "Bad number of argument"
    echo "Usage: $0 <dev IPv4> <app IPv4>/24"
    exit 1
fi

ip tuntap add mode tun name $TUN_NAME
ip link set $TUN_NAME up
ip addr add $1 dev $TUN_NAME
ip route add $2 dev $TUN_NAME