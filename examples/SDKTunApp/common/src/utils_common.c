/**
 * @file utils.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Thibaut Artis thibaut.artis@ackl.io
 */

#include <stdbool.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

bool hex_to_bin(uint8_t *bin_buff, const char *hex, uint8_t *size)
{
  size_t max_size = *size;
  size_t str_len;

  str_len = strlen(hex);
  if (str_len % 2 != 0)
    return false;
  if (str_len / 2 > max_size)
  {
    printf("Error: PSK parameter too big, size: %lu max size: %lu\n", str_len / 2, max_size);
    return false;
  }
  *size = str_len / 2;
  for (uint8_t i = 0; i < *size; ++i)
  {
    char str_byte[3] = {};
    char *err = NULL;
    memcpy(str_byte, hex + 2 * i, 2);
    bin_buff[i] = strtol(str_byte, &err, 16);
    if (*err)
    {
      printf("Error: strtol() failed: %s\n", strerror(errno));
      return false;
    }
  }
  return true;
}