/**
 * @file tun.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <arpa/inet.h>
#include <errno.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

#include "fullsdknet.h"
#include "platform.h"
#include "sdk.h"
#include "tun.h"
#include "udp.h"

#ifdef OTII_MEASUREMENT
#include "otii.h"
#endif

#define IPV6_VERSION 0x06
#define IPV4_VERSION 0x04

// Name of the TUN interface to connect to
char tun_name[IFNAMSIZ] = {0};

static int tun_fd;

static bool close_connection = false;
static bool tun_input_available = false;

void dump_buf(const uint8_t *buf, uint16_t len)
{
  for (size_t i = 0; i < len; i++)
  {
    printf("%.2X ", buf[i]);
  }
  printf("\n");
}

void tun_close_connection(void)
{
  close_connection = true;
}

static void sigint_handler(int sig)
{
  if (sig == SIGINT)
  {
    tun_close_connection();
  }
}

// Catch signal in order to close gracefully
static void set_siginthandler(void)
{
  struct sigaction sigact = {};

  sigact.sa_handler = &sigint_handler;
  sigact.sa_flags = SA_RESETHAND | SA_RESTART;
  sigaction(SIGINT, &sigact, NULL);
}

void set_tun_name(const char *name)
{
  strncpy(tun_name, name, IFNAMSIZ);
}

static int tun_create(void)
{
  struct ifreq ifr;
  int fd;

  if ((fd = open(TUN_FILE, O_RDWR)) < 0)
  {
    return -1;
  }

  memset(&ifr, 0, sizeof(ifr));

  // IFF_TUN means we want a TUN interface
  // IFF_NO_PI means we dont want packet information (adds a 4 bytes header)
  ifr.ifr_flags = IFF_TUN | IFF_NO_PI;
  strncpy(ifr.ifr_name, tun_name, IFNAMSIZ);

  // Ask ioctl to bind fd to a TUN interface with the name and flags previously
  // set
  if (ioctl(fd, TUNSETIFF, (void *)&ifr) < 0)
  {
    printf("tun>failed to create the TUN interface : %s\n", strerror(errno));
    close(fd);
    return -1;
  }

  return fd;
}

static uint8_t get_ip_version(const uint8_t *const packet)
{
  return *packet >> 4;
}

static bool is_ip_udp_packet(const uint8_t *const packet,
                             const size_t packet_size)
{
  if (packet_size < 1)
  {
    return false;
  }

  if (get_ip_version(packet) == IPV6_VERSION &&
      packet_size > sizeof(struct ip6_hdr))
  {
    return ((struct ip6_hdr *)packet)->ip6_nxt == IPPROTO_UDP;
  }

  if (get_ip_version(packet) == IPV4_VERSION &&
      packet_size > sizeof(struct iphdr))
  {
    return ((struct iphdr *)packet)->protocol == IPPROTO_UDP;
  }
  return false;
}

static bool process_uplink(void)
{
  uint8_t packet[MAX_PACKET_SIZE_BYTES] = {0};
  size_t packet_size = 0;

  if (!tun_input_available || !sdk_get_can_send())
  {
    return true;
  }

  tun_input_available = false;
  do
  {
    packet_size = read(tun_fd, packet, MAX_PACKET_SIZE_BYTES);
  } while (packet_size == (uint8_t)-1 && errno == EINTR);

  if (packet_size == (uint8_t)-1)
  {
    printf("tun>read on TUN fd failed : %s\n", strerror(errno));
    return false;
  }
  printf("tun>packet to send(%ld)\n", packet_size);
  printf("tun>");
  dump_buf(packet, packet_size);

  // Here we only support UDP layer on top of IPv4/IPv6 but we could handle
  // other protocols later
  if (!is_ip_udp_packet(packet, packet_size))
  {
    printf("tun>discard non-UDP packet\n");
    return true;
  }

#ifdef OTII_MEASUREMENT
  otii_start_measurement();
  otii_stop_measurement_in(otii_get_timer_delay());
#endif

  return sdk_do_send(packet, packet_size);
}

bool process_downlink(const uint8_t *buff, uint16_t data_size)
{
  int nwrite = 0;

  printf("tun>received data(%d)\n", data_size);
  printf("tun>");
  dump_buf(buff, data_size);

  do
  {
    nwrite = write(tun_fd, buff, data_size);
  } while (nwrite == -1 && errno == EINTR);

  if (nwrite == -1)
  {
    printf("tun>write on TUN failed : %s\n", strerror(errno));
    return false;
  }

  if (nwrite != data_size)
  {
    printf("tun>message truncated, only %d/%d bytes written\n", nwrite,
           data_size);
    return false;
  }
  return true;
}

bool tun_listen(void)
{
  if ((tun_fd = tun_create()) < 0)
  {
    return false;
  }

  set_siginthandler();

  while (!close_connection)
  {
    if (!process_sync())
    {
      break;
    }
    if (!process_uplink())
    {
      break;
    }
    if (!process_app())
    {
      break;
    }
    if (!wait_for_event(tun_fd))
    {
      break;
    }
  }

  sdk_end();
  close(tun_fd);
  return close_connection;
}

static void cb_tun_input_available(void)
{
  tun_input_available = true;
}

bool wait_for_event(int tun_fd)
{
  if (!watch_fd_for_input(tun_fd, &cb_tun_input_available))
  {
    printf("tun>watch_fd_for_input() failed\n");
    return false;
  }

  platform_enter_low_power_ll();

  if (!unwatch_fd_for_input(tun_fd))
  {
    printf("tun>unwatch_fd_for_input() failed\n");
    return false;
  }
  return true;
}
