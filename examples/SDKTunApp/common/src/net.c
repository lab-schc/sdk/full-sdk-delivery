/**
 * @file dtls.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Thibaut Artis thibaut.artis@ackl.io
 */

#include <stdbool.h>
#include <stdio.h>

#include "fullsdknet.h"
#include "tun.h"
#include "otii.h"

static bool transmission_result = true;
static net_status_t transmission_status = 0;
static uint16_t transmission_error = 0;

static bool data_received = false;
static net_status_t data_rx_status = 0;
static uint16_t data_rx_size = 0;

#ifdef OTII_MEASUREMENT
static bool stop_on_rx = false;

void set_stop_on_rx(bool new_stop_on_rx)
{
  stop_on_rx = new_stop_on_rx;
}
#endif

bool sdk_get_can_send(void)
{
  return transmission_result;
}

bool sdk_do_send(uint8_t *packet, uint16_t data_size)
{
  net_status_t status = net_sendto(packet, data_size);

  if (status != NET_SUCCESS)
  {
    // No need to kill the SDK when we get an error.
    // Later we could return specific error code to take a better decision
    printf("tun>net_sendto() failed %d\n", status);
    return false;
  }
  transmission_result = false;
  return true;
}

static void sdk_transmission_result(net_status_t status, uint16_t error)
{
  transmission_status = status;
  transmission_error = error;
  transmission_result = true;
}

static void sdk_data_received(const uint8_t *buff, uint16_t data_size,
                              net_status_t status)
{
#ifdef OTII_MEASUREMENT
  if (stop_on_rx)
  {
    otii_stop_measurement(NULL);
  }
#endif
  data_rx_size = data_size;
  data_rx_status = status;
  data_received = true;

  if (status != NET_SUCCESS)
  {
    return;
  }

  if (!process_downlink(buff, data_size))
  {
    status = NET_ERROR;
  }
}

// net callbacks
net_callbacks_t net_callbacks = {sdk_transmission_result, sdk_data_received};

bool sdk_init_api(void)
{
  net_status_t st = net_initialize(&net_callbacks);
  if (st != NET_SUCCESS)
  {
    fprintf(stderr, "app>Error: net_initialize returned: %d\n", st);
    return false;
  }
  printf("app>net_initialize succeeded\n");
  return true;
}

bool sdk_end(void)
{
  return true;
}