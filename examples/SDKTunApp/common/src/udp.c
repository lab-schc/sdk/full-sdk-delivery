/**
 * @file udp.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <netinet/ip6.h>
#include <netinet/udp.h>

#include "udp.h"

uint16_t udp_checksum(const void *packet, uint16_t size,
                      struct in6_addr *src_addr, struct in6_addr *dst_addr)
{
  const uint16_t *buf = (const uint16_t *)packet;
  uint16_t *ip_src = (uint16_t *)src_addr;
  uint16_t *ip_dst = (uint16_t *)dst_addr;
  uint32_t sum = 0;
  uint16_t len = size;

  while (len > 1)
  {
    sum += *buf++;
    if (sum & 0x80000000)
      sum = (sum & 0xFFFF) + (sum >> 16);
    len -= 2;
  }

  if (len & 1)
  {
    sum += *((char *)buf);
  }

  for (int i = 0; i < 8; i++)
  {
    sum += ip_src[i];
  }

  for (int i = 0; i < 8; i++)
  {
    sum += ip_dst[i];
  }

  sum += htons(IPPROTO_UDP);
  sum += htons(size);

  while (sum >> 16)
  {
    sum = (sum & 0xFFFF) + (sum >> 16);
  }
  return ((uint16_t)~sum);
}

void update_udp_checksum(char *packet)
{
  struct ip6_hdr *iphdr = (struct ip6_hdr *)packet;
  struct udphdr *uhdr = (struct udphdr *)(packet + sizeof(struct ip6_hdr));

  uhdr->uh_sum = 0;
  uhdr->uh_sum = udp_checksum(uhdr, ntohs(uhdr->uh_ulen), &iphdr->ip6_src,
                              &iphdr->ip6_dst);
}

void update_ip6_udp_len(char *packet, size_t size)
{
  struct ip6_hdr *iphdr = (struct ip6_hdr *)packet;
  iphdr->ip6_plen = htons(sizeof(struct udphdr) + size);

  struct udphdr *uhdr = (struct udphdr *)(packet + sizeof(struct ip6_hdr));
  uhdr->uh_ulen = htons(sizeof(struct udphdr) + size);
}
