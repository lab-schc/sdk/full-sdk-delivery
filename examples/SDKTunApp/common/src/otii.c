/**
 * @file otii.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Thibaut Artis thibaut.artis@ackl.io
 */

#include <arpa/inet.h>
#include <string.h>
#include <sys/socket.h>

#include "otii.h"
#include "platform.h"

#define START_STR "start"
#define STOP_STR "stop"
#define START_STR_SIZE (sizeof(START_STR) - 1)
#define STOP_STR_SIZE (sizeof(STOP_STR) - 1)

static int sockfd;

static TimerEvent_t otii_stop_timer;
static int timer_delay = 10000; // in ms

static char ip[50] = "127.0.0.1";
static int port = 7878;

static bool timer_running = false;

int otii_get_timer_delay(void)
{
  return timer_delay;
}

void otii_set_timer_delay(int new_timer_delay)
{
  timer_delay = new_timer_delay;
}

void otii_set_ip(const char *new_ip)
{
  int len = strlen(new_ip);
  memcpy(ip, new_ip, len >= 50 ? 50 : (len + 1));
}

void otii_set_port(int new_port)
{
  port = new_port;
}

bool otii_init(void)
{
  struct sockaddr_in servaddr = {};
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
  {
    return false;
  }
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(ip);
  servaddr.sin_port = htons(port);
  if (connect(sockfd, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0)
  {
    return false;
  }

  TimerInit(&otii_stop_timer, otii_stop_measurement);
  timer_running = true;
  return true;
}

void otii_start_measurement(void)
{
  send(sockfd, START_STR, START_STR_SIZE, 0);
}

void otii_stop_measurement(void *ctx)
{
  (void)ctx;
  if (timer_running)
  {
    TimerStop(&otii_stop_timer);
  }
  timer_running = false;
  send(sockfd, STOP_STR, STOP_STR_SIZE, 0);
}

void otii_stop_measurement_in(int delay)
{
  TimerSetValue(&otii_stop_timer, delay);
  TimerStart(&otii_stop_timer);
}