/**
 * @file sdk.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <stdio.h>
#include <string.h>

#include "api.h"
#include "fullsdkmgt.h"
#include "fullsdkschc.h"
#include "platform.h"
#include "sdk.h"
#include "tun.h"

// Variable used to determine if mgt_process() should be called
static bool mgt_process_request = false;

// Variable set when synchronization procedure is finished.
static bool mgt_bootstrap_result = false;

// Variable set when synchronization for template parameters
// procedure is finished.
static bool mgt_tpl_params_bootstrap_result = false;

#if defined(DTLS_ENABLED)
void sdk_handshake(void);
#endif

#define RECEIVE_BUFFER_SIZE 8000

#ifndef MAX_PAYLOAD_SIZE
#define MAX_PAYLOAD_SIZE RECEIVE_BUFFER_SIZE
#endif
#define IP_PORT_LENGTH_BYTES 2

#ifdef DTLS_ENABLED
#define DTLS_MEMORY_SIZE (2 * (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE)) + MGT_DTLS_DATA_LEN
#else
#define DTLS_MEMORY_SIZE 0
#endif

// Template SID
#ifdef SCHC_TEMPLATE_SID
#define SYNC_SID_SIZE MGT_SYNC_SID_SIZE
#else
#define SYNC_SID_SIZE 0
#endif

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                            \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u + \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u) +   \
      DTLS_MEMORY_SIZE + SYNC_SID_SIZE

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE] = {0};

static TimerEvent_t sdk_timers[9];
static TimerEvent_t polling_timer;

// MGT callbacks
static void cb_mgt_processing_required(void)
{
  mgt_process_request = true;
}

sdk_connection_state_t conn_state = CONNECTING;

static void cb_mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS && conn_state != CONNECTED)
  {
    printf("Info: l2 connection established\n");
    conn_state = CONNECTED;
    // we start a handshake right after l2 connection is established if the security
    // layer is enabled and if sync is not enabled
#if defined(DTLS_ENABLED) && !defined(SYNC_ENABLED)
    sdk_handshake();
#endif
  }
  else if (state != MGT_SUCCESS && conn_state == CONNECTED)
  {
    fprintf(stderr, "Error: l2 connection has been lost\n");
    conn_state = DISCONNECTED;
  }
}

sdk_connection_state_t sdk_get_connection_state(void)
{
  return conn_state;
}

static void cb_start_timer(uint8_t id, uint32_t duration)
{
  TimerSetValue(&sdk_timers[id], duration);
  TimerStart(&sdk_timers[id]);
}

static void cb_stop_timer(uint8_t id)
{
  TimerStop(&sdk_timers[id]);
}

static void sdk_timer_1_event(void *context)
{
  (void)context;
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  (void)context;
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  (void)context;
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  (void)context;
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  (void)context;
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  (void)context;
  mgt_timer_timeout(5);
}

static void sdk_timer_7_event(void *context)
{
  (void)context;
  mgt_timer_timeout(6);
}

static void sdk_timer_8_event(void *context)
{
  (void)context;
  mgt_timer_timeout(7);
}

static void sdk_timer_9_event(void *context)
{
  (void)context;
  mgt_timer_timeout(8);
}

static void app_polling_timer_timeout(void *context)
{
  (void)context;
  mgt_trigger_polling();
  TimerStart(&polling_timer);
}

static void cb_bootstrap_result(mgt_status_t state)
{
  mgt_bootstrap_result = true;
  if (state == MGT_SUCCESS)
  {
    printf("app>%s succeeded\n", mgt_tpl_params_bootstrap_result ? "template parameters synchronization" : "synchronization");
  }
  else
  {
    printf("app>%s failed (status %d)\n", mgt_tpl_params_bootstrap_result ? "template parameters synchronization" : "synchronization", state);
  }
}

mgt_callbacks_t mgt_callbacks = {
    cb_mgt_processing_required,
    cb_mgt_connectivity_state,
    cb_start_timer,
    cb_stop_timer,
    cb_bootstrap_result,
};

bool sdk_init(int ac, const char *av[])
{
  platform_hw_init();
  platform_configure_sleep_mode();
  TimerInit(&sdk_timers[0], sdk_timer_1_event);
  TimerInit(&sdk_timers[1], sdk_timer_2_event);
  TimerInit(&sdk_timers[2], sdk_timer_3_event);
  TimerInit(&sdk_timers[3], sdk_timer_4_event);
  TimerInit(&sdk_timers[4], sdk_timer_5_event);
  TimerInit(&sdk_timers[5], sdk_timer_6_event);
  TimerInit(&sdk_timers[6], sdk_timer_7_event);
  TimerInit(&sdk_timers[7], sdk_timer_8_event);
  TimerInit(&sdk_timers[8], sdk_timer_9_event);

  printf("app>FullSDK version: %s\n", mgt_get_version());

  mgt_status_t mgt_status =
      mgt_initialize(&mgt_callbacks, mgt_mem_block, MEM_BLOCK_SIZE,
                     MAX_MTU_SIZE, MAX_PAYLOAD_SIZE);

  if (mgt_status != MGT_SUCCESS)
  {
    printf("app>mgt_initialize() failed: %d\n", mgt_status);
    return false;
  }

#ifdef TEMPLATE_API
  if (!tpl_init(ac, av))
  {
    return false;
  }
#endif

  frag_profile_init(); // try to load fragmentation profile configuration

  configure_trace();

#if (!defined(SYNC_ENABLED) && !defined(SCHC_TEMPLATE_SID))
  if (!sdk_init_api())
  {
    return false;
  }
#endif

  if (is_polling_enabled())
  {
    mgt_enable_polling(true);
    TimerInit(&polling_timer, app_polling_timer_timeout);
    TimerSetValue(&polling_timer, get_idle_polling_timer_value());
    TimerStart(&polling_timer);
    if (mgt_trigger_polling() != MGT_SUCCESS)
    {
      return false;
    }
  }

  return true;
}

bool process_sync(void)
{
  // Sync V1
#ifdef SYNC_ENABLED
  if (mgt_bootstrap_result)
  {
    mgt_bootstrap_result = false;

    if (!sdk_init_api())
    {
      return false;
    }

    // if the sync is enabled and the security layer is enabled
    // we start the handshake after the sync succeeded
#if defined(DTLS_ENABLED)
    sdk_handshake();
#endif
  }
#endif

// Sync V2 with SID/YANG registry template.
#ifdef SCHC_TEMPLATE_SID
  if (mgt_bootstrap_result)
  {
    mgt_bootstrap_result = false;

    // Template not updated yet.
    if (!mgt_tpl_params_bootstrap_result)
    {
      // Template is now updated.
      mgt_tpl_params_bootstrap_result = true;
      // Start template parameters synchronization process.
      tpl_sid_sync_params();
    }
    else
    {
      // Template parameters are now synchronized
      mgt_tpl_params_bootstrap_result = false;
      if (!sdk_init_api())
      {
        return false;
      }
    }
  }
#endif

  return true;
}

bool process_app(void)
{
  if (mgt_process_request)
  {
    mgt_process_request = false;
    mgt_status_t st;
    if ((st = mgt_process()) != MGT_SUCCESS)
    {
      printf("app>mgt_process() failed: %d\n", st);
      return false;
    }
  }
  return true;
}
