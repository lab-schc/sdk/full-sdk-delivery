/**
 * @file dtls.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Thibaut Artis thibaut.artis@ackl.io
 */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "platform.h"
#include "fullsdknet.h"
#include "tun.h"
#include "fullsdkdtls.h"
#include "utils_common.h"

#ifdef OTII_MEASUREMENT
#include "otii.h"
#endif

#define PSK_ID_MAX_LEN 64
#define PSK_MAX_LEN 255
#define SNI_HOSTNAME_MAX_LEN 255

#define SAVE_FILE "saved_session.bin"

static bool transmission_result = true;
static net_status_t transmission_status = 0;
static uint16_t transmission_error = 0;

static bool data_received = false;
static net_status_t data_rx_status = 0;
static uint16_t data_rx_size = 0;

static bool handshake_done = false;
static bool use_session_resumption = false;

static uint8_t psk_id[PSK_ID_MAX_LEN] = {0};
static uint8_t psk[PSK_MAX_LEN] = {0};

static char sni_hostname[SNI_HOSTNAME_MAX_LEN] = {0};

static dtls_options_t dtls_opt = {NULL, NULL, PSK_MAX_LEN, PSK_ID_MAX_LEN, 3, 30000};
static int dtls_handshake_delay = 0;

void sdk_handshake_result(dtls_status_t status);

dtls_callbacks_t dtls_cb = {sdk_handshake_result, platform_entropy_hardware_poll};

#ifdef OTII_MEASUREMENT
static bool stop_on_rx = false;

void set_stop_on_rx(bool new_stop_on_rx)
{
  stop_on_rx = new_stop_on_rx;
}
#endif

static bool sdk_dtls_save_session(dtls_session_t *session, uint16_t session_size)
{
  FILE *file = fopen(SAVE_FILE, "w");
  if (file == NULL)
  {
    fprintf(stderr, "serializing: Error while opening save file: %s", strerror(errno));
    return false;
  }
  if (fwrite(session, session_size, 1, file) != 1)
    return false;
  if (fclose(file) != 0)
    return false;
  return true;
}

static bool sdk_dtls_load_session(dtls_session_t *session, uint16_t session_size)
{
  if (access(SAVE_FILE, F_OK | R_OK) != 0)
  {
    fprintf(stderr, "session loading: save file %s not found\n", SAVE_FILE);
    return false;
  }
  FILE *file = fopen(SAVE_FILE, "r");
  if (file == NULL)
  {
    fprintf(stderr, "session loading: Error while opening save file: %s", strerror(errno));
    return false;
  }
  if (fread(session, session_size, 1, file) != 1)
    return false;
  if (fclose(file) != 0)
    return false;
  return true;
}

bool sdk_set_dtls_params(const char *key_id, const char *key, int handshake_retry, int handshake_timeout, int handshake_delay)
{
  if (!hex_to_bin(psk_id, key_id, &dtls_opt.psk_id_size))
  {
    fprintf(stderr, "Error: parse_opt() error: Invalid PSK ID\n");
    return false;
  }
  if (!hex_to_bin(psk, key, &dtls_opt.psk_size))
  {
    fprintf(stderr, "Error: parse_opt() error: Invalid PSK\n");
    return false;
  }
  dtls_opt.psk_id = psk_id;
  dtls_opt.psk = psk;
  dtls_opt.nbr_try = handshake_retry;
  dtls_opt.handshake_timeout = handshake_timeout;
  dtls_handshake_delay = handshake_delay;

  char *env_var = NULL;
  if ((env_var = getenv("DTLS_SESSION_RESUMPTION")) != NULL && memcmp(env_var, "true", sizeof("true")) == 0)
  {
    use_session_resumption = true;
  }

  if (use_session_resumption)
  {
    dtls_conf_session_persistence(&sdk_dtls_save_session, &sdk_dtls_load_session);
  }

  return dtls_set_options(&dtls_cb, &dtls_opt) == DTLS_SUCCESS;
}

bool sdk_get_use_sni()
{
  return dtls_set_sni_hostname(NULL) != DTLS_NO_IMPL_ERR;
}

bool sdk_set_dtls_sni_hostname(const char *hostname)
{
  if (!hostname || strlen(hostname) > SNI_HOSTNAME_MAX_LEN)
  {
    return false;
  }
  strncpy(sni_hostname, hostname, SNI_HOSTNAME_MAX_LEN);
  return true;
}

bool sdk_get_can_send(void)
{
  return transmission_result && handshake_done;
}

static void do_handshake()
{
  if (dtls_handshake_delay != 0)
  {
    printf("Info: pausing the program for %d seconds before handshake...\n", dtls_handshake_delay);
    sleep((unsigned int)dtls_handshake_delay);
  }
  if (use_session_resumption)
  {
    dtls_load_session();
  }
  printf("Info: starting handshake\n");
#ifdef OTII_MEASUREMENT
  otii_start_measurement();
#endif
  dtls_handshake();
}

void sdk_handshake(void)
{
  do_handshake();
}

void sdk_handshake_result(dtls_status_t status)
{
#ifdef OTII_MEASUREMENT
  otii_stop_measurement(NULL);
#endif
  if (status != DTLS_SUCCESS)
  {
    fprintf(stderr, "Error: Handshake failed, status: %d\n", status);
    tun_close_connection();
    return;
  }
  printf("Info: Handshake done\n");
  handshake_done = true;
  if (use_session_resumption)
  {
    dtls_save_session();
    printf("saving session for later use in \"saved_session.bin\"\n");
  }
}

bool sdk_do_send(uint8_t *packet, uint16_t data_size)
{
  net_status_t status = net_sendto(packet, data_size);

  if (status != NET_SUCCESS)
  {
    // No need to kill the SDK when we get an error.
    // Later we could return specific error code to take a better decision
    printf("tun>net_sendto() failed %d\n", status);
    return false;
  }
  transmission_result = false;
  return true;
}

static void sdk_transmission_result(net_status_t status, uint16_t error)
{
  transmission_status = status;
  transmission_error = error;
  transmission_result = true;
}

static void sdk_data_received(const uint8_t *buff, uint16_t data_size,
                              net_status_t status)
{
#ifdef OTII_MEASUREMENT
  if (stop_on_rx)
  {
    otii_stop_measurement(NULL);
  }
#endif
  data_rx_size = data_size;
  data_rx_status = status;
  data_received = true;

  if (status != NET_SUCCESS)
  {
    return;
  }

  if (!process_downlink(buff, data_size))
  {
    status = NET_ERROR;
  }
}

// net callbacks
net_callbacks_t net_callbacks = {sdk_transmission_result, sdk_data_received};

bool sdk_init_api(void)
{
  net_status_t st = net_initialize(&net_callbacks);
  if (st != NET_SUCCESS)
  {
    fprintf(stderr, "app>Error: net_initialize returned: %d\n", st);
    return false;
  }
  printf("app>net_initialize succeeded\n");
  return true;
}

bool sdk_end(void)
{
  return dtls_close() == DTLS_SUCCESS;
}