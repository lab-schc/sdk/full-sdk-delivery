/**
 * @file main.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <stdlib.h>

#include "api.h"
#include "sdk.h"

#ifdef OTII_MEASUREMENT
#include "otii.h"
#endif

int main(int ac, const char *av[])
{
  if (!api_check_args(ac, av))
  {
    return EXIT_FAILURE;
  }

#ifdef OTII_MEASUREMENT
  if (!otii_init())
  {
    return EXIT_FAILURE;
  }
#endif

  if (!api_init())
  {
    json_deinit();
    return EXIT_FAILURE;
  }

  if (!sdk_init(ac, av))
  {
    json_deinit();
    return EXIT_FAILURE;
  }
  json_deinit();

  if (!tun_listen())
  {
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}