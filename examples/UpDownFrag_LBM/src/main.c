/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz - aydogan.ersoz@ackl.io
 */

#include <ctype.h>
#include <stdint.h>
#include <string.h>

#include "fullsdkdtg.h"
#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "platform.h"

#define APP_TIMER_PERIOD 60000 * 5 // 5 minutes
// Maximum size of a downlink message.
#define APP_RECEIVE_MAX_LENGTH 512
// MAXIMUM size of an uplink message.
#define APP_SEND_MAX_LENGTH 256 // always fragmentation
// Assume (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE APP_RECEIVE_MAX_LENGTH
#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned
#define APP_TRANSMISSION_MAX_LENGTH MAX_PAYLOAD_SIZE
// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                                         \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u +              \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)
#define DEVICE_DATARATE 5 // SF7/DR5
#define DEVICE_CLASS 'C'  // Class C

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];
static bool mgt_process_request_event = false;
static bool network_connectivity_event = false;
static bool dtg_tx_result_event = false;
static bool dtg_data_received_event = false;
static bool app_timer_event = false;
static bool start_app_timer = false;
static timer_obj_t app_timer;
static timer_obj_t sdk_timers[6];
// Timer IDs (needed for vTAL)
static uint16_t timer_id = 0;
// Buffer for data returned by DTG layer.
static uint8_t reception_buffer[APP_RECEIVE_MAX_LENGTH];
static uint8_t transmission_buffer[APP_TRANSMISSION_MAX_LENGTH];
// Application buffers
static uint8_t uplink_cnt = 0;
static uint16_t dtg_received_data_len = 0;
static uint8_t app_received_data[APP_RECEIVE_MAX_LENGTH];
static uint8_t app_send_data[APP_SEND_MAX_LENGTH];
// Device configuration.
static const dtg_sockaddr_t host_addr = {
    {0x22, 0x22, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x04}, // Address.
    {0x16, 0x33}              // Port.
};
static const dtg_sockaddr_t remote_addr = {
    {0x35, 0x35, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x01}, // Address.
    {0x16, 0x33}              // Port.
};
static dtg_socket_t *socket = NULL;

static void mgt_processing_required(void);
static void mgt_connectivity_state(mgt_status_t state);
static void app_timer_event_fct(void *context);
static void sdk_timer_1_event(void *context);
static void sdk_timer_2_event(void *context);
static void sdk_timer_3_event(void *context);
static void sdk_timer_4_event(void *context);
static void sdk_timer_5_event(void *context);
static void sdk_timer_6_event(void *context);
static void mgt_start_timer(uint8_t id, uint32_t duration);
static void mgt_stop_timer(uint8_t id);
static void dtg_transmission_result(const dtg_socket_t *socket,
                                    dtg_status_t status, uint16_t error);
static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                              uint16_t data_size,
                              const dtg_sockaddr_t *src_addr,
                              dtg_status_t status);
static void mgt_bootstrap_result(mgt_status_t state);

int main(void)
{
  BEGIN_CRITICAL_SECTION();

  platform_hw_init();

#ifdef PLATFORM_TRACE_ENABLED
  platform_enable_traces(true);
#endif

  l2_init();

  END_CRITICAL_SECTION();

  PRINT_MSG("app> fullsdk version: %s\n", mgt_get_version());

  platform_timer_add(&app_timer, timer_id++, app_timer_event_fct, NULL);
  platform_timer_add(&sdk_timers[0], timer_id++, sdk_timer_1_event, NULL);
  platform_timer_add(&sdk_timers[1], timer_id++, sdk_timer_2_event, NULL);
  platform_timer_add(&sdk_timers[2], timer_id++, sdk_timer_3_event, NULL);
  platform_timer_add(&sdk_timers[3], timer_id++, sdk_timer_4_event, NULL);
  platform_timer_add(&sdk_timers[4], timer_id++, sdk_timer_5_event, NULL);
  platform_timer_add(&sdk_timers[5], timer_id++, sdk_timer_6_event, NULL);

  // Initialize MGT layer.
  mgt_callbacks_t callbacks = {mgt_processing_required, mgt_connectivity_state,
                               mgt_start_timer, mgt_stop_timer, mgt_bootstrap_result};

  mgt_status_t mgt_status =
      mgt_initialize(&callbacks, mgt_mem_block, MEM_BLOCK_SIZE, MAX_MTU_SIZE,
                     MAX_PAYLOAD_SIZE);
  if (mgt_status != MGT_SUCCESS)
  {
    PRINT_MSG("app> error in mgt_initialize: %d\n", mgt_status);
    platform_error_handler();
  }

  mgt_set_template_param(0, &host_addr.addr[0], 8);
  mgt_set_template_param(1, &host_addr.addr[8], 8);
  mgt_set_template_param(2, &remote_addr.addr[0], 8);
  mgt_set_template_param(3, &remote_addr.addr[8], 8);
  mgt_set_template_param(4, &host_addr.port[0], IP_PORT_LENGTH_BYTES);
  mgt_set_template_param(5, &remote_addr.port[0], IP_PORT_LENGTH_BYTES);

  // Initialize DTG interface
  dtg_callbacks_t dtg_callbacks = {dtg_transmission_result, dtg_data_received};
  dtg_status_t dtg_status = dtg_initialize(&dtg_callbacks);
  if (dtg_status != DTG_SUCCESS)
  {
    PRINT_MSG("app> error in dtg_initialize: %d\n", dtg_status);
    platform_error_handler();
  }

  // Create the socket.
  socket = dtg_socket(transmission_buffer, APP_TRANSMISSION_MAX_LENGTH);
  if (socket == DTG_INVALID_SOCKET)
  {
    PRINT_MSG("app> error in dtg_socket\n");
    platform_error_handler();
  }
  dtg_status = dtg_bind(socket, &host_addr);
  if (dtg_status != DTG_SUCCESS)
  {
    PRINT_MSG("app> error in dtg_bind: %d\n", dtg_status);
    platform_error_handler();
  }

  char class;
  bool adr;
  uint8_t dr;

  while (true)
  {
    if (mgt_process_request_event)
    {
      mgt_process_request_event = false;
      mgt_status = mgt_process();
      if (mgt_status != MGT_SUCCESS)
      {
        PRINT_MSG("app> error in mgt_process: %d\n", mgt_status);
      }
    }

    if (network_connectivity_event)
    {
      network_connectivity_event = false;
      PRINT_MSG("app> network joined\n");
      l2_set_class(DEVICE_CLASS);
      l2_get_class(&class);
      PRINT_MSG("app> device class %c\n", class);
      l2_set_dr(DEVICE_DATARATE);
      l2_get_dr(&dr);
      PRINT_MSG("app> device data rate %d\n", dr);
      l2_get_adr(&adr);
      PRINT_MSG("app> device adr status %d\n", adr);
      PRINT_MSG("app> dev eui\n");
      PRINT_HEX_BUF(l2_get_dev_eui(), SMTC_MODEM_EUI_LENGTH);
      PRINT_MSG("app> device app key\n");
      PRINT_HEX_BUF(l2_get_app_key(), SMTC_MODEM_KEY_LENGTH);
      PRINT_MSG("app> join eui\n");
      PRINT_HEX_BUF(l2_get_join_eui(), SMTC_MODEM_EUI_LENGTH);

      app_timer_event = true;
    }

    if (dtg_tx_result_event)
    {
      dtg_tx_result_event = false;
      PRINT_MSG("app> uplink sent\n");

      start_app_timer = true;
    }

    if (dtg_data_received_event)
    {
      dtg_data_received_event = false;
      PRINT_MSG("app> downlink received\n");
      PRINT_HEX_BUF(app_received_data, dtg_received_data_len);
    }

    if (start_app_timer)
    {
      start_app_timer = false;

      platform_timer_set_duration(&app_timer, APP_TIMER_PERIOD);
      platform_timer_start(&app_timer);
      PRINT_MSG("app> app timer started for %d seconds\n",
                APP_TIMER_PERIOD / 1000);
    }

    if (app_timer_event)
    {
      app_timer_event = false;

      for (uint16_t i = 0; i < APP_SEND_MAX_LENGTH; i++)
      {
        app_send_data[i] = '0' + uplink_cnt % 10;
      }
      uplink_cnt++;

      PRINT_MSG("app> sending uplink...\n");
      PRINT_HEX_BUF(app_send_data, APP_SEND_MAX_LENGTH);

      dtg_status_t dtg_status =
          dtg_sendto(socket, app_send_data, APP_SEND_MAX_LENGTH, &remote_addr);
      if (dtg_status != DTG_SUCCESS)
      {
        PRINT_MSG("app> error in dtg_sendto: %d\n", dtg_status);
      }
    }

    // we don't use the returned sleep value as low power mode in lbm is not yet
    // supported on our side
    l2_periodic_scheduler();
  }
}

static void app_timer_event_fct(void *context)
{
  app_timer_event = true;
}

static void mgt_processing_required(void)
{
  mgt_process_request_event = true;
}

static void mgt_connectivity_state(mgt_status_t state)
{
  network_connectivity_event = true;
}

static void mgt_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&sdk_timers[id], duration);
  platform_timer_start(&sdk_timers[id]);
}

static void mgt_stop_timer(uint8_t id)
{
  platform_timer_stop(&sdk_timers[id]);
}

static void sdk_timer_1_event(void *context)
{
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  mgt_timer_timeout(5);
}

static void dtg_transmission_result(const dtg_socket_t *socket,
                                    dtg_status_t status, uint16_t error)
{
  dtg_tx_result_event = true;
}

static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                              uint16_t data_size,
                              const dtg_sockaddr_t *src_addr,
                              dtg_status_t status)
{
  dtg_data_received_event = true;

  if (data_size > APP_RECEIVE_MAX_LENGTH)
  {
    data_size = APP_RECEIVE_MAX_LENGTH;
  }

  dtg_received_data_len = data_size;

  if (status == DTG_SUCCESS)
  {
    memcpy(app_received_data, p_buffer, dtg_received_data_len);
  }
}

static void mgt_bootstrap_result(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    PRINT_MSG("sdk> mgt_bootstrap_result - OK\n");
  }
  else
  {
    PRINT_MSG("sdk> mgt_bootstrap_result - KO: %d\n\r", state);
  }
}