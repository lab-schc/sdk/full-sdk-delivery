# PingPongEdhoc

## Description
This Example is a PingPong app aimed to test EDHOC over OSCORE with the SDK 

one server and one client that will run on a same x86 machine. Once the client and the server establishes the UDP communication, they will negotiate and derive their OSCORE security context thanks to EDHOC protocol. Then, once the contexts have been derived, a regular PingPong messages will come and go between the peers using CoAP/OSCORE packets. All COAP/OSCORE packets are encapsulated with a fake IPv6/UDP header in order to enable/test SDK compression

### Basic example flow:
* Server will be launched
* Client will be launched
* Client will be connected to to the server over a local UDP port
* EDHOC negotiation will be run between the peers (each entity must have its context derived at the end of this step)
* Client will generate a CoAP message translate it to OSCORE and send it to the server
* Server will receive the OSCORE message, translate it to a CoAP message, print the received counter value, increment it, put it into a new CoAP message, translate it to OSCORE message
* Client will receive the OSCORE message, translate it to a CoAP message and print it and send back the same COAP packet translated in OSCORE

### Note
You can check this [document](https://acklio.atlassian.net/wiki/spaces/DEV/pages/1999175681/EDHOC) in order to learn more about Fraunhofer OSCORE/EDHOC libraries

## Compilation
```sh
./examples/PingPongEdhoc/build_libs.sh
cmake -S . -B ./build -DAPP_NAME=PingPongEdhoc -DPLATFORM=linux -DTOOLCHAIN=gcc-native -DTARGET=default -DL2_STACK=udp -DEXTENSION_API=fraunhofer -DFRAGMENTATION_API=fraunhofer -DEDHOC_TEST=1 && make -C ./build
```
### Note
Compilation variables [INITATOR_TEST_X] and [RESPONDER_TEST_X] need to be set through the Cmakelist. Those variables allows us to use different types of encryptions and keys with EDHOC protocol

#### TESTS
EDHOC_TEST       | mode                          | RPK/Cert | suite | Ref [1]
|----------------|-------------------------------|----------|-------|-------
 1               | INITIATOR_SK_RESPONDER_SK     | RPK x5t  | 0     | 1-290
 2               | INITIATOR_SDHK_RESPONDER_SDHK | RPK kid  | 0     | 292-540
 3               | INITIATOR_SK_RESPONDER_SK     | x5chain  | 0     | non
 4               | INITIATOR_SDHK_RESPONDER_SDHK | x5chain  | 0     | non
 undefined       | INITIATOR_SK_RESPONDER_SK     | RPK x5t  | 0     | 1-290

## Usage
### Run server
```
./PingPongEdhoc server [l2 host ipv4] [l2 host port] [l2 remote ipv4] [l2 remote port]
```
### Run client
```
./PingPongEdhoc client [l2 host ipv4] [l2 host port] [l2 remote ipv4] [l2 remote port]
```