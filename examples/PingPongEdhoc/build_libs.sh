#!/bin/sh

full_sdk_delivery_root=$(pwd)

patch "$full_sdk_delivery_root/libs/fraunhofer-lib/externals/tinycbor/src/cborvalidation.c" "$full_sdk_delivery_root/libs/tinycbor_fix.patch"

echo "Compiling libcoap..."
cd "$full_sdk_delivery_root/libs/libcoap"
./autogen.sh
./configure --enable-dtls=no --disable-documentation
make

echo "Compiling libtinycbor"
cd "$full_sdk_delivery_root/libs/fraunhofer-lib/externals/tinycbor/" && make lib/libtinycbor.a
