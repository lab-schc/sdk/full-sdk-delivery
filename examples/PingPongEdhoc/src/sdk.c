/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <arpa/inet.h>

#include "fullsdknet.h"
#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "platform.h"
#include "oscore_init.h"
#include "sdk_global.h"

static sem_t sdk_mgt_process_sem;

sem_t data_sent_sem;
sem_t data_rcvd_sem;

#define RECEIVE_BUFFER_SIZE 1500
#define APP_PAYLOAD_SIZE 100

// Assume (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE RECEIVE_BUFFER_SIZE
#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                              \
    (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u + \
     MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

uint8_t app_rx_buf[RECEIVE_BUFFER_SIZE];

uint8_t rx_buf[RECEIVE_BUFFER_SIZE] = {0};
uint16_t rx_buf_size = 0;

static TimerEvent_t sdk_timers[3];

// MGT callbacks.
static void cb_mgt_processing_required(void)
{
    sem_post(&sdk_mgt_process_sem);
}

static void cb_mgt_connectivity_state(mgt_status_t status)
{
    if (status != MGT_SUCCESS)
    {
        printf("connectivity KO (status %d)\n", status);
        return;
    }
    printf("connectivity OK\n");
}

static void cb_start_timer(uint8_t id, uint32_t duration)
{
    TimerSetValue(&sdk_timers[id], duration);
    TimerStart(&sdk_timers[id]);
}

static void cb_stop_timer(uint8_t id)
{
    TimerStop(&sdk_timers[id]);
}

// NET callbacks.
static void net_transmission_result(net_status_t status, uint16_t error)
{
    (void)error;

    if (status != NET_SUCCESS)
    {
        printf("transmission result KO (status %d)\n", status);
        return;
    }
    printf("transmission result OK\n");
    sem_post(&data_sent_sem);
}

static void net_data_received(const uint8_t *buffer, uint16_t data_size,
                              net_status_t status)
{

    rx_buf_size = data_size;
    printf("data received %d bytes - status %d\n", data_size, status);
    uint8_t b;
    for (uint16_t i = 0; i < data_size; i++)
    {
        b = buffer[i];
        rx_buf[i] = buffer[i];
        printf("%02X ", b);
        if ((i + 1) % 10 == 0)
            printf("\n\r");
    }
    printf("\n\r");

    sem_post(&data_rcvd_sem);
}

static mgt_callbacks_t mgt_callbacks = {
    cb_mgt_processing_required,
    cb_mgt_connectivity_state,
    cb_start_timer,
    cb_stop_timer,
    NULL,
};

static net_callbacks_t net_callbacks = {net_transmission_result,
                                        net_data_received};

static void sdk_timer_1_event(void *context)
{
    (void)context;
    mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
    (void)context;
    mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
    (void)context;
    mgt_timer_timeout(2);
}

void *thread_listen(void *vargp)
{
    (void)vargp;
    while (true)
    {
        platform_enter_low_power_ll();
    }

    return NULL;
}

void *thread_sdk(void *vargp)
{
    (void)vargp;
    while (true)
    {
        if (sem_wait(&sdk_mgt_process_sem) < 0)
        {
            printf("sdk_mgt_process_sem failed\n");
        }

        mgt_status_t mgt_status = mgt_process();
        if (mgt_status != MGT_SUCCESS)
        {
            printf("mgt_process failed: %d\n", mgt_status);
        }
    }
    return NULL;
}

bool generate_udp_packet(uint8_t *payload, uint16_t payload_size, uint8_t *packet_out, uint16_t *packet_out_size)
{
    uint8_t ipv6_packet[] = {0x60, 0x00, 0x00, 0x00, 0x00, 0x49, 0x11, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x16, 0x3E, 0x16, 0x3F, 0x00, 0x49, 0x00, 0x00};

    if (!memcpy(packet_out, ipv6_packet, sizeof(ipv6_packet)))
        return false;
    if (!memcpy(packet_out + sizeof(struct ip6_hdr) + sizeof(struct udphdr), payload, payload_size))
        return false;
    *packet_out_size = sizeof(struct ip6_hdr) + sizeof(struct udphdr) + payload_size;
    return true;
}

bool init(char **argv)
{
    printf("FullSDK version: %s\n", mgt_get_version());

    sem_init(&data_rcvd_sem, 0, 0);
    sem_init(&sdk_mgt_process_sem, 0, 0);
    sem_init(&data_sent_sem, 0, 0);

    // SDK timers initialization.
    TimerInit(&sdk_timers[0], sdk_timer_1_event);
    TimerInit(&sdk_timers[1], sdk_timer_2_event);
    TimerInit(&sdk_timers[2], sdk_timer_3_event);

    // should be more than 250 for RESPONDER_TEST_3
    //  Configure L2 MTU.
    l2_set_mtu(255);

    // Configure socket parameters.
    l2_set_ipv4_host_addr(argv[2]);
    l2_set_udp_src_port(argv[3]);
    l2_set_ipv4_remote_addr(argv[4]);
    l2_set_udp_dest_port(argv[5]);

    // init api rules
    net_set_host_static_ip("::");
    net_set_host_static_port("5694");
    net_set_remote_static_ip("::");
    net_set_remote_static_port("5695");

    // Informs the SDK regarding the application mode in order to use the correct
    // fragmentation profile.
    mgt_set_mode(sdk_mode);

    mgt_status_t st = mgt_initialize(&mgt_callbacks, mgt_mem_block, MEM_BLOCK_SIZE,
                                     MAX_MTU_SIZE, MAX_PAYLOAD_SIZE);
    if (st != MGT_SUCCESS)
    {
        printf("Error : mgt_initialize() failed: %d\n", st);
        return false;
    }
    net_status_t status = net_initialize(&net_callbacks);
    if (status != NET_SUCCESS)
    {
        printf("Error : net_initialize() failed (status %d)\n", status);
        return false;
    }
    return true;
}
