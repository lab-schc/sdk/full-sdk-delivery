/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#include <stdint.h>
#include <string.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include "edhoc.h"
#include "sdk_global.h"
#include "libcoap.h"
#include "pdu.h"
#include "fullsdknet.h"
#include "sdk.h"

static coap_pdu_t *edhoc_pdu;

static uint8_t *get_coap_packet_bytes(coap_pdu_t *pdu)
{
    return pdu->token - pdu->hdr_size;
}

static size_t get_coap_packet_len(coap_pdu_t *pdu)
{
    return pdu->used_size + pdu->hdr_size;
}

static bool send_edhoc_coap_packet(coap_pdu_t *write_out)
{
    uint8_t packet[MAX_PACKET_SIZE_BYTES] = {0};
    uint16_t packet_size = 0;

    if (!generate_udp_packet(get_coap_packet_bytes(write_out), get_coap_packet_len(write_out), packet, &packet_size))
        return false;
    PRINT_ARRAY("SEND CoAP message", get_coap_packet_bytes(write_out), get_coap_packet_len(write_out));
    PRINT_ARRAY("SEND IPv6/UDP message", packet, packet_size);
    net_status_t net_status = net_sendto(packet, packet_size);
    if (net_status != NET_SUCCESS)
    {
        printf("net_sendto() failed (status %d)\n", net_status);
        return false;
    }
    // wait net_transmission result to process
    if (sem_wait(&data_sent_sem) < 0)
    {
        printf("Error : sem_wait 1\n");
        return false;
    }
    return true;
}

static bool generate_init_coap_packet(uint8_t *data, uint32_t data_len)
{
    static uint16_t mid = 0;
    static uint32_t token = 1;
    char *uri = ".well-known/edhoc";

    coap_pdu_t *write_out = coap_pdu_init(COAP_MESSAGE_CON, COAP_REQUEST_POST, mid++, 512);
    if (!coap_add_token(write_out, sizeof(token), (uint8_t *)&token))
        return false;
    token++;
    if (!coap_pdu_encode_header(write_out, COAP_PROTO_UDP))
        return false;
    if (!coap_add_option(write_out, COAP_OPTION_URI_PATH, strlen(uri), (uint8_t *)uri))
        return false;
    if (!coap_add_data(write_out, data_len, data))
        return false;
    return send_edhoc_coap_packet(write_out);
}

static bool generate_responder_coap_packet(uint8_t *data, uint32_t data_len)
{
    const uint8_t code_post = 68;

    coap_pdu_t *write_out = coap_pdu_init(COAP_MESSAGE_ACK, code_post, edhoc_pdu->tid, 512);
    if (!coap_add_token(write_out, edhoc_pdu->token_length, edhoc_pdu->token))
        return false;
    if (!coap_pdu_encode_header(write_out, COAP_PROTO_UDP))
        return false;
    if (!coap_add_data(write_out, data_len, data))
        return false;
    return send_edhoc_coap_packet(write_out);
}

EdhocError tx(uint8_t *data, uint32_t data_len)
{
    bool error_gen = true;

    printf("EDHOC COAP TX!\n");
    /*construct a CoAP packet with libcoap whether is a initiator or Responder*/
    if (sdk_mode == SDK_DEVICE_MODE)
        error_gen = generate_init_coap_packet(data, data_len);
    else
        error_gen = generate_responder_coap_packet(data, data_len);
    if (!error_gen)
        return ErrorMessageSent;
    return EdhocNoError;
}

EdhocError rx(uint8_t *data, uint32_t *data_len)
{
    if (sem_wait(&data_rcvd_sem) < 0)
    {
        return ErrorMessageSent;
    }
    uint16_t coap_packet_size = rx_buf_size - (sizeof(struct ip6_hdr) + sizeof(struct udphdr));
    uint8_t *coap_packet = rx_buf + (sizeof(struct ip6_hdr) + sizeof(struct udphdr));

    PRINT_ARRAY("EDHOC IPv6/UDP RX", rx_buf, rx_buf_size)
    PRINT_ARRAY("EDHOC COAP RX", coap_packet, coap_packet_size)
    edhoc_pdu = coap_pdu_init(COAP_MESSAGE_CON, 0, 0, coap_packet_size);
    // convert coap packet to pdu
    if (!coap_pdu_parse(COAP_PROTO_UDP, coap_packet, coap_packet_size, edhoc_pdu))
    {
        printf("Error : failed to parse a CoAP packet\n");
        return ErrorMessageSent;
    }
    // save data payload inside data and payload size inside date_len
    uint8_t *data_p;
    if (coap_get_data(edhoc_pdu, (size_t *)data_len, &data_p) == 0)
    {
        printf("Error : Failed to get COAP payload\n");
        return ErrorMessageSent;
    }
    if (!memcpy(data, data_p, *data_len))
    {
        printf("Error: Failed memcpy\n");
        return MessageBuffToSmall;
    }
    return EdhocNoError;
}
