/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>

#include "fullsdkextapi.h"
#include "fullsdkfragapi.h"
#include "app.h"
#include "sdk.h"

sdk_mode_t sdk_mode;

static int parse_params(int argc, char **argv)
{
    if (argc != 6)
    {
        printf("Usage: %s [mode] "
               "[l2 host ipv4] [l2 host port] [l2 remote ipv4] [l2 remote port]\n",
               argv[0]);
        return EXIT_FAILURE;
    }
    if (strcmp(argv[1], "client") == 0)
    {
        sdk_mode = SDK_DEVICE_MODE;
    }
    else if (strcmp(argv[1], "server") == 0)
    {
        sdk_mode = SDK_APP_MODE;
    }
    else
    {
        printf("invalid sdk mode: %s\n", argv[1]);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
    if (parse_params(argc, argv) != EXIT_SUCCESS)
    {
        return EXIT_FAILURE;
    }

    if (!init(argv))
    {
        return EXIT_FAILURE;
    }

    if (!run())
    {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}