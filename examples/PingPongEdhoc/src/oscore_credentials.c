/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#include <stdlib.h>
#include <stdint.h>

/*Test vector C1.1: Key derivation with Master Salt*/
uint8_t MASTER_SECRET[16] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                             0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10};
uint8_t MASTER_SECRET_LEN = sizeof(MASTER_SECRET);

uint8_t *SENDER_ID = NULL;
uint8_t SENDER_ID_LEN = 0;

uint8_t RECIPIENT_ID[1] = {0x01};
uint8_t RECIPIENT_ID_LEN = sizeof(RECIPIENT_ID);

uint8_t MASTER_SALT[8] = {0x9e, 0x7c, 0xa9, 0x22, 0x23, 0x78, 0x63, 0x40};
uint8_t MASTER_SALT_LEN = sizeof(MASTER_SALT);

uint8_t *ID_CONTEXT = NULL;
uint8_t ID_CONTEXT_LEN = 0;
