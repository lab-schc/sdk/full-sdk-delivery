/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#include "oscore.h"
#include "edhoc.h"
#include "edhoc_init.h"
#include "oscore_init.h"

#include "sdk.h"
#include "sdk_global.h"

//edhoc and OSCORE
static struct context c_sender;
static struct context c_receiver;

static void *thread_app(void *vargp)
{
    if (sdk_mode == SDK_DEVICE_MODE)
    {
        if (!send_oscore(&c_sender, &c_receiver))
        {
            printf("Error : send_oscore failed\n");
        }
    }

    while (true)
    {
        if (sem_wait(&data_rcvd_sem) < 0)
        {
            printf("Error : data_rcvd_sem in thread_app failed\n");
        }

        if (!receive_oscore(&c_sender, &c_receiver))
        {
            printf("Error : receive_oscore failed\n");
        }

        if (!send_oscore(&c_sender, &c_receiver))
        {
            printf("Error : send_oscore failed\n");
        }
    }
}

bool run(void)
{
    uint8_t oscore_master_secret[16];
    uint8_t oscore_master_salt[8];
    pthread_t thread_id_sdk, thread_id_listen, thread_id_app;
    bool edhoc_init;

    pthread_create(&thread_id_sdk, NULL, thread_sdk, NULL);
    pthread_create(&thread_id_listen, NULL, thread_listen, NULL);
    if (sdk_mode == SDK_DEVICE_MODE)
    {
        edhoc_init = edhoc_initiator_init(oscore_master_secret, sizeof(oscore_master_secret), oscore_master_salt, sizeof(oscore_master_salt));
    }
    else
    {
        edhoc_init = edhoc_responder_init(oscore_master_secret, sizeof(oscore_master_secret), oscore_master_salt, sizeof(oscore_master_salt));
    }

    if (!edhoc_init)
    {
        printf("\nError : edhoc_init failed\n");
        return true;
    }

    if (!init_oscore(&c_sender, &c_receiver, oscore_master_secret, oscore_master_salt))
    {
        printf("\nError : init_oscore failed\n");
        return true;
    }

    pthread_create(&thread_id_app, NULL, thread_app, NULL);

    // idle thread
    while (true)
    {
        pause();
    }

    return true;
}