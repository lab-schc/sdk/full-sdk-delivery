/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include "oscore.h"
#include "credentials.h"
//only aimed to use PRINT_ARRAY MACRO
#include "edhoc.h"
#include "oscore_credentials.h"
#include "sdk_global.h"
#include "fullsdknet.h"
#include "sdk.h"

//coap packet GET with number 0 as payload. it will be incremented each time the server receive the packet
static uint8_t coap_packet[] = {0x42, 0x01, 0xD8, 0x76, 0xAB, 0x27, 0xB6, 0x6F, 0x73, 0x63, 0x6F, 0x72, 0x65, 0x05, 0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x04, 0x63, 0x6F, 0x61, 0x70, 0xFF, 0x30};

bool receive_oscore(struct context *c_sender, struct context *c_receiver)
{
    uint8_t coap_rx_buf[256] = {0};
    uint16_t coap_rx_buf_len;
    bool oscore_flagx = false;
    uint8_t *oscore_packet = rx_buf + (sizeof(struct ip6_hdr) + sizeof(struct udphdr));
    uint16_t oscore_packet_size = rx_buf_size - (sizeof(struct ip6_hdr) + sizeof(struct udphdr));

    printf("-----------------------------RECEIVED PACKET---------------------------------------\n");
    //convert oscore to coap
    OscoreError r = oscore2coap(oscore_packet, oscore_packet_size, coap_rx_buf, &coap_rx_buf_len, &oscore_flagx, c_receiver);
    if (r != OscoreNoError)
    {
        PRINT_ARRAY("PACKET_RECEIVED: ", (uint8_t *)rx_buf, rx_buf_size);
        printf("Error : oscore2coap: %d\n", r);
        return false;
    }
    if (oscore_flagx)
    {
        PRINT_ARRAY("OSCORE: ", oscore_packet, oscore_packet_size);
        PRINT_ARRAY("COAP: ", (uint8_t *)coap_rx_buf, coap_rx_buf_len);
    }
    else
    {
        PRINT_ARRAY("COAP: ", (uint8_t *)rx_buf, rx_buf_size);
    }
    printf("--------------------------------RECEIVE ENDED-------------------------------------\n\n");
    //if the server receive the packet it increments the payloads
    if (sdk_mode != SDK_DEVICE_MODE)
    {
        coap_packet[sizeof(coap_packet) - 1]++;
    }
    if (sem_post(&data_sent_sem) < 0)
    {
        printf("Error : data_sent_sem in OSCORE\n");
        return false;
    }
    return true;
}

bool send_oscore(struct context *c_sender, struct context *c_receiver)
{
    printf("--------------------------------SENDING PACKET------------------------------\n");
    uint8_t oscore_buf[1024];
    uint16_t oscore_buf_len = 0;

    PRINT_ARRAY("COAP: ", coap_packet, sizeof(coap_packet));
    OscoreError r = coap2oscore(coap_packet, sizeof(coap_packet), oscore_buf, &oscore_buf_len, c_sender);
    if (r != OscoreNoError)
    {
        printf("Error : coap2oscore: %d\n", r);
        return false;
    }
    PRINT_ARRAY("OSCORE: ", oscore_buf, oscore_buf_len);

    uint8_t packet[MAX_PACKET_SIZE_BYTES] = {0};
    uint16_t packet_size = 0;

    if (!generate_udp_packet(oscore_buf, oscore_buf_len, packet, &packet_size))
        return false;
    PRINT_ARRAY("SEND IPv6/UDP message", packet, packet_size);
    net_status_t net_status = net_sendto(packet, packet_size);
    if (net_status != NET_SUCCESS)
    {
        printf("net_sendto() failed (status %d)\n", net_status);
        return false;
    }
    //stop main thread
    if (sem_wait(&data_sent_sem) < 0)
    {
        printf("Error : data_sent_sem in OSCORE\n");
        return false;
    }
    //increment coap packet
    printf("--------------------------------SENDING ENDED------------------------------\n");
    return true;
}

bool init_oscore(struct context *c_sender, struct context *c_receiver,
                 uint8_t *oscore_master_secret, uint8_t *oscore_master_salt)
{
    /*OSCORE contex initialization*/
    struct oscore_init_params params_sender = {
        CLIENT,
        MASTER_SECRET_LEN,
        oscore_master_secret, //MASTER_SECRET,
        SENDER_ID_LEN,
        SENDER_ID,
        RECIPIENT_ID_LEN,
        RECIPIENT_ID,
        ID_CONTEXT_LEN,
        ID_CONTEXT,
        MASTER_SALT_LEN,
        oscore_master_salt, //MASTER_SALT,
        OSCORE_AES_CCM_16_64_128,
        OSCORE_SHA_256,
    };
    OscoreError r = oscore_context_init(&params_sender, c_sender);

    if (r != OscoreNoError)
    {
        printf("Error : during establishing an OSCORE security context!\n");
        return false;
    }

    /*OSCORE contex initialization*/
    struct oscore_init_params params_receiver = {
        SERVER,
        MASTER_SECRET_LEN,
        oscore_master_secret, //MASTER_SECRET,
        RECIPIENT_ID_LEN,
        RECIPIENT_ID,
        SENDER_ID_LEN,
        SENDER_ID,
        ID_CONTEXT_LEN,
        ID_CONTEXT,
        MASTER_SALT_LEN,
        oscore_master_salt, //MASTER_SALT,
        OSCORE_AES_CCM_16_64_128,
        OSCORE_SHA_256};

    r = oscore_context_init(&params_receiver, c_receiver);

    if (r != OscoreNoError)
    {
        printf("Error : during establishing an OSCORE security context!\n");
        return false;
    }
    return true;
}
