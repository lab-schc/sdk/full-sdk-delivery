/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef CREDENTIALS_SELECT_H_
#define CREDENTIALS_SELECT_H_

#include <stdbool.h>
#include <stdint.h>

/** This function allows us to start the edhoc initiator
 * 
 * Parameters:
 * - oscore_master_secret: buffer where we derive application specific oscore master secret key
 * - oscore_master_secret_size: size of oscore_master_secret key
 * - oscore_master_salt: buffer where we derive application specific oscore master salt
 * - oscore_master_salt_size: size of oscore_master_salt key
 * 
 * Returned Value: true if the operation is successful, false if not 
 **/
bool edhoc_initiator_init(uint8_t *oscore_master_secret, uint16_t oscore_master_secret_size, uint8_t *oscore_master_salt, uint16_t oscore_master_salt_size);

/** This function allows us to start the edhoc responder
 * 
 * Parameters:
 * - oscore_master_secret: buffer where we derive application specific oscore master secret key
 * - oscore_master_secret_size: size of oscore_master_secret key
 * - oscore_master_salt: buffer where we derive application specific oscore master salt
 * - oscore_master_salt_size: size of oscore_master_salt key
 * 
 * Returned Value: true if the operation is successful, false if not 
 **/
bool edhoc_responder_init(uint8_t *oscore_master_secret, uint16_t oscore_master_secret_size, uint8_t *oscore_master_salt, uint16_t oscore_master_salt_size);

#endif