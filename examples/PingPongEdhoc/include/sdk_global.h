/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef SDK_GLOBAL_H
#define SDK_GLOBAL_H

#include <stdint.h>
#include <semaphore.h>
#include "fullsdkextapi.h"
#include "fullsdkfragapi.h"

#define RECEIVE_BUFFER_SIZE 1500
#define MAX_PACKET_SIZE_BYTES 1500
extern sdk_mode_t sdk_mode;
extern uint8_t app_rx_buf[RECEIVE_BUFFER_SIZE];
extern uint8_t rx_buf[RECEIVE_BUFFER_SIZE];
extern uint16_t rx_buf_size;
extern sem_t data_sent_sem;
extern sem_t data_rcvd_sem;

#endif