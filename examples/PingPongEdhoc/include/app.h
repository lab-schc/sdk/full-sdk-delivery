/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef APP_H_
#define APP_H_

#include <stdbool.h>
#include <stdint.h>

/** This function allows us to do all the edhoc and oscore initiation and start oscore PingPong app thread
 * 
 * Parameters: None
 * 
 * Returned Value: true if the operation is successful, false if not 
 **/
bool run(void);

#endif