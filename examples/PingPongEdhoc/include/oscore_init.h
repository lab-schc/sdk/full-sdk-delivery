/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef OSCORE_INIT_H
#define OSCORE_INIT_H

#include <stdbool.h>
#include <stdint.h>
#include "oscore.h"

/** This function allows us to init oscore contexts passed as parameters
 * 
 * Parameters:
 * - c_sender: sender OSCORE context
 * - c_receiver: receiver OSCORE context
 * - oscore_master_secret: application key derived thanks to edhoc
 * - oscore_master_salt: application key derived thanks to edhoc
 * Returned Value: true if the operation is successful, false if not
 **/
bool init_oscore(struct context *c_sender, struct context *c_receiver,
                 uint8_t *oscore_master_secret, uint8_t *oscore_master_salt);

/** This function allows us to handle an OSCORE packet by converting an OSCORE packet to a plain CoAP one
 * 
 * Parameters:
 * - c_sender: OSCORE context that allows us to convert COAP TO OSCORE packet
 * - c_receiver: OSCORE context that allows us to convert OSCORE to COAP packet
 * 
 * Returned Value: true if the operation is successful, false if not
 **/
bool receive_oscore(struct context *c_sender, struct context *c_receiver);

/** This function allows us to create and send an OSCORE packet from a COAP one 
 * 
 * Parameters:
 * - c_sender: OSCORE context that allows us to convert COAP TO OSCORE packet
 * - c_receiver: OSCORE context that allows us to convert OSCORE to COAP packet
 * 
 * Returned Value: true if the operation is successful, false if not
 **/
bool send_oscore(struct context *c_sender, struct context *c_receiver);

#endif