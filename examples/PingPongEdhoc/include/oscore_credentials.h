
/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef OSCORE_CREDENTIALS_H
#define OSCORE_CREDENTIALS_H

#include <stdint.h>

extern uint8_t MASTER_SECRET[];
extern uint8_t SENDER_ID[];
extern uint8_t RECIPIENT_ID[];
extern uint8_t MASTER_SALT[];
extern uint8_t ID_CONTEXT[];

extern uint8_t MASTER_SECRET_LEN;
extern uint8_t SENDER_ID_LEN;
extern uint8_t RECIPIENT_ID_LEN;
extern uint8_t MASTER_SALT_LEN;
extern uint8_t ID_CONTEXT_LEN;

#endif