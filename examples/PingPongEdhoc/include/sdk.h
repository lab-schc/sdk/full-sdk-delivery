/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef SDK_H_
#define SDK_H_

#include <stdbool.h>

/** this function is aimed to be run in a thread that allows us to run platform_enter_low_power_ll() without blocking our program
 * 
 * Parameters: None
 * 
 * Returned Value: NULL
 */
void *thread_listen(void *vargp);

/** this function is aimed to be run in a thread that allows us to run mgt_process() when it's needed
 * 
 * Parameters: None
 * 
 * Returned Value: NULL
 */
void *thread_sdk(void *vargp);

/** This function allows us to init Acklio SDK
 * 
 * Parameters:
 * - argv: current program arguments
 * Returned Value: true if the operation is successful, false if not 
 **/
bool init(char **argv);

/** This function allows us to generate a IPv6/UDP packet
 * 
 * Parameters:
 * - payload: data to be sent
 * - payload_size: data size to be sent
 * - packet_out: IPv6/UDP packet
 * - packet_out_size: IPv6/UDP packet size
 * 
 * Returned Value: true if the operation is successful, false if not 
 **/
bool generate_udp_packet(uint8_t *payload, uint16_t payload_size, uint8_t *packet_out, uint16_t *packet_out_size);

#endif