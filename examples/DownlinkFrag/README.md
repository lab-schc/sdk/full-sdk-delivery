# DownlinkFrag

## Description

This sample application provides an example of how to receive downlink messages longer than LoRaWAN MTU. It relies on ACK-on-Error fragmentation.

It joins the Acklio LoRaWAN network, and then wait for any downlink message. When a message is received, its first five bytes are displayed, with its length.

Compression is disabled. The device is switched to class C, currently required for downlink fragmentation session.

A message is written to the serial-over-USB link on a periodic basis, to let the user check that the application is alive.

The main "process" waits for some time after join, and displays LoRaWAN class. This part of the code is here only to provide an example of a simple implementation of a small finite-state automaton.

To find device UDP port, synchronization env variable must be set. Current app doesn't support 'notemplate' template (EXTENSION_API=default).

## Build

```sh
make clean && make app -j FULLSDK_VERSION=dev EXTENSION_API=template TEMPLATE_ID=ipv6udp SYNC_ENABLED=1 APP_NAME=DownlinkFrag PLATFORM=STM32L476RG-Nucleo TOOLCHAIN=gcc-arm-none TARGET=m4 L2_STACK=semtech LORAWAN_DEVEUI=1111111111111111 LORAWAN_APPEUI=1111111111111111 LORAWAN_APPKEY=11111111111111111111111111111111
```

## Hardware target

The target is the ST NUCLEO-L476RG board, with a LoRaWAN extension board.

### LNS

Create a **Web Socket server** connector.

Create a device profile with one application with **fport** set to **0** and **Type** set to **Bytes**, and activate the above connector.

Configure the device for **Class C** and set **RX Window** to **RX2**. Assign the above profile to the device.

### IPCore

A Predefined device template can be used: `DownlinkFragSampleApp`. 

Create a new flow, assign it the LNS connector created above as the source. In device profile, set **Buffering** to **0** and **Device type** to **Class C**. Complement device profile information:

* **App port**: the UDP port of the cloud application. This port has to be used as the source port by the application
* **Dev port**: the port that would be used on the device if UDP compression was active, which is not the case for this sample application
* **Ip6 app iid**: the IID of the cloud application IPv6 address. Start the IID with `::`
* **Ip6 app prefix**: the prefix of the cloud application IPv6 address. End the prefix with `::` 

Add the device to the list of profile devices.

## Run

To run the sample application:

* connect the ST board to a USB port of a PC
* trace messages can be displayed with a terminal emulator. After a few seconds, one of them informs that the device has joined the LoRaWAN network
* on a server connected to the Internet with IPv6, download and run NBA from IPCore Ip Networks. Then install *netcat-openbsd*. Beware: traditional netcat does not support the command below
* enter the following command:

```sh
nc -6 -p <AppPort> -u <deviceAddress> <DevPort>
```

`<AppPort>` is the source application port. `<deviceAddress>` is the IPv6 address assigned to the device by IPCore during IPCore configuration above. `<DevPort>` is the device application port, currently not used.

* once the command has been validated, it waits for user input. Type the payload to be sent, and send it by hitting the *Enter* key. What has been typed will be sent to the device, with an additional final *Line Feed* character (depending on your terminal configuration).
