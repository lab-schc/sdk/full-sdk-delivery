/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Pascal Bodin - pascal@ackl.io
 *         Jerome Elias - jerome.elias@ackl.io
 *
 * Based on ST's End_Node example. Check license here: www.st.com/SLA0044.
 *
 * This sample application provides an example of how to receive downlink
 * fragmented message.
 *
 * Compression is disabled. The device is switched to class C, currently
 * required for downlink fragmentation session.
 *
 * Ensure that IPCore device template and flow are configured according
 * to attached documentation.
 */

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <string.h>

#include "fullsdkdtg.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "platform.h"
#include "fullsdkextapi.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/**
 * To enable low power mode, we have to undefine here for the moment
 * because we disable the low power mode for all platforms by default
 * in ./l2/<l2_name>/flags.cmake. Once we enable by default then
 * this definition can be done inside platform makefile.
 */
#undef LOW_POWER_DISABLE

#define FVERSION 1

// Maximum size of a downlink message.
#define APP_RECEIVE_MAX_LENGTH 512

// Wait period after join, in ms.
#define APP_AFTER_SYNC_PERIOD 4000

// Period between two "alive" messages, in ms.
#define APP_ALIVE_PERIOD 30000

#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE APP_RECEIVE_MAX_LENGTH
#define APP_TRANSMISSION_MAX_LENGTH MAX_PAYLOAD_SIZE + 100

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                                         \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u +              \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* tx timer callback function*/
static void on_app_timer1_event(void *context);
static void on_alive_timer_event(void *context);
static void sdk_timer_1_event(void *context);
static void sdk_timer_2_event(void *context);
static void sdk_timer_3_event(void *context);
static void sdk_timer_4_event(void *context);
static void sdk_timer_5_event(void *context);
static void sdk_timer_6_event(void *context);

// Callbacks for DTG layer.
static void dtg_transmission_result(const dtg_socket_t *socket,
                                  dtg_status_t status, uint16_t error);
static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                            uint16_t data_size, const dtg_sockaddr_t *p_remote,
                            dtg_status_t status);

// Callbacks for MGT layer.
static void mgt_processing_required(void);
static void mgt_connectivity_state(mgt_status_t state);
static void mgt_start_timer(uint8_t id, uint32_t duration);
static void mgt_stop_timer(uint8_t id);
static void mgt_bootstrap_result(mgt_status_t state);

// Application TX task.
static void app_tx_task(void);

/* Private variables ---------------------------------------------------------*/

// For FullSDK events.
static bool mgt_process_request = false;

// For application events.
static bool app_process_request = false;
static bool app_connectivity_event = false;
static bool app_boostrap_event = false;
static bool app_timer1_event = false;
static bool app_alive_timer_event = false;

// For reception.
static bool rx_process_request = false;

// Current state of application transmission state machine.
typedef enum
{
  TX_WAIT_JOIN,
  TX_WAIT_SYNC,
  TX_WAIT_AFTER_SYNC
} appTxState_t;

static appTxState_t app_tx_current_state = TX_WAIT_JOIN;

static uint8_t tx_buffer[APP_TRANSMISSION_MAX_LENGTH];

// Application reception buffer.
static uint8_t app_rx_data[APP_RECEIVE_MAX_LENGTH];
static uint16_t app_rx_data_len;
static dtg_status_t app_rx_status;

// Timer used for wait after join.
static TimerEvent_t app_timer1;
// Timer used to check application liveliness.
static TimerEvent_t alive_timer;

static TimerEvent_t sdk_timers[6];

static dtg_sockaddr_t socket_addr = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // addr
    {0, 0}                                           // port
};

static dtg_socket_t *socket;

static int8_t datarate = -1; // -1 to indicate ADR otherwise datarate
/* Private functions ---------------------------------------------------------*/

/**
 *
 */
int main(void)
{
  // Platform initialization.
  platform_hw_init();

  // Initialize system to enter sleep mode instead of standby mode
  platform_configure_sleep_mode();

  // L2 init
  l2_init();

  PRINT_MSG("app> LoRa(WAN) version: %X\n", FVERSION);
  PRINT_MSG("app> DownlinkFrag - 1.4.0\n");
  PRINT_MSG("app> FullSDK version: %s\n", mgt_get_version());

#if defined(APP_DR)
#if APP_DR < 0 || APP_DR > 5
  PRINT_MSG("app> error - datarate must be between 0 and 5\n");
#define APP_DR 3
#endif
  datarate = APP_DR;
#endif

  // Initialize SDK timers.
  platform_timer_add(&sdk_timers[0], 0, sdk_timer_1_event, NULL);
  platform_timer_add(&sdk_timers[1], 1, sdk_timer_2_event, NULL);
  platform_timer_add(&sdk_timers[2], 2, sdk_timer_3_event, NULL);
  platform_timer_add(&sdk_timers[3], 3, sdk_timer_4_event, NULL);
  platform_timer_add(&sdk_timers[4], 4, sdk_timer_5_event, NULL);
  platform_timer_add(&sdk_timers[5], 5, sdk_timer_6_event, NULL);

  // Initialize MGT layer.
  mgt_callbacks_t callbacks = {mgt_processing_required, mgt_connectivity_state,
                               mgt_start_timer, mgt_stop_timer, mgt_bootstrap_result};

  mgt_status_t mgt_status =
      mgt_initialize(&callbacks, mgt_mem_block, MEM_BLOCK_SIZE, MAX_MTU_SIZE,
                     MAX_PAYLOAD_SIZE);
  if (mgt_status != MGT_SUCCESS)
  {
    PRINT_MSG("dtg> error in MGT initialization: %d\n", mgt_status);
  }

  while (true)
  {
    if (mgt_process_request)
    {
      mgt_process_request = false;
      mgt_status = mgt_process();
      if (mgt_status != MGT_SUCCESS)
      {
        PRINT_MSG("dtg> error in mgt_process: %d\n", mgt_status);
      }
    }
    if (app_process_request)
    {
      app_process_request = false;
      app_tx_task();
    }
    if (app_alive_timer_event)
    {
      app_alive_timer_event = false;
      PRINTNOW();
      PRINT_MSG("app> alive\n");
      platform_timer_set_duration(&alive_timer, APP_ALIVE_PERIOD);
      platform_timer_start(&alive_timer);
    }
    if (rx_process_request)
    {
      rx_process_request = false;
      // The only processing we perform in this example is to display
      // start of received data.
      uint8_t b;
      switch (app_rx_status)
      {
        case DTG_SUCCESS:
          PRINT_MSG("app> datagram received - length: %d - first "
                    "bytes:\n  ",
                    app_rx_data_len);
          for (uint8_t i = 0; !(i >= app_rx_data_len) && !(i >= 5);
               i++)
          {
            b = app_rx_data[i];
            PRINT_MSG("%02X ", b);
          }
          PRINT_MSG("...\n");
          break;
        case DTG_BUFFER_ERR:
          PRINT_MSG("app> receive buffer is too small\n");
          break;
        case DTG_OVERRUN_ERR:
          PRINT_MSG("app> receive overrun\n");
          break;
        default:
          PRINT_MSG("app> unknown status: %d\n",
                    app_rx_status);
          break;
      }
    }

    /* If a flag is set at this point, mcu must not enter low power and must
     * loop */
    BEGIN_CRITICAL_SECTION();
    /* if an interrupt has occurred after DISABLE_IRQ, it is kept pending
     * and cortex will not enter low power anyway  */
    if ((!mgt_process_request) && (!app_process_request) && (!rx_process_request))
    {
#ifndef LOW_POWER_DISABLE
      platform_enter_low_power_ll();
#endif
    }
    END_CRITICAL_SECTION();
  }
}

/**
 * Will not be called.
 */
static void dtg_transmission_result(const dtg_socket_t *socket,
                                  dtg_status_t status, uint16_t error)
{
  PRINT_MSG("sdk> dtg_transmission_result called\n");
}

/**
 *
 */
static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                            uint16_t data_size, const dtg_sockaddr_t *p_remote,
                            dtg_status_t status)
{

  // Tell main loop.
  app_rx_data_len = data_size;
  app_rx_status = status;
  // Ensure no buffer overflow.
  if (data_size > APP_RECEIVE_MAX_LENGTH)
  {
    PRINT_MSG("sdk> dtg_data_received - too much data received: %d\n", data_size);
    data_size = APP_RECEIVE_MAX_LENGTH;
  }
  memcpy(app_rx_data, p_buffer, data_size);
  rx_process_request = true;
}

/**
 *
 */
static void mgt_processing_required(void)
{
  mgt_process_request = true;
}

/**
 *
 */
static void mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    PRINT_MSG("sdk> mgt_connectivity_state - OK\n");
    app_connectivity_event = true;
    app_process_request = true;
  }
}

static void mgt_bootstrap_result(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    PRINT_MSG("sdk> mgt_bootstrap_result - OK\n"); 
    app_boostrap_event = true;
    app_process_request = true;
  }
  else
  {
    PRINT_MSG("sdk> mgt_bootstrap_result - KO: %d\n\r", state);
  }
}

/**
 *
 */
static void on_app_timer1_event(void *context)
{
  app_timer1_event = true;
  app_process_request = true;
}

/**
 *
 */
static void on_alive_timer_event(void *context)
{
  app_alive_timer_event = true;
}

static void mgt_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&sdk_timers[id], duration);
  platform_timer_start(&sdk_timers[id]);
}

static void mgt_stop_timer(uint8_t id)
{
  platform_timer_stop(&sdk_timers[id]);
}

static void sdk_timer_1_event(void *context)
{
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  mgt_timer_timeout(5);
}

/**
 *
 */
static void app_tx_task(void)
{
  dtg_status_t dtg_status;
  char class;

  switch (app_tx_current_state)
  {
    case TX_WAIT_JOIN:
      if (app_connectivity_event)
      {
        PRINT_MSG("sdk> +JOINED\n");
        app_connectivity_event = false;

        if (datarate == -1)
        {
          l2_set_adr(true);
          PRINT_MSG("app> ADR is enabled\n");
        }
        else
        {
          l2_set_dr(datarate);
          PRINT_MSG("app> DR set to %d\n", datarate);
        }

        // Request switch to class C.
        if (l2_set_class('C') != L2_SUCCESS)
        {
          PRINT_MSG("app> error - class C switch request\n");
        }
        // Execute SYNC process.
        mgt_status_t mgt_status = mgt_sync_bootstrap(15000, 3);
        if (mgt_status != MGT_SUCCESS)
        {
          PRINT_MSG("sdk> error in mgt_sync_bootstrap: %d\n", mgt_status);
          platform_error_handler();
        }
        // Wait for SYNC response.
        app_tx_current_state = TX_WAIT_SYNC;
        break;
      }
      PRINT_MSG("app> error - not a connectivity event\n");
      break;

    case TX_WAIT_SYNC:
      if (app_boostrap_event)
      {
        PRINT_MSG("sdk> +SYNC\n");
        app_boostrap_event = false;

        // Only get device port
        uint8_t *param;
        mgt_status_t mgt_status = mgt_get_template_param(4, &param, NULL);
        if (mgt_status == MGT_SUCCESS)
        {
          memcpy(socket_addr.port, param, sizeof(socket_addr.port));
        }
        else
        {
          PRINT_MSG("sdk> error in mgt_get_template_param: %d\n", mgt_status);
          platform_error_handler();
        }

        // Initialize DTG interface to create and initialize DTG socket
        dtg_callbacks_t dtg_callbacks = {dtg_transmission_result, dtg_data_received};
        dtg_status_t dtg_status = dtg_initialize(&dtg_callbacks);
        if (dtg_status != DTG_SUCCESS)
        {
          PRINT_MSG("sdk> error in dtg_initialize: %d\n", dtg_status);
          platform_error_handler();
        }

        // dtg_transmission_result will never be called but is required.
        socket = dtg_socket(tx_buffer, APP_TRANSMISSION_MAX_LENGTH);
        if (socket == DTG_INVALID_SOCKET)
        {
          PRINT_MSG("sdk> error in socket creation\n");
          platform_error_handler();
        }
        dtg_status = dtg_bind(socket, &socket_addr);
        if (dtg_status != DTG_SUCCESS)
        {
          PRINT_MSG("sdk> error in socket bind: %d\n", dtg_status);
          platform_error_handler();
        }

        // Start alive signalling.
        platform_timer_add(&alive_timer, 6, on_alive_timer_event, NULL);
        platform_timer_set_duration(&alive_timer, APP_ALIVE_PERIOD);
        platform_timer_start(&alive_timer);
        // Wait after sync, before sending data.
        platform_timer_add(&app_timer1, 7, on_app_timer1_event, NULL);
        platform_timer_set_duration(&app_timer1, APP_AFTER_SYNC_PERIOD);
        platform_timer_start(&app_timer1);
        app_tx_current_state = TX_WAIT_AFTER_SYNC;
        break;
      }
      PRINT_MSG("app> error - not a boostrap event\n");
      break;

    case TX_WAIT_AFTER_SYNC:
      // For downlink tests, no more send uplink packets.
      if (app_timer1_event)
      {
        PRINT_MSG("app> end of wait after sync\n");
        app_timer1_event = false;
        // Check class.
        l2_get_class(&class);
        PRINT_MSG("app> class: %c\n", class);
        // Stay in this state.
        break;
      }
      PRINT_MSG("app> error - not a timer1 event\n");
      break;

    default:
      PRINT_MSG("app> error: unknown state: %d\n", app_tx_current_state);
  }
}
