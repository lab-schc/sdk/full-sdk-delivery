#!/usr/bin/python3

# Copyright(C) 2019-2020 ACKLIO SAS - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# @author Aydogan Ersoz - aydogan.ersoz@ackl.io
#
# Dummy fuota server imitator to measure multicast downlink efficiency

import sys
import argparse
import logging
import socket
import time
import struct

log_level = {'info': logging.INFO, 'debug': logging.DEBUG}


def convert_bytes(bytes_number):
    tags = ["byte", "kilobyte", "megabyte", "gigabyte", "terabyte"]
    i = 0
    double_bytes = bytes_number
    while (i < len(tags) and bytes_number >= 1024):
        double_bytes = bytes_number / 1024.0
        i = i + 1
        bytes_number = bytes_number / 1024
    return str(round(double_bytes, 2)) + " " + tags[i]


class UDPClient():
    def __init__(self, app_addr, dev_addr, block_size, block_count, sleep, dl_type):
        self.app_addr = app_addr
        self.dev_addr = dev_addr
        self.block_size = block_size
        self.block_count = block_count
        self.sleep = sleep
        self.dl_type = dl_type
        self.sock = None

    def configure(self):
        self.sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
        self.sock.bind(self.app_addr)
        logging.info('application is binded to {}'.format(self.app_addr))
        self.sock.connect(self.dev_addr)
        logging.info('application is connected to {}'.format(self.dev_addr))

    def send(self):
        i = 0
        while i < self.block_count:
            i += 1
            payload = bytearray(self.block_size)
            payload_len = len(payload)
            # some metadata to measure the efficiency on the device side
            block_index = i.to_bytes(2, 'big')
            payload[payload_len - 4] = block_index[0]
            payload[payload_len - 3] = block_index[1]
            payload[payload_len - 2] = \
                ord('M') if self.dl_type == 'multicast' else ord('U')
            payload[payload_len - 1] = ord('\n')  # for nc compatibility
            self.sock.send(payload)
            logging.info('block number {} - {} send to {}'.format(
                i, convert_bytes(self.block_size), self.dev_addr))
            if i == self.block_count:
                break
            else:
                time.sleep(self.sleep)
        self.shutdown()

    def shutdown(self):
        logging.info('shutting down server...')
        self.sock.close()


def main():
    parser = argparse.ArgumentParser(description='FUOTA server imitator')
    parser.add_argument('--app-ip', type=str, required=True,
                        help='IPv6 address of the application')
    parser.add_argument('--app-port', type=int, required=True,
                        help='UDP port of the application')
    parser.add_argument('--dev-ip', type=str, required=True,
                        help='IPv6 address of the device')
    parser.add_argument('--dev-port', type=int, required=True,
                        help='UDP port of the device')
    parser.add_argument('--block-size', type=int, default=200,
                        help='size of a firmware block in bytes')
    parser.add_argument('--block-count', type=int, default=10,
                        help='number of blocks to be sent')
    parser.add_argument('--sleep', type=int, default=120,
                        help='interblock delay in seconds')
    parser.add_argument('--dl-type', type=str, required=True,
                        help='downlink type (unicast or multicast)')
    parser.add_argument('--log-level', default='debug',
                        choices=['info', 'debug'], help='log level of the application')
    args = parser.parse_args()

    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=log_level[args.log_level])

    fw_size = args.block_size * args.block_count

    logging.info('{} of firmware will be sent in {} blocks being {} per block'.format(
        convert_bytes(fw_size), args.block_count, convert_bytes(args.block_size)))

    udp_client = UDPClient((args.app_ip, args.app_port),
                           (args.dev_ip, args.dev_port),
                           args.block_size,
                           args.block_count,
                           args.sleep,
                           args.dl_type)
    udp_client.configure()

    try:
        udp_client.send()
    except KeyboardInterrupt:
        udp_client.shutdown()


if __name__ == '__main__':
    main()
