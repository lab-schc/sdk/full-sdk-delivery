/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Pascal Bodin - pascal@ackl.io
 *
 * This sample application provides an example of how to use the FullSDK with
 * multicast support.
 *
 * It requests to send a message of 500 bytes on a periodic basis.
 *
 * It demonstrates IPv6/UDP compression using raw API (network API).
 *
 * It displays any received IPv6 packet (unicast and multicast)
 *
 * It shows how to enable a multicast session, and displays any received
 * multicast message.
 */

#include <stdint.h>

#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "fullsdknet.h"
#include "platform.h"

/**
 * To enable low power mode, we have to undefine here for the moment
 * because we disable the low power mode for all platforms by default
 * in ./l2/<l2_name>/flags.cmake. Once we enable by default then
 * this definition can be done inside platform makefile.
 */
#undef LOW_POWER_DISABLE

// Size of buffer used to store application payload.
#define APP_PAYLOAD_MAX_LENGTH 500
// Period between two successive transmission requests, in ms.
#define APP_TX_PERIOD 100000
// Period between two "alive" messages, in ms.
#define APP_ALIVE_PERIOD 30000

// Assume (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE APP_PAYLOAD_MAX_LENGTH
#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                            \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u + \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

/* tx timer callback function*/
static void on_tx_timer_event(void *context);
static void on_alive_timer_event(void *context);
static void sdk_timer_1_event(void *context);
static void sdk_timer_2_event(void *context);
static void sdk_timer_3_event(void *context);
static void sdk_timer_4_event(void *context);
static void sdk_timer_5_event(void *context);
static void sdk_timer_6_event(void *context);

// Callbacks for NET layer.
static void net_transmission_result(net_status_t status, uint16_t error);
static void net_data_received(const uint8_t *buffer, uint16_t data_size,
                              net_status_t status);

// Callbacks for MGT layer.
static void mgt_processing_required(void);
static void mgt_connectivity_state(mgt_status_t state);
static void mgt_start_timer(uint8_t id, uint32_t duration);
static void mgt_stop_timer(uint8_t id);
static void mgt_bootstrap_result(mgt_status_t state);

// Application main task.
static void app_main_task(void *args);
// Fake IPv6/UDP/DLMS packet
static void build_fake_ipv6_udp_dlms_packet(void);

// For FullSDK events.
static bool mgt_process_request = false;
// For application events.
static bool app_process_request = false;
static bool app_connectivity_event = false;
static bool app_tx_done_event = false;
static bool appTimerEvent = false;
static bool appAliveTimerEvent = false;

// Current state of application transmission state machine.
typedef enum
{
  TX_WAIT_JOIN,
  TX_WAIT_SEND_DATA,
  TX_WAIT_AFTER_TX,
  TX_WAIT_DUTY_CYCLE,
} appTxState_t;

static appTxState_t app_tx_current_state = TX_WAIT_JOIN;
// Application payload. After a send request, its contents must not be
// modified until transmission result callback is called.
static uint8_t application_payload[APP_PAYLOAD_MAX_LENGTH];
// Counter value sent as part of the application payload.
static uint16_t counter = 0;

static TimerEvent_t TxTimer;
// Timer used to check application liveliness.
static TimerEvent_t aliveTimer;

// SDK timers.
static TimerEvent_t sdk_timers[6];

// Result of the transmission.
static net_status_t tx_status;
static uint16_t tx_error;
// Flag informing that we transmission result has been called.
static bool transmission_result_called = false;

static int8_t datarate = 5; // We are using DR5 by default.
static char class_c = 'C';  // Class C device.

// Fake IPv6/UDP/DLMS header
static const uint8_t ipv6_udp_dlms_header[] = {
    0x60, 0x01, 0x23, 0x45, // [IPv6] Version - Traffic Class - Flow Label
    0x01, 0x18,             // [IPv6] Payload Length (280 bytes)
    0x11, 0x40,             // [IPv6] Next Header (UDP) - Hop Limit
    0x20, 0x0C, 0xAD, 0x10, // [IPv6] Source Address (200c:AD10::2)
    0x00, 0x00, 0x00, 0x00, // [IPv6] Source Address
    0x00, 0x00, 0x00, 0x00, // [IPv6] Source Address
    0x00, 0x00, 0x00, 0x00, // [IPv6] Source Address
    0x00, 0x00, 0x00, 0x00, // [IPv6] Destination Address (::1)
    0x00, 0x00, 0x00, 0x00, // [IPv6] Destination Address
    0x00, 0x00, 0x00, 0x00, // [IPv6] Destination Address
    0x00, 0x00, 0x00, 0x01, // [IPv6] Destination Address
    0x16, 0x33,             // [UDP] Source Port (5683)
    0x16, 0x33,             // [UDP] Destination Port (5683)
    0x01, 0x18,             // [UDP] Length (280 bytes)
    0x36, 0xDE,             // [UDP] Checksum
    0x00, 0x01,             // [DLMS] DLMS-Wrapper-Version
    0x00, 0x01,             // [DLMS] DLMS-Wrapper-SSAP
    0x00, 0x01,             // [DLMS] DLMS-Wrapper-CSAP
    0x01, 0x08,             // [DLMS] DLMS-Wrapper-Length (264 bytes of DLMS payload)
};
static uint8_t host_ipv6[] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0x00,
                              0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};
static uint8_t remote_ipv6[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08,
                                0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16};
static uint8_t host_port[] = {0x54, 0x45};
static uint8_t remote_port[] = {0x32, 0x23};
static const uint8_t dlms_wrapper_ssap[] = {0x00, 0x01};
static const uint8_t dlms_wrapper_csap[] = {0x00, 0x01};

// Multicast session data.
static uint32_t multicast_addr = 0x00112233;

#ifdef SEMTECH_L2
uint8_t mc_app_skey[] = {0x31, 0xec, 0x4f, 0x83, 0x4e, 0x2b, 0x85, 0x66,
                         0x17, 0x6e, 0xc7, 0x01, 0x84, 0x16, 0xc5, 0x73};
uint8_t mc_net_skey[] = {0x0a, 0x7a, 0x98, 0xb8, 0xc7, 0xac, 0x27, 0x88,
                         0x48, 0x4b, 0xae, 0xf3, 0x13, 0xfc, 0x30, 0x75};
#else
static uint8_t multicast_key_encrypted[] = {0xa8, 0xd4, 0x67, 0x03, 0xd4, 0xd0,
                                            0x06, 0xf4, 0x87, 0xea, 0xb1, 0x14,
                                            0xa4, 0xdd, 0x5a, 0x13};
#endif

int main(void)
{
  // Platform initialization.
  platform_hw_init();

  /* Initialize system to enter sleep mode instead of standby mode */
  platform_configure_sleep_mode();

  PRINT_MSG("=> main - DLMSMulticast BareMetal example\n");
  PRINT_MSG("FullSDK version: %s\n", mgt_get_version());

  // Prepare a large IPv6/UDP/DLMS payload, that will require fragmentation.
  build_fake_ipv6_udp_dlms_packet();

  // Initialize SDK timers.
  platform_timer_add(&sdk_timers[0], 0, sdk_timer_1_event, NULL);
  platform_timer_add(&sdk_timers[1], 1, sdk_timer_2_event, NULL);
  platform_timer_add(&sdk_timers[2], 2, sdk_timer_3_event, NULL);
  platform_timer_add(&sdk_timers[3], 3, sdk_timer_4_event, NULL);
  platform_timer_add(&sdk_timers[4], 4, sdk_timer_5_event, NULL);
  platform_timer_add(&sdk_timers[5], 5, sdk_timer_6_event, NULL);

  // Initialize MGT layer.
  mgt_callbacks_t callbacks = {mgt_processing_required, mgt_connectivity_state,
                               mgt_start_timer, mgt_stop_timer, mgt_bootstrap_result};

  mgt_status_t mgtStatus =
      mgt_initialize(&callbacks, mgt_mem_block, MEM_BLOCK_SIZE, MAX_MTU_SIZE,
                     MAX_PAYLOAD_SIZE);
  if (mgtStatus != MGT_SUCCESS)
  {
    PRINT_MSG("=> main - error in MGT initialization: %d\n", mgtStatus);
  }

  net_callbacks_t net_callbacks;
  net_callbacks.transmission_result = net_transmission_result;
  net_callbacks.data_received = net_data_received;

  // initialize network API
  net_status_t net_status = net_initialize(&net_callbacks);
  if (net_status != NET_SUCCESS)
  {
    PRINT_MSG("main - error in network API initialization\n");
    platform_error_handler();
  }
  // Configure source/destination IP/port
  mgt_set_template_param(0, host_ipv6, 8);
  mgt_set_template_param(1, host_ipv6 + 8, 8);
  mgt_set_template_param(2, remote_ipv6, 8);
  mgt_set_template_param(3, remote_ipv6 + 8, 8);
  mgt_set_template_param(4, host_port, 2);
  mgt_set_template_param(5, remote_port, 2);
  mgt_set_template_param(6, dlms_wrapper_ssap, 2);
  mgt_set_template_param(7, dlms_wrapper_csap, 2);

  TimerInit(&aliveTimer, on_alive_timer_event);
  TimerSetValue(&aliveTimer, APP_ALIVE_PERIOD);
  TimerStart(&aliveTimer);

  app_main_task(NULL);
}

/**
 Callback which is called when an IPv6/UDP packet has been transmitted to the
 FullSDK library
 */
static void net_transmission_result(net_status_t status, uint16_t error)
{
  PRINT_MSG("=> net_transmission_result called (status %d)\n", status);
  transmission_result_called = true;
  tx_status = status;
  tx_error = error;
  app_process_request = true;
}

/**
 Callback which is called when an IPv6/UDP packet has been received from the
 FullSDK library
 */
#ifndef TEST_FUOTA
static void net_data_received(const uint8_t *buffer, uint16_t data_size,
                              net_status_t status)
{
  PRINT_MSG("=> net_data_received - %d bytes - status: %d\n", data_size,
            status);

  uint8_t b;
  for (uint16_t i = 0; i < data_size; i++)
  {
    b = buffer[i];
    PRINT_MSG("%02X ", b);
    if ((i + 1) % 10 == 0)
      PRINT_MSG("\n");
  }
  PRINT_MSG("\n");
}
#else
static void net_data_received(const uint8_t *buffer, uint16_t data_size,
                              net_status_t status)
{
  PRINT_MSG("=> net_data_received - %d bytes - status: %d\n", data_size,
            status);

  static uint16_t mc_packets_received = 0;
  static uint16_t uc_packets_received = 0;
  uint16_t mc_current_packet_index;
  uint16_t uc_current_packet_index;

  if (buffer[data_size - 2] == 'M')
  {
    mc_packets_received++;
    mc_current_packet_index =
        buffer[data_size - 4] * 256 + buffer[data_size - 3];
    PRINT_MSG("=> [MC] %d received / %d total\n", mc_packets_received,
              mc_current_packet_index);
  }
  else
  {
    uc_packets_received++;
    uc_current_packet_index =
        buffer[data_size - 4] * 256 + buffer[data_size - 3];
    PRINT_MSG("=> [UC] %d received / %d total\n", uc_packets_received,
              uc_current_packet_index);
  }
}
#endif

static void mgt_processing_required(void)
{
  mgt_process_request = true;
}

static void mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    PRINT_MSG("=> mgt_connectivity_state - OK\n");
    l2_set_dr(datarate);
    PRINT_MSG("=> main - DR set to %d\n", datarate);
    l2_set_class(class_c);
    PRINT_MSG("=> main - set to class %c\n", class_c);

    PRINT_MSG("=> enabling multicast session\n");
    // This function is used to enable multicast on the device. The MAC address
    // and the encrypted MAC key are provided as parameters. It's implementation
    // depends on the L2 stack used.
#ifdef SEMTECH_L2
    uint8_t *keys[2] = {mc_app_skey, mc_net_skey};
    if (l2_enable_multicast(multicast_addr, keys) != L2_SUCCESS)
#else
    uint8_t *keys[1] = {multicast_key_encrypted};
    if (l2_enable_multicast(multicast_addr, keys) != L2_SUCCESS)
#endif
    {
      PRINT_MSG("=> multicast session activation failed\n");
    }

    app_connectivity_event = true;
    app_process_request = true;
  }
}

static void mgt_bootstrap_result(mgt_status_t state)
{
  // NOP
  (void)state;
}

static void on_tx_timer_event(void *context)
{
  (void)context;
  appTimerEvent = true;
  app_process_request = true;
}

static void on_alive_timer_event(void *context)
{
  (void)context;
  appAliveTimerEvent = true;
}

static void mgt_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&sdk_timers[id], duration);
  platform_timer_start(&sdk_timers[id]);
}

static void mgt_stop_timer(uint8_t id)
{
  platform_timer_stop(&sdk_timers[id]);
}

static void sdk_timer_1_event(void *context)
{
  (void)context;
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  (void)context;
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  (void)context;
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  (void)context;
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  (void)context;
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  (void)context;
  mgt_timer_timeout(5);
}

static void app_main_task(void *args)
{
  (void)args;
  while (true)
  {
    if (mgt_process_request)
    {
      mgt_process_request = false;
      mgt_status_t mgtStatus = mgt_process();
      if (mgtStatus != MGT_SUCCESS)
      {
        PRINT_MSG("=> main - error in mgt_process: %d\n", mgtStatus);
      }
    }
    if (app_process_request)
    {
      net_status_t net_status;

      app_process_request = false;

      switch (app_tx_current_state)
      {
      case TX_WAIT_JOIN:
        if (app_connectivity_event)
        {
          PRINT_MSG("=> app_main_task - JOINED\n");
          app_connectivity_event = false;
          // Wait after join before sending data.
          TimerInit(&TxTimer, on_tx_timer_event);
          TimerSetValue(&TxTimer, APP_TX_PERIOD);
          TimerStart(&TxTimer);

          app_tx_current_state = TX_WAIT_SEND_DATA;
          app_tx_done_event = true;
          break;
        }
        // At this stage, not a connectivity event. Error.
        PRINT_MSG("=> app_main_task - error - not a connectivity event\n");
        break;
      case TX_WAIT_SEND_DATA:
        // Send a new uplink packet to be fragmented.
        if (app_tx_done_event)
        {
          PRINT_MSG("=> app_main_task - sending message %u...\n", counter);

          // Request to send a message.
          net_status =
              net_sendto(application_payload, APP_PAYLOAD_MAX_LENGTH);
          if (net_status != NET_SUCCESS)
          {
            PRINT_MSG("=> app_main_task - error on send - retry later : %d\n",
                      net_status);
            // Stay in same state and restart timer.
            TimerSetValue(&TxTimer, APP_TX_PERIOD);
            TimerStart(&TxTimer);
            break;
          }
          // At this stage, TX request success.
          // Increment counter.
          app_tx_done_event = false;
          counter++;
          break;
        }
        // Handle end of transmission session.
        if (transmission_result_called)
        {
          // The transmission session ends. We can send a new packet.
          transmission_result_called = false;

          if (tx_status == NET_SUCCESS)
          {
            PRINT_MSG("=> app_main_task - transmission result: OK\n");
          }
          else
          {
            PRINT_MSG(
                "=> app_main_task - transmission result: KO (status %d)\n",
                tx_status);
          }

          app_tx_done_event = true;
          app_process_request = true;
          break;
        }

        PRINT_MSG("=> app_main_task - waiting...\n");
        // Wait before next TX.
        TimerSetValue(&TxTimer, APP_TX_PERIOD);
        TimerStart(&TxTimer);
        // At this stage, not a timer event. Error.
        break;

      default:
        PRINT_MSG("=> app_main_task - error: unknown state: %d\n",
                  app_tx_current_state);
      }
    }
    if (appAliveTimerEvent)
    {
      appAliveTimerEvent = false;
      PRINTNOW();
      PRINT_MSG("=> main - alive\n");
      TimerSetValue(&aliveTimer, APP_ALIVE_PERIOD);
      TimerStart(&aliveTimer);
    }

    /* If a flag is set at this point, mcu must not enter low power and must
     * loop */
    BEGIN_CRITICAL_SECTION();
    /* if an interrupt has occurred after DISABLE_IRQ, it is kept pending and
     * cortex will not enter low power anyway  */
    if ((!mgt_process_request) && (!app_process_request))
    {
#ifndef LOW_POWER_DISABLE
      platform_enter_low_power_ll();
#endif
    }
    END_CRITICAL_SECTION();
  }
}

static void build_fake_ipv6_udp_dlms_packet(void)
{
  size_t header_size =
      sizeof(ipv6_udp_dlms_header); // 40(IPv6) + 8(UDP) + 8(DLMS)

  memcpy(application_payload, ipv6_udp_dlms_header,
         sizeof(ipv6_udp_dlms_header));

  // DLMS payload
  for (uint16_t i = 0; i < APP_PAYLOAD_MAX_LENGTH - header_size; i++)
  {
    application_payload[header_size + i] = (uint8_t)i;
  }
}
