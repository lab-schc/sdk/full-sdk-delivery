# DLMSMulticast

## Description

*DLMSMulticast* application shows how to send large uplink messages using ACKOnError fragmentation via Network API and also receiving downlink packets, either in unicast using ACKOnError fragmentation, or in multicast using NoACK fragmentation.

The application uses version 4.6.0 of semtech LoRaWAN stack and support STM32L476RG-Nucleo.

## Build

```sh
FULLSDK_VERSION=dev APP_NAME=DLMSMulticast PLATFORM=STM32L476RG-Nucleo TOOLCHAIN=gcc-arm-none TARGET=m4 L2_STACK=semtech EXTENSION_API=kaifa LORAWAN_DEVEUI=1111111111111111 LORAWAN_APPEUI=1111111111111111 LORAWAN_APPKEY=11111111111111111111111111111111 LORAWAN_GENAPPKEY=11111111111111111111111111111111 make app
```

## Multicast efficiency testing

*fuota_server.py* script can be used to imitate a fuota server to test the efficiency of multicast downlink. It sends packets to the IPv6 address on a local machine on which NBA is listening. All the parameters can be set via arguments.

Use the following command to imitate firmware transmission:

```bash
python3 fuota_server.py --app-ip=<app_ip> --app-port=<app_port> --dev-ip=<dev_ip> --dev-port=<dev_port> --sleep=<sleep> --block-size=<block_size> --block-count=<block_count>
```

*app-ip*: IPv6 address of the application
*app-port*: Source UDP port of the application
*dev-ip*: IPv6 address of the device
*dev-port*: Destination UDP port of the device
*sleep*: Interblock wait time (in seconds)
*block-size*: Size of a firmware block to be sent to a device at a time (in bytes)
*block-count*: Number of firmware blocks to be sent to a device
