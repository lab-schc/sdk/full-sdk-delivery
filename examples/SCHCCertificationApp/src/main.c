/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Jerome Elias - jerome.elias@ackl.io
 *
 * Based on ST's AT_Slave example. Check license here: www.st.com/SLA0044.
 * 
 * This sample application provides a bare-metal application using the datagram
 * API, where the device application is sending uplink packet periodically.
 * 
 * The device is switched to class C, currently
 * required for downlink fragmentation session.
 *
 * Transmit payload can up to MAX_PAYLOAD_SIZE bytes, and can be fragmented if 
 * needed.
 */

#include "platform.h"
#include "fullsdkl2.h"
#include "fullsdkl2acert.h"
#include "fullsdkmgtcert.h"
#ifdef TRACE_ENABLED
#include "fullsdktrace.h"
#endif

/* Private variables ---------------------------------------------------------*/
/**
 * To enable low power mode, we have to undefine here for the moment
 * because we disable the low power mode for all platforms by default
 * in ./l2/<l2_name>/flags.cmake. Once we enable by default then
 * this definition can be done inside platform makefile.
 */
#undef LOW_POWER_DISABLE

// Assume (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE SCHC_CERT_APP_MAX_SIZE

#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned

// Memory block size provided to mgt_initialize.
// Need to add allocate two new buffers for SCHC Certification submodule
#define MEM_BLOCK_SIZE                                            \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u + \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

// Template configuration (to load CBOR compression rules)
// Length must be at least of TEMPLATE_BUFFER_SIZE on 4-bytes aligned.
static uint8_t template_buffer[TEMPLATE_BUFFER_SIZE];

// Timers
static timer_obj_t sdk_timers[6];
static timer_obj_t schc_cert_timers[2];

// Process request bool
static bool mgt_process_request = false;
static bool mgt_cert_process_request = false;

// RX data buffer (initially used for network/dtg functions)
static uint8_t rx_buffer[MAX_PAYLOAD_SIZE];
// TX buffer (initially used for network/dtg functions)
static uint8_t tx_buffer[MAX_PAYLOAD_SIZE];

/* timer callback function*/
static void sdk_timer_1_event(void *context);
static void sdk_timer_2_event(void *context);
static void sdk_timer_3_event(void *context);
static void sdk_timer_4_event(void *context);
static void sdk_timer_5_event(void *context);
static void sdk_timer_6_event(void *context);
static void sdk_timer_7_event(void *context);
static void sdk_timer_8_event(void *context);

// Callbacks for MGT layer.
static void mgt_processing_required(void);
static void mgt_connectivity_state(mgt_status_t state);
static void mgt_start_timer(uint8_t id, uint32_t duration);
static void mgt_stop_timer(uint8_t id);

// Callbacks for MGT SCHC Certification layer.
static void mgt_cert_processing_required(void);
static void mgt_cert_start_timer(uint8_t id, uint32_t duration);
static void mgt_cert_stop_timer(uint8_t id);

/* Private functions ---------------------------------------------------------*/
// Callbacks for timers
static void sdk_timer_1_event(void *context)
{
  (void)context;
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  (void)context;
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  (void)context;
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  (void)context;
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  (void)context;
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  (void)context;
  mgt_timer_timeout(5);
}

static void sdk_timer_7_event(void *context)
{
  (void)context;
  mgt_cert_timer_timeout(0);
}

static void sdk_timer_8_event(void *context)
{
  (void)context;
  mgt_cert_timer_timeout(1);
}

static void mgt_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&sdk_timers[id], duration);
  platform_timer_start(&sdk_timers[id]);
}

static void mgt_stop_timer(uint8_t id)
{
  platform_timer_stop(&sdk_timers[id]);
}

static void mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    PRINT_MSG("sdk> +JOINED\n\r");
    // At this stage, DUT is ready to be tested by TCL.
    PRINT_MSG("sdk> SCHC Certification test application ready\n\r");
  }
}

static void mgt_processing_required(void)
{
  mgt_process_request = true;
}

static void mgt_cert_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&schc_cert_timers[id], duration);
  platform_timer_start(&schc_cert_timers[id]);
}

static void mgt_cert_stop_timer(uint8_t id)
{
  platform_timer_stop(&schc_cert_timers[id]);
}

static void mgt_cert_processing_required(void)
{
  mgt_cert_process_request = true;
}

int main(void)
{
  // Platform initialization.
  platform_hw_init();

  // Initialize system to enter sleep mode instead of standby mode
  platform_configure_sleep_mode();

  // L2 layer initialization
  l2_init();

  PRINT_MSG("app> SCHC Certification test application\n\r");
  PRINT_MSG("app> compiled with FullSDK v%s\n\r", mgt_get_version());
  PRINT_MSG("app> starting...\n\r");

  // The check above ensure that l2_set_class can't return an error.
  // l2_set_class only stores the class.
  l2_set_class('C');

  // SDK timers
  platform_timer_add(&sdk_timers[0], 0, sdk_timer_1_event, NULL);
  platform_timer_add(&sdk_timers[1], 1, sdk_timer_2_event, NULL);
  platform_timer_add(&sdk_timers[2], 2, sdk_timer_3_event, NULL);
  platform_timer_add(&sdk_timers[3], 3, sdk_timer_4_event, NULL);
  platform_timer_add(&sdk_timers[4], 4, sdk_timer_5_event, NULL);
  platform_timer_add(&sdk_timers[5], 5, sdk_timer_6_event, NULL);

  // SCHC Certification timers
  platform_timer_add(&schc_cert_timers[0], 6, sdk_timer_7_event, NULL);
  platform_timer_add(&schc_cert_timers[1], 7, sdk_timer_8_event, NULL);

  // Enable SCHC Certification test app
  mgt_cert_enable_cert_app(true);

  // Initialize MGT layers.
  mgt_callbacks_t mgt_callbacks = {mgt_processing_required, mgt_connectivity_state,
                                   mgt_start_timer, mgt_stop_timer,
                                   NULL};

  // Initialize SDK MGT layer.
  mgt_status_t mgt_status =
      mgt_initialize(&mgt_callbacks, mgt_mem_block, MEM_BLOCK_SIZE,
                     MAX_MTU_SIZE, MAX_PAYLOAD_SIZE);
  if (mgt_status != MGT_SUCCESS)
  {
    PRINT_MSG("sdk> error in mgt_initialize: %d\n\r", mgt_status);
    platform_error_handler();
  }

  // Initialize SCHC certification application.
  mgt_cert_callbacks_t mgt_cert_callbacks = {
      mgt_cert_processing_required,
      mgt_cert_start_timer,
      mgt_cert_stop_timer,
  };

  // Initialize SDK MGT layer.
  mgt_status =
      mgt_cert_initialize(&mgt_cert_callbacks, template_buffer, sizeof(template_buffer),
                          rx_buffer, sizeof(rx_buffer), tx_buffer, sizeof(tx_buffer));
  if (mgt_status != MGT_SUCCESS)
  {
    PRINT_MSG("sdk> error in mgt_cert_initialize: %d\n\r", mgt_status);
    platform_error_handler();
  }

  // At this stage, SCHC Certification app is ready to be tested

  // Infinite loop
  while (1)
  {
    // Call LmHandler process
    l2a_cert_compliance_process();

    // Process FullSDK
    if (mgt_process_request)
    {
      mgt_process_request = false;
      mgt_status = mgt_process();
      if (mgt_status != MGT_SUCCESS)
      {
        PRINT_MSG("sdk> error in mgt_process: %d\n\r", mgt_status);
      }
    }

    // Process SCHC Certification app
    if (mgt_cert_process_request)
    {
      mgt_cert_process_request = false;
      mgt_status = mgt_cert_process();
      if (mgt_status != MGT_SUCCESS)
      {
        PRINT_MSG("sdk> error in mgt_cert_process: %d\n\r", mgt_status);
      }
    }

    /* If a flag is set at this point, mcu must not enter low power and must
     * loop */
    BEGIN_CRITICAL_SECTION();
    /* if an interrupt has occurred after DISABLE_IRQ, it is kept pending and
     * cortex will not enter low power anyway  */
    if (!mgt_process_request || !mgt_cert_process_request)
    {
#ifndef LOW_POWER_DISABLE
      platform_enter_low_power_ll();
#endif
    }
    END_CRITICAL_SECTION();
  }
}

#ifdef TRACE_ENABLED
/**
 * For traces. Called for each trace message.
 */
void tr_handle_trace(const uint8_t *trace, uint8_t length)
{
  static const char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                             '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
  // For every byte of the binary trace message, we need two bytes in the
  // output
  // string, and a final '\0' byte.
  char out_str[TRACE_MAX_LENGTH * 2 + 1];
  for (uint8_t i = 0; i < length; i++)
  {
    out_str[2 * i] = hex[trace[i] >> 4];
    out_str[2 * i + 1] = hex[trace[i] & 0x0f];
  }
  out_str[length * 2] = '\0';
  PRINT_MSG("TRA>%s\n", out_str);
}
#endif