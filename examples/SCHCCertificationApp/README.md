# SCHC Certification application

## Description

*SCHCCertificationApp* application shows how to initialize the SCHC certification FullSDK submodule.

The application uses version 4.6.0 of semtech stack and support the following board only:

* ST NUCLEO-L476RG
* Semtech SX1276MB1MAS LoRa shield
* Semtech SX1272MB2XAS LoRa shield

To change the default Nucleo Lora shield from SX1276 to SX1272, you need to set `-DNUCLEO_LORA_SHIELD=SX1272` environment variable. This is supported only for STM32L476RG-Nucleo board for Semtech L2 stack.

Dynamic template must be used for this application. Synchronization service is not needed and won't be used.

## Prerequisite

Be sure to disable random DevNonce on L2 stack. `-DUSE_RANDOM_DEV_NONCE` must be set to 0 in LoRaMacCrypto.h.

## How to test

Compression rules description is defined on a private document.

Here is the used compression rules used for SCHC Certification :
-----------
RuleId 22 (0x16) for non-matching ruleId.
-----------
RuleId 100 (0x64) for IPv6/UDP compression.
FID	            FL	FP	DI	    TV	            MO	            CDA	           Sent (bits)
IPv6 version	4	1	Bi		6               Equal	        Not-sent
IPv6 Diffserv	8	1	Bi		0               Ignore	        Not-sent
IPv6 Flow label	20	1	Bi		0               Ignore	        Not-sent
IPv6 length	    16	1	Bi		                Ignore	        Compute-*
IPv6 next hdr	8	1	Bi		17              Equal	        Not-sent
IPv6 Hop limit	8	1	Up		40              Ignore	        Not-sent
IPv6 DevPrefix	64	1	Bi		fe80::/64
                                aaaa::/64
                                bbbb::/64       Match-mapping	Mapping sent	2
IPv6 DevIID	    64	1	Bi		                Ignore 	        DevIID
IPv6 AppPrefix	64	1	Bi		aaaa::/64
                                bbbb::/64
                                cccc::/64
                                fe80::/64       Match-mapping	Mapping sent	2
IPv6 AppIID	    64	1	Bi		::1             Equal	        Not sent
UDP DevPort	    16	1	Bi		0x5C40          MSB(12)	        LSB (4)	        4
UDP AppPort	    16	1	Bi		0x1070          MSB (12)	    LSB (4)	        4
UDP length	    16	1	Bi		                Ignore	        Compute-*
UDP checksum	16	1	Bi		                Ignore	        Compute-*
-----------
RuleId 101 (0x65) for IPv6/UDP decompression.
FID	            FL	FP	DI	    TV	            MO	            CDA	           Sent (bits)
IPv6 version	4	1	Bi		6               Equal	        Not-sent
IPv6 Diffserv	8	1	Bi		0               Ignore	        Not-sent
IPv6 Flow label	20	1	Bi		0               Ignore	        Not-sent
IPv6 length	    16	1	Bi		                Ignore	        Compute-*
IPv6 next hdr	8	1	Bi		17              Equal	        Not-sent
IPv6 Hop limit	8	1	Dwn		1               Ignore	        Not-sent
IPv6 DevPrefix	64	1	Bi		aaaa::/64
                                fe80::/64       Match-mapping	Mapping sent	1
IPv6 DevIID	    64	1	Bi		                Ignore 	        DevIID
IPv6 AppPrefix	64	1	Bi		aaaa::/64
                                bbbb::/64
                                eeee::/64
                                fe80::/64       Match-mapping	Mapping sent	2
IPv6 AppIID	    64	1	Bi		::1             Equal	        Not sent
UDP DevPort	    16	1	Bi		0x5C40          MSB (12)	    LSB (4)	        4
UDP AppPort	    16	1	Bi		0x2070          MSB (12)	    LSB (4)	        4
UDP length	    16	1	Bi		                Ignore	        Compute-*
UDP checksum	16	1	Bi		                Ignore	        Compute-*
-----------
Template specification:
@host_ip : fe80::DevIID (DevIID to be computed by device)
@remote_ip : fe80::1
host_udp_port : 0x5C40
remote_udp_port : 0x3070 (default value)
-----------

* Validation no-compression, DevIID and UDP checksum computation
From Chirpstack LNS, on device page, send 0x5010700032 on Fport 224, where 0x50 is the SCHC test command, 0x1070 the new UDP application port to use and 0x0032 the new length of payload with each byte set to value 0.

Device must send at first an acknowledgment 0x50 on Fport 224 and next, an uncompressed uplink on the non-matching ruleId.

* Validate compression
From Chirpstack LNS, on device page, send 0x5030700032 on Fport 224, where 0x50 is the SCHC test command, 0x3070 the new UDP application port to use and 0x0032 the new length of payload with each byte set to value 0.

Device must send at first an acknowledgment 0x50 on Fport 224 and next, a compressed uplink on the right compression ruleId.

* Validate decompression
Device is well connected to IpCore. After NBA agent on server side, with a 'netcat' command, send a payload to device. UDP application port to use must be 0x2070 (see decompression rules table). Device must send only a message on Fport 224 with the 0x51 byte and the uncompressed received message by the device.

* Note : fragmentation will be automatically used if the MTU is too small to send/receive entire payload in a single message.

## Build

```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_VERSION=1.0.0 -DEXTENSION_API=template -DTEMPLATE_ID=dynamic -DAPP_NAME=SCHCCertificationApp -DPLATFORM=STM32L476RG-Nucleo -DTOOLCHAIN=gcc-arm-none -DTARGET=m4 -DL2_STACK=semtech -DLORAWAN_DEVEUI=1111111111111111 -DLORAWAN_APPEUI=1111111111111111 -DLORAWAN_APPKEY=11111111111111111111111111111111 -DSCHC_CERTIFICATION_ENABLED=1 && make -C ./build
```
