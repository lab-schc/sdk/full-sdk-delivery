# SDKBenchTestApp

## Description
This application aims to test the DTLS Server.

To do so, the app will be sending packet to the given remote IPv4 address/UDP port.

As of today, the target of the app is the IP-Connector.

When ran, the app will do the following:
* Initiate a full handshake
* Send a given number of packets of a random size
* Handles downlinks sent by the `udp_server.py`

The application will only stop once the given number of packets to be sent will be sent, as well as when the same number of packets is received.

## Build & Run locally
At the root of the repository, run the following command:
```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_NAME=SDKBenchTestApp -DPLATFORM=linux -DTOOLCHAIN=gcc-native -DTARGET=default -DL2_STACK=udp -DEXTENSION_API=template -DTEMPLATE_ID=ipv6udp -DDTLS_ENABLED=ON -DGENERIC_COMP=ON && make -C ./build
```

To run the app, make sure all the necessary environment variables are set and run the following command:
```sh
./build/linux/SDKBenchTestApp
```

## Build & Run using Docker
At the root of the repository, run the following command:
```
docker build -f ./examples/SDKBenchTestApp/Dockerfile -t 862935980532.dkr.ecr.eu-west-1.amazonaws.com/sdk-benchtester .
```

To run the app, make sure all the necessary environment variables are set and run the following command:
```sh
docker run 862935980532.dkr.ecr.eu-west-1.amazonaws.com/sdk-benchtester
```

## Server application
The `udp_server.py` script can be used to act as a server application receiving the packets sent by the SDKBenchTestApp.

```
usage: udp_server.py [-h] applicationAddress applicationPort

Server Application for receiving uplinks.

positional arguments:
  applicationAddress  Application IPv6 address
  applicationPort     Application UDP port

options:
  -h, --help          show this help message and exit
```

### docker-compose
To run the app more easily, the following docker-compose configuration can be copy/paste in the `docker-compose.yml` from [ipcore-deployment](https://gitlab.com/acklio/ipcore-deployment):
```
  sdk-benchtester:
    image: $DOCKER_IMAGES_REPO/sdk-benchtester:latest
    restart: "unless-stopped"
    environment:
      - BIND_ADDR=
      - BIND_PORT=
      - REMOTE_ADDR=
      - REMOTE_PORT=
      - PSK_ID=
      - PSK_VALUE=
      - HS_RETRY=
      - HS_TIMEOUT=
      - DEVICE_IP6_PREFIX=
      - DEVICE_IP6_IID=
      - APP_IP6_PREFIX=
      - APP_IP6_IID=
      - DEVICE_PORT=
      - APP_PORT=
      - PACKET_MAX_SIZE=
      - PACKET_MIN_SIZE=
      - NB_PACKETS=
    logging: *default-logging
```

## Environment
The following envrionment variables needs to be set:
* `BIND_ADDR`: l2 layer - IPv4 address on which the app will bind itself
* `BIND_PORT`: l2 layer - UDP port on which the app will bind itself
* `REMOTE_ADDR`: l2 layer - IPv4 address of the IP-Connector
* `REMOTE_PORT`: l2 layer - UDP port of the IP-Connector
* `PSK_ID`: PSK identity to be used during DTLS handshake (example: deadbeef)
* `PSK_VALUE`: PSK value to be used during DTLS handshake (example: 173ec59dbed005b649544f980f4a01a1f6e937ae1662b2a3874878b5d5fcd6f5)
* `HS_RETRY`: Number of times the DTLS layer should retry a handshake if it hasn't received a response from the server (example: 3)
* `HS_TIMEOUT`: Amount of time (in milliseconds) the DTLS layer should wait before attempting a handshake retry (example: 30000)
* `DEVICE_IP6_PREFIX`: Device IPv6 prefix configured on IPCore (example: 0x2001abcd00000000)
* `DEVICE_IP6_IID`: Device IPv6 IID configured on IPCore (example: 0x0000000000000004)
* `APP_IP6_PREFIX`: Application IPv6 prefix configured on IPCore (example: 0x1338000000000000)
* `APP_IP6_IID`: Application IPv6 IID configured on IPCore (example: 0x0000000000000042)
* `DEVICE_PORT`: Device UDP port configured on IPCore (example: 0x16EA)
* `APP_PORT`: Application UDP port configured on IPCore (example: 0x1092)
* `PACKET_MAX_SIZE`: Packets maximum size
* `PACKET_MIN_SIZE`: Packets minimum size
* `NB_PACKETS`: The number of packets to be sent / to be received (0 means no limit)