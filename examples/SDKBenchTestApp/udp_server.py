# udp_server.py
# Copyright 2018-2022 ACKLIO SAS - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Author: Kylian Balan kylian.balan@ackl.io

import argparse
import datetime
import socket

# Size of reception buffer.
REC_BUFFER_SIZE = 4096

parser = argparse.ArgumentParser(description='Server Application for receiving uplinks.')
parser.add_argument('applicationAddress', help='Application IPv6 address')
parser.add_argument('applicationPort', type=int, help='Application UDP port')
args = parser.parse_args()

applicationAddress = args.applicationAddress
applicationPort = args.applicationPort
print('application address: {}'.format(applicationAddress))
print('application port: {}'.format(applicationPort))

s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
s.bind((applicationAddress, applicationPort))

packet_nb_map = {}

packet_nb = 0

while True:
    data_array = s.recvfrom(REC_BUFFER_SIZE)

    data = data_array[0]
    address = data_array[1]
    time_str = '{:%H:%M:%S}'.format(datetime.datetime.now())
    msg_id = int.from_bytes(data[:4], 'little')
    packet_nb += 1
    downlink_nb = 1;

    if address in packet_nb_map:
        packet_nb_map[address] += 1
        downlink_nb = packet_nb_map[address]
    else:
        packet_nb_map[address] = downlink_nb

    downlink_bytes = downlink_nb.to_bytes(4, 'little')
    s.sendto(downlink_bytes, address)

    print('time={} device_ipv6={} device_port={} payload_size={} msg_id={} total_count={}'.format(time_str, address[0], address[1], len(data), msg_id, packet_nb))
