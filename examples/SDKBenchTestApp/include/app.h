/**
 * @file app.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#ifndef APP_H
#define APP_H

#include <stdbool.h>
#include <stdint.h>

#define TPL_PARAMS_NB 6

bool app_init(void);
bool app_tpl_init(void);
bool app_run(void);
bool process_downlink(const uint8_t *buff, uint16_t data_size);

#endif /* APP_H */
