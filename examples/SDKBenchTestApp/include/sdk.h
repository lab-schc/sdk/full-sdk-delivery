/**
 * @file sdk.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#ifndef SDK_H
#define SDK_H

#include <stdbool.h>

#ifndef L2_RISINGHF
#define MAX_PAYLOAD_SIZE 8000
#else
#define MAX_PAYLOAD_SIZE 100
#endif

#define APP_TRANSMISSION_MAX_LENGTH MAX_PAYLOAD_SIZE

#define DTLS_MEMORY_SIZE (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE)

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                                         \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u +              \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u) +                \
      DTLS_MEMORY_SIZE

typedef enum
{
  CONNECTING,
  CONNECTED,
  DISCONNECTED
} sdk_connection_state_t;

sdk_connection_state_t sdk_get_connection_state(void);

bool process_app(void);

bool sdk_init(void);

bool sdk_init_interface(void);

void sdk_set_sockets_ip_port(uint8_t **params);

bool dtls_end(void);

bool sdk_do_send(uint8_t *packet, uint16_t data_size);

bool sdk_get_can_send(void);

#endif /* SDK_H */
