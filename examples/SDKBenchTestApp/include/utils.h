/**
 * @file utils.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

void dump_buf(const uint8_t *buf, uint16_t len);
bool hex_to_bin(uint8_t *bin_buff, const char *hex, uint8_t *size);
uint16_t get_byte_array_from_hex_string(const char *hex_string,
                                        uint8_t **byte_array);

#endif /* UTILS_H */
