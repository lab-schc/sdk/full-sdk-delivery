/**
 * @file dtls.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#ifndef DTLS_H
#define DTLS_H

#include <stdbool.h>

#define PSK_ID_MAX_LEN_BYTES 4
#define PSK_VALUE_MAX_LEN_BYTES 255

#define APP_TRANSMISSION_MAX_LENGTH MAX_PAYLOAD_SIZE

bool dtls_set_params(const char *key_id, const char *key, int handshake_retry,
                     int handshake_timeout);
void dtls_start_handshake(void);
bool is_handshake_done(void);

#endif /* DTLS_H */
