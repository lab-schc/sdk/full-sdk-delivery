/**
 * @file app.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <fcntl.h>
#include <linux/limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "app.h"
#include "dtls.h"
#include "fullsdkextapi.h"
#include "fullsdkl2.h"
#include "platform.h"
#include "sdk.h"
#include "utils.h"

#ifdef L2_RISINGHF
#define VIDPID "0483:5740"
#endif

static int packet_max_size = 0;
static int packet_min_size = 0;
static int random_data_fd = 0;
static int nb_packets = 0;
static int nb_packets_to_send = 0;

static int downlink_fd = 0;
static int downlink_packet_nb = 0;

static bool run = true;

static void sigint_handler(int sig)
{
  if (sig == SIGINT)
  {
    run = false;
  }
}

static void set_siginthandler(void)
{
  struct sigaction sigact = {};

  sigact.sa_handler = &sigint_handler;
  sigact.sa_flags = SA_RESETHAND | SA_RESTART;
  sigaction(SIGINT, &sigact, NULL);
}

bool app_init(void)
{
  // L2 parameters
#ifndef L2_RISINGHF
  const char *host_addr = getenv("BIND_ADDR");
  const char *host_port = getenv("BIND_PORT");
  const char *remote_addr = getenv("REMOTE_ADDR");
  const char *remote_port = getenv("REMOTE_PORT");

  if (!host_addr || !host_port || !remote_addr || !remote_port)
  {
    printf("app>invalid L2 parameters\n");
    printf("app>BIND_ADDR: %s, BIND_PORT: %s, REMOTE_ADDR: %s, "
           "REMOTE_PORT: %s\n",
           host_addr ? "OK" : "ERROR", host_port ? "OK" : "ERROR",
           remote_addr ? "OK" : "ERROR", remote_port ? "OK" : "ERROR");
    return false;
  }

  l2_set_ipv4_host_addr(host_addr);
  l2_set_udp_src_port(host_port);
  l2_set_ipv4_remote_addr(remote_addr);
  l2_set_udp_dest_port(remote_port);
#else
  l2_set_vidpid((uint8_t*)VIDPID);
#endif

  // DTLS parameters
  const char *psk_id = getenv("PSK_ID");
  const char *psk_value = getenv("PSK_VALUE");
  int handshake_retry = atoi(getenv("HS_RETRY"));
  int handshake_timeout = atoi(getenv("HS_TIMEOUT"));

  if (!psk_id || !psk_value || !handshake_retry || !handshake_timeout ||
      strlen(psk_id) > PSK_ID_MAX_LEN_BYTES * 2 ||
      strlen(psk_value) > PSK_VALUE_MAX_LEN_BYTES * 2)
  {
    printf("app>invalid DTLS parameters\n");
    printf(
        "app>psk_id: %s, psk_value: %s, handshake_retry: %d, "
        "handshake_timeout: %d\n",
        psk_id && strlen(psk_id) <= PSK_ID_MAX_LEN_BYTES * 2 ? "OK" : "ERROR",
        psk_value && strlen(psk_value) <= PSK_VALUE_MAX_LEN_BYTES * 2 ? "OK"
                                                                      : "ERROR",
        handshake_retry, handshake_timeout);
    return false;
  }

  if (!dtls_set_params(psk_id, psk_value, handshake_retry, handshake_timeout))
  {
    return false;
  }

  // App parameters
  const char *packet_max_size_str = getenv("PACKET_MAX_SIZE");
  const char *packet_min_size_str = getenv("PACKET_MIN_SIZE");
  const char *nb_packets_str = getenv("NB_PACKETS");
  if (!packet_max_size_str || !packet_min_size_str || !nb_packets_str)
  {
    printf("app>invalid app parameters\n");
    printf("app>packet_max_size: %s, packet_min_size: %s, nb_packets: %s\n",
           packet_max_size_str ? "OK" : "ERROR",
           packet_min_size_str ? "OK" : "ERROR",
           nb_packets_str ? "OK" : "ERROR");
    return false;
  }

  nb_packets_to_send = atoi(nb_packets_str);
  packet_max_size = atoi(packet_max_size_str);
  packet_min_size = atoi(packet_min_size_str);
  if (packet_max_size == 0)
  {
    printf("app>invalid packet_max_size (cannot be 0)\n");
    return false;
  }
  if (packet_min_size > packet_max_size)
  {
    printf("app>invalid packet_min_size (max value: %d)\n", packet_max_size);
    return false;
  }

  random_data_fd = open("/dev/urandom", O_RDONLY);
  if (random_data_fd == -1)
  {
    printf("app>cannot open /dev/urandom\n");
    return false;
  }

  const char *filename = getenv("DEVICE_IP6_IID");
  char filepath[PATH_MAX] = "/tmp/";

  strcat(filepath, filename);
  strcat(filepath, ".txt");
  downlink_fd = open(filepath, O_WRONLY | O_CREAT, 0644);
  if (downlink_fd == -1)
  {
    printf("app>cannot open %s\n", filepath);
    return false;
  }

  return true;
}

bool app_tpl_init(void)
{
  const char *tpl_params[TPL_PARAMS_NB] = {0};

  tpl_params[0] = getenv("DEVICE_IP6_PREFIX");
  tpl_params[1] = getenv("DEVICE_IP6_IID");
  tpl_params[2] = getenv("APP_IP6_PREFIX");
  tpl_params[3] = getenv("APP_IP6_IID");
  tpl_params[4] = getenv("DEVICE_PORT");
  tpl_params[5] = getenv("APP_PORT");

  if (!tpl_params[0] || strlen(tpl_params[0]) != 18 || !tpl_params[1] ||
      strlen(tpl_params[1]) != 18 || !tpl_params[2] ||
      strlen(tpl_params[2]) != 18 || !tpl_params[3] ||
      strlen(tpl_params[3]) != 18 || !tpl_params[4] ||
      strlen(tpl_params[4]) != 6 || !tpl_params[5] ||
      strlen(tpl_params[5]) != 6)
  {
    printf("app>invalid template parameters\n");
    printf("app>DEVICE_IP6_PREFIX: %s, DEVICE_IP6_IID: %s, APP_IP6_PREFIX: %s, "
           "APP_IP6_IID: %s, DEVICE_PORT: %s, APP_PORT:%s\n",
           (tpl_params[0] && strlen(tpl_params[0]) == 18) ? "OK" : "ERROR",
           (tpl_params[1] && strlen(tpl_params[1]) == 18) ? "OK" : "ERROR",
           (tpl_params[2] && strlen(tpl_params[2]) == 18) ? "OK" : "ERROR",
           (tpl_params[3] && strlen(tpl_params[3]) == 18) ? "OK" : "ERROR",
           (tpl_params[4] && strlen(tpl_params[4]) == 6) ? "OK" : "ERROR",
           (tpl_params[5] && strlen(tpl_params[5]) == 6) ? "OK" : "ERROR");
    return false;
  }

  uint8_t *byte_tpl_params[TPL_PARAMS_NB] = {0};

  for (uint8_t i = 0; i < TPL_PARAMS_NB; i++)
  {
    const char *value = tpl_params[i];

    if (!value)
    {
      printf("app>invalid template parameter at index %d\n", i);
      return false;
    }

    uint8_t *byte_array = NULL;
    uint16_t array_len = get_byte_array_from_hex_string(value, &byte_array);

    if (!byte_array)
    {
      printf("app>invalid template parameter %s\n", value);
      return false;
    }

    byte_tpl_params[i] = byte_array;

    printf("app>loading parameter (index: %d, value: %s)\n", i, value);

    mgt_status_t status = mgt_set_template_param(i, byte_array, array_len);

    if (status != MGT_SUCCESS)
    {
      printf("app>mgt_set_template_param() failed, status: %d\n", status);
      return false;
    }
  }

  sdk_set_sockets_ip_port(byte_tpl_params);

  for (uint8_t i = 0; i < TPL_PARAMS_NB; i++)
  {
    free(byte_tpl_params[i]);
  }

  return true;
}

static bool process_uplink(void)
{
  uint8_t *packet = malloc(sizeof(uint8_t) * (packet_max_size + sizeof(int)));
  size_t additional_size =
      packet_min_size == packet_max_size
          ? 0
          : random() % (packet_max_size - packet_min_size) + 1;
  size_t packet_size = packet_min_size + additional_size;

  if (!sdk_get_can_send())
  {
    return true;
  }

  nb_packets++;

  memcpy(packet, &nb_packets, sizeof(int));

  if (read(random_data_fd, &packet[sizeof(int)], packet_size) == -1)
  {
    return false;
  }

  printf("packet with id=%d sent\n", nb_packets);

  return sdk_do_send(packet, packet_size);
}

bool process_downlink(const uint8_t *buff, uint16_t data_size)
{
  printf("app>received data(%d)\n", data_size);

  if (data_size < 4)
  {
    printf("app>");
    dump_buf(buff, data_size);
    return true;
  }

  downlink_packet_nb =
      buff[0] + (buff[1] << 8) + (buff[2] << 16) + (buff[3] << 24);
  printf("app>packet_nb: %d\n", downlink_packet_nb);
  dprintf(downlink_fd, "received packet with packet_nb=%d\n",
          downlink_packet_nb);
  return true;
}

bool app_run(void)
{
  set_siginthandler();

  while (run)
  {
    if (nb_packets_to_send && nb_packets == nb_packets_to_send &&
        downlink_packet_nb == nb_packets_to_send && sdk_get_can_send())
    {
      break;
    }
    if (nb_packets < nb_packets_to_send && !process_uplink())
    {
      break;
    }
    if (!process_app())
    {
      break;
    }

    platform_enter_low_power_ll();
  }

  printf("sent/received %d packets over %d packets\n", nb_packets,
         nb_packets_to_send);
  close(random_data_fd);
  close(downlink_fd);
  return dtls_end();
}
