/**
 * @file sdk.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <stdio.h>
#include <string.h>

#include "app.h"
#include "dtls.h"
#include "fullsdkdtg.h"
#include "fullsdkmgt.h"
#include "platform.h"
#include "sdk.h"

static bool mgt_process_request = false;

static uint8_t app_transmission_data[APP_TRANSMISSION_MAX_LENGTH];

static dtg_socket_t *socket;
static dtg_sockaddr_t socket_ipv6_host_addr = {0};
static dtg_sockaddr_t socket_ipv6_remote_addr = {0};

static bool transmission_result = true;
static dtg_status_t transmission_status = DTG_SUCCESS;
static uint16_t transmission_error = 0;

static bool data_received = false;
static dtg_status_t data_rx_status = DTG_SUCCESS;
static uint16_t data_rx_size = 0;

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE] = {0};

static TimerEvent_t sdk_timers[9];

static void cb_mgt_processing_required(void)
{
  mgt_process_request = true;
}

static sdk_connection_state_t conn_state = CONNECTING;

static void cb_mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS && conn_state != CONNECTED)
  {
    printf("Info: l2 connection established\n");
    conn_state = CONNECTED;
    // We start a handshake after l2 connection is established
    dtls_start_handshake();
  }
  else if (state != MGT_SUCCESS && conn_state == CONNECTED)
  {
    fprintf(stderr, "Error: l2 connection has been lost\n");
    conn_state = DISCONNECTED;
  }
}

sdk_connection_state_t sdk_get_connection_state(void)
{
  return conn_state;
}

static void cb_start_timer(uint8_t id, uint32_t duration)
{
  TimerSetValue(&sdk_timers[id], duration);
  TimerStart(&sdk_timers[id]);
}

static void cb_stop_timer(uint8_t id)
{
  TimerStop(&sdk_timers[id]);
}

static void sdk_timer_1_event(void *context)
{
  (void)context;
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  (void)context;
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  (void)context;
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  (void)context;
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  (void)context;
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  (void)context;
  mgt_timer_timeout(5);
}

static void sdk_timer_7_event(void *context)
{
  (void)context;
  mgt_timer_timeout(6);
}

static void sdk_timer_8_event(void *context)
{
  (void)context;
  mgt_timer_timeout(7);
}

static void sdk_timer_9_event(void *context)
{
  (void)context;
  mgt_timer_timeout(8);
}

static mgt_callbacks_t mgt_callbacks = {
    cb_mgt_processing_required,
    cb_mgt_connectivity_state,
    cb_start_timer,
    cb_stop_timer,
    NULL,
};

static void sdk_transmission_result(const dtg_socket_t *socket,
                                    dtg_status_t status, uint16_t error)
{
  (void)socket;
  transmission_status = status;
  transmission_error = error;
  transmission_result = true;
}

static void sdk_data_received(const dtg_socket_t *socket, uint8_t *p_buffer,
                              uint16_t data_size,
                              const dtg_sockaddr_t *p_remote,
                              dtg_status_t status)
{
  (void)socket;
  (void)p_remote;
  data_rx_size = data_size;
  data_rx_status = status;
  data_received = true;

  if (status != DTG_SUCCESS)
  {
    return;
  }

  process_downlink(p_buffer, data_size);
}

// dtg callbacks
static dtg_callbacks_t dtg_callbacks = {sdk_transmission_result,
                                        sdk_data_received};

bool sdk_init_interface(void)
{
  dtg_status_t status = dtg_initialize(&dtg_callbacks);
  if (status != DTG_SUCCESS)
  {
    fprintf(stderr, "app>Error: dtg_initialize returned: %d\n", status);
    return false;
  }

  // Create the socket
  socket = dtg_socket(app_transmission_data, APP_TRANSMISSION_MAX_LENGTH);
  if (socket == DTG_INVALID_SOCKET)
  {
    fprintf(stderr, "app>Error: dtg_socket returned: %d\n", status);
    return false;
  }

  // Bind the socket
  status = dtg_bind(socket, &socket_ipv6_host_addr);
  if (status != DTG_SUCCESS)
  {
    fprintf(stderr, "app>Error: dtg_bind returned: %d\n", status);
    return false;
  }

  printf("app>dtg initialization succeeded\n");
  return true;
}

bool sdk_init(void)
{
  platform_hw_init();
  platform_configure_sleep_mode();
  TimerInit(&sdk_timers[0], sdk_timer_1_event);
  TimerInit(&sdk_timers[1], sdk_timer_2_event);
  TimerInit(&sdk_timers[2], sdk_timer_3_event);
  TimerInit(&sdk_timers[3], sdk_timer_4_event);
  TimerInit(&sdk_timers[4], sdk_timer_5_event);
  TimerInit(&sdk_timers[5], sdk_timer_6_event);
  TimerInit(&sdk_timers[6], sdk_timer_7_event);
  TimerInit(&sdk_timers[7], sdk_timer_8_event);
  TimerInit(&sdk_timers[8], sdk_timer_9_event);

  printf("app>FullSDK version: %s\n", mgt_get_version());

  mgt_status_t mgt_status =
      mgt_initialize(&mgt_callbacks, mgt_mem_block, MEM_BLOCK_SIZE,
                     MAX_MTU_SIZE, MAX_PAYLOAD_SIZE);

  if (mgt_status != MGT_SUCCESS)
  {
    printf("app>mgt_initialize() failed: %d\n", mgt_status);
    return false;
  }

  if (!app_tpl_init())
  {
    return false;
  }

  if (!sdk_init_interface())
  {
    return false;
  }
  return true;
}

void sdk_set_sockets_ip_port(uint8_t **params)
{
  memcpy(socket_ipv6_host_addr.addr, params[0], 8);
  memcpy(&socket_ipv6_host_addr.addr[8], params[1], 8);
  memcpy(socket_ipv6_remote_addr.addr, params[2], 8);
  memcpy(&socket_ipv6_remote_addr.addr[8], params[3], 8);
  memcpy(socket_ipv6_host_addr.port, params[4], 2);
  memcpy(socket_ipv6_remote_addr.port, params[5], 2);
}

bool sdk_get_can_send(void)
{
  return is_handshake_done() && transmission_result &&
         sdk_get_connection_state() == CONNECTED;
}

bool sdk_do_send(uint8_t *packet, uint16_t data_size)
{
  dtg_status_t status =
      dtg_sendto(socket, packet, data_size, &socket_ipv6_remote_addr);

  if (status != DTG_SUCCESS)
  {
    printf("tun>dtg_sendto() failed %d\n", status);
    return false;
  }
  transmission_result = false;
  return true;
}

bool process_app(void)
{
  if (mgt_process_request)
  {
    mgt_process_request = false;
    mgt_status_t st;
    if ((st = mgt_process()) != MGT_SUCCESS)
    {
      printf("app>mgt_process() failed: %d\n", st);
      return false;
    }
  }
  return true;
}
