/**
 * @file utils.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

void dump_buf(const uint8_t *buf, uint16_t len)
{
  for (size_t i = 0; i < len; i++)
  {
    printf("%.2X ", buf[i]);
  }
  printf("\n");
}

bool hex_to_bin(uint8_t *bin_buff, const char *hex, uint8_t *size)
{
  size_t max_size = *size;
  size_t str_len;

  str_len = strlen(hex);
  if (str_len % 2 != 0)
    return false;
  if (str_len / 2 > max_size)
  {
    printf("Error: PSK parameter too big, size: %lu max size: %lu\n",
           str_len / 2, max_size);
    return false;
  }
  *size = str_len / 2;
  for (uint8_t i = 0; i < *size; ++i)
  {
    char str_byte[3] = {};
    char *err = NULL;
    memcpy(str_byte, hex + 2 * i, 2);
    bin_buff[i] = strtol(str_byte, &err, 16);
    if (*err)
    {
      printf("Error: strtol() failed: %s\n", strerror(errno));
      return false;
    }
  }
  return true;
}

static bool is_valid_hex_string(const char *str, uint32_t len)
{
  if (len < 4 || strncmp(str, "0x", 2))
  {
    return false;
  }

  if (len % 2 != 0)
  {
    return false;
  }

  for (uint32_t i = 2; i < len; i++)
  {
    if (!isxdigit(str[i]))
    {
      return false;
    }
  }
  return true;
}

uint16_t get_byte_array_from_hex_string(const char *hex_string,
                                        uint8_t **byte_array)
{
  uint16_t len = (uint16_t)strlen(hex_string);

  if (!is_valid_hex_string(hex_string, len))
  {
    *byte_array = NULL;
    return 0;
  }

  uint16_t array_len = (len - 2) / 2;
  *byte_array = malloc(sizeof(uint8_t) * array_len);

  if (!*byte_array)
  {
    return 0;
  }

  memset(*byte_array, 0, array_len);

  // Skipping "0x"
  hex_string += 2;

  for (uint16_t idx = 0; idx < array_len; idx++)
  {
    sscanf(hex_string, "%2hhx", &((*byte_array)[idx]));
    hex_string += 2;
  }
  return array_len;
}