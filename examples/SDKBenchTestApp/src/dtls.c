/**
 * @file dtls.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "app.h"
#include "dtls.h"
#include "fullsdkdtg.h"
#include "fullsdkdtls.h"
#include "platform.h"
#include "sdk.h"
#include "utils.h"

static bool handshake_done = false;

static uint8_t psk_id[PSK_ID_MAX_LEN_BYTES] = {0};
static uint8_t psk_value[PSK_VALUE_MAX_LEN_BYTES] = {0};

static dtls_options_t dtls_opt = {
    NULL, NULL, PSK_VALUE_MAX_LEN_BYTES, PSK_ID_MAX_LEN_BYTES, 3, 30000};

static void dtls_handshake_result(dtls_status_t status)
{
  if (status != DTLS_SUCCESS)
  {
    fprintf(stderr, "Error: Handshake failed, status: %d\n", status);
    return;
  }
  printf("Info: Handshake done\n");
  handshake_done = true;
}

static dtls_callbacks_t dtls_cb = {dtls_handshake_result,
                                   platform_entropy_hardware_poll};

bool dtls_set_params(const char *key_id, const char *key_value,
                     int handshake_retry, int handshake_timeout)
{
  if (!hex_to_bin(psk_id, key_id, &dtls_opt.psk_id_size))
  {
    fprintf(stderr, "Error: parse_opt() error: Invalid PSK ID\n");
    return false;
  }
  if (!hex_to_bin(psk_value, key_value, &dtls_opt.psk_size))
  {
    fprintf(stderr, "Error: parse_opt() error: Invalid PSK\n");
    return false;
  }

  dtls_opt.psk_id = psk_id;
  dtls_opt.psk = psk_value;
  dtls_opt.nbr_try = handshake_retry;
  dtls_opt.handshake_timeout = handshake_timeout;
  return dtls_set_options(&dtls_cb, &dtls_opt) == DTLS_SUCCESS;
}

void dtls_start_handshake(void)
{
  printf("Info: starting handshake\n");
  dtls_handshake();
}

bool is_handshake_done(void)
{
  return handshake_done;
}

bool dtls_end(void)
{
  return dtls_close() == DTLS_SUCCESS;
}