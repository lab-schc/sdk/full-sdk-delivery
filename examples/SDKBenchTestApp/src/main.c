/**
 * @file main.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Kylian Balan kylian.balan@ackl.io
 */

#include <stdlib.h>

#include "app.h"
#include "sdk.h"

int main(void)
{
  if (!app_init())
  {
    return EXIT_FAILURE;
  }

  if (!sdk_init())
  {
    return EXIT_FAILURE;
  }

  if (!app_run())
  {
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}