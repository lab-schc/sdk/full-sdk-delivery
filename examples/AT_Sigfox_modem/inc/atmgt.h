/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Flavien Moullec flavien@ackl.io
 *
 * @brief AT SCHC commands for Management layer
 */

#include <stdbool.h>

#include "fullsdkmgt.h"

bool fullsdk_is_busy(void);

mgt_status_t fullsdk_mgt_init(void);

void fullsdk_process(void);
