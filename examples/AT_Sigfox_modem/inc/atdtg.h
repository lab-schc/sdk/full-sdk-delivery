/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Flavien Moullec flavien@ackl.io
 *
 * @brief AT SCHC commands for Datagram layer
 */

#ifndef AT_DTG_H
#define AT_DTG_H

#include "fullsdkdtg.h"

dtg_status_t fullsdk_dtg_init(void);

dtg_status_t fullsdk_send_data(const uint8_t *data, uint16_t data_size);

#endif /* AT_DTG_H */
