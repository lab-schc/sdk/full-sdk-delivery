/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis - thibaut.artis@ackl.io
 */

#ifndef __AT_H
#define __AT_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdint.h>

  /**
 * @brief AT command status codes
 */
  typedef enum
  {
    AT_OK = 0,
    AT_ERROR,
    AT_PARAM_ERROR,
    AT_BUSY_ERROR,
    AT_TEST_PARAM_OVERFLOW,
    AT_LIB_ERROR,
    AT_RX_TIMEOUT,
    AT_RX_ERROR,
    AT_L2_ERROR,
    AT_NOT_IMPLEMENTED,
    AT_PLATFORM_ERROR,
    AT_SCHC_MGT_ERROR,
    AT_SCHC_DTG_ERROR,
    AT_SCHC_NET_ERROR,
    AT_MAX,
  } at_status_t;

  extern bool init_flag;

  /**
 * @brief  Structure defining an AT Command
 */
  typedef struct
  {
    const char *string; /*< command string, after the "AT" */
    const int
        size_string; /*< size of the command string, not including the final \0 */
    at_status_t (*get)(
        const char *param,
        uint8_t *error);                                   /*< ? after the string to get the current value*/
    at_status_t (*set)(const char *param, uint8_t *error); /*< = (but not =?\0)
                                            after the string to set a value */
    at_status_t (*run)(
        const char *param,
        uint8_t *error); /*< \0 after the string - run the command */
#if !defined(NO_HELP)
    const char *help_string; /*< to be printed when AT? */
    const char *help_param;  /*< to be printed when =? after the string */
#endif
  } at_command_t;

/* Exported constants --------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/

/* AT printf */
#define AT_PRINTF(...) vcom_Send(__VA_ARGS__)

/* AT Command strings. Commands start with AT */
#define AT_RESET "Z"
#define AT_RFS "$RFS"
#define AT_VER "+VER"
#define AT_ID "$ID"
#define AT_PAC "$PAC"
#define AT_S410 "S410"
#define AT_SENDB "$SB"
#define AT_SENDF "$SF"
#define AT_TM "$TM"
#define AT_CW "$CW"
#define AT_PN "$PN"
#define AT_BAT "+BAT"
#define AT_S302 "S302"
#define AT_S300 "S300"
#define AT_S400 "S400"
#define AT_RC "$RC"
#define AT_RSSICAL "$RSSICAL"
#define ATE "E"

#define AT_INIT "+SCHCINIT"
#define AT_FSDKVER "+SCHCVERSION"
#define AT_EXTSEND "+SCHCSEND"
#define AT_EXTSENDACK "+SCHCSENDACK"
#define AT_ENABLE_DL "+EDL"
#define AT_REPEAT_MODE "+RPMODE"
#define AT_TRACE "+TRACE"

/* Exported functions ------------------------------------------------------- */

  /**
 * @brief  Return AT_ERROR in all cases
 * @param  Param string of the AT command - unused
 * @retval AT_ERROR
 */
  at_status_t at_return_error(const char *param, uint8_t *error);

  /**
 * @brief  Trig a reset of the MCU
 * @param  Param string of the AT command - unused
 * @retval AT_OK
 */
  at_status_t at_reset(const char *param, uint8_t *error);

  /**
 * @brief  Restore factory settings in Eeprom
 * @param  Param string of the AT command - unused
 * @retval AT_OK
 */
  at_status_t AT_restore_factory_settings(const char *param, uint8_t *error);

  /**
 * @brief  Send Bit w/wo ack to Sigfox Nw
 * @param  String pointing to provided ADR setting
 * @retval AT_OK if OK, or an appropriate AT_xxx error code
 */
  at_status_t at_send_bit(const char *param, uint8_t *error);

  /**
 * @brief  Send  frame w/wo ack to Sigfox Nw
 * @param  String pointing to provided ADR setting
 * @retval AT_OK if OK, or an appropriate AT_xxx error code
 */
  at_status_t at_send_frame(const char *param, uint8_t *error);

  /**
 * @brief  Get the DevId
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_dev_id_get(const char *param, uint8_t *error);

  /**
 * @brief  Get the Dev Pac
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_dev_pac_get(const char *param, uint8_t *error);

  /**
 * @brief  Set public Key
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_public_key_set(const char *param, uint8_t *error);

  /**ADSDK
 * @brief  Get public Key
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_public_key_get(const char *param, uint8_t *error);
  /**
 * @brief  Print the version of the AT_Slave FW
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_version_get(const char *param, uint8_t *error);

  /**
 * @brief  Print the batery level
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_bat_get(const char *param, uint8_t *error);

  /**
 * @brief  Test Tone
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_test_cw(const char *param, uint8_t *error);

  /**
 * @brief  Tx Test with prbs9 modulation
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_test_pn(const char *param, uint8_t *error);

  /**
 * @brief  Test Tone
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_test_mode(const char *param, uint8_t *error);

  /**
 * @brief  set the output power of the radio (power in dBm)
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_power_set(const char *param, uint8_t *error);

  /**
 * @brief  get the output power of the radio (power in dBm)
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_power_get(const char *param, uint8_t *error);
  /**
 * @brief  send an out of band message one
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_out_of_band_run(const char *param, uint8_t *error);

  /**
 * @brief  to configure the enabled channels for FCC
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_channel_config_fcc_set(const char *param, uint8_t *error);

  /**
 * @brief  set zones (1 2 3 or 4)
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_rc_set(const char *param, uint8_t *error);

  /**
 * @brief  get zones (1 2 3 or 4)
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_rc_get(const char *param, uint8_t *error);

  /**
 * @brief  to get the rssi calibration value from eeprom.
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_rssi_cal_get(const char *param, uint8_t *error);

  /**
 * @brief  to set the rssi calibration state in eeprom.
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_rssi_cal_set(const char *param, uint8_t *error);
  /**
 * @brief  to get the current echo state from eeprom.
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_echo_get(const char *param, uint8_t *error);

  /**
 * @brief  to set the echo state in eeprom.
 * @param  String parameter
 * @retval AT_OK
 */
  at_status_t at_echo_set(const char *param, uint8_t *error);

  /**
 * @brief initializes the Acklio SDK
 * @param None
 * @retval AT_OK
 */
  at_status_t at_fullsdk_init(const char *param, uint8_t *error);

  /**
 * @brief Prints the version of the fullsdk running
 * @param Param represented in ASCII format
 * @retval AT_OK
 */
  at_status_t at_fullsdk_version_get(const char *param, uint8_t *error);

  /**
 * @brief to send messages to the sigfox network via Acklio SDK
 * @param String parameter
 * @retval AT_OK
 */
  at_status_t at_fullsdk_send(const char *param, uint8_t *error);

  /**
 * @brief to set the value of the initiate_downlink flag
 * @param 0 or 1
 * @retval AT_OK
 */
  at_status_t at_fullsdk_enable_downlink(const char *param, uint8_t *error);

  /**
 * @brief to set the value of the tx_repeat flag
 * @param 1 or 2 or 3
 * @retval AT_OK
 */
  at_status_t at_fullsdk_set_tx_repeat_mode(const char *param, uint8_t *error);

#ifdef PLATFORM_TRACE_ENABLED
  /**
 * @brief Sets whether platform traces should be enabled or not
 * @param bool parameter
 * @retval AT_OK
 */
  at_status_t at_set_platform_traces(const char *param, uint8_t *error);
#endif

#ifdef __cplusplus
}
#endif

#endif /* __AT_H */
