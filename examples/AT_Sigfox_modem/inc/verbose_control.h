/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis - thibaut.artis@ackl.io
 */

#ifndef VERBOSE_CONTROL_H_
#define VERBOSE_CONTROL_H_

#ifndef VERBOSE_LEVEL
#define VERBOSE_LEVEL 2
#endif

#define VERBOSE_LEVEL_1 1
#define VERBOSE_LEVEL_2 2

#define TVL1(X)                                                                \
  do                                                                           \
  {                                                                            \
    if (VERBOSE_LEVEL >= VERBOSE_LEVEL_1)                                      \
    {                                                                          \
      X                                                                        \
    }                                                                          \
  } while (0);
#define TVL2(X)                                                                \
  do                                                                           \
  {                                                                            \
    if (VERBOSE_LEVEL >= VERBOSE_LEVEL_2)                                      \
    {                                                                          \
      X                                                                        \
    }                                                                          \
  } while (0);

#endif /* VERBOSE_CONTROL_H_ */