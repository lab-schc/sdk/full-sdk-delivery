# AT_Sigfox_modem

## Overview

AT_Sigfox_modem application allow to send packets by using the Datagram API of the FullSDK library.
This example used a default IPv6/UDP SCHC template.

This application is able to send up to 300 bytes packets over the serial, which will be fragmented using SCHC NoACK fragmentation mode.

## Build the application

```sh
cmake -S . -B ./build/ -DFULLSDK_VERSION=dev -DAPP_NAME=AT_Sigfox_modem -DTOOLCHAIN=gcc-arm-none -DTARGET=m0plus -DPLATFORM=B-L072Z-LRWAN1 -DL2_STACK=sigfox -DEXTENSION_API=template -DTEMPLATE_ID=dynamic -DPLATFORM_TRACE_ENABLED=ON && make -C ./build
```

## Usage

### Initialize the FullSDK

```sh
AT+SCHCINIT
OK
```

### Activate platform traces

Note: `-DPLATFORM_TRACE_ENABLED=ON` must be set at compilation time

```sh
AT+TRACE=1
OK
```

### Send an uplink packet

```sh
AT+SCHCSEND=<ascii_data>
```

Example:

```sh
AT+SCHCSEND=4F7569636865204C6F727261696E65
```
