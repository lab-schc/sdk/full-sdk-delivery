/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis - thibaut.artis@ackl.io
 */

#include <stdint.h>
#include <stdlib.h>

#include "at.h"
#include "command.h"
#include "hw.h"

/* comment the following to have help message */
/* #define NO_HELP */
/* #define NO_KEY_ADDR_EUI */

// We support a payload of MAX_PAYLOAD_SIZE bytes in binary, so
// **MAX_PAYLOAD_SIZE bytes in ASCII. Moreover there is at least 62 bytes of
// AT-command overhead. and the rest is the safety margin.
#define CMD_SIZE (2 * MAX_PAYLOAD_SIZE + 128)

/**
 * @brief  Array corresponding to the description of each possible AT Error
 */
static const char *const at_status_description[] = {
    "OK\n",                     /* AT_OK */
    "AT_ERROR\n",               /* AT_ERROR */
    "AT_PARAM_ERROR\n",         /* AT_PARAM_ERROR */
    "AT_BUSY_ERROR\n",          /* AT_BUSY_ERROR */
    "AT_TEST_PARAM_OVERFLOW\n", /* AT_TEST_PARAM_OVERFLOW */
    "AT_LIB_ERROR\n",           /* AT_LIB_ERROR */
    "AT_RX_TIMEOUT\n",          /* AT_RX_TIMEOUT */
    "AT_RX_ERROR\n",            /* AT_RX_ERROR */
    "AT_L2_ERROR\n",            /* AT_L2_ERROR */
    "AT_NOT_IMPLEMENTED\n",     /* AT_NOT_IMPLEMENTED */
    "AT_PLATFORM_ERROR\n",      /* AT_PLATFORM_ERROR */
    "AT_ERROR,1",               /* AT MGT error */
    "AT_ERROR,2",               /* AT DTG error */
    "AT_ERROR,3",               /* AT NET error */
    "error unknown\n",          /* AT_MAX */
};

static void com_error(at_status_t status, uint8_t error)
{
  if (status > AT_MAX)
  {
    status = AT_MAX;
  }

  if (status < AT_SCHC_MGT_ERROR || status == AT_MAX)
  {
    // No secondary error code
    AT_PRINTF(at_status_description[status]);
    return;
  }

  AT_PRINTF("%s,%u\n", at_status_description[status], error);
}

/**
 * @brief  Array of all supported AT Commands
 */
static const at_command_t at_command[] = {
    {
        .string = AT_RESET,
        .size_string = sizeof(AT_RESET) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_RESET ": Trig a MCU reset\r\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_reset,
    },

    {
        .string = AT_RFS,
        .size_string = sizeof(AT_RFS) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_RFS ": Restore EEPROM Factory Settings\r\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = AT_restore_factory_settings,
    },

    {
        .string = AT_VER,
        .size_string = sizeof(AT_VER) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_VER ": Get the FW version\r\n",
        .help_param = "",
#endif
        .get = at_version_get,
        .set = at_return_error,
        .run = at_version_get,
    },

    {
        .string = AT_ID,
        .size_string = sizeof(AT_ID) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_ID ": Get the ID\r\n",
        .help_param = "",
#endif
        .get = at_dev_id_get,
        .set = at_return_error,
        .run = at_dev_id_get,
    },

    {
        .string = AT_PAC,
        .size_string = sizeof(AT_PAC) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_PAC ": Get the PAC\r\n",
        .help_param = "",
#endif
        .get = at_dev_pac_get,
        .set = at_return_error,
        .run = at_dev_pac_get,
    },

    {
        .string = AT_S410,
        .size_string = sizeof(AT_S410) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_S410 ": 0:Private Key 1:Public Key\r\n",
        .help_param = "[0 ..1]",
#endif
        .get = at_public_key_get,
        .set = at_public_key_set,
        .run = at_return_error,
    },

    {
        .string = AT_SENDB,
        .size_string = sizeof(AT_SENDB) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SENDB ": Send a bit to the sigfox network\r\n",
        .help_param = "AT" AT_SENDB "=<Bit>,<Opt downlink>,<Opt TxRepeat><CR>",
#endif
        .get = at_return_error,
        .set = at_send_bit,
        .run = at_return_error,
    },

    {
        .string = AT_SENDF,
        .size_string = sizeof(AT_SENDF) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_SENDF ": Send a frame to the sigfox network\r\n",
        .help_param =
            "AT" AT_SENDF "=<Payload>,<Opt downlink>,<Opt TxRepeat><CR>",
#endif
        .get = at_return_error,
        .set = at_send_frame,
        .run = at_return_error,
    },

    {
        .string = AT_CW,
        .size_string = sizeof(AT_CW) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_CW ": Run CW Test mode\r\n",
        .help_param = "AT" AT_CW "=<frequency><CR> frequency in Hz or in MHz,",
#endif
        .get = at_return_error,
        .set = at_test_cw,
        .run = at_return_error,
    },

    {
        .string = AT_PN,
        .size_string = sizeof(AT_PN) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_PN ": Run PRBS9 BPBSK Test mode\r\n",
        .help_param = "AT" AT_PN "=<frequency>,<bitrate><CR> frequency in Hz "
                      "or in MHz, bitrate=[100, 600] ",
#endif
        .get = at_return_error,
        .set = at_test_pn,
        .run = at_return_error,
    },

    {
        .string = AT_TM,
        .size_string = sizeof(AT_TM) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_TM ": Run Sigfox Test mode\r\n",
        .help_param = "AT" AT_TM "=<rc>,<mode><CR> ",
#endif
        .get = at_return_error,
        .set = at_test_mode,
        .run = at_return_error,
    },

    {
        .string = AT_BAT,
        .size_string = sizeof(AT_BAT) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_BAT ": Get the battery Level in mV \r\n",
        .help_param = "",
#endif
        .get = at_bat_get,
        .set = at_return_error,
        .run = at_return_error,
    },

    {
        .string = AT_S302,
        .size_string = sizeof(AT_S302) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_S302 ": set Radio output power in dBm\r\n",
        .help_param = "AT" AT_S302 "=<power>,<CR>  power in dBm=[0..20]",
#endif
        .get = at_power_get,
        .set = at_power_set,
        .run = at_return_error,
    },

    {
        .string = AT_S300,
        .size_string = sizeof(AT_S300) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_S300 ": send an out of band message once.\r\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_out_of_band_run,
    },

    {
        .string = AT_S400,
        .size_string = sizeof(AT_S400) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_S400 ": configure specific variables for standard.\r\n",
        .help_param = "<param1><param2><param3>,<param4><CR>",
#endif
        .get = at_return_error,
        .set = at_channel_config_fcc_set,
        .run = at_return_error,
    },

    {
        .string = AT_RC,
        .size_string = sizeof(AT_RC) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_RC ": to set/get the Regional Config zones.\r\n",
        .help_param = "<param1><CR> param1=[1,2,3,4]",
#endif
        .get = at_rc_get,
        .set = at_rc_set,
        .run = at_return_error,
    },

    {
        .string = AT_RSSICAL,
        .size_string = sizeof(AT_RSSICAL) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_RSSICAL
                       ": to set/get the rssi calibration value in dB.\r\n",
        .help_param = "<param1><CR> param1=rssi value in dB",
#endif
        .get = at_rssi_cal_get,
        .set = at_rssi_cal_set,
        .run = at_return_error,
    },

    {
        .string = ATE,
        .size_string = sizeof(ATE) - 1,
#ifndef NO_HELP
        .help_string = "AT" ATE ": to set/get the echo state.\r\n",
        .help_param = "<param1><CR> param1=[0,1]",
#endif
        .get = at_echo_get,
        .set = at_echo_set,
        .run = at_return_error,
    },

    {
        .string = AT_INIT,
        .size_string = sizeof(AT_INIT) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_INIT " : initialize fullsdk.\r\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_fullsdk_init,
    },

    {
        .string = AT_FSDKVER,
        .size_string = sizeof(AT_FSDKVER) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_FSDKVER ": Prints the fullsdk version\n",
#endif
        .get = at_return_error,
        .set = at_return_error,
        .run = at_fullsdk_version_get,
    },

    {
        .string = AT_EXTSEND,
        .size_string = sizeof(AT_EXTSEND) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_EXTSEND
            " : Sends data to the sigfox network through the acklio SDK.\r\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_fullsdk_send,
        .run = at_return_error,
    },

    {
        .string = AT_ENABLE_DL,
        .size_string = sizeof(AT_ENABLE_DL) - 1,
#ifndef NO_HELP
        .help_string = "AT" AT_ENABLE_DL
                       " : Sets the downlink flag to the value specified.\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_fullsdk_enable_downlink,
        .run = at_return_error,
    },

    {
        .string = AT_REPEAT_MODE,
        .size_string = sizeof(AT_REPEAT_MODE) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_REPEAT_MODE " : Sets the tx_repeat to the value specified "
            "(between 1 and 3 included).\n",
        .help_param = "",
#endif
        .get = at_return_error,
        .set = at_fullsdk_set_tx_repeat_mode,
        .run = at_return_error,
    },

#ifdef PLATFORM_TRACE_ENABLED
    {
        .string = AT_TRACE,
        .size_string = sizeof(AT_TRACE) - 1,
#ifndef NO_HELP
        .help_string =
            "AT" AT_TRACE ": Set whether platform traces are enabled or not\n",
#endif
        .get = at_return_error,
        .set = at_set_platform_traces,
        .run = at_return_error,
    },
#endif
};

/**
 * @brief  Parse a command and process it
 * @param  The command
 * @retval None
 */
static void parse_cmd(const char *cmd);

/* Exported functions
 * ---------------------------------------------------------*/

void CMD_Init(void)
{
  vcom_Init();
  vcom_ReceiveInit();
}

void CMD_Process(void)
{
  static char command[CMD_SIZE];
  static unsigned i = 0;
  unsigned cmd_size = 0;

  /* Process all commands */
  while (IsNewCharReceived() == SET)
  {
    command[i] = GetNewChar();

#if 0 /* echo On    */
    PRINTF("%c", command[i]);
#endif

    if (command[i] == AT_ERROR_RX_CHAR)
    {
      memset(command, '\0', i);
      i = 0;
      com_error(AT_RX_ERROR, 0);
      break;
    }
    else if ((command[i] == '\r') || (command[i] == '\n'))
    {
      if (i != 0)
      {
        command[i] = '\0';
        /* need to put static i=0 to avoid realtime issue when CR+LF is used by
         * hyperterminal */
        cmd_size = i;
        i = 0;
        parse_cmd(command);

        memset(command, '\0', cmd_size);
      }
    }
    else if (i == (CMD_SIZE - 1))
    {
      memset(command, '\0', i);
      i = 0;
      com_error(AT_TEST_PARAM_OVERFLOW, 0);
    }
    else
    {
      i++;
    }
  }
}

static void parse_cmd(const char *cmd)
{
  at_status_t status = AT_OK;
  uint8_t error = 0;
  const at_command_t *cur_at_command;
  int i;

  if ((cmd[0] != 'A') || (cmd[1] != 'T'))
  {
    status = AT_ERROR;
  }
  else if (cmd[2] == '\0')
  {
    /* status = AT_OK; */
  }
  else if (cmd[2] == '?')
  {
#ifdef NO_HELP
#else
    AT_PRINTF("AT?              : Help on <CMD>\r\n");
    AT_PRINTF("AT+<CMD>         : Run the <CMD>\r\n");
    AT_PRINTF("AT+<CMD>?        : Get the value\r\n");
    AT_PRINTF("AT+<CMD>=<value> : Set the value\r\n");
    AT_PRINTF("AT+<CMD>=?       : Get the value range\r\n");
    for (i = 0; i < (sizeof(at_command) / sizeof(at_command_t)); i++)
    {
      AT_PRINTF(at_command[i].help_string);
    }
#endif
  }
  else
  {
    /* point to the start of the command, excluding AT */
    status = AT_ERROR;
    cmd += 2;
    for (i = 0; i < (sizeof(at_command) / sizeof(at_command_t)); i++)
    {
      if (strncmp(cmd, at_command[i].string, at_command[i].size_string) == 0)
      {
        cur_at_command = &(at_command[i]);
        /* point to the string after the command to parse it */
        cmd += cur_at_command->size_string;

        /* parse after the command */
        switch (cmd[0])
        {
        case '\0': /* ATXXX nothing after the command */
          status = cur_at_command->run(cmd, &error);
          break;
        case '=': /* ATXXX=? */
          if ((cmd[1] == '?') && (cmd[2] == '\0'))
          {
            AT_PRINTF(cur_at_command->help_param);
            status = AT_OK;
          }
          else
          { /* ATXXX=input*/
            status = cur_at_command->set(cmd + 1, &error);
          }
          break;
        case '?': /* ATXXX? */
          status = cur_at_command->get(cmd, &error);
          break;
        default:
          /* not recognized */
          break;
        }
        /* we end the loop as the command was found */
        break;
      }
    }
  }

  com_error(status, error);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
