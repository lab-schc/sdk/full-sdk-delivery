/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Thibaut Artis - thibaut.artis@ackl.io
 *
 * Based on ST's AT_modem example. Check license here: www.st.com/SLA0044.
 *
 */

#include "at.h"
#include "atmgt.h"
#include "command.h"
#include "hw.h"
#include "hw_eeprom.h"
#include "low_power_manager.h"
#include "platform.h"
#include "scheduler.h"

int main(void)
{
  /* STM32 HAL library initialization*/
  HAL_Init();
  /* Configure the system clock*/
  SystemClock_Config();

  /* Configure the hardware*/
  HW_Init();
  /* Initialize Eeprom factory settings at device Birth*/
  HW_EEPROM_Init();

  CMD_Init();
  /*Disable standby mode*/
  LPM_SetOffMode(LPM_APPLI_Id, LPM_Disable);

  BSP_LED_Init(LED_BLUE);
  BSP_LED_Init(LED_GREEN);
  BSP_LED_Init(LED_RED2);

  AT_PRINTF("app> ATModem initialized\n");

  SCH_RegTask(VCOM_TASK, CMD_Process);
  /* main loop*/
  while (1)
  {
    SCH_Run();

    fullsdk_process();

    SCH_Idle();
  }
}

void SCH_Idle(void)
{
  BACKUP_PRIMASK();

  DISABLE_IRQ();

#ifndef LOW_POWER_DISABLE
  if (!fullsdk_is_busy())
  {
    LPM_EnterLowPower();
  }
#endif

  ENABLE_IRQ();

  RESTORE_PRIMASK();
}
