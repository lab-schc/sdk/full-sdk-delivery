/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Flavien Moullec flavien@ackl.io
 *
 * @brief AT SCHC commands for Datagram layer
 */

#include "atdtg.h"
#include "at.h"
#include "platform.h"
#include "sigfoxconfiguration.h"
#include "verbose_control.h"

#define APP_PAYLOAD_MAX_LENGTH MAX_PAYLOAD_SIZE

dtg_socket_t *socket = NULL;
static uint8_t socket_id = 0;

// Received/Transmission data buffers.
static uint8_t app_tx_data[APP_PAYLOAD_MAX_LENGTH];

static dtg_sockaddr_t socket_addr = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // addr
    {0, 0}                                            // port
};
static dtg_sockaddr_t dest_addr = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // addr
    {0, 0}                                            // port
};

static void dtg_transmission_result(const dtg_socket_t *socket,
                                    dtg_status_t status, uint16_t error)
{
  if (status == DTG_SUCCESS)
  {
    AT_PRINTF("+SENDOK,%d\n", socket_id);
  }
  else
  {
    AT_PRINTF("+SENDFAIL:%d,%d,%d\n", socket_id, status, error);
  }
}

static void dtg_data_received(const dtg_socket_t *socket, const uint8_t *p_buffer,
                              uint16_t data_size,
                              const dtg_sockaddr_t *p_remote,
                              dtg_status_t status)
{
  if (status == DTG_SUCCESS)
  {
    AT_PRINTF("+RECVOK,%d:", socket_id);
    for (uint16_t i = 0; i < data_size; i++)
    {
      AT_PRINTF("%02X", p_buffer[i]);
    }
    AT_PRINTF("\n");
  }
  else
  {
    AT_PRINTF("+RECVFAIL:%d,%d\n", socket_id, status);
  }
}

dtg_status_t fullsdk_dtg_init(void)
{
  // Initialize DTG interface
  dtg_callbacks_t dtg_callbacks = {dtg_transmission_result, dtg_data_received};

  dtg_status_t status = dtg_initialize(&dtg_callbacks);
  if (status != DTG_SUCCESS)
  {
    return status;
  }

  socket = dtg_socket(app_tx_data, MAX_PAYLOAD_SIZE);

  if (socket == DTG_INVALID_SOCKET)
  {
    return DTG_NO_SOCKET_ERR;
  }
  status = dtg_bind(socket, &socket_addr);
  if (status != DTG_SUCCESS)
  {

    return status;
  }

  init_flag = true;

  return DTG_SUCCESS;
}

dtg_status_t fullsdk_send_data(const uint8_t *data, uint16_t data_size)
{

  if (!init_flag)
  {
    return DTG_ERROR;
  }

  return dtg_sendto(socket, data, data_size, &dest_addr);
}