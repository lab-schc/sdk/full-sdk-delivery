/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Flavien Moullec flavien@ackl.io
 *
 * @brief AT SCHC commands for Management layer
 */

#ifndef AT_MGT_H
#define AT_MGT_H

#include "at.h"
#include "fullsdkextapi.h"
#include "fullsdkmgt.h"
#include "platform.h"
#include "verbose_control.h"

#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned

// Memory block size provided to mgt_initialize
#define MEM_BLOCK_SIZE                                                         \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u +              \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE];

// Buffer size (in bytes) to provision the CBOR template.
#ifndef TPL_MEMORY_SIZE
#define TPL_MEMORY_SIZE 1000
#endif

static uint8_t tpl_memory_area[TPL_MEMORY_SIZE];
static bool mgt_process_request = false;

// Default IPv6/UDP CBOR rule.
const uint8_t cbor_rule[] = {
    0xB1, 0x00, 0x84, 0x18, 0x96, 0x01, 0x86, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x01, 0x81, 0x83, 0x02, 0x18, 0x1C, 0x8E, 0x88, 0x0C, 0x04, 0x01, 0x02,
    0x00, 0x00, 0x81, 0x41, 0x06, 0x02, 0x88, 0x0D, 0x08, 0x01, 0x01, 0x00,
    0x00, 0x81, 0x41, 0x00, 0x02, 0x88, 0x0E, 0x14, 0x01, 0x01, 0x00, 0x00,
    0x81, 0x44, 0x00, 0x01, 0x23, 0x45, 0x02, 0x88, 0x0F, 0x10, 0x01, 0x01,
    0x00, 0x08, 0x81, 0x42, 0x00, 0x00, 0x02, 0x88, 0x10, 0x08, 0x01, 0x01,
    0x00, 0x00, 0x81, 0x41, 0x11, 0x02, 0x88, 0x11, 0x08, 0x01, 0x01, 0x00,
    0x00, 0x81, 0x41, 0x40, 0x02, 0x88, 0x12, 0x18, 0x40, 0x01, 0x02, 0x00,
    0x00, 0x81, 0x00, 0x02, 0x88, 0x13, 0x18, 0x40, 0x01, 0x02, 0x00, 0x00,
    0x81, 0x01, 0x02, 0x88, 0x14, 0x18, 0x40, 0x01, 0x02, 0x00, 0x00, 0x81,
    0x02, 0x02, 0x88, 0x15, 0x18, 0x40, 0x01, 0x02, 0x00, 0x00, 0x81, 0x03,
    0x02, 0x88, 0x16, 0x10, 0x01, 0x02, 0x00, 0x00, 0x81, 0x04, 0x02, 0x88,
    0x17, 0x10, 0x01, 0x02, 0x00, 0x00, 0x81, 0x05, 0x02, 0x88, 0x18, 0x18,
    0x10, 0x01, 0x01, 0x00, 0x04, 0x81, 0x42, 0x00, 0x00, 0x02, 0x88, 0x18,
    0x19, 0x10, 0x01, 0x01, 0x00, 0x05, 0x81, 0x42, 0x00, 0x00, 0x02, 0xA4,
    0xD9};

// Timers
static timer_obj_t sdk_timers[6];

// Callbacks for timers
static void sdk_timer_1_event(void *context)
{
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  mgt_timer_timeout(2);
}

static void sdk_timer_4_event(void *context)
{
  mgt_timer_timeout(3);
}

static void sdk_timer_5_event(void *context)
{
  mgt_timer_timeout(4);
}

static void sdk_timer_6_event(void *context)
{
  mgt_timer_timeout(5);
}

static void mgt_start_timer(uint8_t id, uint32_t duration)
{
  platform_timer_set_duration(&sdk_timers[id], duration);
  platform_timer_start(&sdk_timers[id]);
}

static void mgt_stop_timer(uint8_t id)
{
  platform_timer_stop(&sdk_timers[id]);
}

static void mgt_bootstrap_result(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    AT_PRINTF("+SYNCOK\n");
  }
  else
  {
    AT_PRINTF("+SYNCFAIL,%d\n", state);
  }
}

static void mgt_connectivity_state(mgt_status_t state)
{
  if (state == MGT_SUCCESS)
  {
    AT_PRINTF("+CONNECTED\n");
  }
  else
  {
    AT_PRINTF("+CONNFAIL\n");
  }
}

static void mgt_processing_required(void)
{
  mgt_process_request = true;
}

bool fullsdk_is_busy(void)
{
  return mgt_process_request;
}

mgt_status_t fullsdk_mgt_init(void)
{
  if (init_flag)
    return MGT_L2A_ERR;
  // return SFX_ERR_NONE;

  platform_timer_add(&sdk_timers[0], 0, sdk_timer_1_event, NULL);
  platform_timer_add(&sdk_timers[1], 1, sdk_timer_2_event, NULL);
  platform_timer_add(&sdk_timers[2], 2, sdk_timer_3_event, NULL);
  platform_timer_add(&sdk_timers[3], 3, sdk_timer_4_event, NULL);
  platform_timer_add(&sdk_timers[4], 4, sdk_timer_5_event, NULL);
  platform_timer_add(&sdk_timers[5], 5, sdk_timer_6_event, NULL);

  // Initialize MGT layer.
  mgt_callbacks_t callbacks = {mgt_processing_required, mgt_connectivity_state,
                               mgt_start_timer, mgt_stop_timer,
                               mgt_bootstrap_result};
  mgt_status_t status =
      mgt_initialize(&callbacks, mgt_mem_block, MEM_BLOCK_SIZE, MAX_MTU_SIZE,
                     MAX_PAYLOAD_SIZE);
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  // Provision template
  status =
      mgt_provision_header_rules((const uint8_t *)cbor_rule, sizeof(cbor_rule),
                                 tpl_memory_area, TPL_MEMORY_SIZE);
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  // Configure template parameters with dummy values.
  // IPv6DevPrefix
  static uint8_t ipv6_dev_prefix[] = {0x00, 0x00, 0x00, 0x00,
                                      0x00, 0x00, 0x00, 0x00};
  status = mgt_set_template_param(0, ipv6_dev_prefix, sizeof(ipv6_dev_prefix));
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  // IPv6DevIID
  static uint8_t ipv6_dev_iid[] = {0x00, 0x00, 0x00, 0x00,
                                   0x00, 0x00, 0x00, 0x00};
  status = mgt_set_template_param(1, ipv6_dev_iid, sizeof(ipv6_dev_iid));
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  // IPv6AppPrefix
  static uint8_t ipv6_app_prefix[] = {0x00, 0x00, 0x00, 0x00,
                                      0x00, 0x00, 0x00, 0x00};
  status = mgt_set_template_param(2, ipv6_app_prefix, sizeof(ipv6_app_prefix));
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  // IPv6AppIID
  static uint8_t ipv6_app_iid[] = {0x00, 0x00, 0x00, 0x00,
                                   0x00, 0x00, 0x00, 0x00};
  status = mgt_set_template_param(3, ipv6_app_iid, sizeof(ipv6_app_iid));
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  // UDPDevPort
  static uint8_t udp_dev_port[] = {0x00, 0x00};
  status = mgt_set_template_param(4, udp_dev_port, sizeof(udp_dev_port));
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  // UDPAppPort
  static uint8_t udp_app_port[] = {0x00, 0x00};
  status = mgt_set_template_param(5, udp_app_port, sizeof(udp_app_port));
  if (status != MGT_SUCCESS)
  {
    return status;
  }

  PRINT_DBG("sdk> template configured\n");

  return MGT_SUCCESS;
}

void fullsdk_process(void)
{
  if (mgt_process_request)
  {
    mgt_process_request = false;
    mgt_status_t mgt_status = mgt_process();
    if (mgt_status != MGT_SUCCESS)
    {
      AT_PRINTF("sdk> error in mgt_process: %d\n\r", mgt_status);
    }
  }
}

#endif /* AT_MGT_H */