#!/bin/sh

echo "USAGE: ./examples/OscoreTunProxy/make_tarball.sh [0|1 COMPRESSION_MODE] [FULLSDK_VERSION] [ARCHI] [UOSCORE|LIBOSCORE OSCORE_LIB] [1|2|3|4 EDHOC_TEST]"

echo "current parameters: $@\n\n\n\n"

MODE=$1

##Run Build script
sudo ./examples/OscoreTunProxy/build_libs.sh

if [ $MODE -eq 0 ]
then
    echo "REGULAR COMPRESSION ENABLED\n\n"
    ##compile OSCORETUNProxy example with specific parameters
    make FULLSDK_VERSION=$2 APP_NAME=OscoreTunProxy ARCH=$3 PLATFORM=linux L2_STACK=udp EXTENSION_API=orangelabs FRAGMENTATION_API=orangelabs OSCORE_LIB=$4 EDHOC_TEST=$5 app -j
elif [ $MODE -eq 1 ]
    then
        echo "COMPRESSION PATTERN ENABLED\n\n"
        make FULLSDK_VERSION=$2 APP_NAME=OscoreTunProxy ARCH=$3 PLATFORM=linux L2_STACK=udp EXTENSION_API=orangelabs FRAGMENTATION_API=orangelabs PATTERN_COMP=1 OSCORE_LIB=$4 EDHOC_TEST=$5 app -j
else
    echo "Error the first argument should be equal to 1 or 0\n"
    exit 1
fi
##get return value of the previous command
RESULT=$?
if [ $RESULT -eq 0 ]
then
    mkdir ./build/delivery
    cp ./build/linux/OscoreTunProxy ./libs/libsodium/src/libsodium/.libs/libsodium.so* ./libs/libcoap/.libs/libcoap-2*.so* ./libs/libcose/bin/libcose.so ./libs/nanocbor/bin/nanocbor.so ./examples/OscoreTunProxy/README_orange.md ./examples/OscoreTunProxy/scripts -R ./build/delivery
    ##if we are using PATTERN COMP We copy the json file
    if [ $MODE -eq 1 ]
        then
        cp ./examples/OscoreTunProxy/pattern_compression_rules.json ./build/delivery
    fi
    cd build
    tar -czvf ./OscoreTunProxy.tar.gz ./delivery/
else 
    echo "Error OSCORETunProxy compilation failure\n"
fi