# OscoreTunProxy

## Requirements

Add the libraries' directories to your LD_LIBRARY_PATH:
POSIX shells: `export LD_LIBRARY_PATH=.`
Fish: `set -x LD_LIBRARY_PATH .`

Run `sudo ./scripts/tun_client.sh` on the client machine and `sudo ./scripts/tun_server.sh` on the server machine, these will setup the TUN interfaces

### Server end

`./OscoreTunProxy coap_tun_server server <local-IPv4> 5690 <remote-IPv4> 5689 4242:4242:4242:4242::3 5696 2121:2121:2121:2121::3 5694 1`

Open a second terminal and launch a leshan server:
`java -jar leshan-server-demo/target/leshan-server-demo-*-SNAPSHOT-jar-with-dependencies.jar -lp 5696`

---

### Client end

In a first shell:
`./OscoreTunProxy coap_tun_client client <local-IPv4> 5689 <remote-IPv4> 5690 2121:2121:2121:2121::4 5694 4242:4242:4242:4242::4 5696 1`

And in a last shell:

If you are on a mangOH board:
Run the script `set_wclient_config_mangoh.sh <client-port> <server-addr> <server-port>`
And then `app restart wClient`

If not, then directly launch the wClient:
`./wClient -l 5694 -h 4242:4242:4242:4242::4 -p 5696`

Note: If both the client and server run on the same machine the server and client IPv6 addresses given as parameters must not be the exact ones otherwise packets will bypass the tun interfaces

Note: Switch the last parameter to 0 to bypass SCHC compression
