#!/bin/sh


mangoh=false
full_sdk_delivery_root=$(pwd)

while getopts "mp:" opt
do
  case $opt in
    m ) mangoh=true;;
  esac
done

##UOSCORE PATCHES
patch "$full_sdk_delivery_root/libs/fraunhofer-lib/modules/oscore/oscore.h" "$full_sdk_delivery_root/libs/uoscore_patch/oscore.patch"
patch "$full_sdk_delivery_root/libs/fraunhofer-lib/modules/oscore/src/coap.c" "$full_sdk_delivery_root/libs/uoscore_patch/oscore_coap.patch"
patch "$full_sdk_delivery_root/libs/fraunhofer-lib/modules/oscore/inc/error.h" "$full_sdk_delivery_root/libs/uoscore_patch/oscore_error.patch"
patch "$full_sdk_delivery_root/libs/fraunhofer-lib/modules/oscore/src/oscore2coap.c" "$full_sdk_delivery_root/libs/uoscore_patch/oscore2coap.patch"
patch "$full_sdk_delivery_root/libs/fraunhofer-lib/modules/oscore/src/coap2oscore.c" "$full_sdk_delivery_root/libs/uoscore_patch/coap2oscore.patch"
cp "$full_sdk_delivery_root/libs/uoscore_patch/oscore_sdk.patch" "$full_sdk_delivery_root/libs/fraunhofer-lib/modules/oscore/inc/oscore_sdk.h"
patch "$full_sdk_delivery_root/libs/fraunhofer-lib/externals/tinycbor/src/cborvalidation.c" "$full_sdk_delivery_root/libs/tinycbor_fix.patch"

##LIBOSCORE PATCHES
patch "$full_sdk_delivery_root/libs/liboscore/src/protection.c" "$full_sdk_delivery_root/libs/liboscore_fix.patch"
patch "$full_sdk_delivery_root/libs/libcose/Makefile" "$full_sdk_delivery_root/libs/libcose_makefile_fix.patch"

libcoap_configure="./configure --enable-dtls=no --disable-documentation"

libsodium_configure="./configure"
export PKG_CONFIG_PATH="$full_sdk_delivery_root/libs/libsodium/"
if [ "$mangoh" = true ];
then
  export PKG_CONFIG_PATH=/home/mangoh/.leaf/wp77-toolchain_SWI9X06Y_02.35.02.00-linux64/sysroots/armv7a-neon-poky-linux-gnueabi/lib/pkgconfig/
  export INC_GLOBAL=/home/mangoh/.leaf/wp77-toolchain_SWI9X06Y_02.35.02.00-linux64/sysroots/armv7a-neon-poky-linux-gnueabi/usr/include/:/home/mangoh/.leaf/wp77-toolchain_SWI9X06Y_02.35.02.00-linux64/sysroots/armv7a-neon-poky-linux-gnueabi/include/:/home/mangoh/.leaf/wp77-toolchain_SWI9X06Y_02.35.02.00-linux64/sysroots/armv7a-neon-poky-linux-gnueabi/
  export CC=/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-gcc
  export CFLAGS=--sysroot=/home/mangoh/.leaf/wp77-toolchain_SWI9X06Y_02.35.02.00-linux64/sysroots/armv7a-neon-poky-linux-gnueabi
  export CPATH=/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/x86_64-pokysdk-linux/usr/lib/arm-poky-linux-gnueabi/gcc/arm-poky-linux-gnueabi/7.3.0/include/:/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/x86_64-pokysdk-linux/usr/lib/arm-poky-linux-gnueabi/gcc/arm-poky-linux-gnueabi/7.3.0/usr/include/:/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/armv7a-neon-poky-linux-gnueabi/usr/include/:/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/armv7a-neon-poky-linux-gnueabi/include/
  configure_arm="--host=arm-poky-linux-gnueabi --prefix=/home/mangoh/.leaf/wp77-toolchain_SWI9X06Y_02.35.02.00-linux64/sysroots/armv7a-neon-poky-linux-gnueabi"
  libcoap_configure="$libcoap_configure $configure_arm"
  libsodium_configure="$libsodium_configure $configure_arm"
fi

echo "Compiling libcoap"
cd "$full_sdk_delivery_root/libs/libcoap"
./autogen.sh
$libcoap_configure
make

echo "Compiling libsodium"
cd "$full_sdk_delivery_root/libs/libsodium"
./autogen.sh -s 
$libsodium_configure
make

echo "Compiling nanocbor"
cd "$full_sdk_delivery_root/libs/nanocbor" && make

echo "Compiling libcose"
cd "$full_sdk_delivery_root/libs/libcose" && make

echo "Compiling libtinycbor"
cd "$full_sdk_delivery_root/libs/fraunhofer-lib/externals/tinycbor/" && make lib/libtinycbor.a

