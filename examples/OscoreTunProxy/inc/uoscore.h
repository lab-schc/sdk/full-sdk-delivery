/**
 * @file usocore.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef UOSCORE_H_
#define UOSCORE_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "oscore.h"
#include "utils.h"

extern struct context c_sender;
extern struct context c_receiver;

bool set_oscore_security_context(struct context *c_sender, struct context *c_receiver, uint8_t *oscore_master_secret, uint16_t oscore_master_secret_size, uint8_t *oscore_master_salt, uint16_t oscore_master_salt_size);
coap_oscore_res_t coap_to_oscore(uint8_t *coap_packet, uint16_t coap_packet_size, uint8_t *out, uint16_t *out_size);
coap_oscore_res_t oscore_to_coap(uint8_t *oscore_packet, uint16_t oscore_packet_size, uint8_t *out, uint16_t *out_size);

#endif