/**
 * @file liboscore.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Thibaut Artis thibaut.Artis@ackl.io
 */

#ifndef LIBOSCORE_H_
#define LIBOSCORE_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "utils.h"

#define SENDER_KEY                                                                \
    {                                                                             \
        21, 68, 108, 130, 191, 168, 146, 141, 235, 127, 249, 82, 132, 37, 174, 9, \
            203, 193, 76, 0, 206, 134, 156, 96, 140, 99, 176, 80, 139, 254, 22,   \
            176                                                                   \
    }
#define RECIPIENT_KEY                                                            \
    {                                                                            \
        50, 136, 42, 28, 97, 144, 48, 132, 56, 236, 152, 230, 169, 50, 240, 32,  \
            112, 143, 55, 57, 223, 228, 109, 119, 152, 155, 3, 155, 31, 252, 28, \
            172                                                                  \
    }
#define COMMON_IV                                          \
    {                                                      \
        100, 240, 189, 49, 77, 75, 224, 60, 39, 12, 43, 28 \
    }
#define SENDER_ID \
    {             \
        2         \
    }
#define RECIPIENT_ID \
    {                \
        1            \
    }

bool set_oscore_security_context(const char *role);
coap_oscore_res_t coap_to_oscore(const coap_pdu_t *to_protect,
                                 coap_pdu_t **out);
coap_oscore_res_t oscore_to_coap(coap_pdu_t *to_decrypt, coap_pdu_t **out);

#endif