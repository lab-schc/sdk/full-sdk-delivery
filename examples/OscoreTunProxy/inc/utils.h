/**
 * @file utils.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Hadi Bereksi hadi-ilies.berksi-reguig@ackl.io
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdbool.h>

#include "libcoap.h"
#include "pdu.h"
#define COAP_PDU_MAX_SIZE 512

typedef enum
{
  CO_SUCCESS,
  CO_COAP_ERROR,
  CO_OSCORE_ERROR,
  CO_MAX_SESSION_REACHED,
  CO_UNKNOWN_SESSION,
} coap_oscore_res_t;

uint8_t *get_coap_packet_bytes(coap_pdu_t *pdu);
size_t get_coap_packet_len(coap_pdu_t *pdu);
void coap_dump(coap_pdu_t *pdu);
void dump_buf(uint8_t *buf, uint16_t len);

#endif // OSCORE_LIBS_H_
