/**
 * @file app_globals.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef APP_GLOBALS_H_
#define APP_GLOBALS_H_

#include <stdbool.h>
#include <semaphore.h>

#include "fullsdkextapi.h"
#include "fullsdkfragapi.h"
#define RECEIVE_BUFFER_SIZE 8000

extern sdk_mode_t sdk_mode;
extern sem_t sdk_mgt_process_sem;
extern sem_t data_sent_sem;
extern sem_t data_rcvd_sem;

extern uint8_t rx_buf[RECEIVE_BUFFER_SIZE];
extern uint16_t rx_buf_size;
extern bool is_edhoc_done;

extern const char *host_ip;
extern const char *host_port;
extern const char *remote_ip;
extern const char *remote_port;
extern bool schc_compression;

#endif