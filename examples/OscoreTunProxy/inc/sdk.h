/**
 * @file sdk.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Thibaut Artis thibaut.artis@ackl.io
 */

#ifndef ACKLIO_TUN_SDK_H_
#define ACKLIO_TUN_SDK_H_

#include "tun.h"

bool generate_udp_packet(uint8_t *payload, uint16_t payload_size, uint8_t *packet_out, uint16_t *packet_out_size);

// In this file are the functions used to make the bridge between the SDK and
// the TUN interface parts

// Variable used to determine if mgt_process() should be called
extern bool mgt_process_request;

// Callback called by the SDK once a packet has been sent over LPWAN
void tun_net_transmission_result(net_status_t status, uint16_t error);
// Callback called by the SDK once a packet has been received from LPWAN
void tun_net_data_received(const uint8_t *buff, uint16_t data_size,
                           net_status_t status);

// Returns true if the SDK is free to start an uplink communication, false
// otherwise
bool get_can_send(void);

// Function to be called when a packet is sent to prevent other packets from
// being sent while the current one has not been entirely transmitted
void set_cannot_send(void);

// Function called at every loop iteration in tun_listen
// Calls mgt_process when required, otherwise calls wait_for_event
bool process_app(int tun_fd);

bool sdk_init(const char **av);

void *thread_listen(void *vargp);
void *thread_sdk(void *vargp);

#endif // ACKLIO_TUN_SDK_H_
