/**
 * @file pattern_compression.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef PATTERN_COMPRESSION_H_
#define PATTERN_COMPRESSION_H_

#include <stdint.h>
#include <stdbool.h>

#define NO_COMPRESSION_RULE_ID 150
#define MAX_PATTERN_SIZE 1024

typedef struct
{
    uint8_t rule_id;
    uint16_t pattern_size;
    uint8_t mask[MAX_PATTERN_SIZE];
    uint8_t value[MAX_PATTERN_SIZE];
} pattern_table_t;

//pattern compression globals
extern pattern_table_t *pattern_tables;
extern uint16_t nb_pattern_table;
extern const char *pattern_comp_json_filename;

/**
 *  This function allows to free the pattern tables
 *  
 * Parameters:
 * None 
 * returned value
 * None
 */
void pattern_free_tables(void);

/**
 *  This function allows to get all the patterns from a json file
 *  
 * Parameters: 
 *  nb_pattern_table: nb pattern table inside the json file
 * 
 * returned value
 *  pattern_table_t: array of pattern in case of success otherwise NULL 
 */
pattern_table_t *pattern_compression_init(uint16_t *nb_pattern_table);

/**
 *  This function allows to compress a packet if a pattern match
 *  
 * Parameters: 
 *  pattern_tables: array of patterns
 *  plaintext: data to be compressed 
 *  plaintext_size: data size
 * 
 * Returned value
 *  bool: true in case of success otherwise false 
 */
bool pattern_compression_match(pattern_table_t *pattern_tables, uint8_t *plaintext, uint32_t *plaintext_size);

/**
 * This function allows to decompress a packet if a rule_id inside the plaintext match
 *  
 * Parameters: 
 *  pattern_tables: array pattern table inside the json file
 *  plaintext: data to be decompressed 
 *  plaintext_size: data size

 * Returned value
 *  bool: true in case of success otherwise false 
 */
bool pattern_decompression_match(pattern_table_t *pattern_tables, uint8_t *plaintext, uint32_t *plaintext_size);

#endif