/**
 * @file common.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <errno.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <signal.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

#include "fullsdknet.h"
#include "utils.h"
#include "platform.h"
#include "sdk.h"
#include "tun.h"
#include "udp.h"
#include "fullsdkextapi.h"
#include "fullsdkmgt.h"
#include "utils.h"

#ifdef UOSCORE
#include "uoscore.h"
#endif

#ifdef LIBOSCORE
#include "liboscore.h"
#endif

#ifdef PATTERN_COMP
#include "pattern_compression.h"
#endif

#define OSCORE_MAX_PAYLOAD_SIZE 255

bool process_uplink_packet(uint8_t *packet, size_t *packet_size);
bool process_downlink_packet(uint8_t *packet, size_t *packet_size, uint16_t data_size);
void disable_inner_compression(void);

#endif