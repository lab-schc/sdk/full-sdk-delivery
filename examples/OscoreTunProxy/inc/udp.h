/**
 * @file udp.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Thibaut Artis thibaut.artis@ackl.io
 */

#ifndef UDP_H_
#define UDP_H_

#include <stdint.h>
#include <stdlib.h>

uint16_t udp_checksum(const void *packet, uint16_t size,
             struct in6_addr *src_addr, struct in6_addr *dst_addr);
void update_udp_checksum(char *packet);
void update_ip6_udp_len(char *packet, size_t size);

#endif // UDP_H_