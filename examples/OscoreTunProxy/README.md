# OscoreTunProxy

## Building

You can run this project with two differentes OSCORE libs. 

From the top of the repository:

### UOSCORE
```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_NAME=OscoreTunProxy -DTOOLCHAIN=gcc-native -DTARGET=default -DPLATFORM=linux -DL2_STACK=udp -DEXTENSION_API=orangelabs -DFRAGMENTATION_API=orangelabs -DOSCORE_LIB=UOSCORE -DEDHOC_TEST=1 && make -C ./build
```
### LIBOSCORE
```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_NAME=OscoreTunProxy -DTOOLCHAIN=gcc-native -DTARGET=default -DPLATFORM=linux -DL2_STACK=udp -DEXTENSION_API=orangelabs -DFRAGMENTATION_API=orangelabs -DOSCORE_LIB=LIBOSCORE -DEDHOC_TEST=<edhoc_test_number> && make -C ./build
```

### PATTERN COMPRESSION
```sh
cmake -S . -B ./build -DFULLSDK_VERSION=dev -DAPP_NAME=OscoreTunProxy -DTOOLCHAIN=gcc-native -DTARGET=default -DPLATFORM=linux -DL2_STACK=udp -DEXTENSION_API=orangelabs -DFRAGMENTATION_API=orangelabs -DPATTERN_COMP=1 -DOSCORE_LIB=LIBOSCORE -DEDHOC_TEST=<edhoc_test_number> && make -C ./build
```
```sh
./build/linux/./OscoreTunProxy coap_tun_server server 127.0.0.1 5690 127.0.0.1 5689 4242:4242:4242:4242::3 5696 2121:2121:2121:2121::3 5694 1 examples/OscoreTunProxy/pattern_compression_rules.json
```

### Notes
* Don't forget to export the needed shared libraries otherwise it will not work
* Edhoc exchange is done on both libraries (UOSCORE and LIBOSCORE). But OSCORE `master` and `salt` keys are only derived with UOSCORE. OSCORE's credentials are still constants values on LIBOSCORE
* Compilation variables [INITATOR_TEST_X] and [RESPONDER_TEST_X] are set through the Cmakelist. Those variables allow us to use different types of encryptions and keys with EDHOC protocol
* `Pattern compression` is a simple compression mode that can be configurable through an external `JSON file` to add flexibility for inner compression. We expect to have messages that have a lot of known bytes (pattern). If a message match the pattern, then we build a residue with the remaining bytes
* You can enable pattern compression by adding `PATTERN_COMP=1` as argument of the compilation command line
* The document that explains the Pattern compression is [available](https://acklio.atlassian.net/wiki/spaces/DEV/pages/2450128934/Pattern+compression) on confluence

#### TESTS 
EDHOC_TEST       | mode                          | RPK/Cert | suite | Ref [1]
|----------------|-------------------------------|----------|-------|-------
 1               | INITIATOR_SK_RESPONDER_SK     | RPK x5t  | 0     | 1-290
 2               | INITIATOR_SDHK_RESPONDER_SDHK | RPK kid  | 0     | 292-540
 3               | INITIATOR_SK_RESPONDER_SK     | x5chain  | 0     | non
 4               | INITIATOR_SDHK_RESPONDER_SDHK | x5chain  | 0     | non


Run `./build_libs.sh -p .` from the top of the repository to build the libraries

in order to build for mangOH board:
 - change ARCH to `mangoh`
 - set the following env variables:
    - `export CC=/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi/arm-poky-linux-gnueabi-gcc`
    - `export CFLAGS=--sysroot=/home/mangoh/.leaf/wp77-toolchain_SWI9X06Y_02.35.02.00-linux64/sysroots/armv7a-neon-poky-linux-gnueabi`
    - `export CPATH=/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/x86_64-pokysdk-linux/usr/lib/arm-poky-linux-gnueabi/gcc/arm-poky-linux-gnueabi/7.3.0/include/:/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/x86_64-pokysdk-linux/usr/lib/arm-poky-linux-gnueabi/gcc/arm-poky-linux-gnueabi/7.3.0/usr/include/:/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/armv7a-neon-poky-linux-gnueabi/usr/include/:/opt/mangoh-lwm2m/leaf-data/mangoh-lwm2m/wp77-toolchain/sysroots/armv7a-neon-poky-linux-gnueabi/include/`
 - add the -m option to the `build_libs.sh` script

## Running the example

## Requirements

Add the libraries' directories to your LD_LIBRARY_PATH:
POSIX shells: `export LD_LIBRARY_PATH=.`
Fish: `set -x LD_LIBRARY_PATH .`

Run `sudo ./scripts/tun_client.sh` on the client machine and `sudo ./scripts/tun_server.sh` on the server machine, these will setup the TUN interfaces

### Server end

`./OscoreTunProxy coap_tun_server server <local-IPv4> 5690 <remote-IPv4> 5689 4242:4242:4242:4242::3 5696 2121:2121:2121:2121::3 5694 1`

Open a second terminal and launch a leshan server:
`java -jar leshan-server-demo/target/leshan-server-demo-*-SNAPSHOT-jar-with-dependencies.jar -lp 5696`

---

### Client end

In a first shell:
`./OscoreTunProxy coap_tun_client client <local-IPv4> 5689 <remote-IPv4> 5690 2121:2121:2121:2121::4 5694 4242:4242:4242:4242::4 5696 1`

And in a last shell:

If you are on a mangOH board:
Run the script `set_wclient_config_mangoh.sh <client-port> <server-addr> <server-port>`
And then `app restart wClient`

If not, then directly launch the wClient:
`./wClient -l 5694 -h 4242:4242:4242:4242::4 -p 5696`

Note: If both the client and server run on the same machine the server and client IPv6 addresses given as parameters must not be the exact ones otherwise packets will bypass the tun interfaces

Note: Switch the last parameter to 0 to bypass SCHC compression


## Generating OSCORE security configuration

Install the dev version of aiocoap: `pip3 install --upgrade ".[all,docs]"`

In the liboscore folder go in `tests/riot-tests/plugtest-server/`

Now create a folder and two files inside it:

`secret.json`

With written inside

```
{
  "secret_hex": "0102030405060708090a0b0c0d0e0f10", <= 16 bytes as hex
  "salt_hex": "9e7ca92223786340" <= 10 bytes as hex
}
```

and `settings.json`

```
{
  "algorithm": "AES-CCM-16-64-128",

  "sender-id_hex": "02",
  "recipient-id_hex": "01"
}
```

Now run : `./oscore-key-derivation <folder>`

## Run delivery script for customers

`./examples/OscoreTunProxy/make_tarball.sh [COMPRESSION_MODE] [FULLSDK_VERSION] [ARCH] [OSCORE_LIB] [EDHOC_TEST]`

* COMPRESSION_MODE can be equal to 0 for REGULAR COMPRESSION or 1 for PATTERN COMPRESSION
* FULLSDK_VERSION can be equal to any value
* ARCH has been tested with `x86_64` only
* OSCORE_LIB parameter can be `UOSCORE` or `LIBOSCORE` 
* EDHOC_TEST can be equal to 4 values check the `TESTS` section above

### Note:
* As mentionned above don't forget to execute the scripts inside the `scripts folder` as `admin` BEFORE running the `OscoreTunProxy binary`