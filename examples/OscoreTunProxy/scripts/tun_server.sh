#!/bin/sh
ip tuntap add mode tun name coap_tun_server
ip link set coap_tun_server up
ip -6 addr add 4242:4242:4242:4242::3 dev coap_tun_server
ip -6 route add 2121:2121:2121:2121::/64 dev coap_tun_server