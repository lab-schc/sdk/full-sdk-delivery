#!/bin/sh
ip tuntap add mode tun name coap_tun_client
ip link set coap_tun_client up
ip -6 addr add 2121:2121:2121:2121::4 dev coap_tun_client
ip -6 route add 4242:4242:4242:4242::/64 dev coap_tun_client