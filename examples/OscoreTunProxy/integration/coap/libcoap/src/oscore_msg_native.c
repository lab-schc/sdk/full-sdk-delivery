/**
 * @file oscore_msg_native.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Thibaut Artis thibaut.artis@ackl.io
 *
 * libcoap light integration implementation.
 */

#include <unistd.h>
#include <stdio.h>

#include <oscore_native/message.h>

#include "pdu.h"
#include "option.h"

uint8_t oscore_msg_native_get_code(oscore_msg_native_t msg)
{
  return msg.pkt->code;
}

void oscore_msg_native_set_code(oscore_msg_native_t msg, uint8_t code)
{
  msg.pkt->code = code;
}

oscore_msgerr_native_t oscore_msg_native_append_option(oscore_msg_native_t msg,
                                                      uint16_t option_number,
                                                      const uint8_t* value,
                                                      size_t value_len)
{
  return !coap_add_option(msg.pkt, option_number, value_len, value);
}

bool oscore_msgerr_native_is_error(oscore_msgerr_native_t err)
{
  return err != 0;
}

void oscore_msg_native_optiter_init(oscore_msg_native_t msg,
                                    oscore_msg_native_optiter_t *iter)
{
  coap_option_iterator_init(msg.pkt, &(iter->pos), COAP_OPT_ALL);
}

bool oscore_msg_native_optiter_next(oscore_msg_native_t msg,
                                    oscore_msg_native_optiter_t *iter,
                                    uint16_t *option_number,
                                    const uint8_t **value,
                                    size_t *value_len)
{
  coap_opt_t *opt = coap_option_next(&(iter->pos));

  if (opt == NULL)
    return false;
  *option_number = iter->pos.type;
  *value = coap_opt_value(opt);
  *value_len = coap_opt_length(opt);

  return true;
}

oscore_msgerr_native_t oscore_msg_native_optiter_finish(
                                    oscore_msg_native_t msg,
                                    oscore_msg_native_optiter_t *iter)
{
  (void)msg;
  (void)iter;
  return 0;
}

oscore_msgerr_native_t oscore_msg_native_update_option(
                                    oscore_msg_native_t msg,
                                    uint16_t option_number,
                                    size_t option_occurrence,
                                    const uint8_t *value,
                                    size_t value_len)
{
  coap_opt_iterator_t iter;
  if (!coap_option_iterator_init(msg.pkt, &iter, COAP_OPT_ALL))
    return 1;

  uint16_t iter_len; 
  uint16_t iter_delta;

  while (true)
  {
    coap_opt_t *opt = coap_option_next(&iter);
    if (opt == NULL)
      return 1;
    
    iter_len = coap_opt_length(opt);
    iter_delta = iter.type;
    if (iter_delta == option_number)
    {
      if (option_occurrence > 0)
      {
        option_occurrence -= 1;
        continue;
      }
      else if (value_len != (size_t)iter_len)
        return 1;
      else if (value_len != 0)
      {
        iter_delta = coap_opt_delta(opt);
        uint8_t opt_header_size = 1;
        if (iter_len >= COAP_EXTENDED_OFFSET_1 && iter_len < COAP_EXTENDED_OFFSET_2)
          opt_header_size += 1;
        else if (iter_len >= COAP_EXTENDED_OFFSET_2)
          opt_header_size += 2;
        if (iter_delta >= COAP_EXTENDED_OFFSET_1 && iter_delta < COAP_EXTENDED_OFFSET_2)
          opt_header_size += 1;
        else if (iter_delta >= COAP_EXTENDED_OFFSET_2)
          opt_header_size += 2;

        return !coap_opt_encode(opt, iter_len + opt_header_size,
                               iter_delta, value, value_len);
      }
    }
  }
}

oscore_msgerr_native_t oscore_msg_native_map_payload(oscore_msg_native_t msg,
                                                    uint8_t **payload,
                                                    size_t *payload_len)
{
  *payload = msg.oscbuf->buf;
  *payload_len = msg.oscbuf->avail;
  return 0;
}

oscore_msgerr_native_t oscore_msg_native_trim_payload(oscore_msg_native_t msg,
                                                      size_t payload_len)
{
  if (payload_len > msg.oscbuf->avail)
    return 1;
  msg.oscbuf->avail = payload_len;
  return 0;
}