/**
 * @file pattern_compression.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "parson.h"
#include "utils.h"
#include "pattern_compression.h"

pattern_table_t *pattern_tables;
uint16_t nb_pattern_table;

bool pattern_compression_match(pattern_table_t *pattern_tables, uint8_t *plaintext, uint32_t *plaintext_size)
{
    uint8_t out_inner_comp_buf[MAX_PATTERN_SIZE] = {0};
    uint16_t out_data_size = 0;

    printf("------------- Check Value pattern ------------\n");
    dump_buf(pattern_tables->value, pattern_tables->pattern_size);

    out_inner_comp_buf[out_data_size] = pattern_tables->rule_id;
    out_data_size++;
    for (uint16_t i = 0; i < *plaintext_size; i++)
    {
        if (i < pattern_tables->pattern_size && pattern_tables->mask[i] == 1)
        {
            if (plaintext[i] != pattern_tables->value[i])
                return false;
            else
                continue;
        }
        out_inner_comp_buf[out_data_size++] = plaintext[i];
    }
    printf("------------- Inner Compressed packet ------------\n");
    dump_buf(out_inner_comp_buf, out_data_size);

    // Move the compressed OSCORE Plaintext into the input buffer.
    memcpy(plaintext, out_inner_comp_buf, out_data_size);
    *plaintext_size = out_data_size;
    return true;
}

static uint16_t pattern_count_mask(uint8_t *mask, uint16_t pattern_size)
{
    uint16_t counter = 0;

    //get nb byte equal to 1
    for (uint16_t i = 0; i < pattern_size; i++)
        if (mask[i] == 1)
            counter++;
    return counter;
}

bool pattern_decompression_match(pattern_table_t *pattern_tables, uint8_t *plaintext, uint32_t *plaintext_size)
{
    uint8_t out_inner_decomp_buf[MAX_PATTERN_SIZE] = {0};
    uint16_t out_data_size = 0;
    uint8_t rule_id = plaintext[0];
    uint8_t *tmp = plaintext + sizeof(rule_id);

    if (pattern_tables->rule_id != rule_id)
        return false;

    uint16_t j = 0;
    //reassembled packet equals to nb of "1" + the residue - rule_id
    uint16_t size = pattern_count_mask(pattern_tables->mask, pattern_tables->pattern_size) + *plaintext_size - sizeof(uint8_t);
    for (uint16_t i = 0; i < size; i++)
    {
        if (i < pattern_tables->pattern_size && pattern_tables->mask[i] == 1)
        {
            out_inner_decomp_buf[out_data_size++] = pattern_tables->value[i];
            continue;
        }
        out_inner_decomp_buf[out_data_size++] = tmp[j++];
    }
    printf("------------- Plaintext ------------\n");
    dump_buf(out_inner_decomp_buf, out_data_size);

    // Move the OSCORE Plaintext message into the input buffer.
    memcpy(plaintext, out_inner_decomp_buf, out_data_size);
    *plaintext_size = out_data_size;
    return true;
}

static void display_logs(pattern_table_t *pattern_tables, uint16_t nb_pattern_table)
{
    for (int i = 0; i < nb_pattern_table; i++)
    {
        printf("PATTERN TABLE %d\n", i + 1);
        printf("pattern size = %d | rule_id = %d\n", pattern_tables[i].pattern_size, pattern_tables[i].rule_id);
        printf("MASK = ");
        for (uint16_t j = 0; j < pattern_tables[i].pattern_size; j++)
        {
            printf("%d ", pattern_tables[i].mask[j]);
        }
        printf("\nVALUE = ");
        for (uint16_t j = 0; j < pattern_tables[i].pattern_size; j++)
        {
            printf("%02X ", pattern_tables[i].value[j]);
        }
        printf("\n");
    }
}

void pattern_free_tables(void)
{
    free(pattern_tables);
}

static char **split_c(char *this, char separator)
{
    size_t i = 0;
    char **array = malloc(sizeof(char *));

    if (array == NULL)
        return NULL;
    array[i] = strtok(this, &separator);
    while (array[i] != NULL)
    {
        i++;
        array = realloc(array, sizeof(char *) * (i + 1));
        array[i] = strtok(NULL, &separator);
    }
    return array;
}

static void pattern_get_value(uint8_t *dest, char *src, uint16_t src_size)
{
    char **splited_value = split_c(src, ' ');

    for (uint16_t i = 0; i < src_size; i++)
        sscanf(splited_value[i], "%hhx", &dest[i]);
}

static void pattern_ascii_to_hex(uint8_t *dest, const char *src, uint16_t src_size)
{
    for (uint16_t i = 0; i < src_size; i++)
        dest[i] = src[i] - '0';
}

pattern_table_t *pattern_compression_init(uint16_t *nb_pattern_table)
{
    JSON_Value *root_value;
    JSON_Object *json_pattern_tables;
    JSON_Array *json_pattern_table;
    pattern_table_t *patterns = NULL;

    root_value = json_parse_file(pattern_comp_json_filename);
    if (json_value_get_type(root_value) != JSONObject)
    {
        printf("Error: wrong JSON format\n");
        return NULL;
    }
    json_pattern_tables = json_value_get_object(root_value);
    json_pattern_table = json_object_get_array(json_pattern_tables, "patterns_table");
    *nb_pattern_table = json_array_get_count(json_pattern_table);
    patterns = malloc(sizeof(pattern_table_t) * *nb_pattern_table);

    if (!patterns)
        return NULL;
    for (int i = 0; i < *nb_pattern_table; i++)
    {
        JSON_Object *table = json_array_get_object(json_pattern_table, i);

        patterns[i].rule_id = (int)json_object_get_number(table, "rule_id");
        patterns[i].pattern_size = (int)json_object_get_number(table, "size");
        pattern_ascii_to_hex(patterns[i].mask, json_object_get_string(table, "mask"), patterns[i].pattern_size);
        char *value = (char *)json_object_get_string(table, "value");
        pattern_get_value(patterns[i].value, value, patterns[i].pattern_size);
    }
    /* cleanup code */
    json_value_free(root_value);
    //print logs
    display_logs(patterns, *nb_pattern_table);
    return patterns;
}