/**
 * @file main.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

#include "fullsdkmgt.h"
#include "fullsdknet.h"
#include "tun.h"
#include "sdk.h"
#include "utils.h"
#include "common.h"
#include "edhoc_init.h"
#include "app_globals.h"

#ifdef UOSCORE
struct context c_sender;
struct context c_receiver;
#endif

bool is_edhoc_done = false;
const char *host_ip;
const char *host_port;
const char *remote_ip;
const char *remote_port;
bool schc_compression;

int main(int ac, const char **av)
{
#ifdef REGULAR_COMP
  if (ac < 12 || strlen(av[11]) != 1)
  {
    printf("Usage: %s [tun_name] [oscore_role: client|server] "
           "[l2 host ipv4] [l2 host port] [l2 remote ipv4] [l2 remote port] "
           "[coap host ipv6] [coap host port] [coap remote ipv6] "
           "[coap remote port] [0|1 schc compression]\n",
           av[0]);
    return EXIT_FAILURE;
  }
#elif PATTERN_COMP
  if (ac < 13 || strlen(av[11]) != 1)
  {
    printf("Usage: %s [tun_name] [oscore_role: client|server] "
           "[l2 host ipv4] [l2 host port] [l2 remote ipv4] [l2 remote port] "
           "[coap host ipv6] [coap host port] [coap remote ipv6] "
           "[coap remote port] [0|1 schc compression] [pattern compression rules json file path]\n",
           av[0]);
    return EXIT_FAILURE;
  }
#endif
#ifdef UOSCORE
  printf("UOSCORE ENABLED\n\n");
#elif LIBOSCORE
  printf("LIBOSCORE ENABLED\n\n");
#endif
  set_tun_name(av[1]);

  schc_compression = *(av[11]) == '1';
  enable_schc_compression(schc_compression);
  if (schc_compression)
  {
    if (!sdk_init(av))
      return EXIT_FAILURE;
  }
  else if (!setup_l2a_bypass(av))
    return EXIT_FAILURE;

  //set src/dest ip/port
  host_ip = av[7];
  host_port = av[8];
  remote_ip = av[9];
  remote_port = av[10];
  //INIT EDHOC
  uint8_t oscore_master_secret[16];
  uint8_t oscore_master_salt[8];
  pthread_t thread_id_sdk, thread_id_listen;
  bool edhoc_init;

  if (schc_compression)
  {
    pthread_create(&thread_id_sdk, NULL, thread_sdk, NULL);
    pthread_create(&thread_id_listen, NULL, thread_listen, NULL);
  }
  if (!memcmp(av[2], "client", sizeof("client")))
  {
    edhoc_init = edhoc_initiator_init(oscore_master_secret, sizeof(oscore_master_secret), oscore_master_salt, sizeof(oscore_master_salt));
  }
  else
  {
    edhoc_init = edhoc_responder_init(oscore_master_secret, sizeof(oscore_master_secret), oscore_master_salt, sizeof(oscore_master_salt));
  }
  if (!edhoc_init)
  {
    printf("\nError : edhoc_init failed\n");
    return EXIT_FAILURE;
  }
  is_edhoc_done = true;
  if (schc_compression)
  {
    sem_destroy(&data_rcvd_sem);
    sem_destroy(&sdk_mgt_process_sem);
    sem_destroy(&data_sent_sem);
    printf("threads canceled\n");
  }
  printf("EDHOC Exchange Finished Successfully\n");

#ifdef LIBOSCORE
  set_oscore_security_context(av[2]);
#endif
#ifdef UOSCORE
  set_oscore_security_context(&c_sender, &c_receiver, oscore_master_secret, sizeof(oscore_master_secret), oscore_master_salt, sizeof(oscore_master_salt));
#endif

  if (!tun_listen())
  {
#ifdef PATTERN_COMP
    pattern_free_tables();
#endif
    return EXIT_FAILURE;
  }
#ifdef PATTERN_COMP
  pattern_free_tables();
#endif
  return EXIT_SUCCESS;
}