/**
 * @file liboscore.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Thibaut Artis thibaut.artis@ackl.io
 */

#include "oscore/context_impl/primitive.h"
#include "oscore/contextpair.h"
#include "oscore/message.h"
#include "oscore/protection.h"
#include "oscore_native/message.h"
#include "oscore_native/test.h"
#include "common.h"

#define COAP_TOKEN_MAX_LEN 8
#define MAX_PARALLEL_SESSION 32

bool inner_compression = true;

void disable_inner_compression(void)
{
    inner_compression = false;
}

#ifdef REGULAR_COMP
static coap_oscore_res_t
oscore_msg_inner_compression(oscore_msg_protected_t *msg)
{
    uint16_t plaintext_size = msg->backend.oscbuf->avail - msg->tag_length;

    printf("------------- Plaintext ------------\n");
    dump_buf(msg->backend.oscbuf->buf, plaintext_size);

    uint8_t out_inner_comp_buf[OSCORE_MAX_PAYLOAD_SIZE] = {0};
    uint16_t out_data_size;
    mgt_status_t status = mgt_ext_oscore_inner_compression(
        out_inner_comp_buf, sizeof(out_inner_comp_buf), &out_data_size,
        msg->backend.oscbuf->buf, plaintext_size);
    if (status != MGT_SUCCESS)
    {
        printf("Inner compression error in SDK\n");
        return CO_OSCORE_ERROR;
    }

    printf("------------- Inner compressed packet ------------\n");
    dump_buf(out_inner_comp_buf, out_data_size);

    // Move the compressed OSCORE Plaintext into the input buffer.
    memcpy(msg->backend.oscbuf->buf, out_inner_comp_buf, out_data_size);
    msg->backend.oscbuf->avail = out_data_size + msg->tag_length;

    return CO_SUCCESS;
}

static coap_oscore_res_t
oscore_msg_inner_decompression(oscore_msg_protected_t *msg)
{
    uint16_t plaintext_size = msg->backend.oscbuf->avail - msg->tag_length;

    printf("------------- Inner compressed packet ------------\n");
    dump_buf(msg->backend.oscbuf->buf, plaintext_size);

    uint8_t out_inner_decomp_buf[OSCORE_MAX_PAYLOAD_SIZE] = {0};
    uint16_t out_data_size;
    mgt_status_t status = mgt_ext_oscore_inner_decompression(
        out_inner_decomp_buf, sizeof(out_inner_decomp_buf), &out_data_size,
        msg->backend.oscbuf->buf, plaintext_size);
    if (status != MGT_SUCCESS)
    {
        printf("Inner decompression error in SDK\n");
        return CO_OSCORE_ERROR;
    }

    printf("------------- Plaintext ------------\n");
    dump_buf(out_inner_decomp_buf, out_data_size);

    // Move the OSCORE Plaintext message into the input buffer.
    memcpy(msg->backend.oscbuf->buf, out_inner_decomp_buf, out_data_size);
    msg->backend.oscbuf->avail = out_data_size + msg->tag_length;

    return CO_SUCCESS;
}
#endif

#ifdef PATTERN_COMP

static coap_oscore_res_t
oscore_msg_inner_compression(oscore_msg_protected_t *msg)
{
    uint32_t plaintext_size = msg->backend.oscbuf->avail - msg->tag_length;

    printf("------------- Plaintext ------------\n");
    dump_buf(msg->backend.oscbuf->buf, plaintext_size);

    for (uint16_t i = 0; i < nb_pattern_table; i++)
    {
        if (pattern_compression_match(&pattern_tables[i], msg->backend.oscbuf->buf, &plaintext_size))
        {
            msg->backend.oscbuf->avail = plaintext_size + msg->tag_length;
            printf("\nPATTERN COMPRESSION MATCHED: %d\n", pattern_tables[i].rule_id);
            return CO_SUCCESS;
        }
    }
    uint8_t out_inner_comp_buf[MAX_PACKET_SIZE_BYTES] = {0};
    uint16_t out_data_size = plaintext_size + sizeof(uint8_t);

    out_inner_comp_buf[0] = NO_COMPRESSION_RULE_ID;
    if (memcpy(out_inner_comp_buf + sizeof(uint8_t), msg->backend.oscbuf->buf, msg->backend.oscbuf->avail) == NULL)
        return CO_OSCORE_ERROR;
    printf("NO PATTERN COMPRESSION HAS MATCHED\n");
    printf("------------- Inner NOT Compressed packet ------------\n");
    dump_buf(out_inner_comp_buf, out_data_size);

    // Move the compressed OSCORE Plaintext into the input buffer.
    if (memcpy(msg->backend.oscbuf->buf, out_inner_comp_buf, out_data_size) == NULL)
        return CO_OSCORE_ERROR;
    msg->backend.oscbuf->avail = out_data_size + msg->tag_length;

    return CO_SUCCESS;
}

static coap_oscore_res_t
oscore_msg_inner_decompression(oscore_msg_protected_t *msg)
{
    uint32_t plaintext_size = msg->backend.oscbuf->avail - msg->tag_length;

    printf("------------- Inner compressed packet ------------\n");
    dump_buf(msg->backend.oscbuf->buf, plaintext_size);

    if (msg->backend.oscbuf->buf[0] == NO_COMPRESSION_RULE_ID)
    {
        //remove rule_id
        uint8_t *tmp = msg->backend.oscbuf->buf + sizeof(uint8_t);
        msg->backend.oscbuf->avail = msg->backend.oscbuf->avail - sizeof(uint8_t);
        memcpy(msg->backend.oscbuf->buf, tmp, msg->backend.oscbuf->avail);

        printf("------------- Inner NOT DEcompressed packet ------------\n");
        dump_buf(msg->backend.oscbuf->buf, msg->backend.oscbuf->avail);
    }
    else
    {
        for (uint16_t i = 0; i < nb_pattern_table; i++)
        {
            if (pattern_decompression_match(&pattern_tables[i], msg->backend.oscbuf->buf, &plaintext_size))
            {
                msg->backend.oscbuf->avail = plaintext_size + msg->tag_length;
                printf("\nPATTERN DECOMPRESSION MATCHED: %d\n", pattern_tables[i].rule_id);
                return CO_SUCCESS;
            }
        }
    }
    return CO_SUCCESS;
}

#endif
// Used to match tokens between requests and responses
typedef struct
{
    oscore_requestid_t request_id;
    bool free;
    bool observe;
    uint8_t token_len;
    uint8_t token[COAP_TOKEN_MAX_LEN];
} request_matcher_t;

typedef enum
{
    UP,
    DOWN,
} matcher_dir_t;

static request_matcher_t up_matchers[MAX_PARALLEL_SESSION] = {0};
static request_matcher_t down_matchers[MAX_PARALLEL_SESSION] = {0};

static void init_matchers(void)
{
    for (size_t i = 0; i < MAX_PARALLEL_SESSION; i++)
    {
        up_matchers[i].free = true;
        up_matchers[i].observe = false;
        down_matchers[i].free = true;
        down_matchers[i].observe = false;
    }
}

static bool is_observe_packet(const coap_pdu_t *pdu)
{
    coap_opt_iterator_t iter;
    if (coap_option_iterator_init(pdu, &iter, COAP_OPT_ALL))
    {
        if (coap_check_option((coap_pdu_t *)pdu, COAP_OPTION_OBSERVE, &iter) !=
            NULL)
            return true;
    }
    return false;
}

// Matches a new token with a oscore request id
static coap_oscore_res_t reserve_free_matcher(uint8_t *token, uint8_t token_len,
                                              oscore_requestid_t **request_id,
                                              matcher_dir_t direction,
                                              bool is_observe)
{
    request_matcher_t *matchers;
    if (direction == UP)
        matchers = &(up_matchers[0]);
    else
        matchers = &(down_matchers[0]);

    int free_idx = -1;
    for (size_t i = 0; i < MAX_PARALLEL_SESSION; i++)
    {
        if (token_len == matchers[i].token_len &&
            !memcmp(matchers[i].token, token, matchers[i].token_len) &&
            !matchers[i].free)
        {
            *request_id = &matchers[free_idx].request_id;
            return CO_SUCCESS;
        }
        if (matchers[i].free)
        {
            free_idx = i;
        }
    }
    if (free_idx != -1)
    {
        // In our case packets with token_len == 0 are always RESET message,
        // they dont expect a response so we dont bother reserving a matcher for
        // that
        if (token_len != 0)
        {
            matchers[free_idx].free = false;
            memcpy(matchers[free_idx].token, token, token_len);
            matchers[free_idx].token_len = token_len;
        }
        *request_id = &(matchers[free_idx].request_id);
        return CO_SUCCESS;
    }
    return CO_MAX_SESSION_REACHED;
}

// Checks if token has already been used, if yes sets request_id to the
// corresponding oscore request id
static coap_oscore_res_t do_match(uint8_t *token, uint8_t token_len,
                                  oscore_requestid_t **request_id,
                                  matcher_dir_t direction, bool is_observe)
{
    request_matcher_t *matchers;
    if (direction == UP)
        matchers = &(down_matchers[0]);
    else
        matchers = &(up_matchers[0]);

    for (size_t i = 0; i < MAX_PARALLEL_SESSION; i++)
    {
        if (token_len == matchers[i].token_len &&
            !memcmp(matchers[i].token, token, matchers[i].token_len) &&
            !matchers[i].free)
        {
            if (!is_observe)
                matchers[i].free = true;
            *request_id = &(matchers[i].request_id);
            return CO_SUCCESS;
        }
    }
    return CO_UNKNOWN_SESSION;
}

// Some basic cryptographic informations can be generated with :
// full-sdk-delivery/libs/liboscore/tests/riot-tests/plugtest-server/oscore-key-derivation
static struct oscore_context_primitive_immutables key;
static struct oscore_context_primitive primitive = {
    .immutables = &key,
};
static oscore_context_t secctx = {
    .type = OSCORE_CONTEXT_PRIMITIVE,
    .data = (void *)(&primitive),
};

bool set_oscore_security_context(const char *role)
{
    if (!memcmp(role, "client", sizeof("client")))
    {
        struct oscore_context_primitive_immutables _key = {
            .common_iv = COMMON_IV,
            .sender_id_len = 1,
            .sender_id = SENDER_ID,
            .sender_key = SENDER_KEY,
            .recipient_id_len = 1,
            .recipient_id = RECIPIENT_ID,
            .recipient_key = RECIPIENT_KEY,
        };
        memcpy(&key, &_key, sizeof(struct oscore_context_primitive_immutables));
    }
    else
    {
        struct oscore_context_primitive_immutables _key = {
            .common_iv = COMMON_IV,
            .sender_id_len = 1,
            .sender_id = RECIPIENT_ID,
            .sender_key = RECIPIENT_KEY,
            .recipient_id_len = 1,
            .recipient_id = SENDER_ID,
            .recipient_key = SENDER_KEY,
        };
        memcpy(&key, &_key, sizeof(struct oscore_context_primitive_immutables));
    }
    init_matchers();

    return !oscore_cryptoerr_is_error(
        oscore_crypto_aead_from_number(&key.aeadalg, 24));
}

coap_oscore_res_t coap_to_oscore(const coap_pdu_t *to_protect, coap_pdu_t **out)
{
    oscore_msg_protected_t protected;
    oscore_buf_t oscbuf = OSCORE_BUF_INIT;
    oscore_msg_native_t protect = {
        .pkt = coap_pdu_init(to_protect->type, to_protect->code, to_protect->tid,
                             COAP_PDU_MAX_SIZE),
        .oscbuf = &oscbuf,
    };

    bool is_observe = is_observe_packet(to_protect);

    enum oscore_prepare_result prep_res;
    oscore_requestid_t *request_id;
    coap_oscore_res_t res = do_match(to_protect->token, to_protect->token_length,
                                     &request_id, UP, is_observe);
    // If matching did not find any corresponding oscore_requestid_t it means
    // we're in a new Request-Response session and we're going to send a Request.
    if (res == CO_UNKNOWN_SESSION)
    {
        res = reserve_free_matcher(to_protect->token, to_protect->token_length,
                                   &request_id, UP, is_observe);
        if (res != CO_SUCCESS)
            return res;
        prep_res = oscore_prepare_request(protect, &protected, &secctx, request_id);

        // We have to set the code manually here in order to respect the RFC
        // liboscore doesnt implement the different coap code for observe messages
        // We have to do this modification here because we can't change the CoAP
        // code after we have called coap_pdu_encode_header
        if (is_observe)
            oscore_msg_native_set_code(protect, 0x05); // 0.05 Fetch
    }
    // If matching did found a corresponding oscore_requestid_t it means
    // we're going to send a Response to answer a Request
    else
    {
        prep_res =
            oscore_prepare_response(protect, &protected, &secctx, request_id);
        if (is_observe)
            oscore_msg_native_set_code(protect, 0x45); // 2.05 Content
    }

    if (prep_res != OSCORE_PREPARE_OK)
        return CO_OSCORE_ERROR;

    if (!coap_add_token(protect.pkt, to_protect->token_length, to_protect->token))
        return CO_COAP_ERROR;

    if (!coap_pdu_encode_header(protect.pkt, COAP_PROTO_UDP))
        return CO_COAP_ERROR;

    coap_opt_t *opt = NULL;
    oscore_msgerr_protected_t oscerr;

    oscore_msg_protected_set_code(&protected, to_protect->code);
protected
    .backend.oscbuf->avail -= 1;

    coap_opt_iterator_t iter;
    if (coap_option_iterator_init(to_protect, &iter, COAP_OPT_ALL))
    {
        while ((opt = coap_option_next(&iter)) != NULL)
        {
            uint16_t opt_len = coap_opt_length(opt);
            uint16_t opt_delta = iter.type;
            const uint8_t *opt_value = coap_opt_value(opt);

            if (oscore_msg_protected_append_option(&protected, opt_delta, opt_value,
                                                   opt_len) != OK)
                return CO_OSCORE_ERROR;

            opt_len += 1;
            uint8_t len = *opt & 0x0f;
            if (len == COAP_EXTENDED_1_BYTE)
                opt_len += 1;
            else if (len == COAP_EXTENDED_2_BYTE)
                opt_len += 2;
            uint8_t delta = (*opt++ & 0xf0) >> 4;
            if (delta == COAP_EXTENDED_1_BYTE)
                opt_len += 1;
            else if (delta == COAP_EXTENDED_2_BYTE)
                opt_len += 2;

        protected
            .backend.oscbuf->avail -= opt_len;
        }
    }

    size_t from_payload_len, to_payload_len = 0;
    uint8_t *from_payload, *to_payload = NULL;

    coap_get_data(to_protect, &from_payload_len, &from_payload);
    oscore_msg_protected_map_payload(&protected, &to_payload, &to_payload_len);

    if (from_payload_len != 0)
    {
        memcpy(to_payload, from_payload, from_payload_len);
    protected
        .backend.oscbuf->avail -= from_payload_len + 1;
    }

    if (oscore_msg_protected_trim_payload(&protected, from_payload_len) != OK)
        return CO_OSCORE_ERROR;

    if (inner_compression)
    {
        // Apply inner compression to the OSCORE Plaintext.
        if (oscore_msg_inner_compression(&protected) != CO_SUCCESS)
        {
            return CO_OSCORE_ERROR;
        }
    }

    oscore_msg_native_t write_out;
    oscerr = oscore_encrypt_message(&protected, &write_out);
    if (oscerr != OSCORE_FINISH_OK)
        return CO_OSCORE_ERROR;

    oscore_msg_native_map_payload(protected.backend, &to_payload,
                                  &to_payload_len);
    coap_add_data(protected.backend.pkt, to_payload_len, to_payload);

    *out = write_out.pkt;

    return CO_SUCCESS;
}

coap_oscore_res_t oscore_to_coap(coap_pdu_t *to_decrypt, coap_pdu_t **out)
{
    const uint8_t *oscore_hdr = NULL;
    uint16_t oscore_hdr_len = 0;
    coap_opt_iterator_t iter;
    coap_opt_t *opt;

    if (!coap_option_iterator_init(to_decrypt, &iter, COAP_OPT_ALL))
        return CO_COAP_ERROR;
    while ((opt = coap_option_next(&iter)) != NULL)
    {
        if (iter.type != COAP_OSCORE_OPTION)
            continue;
        oscore_hdr_len = coap_opt_length(opt);
        oscore_hdr = coap_opt_value(opt);
        break;
    }
    if (opt == NULL)
        return CO_COAP_ERROR;

    oscore_oscoreoption_t oscore_opt;
    if (!oscore_oscoreoption_parse(&oscore_opt, oscore_hdr, oscore_hdr_len))
        return CO_OSCORE_ERROR;

    oscore_buf_t oscbuf = OSCORE_BUF_INIT;
    oscore_msg_native_t nat_decrypt = {.oscbuf = &oscbuf, .pkt = to_decrypt};
    uint8_t *oscore_payload;
    size_t oscore_payload_len;
    if (!coap_get_data(to_decrypt, &oscore_payload_len, &oscore_payload))
        return CO_COAP_ERROR;
    if (oscore_msg_native_trim_payload(nat_decrypt, oscore_payload_len))
        return CO_OSCORE_ERROR;
    memcpy(oscbuf.buf, oscore_payload, oscore_payload_len);

    oscore_msg_protected_t decrypted;
    int unprotect_res;
    oscore_requestid_t *request_id;
    coap_oscore_res_t res =
        do_match(to_decrypt->token, to_decrypt->token_length, &request_id, DOWN,
                 is_observe_packet(to_decrypt));
    // If matching did not find any corresponding oscore_requestid_t it means we
    // just received a Request belonging to a new Request-Response session
    if (res == CO_UNKNOWN_SESSION)
    {
        res =
            reserve_free_matcher(to_decrypt->token, to_decrypt->token_length,
                                 &request_id, DOWN, is_observe_packet(to_decrypt));
        if (res != CO_SUCCESS)
            return res;
        unprotect_res = oscore_unprotect_request(nat_decrypt, &decrypted,
                                                 oscore_opt, &secctx, request_id);
    }
    // If matching did found a corresponding oscore_requestid_t it means
    // we just received a Response to a previously sent Request
    else
    {
        unprotect_res = oscore_unprotect_response(nat_decrypt, &decrypted,
                                                  oscore_opt, &secctx, request_id);
    }

    if (unprotect_res)
        return CO_OSCORE_ERROR;

    if (inner_compression)
    {
        // Apply OSCORE inner decompression to the decrypted message.
        if (oscore_msg_inner_decompression(&decrypted) != CO_SUCCESS)
        {
            return CO_OSCORE_ERROR;
        }
    }

    coap_pdu_t *write_out =
        coap_pdu_init(to_decrypt->type, oscore_msg_protected_get_code(&decrypted),
                      to_decrypt->tid, COAP_PDU_MAX_SIZE);

    if (!coap_add_token(write_out, to_decrypt->token_length, to_decrypt->token))
        return CO_COAP_ERROR;

    if (!coap_pdu_encode_header(write_out, COAP_PROTO_UDP))
        return CO_COAP_ERROR;

    oscore_msg_protected_optiter_t p_iter;
    oscore_msg_protected_optiter_init(&decrypted, &p_iter);

    uint16_t opt_num;
    const uint8_t *opt_val;
    size_t opt_len;

    while (oscore_msg_protected_optiter_next(&decrypted, &p_iter, &opt_num,
                                             &opt_val, &opt_len))
    {
        if (!coap_add_option(write_out, opt_num, opt_len, opt_val))
            return CO_COAP_ERROR;
    }

    uint8_t *from_payload;
    size_t from_payload_len;
    oscore_msg_protected_map_payload(&decrypted, &from_payload,
                                     &from_payload_len);
    if (!coap_add_data(write_out, from_payload_len, from_payload))
        return CO_COAP_ERROR;

    *out = write_out;

    return CO_SUCCESS;
}

bool process_uplink_packet(uint8_t *packet, size_t *packet_size)
{
    coap_pdu_t *pdu = coap_pdu_init(
        COAP_MESSAGE_CON, 0, 0,
        *packet_size - (sizeof(struct ip6_hdr) + sizeof(struct udphdr)));
    if (!coap_pdu_parse(
            COAP_PROTO_UDP,
            packet + (sizeof(struct ip6_hdr) + sizeof(struct udphdr)),
            *packet_size - (sizeof(struct ip6_hdr) + sizeof(struct udphdr)), pdu))
    {
        printf("Error : failed to parse a CoAP packet\n");
        return false;
    }
    printf("------- coap part -------\n");
    coap_dump(pdu);

    coap_pdu_t *oscore;
    coap_oscore_res_t res = coap_to_oscore(pdu, &oscore);
    coap_delete_pdu(pdu);
    if (res != CO_SUCCESS)
    {
        printf("Error : failed to transform a CoAP packet to OSCORE\n");
        return false;
    }
    printf("------- oscore encrypted -------\n");
    coap_dump(oscore);

    *packet_size = get_coap_packet_len(oscore) + sizeof(struct ip6_hdr) +
                   sizeof(struct udphdr);
    memcpy(packet + sizeof(struct ip6_hdr) + sizeof(struct udphdr),
           get_coap_packet_bytes(oscore), get_coap_packet_len(oscore));
    coap_delete_pdu(oscore);
    return true;
}

bool process_downlink_packet(uint8_t *packet, size_t *packet_size, uint16_t data_size)
{
    coap_pdu_t *oscore = coap_pdu_init(COAP_MESSAGE_CON, 0, 0, data_size);
    if (!coap_pdu_parse(
            COAP_PROTO_UDP,
            packet + (sizeof(struct ip6_hdr) + sizeof(struct udphdr)),
            data_size - (sizeof(struct ip6_hdr) + sizeof(struct udphdr)), oscore))
    {
        printf("Error : failed to parse an OSCORE packet\n");
        return false;
    }
    printf("------- OSCORE part -------\n");
    coap_dump(oscore);

    coap_pdu_t *coap;
    coap_oscore_res_t res = oscore_to_coap(oscore, &coap);
    coap_delete_pdu(oscore);
    if (res != CO_SUCCESS)
    {
        printf("Error : failed to transform an OSCORE packet to CoAP\n");
        return false;
    }
    printf("-------- CoAP decrypted --------\n");
    coap_dump(coap);

    *packet_size = get_coap_packet_len(coap) + sizeof(struct ip6_hdr) +
                   sizeof(struct udphdr);
    memcpy((void *)(packet + sizeof(struct ip6_hdr) + sizeof(struct udphdr)),
           get_coap_packet_bytes(coap), get_coap_packet_len(coap));
    update_ip6_udp_len(packet, get_coap_packet_len(coap));
    update_udp_checksum(packet);
    coap_delete_pdu(coap);
    return true;
}