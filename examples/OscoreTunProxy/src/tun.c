/**
 * @file tun.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <arpa/inet.h>
#include <errno.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>

#include "fullsdknet.h"
#include "libcoap.h"
#include "pdu.h"
#include "utils.h"
#include "platform.h"
#include "sdk.h"
#include "tun.h"
#include "udp.h"
#include "app_globals.h"

#ifdef LOGS
#include "logs.h"
#endif
#include "common.h"
#define L2A_BYPASS_BUFF_SIZE 512

// Name of the TUN interface to connect to
char tun_name[IFNAMSIZ] = {0};

static int tun_fd;

static bool close_connection = false;
static bool tun_input_available = false;

int sockfd;
bool downlink_available;
uint8_t l2a_bypass_buff[L2A_BYPASS_BUFF_SIZE] = {0};

void enable_schc_compression(bool comp)
{
  schc_compression = comp;
}

static void _downlink_available_callback_l2a_bypass(void)
{
  downlink_available = true;
}

bool send_data_l2a_bypass(const uint8_t *data, uint16_t data_size)
{
  ssize_t ret;

  do
  {
    ret = send(sockfd, data, data_size, 0);
  } while (ret == -1 && errno == EINTR);
  return ret != -1;
}

bool read_downlink_l2a_bypass(void)
{
  ssize_t ret;

  do
  {
    ret = recv(sockfd, l2a_bypass_buff, L2A_BYPASS_BUFF_SIZE, 0);
  } while (ret == -1 && errno == EINTR);
  if (ret == -1)
    return false;
  if (!schc_compression) {
    rx_buf_size = ret;
    memcpy(rx_buf, l2a_bypass_buff, ret);
  }
  if (is_edhoc_done) {
    process_downlink(l2a_bypass_buff, ret);
  }
  return true;
}

bool setup_l2a_bypass(const char **av)
{
  struct sockaddr_in srcaddr = {};
  struct sockaddr_in dstaddr = {};

  disable_inner_compression();
  if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    return false;
  srcaddr.sin_family = AF_INET;
  srcaddr.sin_addr.s_addr = inet_addr(av[3]);
  srcaddr.sin_port = htons(atoi(av[4]));

  if (bind(sockfd, (struct sockaddr *)&srcaddr, sizeof(srcaddr)) < 0)
    return false;

  dstaddr.sin_family = AF_INET;
  dstaddr.sin_addr.s_addr = inet_addr(av[5]);
  dstaddr.sin_port = htons(atoi(av[6]));
  if (connect(sockfd, (struct sockaddr *)&dstaddr, sizeof(dstaddr)) < 0)
    return false;
  if (!watch_fd_for_input(sockfd, &_downlink_available_callback_l2a_bypass))
    return false;
  return true;
}

static bool process_app_l2a_bypass(int tun_fd)
{
  if (downlink_available)
  {
    downlink_available = false;
    if (!read_downlink_l2a_bypass())
      printf("Error : read_downlink_l2a_bypass() failed\n");
  }
  if (!wait_for_event(tun_fd))
    return false;
  return true;
}

void tun_close_connection()
{
  close_connection = true;
}

static void sigint_handler(int sig)
{
  if (sig == SIGINT)
    tun_close_connection();
}

// Catch signal in order to close gracefully
static void set_siginthandler()
{
  struct sigaction sigact = {};

  sigact.sa_handler = &sigint_handler;
  sigact.sa_flags = SA_RESETHAND;
  sigaction(SIGINT, &sigact, NULL);
}

void set_tun_name(const char *name)
{
  strncpy(tun_name, name, IFNAMSIZ);
}

static int tun_create()
{
  struct ifreq ifr;
  int fd;

  if ((fd = open(TUN_FILE, O_RDWR)) < 0)
    return -1;
  memset(&ifr, 0, sizeof(ifr));
  // IFF_TUN means we want a TUN interface
  // IFF_NO_PI means we dont want packet information (adds a 4 bytes header)
  ifr.ifr_flags = IFF_TUN | IFF_NO_PI;
  strncpy(ifr.ifr_name, tun_name, IFNAMSIZ);
  // Ask ioctl to bind fd to a TUN interface with the name and flags previously
  // set
  if (ioctl(fd, TUNSETIFF, (void *)&ifr) < 0)
  {
    printf("Failed to create the TUN interface : %s\n", strerror(errno));
    close(fd);
    return -1;
  }

  return fd;
}

static bool discard_not_udp(uint8_t *packet)
{
  struct ip6_hdr *ip = (struct ip6_hdr *)packet;
  return ip->ip6_nxt != IPPROTO_UDP;
}

static bool process_uplink()
{
  uint8_t packet[MAX_PACKET_SIZE_BYTES] = {0};
  size_t packet_size = 0;

  if (!tun_input_available || !get_can_send())
    return true;
  tun_input_available = false;
  do
  {
    packet_size = read(tun_fd, packet, sizeof(packet));
  } while ((int)packet_size == -1 && errno == EINTR);
  if ((int)packet_size == -1)
  {
    printf("Error : read on TUN fd failed : %s\n", strerror(errno));
    return false;
  }

  if (discard_not_udp(packet))
  {
    printf("Discarded a non-UDP\n");
    return true;
  }

  printf("--------packet to send--------\n");
  dump_buf(packet, packet_size);

  if (!process_uplink_packet(packet, &packet_size))
    return false;

  printf("--------final packet--------\n");
  dump_buf(packet, packet_size);
  printf("\n");

  if (schc_compression)
  {
    set_cannot_send();
    net_status_t status = net_sendto(packet, packet_size);
    if (status != NET_SUCCESS)
    {
      // No need to kill the SDK when we get an error.
      // Later we could return specific error code to take a better decision
      printf("Error : net_sendto() failed %d\n", status);
    }
  }
  else
  {
    update_ip6_udp_len(
        (char*) packet, packet_size - (sizeof(struct ip6_hdr) + sizeof(struct udphdr)));
    update_udp_checksum((char*) packet);

    if (!send_data_l2a_bypass(packet, packet_size))
      printf("Error : send_data_l2a_bypass() failed\n");
  }
  return true;
}

bool process_downlink(const uint8_t *buff, uint16_t data_size)
{
  int nwrite = 0;
  uint8_t packet[MAX_PACKET_SIZE_BYTES] = {0};
  size_t packet_size = 0;

  if (data_size > MAX_PACKET_SIZE_BYTES)
    return false;
  memcpy(packet, buff, data_size);

  printf("--------received data---------\n");
  dump_buf(packet, data_size);

  if (!process_downlink_packet(packet, &packet_size, data_size))
    return false;

  printf("--------final data--------\n");
  dump_buf(packet, packet_size);
  printf("\n");

  do
  {
    nwrite = write(tun_fd, packet, packet_size);
  } while (nwrite == -1 && errno == EINTR);
  if (nwrite == -1)
  {
    printf("Error : write on TUN failed : %s\n", strerror(errno));
    return false;
  }
  if (nwrite != (int) packet_size)
  {
    printf("Warning: Message truncated, only %d/%ld bytes written\n", nwrite,
           packet_size);
    return false;
  }
  return true;
}

bool tun_listen()
{
  if ((tun_fd = tun_create()) < 0)
    return false;
  set_siginthandler();
  while (!close_connection)
  {
    if (schc_compression)
    {
      if (!process_app(tun_fd))
        break;
    }
    else if (!process_app_l2a_bypass(tun_fd))
      break;
    process_uplink();
  }
  close(tun_fd);
  return close_connection;
}

static void cb_tun_input_available(void)
{
  tun_input_available = true;
}

bool wait_for_event(int tun_fd)
{
  if (!watch_fd_for_input(tun_fd, &cb_tun_input_available))
  {
    printf("Error : watch_fd_for_input() failed\n");
    return false;
  }
  platform_enter_low_power_ll();
  if (!unwatch_fd_for_input(tun_fd))
  {
    printf("Error : unwatch_fd_for_input() failed\n");
    return false;
  }
  return true;
}
