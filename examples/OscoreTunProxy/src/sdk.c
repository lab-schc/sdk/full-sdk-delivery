/**
 * @file sdk.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Thibaut Artis thibaut.artis@ackl.io
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <netinet/ip.h>
#include <arpa/inet.h>

#include "fullsdkextapi.h"
#include "fullsdkfragapi.h"
#include "fullsdkl2.h"
#include "fullsdkmgt.h"
#include "fullsdknet.h"
#include "platform.h"
#include "sdk.h"
#include "tun.h"
#include "app_globals.h"

#ifdef PATTERN_COMP
#include "pattern_compression.h"

const char *pattern_comp_json_filename;
#endif

bool mgt_process_request = false;

sdk_mode_t sdk_mode;

static bool transmission_result = true;
static net_status_t transmission_status = 0;
static uint16_t transmission_error = 0;

static bool data_received = false;
static net_status_t data_rx_status = 0;
static uint16_t data_rx_size = 0;

// Assume (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) must be 4-bytes aligned
#define MAX_PAYLOAD_SIZE 8000
#define MAX_MTU_SIZE 256 // Assume MAX_MTU_SIZE must be 4-bytes aligned
#define IP_PORT_LENGTH_BYTES 2

// Memory block size provided to mgt_initialize.
#define MEM_BLOCK_SIZE                                                         \
  (MAX_MTU_SIZE * 4u + (MAX_PAYLOAD_SIZE + MGT_PROTO_SIZE) * 4u +              \
   MGT_SCHC_ACK_PACKET_SIZE * 3u + MGT_SYNC_PACKET_SIZE * 2u)

static uint8_t mgt_mem_block[MEM_BLOCK_SIZE] = {0};

uint8_t rx_buf[RECEIVE_BUFFER_SIZE] = {0};
uint16_t rx_buf_size = 0;

static TimerEvent_t sdk_timers[3];

sem_t sdk_mgt_process_sem;
sem_t data_sent_sem;
sem_t data_rcvd_sem;

// MGT callbacks
static void cb_mgt_processing_required(void)
{
  mgt_process_request = true;
  sem_post(&sdk_mgt_process_sem);
}

static void cb_mgt_connectivity_state(mgt_status_t state)
{
  if (state != MGT_SUCCESS)
    tun_close_connection();
}

static void cb_start_timer(uint8_t id, uint32_t duration)
{
  TimerSetValue(&sdk_timers[id], duration);
  TimerStart(&sdk_timers[id]);
}

static void cb_stop_timer(uint8_t id)
{
  TimerStop(&sdk_timers[id]);
}

static void sdk_timer_1_event(void *context)
{
  (void)context; /* unused */
  mgt_timer_timeout(0);
}

static void sdk_timer_2_event(void *context)
{
  (void)context; /* unused */
  mgt_timer_timeout(1);
}

static void sdk_timer_3_event(void *context)
{
  (void)context; /* unused */
  mgt_timer_timeout(2);
}

mgt_callbacks_t mgt_callbacks = {
    cb_mgt_processing_required,
    cb_mgt_connectivity_state,
    cb_start_timer,
    cb_stop_timer,
    NULL,
};

// net callbacks
net_callbacks_t net_callbacks = {tun_net_transmission_result,
                                 tun_net_data_received};

// Computing the internet checksum (RFC 1071).
// Note that the internet checksum is not guaranteed to preclude collisions.
static uint16_t checksum(uint16_t *addr, int len)
{

  int count = len;
  register uint32_t sum = 0;
  uint16_t answer = 0;

  // Sum up 2-byte values until none or only one byte left.
  while (count > 1)
  {
    sum += *(addr++);
    count -= 2;
  }

  // Add left-over byte, if any.
  if (count > 0)
  {
    sum += *(uint8_t *)addr;
  }

  // Fold 32-bit sum into 16 bits; we lose information by doing this,
  // increasing the chances of a collision.
  // sum = (lower 16 bits) + (upper 16 bits shifted right 16 bits)
  while (sum >> 16)
  {
    sum = (sum & 0xffff) + (sum >> 16);
  }

  // Checksum is one's compliment of sum.
  answer = ~sum;

  return answer;
}

// Build IPv6 UDP pseudo-header and call checksum function (Section 8.1 of RFC 2460).
static uint16_t udp6_checksum(struct ip6_hdr iphdr, struct udphdr udphdr, uint8_t *payload, uint16_t payloadlen)
{

  char buf[IP_MAXPACKET];
  char *ptr;
  int chksumlen = 0;
  int i;

  ptr = &buf[0]; // ptr points to beginning of buffer buf

  // Copy source IP address into buf (128 bits)
  memcpy(ptr, &iphdr.ip6_src.s6_addr, sizeof(iphdr.ip6_src.s6_addr));
  ptr += sizeof(iphdr.ip6_src.s6_addr);
  chksumlen += sizeof(iphdr.ip6_src.s6_addr);

  // Copy destination IP address into buf (128 bits)
  memcpy(ptr, &iphdr.ip6_dst.s6_addr, sizeof(iphdr.ip6_dst.s6_addr));
  ptr += sizeof(iphdr.ip6_dst.s6_addr);
  chksumlen += sizeof(iphdr.ip6_dst.s6_addr);

  // Copy UDP length into buf (32 bits)
  memcpy(ptr, &udphdr.len, sizeof(udphdr.len));
  ptr += sizeof(udphdr.len);
  chksumlen += sizeof(udphdr.len);

  // Copy zero field to buf (24 bits)
  *ptr = 0;
  ptr++;
  *ptr = 0;
  ptr++;
  *ptr = 0;
  ptr++;
  chksumlen += 3;

  // Copy next header field to buf (8 bits)
  memcpy(ptr, &iphdr.ip6_nxt, sizeof(iphdr.ip6_nxt));
  ptr += sizeof(iphdr.ip6_nxt);
  chksumlen += sizeof(iphdr.ip6_nxt);

  // Copy UDP source port to buf (16 bits)
  memcpy(ptr, &udphdr.source, sizeof(udphdr.source));
  ptr += sizeof(udphdr.source);
  chksumlen += sizeof(udphdr.source);

  // Copy UDP destination port to buf (16 bits)
  memcpy(ptr, &udphdr.dest, sizeof(udphdr.dest));
  ptr += sizeof(udphdr.dest);
  chksumlen += sizeof(udphdr.dest);

  // Copy UDP length again to buf (16 bits)
  memcpy(ptr, &udphdr.len, sizeof(udphdr.len));
  ptr += sizeof(udphdr.len);
  chksumlen += sizeof(udphdr.len);

  // Copy UDP checksum to buf (16 bits)
  // Zero, since we don't know it yet
  *ptr = 0;
  ptr++;
  *ptr = 0;
  ptr++;
  chksumlen += 2;

  // Copy payload to buf
  memcpy(ptr, payload, payloadlen * sizeof(uint8_t));
  ptr += payloadlen;
  chksumlen += payloadlen;

  // Pad to the next 16-bit boundary
  for (i = 0; i < payloadlen % 2; i++, ptr++)
  {
    *ptr = 0;
    ptr++;
    chksumlen++;
  }

  return checksum((uint16_t *)buf, chksumlen);
}

bool generate_udp_packet(uint8_t *payload, uint16_t payload_size, uint8_t *packet_out, uint16_t *packet_out_size)
{
  struct ip6_hdr ipv6_hdr;
  struct udphdr udp_hdr;

  ipv6_hdr.ip6_flow = htonl((6 << 28) | (0 << 20) | 0);

  // Payload length (16 bits): UDP header + UDP data
  ipv6_hdr.ip6_plen = htons(sizeof(struct udphdr) + payload_size);

  // Next header (8 bits): 17 for UDP
  ipv6_hdr.ip6_nxt = IPPROTO_UDP;

  // Hop limit (8 bits): default to maximum value
  ipv6_hdr.ip6_hops = 255;

  // Source IPv6 address (128 bits)
  if ((inet_pton(AF_INET6, host_ip, &(ipv6_hdr.ip6_src))) != 1)
  {
    return false;
  }

  // Destination IPv6 address (128 bits)
  if ((inet_pton(AF_INET6, remote_ip, &(ipv6_hdr.ip6_dst))) != 1)
  {
    return false;
  }

  // UDP header

  // Source port number (16 bits): pick a number
  udp_hdr.source = htons(atoi(host_port));

  // Destination port number (16 bits): pick a number
  udp_hdr.dest = htons(atoi(remote_port));

  // Length of UDP datagram (16 bits): UDP header + UDP data
  udp_hdr.len = htons(sizeof(struct udphdr) + payload_size);

  //UDP checksum (16 bits)
  udp_hdr.check = udp6_checksum(ipv6_hdr, udp_hdr, payload, payload_size);
  if (!memcpy(packet_out, &ipv6_hdr, sizeof(struct ip6_hdr)))
    return false;
  if (!memcpy(packet_out + sizeof(struct ip6_hdr), &udp_hdr, sizeof(struct udphdr)))
    return false;
  if (!memcpy(packet_out + sizeof(struct ip6_hdr) + sizeof(struct udphdr), payload, payload_size))
    return false;
  *packet_out_size = sizeof(struct ip6_hdr) + sizeof(struct udphdr) + payload_size;
  return true;
}

bool sdk_init(const char **av)
{
  platform_hw_init();
  platform_configure_sleep_mode();

  //init the needed semaphores in order to make Edhoc (blocking) works with the SDK (non-blocking)
  sem_init(&data_rcvd_sem, 0, 0);
  sem_init(&sdk_mgt_process_sem, 0, 0);
  sem_init(&data_sent_sem, 0, 0);

  TimerInit(&sdk_timers[0], sdk_timer_1_event);
  TimerInit(&sdk_timers[1], sdk_timer_2_event);
  TimerInit(&sdk_timers[2], sdk_timer_3_event);

  sdk_mode = !memcmp(av[2], "client", sizeof("client")) ? SDK_DEVICE_MODE : SDK_APP_MODE;
  mgt_set_mode(sdk_mode);
  l2_set_ipv4_host_addr(av[3]);
  l2_set_udp_src_port(av[4]);
  l2_set_ipv4_remote_addr(av[5]);
  l2_set_udp_dest_port(av[6]);

  net_set_host_static_ip(av[7]);
  net_set_host_static_port(av[8]);
  net_set_remote_static_ip(av[9]);
  net_set_remote_static_port(av[10]);

  // should be more than 250 for RESPONDER_TEST_3
  // Configure L2 MTU.
  l2_set_mtu(255);

  printf("FullSDK version: %s\n", mgt_get_version());

  if (mgt_initialize(&mgt_callbacks, mgt_mem_block, MEM_BLOCK_SIZE,
                     MAX_MTU_SIZE, MAX_PAYLOAD_SIZE) != MGT_SUCCESS)
  {
    printf("Error : mgt_initialize() failed\n");
    return false;
  }

  net_status_t status = net_initialize(&net_callbacks);
  if (status != NET_SUCCESS)
  {
    printf("Error : net_initialize field %d\n", status);
    return false;
  }

#ifdef PATTERN_COMP
  pattern_comp_json_filename = av[12];
  pattern_tables = pattern_compression_init(&nb_pattern_table);
  if (!pattern_tables)
  {
    printf("Error: pattern_compression_init\n");
    return false;
  }
#endif

  return true;
}

void tun_net_transmission_result(net_status_t status, uint16_t error)
{
  transmission_status = status;
  transmission_error = error;
  transmission_result = true;
  //edhoc
  sem_post(&data_sent_sem);
}

void tun_net_data_received(const uint8_t *buff, uint16_t data_size,
                           net_status_t status)
{
  data_rx_size = data_size;
  rx_buf_size = data_size;
  data_rx_status = status;
  data_received = true;

  if (status != NET_SUCCESS)
    return;

  uint8_t b;
  for (uint16_t i = 0; i < data_size; i++)
  {
    b = buff[i];
    rx_buf[i] = buff[i];
    printf("%02X ", b);
    if ((i + 1) % 10 == 0)
      printf("\n\r");
  }
  printf("\n\r");
  sem_post(&data_rcvd_sem);

  //OSCORE stuff
  if (is_edhoc_done)
    if (!process_downlink(buff, data_size))
      status = NET_ERROR;
}

bool get_can_send(void)
{
  return transmission_result;
}

void set_cannot_send(void)
{
  transmission_result = false;
}

bool process_app(int tun_fd)
{
  if (mgt_process_request)
  {
    mgt_process_request = false;
    mgt_status_t st;
    if ((st = mgt_process()) != MGT_SUCCESS)
    {
      printf("Error : mgt_process() failed: %d\n", st);
      return false;
    }
  }
  if (!wait_for_event(tun_fd))
    return false;
  return true;
}

void *thread_listen(void *vargp)
{
  (void)vargp; /* unused */

  while (true)
  {
    if (is_edhoc_done)
      break;
    platform_enter_low_power_ll();
  }
  return NULL;
}

void *thread_sdk(void *vargp)
{
  (void)vargp; /* unused */

  while (true)
  {
    if (is_edhoc_done)
      break;
    if (sem_wait(&sdk_mgt_process_sem) < 0)
    {
      printf("sdk_mgt_process_sem failed\n");
    }

    mgt_status_t mgt_status = mgt_process();
    if (mgt_status != MGT_SUCCESS)
    {
      printf("mgt_process failed: %d\n", mgt_status);
    }
  }
  return NULL;
}