/**
 * @file oscore.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Thibaut Artis thibaut.artis@ackl.io
 *
 * libcoap light integration implementation.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fullsdkextapi.h"
#include "fullsdkmgt.h"
#include "utils.h"
#include "coap_debug.h"
#include "pdu.h"

void dump_buf(uint8_t *buf, uint16_t len)
{
  for (size_t i = 0; i < len; i++)
  {
    printf("%.2X ", buf[i]);
  }
  printf("\n");
}

uint8_t *get_coap_packet_bytes(coap_pdu_t *pdu)
{
  return pdu->token - pdu->hdr_size;
}

size_t get_coap_packet_len(coap_pdu_t *pdu)
{
  return pdu->used_size + pdu->hdr_size;
}

void coap_dump(coap_pdu_t *pdu)
{
  uint8_t *p = get_coap_packet_bytes(pdu);
  uint16_t len = get_coap_packet_len(pdu);
  dump_buf(p, len);
}
