/**
 * @file uoscore.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#include "libcoap.h"
#include "oscore.h"
#include "uoscore.h"
#include "common.h"

static uint8_t *SENDER_ID = NULL;
static uint8_t SENDER_ID_LEN = 0;

static uint8_t RECIPIENT_ID[1] = {0x01};
static uint8_t RECIPIENT_ID_LEN = sizeof(RECIPIENT_ID);

static uint8_t *ID_CONTEXT = NULL;
static uint8_t ID_CONTEXT_LEN = 0;

#ifdef REGULAR_COMP
static bool
oscore_msg_inner_compression(uint8_t *plaintext, uint32_t *plaintext_size)
{
    printf("------------- Plaintext ------------\n");
    dump_buf(plaintext, *plaintext_size);

    uint8_t out_inner_comp_buf[OSCORE_MAX_PAYLOAD_SIZE] = {0};
    uint16_t out_data_size;
    mgt_status_t status = mgt_ext_oscore_inner_compression(
        out_inner_comp_buf, sizeof(out_inner_comp_buf), &out_data_size,
        plaintext, *plaintext_size);
    if (status != MGT_SUCCESS)
    {
        printf("Inner compression error in SDK: %d\n", status);
        return false;
    }

    printf("------------- Inner compressed packet ------------\n");
    dump_buf(out_inner_comp_buf, out_data_size);

    // Move the compressed OSCORE Plaintext into the input buffer.
    if (memcpy(plaintext, out_inner_comp_buf, out_data_size) == NULL)
        return false;
    *plaintext_size = out_data_size;
    return true;
}

static bool
oscore_msg_inner_decompression(uint8_t *plaintext, uint32_t *plaintext_size)
{
    printf("------------- Inner compressed packet ------------\n");
    dump_buf(plaintext, *plaintext_size);

    uint8_t out_inner_decomp_buf[OSCORE_MAX_PAYLOAD_SIZE] = {0};
    uint16_t out_data_size;
    mgt_status_t status = mgt_ext_oscore_inner_decompression(
        out_inner_decomp_buf, sizeof(out_inner_decomp_buf), &out_data_size,
        plaintext, *plaintext_size);
    if (status != MGT_SUCCESS)
    {
        printf("Inner decompression error in SDK: %d\n", status);
        return false;
    }

    printf("------------- Plaintext ------------\n");
    dump_buf(out_inner_decomp_buf, out_data_size);

    // Move the OSCORE Plaintext message into the input buffer.
    if (memcpy(plaintext, out_inner_decomp_buf, out_data_size) == NULL)
        return false;
    *plaintext_size = out_data_size;
    return true;
}
#endif

#ifdef PATTERN_COMP

static bool oscore_msg_inner_compression(uint8_t *plaintext, uint32_t *plaintext_size)
{
    printf("------------- Plaintext ------------\n");
    dump_buf(plaintext, *plaintext_size);

    for (uint16_t i = 0; i < nb_pattern_table; i++)
    {
        if (pattern_compression_match(&pattern_tables[i], plaintext, plaintext_size))
        {
            printf("\nPATTERN COMPRESSION MATCHED: %d\n", pattern_tables[i].rule_id);
            return true;
        }
    }
    //add rule_id
    uint8_t out_inner_comp_buf[MAX_PACKET_SIZE_BYTES] = {0};
    uint16_t out_data_size = *plaintext_size + sizeof(uint8_t);

    out_inner_comp_buf[0] = NO_COMPRESSION_RULE_ID;
    if (memcpy(out_inner_comp_buf + sizeof(uint8_t), plaintext, *plaintext_size) == NULL)
        return false;
    printf("NO PATTERN COMPRESSION HAS MATCHED\n");
    printf("------------- Inner NOT Compressed packet ------------\n");
    dump_buf(out_inner_comp_buf, out_data_size);

    // Move the compressed OSCORE Plaintext into the input buffer.
    if (memcpy(plaintext, out_inner_comp_buf, out_data_size) == NULL)
        return false;
    *plaintext_size = out_data_size;
    return true;
}

static bool oscore_msg_inner_decompression(uint8_t *plaintext, uint32_t *plaintext_size)
{
    printf("------------- Inner compressed packet ------------\n");
    dump_buf(plaintext, *plaintext_size);

    if (plaintext[0] == NO_COMPRESSION_RULE_ID)
    {
        //remove rule_id
        uint8_t *tmp = plaintext + sizeof(uint8_t);
        *plaintext_size = *plaintext_size - sizeof(uint8_t);
        memcpy(plaintext, tmp, *plaintext_size);

        printf("------------- Inner NOT DEcompressed packet ------------\n");
        dump_buf(plaintext, *plaintext_size);
    }
    else
    {
        for (uint16_t i = 0; i < nb_pattern_table; i++)
        {
            if (pattern_decompression_match(&pattern_tables[i], plaintext, plaintext_size))
            {
                printf("\nPATTERN DECOMPRESSION MATCHED: %d\n", pattern_tables[i].rule_id);
                return true;
            }
        }
    }
    return true;
}

#endif

inner_t inner_compression = oscore_msg_inner_compression;

void disable_inner_compression(void)
{
    inner_compression = NULL;
}

bool set_oscore_security_context(struct context *c_sender, struct context *c_receiver, uint8_t *oscore_master_secret, uint16_t oscore_master_secret_size, uint8_t *oscore_master_salt, uint16_t oscore_master_salt_size)
{
    /*OSCORE contex initialization*/
    struct oscore_init_params params_sender =
    {
        CLIENT,
        {
            oscore_master_secret_size,
            oscore_master_secret
        },
        {
            SENDER_ID_LEN,
            SENDER_ID
        },
        {
            RECIPIENT_ID_LEN,
            RECIPIENT_ID
        },
        {
            ID_CONTEXT_LEN,
            ID_CONTEXT
        },
        {
            oscore_master_salt_size,
            oscore_master_salt
        },
        OSCORE_AES_CCM_16_64_128,
        OSCORE_SHA_256
    };
    OscoreError r = oscore_context_init(&params_sender, c_sender);

    if (r != OscoreNoError)
    {
        printf("Error : during establishing an OSCORE security context!\n");
        return false;
    }

    /*OSCORE contex initialization*/
    struct oscore_init_params params_receiver =
    {
        SERVER,
        {
            oscore_master_secret_size,
            oscore_master_secret
        },
        {
            SENDER_ID_LEN,
            SENDER_ID
        },
        {
            RECIPIENT_ID_LEN,
            RECIPIENT_ID
        },
        {
            ID_CONTEXT_LEN,
            ID_CONTEXT
        },
        {
            oscore_master_salt_size,
            oscore_master_salt
        },
        OSCORE_AES_CCM_16_64_128,
        OSCORE_SHA_256
    };

    r = oscore_context_init(&params_receiver, c_receiver);

    if (r != OscoreNoError)
    {
        printf("Error : during establishing an OSCORE security context!\n");
        return false;
    }
    return true;
}

coap_oscore_res_t coap_to_oscore(uint8_t *coap_packet, uint16_t coap_packet_size, uint8_t *out, uint16_t *out_size)
{
    uint8_t oscore_buf[OSCORE_MAX_PAYLOAD_SIZE] = {0};
    uint16_t oscore_buf_len = 0;

    if (inner_compression != NULL)
    {
        inner_compression = oscore_msg_inner_compression;
    }
    OscoreError r = coap2oscore(coap_packet, coap_packet_size, oscore_buf, &oscore_buf_len, &c_sender, inner_compression);
    if (r != OscoreNoError)
    {
        printf("Error : coap2oscore: %d\n", r);
        return CO_OSCORE_ERROR;
    }
    if (memcpy(out, oscore_buf, oscore_buf_len) == NULL)
        return CO_COAP_ERROR;
    *out_size = oscore_buf_len;
    return CO_SUCCESS;
}

coap_oscore_res_t oscore_to_coap(uint8_t *oscore_packet, uint16_t oscore_packet_size, uint8_t *out, uint16_t *out_size)
{
    uint8_t coap_rx_buf[OSCORE_MAX_PAYLOAD_SIZE] = {0};
    uint16_t coap_rx_buf_len = sizeof(coap_rx_buf);
    bool oscore_flagx = false;

    if (inner_compression != NULL)
    {
        inner_compression = oscore_msg_inner_decompression;
    }
    //convert oscore to coap
    OscoreError r = oscore2coap(oscore_packet, oscore_packet_size, coap_rx_buf, &coap_rx_buf_len, &oscore_flagx, &c_receiver, inner_compression);
    if (r != OscoreNoError)
    {
        printf("Error : oscore2coap: %d\n", r);
        return CO_OSCORE_ERROR;
    }
    if (oscore_flagx)
    {
        if (memcpy(out, coap_rx_buf, coap_rx_buf_len) == NULL)
            return CO_OSCORE_ERROR;
        *out_size = coap_rx_buf_len;
    }
    else
    {
        //COAP packet not oscorED
        return CO_OSCORE_ERROR;
    }
    return CO_SUCCESS;
}

bool process_uplink_packet(uint8_t *packet, size_t *packet_size)
{
    uint8_t *coap_packet = packet + (sizeof(struct ip6_hdr) + sizeof(struct udphdr));
    uint16_t coap_packet_size = *packet_size - (sizeof(struct ip6_hdr) + sizeof(struct udphdr));

    printf("------- coap part -------\n");
    dump_buf(coap_packet, coap_packet_size);

    uint8_t oscore[MAX_PACKET_SIZE_BYTES] = {0};
    uint16_t oscore_size = 0;
    coap_oscore_res_t res = coap_to_oscore(coap_packet, coap_packet_size, oscore, &oscore_size);
    if (res != CO_SUCCESS)
    {
        printf("Error : failed to transform a CoAP packet to OSCORE\n");
        return false;
    }
    printf("------- oscore encrypted -------\n");
    dump_buf(oscore, oscore_size);

    *packet_size = oscore_size + sizeof(struct ip6_hdr) + sizeof(struct udphdr);
    memcpy(packet + sizeof(struct ip6_hdr) + sizeof(struct udphdr), oscore, oscore_size);
    return true;
}

bool process_downlink_packet(uint8_t *packet, size_t *packet_size, uint16_t data_size)
{
    uint8_t *oscore = packet + (sizeof(struct ip6_hdr) + sizeof(struct udphdr));
    uint16_t oscore_size = data_size - (sizeof(struct ip6_hdr) + sizeof(struct udphdr));
    uint8_t coap[MAX_PACKET_SIZE_BYTES] = {0};
    uint16_t coap_size = 0;

    printf("------- OSCORE part -------\n");
    dump_buf(oscore, oscore_size);
    coap_oscore_res_t res = oscore_to_coap(oscore, oscore_size, coap, &coap_size);

    if (res != CO_SUCCESS)
    {
        printf("Error : failed to transform an OSCORE packet to CoAP\n");
        return false;
    }
    printf("-------- CoAP decrypted --------\n");
    dump_buf((uint8_t *)coap, coap_size);

    *packet_size = coap_size + sizeof(struct ip6_hdr) + sizeof(struct udphdr);
    memcpy((void *)(packet + sizeof(struct ip6_hdr) + sizeof(struct udphdr)), coap, coap_size);
    update_ip6_udp_len((char*) packet, coap_size);
    update_udp_checksum((char*) packet);
    return true;
}
