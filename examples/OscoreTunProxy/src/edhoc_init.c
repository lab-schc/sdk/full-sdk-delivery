/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Hadi Bereksi hadi-ilies.bereksi-reguig@ackl.io
 */

#include <stdbool.h>

#include "edhoc.h"
#include "credentials.h"

bool edhoc_initiator_init(uint8_t *oscore_master_secret, uint16_t oscore_master_secret_size,
                          uint8_t *oscore_master_salt, uint16_t oscore_master_salt_size)
{
    /* edhoc declarations */
    uint8_t PRK_4x3m[PRK_DEFAULT_SIZE];
    uint8_t th4[SHA_DEFAULT_SIZE];
    uint8_t err_msg[ERR_MSG_DEFAULT_SIZE];
    uint32_t err_msg_len = sizeof(err_msg);
    struct other_party_cred cred_r = {
        {ID_CRED_R_LEN, ID_CRED_R},
        {CRED_R_LEN, CRED_R},
        {PK_R_LEN, PK_R},
        {G_R_LEN, G_R},
        {CA_LEN, CA},
        {CA_PK_LEN, CA_PK}};
    uint16_t cred_num = 1;
    struct edhoc_initiator_context c_i = {
        METHOD_TYPE,
        CORR,
        {SUITES_I_LEN, SUITES_I},
        {C_I_LEN, C_I},
        {AD_1_LEN, AD_1},
        {AD_3_LEN, AD_3},
        {ID_CRED_I_LEN, ID_CRED_I},
        {CRED_I_LEN, CRED_I},
        {G_X_LEN, G_X},
        {X_LEN, X},
        {G_I_LEN, G_I},
        {I_LEN, I},
        {SK_I_LEN, SK_I},
        {PK_I_LEN, PK_I}};
    uint8_t ad_2[AD_DEFAULT_SIZE];
    uint64_t ad_2_len = sizeof(ad_2);
    EdhocError r;

    r = edhoc_initiator_run(
        &c_i,
        &cred_r, cred_num,
        err_msg, &err_msg_len,
        ad_2, &ad_2_len,
        PRK_4x3m, sizeof(PRK_4x3m),
        th4, sizeof(th4));
    if (r != EdhocNoError)
    {
        printf("Error in edhoc_initiator_run, (Error code %d)\n", r);
        return false;
    }

    PRINT_ARRAY("PRK_4x3m", PRK_4x3m, sizeof(PRK_4x3m));
    PRINT_ARRAY("th4", th4, sizeof(th4));

    edhoc_exporter(SHA_256, AES_CCM_16_64_128, PRK_4x3m, sizeof(PRK_4x3m), th4, sizeof(th4), "OSCORE Master Secret", oscore_master_secret, oscore_master_secret_size);
    PRINT_ARRAY("OSCORE Master Secret", oscore_master_secret, oscore_master_secret_size);

    edhoc_exporter(SHA_256, AES_CCM_16_64_128, PRK_4x3m, sizeof(PRK_4x3m), th4, sizeof(th4), "OSCORE Master Salt", oscore_master_salt, oscore_master_salt_size);
    PRINT_ARRAY("OSCORE Master Salt", oscore_master_salt, oscore_master_salt_size);

    return true;
}

bool edhoc_responder_init(uint8_t *oscore_master_secret, uint16_t oscore_master_secret_size,
                          uint8_t *oscore_master_salt, uint16_t oscore_master_salt_size)
{
    /* edhoc declarations */
    uint8_t PRK_4x3m[PRK_DEFAULT_SIZE];
    uint8_t th4[SHA_DEFAULT_SIZE];
    uint8_t err_msg[ERR_MSG_DEFAULT_SIZE];
    uint32_t err_msg_len = sizeof(err_msg);
    struct other_party_cred cred_i = {
        {ID_CRED_I_LEN, ID_CRED_I},
        {CRED_I_LEN, CRED_I},
        {PK_I_LEN, PK_I},
        {G_I_LEN, G_I},
        {CA_LEN, CA},
        {CA_PK_LEN, CA_PK}};
    uint16_t cred_num = 1;
    struct edhoc_responder_context c_r = {
        {SUITES_R_LEN, SUITES_R},
        {G_Y_LEN, G_Y},
        {Y_LEN, Y},
        {C_R_LEN, C_R},
        {G_R_LEN, G_R},
        {R_LEN, R},
        {AD_2_LEN, AD_2},
        {ID_CRED_R_LEN, ID_CRED_R},
        {CRED_R_LEN, CRED_R},
        {SK_R_LEN, SK_R},
        {PK_R_LEN, PK_R}};
    uint8_t ad_1[AD_DEFAULT_SIZE];
    uint64_t ad_1_len = sizeof(ad_1);
    uint8_t ad_3[AD_DEFAULT_SIZE];
    uint64_t ad_3_len = sizeof(ad_1);
    EdhocError r;

    r = edhoc_responder_run(&c_r, &cred_i, cred_num, err_msg, &err_msg_len,
                            (uint8_t *)&ad_1, &ad_1_len, (uint8_t *)&ad_3, &ad_3_len,
                            PRK_4x3m, sizeof(PRK_4x3m), th4, sizeof(th4));
    if (r != EdhocNoError)
    {
        printf("error responder run (Error Code %d)\n", r);
        return false;
    }

    PRINT_ARRAY("PRK_4x3m", PRK_4x3m, sizeof(PRK_4x3m));

    PRINT_ARRAY("th4", th4, sizeof(th4));

    edhoc_exporter(SHA_256, AES_CCM_16_64_128, PRK_4x3m, sizeof(PRK_4x3m), th4, sizeof(th4), "OSCORE Master Secret", oscore_master_secret, oscore_master_secret_size);
    PRINT_ARRAY("OSCORE Master Secret", oscore_master_secret, oscore_master_secret_size);

    edhoc_exporter(SHA_256, AES_CCM_16_64_128, PRK_4x3m, sizeof(PRK_4x3m), th4, sizeof(th4), "OSCORE Master Salt", oscore_master_salt, oscore_master_salt_size);
    PRINT_ARRAY("OSCORE Master Salt", oscore_master_salt, oscore_master_salt_size);

    return true;
}