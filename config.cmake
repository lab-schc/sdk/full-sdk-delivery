
option(ADDR_SANITIZER "Enables the address sanitizer" OFF)
option(LEAK_SANITIZER "Enables the leak sanitizer" OFF)
option(MODEM_TRACE_ENABLED "Enabled modem traces for debug for LBM L2 stack" OFF)
option(PLATFORM_TRACE_ENABLED "Enabled platform traces for debug" OFF)
option(USE_FREERTOS "Includes freertos code in the compilation" OFF)

set(APP_VERSION "dev" CACHE STRING "Version of the application to be built")
set(FULLSDK_DR "ADR" CACHE STRING "Data rate to be used by the application (only for LoRa)")
set(APP_NAME "SDKTunApp" CACHE STRING "Name of the example to build")
set(PLATFORM "linux" CACHE STRING "Platform on which the application is supposed to run")
set(L2_STACK "udp" CACHE STRING "L2 layer to be used for the transport of the SCHC packets")
set(FULLSDK_MAX_PAYLOAD_SIZE "1500" CACHE STRING "Maximum size of the packets to be sent by an application")
