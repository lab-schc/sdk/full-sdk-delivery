# Full-SDK delivery package

## Overview

This repository is used to build several applications, for several platforms using the FullSDK library.

```
+-------------------------+
|                         |
|        examples         |
|                         |
+-----------+-+-----------+
|           | |           |
|   libs    | |    os     |
|           | |           |
+-----------+-+-----------+
|                         |
|       *full-sdk*        |
|                         |
+-----------+-+-----------+
|           | |           |
|     l2    | | platforms |
|           | |           |
+-----------+ +-----------+
```

## Clone

Clone the project with its git submodules:

```sh
git clone --recurse-submodules git@gitlab.com:lab-schc/sdk/full-sdk-delivery.git
```

## Repository structure

- `examples/`: list of applications
- `full-sdk/`: submodule containing the source code of the FullSDK library
- `l2/`: list of supported L2 stacks
- `libs/`: third-party libraries used in the applications
- `os/`: OS dependencies (freeRTOS,...)
- `platforms/`: specific code for the targeted platform (m4, x86_64,..)

## List of example applications

- [AT_Sigfox_modem](examples/AT_Sigfox_modem/README.md)
- [ATModem](examples/ATModem/README.md)
- [DLMSMulticast](examples/DLMSMulticast/README.md)
- [DownlinkFrag](examples/DownlinkFrag/README.md)
- [FragAoEExample](examples/FragAoEExample/README.md)
- [OscoreTunProxy](examples/OscoreTunProxy/README.md)
- [PingPong](examples/PingPong/README.md)
- [PingPongEdhoc](examples/PingPongEdhoc/README.md)
- [SCHCCertificationApp](examples/SCHCCertificationApp/README.md)
- [SDKBenchTestApp](examples/SDKBenchTestApp/README.md)
- [SDKTunApp](examples/SDKTunApp/README.md)
- [uIP_udp_client_server_example](examples/uIP_udp_client_server_example/README.md)
- [UpDownFrag](examples/UpDownFrag/README.md)
- [UpDownFragClassA](examples/UpDownFragClassA/)

## Prerequisites

cmake 3.21 version or above.
openocd 0.10.0

### Arm cross compiler

An arm cross compiler is required in order to build the firmware that will be
flashed on the devices. It can be retrieved and installed with the following commands :

```bash
cd /opt
su
wget -O archive.tar.bz2 "https://developer.arm.com/-/media/Files/downloads/gnu/11.2-2022.02/binrel/gcc-arm-11.2-2022.02-x86_64-arm-none-eabi.tar.xz"
tar xf archive.tar.bz2 -C /opt
```

### Additionnal dependencies

#### LoRa Semtech stack

The semtech source tree is also required in order to build the firmware. It can
be retrieved at the following url for LoRa : <https://www.st.com/en/embedded-software/i-cube-lrwan.html>

Once downloaded, it needs to be extracted at a place the compiler will find it

```bash
cd /opt
sudo unzip en.i-cube_lrwan.zip
```

#### Sigfox stack

And the following url for Sigfox : <https://www.st.com/en/embedded-software/x-cube-sfox.html>

Once downloaded, follow the same procedure as for LoRa

```bash
cd /opt
sudo unzip en.x-cube-sfox.zip
```

In order to authenticate your device on the Sigfox network you need a custom _sigfox*data.h_ file, you can get it by following the instructions in the _Getting Sigfox ID and Keys for your module_ section from [this link](https://github.com/aureleq/muRataSigfox).

---

**Note :** You must replace the file `l2/sigfox/inc/sigfox_data.h` with the one you received from murata.

---

## Building the applications

The firmware is compiled using CMake (>= 3.21).
In order to build an example application, you can run the following command:

```sh
cmake -S . -B ./build -DAPP_NAME=<app_name> -DTOOLCHAIN=<toolchain> -DTARGET=<target> -DL2_STACK=<l2_stack> && make -C ./build
```

where:

- `app_name` is the name of the application in the `examples` folder
- `toolchain` and `target` corresponds to the compilation toolchain and the associated target to be used.  It should be one of the supported toolchain/target in the `full-sdk/toolchains/` folder.
- `l2_stack` is the type of L2 stack to be used. It should be one of the supported L2 stacks listed in the `l2` folder

To compile an application in debug mode, you need to set `-DDEBUG_ENABLED=ON`.

For some applications, additional environment variables need to be set. You can check the `README` file of the corresponding application for more details

To change the default Nucleo Lora shield from SX1276 to SX1272, you need to set `NUCLEO_LORA_SHIELD=SX1272` environment variable. This is supported only for STM32L476RG-Nucleo board for Semtech L2 stack.

### Enabling SCHC Certification submodule

```sh
SCHC_CERTIFICATION_ENABLED=ON
```

SCHC Certification application is described in the FullSDK [README](full-sdk/README.md). This env variable is used for the L2 layer and works only with `semtech` L2 layer, which used LoRaWAN v1.0.4 implemented in v4.6.0 LoRaMac-node library.

SCHC Certification submodule uses two additionnal timers, one for sending polling messages regularly, and a second one to retry sending messages. Make sure to initialize enough timers to not to have an issue during runtime.

Integrator must develop L2 layer with the mandatory callbacks defined in `fullsdkl2a.h` and must apply patch to modify LoRaMac-node library to be functionnal. Thoses patches can be found in `/l2/semtech/patch/LmhpCompliance.*.patch` files.

## Erasing the microcontroller

The memory of the microcontroller can be erased by executing the following command in the example's directory:

```sh
OPENOCD_TARGET=<openocd.cfg> make -C openocd/ erase
```

Note: openocd.cfg here is the ".cfg" file in the openocd directory that is made for the selected microcontroller architecture

## Flashing the microcontroller

The executable file of the example can be flashed into the microcontroller by executing the following command in the example's directory:

```sh
OPENOCD_TARGET=<openocd.cfg> BIN_FILE=build/<toolchain>/<target>/<app_name>.bin make -C openocd/ flash
```

## Debugging the microcontroller

An example can be debugged by executing the following command in the example's directory:

```sh
OPENOCD_TARGET=<openocd.cfg> make -C openocd/ debug
```

For more information about debugging can be found [debugging documentation].

## Region and subregion configuration

### Semtech 4.6.0

See [readme](l2/semtech/readme.md)

### Semtech 4.4.7

See [readme](l2/semtech_4.4.7/readme.md)

### LBM TAP 2.0.4

See [readme](l2/lbm/readme.md)

## Memory footprint analysis

`fpvgcc` packet must be installed via Python packet manager:

```sh
pip3 install fpvgcc
```

Complete documentation of `fpvgcc` software can be found [here](https://fpv-gcc.readthedocs.io/en/latest/usage.html).

### Types of analysis

All the following commands must be executed from the repository root.

#### Per _included_ file

A memory footprint report is generated per file that are _included_ in the final executable. Summarized memory usage of fullSDK can be viewed by this command.

```sh
fpvgcc build/<platform>/<app_name>.map --sar
```

Example output:
| FILE          | VEC | FLASH | RAM  | TOTAL |
| ------------- | --- | ----- | ---- | ----- |
| libfullsdk.a  |     | 26848 | 2884 | 29732 |
| LoRaMac.c.obj |     | 11532 | 1716 | 13248 |
| libgcc.a      |     | 9880  |      | 9880  |
| command.c.obj |     | 4279  | 2588 | 6867  |

#### Per _included_ object file

A memory footprint report is generated per object file that are _included_ from fullSDK library in the final executable. Memory usage of each included file from fullSDK can be viewed by this command.

```sh
fpvgcc build/<platform>/<app_name>.map --sobj lib<fullsdk_lib_name>.a
```

Example output:
| OBJFILE             | VEC | FLASH | RAM | TOTAL |
| ------------------- | --- | ----- | --- | ----- |
| fragaesndpriv.c.obj |     | 2172  |     | 2172  |
| decompressor.c.obj  |     | 1800  |     | 1800  |
| fragaarecpriv.c.obj |     | 1712  |     | 1712  |
| fullsdkcdpriv.c.obj |     | 688   | 960 | 1648  |

#### Per _included_ symbol

A memory footprint report is generated per symbol that are _included_ from fullSDK library in the final executable. Memory usage of each included symbol from fullSDK can be viewed by this command.

```sh
fpvgcc build/<platform>/<app_name>.map --ssym lib<fullsdk_lib_name>.a
```

Example output:
| SYMBOL               | VEC | FLASH | RAM | TOTAL |
| -------------------- | --- | ----- | --- | ----- |
| fullsdkcdpriv_c_obj  |     |       | 960 | 960   |
| aa_rec_run_fsm       |     | 730   |     | 730   |
| fullsdkmgtpriv_c_obj |     |       | 692 | 692   |
| get_default_rules    |     | 688   |     | 688   |
| ae_get_frag          |     | 488   |     | 488   |
| mgt_initialize       |     | 476   |     | 476   |

#### Sections of an object file

A memory footprint report is generated for an object file that _exist_ in fullSDK library. Sections are not necessarily included in the final executable. Memory usage of each section of an object file can be viewed by this command.

```sh
fpvgcc build/<platform>/<app_name>.map --lobj <fullsdk_obj_file>.c.obj
```

#### Sections of all object files

A memory footprint report is generated for all the object files that _exist_ in fullSDK library. Sections are not necessarily included in the final executable. Memory usage of each section of all object files can be viewed by this command.

```sh
fpvgcc build/<platform>/<app_name>.map --lar lib<fullsdk_lib_name>.a
```

## Support

The full documentation page can be found [here](https://lab-schc.github.io/).

For more information about the installation or setup of the SDK, you may contact the support team at support@lab-schc.fr.

[debugging documentation]: https://lab-schc.github.io/docs/getting-started.html#debugging
