/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz - aydogan.ersoz@ackl.io
 */

#include <stdint.h>

#include "platform.h"

void platform_timer_init(void)
{
}

void platform_timer_add(timer_obj_t *tmr, uint16_t id, void (*cb)(void *),
                        void *cb_arg)
{
  (void)id;

  TimerInit(tmr, cb);
  TimerSetContext(tmr, cb_arg);
}

void platform_timer_set_duration(timer_obj_t *tmr, uint32_t duration)
{
  TimerSetValue(tmr, duration);
}

void platform_timer_start(timer_obj_t *tmr)
{
  TimerStart(tmr);
}

void platform_timer_stop(timer_obj_t *tmr)
{
  TimerStop(tmr);
}
