/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz - aydogan.ersoz@ackl.io
 */

#include <math.h>
#include <stdint.h>

#include "stm32l4xx_hal.h"

#include "HTAL.h"
#include "platform.h"
#include "vTAL.h"

#define LPTIM2_CLOCK_FREQ 32768u // rtc crystal value on the pcb (X2 component)
#define LPTIM2_PRESCALER 128u
#define S_TO_MS 1000u // second to millisecond conversion
#define TICK_CNT_1_S (1 / (1 / (double)(LPTIM2_CLOCK_FREQ / LPTIM2_PRESCALER)))

typedef struct hal_lp_timer_irq_s
{
  void *callback_arg;
  void (*callback)(void *callback_arg);
} hal_lp_timer_irq_t;

static LPTIM_HandleTypeDef lptim_handle;
static hal_lp_timer_irq_t lptim_tmr_irq = {.callback_arg = NULL,
                                           .callback = NULL};
static uint32_t volatile number_of_1s_requested;
static uint32_t volatile number_of_1s_passed;

void platform_timer_init(void)
{
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_LPTIM2;
  // LPTIM2 is clocked from LSE (low speed external clock 32.768KHz)
  PeriphClkInit.Lptim2ClockSelection = RCC_LPTIM2CLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    while (1)
      ;
  }

  __HAL_RCC_LPTIM2_CLK_ENABLE();

  HAL_NVIC_SetPriority(LPTIM2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(LPTIM2_IRQn);

  lptim_handle.Instance = LPTIM2;
  lptim_handle.Init.Clock.Source = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;
  // each clock tick will be (1/(LPTIM2_CLOCK_FREQ/LPTIM2_PRESCALER)) seconds
  lptim_handle.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV128;
  lptim_handle.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;
  lptim_handle.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;
  lptim_handle.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;
  lptim_handle.Init.CounterSource = LPTIM_COUNTERSOURCE_INTERNAL;
  lptim_handle.Init.Input1Source = LPTIM_INPUT1SOURCE_GPIO;
  lptim_handle.Init.Input2Source = LPTIM_INPUT2SOURCE_GPIO;

  HAL_LPTIM_Init(&lptim_handle);

  __HAL_RCC_LPTIM2_CLK_ENABLE();
  HAL_NVIC_SetPriority(LPTIM2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(LPTIM2_IRQn);

  lptim_tmr_irq.callback_arg = NULL;
  lptim_tmr_irq.callback = NULL;
  number_of_1s_requested = 0;
  number_of_1s_passed = 0;

  VTAL_init();
}

void platform_timer_add(timer_obj_t *tmr, uint16_t id, void (*cb)(void *),
                        void *cb_arg)
{
  tmr->timerID = id;
  tmr->expiredTimeEvent = cb;
  tmr->eventContextInfo = cb_arg;
  tmr->timerMode = VTAL_ONE_SHOT_TIMER;
}

void platform_timer_set_duration(timer_obj_t *tmr, uint32_t duration)
{
  tmr->expiredTime.seconds = (uint32_t)(duration / S_TO_MS);
  tmr->expiredTime.milliseconds = duration % S_TO_MS;
}

void platform_timer_start(timer_obj_t *tmr)
{
  VTAL_addTimer(tmr);
}

void platform_timer_stop(timer_obj_t *tmr)
{
  VTAL_removeTimer(tmr->timerID);
}

void HTAL_PhysicalTimerInit(void)
{
}

void HTAL_startPhysicalTimer(long period_ms, void (*cb)(void *), void *cb_arg)
{
  number_of_1s_requested = ceil((double)period_ms / S_TO_MS);
  number_of_1s_passed = 0;

  HAL_LPTIM_TimeOut_Start_IT(&lptim_handle, 0xFFFF, TICK_CNT_1_S);
  lptim_tmr_irq.callback = cb;
  lptim_tmr_irq.callback_arg = cb_arg;
}

long HTAL_remainingTime(void)
{
  // must return in ms
  return (number_of_1s_requested - number_of_1s_passed) * S_TO_MS;
}

void HTAL_stopPhysicalTimer(void)
{
  HAL_LPTIM_TimeOut_Stop_IT(&lptim_handle);
}

// this function (weakly defined in STM32 HAL library) is called by
// `HAL_LPTIM_IRQHandler` which is actually called by `LPTIM2_IRQHandler`
void HAL_LPTIM_CompareMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
  if (hlptim->Instance == LPTIM2)
  {
    number_of_1s_passed++;
    if (number_of_1s_requested == number_of_1s_passed)
    {
      number_of_1s_passed = 0;

      HTAL_stopPhysicalTimer();

      if (lptim_tmr_irq.callback != NULL)
      {
        lptim_tmr_irq.callback(lptim_tmr_irq.callback_arg);
      }

      HTAL_notifyTimeoutToVTAL();
    }
    else
    {
      HAL_LPTIM_TimeOut_Start_IT(&lptim_handle, 0xFFFF, TICK_CNT_1_S);
    }
  }
}

void LPTIM2_IRQHandler(void)
{
  HAL_LPTIM_IRQHandler(&lptim_handle);
}