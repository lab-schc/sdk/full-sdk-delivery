/**
 * @file platform.c
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Aydogan Ersoz - aydogan.ersoz@ackl.io
 *
 * @brief LBM platform implementation
 */

#include <stdbool.h>
#include <stdio.h>

#include "platform.h"
#include "smtc_modem_api.h"
#include "stm32l4xx_hal_rng.h"

void platform_uart_rx_callback(uint8_t *data);
static void (*uart_rx_callback)(uint8_t *ch);

#ifdef PLATFORM_TRACE_ENABLED
bool platform_traces_enabled = false;
void platform_enable_traces(bool enable)
{
  platform_traces_enabled = enable;
}
#else
void platform_enable_traces(bool enable)
{
  (void)enable;
}
#endif

uint32_t platform_get_clock_ms(void)
{
  return (uint32_t)smtc_modem_hal_get_time_in_ms();
}

/**
 * Code from MbedOS:
 * https://gitlab.exmachina.fr/fw-libs/mbed-os/raw/5.11.2/targets/TARGET_STM/trng_api.c
 */

/**
 *  Copyright (C) 2006-2016, ARM Limited, All Rights Reserved
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may
 *  not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

#define ERR_ENTROPY_SOURCE_FAILED -0x003C

int platform_entropy_hardware_poll(void *data, unsigned char *output,
                                   size_t len, size_t *olen)
{
  RNG_HandleTypeDef RngHandle;
  uint32_t dummy;
  int ret = 0;
  volatile uint8_t random[4];
  ((void)data);

#if defined(RCC_PERIPHCLK_RNG)
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

  /*Select PLLQ output as RNG clock source */
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RNG;
#if ((CLOCK_SOURCE)&USE_PLL_MSI)
  PeriphClkInitStruct.RngClockSelection = RCC_RNGCLKSOURCE_MSI;
#else
  PeriphClkInitStruct.RngClockSelection = RCC_RNGCLKSOURCE_PLL;
#endif
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    PRINT_MSG("error: entropy_hardware_poll: RNG clock configuration error\n");
    return ERR_ENTROPY_SOURCE_FAILED;
  }
#endif

  /* RNG Peripheral clock enable */
  __HAL_RCC_RNG_CLK_ENABLE();

  /* Initialize RNG instance */
  RngHandle.Instance = RNG;
  RngHandle.State = HAL_RNG_STATE_RESET;
  RngHandle.Lock = HAL_UNLOCKED;

  HAL_RNG_Init(&RngHandle);

  /* first random number generated after setting the RNGEN bit should not be
   * used */
  HAL_RNG_GenerateRandomNumber(&RngHandle, &dummy);

  *olen = 0;
  /* Get Random byte */
  while ((*olen < len) && (ret == 0))
  {
    if (HAL_RNG_GenerateRandomNumber(&RngHandle, (uint32_t *)random) != HAL_OK)
      ret = ERR_ENTROPY_SOURCE_FAILED;
    else
      for (uint8_t i = 0; (i < 4) && (*olen < len); i++)
      {
        *output++ = random[i];
        *olen += 1;
        random[i] = 0;
      }
  }
  /* Just be extra sure that we didn't do it wrong */
  if ((__HAL_RNG_GET_FLAG(&RngHandle, (RNG_FLAG_CECS | RNG_FLAG_SECS))) != 0)
    ret = ERR_ENTROPY_SOURCE_FAILED;
  /*Disable the RNG peripheral */
  HAL_RNG_DeInit(&RngHandle);
  /* RNG Peripheral clock disable - assume we're the only users of RNG  */
  __HAL_RCC_RNG_CLK_DISABLE();
  return ret;
}

void platform_hw_init(void)
{
  platform_timer_init();

  uart_rx_callback = NULL;
}

void platform_delay_ms(uint32_t ms)
{
  hal_mcu_wait_us(ms * 1000);
}

void platform_reset(void)
{
  hal_mcu_reset();
}

void platform_reload_watchdog(void)
{
  watchdog_reload();
}

void platform_print_hex_buffer(const uint8_t *buffer, uint16_t size)
{
  uint8_t newline = 0;

  for (uint16_t i = 0; i < size; i++)
  {
    if (newline != 0)
    {
      PRINT_MSG("\n");
      newline = 0;
    }

    PRINT_MSG("%02X ", buffer[i]);

    if (((i + 1) % 16) == 0)
    {
      newline = 1;
    }
  }
  PRINT_MSG("\n");
}

void platform_error_handler(void)
{
  PRINT_MSG("***** entered error state\n");
  while (true)
    ;
}

void platform_configure_sleep_mode(void)
{
  // TODO(aydu): to be implemented later
}

void platform_enter_low_power(void)
{
  // TODO(aydu): to be implemented later
}

void platform_enter_low_power_ll(void)
{
  // TODO(aydu): to be implemented later
}

void platform_led_on(led_e led)
{
  // TODO(aydu): to be implemented later
  (void)led;
}

void platform_led_off(led_e led)
{
  // TODO(aydu): to be implemented later
  (void)led;
}

void platform_led_toggle(led_e led)
{
  // TODO(aydu): to be implemented later
  (void)led;
}

void platform_set_uart_rx_callback(void (*callback)(uint8_t *ch))
{
  uart_rx_callback = callback;
}

void platform_set_sleep_for_ms(uint32_t ms)
{
  // hal_mcu_set_sleep_for_ms(ms);
  HAL_Delay(ms);
}

void platform_uart_rx_callback(uint8_t *data)
{
  if (uart_rx_callback != NULL)
  {
    uart_rx_callback(data);
  }
}
