set(LBM_MAIN_SRC_DIR ${PROJECT_SOURCE_DIR}/libs/lbm_3.3.0)
set(VTAL_SRC_DIR ${PROJECT_SOURCE_DIR}/libs/vTAL/vTAL)

# Radio shield selection:
# - LR111X
# - LR112X
# - SX1261
# - SX1262
# - SX1268
# Default value is LR1110.
set(PLATFORM_RADIO_SHIELD "LR1110" CACHE STRING "Radio shield used with the board")

set(PLATFORM_INC_DIR
  ${CMAKE_CURRENT_LIST_DIR}/inc
  ${VTAL_SRC_DIR}
  ${PROJECT_SOURCE_DIR}/build/${PLATFORM}/l2/${L2_STACK}/patch
  ${PROJECT_SOURCE_DIR}/build/${PLATFORM}/l2/${L2_STACK}/patch/trace
  ${VTAL_SRC_DIR}/vTAL-src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_api
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lorawan_api
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lorawan_manager
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lorawan_packages
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lorawan_packages/fuota_packages
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/lr1mac_class_b
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/lr1mac_class_c
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/relay/common
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/relay/relay_ed
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/relay/relay_master
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/services
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/services/smtc_multicast
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/lr1mac/src/smtc_real/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/modem_supervisor
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/modem_utilities
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_planner/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_modem_crypto/
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_modem_crypto/smtc_secure_element
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_modem_crypto/soft_secure_element
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_ral/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/smtc_ralf/src
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/radio_hal
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/mcu_drivers/cmsis/Core/Include
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/mcu_drivers/cmsis/Device/ST/STM32L4xx/Include
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/mcu_drivers/STM32L4xx_HAL_Driver/Inc/
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/mcu_drivers/core
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/smtc_hal_l4
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/utilities/user_app/littlefs
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_hal/smtc_modem_hal
  ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_hal
)

if(${PLATFORM_RADIO_SHIELD} STREQUAL "LR1110" OR ${PLATFORM_RADIO_SHIELD} STREQUAL "LR1120")
  set(PLATFORM_INC_DIR
    ${PLATFORM_INC_DIR}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src)

  set(PLATFORM_SRC
    ${PLATFORM_SRC}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_bootloader.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_crypto_engine.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_driver_version.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_gnss.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_lr_fhss.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_radio_timings.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_radio.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_regmem.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_system.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/lr11xx_driver/src/lr11xx_wifi.c)
elseif(${PLATFORM_RADIO_SHIELD} STREQUAL "SX1261" OR ${PLATFORM_RADIO_SHIELD} STREQUAL "SX1262" OR ${PLATFORM_RADIO_SHIELD} STREQUAL "SX1268")
    set(PLATFORM_INC_DIR
    ${PLATFORM_INC_DIR}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/sx126x_driver/src)

  set(PLATFORM_SRC
    ${PLATFORM_SRC}
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/sx126x_driver/src/lr_fhss_mac.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/sx126x_driver/src/sx126x_lr_fhss.c
    ${LBM_MAIN_SRC_DIR}/lora_basics_modem/smtc_modem_core/radio_drivers/sx126x_driver/src/sx126x.c)
else()
  message(FATAL_ERROR "SHIELD ${PLATFORM_RADIO_SHIELD} is not supported")
endif()
