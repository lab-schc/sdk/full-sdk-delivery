/**
 * @file platform.h
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * @author Aydogan Ersoz - aydogan.ersoz@ackl.io
 *
 * @brief LBM platform interface
 */

#ifndef APP_INC_PLATFORM_H_
#define APP_INC_PLATFORM_H_

#include <stdbool.h>
#include <stdint.h>

#include "smtc_modem_hal.h"
#include "stm32l4xx_hal.h"
#include "stm32l4xx_ll_utils.h"
#include "smtc_hal_mcu.h"
#include "smtc_hal_rtc.h"
#include "smtc_hal_watchdog.h"
#include "vTAL.h"

typedef enum
{
  LED1 = 1,
  LED2,
  LED3,
  LED4
} led_e;

typedef VTAL_tstrConfig timer_obj_t;

#define PRINT_AT(...) hal_trace_print_var(__VA_ARGS__)
#define PRINT_MSG(...) hal_trace_print_var(__VA_ARGS__)
#define PRINT_HEX_BUF(buf, size) platform_print_hex_buffer(buf, size);

#ifdef PLATFORM_TRACE_ENABLED
extern bool platform_traces_enabled;

#define PRINT_DBG(...)                  \
  do                                    \
  {                                     \
    if (platform_traces_enabled)        \
    {                                   \
      hal_trace_print_var(__VA_ARGS__); \
    }                                   \
  } while (0);
#define PRINT_HEX_BUF_DBG(buf, size) \
  do                                 \
  {                                  \
    if (platform_traces_enabled)     \
    {                                \
      PRINT_HEX_BUF(buf, size);      \
    }                                \
  } while (0);
#define PRINT_MSG_TS(fmt, arg...)                               \
  do                                                            \
  {                                                             \
    if (platform_traces_enabled)                                \
    {                                                           \
      hal_trace_print_var("%.10lu " fmt, HAL_GetTick(), ##arg); \
    }                                                           \
  } while (0);
#define PRINTNOW()                             \
  do                                           \
  {                                            \
    if (platform_traces_enabled)               \
    {                                          \
      uint32_t stime = hal_rtc_get_time_ms();  \
      hal_trace_print_var("%.5d ms: ", stime); \
    }                                          \
  } while (0)
#else
#define PRINT_DBG(...) \
  do                   \
  {                    \
  } while (0)
#define PRINT_HEX_BUF_DBG(buf, size) \
  do                                 \
  {                                  \
  } while (0)
#define PRINT_MSG_TS(...) \
  do                      \
  {                       \
  } while (0)
#define PRINTNOW() \
  do               \
  {                \
  } while (0)
#endif /* PLATFORM_TRACE_ENABLED */

#define BEGIN_CRITICAL_SECTION() \
  uint32_t mask;                 \
  hal_mcu_critical_section_begin(&mask)

#define END_CRITICAL_SECTION() hal_mcu_critical_section_end(&mask)

int platform_entropy_hardware_poll(void *data, unsigned char *output,
                                   size_t len, size_t *olen);

/**
 * @brief Initializes hardware
 */
void platform_hw_init(void);

/**
 * @brief Waits for `ms` milliseconds
 */
void platform_delay_ms(uint32_t ms);

/**
 * @brief Resets hardware
 */
void platform_reset(void);

/**
 * @brief Prints hexadecimal buffer
 */
void platform_print_hex_buffer(const uint8_t *buffer, uint16_t size);

/**
 * @brief Configures sleep mode
 */
void platform_configure_sleep_mode(void);

/**
 * @brief Enters into error state
 */
void platform_error_handler(void);

/**
 * @brief Initializes timer module
 */
void platform_timer_init(void);

/**
 * @brief Reloads watchdog timer to avoid system reset
 */
void platform_reload_watchdog(void);

/**
 * @brief Adds a timer
 *
 * @param tmr Timer object
 * @param id Timer ID
 * @param cb Callback to be executed when the timer goes off
 * @param cb_arg Argument to the callback
 */
void platform_timer_add(timer_obj_t *tmr, uint16_t id, void (*cb)(void *),
                        void *cb_arg);

/**
 * @brief Starts a timer
 *
 * @param tmr Timer object
 */
void platform_timer_start(timer_obj_t *tmr);

/**
 * @brief Sets the duration of a timer
 *
 * @param tmr Timer object
 * @param duration Duration of the timer in milliseconds
 */
void platform_timer_set_duration(timer_obj_t *tmr, uint32_t duration);

/**
 * @brief Stops a timer
 *
 * @param tmr Timer object
 */
void platform_timer_stop(timer_obj_t *tmr);

/**
 * @brief Enters low power mode
 */
void platform_enter_low_power(void);

/**
 * @brief Switches on led
 */
void platform_led_on(led_e led);

/**
 * @brief Switches off led
 */
void platform_led_off(led_e led);

/**
 * @brief Toggles led
 */
void platform_led_toggle(led_e led);

/**
 * @brief Sets callback to be executed when a byte is received from UART
 */
void platform_set_uart_rx_callback(void (*callback)(uint8_t *ch));

/**
 * @brief Sets the MCU in sleep mode for the given number of milliseconds
 *
 * @param ms Time in milliseconds
 */
void platform_set_sleep_for_ms(uint32_t ms);

/**
 * @brief Enables enabling or disabling platform traces
 *
 * @param enable True to enable, False to disable
 */
void platform_enable_traces(bool enable);

#endif /* APP_INC_PLATFORM_H_ */
