/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz - aydogan.ersoz@ackl.io
 */

#ifndef APP_INC_PLATFORM_H_
#define APP_INC_PLATFORM_H_

#include <stdint.h>
#include <stdio.h>

#include "LoRaMac.h"
#include "board.h"
#include "delay.h"
#include "lpm-board.h"
#include "nvmm.h"
#include "uart.h"
#include "utilities.h"

#define BEGIN_CRITICAL_SECTION() CRITICAL_SECTION_BEGIN()
#define END_CRITICAL_SECTION() CRITICAL_SECTION_END()

#define PRINT_AT(...) printf(__VA_ARGS__)
#define PRINT_MSG(...) printf(__VA_ARGS__)
#define PRINT_HEX_BUF(buf, size) platform_print_hex_buffer(buf, size);

#ifdef PLATFORM_TRACE_ENABLED
extern bool platform_traces_enabled;

#define PRINT_DBG(...)           \
  do                             \
  {                              \
    if (platform_traces_enabled) \
    {                            \
      printf(__VA_ARGS__);       \
    }                            \
  } while (0);
#define PRINT_HEX_BUF_DBG(buf, size) \
  do                                 \
  {                                  \
    if (platform_traces_enabled)     \
    {                                \
      PRINT_HEX_BUF(buf, size);      \
    }                                \
  } while (0);
#define PRINT_MSG_TS(fmt, arg...)                  \
  do                                               \
  {                                                \
    if (platform_traces_enabled)                   \
    {                                              \
      printf("%.10lu " fmt, HAL_GetTick(), ##arg); \
    }                                              \
  } while (0);
#define PRINTNOW()                                         \
  do                                                       \
  {                                                        \
    SysTime_t stime = SysTimeGetMcuTime();                 \
    printf("%3ds%03d: ", stime.Seconds, stime.SubSeconds); \
  } while (0)
#else
#define PRINT_DBG(...) \
  do                   \
  {                    \
  } while (0)
#define PRINT_HEX_BUF_DBG(buf, size) \
  do                                 \
  {                                  \
  } while (0)
#define PRINT_MSG_TS(...) \
  do                      \
  {                       \
  } while (0)
#define PRINTNOW() \
  do               \
  {                \
  } while (0)
#endif /* PLATFORM_TRACE_ENABLED */

typedef enum
{
  LED1 = 1,
  LED2
} led_e;

void platform_hw_init(void);
void platform_reset(void);
void platform_print_hex_buffer(const uint8_t *buffer, uint16_t size);
void platform_error_handler(void);
void platform_delay_ms(uint32_t ms);
uint32_t platform_get_clock_ms(void);
int platform_entropy_hardware_poll(void *data, unsigned char *output,
                                   size_t len, size_t *olen);
void platform_configure_sleep_mode(void);
void platform_enter_low_power(void);
void platform_enter_low_power_ll(void);
void platform_lpm_set_off_mode(LpmId_t id, LpmSetMode_t mode);
void platform_lpm_set_stop_mode(LpmId_t id, LpmSetMode_t mode);
// Two leds are available (whose ids are 1 and 2)
void platform_led_on(led_e led);
void platform_led_off(led_e led);
void platform_led_toggle(led_e led);
void platform_set_uart_rx_callback(void (*callback)(uint8_t *ch));
// Not to include MCU HAL library header
uint32_t HAL_GetTick(void);

// Non-volatile memory management

/**
 * @brief Writes some data in the nvm along with a CRC in order to ensure data
 * integrity
 * The size of the CRC is equal to `sizeof(uint32_t)`
 *
 * @param src The data to write
 * @param size The size of this data
 * @param offset The offset from where the writing process must starts in the
 * memory
 * @return The size of the written data (CRC included) or -1 if an error
 * occured
 */
int32_t platform_nvm_write(uint8_t *src, uint16_t size, uint16_t offset);

/**
 * @brief Reads some data in the nvm
 * Before reading the data, the CRC is checked
 *
 * @param dest A pointer where the read data must be stored
 * @param size The size to read in the memory
 * @param offset The offset from where the reading process must starts in the
 * memory
 * @return The size of the read data (CRC not included) or -1 if an error
 * occured
 */
int32_t platform_nvm_read(uint8_t *dest, uint16_t size, uint16_t offset);

// Timer management

#ifdef USE_EXTERNAL_TIMESERVER
#include "vTAL.h"
typedef VTAL_tstrConfig timer_obj_t;
#else
typedef TimerEvent_t timer_obj_t;
typedef TimerTime_t timer_time_t;
#endif

/**
 * @brief Initializes timer module
 */
void platform_timer_init(void);

/**
 * @brief Adds a timer
 *
 * @param tmr Timer object
 * @param id Timer ID
 * @param cb Callback to be executed when the timer goes off
 * @param cb_arg Argument to the callback
 */
void platform_timer_add(timer_obj_t *tmr, uint16_t id, void (*cb)(void *),
                        void *cb_arg);

/**
 * @brief Starts a timer
 *
 * @param tmr Timer object
 */
void platform_timer_start(timer_obj_t *tmr);

/**
 * @brief Sets the duration of a timer
 *
 * @param tmr Timer object
 * @param duration Duration of the timer in milliseconds
 */
void platform_timer_set_duration(timer_obj_t *tmr, uint32_t duration);

/**
 * @brief Stops a timer
 *
 * @param tmr Timer object
 */
void platform_timer_stop(timer_obj_t *tmr);

/**
 * @brief Sets the MCU in sleep mode for the given number of milliseconds
 *
 * @param ms Time in milliseconds
 */
void platform_set_sleep_for_ms(uint32_t ms);

/**
 * @brief Reloads watchdog timer to avoid system reset
 */
void platform_reload_watchdog(void);

/**
 * @brief Enables enabling or disabling platform traces
 *
 * @param enable True to enable, False to disable
 */
void platform_enable_traces(bool enable);

/**
 * @brief Read the current time (ms)
 *
 * @param time returns current time (ms)
 */
uint32_t platform_get_current_time(void);

/**
 * @brief  Return the Time elapsed since a fixed moment (ms)
 *
 * @param[in] fixed moment (ms)
 * @return time returns elapsed time
 */
uint32_t
platform_get_elpased_time(uint32_t past);

#endif /* APP_INC_PLATFORM_H_ */
