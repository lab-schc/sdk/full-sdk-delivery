add_compile_definitions(USE_HAL_DRIVER STM32L476xx NUCLEO_L476RG)
set(CMAKE_C_FLAGS
  "${CMAKE_C_FLAGS} -T${PROJECT_SOURCE_DIR}/libs/lbm/host_driver/STMicroelectronics/gcc/stm32l476rgtx_flash.ld -mthumb -mfpu=fpv4-sp-d16 -fstack-usage -ffunction-sections -Wl,--gc-sections -MD")

include(${PROJECT_SOURCE_DIR}/platforms/${PLATFORM}/${L2_STACK}/flags.cmake)
