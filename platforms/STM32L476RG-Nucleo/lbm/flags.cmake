# Radio shield selection:
# - LR1110MB1DxS
# - LR1110MB1GxS
# - LR1120MB1DxS
# - LR1120MB1GxS
# - SX1261MB1BAS
# - SX1261MB1CAS
# - SX1261MB2BAS
# Default value is LR1110MB1DxS.
set(PLATFORM_RADIO_SHIELD "LR1110MB1DxS" CACHE STRING "Radio shield used with the board")

set(PLATFORM_INC_DIR
  ${PROJECT_SOURCE_DIR}/platforms/${PLATFORM}/${L2_STACK}/inc/
  ${PROJECT_SOURCE_DIR}/libs/lbm/smtc_hal/inc/
  ${PROJECT_SOURCE_DIR}/libs/lbm/smtc_hal/board/
  ${PROJECT_SOURCE_DIR}/libs/lbm/smtc_hal/STMicroelectronics/STM32L4xx/inc/
  ${PROJECT_SOURCE_DIR}/libs/lbm/projects/LR1110/LR1110MB1LxKS/BSP/board/
  ${PROJECT_SOURCE_DIR}/libs/lbm/lora_basics_modem/lora_basics_modem/smtc_modem_hal/
  ${PROJECT_SOURCE_DIR}/libs/lbm/host_driver/STMicroelectronics/STM32L4xx/Drivers/STM32L4xx_HAL_Driver/Inc/
  ${PROJECT_SOURCE_DIR}/libs/lbm/host_driver/STMicroelectronics/STM32L4xx/hal_config/
  ${PROJECT_SOURCE_DIR}/libs/lbm/host_driver/STMicroelectronics/STM32L4xx/Drivers/CMSIS/Device/ST/STM32L4xx/Include/
  ${PROJECT_SOURCE_DIR}/libs/vTAL/vTAL/)