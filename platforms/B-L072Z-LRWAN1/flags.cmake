add_compile_definitions(USE_HAL_DRIVER STM32L072xx ENABLE_RETARGET)

if(DEBUG_ENABLED)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Os -g3")
else()
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Os")
endif()

set(CMAKE_C_FLAGS
  "${CMAKE_C_FLAGS} -fstack-usage -ffunction-sections -Wl,--gc-sections -MD")

include(${CMAKE_CURRENT_LIST_DIR}/${L2_STACK}/flags.cmake)
