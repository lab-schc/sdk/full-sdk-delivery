/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Flavien Moullec - flavien@ackl.io
 */

#ifndef APP_INC_PLATFORM_H_
#define APP_INC_PLATFORM_H_

#include <stdint.h>

#include "hw.h"
#include "low_power_manager.h"
#include "timeServer.h"
#include "vcom.h"

#define PRINT_MSG PRINTF
#define PRINT_HEX_BUF(buf, size) platform_print_hex_buffer(buf, size);

#ifdef PLATFORM_TRACE_ENABLED
extern bool platform_traces_enabled;

#define PRINT_DBG(...)                                                         \
  do                                                                           \
  {                                                                            \
    if (platform_traces_enabled)                                               \
    {                                                                          \
      vcom_Send(__VA_ARGS__);                                                  \
    }                                                                          \
  } while (0);
#define PRINT_HEX_BUF_DBG(buf, size)                                           \
  do                                                                           \
  {                                                                            \
    if (platform_traces_enabled)                                               \
    {                                                                          \
      PRINT_HEX_BUF(buf, size);                                                \
    }                                                                          \
  } while (0);
#else
#define PRINT_DBG(...)                                                         \
  do                                                                           \
  {                                                                            \
  } while (0)
#define PRINT_HEX_BUF_DBG(buf, size)                                           \
  do                                                                           \
  {                                                                            \
  } while (0)
#endif /* PLATFORM_TRACE_ENABLED */

typedef TimerEvent_t timer_obj_t;

void platform_print_hex_buffer(const uint8_t *buffer, uint16_t size);

void platform_hw_init(void);

/**
 * @brief Adds a timer
 *
 * @param tmr Timer object
 * @param id Timer ID
 * @param cb Callback to be executed when the timer goes off
 * @param cb_arg Argument to the callback
 */
void platform_timer_add(timer_obj_t *tmr, uint16_t id, void (*cb)(void *),
                        void *cb_arg);

/**
 * @brief Starts a timer
 *
 * @param tmr Timer object
 */
void platform_timer_start(timer_obj_t *tmr);

/**
 * @brief Sets the duration of a timer
 *
 * @param tmr Timer object
 * @param duration Duration of the timer in milliseconds
 */
void platform_timer_set_duration(timer_obj_t *tmr, uint32_t duration);

/**
 * @brief Stops a timer
 *
 * @param tmr Timer object
 */
void platform_timer_stop(timer_obj_t *tmr);

/**
 * @brief Enables enabling or disabling platform traces
 *
 * @param enable True to enable, False to disable
 */
void platform_enable_traces(bool enable);

#endif /* APP_INC_PLATFORM_H_ */
