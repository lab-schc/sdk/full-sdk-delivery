set(PLATFORM_TOP_DIR /opt/STM32CubeExpansion_SFOX_V1.1.0)
set(PLATFORM_SIGFOX_TOP_DIR ${PLATFORM_TOP_DIR}/Projects/B-L072Z-LRWAN1/Applications/Sgfx/Sgfx_ModemAT)

set(
  PLATFORM_INC_DIR
  ${CMAKE_CURRENT_LIST_DIR}/inc
  ${PLATFORM_TOP_DIR}/Drivers/BSP/Components/Common
  ${PLATFORM_TOP_DIR}/Drivers/BSP/Components/hts221
  ${PLATFORM_TOP_DIR}/Drivers/BSP/Components/lps22hb
  ${PLATFORM_TOP_DIR}/Drivers/BSP/Components/lps25hb
  ${PLATFORM_TOP_DIR}/Drivers/BSP/Components/sx1276
  ${PLATFORM_TOP_DIR}/Drivers/BSP/X_NUCLEO_IKS01A1
  ${PLATFORM_TOP_DIR}/Drivers/BSP/B-L072Z-LRWAN1
  ${PLATFORM_TOP_DIR}/Drivers/CMSIS/Device/ST/STM32L0xx/Include
  ${PLATFORM_TOP_DIR}/Drivers/CMSIS/Include
  ${PLATFORM_TOP_DIR}/Drivers/STM32L0xx_HAL_Driver/Inc
  ${PLATFORM_TOP_DIR}/Drivers/BSP/MLM32L07X01
  ${PLATFORM_TOP_DIR}/Projects/B-L072Z-LRWAN1/Applications/Sgfx/Common/cwmx1zzabz/Radio_Sigfox_Driver
  ${PLATFORM_TOP_DIR}/Projects/B-L072Z-LRWAN1/Applications/Sgfx/Common/cwmx1zzabz/Credentials_libs
  ${PLATFORM_TOP_DIR}/Projects/B-L072Z-LRWAN1/Applications/Sgfx/Sgfx_ModemAT/inc
)

add_compile_definitions(HAL_CRC_MODULE_ENABLED USE_B_L072Z_LRWAN1 USE_FULL_LL_DRIVER LOW_POWER_DISABLE USE_RADIO_LL_DRIVER)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -T${CMAKE_CURRENT_BINARY_DIR}/platforms/${PLATFORM}/${L2_STACK}/Patching/stm32l072xx_flash.ld")

# Had to declare a new variable to not use EXTRA_LIBS
set(
  EXTRA_PL_LIBS
  ${PLATFORM_TOP_DIR}/Projects/B-L072Z-LRWAN1/Applications/Sgfx/Common/cwmx1zzabz/Radio_Sigfox_Driver/SgfxSTModemSx1276V123_CM0_GCC.a
  ${PLATFORM_TOP_DIR}/Projects/B-L072Z-LRWAN1/Applications/Sgfx/Common/cwmx1zzabz/Credentials_libs/SgfxCredentialsV011_CM0_GCC.a)