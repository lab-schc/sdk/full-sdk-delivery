/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Flavien Moullec - flavien@ackl.io
 */

#include <stdint.h>

#include "platform.h"

void platform_print_hex_buffer(const uint8_t *buffer, uint16_t size)
{
  uint8_t newline = 0;

  for (uint16_t i = 0; i < size; i++)
  {
    if (newline != 0)
    {
      PRINT_MSG("\n");
      newline = 0;
    }

    PRINT_MSG("%02X ", buffer[i]);

    if (((i + 1) % 16) == 0)
    {
      newline = 1;
    }
  }
  PRINT_MSG("\n");
}

#ifdef PLATFORM_TRACE_ENABLED
bool platform_traces_enabled = false;
void platform_enable_traces(bool enable)
{
  platform_traces_enabled = enable;
}
#else
void platform_enable_traces(bool enable)
{
  (void)enable;
}
#endif

void platform_hw_init(void)
{
  /* STM32 HAL library initialization*/
  HAL_Init();

  /* Configure the system clock*/
  SystemClock_Config();

  /* Configure the debug mode*/
  DBG_Init();

  /* Configure the hardware*/
  HW_Init();
}

void platform_timer_add(timer_obj_t *tmr, uint16_t id, void (*cb)(void *),
                        void *cb_arg)
{
  (void)id;

  TimerInit(tmr, cb);
}

void platform_timer_set_duration(timer_obj_t *tmr, uint32_t duration)
{
  TimerSetValue(tmr, duration);
}

void platform_timer_start(timer_obj_t *tmr)
{
  TimerStart(tmr);
}

void platform_timer_stop(timer_obj_t *tmr)
{
  TimerStop(tmr);
}