/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Aydogan Ersoz - aydogan.ersoz@ackl.io
 *
 * Based on ST's AT_Slave example. Check license here: www.st.com/SLA0044.
 *
 */

#include "hw.h"
#include "vcom.h"
#include "b-l072z-lrwan1.h"

void LED_Init(void)
{
  BSP_LED_Init(LED_BLUE);
  BSP_LED_Init(LED_GREEN);
}

void LED_Alive_Toggle(void)
{
  BSP_LED_Toggle(LED_BLUE);
}

void LED_SleepMode_On(void)
{
  BSP_LED_On(LED_GREEN);
}

void LED_SleepMode_Off(void)
{
  BSP_LED_Off(LED_GREEN);
}
