/**
 * @copyright
 * Copyright (c) 2018-2023 ACKLIO SAS
 * Copyright (c) 2024 ACTILITY SA - All Rights Reserved
 * 
 * This file is part of lab.SCHC FullSDK.
 * 
 * Use of this source code is governed by an MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 * 
 * Author: Flavien Moullec - flavien@ackl.io
 * Author: Aydogan Ersoz - aydogan.ersoz@ackl.io
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "systime.h"

#include "platform.h"

// Here the base offset is equal to the size of the Mac context (because we
// don't want to override it)
#define BASE_OFFSET sizeof(LoRaMacNvmData_t)

#define OFFSET (BASE_OFFSET + offset)

// defined in board.c
extern Gpio_t Led1;
extern Gpio_t Led2;
extern Gpio_t Led3;
extern Gpio_t Led4;
// defined in board.c
extern Uart_t Uart2;

static Gpio_t Button1;

static Gpio_t *platform_convert_led(led_e led);
static void platform_uart_rx_callback(UartNotifyId_t id);
static void button_init(void);

static void (*uart_rx_callback)(uint8_t *ch);

#ifdef PLATFORM_TRACE_ENABLED
bool platform_traces_enabled = false;
void platform_enable_traces(bool enable)
{
  platform_traces_enabled = enable;
}
#else
void platform_enable_traces(bool enable)
{
  (void)enable;
}
#endif

void platform_hw_init(void)
{
  BoardInitMcu();
  BoardInitPeriph();

  Uart2.IrqNotify = platform_uart_rx_callback;
  uart_rx_callback = NULL;

  button_init();
}

void platform_reset(void)
{
  BoardResetMcu();
}

void platform_print_hex_buffer(const uint8_t *buffer, uint16_t size)
{
  uint8_t newline = 0;

  for (uint16_t i = 0; i < size; i++)
  {
    if (newline != 0)
    {
      PRINT_MSG("\n");
      newline = 0;
    }

    PRINT_MSG("%02X ", buffer[i]);

    if (((i + 1) % 16) == 0)
    {
      newline = 1;
    }
  }
  PRINT_MSG("\n");
}

void platform_error_handler(void)
{
  PRINT_MSG("***** entered error state\n");
  while (true)
    ;
}

void platform_delay_ms(uint32_t ms)
{
  DelayMs(ms);
}

uint32_t platform_get_clock_ms(void)
{
  return SysTimeToMs(SysTimeGetMcuTime());
}

__attribute__((weak)) void user_button_callback(void *context)
{
  /* to be implemented by the application */
}

static void button_init(void)
{
  GpioInit(&Button1, PB_2, PIN_INPUT, PIN_PUSH_PULL, PIN_NO_PULL, 0);
  GpioSetInterrupt(&Button1, IRQ_FALLING_EDGE, IRQ_VERY_LOW_PRIORITY,
                   user_button_callback);
}

// Initialize system to enter sleep mode instead of standby mode
void platform_configure_sleep_mode(void)
{
  platform_lpm_set_off_mode(LPM_APPLI_ID, LPM_DISABLE);
  platform_lpm_set_stop_mode(LPM_APPLI_ID, LPM_DISABLE);
}

// Use this instead of `platform_enter_low_power` when `ENABLE_IRQ` and
// `DISABLE_IRQ` are not explicitly called. it should be prefereble over
// `platform_enter_low_power`.
void platform_enter_low_power(void)
{
  BoardLowPowerHandler();
}

// `LPM_EnterLowPower` equivalent
void platform_enter_low_power_ll(void)
{
  LpmEnterLowPower();
}

// `LPM_SetOffMode` equivalent
void platform_lpm_set_off_mode(LpmId_t id, LpmSetMode_t mode)
{
  LpmSetOffMode(id, mode);
}

// `LPM_SetStopMode` equivalent
void platform_lpm_set_stop_mode(LpmId_t id, LpmSetMode_t mode)
{
  LpmSetStopMode(id, mode);
}

void platform_led_on(led_e led)
{
  GpioWrite(platform_convert_led(led), 1);
}

void platform_led_off(led_e led)
{
  GpioWrite(platform_convert_led(led), 0);
}

void platform_led_toggle(led_e led)
{
  GpioToggle(platform_convert_led(led));
}

// `vcom_ReceiveInit` equivalent
void platform_set_uart_rx_callback(void (*callback)(uint8_t *ch))
{
  uart_rx_callback = callback;
}

static Gpio_t *platform_convert_led(led_e led)
{
  switch (led)
  {
    case LED1:
      return &Led1;

    case LED2:
      return &Led2;

    case LED3:
      return &Led3;

    case LED4:
      return &Led4;

    default:
      return &Led1;
  }
}

static void platform_uart_rx_callback(UartNotifyId_t id)
{
  uint8_t data;

  if (id == UART_NOTIFY_RX)
  {
    if (UartGetChar(&Uart2, &data) == 0)
    {
      if (uart_rx_callback != NULL)
      {
        uart_rx_callback(&data);
      }
    }
  }
}

int32_t platform_nvm_write(uint8_t *src, uint16_t size, uint16_t offset)
{
  if (NvmmWrite(src, size, OFFSET) != size)
  {
    return -1;
  }

  uint32_t crc = Crc32(src, size);

  if (NvmmWrite((uint8_t *)&crc, sizeof(crc), OFFSET + size) != sizeof(crc))
  {
    return -1;
  }
  // here we return the total size written
  return (int32_t)size + sizeof(crc);
}

static bool platform_nvm_verify(uint16_t size, uint16_t offset)
{
  return NvmmCrc32Check(size + sizeof(uint32_t), OFFSET);
}

int32_t platform_nvm_read(uint8_t *dest, uint16_t size, uint16_t offset)
{
  if (!platform_nvm_verify(size, offset))
  {
    return -1;
  }

  if (NvmmRead(dest, size, OFFSET) != size)
  {
    return -1;
  }
  return (int32_t)size;
}

void platform_reload_watchdog(void)
{
  // not needed for Semtech
}

void platform_set_sleep_for_ms(uint32_t ms)
{
  // TODO(aydu): will be implemented as part of DEVSDK-1514
}