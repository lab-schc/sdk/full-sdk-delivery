set_property(SOURCE ${SEMTECH_PLATFORM_SRC_DIR}/boards/B-L072Z-LRWAN1/cmsis/arm-gcc/startup_stm32l072xx.s PROPERTY LANGUAGE C)

file(GLOB PLATFORM_SYS_SRC "${SEMTECH_PLATFORM_SRC_DIR}/system/*.c")
file(GLOB PLATFORM_NUC_SRC "${SEMTECH_PLATFORM_SRC_DIR}/boards/B-L072Z-LRWAN1/*.c")
file(GLOB PLATFORM_HAL_SRC "${SEMTECH_PLATFORM_SRC_DIR}/boards/mcu/stm32/STM32L0xx_HAL_Driver/Src/stm32l0xx_hal*.c")

list(REMOVE_ITEM PLATFORM_NUC_SRC "${SEMTECH_PLATFORM_SRC_DIR}/boards/B-L072Z-LRWAN1/i2c-board.c")
list(REMOVE_ITEM PLATFORM_NUC_SRC "${SEMTECH_PLATFORM_SRC_DIR}/boards/B-L072Z-LRWAN1/board.c")
list(REMOVE_ITEM PLATFORM_NUC_SRC "${SEMTECH_PLATFORM_SRC_DIR}/boards/B-L072Z-LRWAN1/uart-board.c")
list(REMOVE_ITEM PLATFORM_SYS_SRC "${SEMTECH_PLATFORM_SRC_DIR}/system/gps.c")
list(REMOVE_ITEM PLATFORM_SYS_SRC "${SEMTECH_PLATFORM_SRC_DIR}/system/i2c.c")
list(REMOVE_ITEM PLATFORM_SYS_SRC "${SEMTECH_PLATFORM_SRC_DIR}/system/adc.c")

add_library(
  ${PLATFORM_LIB}
  OBJECT
  ${CMAKE_CURRENT_LIST_DIR}/src/platform.c
  ${CMAKE_CURRENT_BINARY_DIR}/Patching/board.c
  ${CMAKE_CURRENT_BINARY_DIR}/Patching/uart-board.c
  ${CMAKE_CURRENT_LIST_DIR}/src/timer_semtech.c
  ${SEMTECH_PLATFORM_SRC_DIR}/radio/sx1276/sx1276.c
  ${SEMTECH_PLATFORM_SRC_DIR}/boards/mcu/utilities.c
  ${SEMTECH_PLATFORM_SRC_DIR}/boards/B-L072Z-LRWAN1/cmsis/system_stm32l0xx.c
  ${SEMTECH_PLATFORM_SRC_DIR}/boards/B-L072Z-LRWAN1/cmsis/arm-gcc/startup_stm32l072xx.s
  ${PLATFORM_SYS_SRC}
  ${PLATFORM_NUC_SRC}
  ${PLATFORM_HAL_SRC})

target_include_directories(
  ${PLATFORM_LIB}
  PRIVATE
  ${PLATFORM_INC_DIR}
)

find_program(PATCH_CMD patch)
add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/Patching/board.c ${CMAKE_CURRENT_BINARY_DIR}/Patching/uart-board.c ${CMAKE_CURRENT_BINARY_DIR}/Patching/stm32l072xx_flash.ld
  COMMENT "Patching platform files"
  COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_CURRENT_BINARY_DIR}/Patching
  COMMAND ${CMAKE_COMMAND} -E copy ${PROJECT_SOURCE_DIR}/platforms/${PLATFORM}/${L2_STACK}/patch/* ${CMAKE_CURRENT_BINARY_DIR}/Patching/
  COMMAND ${PATCH_CMD} ${SEMTECH_PLATFORM_SRC_DIR}/boards/${PLATFORM}/cmsis/arm-gcc/stm32l072xx_flash.ld  ${CMAKE_CURRENT_BINARY_DIR}/Patching/stm32l072xx_flash_stack_medium.ld.patch -o ${CMAKE_CURRENT_BINARY_DIR}/Patching/stm32l072xx_flash.ld
  COMMAND ${PATCH_CMD} ${SEMTECH_PLATFORM_SRC_DIR}/boards/${PLATFORM}/board.c  ${CMAKE_CURRENT_BINARY_DIR}/Patching/board.c.patch -o ${CMAKE_CURRENT_BINARY_DIR}/Patching/board.c
  COMMAND ${PATCH_CMD} ${SEMTECH_PLATFORM_SRC_DIR}/boards/${PLATFORM}/uart-board.c  ${CMAKE_CURRENT_BINARY_DIR}/Patching/uart-board.c.patch -o ${CMAKE_CURRENT_BINARY_DIR}/Patching/uart-board.c
)
